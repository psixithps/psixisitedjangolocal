from django.conf.urls import url
from main.views import mainInit

urlpatterns = [
    url(r'^$', mainInit, name = 'mainCentral'),
]