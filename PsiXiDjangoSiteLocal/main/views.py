import json
from collections import OrderedDict
from django.shortcuts import render
from django.template.loader import render_to_string as rts
from django.http import JsonResponse
from functools import lru_cache

#@lru_cache(maxsize = None)
class cachedFragments():
    def getMenu(self):
        menu = OrderedDict({
            0: {
                'textname': 'Главная'
                }
            })
        return menu

    def getMenuSerialized(self):
        menuSerialized = json.dumps(self.getMenu())
        return menuSerialized

def mainInit(request):
    if request.method == "GET":
        fragments = cachedFragments()
        if request.is_ajax():
            return JsonResponse({"newPage": json.dumps({
                "newTemplate": rts("main/main.html", {
                    'parentTemplate': 'base/classicPage/emptyParent.html',
                    'content': ('main', 'main'),
                    'menu': fragments.getMenuSerialized(),
                    }),
                'titl': '',
                'url': request.path
                })})
        return render(request, 'main/main.html', {
            'parentTemplate': 'base/classicPage/base.html',
            'content': ('main', 'main'),
            'menu': fragments.getMenuSerialized()
            })