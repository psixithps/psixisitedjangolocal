�}q (X   docqX&  This module provides access to some objects used or maintained by the
interpreter and to functions that interact strongly with the interpreter.

Dynamic objects:

argv -- command line arguments; argv[0] is the script pathname if known
path -- module search path; path[0] is the script directory, else ''
modules -- dictionary of loaded modules

displayhook -- called to show results in an interactive session
excepthook -- called to handle any uncaught exception other than SystemExit
  To customize printing in an interactive session or to install a custom
  top-level exception handler, assign other functions to replace these.

stdin -- standard input file object; used by input()
stdout -- standard output file object; used by print()
stderr -- standard error object; used for error messages
  By assigning other file objects (or objects that behave like files)
  to these, it is possible to redirect all of the interpreter's I/O.

last_type -- type of last uncaught exception
last_value -- value of last uncaught exception
last_traceback -- traceback of last uncaught exception
  These three are only available in an interactive session after a
  traceback has been printed.

Static objects:

builtin_module_names -- tuple of module names built into this interpreter
copyright -- copyright notice pertaining to this interpreter
exec_prefix -- prefix used to find the machine-specific Python library
executable -- absolute path of the executable binary of the Python interpreter
float_info -- a struct sequence with information about the float implementation.
float_repr_style -- string indicating the style of repr() output for floats
hash_info -- a struct sequence with information about the hash algorithm.
hexversion -- version information encoded as a single integer
implementation -- Python implementation information.
int_info -- a struct sequence with information about the int implementation.
maxsize -- the largest supported length of containers.
maxunicode -- the value of the largest Unicode code point
platform -- platform identifier
prefix -- prefix used to find the Python library
thread_info -- a struct sequence with information about the thread implementation.
version -- the version of this interpreter as a string
version_info -- version information as a named tuple
dllhandle -- [Windows only] integer handle of the Python DLL
winver -- [Windows only] version number of the Python DLL
__stdin__ -- the original stdin; don't touch!
__stdout__ -- the original stdout; don't touch!
__stderr__ -- the original stderr; don't touch!
__displayhook__ -- the original displayhook; don't touch!
__excepthook__ -- the original excepthook; don't touch!

Functions:

displayhook() -- print an object to the screen, and save it in builtins._
excepthook() -- print an exception and its traceback to sys.stderr
exc_info() -- return thread-safe information about the current exception
exit() -- exit the interpreter by raising SystemExit
getdlopenflags() -- returns flags to be used for dlopen() calls
getprofile() -- get the global profiling function
getrefcount() -- return the reference count for an object (plus one :-)
getrecursionlimit() -- return the max recursion depth for the interpreter
getsizeof() -- return the size of an object in bytes
gettrace() -- get the global debug tracing function
setcheckinterval() -- control how often the interpreter checks for events
setdlopenflags() -- set the flags to be used for dlopen() calls
setprofile() -- set the global profiling function
setrecursionlimit() -- set the max recursion depth for the interpreter
settrace() -- set the global debug tracing function
qX   membersq}q(X   get_coroutine_wrapperq}q(X   valueq}q(hXc   get_coroutine_wrapper()

Return the wrapper for coroutine objects set by sys.set_coroutine_wrapper.q	X	   overloadsq
]q}q(hXJ   Return the wrapper for coroutine objects set by sys.set_coroutine_wrapper.qX   argsq)uauX   kindqX   functionquX   argvq}q(h}qX   typeq]q(X   builtinsqX   listq�qX   __builtin__qX   listq�qeshX   dataquX   stdoutq}q(h}qh]q (X   _ioq!X   TextIOWrapperq"�q#hX   fileq$�q%eshhuX
   float_infoq&}q'(h}q(h]q)(X   sysq*X
   float_infoq+�q,X   sysq-X   sys.float_infoq.�q/eshhuX   internq0}q1(h}q2(hX   intern(string) -> string

``Intern'' the given string.  This enters the string in the (global)
table of interned strings whose purpose is to speed up dictionary lookups.
Return the string itself or the previously interned string object with the
same value.q3h
]q4(}q5(X   ret_typeq6]q7X    q8X
   string

``q9�q:ahX�   Intern'' the given string.  This enters the string in the (global)
table of interned strings whose purpose is to speed up dictionary lookups.
Return the string itself or the previously interned string object with the
same value.q;h}q<X   nameq=X   stringq>s�q?u}q@(X   ret_typeqA]qBhX   strqC�qDaX   argsqE}qF(X   typeqG]qHhDaX   nameqIX   stringqJu�qKueuhhuX
   __stdout__qL}qM(h}qNh]qO(h#h%eshhuX   winverqP}qQ(h}qRh]qS(hX   strqT�qUhDeshhuX   is_finalizingqV}qW(h}qX(hX1   is_finalizing()
Return True if Python is exiting.qYh
]qZ}q[(hX!   Return True if Python is exiting.q\h)uauhhuX   exc_infoq]}q^(h}q_(hX�   exc_info() -> (type, value, traceback)

Return information about the most recent exception caught by an except
clause in the current stack frame or in an older stack frame.q`h
]qa(}qb(h6]qch8h8�qdahX�   (type, value, traceback)

Return information about the most recent exception caught by an except
clause in the current stack frame or in an older stack frame.qeh)u}qf(X   ret_typeqg]qhhX   tupleqi�qjaX   argsqk)ueuhhuX   setcheckintervalql}qm(h}qn(hX�   setcheckinterval(n)

Tell the Python interpreter to check for asynchronous events every
n instructions.  This also affects how often thread switches occur.qoh
]qp(}qq(hX�   Tell the Python interpreter to check for asynchronous events every
n instructions.  This also affects how often thread switches occur.qrh}qsh=X   nqts�quu}qv(hg]qwhX   NoneTypeqx�qyahk}qz(X   typeq{]q|hX   intq}�q~aX   nameqX   valueq�u�q�ueuhhuX   version_infoq�}q�(h}q�h]q�(X   sysq�X   version_infoq��q�hX   sys.version_infoq��q�eshhuX   int_infoq�}q�(h}q�h]q�(X   sysq�X   int_infoq��q�X   sysq�X   int_infoq��q�eshhuX   __package__q�}q�(h}q�h]q�(hUhyeshhuX   api_versionq�}q�(h}q�h]q�(hX   intq��q�h~eshhuX   __interactivehook__q�}q�(h}q�h
NshhuX   __displayhook__q�}q�(h}q�(hXZ   displayhook(object) -> None

Print an object to sys.stdout and also save it in builtins._
q�h
]q�(}q�(h6]q�hX   NoneTypeq��q�ahX=   Print an object to sys.stdout and also save it in builtins._
q�h}q�h=X   objectq�s�q�u}q�(hg]q�hyahk}q�(h{]q�hX   objectq��q�ahX   valueq�u�q�ueuhhuX	   hash_infoq�}q�(h}q�h]q�(X   sysq�X	   hash_infoq��q�X   sysq�X	   hash_infoq��q�eshhuX   pathq�}q�(h}q�h]q�(hheshhuX   _homeq�}q�(h}q�h]q�hUashhuX
   hexversionq�}q�(h}q�h]q�(h�h~eshhuX   __spec__q�}q�(h}q�h]q�X   _frozen_importlibq�X
   ModuleSpecq҆q�ashhuX   __excepthook__q�}q�(h}q�(hXt   excepthook(exctype, value, traceback) -> None

Handle an exception by displaying it with a traceback on sys.stderr.
q�h
]q�(}q�(h6]q�h�ahXE   Handle an exception by displaying it with a traceback on sys.stderr.
q�h}q�h=X   exctypeq�s}q�h=X   valueq�s}q�h=X	   tracebackq�s�q�u}q�(hg]q�hyahk}q�(h{]q�h�ahX   exctypeq�u}q�(h{]q�h�ahX   valueq�u}q�(h{]q�h�ahX	   tracebackq�u�q�ueuhhuX   getrecursionlimitq�}q�(h}q�(hX�   getrecursionlimit()

Return the current value of the recursion limit, the maximum depth
of the Python interpreter stack.  This limit prevents infinite
recursion from causing an overflow of the C stack and crashing Python.q�h
]q�(}q�(hX�   Return the current value of the recursion limit, the maximum depth
of the Python interpreter stack.  This limit prevents infinite
recursion from causing an overflow of the C stack and crashing Python.q�h)u}q�(hg]q�h~ahk)ueuhhuX   thread_infoq�}q�(h}q�h]q�X   sysq�X   thread_infoq��q�ashhuX   prefixq�}r   (h}r  h]r  (hUhDeshhuX   flagsr  }r  (h}r  h]r  (X   sysr  X   flagsr  �r	  h-X   flagsr
  �r  eshhuX   base_exec_prefixr  }r  (h}r  h]r  hUashhuX	   _getframer  }r  (h}r  (hX�  _getframe([depth]) -> frameobject

Return a frame object from the call stack.  If optional integer depth is
given, return the frame object that many calls below the top of the stack.
If that is deeper than the call stack, ValueError is raised.  The default
for depth is zero, returning the frame at the top of the call stack.

This function should be used for internal and specialized
purposes only.r  h
]r  }r  (h6]r  h8X   frameobjectr  �r  ahXl  Return a frame object from the call stack.  If optional integer depth is
given, return the frame object that many calls below the top of the stack.
If that is deeper than the call stack, ValueError is raised.  The default
for depth is zero, returning the frame at the top of the call stack.

This function should be used for internal and specialized
purposes only.r  h}r  (h=X   depthr  X   default_valuer  X   Noner  u�r  uauhhuX   setswitchintervalr  }r   (h}r!  (hX|  setswitchinterval(n)

Set the ideal thread switching delay inside the Python interpreter
The actual frequency of switching threads can be lower if the
interpreter executes long sequences of uninterruptible code
(this is implementation-specific and workload-dependent).

The parameter must represent the desired switching delay in seconds
A typical value is 0.005 (5 milliseconds).r"  h
]r#  (}r$  (hXf  Set the ideal thread switching delay inside the Python interpreter
The actual frequency of switching threads can be lower if the
interpreter executes long sequences of uninterruptible code
(this is implementation-specific and workload-dependent).

The parameter must represent the desired switching delay in seconds
A typical value is 0.005 (5 milliseconds).r%  h}r&  h=hts�r'  u}r(  (hA]r)  hyahE}r*  (hG]r+  hX   floatr,  �r-  ahIhtu�r.  ueuhhuX   implementationr/  }r0  (h}r1  h]r2  (X   typesr3  X   SimpleNamespacer4  �r5  hX   sys.implementationr6  �r7  eshhuX   displayhookr8  }r9  (h}r:  (hXZ   displayhook(object) -> None

Print an object to sys.stdout and also save it in builtins._
r;  h
]r<  (}r=  (h6]r>  h�ahX=   Print an object to sys.stdout and also save it in builtins._
r?  h}r@  h=X   objectrA  s�rB  u}rC  (hg]rD  hyahk}rE  (h{]rF  h�ahh�u�rG  ueuhhuX   dont_write_bytecoderH  }rI  (h}rJ  h]rK  (hX   boolrL  �rM  hX   boolrN  �rO  eshhuX   _debugmallocstatsrP  }rQ  (h}rR  (hX�   _debugmallocstats()

Print summary info to stderr about the state of
pymalloc's structures.

In Py_DEBUG mode, also perform some expensive internal consistency
checks.
rS  h
]rT  }rU  (hX�   Print summary info to stderr about the state of
pymalloc's structures.

In Py_DEBUG mode, also perform some expensive internal consistency
checks.
rV  h)uauhhuX   gettracerW  }rX  (h}rY  (hX{   gettrace()

Return the global debug tracing function set with sys.settrace.
See the debugger chapter in the library manual.rZ  h
]r[  (}r\  (hXo   Return the global debug tracing function set with sys.settrace.
See the debugger chapter in the library manual.r]  h)u}r^  (hg]r_  h�ahk)ueuhhuX   exitr`  }ra  (h}rb  (hX>  exit([status])

Exit the interpreter by raising SystemExit(status).
If the status is omitted or None, it defaults to zero (i.e., success).
If the status is an integer, it will be used as the system exit status.
If it is another kind of object, it will be printed and the system
exit status will be one (i.e., failure).rc  h
]rd  (}re  (hX.  Exit the interpreter by raising SystemExit(status).
If the status is omitted or None, it defaults to zero (i.e., success).
If the status is an integer, it will be used as the system exit status.
If it is another kind of object, it will be printed and the system
exit status will be one (i.e., failure).rf  h}rg  (h=X   statusrh  j  j  u�ri  u}rj  (hg]rk  hyahk)u}rl  (hg]rm  hyahk}rn  (h{]ro  h�ahX   coderp  u�rq  ueuhhuX   float_repr_stylerr  }rs  (h}rt  h]ru  hUashhuX   __doc__rv  }rw  (h}rx  h]ry  (hUhDeshhuX
   __stderr__rz  }r{  (h}r|  h]r}  (h#h%eshhuX   getallocatedblocksr~  }r  (h}r�  (hXr   getallocatedblocks() -> integer

Return the number of memory blocks currently allocated, regardless of their
size.r�  h
]r�  }r�  (h6]r�  hX   intr�  �r�  ahXQ   Return the number of memory blocks currently allocated, regardless of their
size.r�  h)uauhhuX   getfilesystemencodingr�  }r�  (h}r�  (hXw   getfilesystemencoding() -> string

Return the encoding used to convert Unicode filenames in
operating system filenames.r�  h
]r�  (}r�  (h6]r�  hX   strr�  �r�  ahXT   Return the encoding used to convert Unicode filenames in
operating system filenames.r�  h)u}r�  (hg]r�  h�ahk)ueuhhuX	   __stdin__r�  }r�  (h}r�  h]r�  (h#h%eshhuX
   maxunicoder�  }r�  (h}r�  h]r�  (h�h~eshhuX   getrefcountr�  }r�  (h}r�  (hX�   getrefcount(object) -> integer

Return the reference count of object.  The count returned is generally
one higher than you might expect, because it includes the (temporary)
reference as an argument to getrefcount().r�  h
]r�  (}r�  (h6]r�  j�  ahX�   Return the reference count of object.  The count returned is generally
one higher than you might expect, because it includes the (temporary)
reference as an argument to getrefcount().r�  h}r�  h=X   objectr�  s�r�  u}r�  (hg]r�  hyahk)ueuhhuX
   _mercurialr�  }r�  (h}r�  h]r�  hX   tupler�  �r�  ashhuX
   path_hooksr�  }r�  (h}r�  h]r�  (hheshhuX	   callstatsr�  }r�  (h}r�  (hX�  callstats() -> tuple of integers

Return a tuple of function call statistics, if CALL_PROFILE was defined
when Python was built.  Otherwise, return None.

When enabled, this function returns detailed, implementation-specific
details about the number of function calls executed. The return value is
a 11-tuple where the entries in the tuple are counts of:
0. all function calls
1. calls to PyFunction_Type objects
2. PyFunction calls that do not create an argument tuple
3. PyFunction calls that do not create an argument tuple
   and bypass PyEval_EvalCodeEx()
4. PyMethod calls
5. PyMethod calls on bound methods
6. PyType calls
7. PyCFunction calls
8. generator calls
9. All other calls
10. Number of stack pops performed by call_function()r�  h
]r�  (}r�  (h6]r�  hX   tupler�  �r�  ahX�  Return a tuple of function call statistics, if CALL_PROFILE was defined
when Python was built.  Otherwise, return None.

When enabled, this function returns detailed, implementation-specific
details about the number of function calls executed. The return value is
a 11-tuple where the entries in the tuple are counts of:
0. all function calls
1. calls to PyFunction_Type objects
2. PyFunction calls that do not create an argument tuple
3. PyFunction calls that do not create an argument tuple
   and bypass PyEval_EvalCodeEx()
4. PyMethod calls
5. PyMethod calls on bound methods
6. PyType calls
7. PyCFunction calls
8. generator calls
9. All other calls
10. Number of stack pops performed by call_function()r�  h)u}r�  (hg]r�  h�ahk)ueuhhuX   platformr�  }r�  (h}r�  h]r�  (hUhDeshhuX   maxsizer�  }r�  (h}r�  h]r�  (h�h~eshhuX   __name__r�  }r�  (h}r�  h]r�  (hUhDeshhuX   exec_prefixr�  }r�  (h}r�  h]r�  (hUhDeshhuX	   _xoptionsr�  }r�  (h}r�  h]r�  (hX   dictr�  �r�  hX   dictr�  �r�  eshhuX   versionr�  }r�  (h}r�  h]r�  (hUhDeshhuX
   setprofiler�  }r�  (h}r�  (hX�   setprofile(function)

Set the profiling function.  It will be called on each function call
and return.  See the profiler chapter in the library manual.r�  h
]r�  }r�  (hX�   Set the profiling function.  It will be called on each function call
and return.  See the profiler chapter in the library manual.r�  h}r�  h=X   functionr�  s�r�  uauhhuX   getdefaultencodingr�  }r�  (h}r�  (hXo   getdefaultencoding() -> string

Return the current default string encoding used by the Unicode 
implementation.r�  h
]r�  (}r�  (h6]r�  j�  ahXO   Return the current default string encoding used by the Unicode 
implementation.r�  h)u}r�  (hg]r�  hDahk)ueuhhuX   call_tracingr�  }r�  (h}r�  (hX�   call_tracing(func, args) -> object

Call func(*args), while tracing is enabled.  The tracing state is
saved, and restored afterwards.  This is intended to be called from
a debugger from a checkpoint, to recursively debug some other code.r�  h
]r�  (}r�  (h6]r�  hX   objectr�  �r�  ahX�   Call func(*args), while tracing is enabled.  The tracing state is
saved, and restored afterwards.  This is intended to be called from
a debugger from a checkpoint, to recursively debug some other code.r�  h}r�  h=X   funcr�  s}r�  h=X   argsr�  s�r�  u}r�  (hg]r�  hyahk}r   (h{]r  h�ahX   funcr  u}r  (h{]r  hjahX   argsr  u�r  ueuhhuX   getswitchintervalr  }r  (h}r	  (hXO   getswitchinterval() -> current thread switch interval; see setswitchinterval().r
  h
]r  (}r  (h6]r  h8X   currentr  �r  ahX   ().r  h)u}r  (hA]r  j-  ahE)ueuhhuX   _current_framesr  }r  (h}r  (hX�   _current_frames() -> dictionary

Return a dictionary mapping each current thread T's thread id to T's
current stack frame.

This function should be used for specialized purposes only.r  h
]r  }r  (h6]r  hX   dictr  �r  ahX�   Return a dictionary mapping each current thread T's thread id to T's
current stack frame.

This function should be used for specialized purposes only.r  h)uauhhuX
   getprofiler  }r  (h}r  (hXt   getprofile()

Return the profiling function set with sys.setprofile.
See the profiler chapter in the library manual.r   h
]r!  }r"  (hXf   Return the profiling function set with sys.setprofile.
See the profiler chapter in the library manual.r#  h)uauhhuX
   __loader__r$  }r%  (h]r&  h�X   BuiltinImporterr'  �r(  ahX   typerefr)  uX   stdinr*  }r+  (h}r,  h]r-  (h#h%eshhuX	   copyrightr.  }r/  (h}r0  h]r1  (hUhDeshhuX
   executabler2  }r3  (h}r4  h]r5  (hUhDeshhuX   modulesr6  }r7  (h}r8  h]r9  (j�  j�  eshhuX   setrecursionlimitr:  }r;  (h}r<  (hX�   setrecursionlimit(n)

Set the maximum depth of the Python interpreter stack to n.  This
limit prevents infinite recursion from causing an overflow of the C
stack and crashing Python.  The highest possible limit is platform-
dependent.r=  h
]r>  (}r?  (hX�   Set the maximum depth of the Python interpreter stack to n.  This
limit prevents infinite recursion from causing an overflow of the C
stack and crashing Python.  The highest possible limit is platform-
dependent.r@  h}rA  h=hts�rB  u}rC  (hg]rD  hyahk}rE  (h{]rF  h~ahX   limitrG  u�rH  ueuhhuX	   getsizeofrI  }rJ  (h}rK  (hXF   getsizeof(object, default) -> int

Return the size of object in bytes.rL  h
]rM  (}rN  (h6]rO  j�  ahX#   Return the size of object in bytes.rP  h}rQ  h=X   objectrR  s}rS  h=X   defaultrT  s�rU  u}rV  (hg]rW  h~ahk}rX  (h{]rY  h�ahX   orZ  u�r[  ueuhhuX   builtin_module_namesr\  }r]  (h}r^  h]r_  (j�  hjeshhuX   base_prefixr`  }ra  (h}rb  h]rc  hUashhuX	   dllhandlerd  }re  (h}rf  h]rg  (h�h~eshhuX	   meta_pathrh  }ri  (h}rj  h]rk  (hheshhuX   __plenrl  }rm  (h}rn  h]ro  h�ashhuX   _clear_type_cacherp  }rq  (h}rr  (hXA   _clear_type_cache() -> None
Clear the internal type lookup cache.rs  h
]rt  }ru  (h6]rv  h�ahX%   Clear the internal type lookup cache.rw  h)uauhhuX   warnoptionsrx  }ry  (h}rz  h]r{  (hheshhuX
   excepthookr|  }r}  (h}r~  (hXt   excepthook(exctype, value, traceback) -> None

Handle an exception by displaying it with a traceback on sys.stderr.
r  h
]r�  (}r�  (h6]r�  h�ahXE   Handle an exception by displaying it with a traceback on sys.stderr.
r�  h}r�  h=X   exctyper�  s}r�  h=X   valuer�  s}r�  h=X	   tracebackr�  s�r�  u}r�  (hg]r�  hyahk}r�  (h{]r�  h�ahh�u}r�  (h{]r�  h�ahh�u}r�  (h{]r�  h�ahh�u�r�  ueuhhuX   getcheckintervalr�  }r�  (h}r�  (hXE   getcheckinterval() -> current check interval; see setcheckinterval().r�  h
]r�  (}r�  (h6]r�  j  ahX   ().r�  h)u}r�  (hg]r�  h~ahk)ueuhhuX   __egginsertr�  }r�  (h}r�  h]r�  h�ashhuj'  }r�  (h}r�  (h}r�  (X   __ge__r�  }r�  (h}r�  (hX   Return self>=value.r�  h
]r�  }r�  (hX   Return self>=value.r�  h}r�  (h=hX
   arg_formatr�  X   *r�  u}r�  (h=X   kwargsr�  j�  X   **r�  u�r�  uauhX   methodr�  uX   create_moduler�  }r�  (h}r�  (hX   Create a built-in moduler�  h
]r�  }r�  (hX   Create a built-in moduler�  h}r�  (h=hj�  j�  u}r�  (h=j�  j�  j�  u�r�  uauhhuX   __str__r�  }r�  (h}r�  (hX   Return str(self).r�  h
]r�  }r�  (hX   Return str(self).r�  h}r�  (h=hj�  j�  u}r�  (h=j�  j�  j�  u�r�  uauhj�  uX   __repr__r�  }r�  (h}r�  (hX   Return repr(self).r�  h
]r�  }r�  (hX   Return repr(self).r�  h}r�  (h=hj�  j�  u}r�  (h=j�  j�  j�  u�r�  uauhj�  uX   __init__r�  }r�  (h}r�  (hX>   Initialize self.  See help(type(self)) for accurate signature.r�  h
]r�  }r�  (hX>   Initialize self.  See help(type(self)) for accurate signature.r�  h}r�  (h=hj�  j�  u}r�  (h=j�  j�  j�  u�r�  uauhj�  uX
   get_sourcer�  }r�  (h}r�  (hX8   Return None as built-in modules do not have source code.r�  h
]r�  }r�  (hX8   Return None as built-in modules do not have source code.r�  h}r�  (h=hj�  j�  u}r�  (h=j�  j�  j�  u�r�  uauhhujv  }r�  (h}r�  h]r�  hUashhuX   module_reprr�  }r�  (h}r�  (hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        r�  h
]r�  }r�  (hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        r�  h}r�  (h=hj�  j�  u}r�  (h=j�  j�  j�  u�r�  uauhhuX   __new__r�  }r�  (h}r�  (hXG   Create and return a new object.  See help(type) for accurate signature.r�  h
]r�  }r�  (hXG   Create and return a new object.  See help(type) for accurate signature.r�  h}r�  (h=hj�  j�  u}r�  (h=j�  j�  j�  u�r�  uauhhuX
   __module__r�  }r�  (h}r�  h]r   hUashhuX   load_moduler  }r  (h}r  (hX�   Load the specified module into sys.modules and return it.

    This method is deprecated.  Use loader.exec_module instead.

    r  h
]r  }r  (hX�   Load the specified module into sys.modules and return it.

    This method is deprecated.  Use loader.exec_module instead.

    r  h}r  (h=hj�  j�  u}r	  (h=j�  j�  j�  u�r
  uauhhuX   __dir__r  }r  (h}r  (hX.   __dir__() -> list
default dir() implementationr  h
]r  }r  (h6]r  hX   listr  �r  ahX   default dir() implementationr  h}r  (h]r  j�  ah=X   selfr  u�r  uauhj�  uX	   __class__r  }r  (h]r  hX   typer  �r  ahj)  uX   __reduce_ex__r  }r  (h}r   (hX   helper for pickler!  h
]r"  }r#  (hX   helper for pickler$  h}r%  (h=hj�  j�  u}r&  (h=j�  j�  j�  u�r'  uauhj�  uX
   __reduce__r(  }r)  (h}r*  (hX   helper for pickler+  h
]r,  }r-  (hX   helper for pickler.  h}r/  (h=hj�  j�  u}r0  (h=j�  j�  j�  u�r1  uauhj�  uX
   __sizeof__r2  }r3  (h}r4  (hX6   __sizeof__() -> int
size of object in memory, in bytesr5  h
]r6  }r7  (h6]r8  j�  ahX"   size of object in memory, in bytesr9  h}r:  (h]r;  j�  ah=j  u�r<  uauhj�  uX   find_moduler=  }r>  (h}r?  (hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        r@  h
]rA  }rB  (hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        rC  h}rD  (h=hj�  j�  u}rE  (h=j�  j�  j�  u�rF  uauhhuX   __gt__rG  }rH  (h}rI  (hX   Return self>value.rJ  h
]rK  }rL  (hX   Return self>value.rM  h}rN  (h=hj�  j�  u}rO  (h=j�  j�  j�  u�rP  uauhj�  uX   __dict__rQ  }rR  (h}rS  h]rT  hX   mappingproxyrU  �rV  ashhuX   exec_modulerW  }rX  (h}rY  (hX   Exec a built-in modulerZ  h
]r[  }r\  (hX   Exec a built-in moduler]  h}r^  (h=hj�  j�  u}r_  (h=j�  j�  j�  u�r`  uauhhuX   __eq__ra  }rb  (h}rc  (hX   Return self==value.rd  h
]re  }rf  (hX   Return self==value.rg  h}rh  (h=hj�  j�  u}ri  (h=j�  j�  j�  u�rj  uauhj�  uX   __hash__rk  }rl  (h}rm  (hX   Return hash(self).rn  h
]ro  }rp  (hX   Return hash(self).rq  h}rr  (h=hj�  j�  u}rs  (h=j�  j�  j�  u�rt  uauhj�  uX   __le__ru  }rv  (h}rw  (hX   Return self<=value.rx  h
]ry  }rz  (hX   Return self<=value.r{  h}r|  (h=hj�  j�  u}r}  (h=j�  j�  j�  u�r~  uauhj�  uX
   __format__r  }r�  (h}r�  (hX   default object formatterr�  h
]r�  }r�  (hX   default object formatterr�  h}r�  (h=hj�  j�  u}r�  (h=j�  j�  j�  u�r�  uauhj�  uX   __weakref__r�  }r�  (h}r�  (h]r�  hX   objectr�  �r�  ahX2   list of weak references to the object (if defined)r�  uhX   propertyr�  uX   get_coder�  }r�  (h}r�  (hX9   Return None as built-in modules do not have code objects.r�  h
]r�  }r�  (hX9   Return None as built-in modules do not have code objects.r�  h}r�  (h=hj�  j�  u}r�  (h=j�  j�  j�  u�r�  uauhhuX	   find_specr�  }r�  (h}r�  h]r�  hX   methodr�  �r�  ashhuX   __setattr__r�  }r�  (h}r�  (hX%   Implement setattr(self, name, value).r�  h
]r�  }r�  (hX%   Implement setattr(self, name, value).r�  h}r�  (h=hj�  j�  u}r�  (h=j�  j�  j�  u�r�  uauhj�  uX   __lt__r�  }r�  (h}r�  (hX   Return self<value.r�  h
]r�  }r�  (hX   Return self<value.r�  h}r�  (h=hj�  j�  u}r�  (h=j�  j�  j�  u�r�  uauhj�  uX   __subclasshook__r�  }r�  (h}r�  (hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r�  h
]r�  }r�  (hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r�  h}r�  (h=hj�  j�  u}r�  (h=j�  j�  j�  u�r�  uauhhuX
   is_packager�  }r�  (h}r�  (hX4   Return False as built-in modules are never packages.r�  h
]r�  }r�  (hX4   Return False as built-in modules are never packages.r�  h}r�  (h=hj�  j�  u}r�  (h=j�  j�  j�  u�r�  uauhhuX   __delattr__r�  }r�  (h}r�  (hX   Implement delattr(self, name).r�  h
]r�  }r�  (hX   Implement delattr(self, name).r�  h}r�  (h=hj�  j�  u}r�  (h=j�  j�  j�  u�r�  uauhj�  uX   __ne__r�  }r�  (h}r�  (hX   Return self!=value.r�  h
]r�  }r�  (hX   Return self!=value.r�  h}r�  (h=hj�  j�  u}r�  (h=j�  j�  j�  u�r�  uauhj�  uuhX�   Meta path import for built-in modules.

    All methods are either class or static methods to avoid the need to
    instantiate the class.

    r�  X	   is_hiddenr�  �X   basesr�  ]r�  j�  aX   mror�  ]r�  (j(  j�  euhhuX	   byteorderr�  }r�  (h}r�  h]r�  (hUhDeshhuX   set_coroutine_wrapperr�  }r�  (h}r�  (hXD   set_coroutine_wrapper(wrapper)

Set a wrapper for coroutine objects.r�  h
]r�  }r�  (hX$   Set a wrapper for coroutine objects.r�  h}r�  h=X   wrapperr�  s�r�  uauhhuX   settracer�  }r�  (h}r�  (hX�   settrace(function)

Set the global debug tracing function.  It will be called on each
function call.  See the debugger chapter in the library manual.r�  h
]r�  (}r�  (hX�   Set the global debug tracing function.  It will be called on each
function call.  See the debugger chapter in the library manual.r�  h}r�  h=X   functionr�  s�r�  u}r�  (hg]r�  hyahk}r�  (h{]r�  h�ahjZ  u�r�  ueuhhuX   stderrr   }r  (h}r  h]r  (h#h%eshhuX   getwindowsversionr  }r  (h}r  (hX2  getwindowsversion()

Return information about the running version of Windows as a named tuple.
The members are named: major, minor, build, platform, service_pack,
service_pack_major, service_pack_minor, suite_mask, and product_type. For
backward compatibility, only the first 5 items are available by indexing.
All elements are numbers, except service_pack which is a string. Platform
may be 0 for win32s, 1 for Windows 9x/ME, 2 for Windows NT/2000/XP/Vista/7,
3 for Windows CE. Product_type may be 1 for a workstation, 2 for a domain
controller, 3 for a server.r  h
]r  (}r	  (hX  Return information about the running version of Windows as a named tuple.
The members are named: major, minor, build, platform, service_pack,
service_pack_major, service_pack_minor, suite_mask, and product_type. For
backward compatibility, only the first 5 items are available by indexing.
All elements are numbers, except service_pack which is a string. Platform
may be 0 for win32s, 1 for Windows 9x/ME, 2 for Windows NT/2000/XP/Vista/7,
3 for Windows CE. Product_type may be 1 for a workstation, 2 for a domain
controller, 3 for a server.r
  h)u}r  (hg]r  hjahk)ueuhhuX   path_importer_cacher  }r  (h}r  h]r  (j�  j�  eshhuuu.