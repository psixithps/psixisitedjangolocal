�}q (X   docqX  Functions to convert between Python values and C structs.
Python bytes objects are used to hold the data representing the C struct
and also as format strings (explained below) to describe the layout of data
in the C struct.

The optional first format char indicates byte order, size and alignment:
  @: native order, size & alignment (default)
  =: native order, std. size & alignment
  <: little-endian, std. size & alignment
  >: big-endian, std. size & alignment
  !: same as >

The remaining chars indicate types of args and must match exactly;
these can be preceded by a decimal repeat count:
  x: pad byte (no data); c:char; b:signed byte; B:unsigned byte;
  ?: _Bool (requires C99; if not available, char is used instead)
  h:short; H:unsigned short; i:int; I:unsigned int;
  l:long; L:unsigned long; f:float; d:double.
Special cases (preceding decimal count indicates length):
  s:string (array of char); p: pascal string (with count byte).
Special cases (only available in native format):
  n:ssize_t; N:size_t;
  P:an integer type that is wide enough to hold a pointer.
Special case (not in native mode unless 'long long' in platform C):
  q:long long; Q:unsigned long long
Whitespace between formats is ignored.

The variable struct.error is an exception raised on errors.
qX   membersq}q(X   iter_unpackq}q(X   valueq}q(hX  iter_unpack(fmt, buffer) -> iterator(v1, v2, ...)

Return an iterator yielding tuples unpacked from the given bytes
source according to the format string, like a repeated invocation of
unpack_from().  Requires that the bytes length be a multiple of the
format struct size.q	X	   overloadsq
]q}q(X   ret_typeq]qX    qX   iteratorq�qahX�   (v1, v2, ...)

Return an iterator yielding tuples unpacked from the given bytes
source according to the format string, like a repeated invocation of
unpack_from().  Requires that the bytes length be a multiple of the
format struct size.qX   argsq}qX   nameqX   fmtqs}qhX   bufferqs�quauX   kindqX   functionquX   __package__q}q(h}qX   typeq]q (X   builtinsq!X   strq"�q#X   __builtin__q$X   NoneTypeq%�q&eshX   dataq'uX   packq(}q)(h}q*(hX�   pack(fmt, v1, v2, ...) -> bytes

Return a bytes object containing the values v1, v2, ... packed according
to the format string fmt.  See help(struct) for more on format strings.q+h
]q,(}q-(h]q.h!X   bytesq/�q0ahX�   Return a bytes object containing the values v1, v2, ... packed according
to the format string fmt.  See help(struct) for more on format strings.q1h(}q2hX   fmtq3s}q4hX   v1q5s}q6hX   v2q7s}q8(hhX
   arg_formatq9X   *q:utq;u}q<(X   ret_typeq=]q>h$X   strq?�q@aX   argsqA}qB(X   typeqC]qDh@aX   nameqEX   fmtqFu}qG(hC]qHh$X   tupleqI�qJaX
   arg_formatqKh:hEX   valuesqLu�qMueuhhuX   _clearcacheqN}qO(h}qP(hX   Clear the internal cache.qQh
]qR(}qS(hX   Clear the internal cache.qTh}qU(hhh9h:u}qV(hX   kwargsqWh9X   **qXu�qYu}qZ(h=]q[h&ahA)ueuhhuX   __doc__q\}q](h}q^h]q_(h#h&eshh'uX   unpack_fromq`}qa(h}qb(hX�   unpack_from(fmt, buffer, offset=0) -> (v1, v2, ...)

Return a tuple containing values unpacked according to the format string
fmt.  Requires len(buffer[offset:]) >= calcsize(fmt).  See help(struct)
for more on format strings.qch
]qd(}qe(h]qfhh�qgahX�   (v1, v2, ...)

Return a tuple containing values unpacked according to the format string
fmt.  Requires len(buffer[offset:]) >= calcsize(fmt).  See help(struct)
for more on format strings.qhh}qihX   fmtqjs}qkhX   bufferqls}qm(hX   offsetqnX   default_valueqoX   0qpu�qqu}qr(h=]qshJahA}qt(hC]quh@ahEX   fmtqvu}qw(hC]qxX   arrayqyX   arrayqz�q{ahEX   bufferq|u}q}(hC]q~h$X   intq�q�ahEX   offsetq�X   default_valueq�hpu�q�u}q�(h=]q�hJahA}q�(hC]q�h@ahEX   fmtq�u}q�(hC]q�h@ahEX   bufferq�u}q�(hC]q�h�ahEX   offsetq�h�hpu�q�u}q�(h=]q�hJahA}q�(hC]q�h@ahEX   fmtq�u}q�(hC]q�h$X   bufferq��q�ahEX   bufferq�u}q�(hC]q�h�ahEX   offsetq�h�hpu�q�ueuhhuX   __name__q�}q�(h}q�h]q�(h#h@eshh'uX   errorq�}q�(h}q�(h}q�(X   __ge__q�}q�(h}q�(hX   Return self>=value.q�h
]q�}q�(hX   Return self>=value.q�h}q�(hhh9h:u}q�(hhWh9hXu�q�uauhX   methodq�uX   __delattr__q�}q�(h}q�(hX   Implement delattr(self, name).q�h
]q�(}q�(hX   Implement delattr(self, name).q�h}q�(hhh9h:u}q�(hhWh9hXu�q�u}q�(h=]q�h&ahA}q�(hC]q�h$X   objectq��q�ahEX   selfq�u}q�(hC]q�h@ahEX   nameq�u�q�ueuhh�uX   __str__q�}q�(h}q�(hX   Return str(self).q�h
]q�(}q�(hX   Return str(self).q�h}q�(hhh9h:u}q�(hhWh9hXu�q�u}q�(h=]q�h@ahA}q�(hC]q�h�ahEX   selfq�u�q�ueuhh�uX   __traceback__q�}q�(h}q�h]q�h!X   objectqچq�ashX   propertyq�uX   __init__q�}q�(h}q�(hX>   Initialize self.  See help(type(self)) for accurate signature.q�h
]q�(}q�(hX>   Initialize self.  See help(type(self)) for accurate signature.q�h}q�(hhh9h:u}q�(hhWh9hXu�q�u}q�(h=]q�h&ahA}q�(hC]q�X
   exceptionsq�X   BaseExceptionq�q�ahEX   selfq�u}q�(hC]q�hJahKh:hEX   argsq�u�q�ueuhh�uX   __repr__q�}q�(h}q�(hX   Return repr(self).q�h
]q�(}q�(hX   Return repr(self).q�h}q�(hhh9h:u}q�(hhWh9hXu�q�u}q�(h=]q�h@ahA}q�(hC]r   h�ahEh�u�r  ueuhh�uh\}r  (h}r  h]r  (h!X   NoneTyper  �r  h@eshh'uX   with_tracebackr  }r  (h}r	  (hXQ   Exception.with_traceback(tb) --
    set self.__traceback__ to tb and return self.r
  h
]r  }r  (hX-   set self.__traceback__ to tb and return self.r  h}r  (h]r  h!X   objectr  �r  ahX   selfr  u}r  hX   tbr  s�r  uauhh�uX   __new__r  }r  (h}r  (hXG   Create and return a new object.  See help(type) for accurate signature.r  h
]r  (}r  (hXG   Create and return a new object.  See help(type) for accurate signature.r  h}r  (hhh9h:u}r  (hhWh9hXu�r  u}r   (h=]r!  h�ahA}r"  (hC]r#  h$X   typer$  �r%  ahEX   clsr&  u}r'  (hC]r(  h$X   dictr)  �r*  ahKX   **r+  hEX   kwArgsr,  u}r-  (hC]r.  hJahKh:hEX   argsr/  u�r0  u}r1  (h=]r2  h�ahA}r3  (hC]r4  j%  ahEX   clsr5  u}r6  (hC]r7  hJahKh:hEX   argsr8  u�r9  ueuhhuX
   __module__r:  }r;  (h}r<  h]r=  (h#h@eshh'uX
   __reduce__r>  }r?  (h}r@  (hX   helper for picklerA  h
]rB  }rC  (h=]rD  h�ahA}rE  (hC]rF  h�ahEh�u�rG  uauhh�uX   __dir__rH  }rI  (h}rJ  (hX.   __dir__() -> list
default dir() implementationrK  h
]rL  }rM  (h]rN  h!X   listrO  �rP  ahX   default dir() implementationrQ  h}rR  (h]rS  j  ahj  u�rT  uauhh�uX	   __class__rU  }rV  (h]rW  h!X   typerX  �rY  ahX   typerefrZ  uX   __reduce_ex__r[  }r\  (h}r]  (hX   helper for pickler^  h
]r_  (}r`  (hX   helper for picklera  h}rb  (hhh9h:u}rc  (hhWh9hXu�rd  u}re  (h=]rf  h�ahA}rg  (hC]rh  h�ahEh�u}ri  (hC]rj  h�ahEX   protocolrk  u�rl  ueuhh�uX
   __sizeof__rm  }rn  (h}ro  (hX6   __sizeof__() -> int
size of object in memory, in bytesrp  h
]rq  (}rr  (h]rs  h!X   intrt  �ru  ahX"   size of object in memory, in bytesrv  h}rw  (h]rx  j  ahj  u�ry  u}rz  (h=]r{  h�ahA}r|  (hC]r}  h�ahEX   selfr~  u�r  ueuhh�uX   __gt__r�  }r�  (h}r�  (hX   Return self>value.r�  h
]r�  }r�  (hX   Return self>value.r�  h}r�  (hhh9h:u}r�  (hhWh9hXu�r�  uauhh�uX   __dict__r�  }r�  (h}r�  h]r�  (h!X   mappingproxyr�  �r�  h$X	   dictproxyr�  �r�  eshh'uX   __suppress_context__r�  }r�  (h}r�  h]r�  h�ashh�uX   __eq__r�  }r�  (h}r�  (hX   Return self==value.r�  h
]r�  }r�  (hX   Return self==value.r�  h}r�  (hhh9h:u}r�  (hhWh9hXu�r�  uauhh�uX   __hash__r�  }r�  (h}r�  (hX   Return hash(self).r�  h
]r�  (}r�  (hX   Return hash(self).r�  h}r�  (hhh9h:u}r�  (hhWh9hXu�r�  u}r�  (h=]r�  h�ahA}r�  (hC]r�  h�ahEX   selfr�  u�r�  ueuhh�uh}r�  (h}r�  h]r�  (h�h�eshh�uX   __le__r�  }r�  (h}r�  (hX   Return self<=value.r�  h
]r�  }r�  (hX   Return self<=value.r�  h}r�  (hhh9h:u}r�  (hhWh9hXu�r�  uauhh�uX
   __format__r�  }r�  (h}r�  (hX   default object formatterr�  h
]r�  (}r�  (hX   default object formatterr�  h}r�  (hhh9h:u}r�  (hhWh9hXu�r�  u}r�  (h=]r�  h@ahA}r�  (hC]r�  h�ahEX   selfr�  u}r�  (hC]r�  h@ahEX
   formatSpecr�  u�r�  ueuhh�uX   __context__r�  }r�  (h}r�  (h]r�  h�ahX   exception contextr�  uhh�uX   __setstate__r�  }r�  (h}r�  (hX.   __setstate__(self: BaseException, state: dict)r�  h
]r�  }r�  (h=]r�  h&ahA}r�  (hC]r�  h�ahEh�u}r�  (hC]r�  j*  ahEX   stater�  u�r�  uauhh�uX   __weakref__r�  }r�  (h}r�  (h]r�  h�ahX2   list of weak references to the object (if defined)r�  uhh�uX   __setattr__r�  }r�  (h}r�  (hX%   Implement setattr(self, name, value).r�  h
]r�  (}r�  (hX%   Implement setattr(self, name, value).r�  h}r�  (hhh9h:u}r�  (hhWh9hXu�r�  u}r�  (h=]r�  h&ahA}r�  (hC]r�  h�ahEX   selfr�  u}r�  (hC]r�  h@ahEX   namer�  u}r�  (hC]r�  h�ahEX   valuer�  u�r�  ueuhh�uX   __lt__r�  }r�  (h}r�  (hX   Return self<value.r   h
]r  }r  (hX   Return self<value.r  h}r  (hhh9h:u}r  (hhWh9hXu�r  uauhh�uX   __subclasshook__r  }r  (h}r	  (hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r
  h
]r  }r  (hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r  h}r  (hhh9h:u}r  (hhWh9hXu�r  uauhhuX   __ne__r  }r  (h}r  (hX   Return self!=value.r  h
]r  }r  (hX   Return self!=value.r  h}r  (hhh9h:u}r  (hhWh9hXu�r  uauhh�uX	   __cause__r  }r  (h}r  (h]r  h�ahX   exception causer  uhh�uuhhX   basesr   ]r!  h!X	   Exceptionr"  �r#  aX   mror$  ]r%  (X   structr&  X   errorr'  �r(  j#  h!X   BaseExceptionr)  �r*  h�euhhuX   unpackr+  }r,  (h}r-  (hX�   unpack(fmt, buffer) -> (v1, v2, ...)

Return a tuple containing values unpacked according to the format string
fmt.  Requires len(buffer) == calcsize(fmt). See help(struct) for more
on format strings.r.  h
]r/  (}r0  (h]r1  hgahX�   (v1, v2, ...)

Return a tuple containing values unpacked according to the format string
fmt.  Requires len(buffer) == calcsize(fmt). See help(struct) for more
on format strings.r2  h}r3  hX   fmtr4  s}r5  hX   bufferr6  s�r7  u}r8  (h=]r9  hJahA}r:  (hC]r;  h@ahEX   fmtr<  u}r=  (hC]r>  h@ahEX   stringr?  u�r@  u}rA  (h=]rB  hJahA}rC  (hC]rD  h@ahEX   fmtrE  u}rF  (hC]rG  h{ahEX   bufferrH  u�rI  u}rJ  (h=]rK  hJahA}rL  (hC]rM  h@ahEX   fmtrN  u}rO  (hC]rP  h�ahEX   bufferrQ  u�rR  ueuhhuX   calcsizerS  }rT  (h}rU  (hX`   calcsize(fmt) -> integer

Return size in bytes of the struct described by the format string fmt.rV  h
]rW  (}rX  (h]rY  ju  ahXF   Return size in bytes of the struct described by the format string fmt.rZ  h}r[  hX   fmtr\  s�r]  u}r^  (h=]r_  h�ahA}r`  (hC]ra  h@ahEX   fmtrb  u�rc  ueuhhuX	   pack_intord  }re  (h}rf  (hX  pack_into(fmt, buffer, offset, v1, v2, ...)

Pack the values v1, v2, ... according to the format string fmt and write
the packed bytes into the writable buffer buf starting at offset.  Note
that the offset is a required argument.  See help(struct) for more
on format strings.rg  h
]rh  (}ri  (hX�   Pack the values v1, v2, ... according to the format string fmt and write
the packed bytes into the writable buffer buf starting at offset.  Note
that the offset is a required argument.  See help(struct) for more
on format strings.rj  h(}rk  hX   fmtrl  s}rm  hX   bufferrn  s}ro  hX   offsetrp  s}rq  hX   v1rr  s}rs  hX   v2rt  s}ru  (hhh9h:utrv  u}rw  (h=]rx  h&ahA(}ry  (hC]rz  h@ahEX   fmtr{  u}r|  (hC]r}  h{ahEX   bufferr~  u}r  (hC]r�  h�ahEX   offsetr�  u}r�  (hC]r�  hJahKh:hEX   argsr�  utr�  ueuhhuX   BuiltinImporterr�  }r�  (h}r�  (h}r�  (h�}r�  (h}r�  (hX   Return self>=value.r�  h
]r�  }r�  (hX   Return self>=value.r�  h}r�  (hhh9h:u}r�  (hhWh9hXu�r�  uauhh�uX   create_moduler�  }r�  (h}r�  (hX   Create a built-in moduler�  h
]r�  }r�  (hX   Create a built-in moduler�  h}r�  (hhh9h:u}r�  (hhWh9hXu�r�  uauhhuh�}r�  (h}r�  (hX   Return str(self).r�  h
]r�  }r�  (hX   Return str(self).r�  h}r�  (hhh9h:u}r�  (hhWh9hXu�r�  uauhh�uh�}r�  (h}r�  (hX   Return repr(self).r�  h
]r�  }r�  (hX   Return repr(self).r�  h}r�  (hhh9h:u}r�  (hhWh9hXu�r�  uauhh�uh�}r�  (h}r�  (hX>   Initialize self.  See help(type(self)) for accurate signature.r�  h
]r�  }r�  (hX>   Initialize self.  See help(type(self)) for accurate signature.r�  h}r�  (hhh9h:u}r�  (hhWh9hXu�r�  uauhh�uX
   get_sourcer�  }r�  (h}r�  (hX8   Return None as built-in modules do not have source code.r�  h
]r�  }r�  (hX8   Return None as built-in modules do not have source code.r�  h}r�  (hhh9h:u}r�  (hhWh9hXu�r�  uauhhuh\}r�  (h}r�  h]r�  h#ashh'uX   module_reprr�  }r�  (h}r�  (hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        r�  h
]r�  }r�  (hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        r�  h}r�  (hhh9h:u}r�  (hhWh9hXu�r�  uauhhuj  }r�  (h}r�  (hXG   Create and return a new object.  See help(type) for accurate signature.r�  h
]r�  }r�  (hXG   Create and return a new object.  See help(type) for accurate signature.r�  h}r�  (hhh9h:u}r�  (hhWh9hXu�r�  uauhhuj:  }r�  (h}r�  h]r�  h#ashh'uX   load_moduler�  }r�  (h}r�  (hX�   Load the specified module into sys.modules and return it.

    This method is deprecated.  Use loader.exec_module instead.

    r�  h
]r�  }r�  (hX�   Load the specified module into sys.modules and return it.

    This method is deprecated.  Use loader.exec_module instead.

    r�  h}r�  (hhh9h:u}r�  (hhWh9hXu�r�  uauhhujH  }r�  (h}r�  (hX.   __dir__() -> list
default dir() implementationr�  h
]r�  }r�  (h]r�  jP  ahX   default dir() implementationr�  h}r�  (h]r�  j  ahj  u�r�  uauhh�ujU  }r�  (h]r�  jY  ahjZ  uj[  }r�  (h}r�  (hX   helper for pickler�  h
]r�  }r�  (hX   helper for pickler�  h}r�  (hhh9h:u}r�  (hhWh9hXu�r�  uauhh�uj>  }r�  (h}r�  (hX   helper for pickler�  h
]r�  }r�  (hX   helper for pickler�  h}r   (hhh9h:u}r  (hhWh9hXu�r  uauhh�ujm  }r  (h}r  (hX6   __sizeof__() -> int
size of object in memory, in bytesr  h
]r  }r  (h]r  ju  ahX"   size of object in memory, in bytesr	  h}r
  (h]r  j  ahj  u�r  uauhh�uX   find_moduler  }r  (h}r  (hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        r  h
]r  }r  (hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        r  h}r  (hhh9h:u}r  (hhWh9hXu�r  uauhhuj�  }r  (h}r  (hX   Return self>value.r  h
]r  }r  (hX   Return self>value.r  h}r  (hhh9h:u}r  (hhWh9hXu�r  uauhh�uj�  }r   (h}r!  h]r"  j�  ashh'uX   exec_moduler#  }r$  (h}r%  (hX   Exec a built-in moduler&  h
]r'  }r(  (hX   Exec a built-in moduler)  h}r*  (hhh9h:u}r+  (hhWh9hXu�r,  uauhhuj�  }r-  (h}r.  (hX   Return self==value.r/  h
]r0  }r1  (hX   Return self==value.r2  h}r3  (hhh9h:u}r4  (hhWh9hXu�r5  uauhh�uj�  }r6  (h}r7  (hX   Return hash(self).r8  h
]r9  }r:  (hX   Return hash(self).r;  h}r<  (hhh9h:u}r=  (hhWh9hXu�r>  uauhh�uj�  }r?  (h}r@  (hX   Return self<=value.rA  h
]rB  }rC  (hX   Return self<=value.rD  h}rE  (hhh9h:u}rF  (hhWh9hXu�rG  uauhh�uj�  }rH  (h}rI  (hX   default object formatterrJ  h
]rK  }rL  (hX   default object formatterrM  h}rN  (hhh9h:u}rO  (hhWh9hXu�rP  uauhh�uj�  }rQ  (h}rR  (h]rS  h�ahX2   list of weak references to the object (if defined)rT  uhh�uX   get_coderU  }rV  (h}rW  (hX9   Return None as built-in modules do not have code objects.rX  h
]rY  }rZ  (hX9   Return None as built-in modules do not have code objects.r[  h}r\  (hhh9h:u}r]  (hhWh9hXu�r^  uauhhuX	   find_specr_  }r`  (h}ra  h]rb  h!X   methodrc  �rd  ashh'uj�  }re  (h}rf  (hX%   Implement setattr(self, name, value).rg  h
]rh  }ri  (hX%   Implement setattr(self, name, value).rj  h}rk  (hhh9h:u}rl  (hhWh9hXu�rm  uauhh�uj�  }rn  (h}ro  (hX   Return self<value.rp  h
]rq  }rr  (hX   Return self<value.rs  h}rt  (hhh9h:u}ru  (hhWh9hXu�rv  uauhh�uj  }rw  (h}rx  (hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
ry  h
]rz  }r{  (hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r|  h}r}  (hhh9h:u}r~  (hhWh9hXu�r  uauhhuX
   is_packager�  }r�  (h}r�  (hX4   Return False as built-in modules are never packages.r�  h
]r�  }r�  (hX4   Return False as built-in modules are never packages.r�  h}r�  (hhh9h:u}r�  (hhWh9hXu�r�  uauhhuh�}r�  (h}r�  (hX   Implement delattr(self, name).r�  h
]r�  }r�  (hX   Implement delattr(self, name).r�  h}r�  (hhh9h:u}r�  (hhWh9hXu�r�  uauhh�uj  }r�  (h}r�  (hX   Return self!=value.r�  h
]r�  }r�  (hX   Return self!=value.r�  h}r�  (hhh9h:u}r�  (hhWh9hXu�r�  uauhh�uuhX�   Meta path import for built-in modules.

    All methods are either class or static methods to avoid the need to
    instantiate the class.

    r�  X	   is_hiddenr�  �j   ]r�  h�aj$  ]r�  (X   _frozen_importlibr�  j�  �r�  h�euhhuX   Structr�  }r�  (h]r�  h!X   Structr�  �r�  ahjZ  uX   __spec__r�  }r�  (h}r�  h]r�  j�  X
   ModuleSpecr�  �r�  ashh'uX
   __loader__r�  }r�  (h]r�  j�  ahjZ  uuu.