from os import path
from datetime import datetime
from django.urls import reverse
from django.conf import settings
from django.core.cache import caches
from psixiSocial.models import mainUserProfile, THPSProfile, userProfilePrivacy
from psixiSocial.privacyIdentifier import checkPrivacy
from psixiSocial.translators import usersListText

def generateSmallThumbs(image):
    imageFullName = image.name
    newImagePath = '%spsixiSocial/CACHE/users/50x50/%s' % (settings.MEDIA_URL, imageFullName)
    fullNewImagePath = '%s/%s' % (settings.BASE_DIR, newImagePath)
    if path.exists(fullNewImagePath):
        return newImagePath
    dotIndex = imageFullName.rfind(".") + 1
    name = imageFullName[0 : dotIndex]
    frmat = imageFullName[dotIndex : len(imageFullName)]
    if frmat == "jpg":
        frmat = "jpeg"
    class thumbResize(ImageSpec):
        processors = [ResizeTiFill(50, 50)]
        format = frmat
        options = {'quality': 60}
    with open(image.url, 'r') as originalImage:
        newImg = thumbResize(source = originalImage)
        newImgGenerated = newImg.generate()
    with open(newImagePath, 'wb') as thumb:
        thumb.write(newImgGenerated.read())
    return newImagePath

def getThumbs(userInstances, requestUserId, privacyCache):
    result = []
    statusData = {}
    textDict = None
    iffyStatusText = None
    onlineStatusText = None
    addRequestText = None
    confirmText = None
    cancelText = None
    rejectText = None
    removeText = None
    iterableIds = set(userInstances.keys())
    thumbItemsToAdd = {}
    thumbsCache = caches['dialogThumbs']
    itemsDictMany = thumbsCache.get_many(iterableIds)
    itemsDictKeys = itemsDictMany.keys()
    itemsSurplusSet = iterableIds - itemsDictKeys
    itemsSurplusSet1 = set()
    for id in itemsDictKeys:
        myItem = itemsDictMany[id].get(requestUserId, None)
        if myItem is None:
            itemsSurplusSet1.add(id)
        elif myItem is False:
            iterableIds.remove(id)
        else:
            if id == requestUserId:
                myItem = itemsDictMany[id][id]['item']
                statusData.update({requestUserId: 'online'})
                textDict = usersListText(lang = 'ru').getDict()
                onlineStatusText = textDict['onlineStatus']
                result.append((myItem[0], '<noscript><p class = "online">{0}</p></noscript>'.format(onlineStatusText), myItem[1]))
                iterableIds.remove(id)
    idsToGetTHPSProfiles = itemsSurplusSet | itemsSurplusSet1
    if len(idsToGetTHPSProfiles) > 0:
        users = {id: instance for id, instance in userInstances.items() if id in idsToGetTHPSProfiles}
        thpsProfiles = THPSProfile.objects.filter(toUser__in = users.values())
        user = users.get(requestUserId, None)
        if user is not None:
            id = user.id
            profilePath = reverse("profile", kwargs = {'currentUser': user.profileUrl})
            firstName = user.first_name or None
            lastName = user.last_name or None
            if firstName and lastName:
                name = '{0} {1}'.format(firstName, lastName)
                smallName = None
            else:
                if firstName:
                    name = firstName
                    smallName = user.username
                else:
                    name = user.username
                    smallName = None
            avatar = user.avatarThumbMiddle or None
            if avatar is None:
                if smallName:
                    itemString = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(id, name, smallName, profilePath)
                else:
                    itemString = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(id, name, profilePath)
            else:
                if smallName:
                    itemString = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(id, profilePath, avatar.url, name, smallName)
                else:
                    itemString = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(id, profilePath, avatar.url, name)
            try:
                thpsProfile = thpsProfiles.get(toUser__exact = user)
            except:
                thpsProfile = None
            if thpsProfile is not None:
                thpsItem = {}
                nick = thpsProfile.nickName
                clan = thpsProfile.clan or None
                if clan is not None:
                    thpsItem.update({'nick': nick, 'clan': clan})
                else:
                    thpsItem.update({'nick': nick})
                    thpsAvatar = thpsProfile.avatarThumbMiddle or None
                    if thpsAvatar is not None:
                        thpsItem.update({'avatar': thpsProfile})
                item.update({'thps': thpsItem})
            if onlineStatusText is None:
                if textDict is None:
                    textDict = usersListText(lang = 'ru').getDict()
                onlineStatusText = textDict['onlineStatus']
            item = {'item': (itemString, '</div>'), 'showInOnlineList': True}
            itemsDict = itemsDictMany.get(requestUserId, None)
            if itemsDict is None:
                thumbItemsToAdd.update({requestUserId: {requestUserId: item}})
            else:
                itemsDict.update({requestUserId: item})
                thumbItemsToAdd.update({requestUserId: itemsDict})
            result.append((itemString, '<noscript><p><a class = "online">{0}</a></p></noscript>'.format(onlineStatusText), '</div>'))
            statusData.update({requestUserId: 'online'})
            iterableIds.remove(requestUserId)
            idsToGetTHPSProfiles.remove(requestUserId)
        friendsIdsCache = caches['friendsIdCache']
        friendsIds = friendsIdsCache.get(requestUserId, None)
        if friendsIds is None:
            friendsCache = caches['friendsCache']
            userFriends = friendsCache.get(requestUserId, None)
            if userFriends is None:
                userFriends = requestUser.friends.all()
                if userFriends.count():
                    friendsIdsQuerySet = userFriends.values("id")
                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                    friendsCache.set(requestUserId, userFriends)
                else:
                    friendsIds = set()
                    friendsCache.set(requestUserId, False)
            elif userFriends is False:
                friendsIds = set()
            else:
                if userFriends.count():
                    friendsIdsQuerySet = userFriends.values("id")
                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                else:
                    friendsIds = set()
            friendsIdsCache.set(requestUserId, friendsIds)
        friendShipStatusCache = caches['friendShipStatusCache']
        friendShipStatusMany = friendShipStatusCache.get_many(idsToGetTHPSProfiles)
        friendShipStatusManyKeys = set(friendShipStatusMany.keys())
        friendShipSurplus0 = idsToGetTHPSProfiles - friendShipStatusManyKeys
        friendShipSurplus1 = set(id for id in friendShipStatusManyKeys if requestUserId not in friendShipStatusMany[id])
        friendShipSurplus = friendShipSurplus0 | friendShipSurplus1 - friendsIds
        if len(friendShipSurplus) > 0:
            friendShipRequestsCache = caches['friendshipRequests']
            friendShipRequestsMany = friendShipRequestsCache.get_many(friendShipSurplus)
        friendShipStatusToAdd = {}
        privacyMany = privacyCache.get_many(iterableIds)
        privacyManyKeys = privacyMany.keys()
        privacySurplusSet = iterableIds - privacyManyKeys
        privacySurplusSet1 = set()
        for id in privacyManyKeys:
            privacyItem = privacyMany[id]
            privacyItemMy = privacyItem.get(requestUserId, None)
            if privacyItemMy is None or 'viewProfile' or 'viewMainInfo' or 'viewTHPSInfo' or 'viewAvatar' or 'viewOnlineStatus' not in privacyItemMy:
                privacySurplusSet1.add(id)
        idsToGetPrivacyModel = privacySurplusSet | privacySurplusSet1
        if len(idsToGetPrivacyModel) > 0:
            users = {id: instance for id, instance in userInstances.items() if id in idsToGetPrivacyModel}
            privacyInstances = userProfilePrivacy.objects.filter(user__in = users.values())
        privacyToAdd = {}
    onlineCache = caches['onlineUsers']
    onlineUsers = onlineCache.get('online', set())
    iffyCache = caches['iffyUsersOnlineStatus']
    iffyUsers = iffyCache.get('iffy', set())
    iffyMany = iffyCache.get_many(iterableIds)
    for id in iterableIds:
        user = None
        avatar = None
        itemsDict = itemsDictMany.get(id, None)
        if itemsDict is None:
            friendShipDict = friendShipStatusMany.get(id, None)
            if friendShipDict is None:
                if id in friendsIds:
                    friendShipStatus = True
                else:
                    requests = friendShipRequestsMany.get(requestUserId, set())
                    friendShipStatus = False
                    for request in requests:
                        if request[0] == id:
                            friendShipStatus = "request"
                            break
                    if friendShipStatus is False:
                        requests = friendShipRequestsMany.get(id, set())
                        for request in requests:
                            if request[0] == requestUserId:
                                friendShipStatus = "myRequest"
                                break
                friendShipStatusToAdd.update({id: {requestUserId: friendShipStatus}})
            else:
                friendShipStatus = friendShipDict.get(requestUserId, None)
                if friendShipStatus is None:
                    if id in friendsIds:
                        friendShipStatus = True
                    else:
                        requests = friendShipRequestsMany.get(requestUserId, set())
                        friendShipStatus = False
                        for request in requests:
                            if request[0] == id:
                                friendShipStatus = "request"
                                break
                        if friendShipStatus is False:
                            requests = friendShipRequestsMany.get(id, set())
                            for request in requests:
                                if request[0] == requestUserId:
                                    friendShipStatus = "myRequest"
                                    break
                    friendShipDict.update({requestUserId: friendShipStatus})
                    friendShipStatusToAdd.update({id: friendShipDict})
            privacyDict = privacyMany.get(id, None)
            if privacyDict is None:
                user = users[id]
                privacySettings = privacyInstances.get(user = user)
                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = friendShipStatus, isAuthenticated = True)
                viewProfile = privacyChecker.checkViewProfilePrivacy()
                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                viewAvatar = privacyChecker.checkAvatarPrivacy()
                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                privacyToAdd.update({id: {requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}}})
            else:
                userPrivacyDict = privacyDict.get(requestUserId, None)
                if userPrivacyDict is None:
                    user = users[id]
                    privacySettings = privacyInstances.get(user = user)
                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = friendShipStatus, isAuthenticated = True)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    privacyDict.update({requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                    privacyToAdd.update({id: privacyDict})
                else:
                    viewProfile = userPrivacyDict.get('viewProfile', None)
                    viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                    viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                    viewAvatar = userPrivacyDict.get('viewAvatar', None)
                    viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                    if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None:
                        user = users[id]
                        privacySettings = privacyInstances.get(user = user)
                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = friendShipStatus, isAuthenticated = True)
                        if viewProfile is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewProfile': viewProfile})
                        if viewMainInfo is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                        if viewTHPSInfo is None:
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                        if viewAvatar is None:
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            userPrivacyDict.update({'viewAvatar': viewAvatar})
                        if viewOnlineStatus is None:
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                        privacyToAdd.update({id: privacyDict})
            if viewProfile:
                if user is None:
                    user = users[id]
                profilePath = reverse("profile", kwargs = {'currentUser': user.profileUrl})
                if viewMainInfo:
                    firstName = user.first_name or None
                    lastName = user.last_name or None
                    if firstName and lastName:
                        name = '{0} {1}'.format(firstName, lastName)
                        smallName = user.username
                    else:
                        if firstName:
                            name = firstName
                            smallName = user.username
                        else:
                            name = user.username
                            smallName = None
                else:
                    name = user.username
                    smallName = None
                if viewAvatar:
                    avatar = user.avatarThumbMiddle or None
                    if avatar is not None:
                        if smallName:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(id, profilePath, avatar.url, name, smallName)
                        else:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(id, profilePath, avatar.url, name)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(id, name, smallName, profilePath)
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(id, name, profilePath)
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(id, name, smallName, profilePath)
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(id, name, profilePath)
                if viewTHPSInfo:
                    try:
                        thpsProfile = thpsProfiles.get(toUser__exact = user)
                    except:
                        thpsProfile = None
                    if thpsProfile is not None:
                        thpsItem = {}
                        nick = thpsProfile.nickName
                        clan = thpsProfile.clan or None
                        if clan is not None:
                            thpsItem.update({'nick': nick, 'clan': clan})
                        else:
                            thpsItem.update({'nick': nick})
                        thpsAvatar = thpsProfile.avatarThumbMiddle or None
                        if thpsAvatar is not None:
                            thpsItem.update({'avatar': thpsProfile})
                        item.update({'thps': thpsItem})
                if textDict is None:
                    textDict = usersListText(lang = 'ru').getDict()
                if friendShipStatus is True:
                    if removeText is None:
                        removeText = textDict['removeFromFriends']
                    itemString1 = '<div class = "actions"><form><div><p><a href = "{0}">{1}</a></p></div></form></div></div>'.format(reverse("removeFriend", kwargs = {'userId': id}), removeText)
                elif friendShipStatus is False:
                    if addRequestText is None:
                        addRequestText = textDict['addToFriends']
                    itemString1 = '<div class = "actions"><form><div><p><a href = "{0}">{1}</a></p></div></form></div></div>'.format(reverse("addFriendRequest", kwargs = {'userId': id}), addRequestText)
                elif friendShipStatus == "request":
                    if confirmText is None:
                        confirmText = textDict['confirm']
                    if rejectText is None:
                        rejectText = textDict['reject']
                    itemString1 = '<div class = "actions"><form><div><p><a href = "{0}">{1}</a></p></div><div><p><a href = "{2}">{3}</a></p></div></form></div></div>'.format(reverse("confirmFriend", kwargs = {'userId': id}), cancelText, reverse("rejectFriend", kwargs = {'userId': id}), rejectText)
                elif friendShipStatus == "myRequest":
                    if cancelText is None:
                        cancelText = textDict['cancel']
                    itemString1 = '<div class = "actions"><form><div><p><a href = "{0}">{1}</a></p></div></form></div></div>'.format(reverse("cancelAddFriendRequest", kwargs = {'userId': id}), cancelText)
                if viewOnlineStatus:
                    if id in iffyUsers:
                        iffyTime = iffyMany.get(id, None)
                        if isinstance(iffyTime, datetime):
                            statusData.update({id: {'iffy': str(iffyTime)}})
                            if iffyStatusText is None:
                                iffyStatusText = textDict['uknownStatus']
                            statusString = '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText)
                        else:
                            statusString = None
                    elif id in onlineUsers:
                        statusData.update({id: 'online'})
                        if onlineStatusText is None:
                            onlineStatusText = textDict['onlineStatus']
                        statusString = '<noscript><p class = "online">{0}</p></noscript>'.format(onlineStatusText)
                    else:
                        statusString = None
                else:
                    statusString = None
                if statusString is None:
                    itemTuple = (itemString0, itemString1)
                    item = {'item': itemTuple, 'showInOnlineList': viewOnlineStatus}
                    result.append(itemTuple)
                else:
                    item = {'item': (itemString0, itemString1), 'showInOnlineList': viewOnlineStatus}
                    result.append((itemString0, statusString, itemString1))
                thumbItemsToAdd.update({id: {requestUserId: item}})
            else:
                thumbItemsToAdd.update({id: {requestUserId: False}})
        else:
            item = itemsDict.get(requestUserId, None)
            if item is None:
                friendShipDict = friendShipStatusMany.get(id, None)
                if friendShipDict is None:
                    if id in friendsIds:
                        friendShipStatus = True
                    else:
                        requests = friendShipRequestsMany.get(requestUserId, set())
                        friendShipStatus = False
                        for request in requests:
                            if request[0] == id:
                                friendShipStatus = "request"
                                break
                        if friendShipStatus is False:
                            requests = friendShipRequestsMany.get(id, set())
                            for request in requests:
                                if request[0] == requestUserId:
                                    friendShipStatus = "myRequest"
                                    break
                    friendShipStatusToAdd.update({id: {requestUserId: friendShipStatus}})
                else:
                    friendShipStatus = friendShipDict.get(requestUserId, None)
                    if friendShipStatus is None:
                        if id in friendsIds:
                            friendShipStatus = True
                        else:
                            requests = friendShipRequestsMany.get(requestUserId, set())
                            friendShipStatus = False
                            for request in requests:
                                if request[0] == id:
                                    friendShipStatus = "request"
                                    break
                            if friendShipStatus is False:
                                requests = friendShipRequestsMany.get(id, set())
                                for request in requests:
                                    if request[0] == requestUserId:
                                        friendShipStatus = "myRequest"
                                        break
                        friendShipDict.update({requestUserId: friendShipStatus})
                        friendShipStatusToAdd.update({id: friendShipDict})
                privacyDict = privacyMany.get(id, None)
                if privacyDict is None:
                    user = users[id]
                    privacySettings = privacyInstances.get(user = user)
                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = friendShipStatus, isAuthenticated = True)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    privacyToAdd.update({id: {requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}}})
                else:
                    userPrivacyDict = privacyDict.get(requestUserId, None)
                    if userPrivacyDict is None:
                        user = users[id]
                        privacySettings = privacyInstances.get(user = user)
                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = friendShipStatus, isAuthenticated = True)
                        viewProfile = privacyChecker.checkViewProfilePrivacy()
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        privacyDict.update({requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                        privacyToAdd.update({id: privacyDict})
                    else:
                        viewProfile = userPrivacyDict.get('viewProfile', None)
                        viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                        viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                        viewAvatar = userPrivacyDict.get('viewAvatar', None)
                        viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                        if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None:
                            user = users[id]
                            privacySettings = privacyInstances.get(user = user)
                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = friendShipStatus, isAuthenticated = True)
                            if viewProfile is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewProfile': viewProfile})
                            if viewMainInfo is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                            if viewTHPSInfo is None:
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                            if viewAvatar is None:
                                viewAvatar = privacyChecker.checkAvatarPrivacy()
                                userPrivacyDict.update({'viewAvatar': viewAvatar})
                            if viewOnlineStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                            privacyToAdd.update({id: privacyDict})
                if viewProfile:
                    if user is None:
                        user = users[id]
                    profilePath = reverse("profile", kwargs = {'currentUser': user.profileUrl})
                    if viewMainInfo:
                        firstName = user.first_name or None
                        lastName = user.last_name or None
                        if firstName and lastName:
                            name = '{0} {1}'.format(firstName, lastName)
                            smallName = user.username
                        else:
                            if firstName:
                                name = firstName
                                smallName = user.username
                            else:
                                name = user.username
                                smallName = None
                    else:
                        name = user.username
                        smallName = None
                    if viewAvatar:
                        if avatar is not None:
                            if smallName:
                                itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(id, profilePath, avatar.url, name, smallName)
                            else:
                                itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(id, profilePath, avatar.url, name)
                        else:
                            if smallName:
                                itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(id, name, smallName, profilePath)
                            else:
                                itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(id, name, profilePath)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(id, name, smallName, profilePath)
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(id, name, profilePath)
                    if viewTHPSInfo:
                        try:
                            thpsProfile = thpsProfiles.get(toUser__exact = user)
                        except:
                            thpsProfile = None
                        if thpsProfile is not None:
                            thpsItem = {}
                            nick = thpsProfile.nickName
                            clan = thpsProfile.clan or None
                            if clan is not None:
                                thpsItem.update({'nick': nick, 'clan': clan})
                            else:
                                thpsItem.update({'nick': nick})
                            thpsAvatar = thpsProfile.avatarThumbMiddle or None
                            if thpsAvatar is not None:
                                thpsItem.update({'avatar': thpsProfile})
                            item.update({'thps': thpsItem})
                    if textDict is None:
                        textDict = usersListText(lang = 'ru').getDict()
                    if friendShipStatus is True:
                        if removeText is None:
                            removeText = textDict['removeFromFriends']
                        itemString1 = '<div class = "actions"><form><div><p><a href = "{0}">{1}</a></p></div></form></div></div>'.format(reverse("removeFriend", kwargs = {'userId': id}), removeText)
                    elif friendShipStatus is False:
                        if addRequestText is None:
                            addRequestText = textDict['addToFriends']
                        itemString1 = '<div class = "actions"><form><div><p><a href = "{0}">{1}</a></p></div></form></div></div>'.format(reverse("addFriendRequest", kwargs = {'userId': id}), addRequestText)
                    elif friendShipStatus == "request":
                        if confirmText is None:
                            confirmText = textDict['confirm']
                        if rejectText is None:
                            rejectText = textDict['reject']
                        itemString1 = '<div class = "actions"><form><div><p><a href = "{0}">{1}</a></p></div><div><p><a href = "{2}">{3}</a></p></div></form></div></div>'.format(reverse("confirmFriend", kwargs = {'userId': id}), cancelText, reverse("rejectFriend", kwargs = {'userId': id}), rejectText)
                    elif friendShipStatus == "myRequest":
                        if cancelText is None:
                            cancelText = textDict['cancel']
                        itemString1 = '<div class = "actions"><form><div><p><a href = "{0}">{1}</a></p></div></form></div></div>'.format(reverse("cancelAddFriendRequest", kwargs = {'userId': id}), cancelText)
                    if viewOnlineStatus:
                        if id in iffyUsers:
                            iffyTime = iffyMany.get(id, None)
                            if isinstance(iffyTime, datetime):
                                statusData.update({id: {'iffy': str(iffyTime)}})
                                if iffyStatusText is None:
                                    iffyStatusText = textDict['uknownStatus']
                                statusString = '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText)
                            else:
                                statusString = None
                        elif id in onlineUsers:
                            statusData.update({id: 'online'})
                            if onlineStatusText is None:
                                onlineStatusText = textDict['onlineStatus']
                            statusString = '<noscript><p class = "online">{0}</p></noscript>'.format(onlineStatusText)
                        else:
                            statusString = None
                    else:
                        statusString = None
                    if statusString is None:
                        itemTuple = (itemString0, itemString1)
                        item = {'item': itemTuple, 'showInOnlineList': viewOnlineStatus}
                        result.append(itemTuple)
                    else:
                        item = {'item': (itemString0, itemString1), 'showInOnlineList': viewOnlineStatus}
                        result.append((itemString0, statusString, itemString1))
                    itemsDict.update({requestUserId: item})
                    thumbItemsToAdd.update({id: itemsDict})
                else:
                    itemsDict.update({requestUserId: False})
                    thumbItemsToAdd.update({id: itemsDict})
            else:
                if item is not False:
                    if item.pop('showInOnlineList'):
                        if id in iffyUsers:
                            iffyTime = iffyMany.get(id, None)
                            if isinstance(iffyTime, datetime):
                                statusData.update({id: {'iffy': str(iffyTime)}})
                                if iffyStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    iffyStatusText = textDict['uknownStatus']
                                statusString = '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText)
                            else:
                                statusString = None
                        elif id in onlineUsers:
                            statusData.update({id: 'online'})
                            if onlineStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                onlineStatusText = textDict['onlineStatus']
                            statusString = '<noscript><p class = "online">{0}</p></noscript>'.format(onlineStatusText)
                        else:
                            statusString = None
                    else:
                        statusString = None
                    if statusString is None:
                        result.append(item['item'])
                    else:
                        item = item['item']
                        result.append((item[0], statusString, item[1]))
    if len(thumbItemsToAdd) > 0:
        thumbsCache.set_many(thumbItemsToAdd)
        if len(privacyToAdd) > 0:
            privacyCache.set_many(privacyToAdd)
            if len(friendShipStatusToAdd) > 0:
                friendShipStatusCache.set_many(friendShipStatusToAdd)
    return statusData, result