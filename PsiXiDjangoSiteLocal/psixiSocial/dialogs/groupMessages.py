import json, base64
from functools import lru_cache
from collections import OrderedDict
from copy import copy
from datetime import datetime
from dateutil import tz
from django.conf import settings
from psixiSocial.login import login_decorator
from django.shortcuts import render
from django.core.paginator import Paginator
from django.core.cache import caches
from django.core.files.base import ContentFile
from django.core.validators import FileExtensionValidator, validate_image_file_extension, ValidationError
from django.http import JsonResponse
from django.template.loader import render_to_string as rts
from django.urls import reverse
from base.helpers import checkCSRF
from django.db.transaction import atomic, Error
from psixiSocial.models import dialogMessage, groupDialogAttributes, groupMessage, imagesAttachmentMessageGroup
from psixiSocial.forms import sendMessageForm
from psixiSocial.dialogs.dialogs import getMainDialogThumbs, getMainDialogThumbsXHR, getSmallDialogThumbsSimple
from psixiSocial.dialogs.groupDialogThumbs import getMainAndSmallThumbs
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

class cachedFragments():
    def __init__(self, *args, **kwargs):
        pass

    @property
    @lru_cache(maxsize = 16)
    def menu(self):
        menu = OrderedDict({
            0: {},
            1: {},
            2: {}
            })
        return menu

    @property
    @lru_cache(maxsize = 16)
    def menuSerialized(self):
        menu = self.menu
        if isinstance(menu, OrderedDict):
            return json.dumps(menu)
        else:
            return json.dumps(menu())

    def menuGetter(self, methodName):
        methodOrCached = getattr(self, methodName)
        if isinstance(methodOrCached, OrderedDict):
            return methodOrCached
        elif isinstance(methodOrCached, str):
            return methodOrCached
        else:
            return methodOrCached()

@login_decorator
def dialogGroup(request, dialogSlug):
    method = request.method
    if method == "GET":
        dialogsCache = caches['dialogGroup']
        cachedDialog = dialogsCache.get(dialogSlug, None)
        if cachedDialog is None:
            try:
                dialogAttributes = groupDialogAttributes.objects.get(slug__exact = dialogSlug)
            except groupDialogAttributes.DoesNotExist:
                dialogAttributes = None
            if dialogAttributes is not None:
                dialog = dialogAttributes.dialog
                dialogUsers = dialog.users.all()
                dialogUsersIdsQuerySet = dialogUsers.values("id")
                dialogUsersIds = {str(user.get("id")) for user in dialogUsersIdsQuerySet}
                dialogsCache.set(dialogSlug, {'attributes': dialogAttributes, 'dialog': dialog, 'users': dialogUsers, 'usersIds': dialogUsersIds})
        else:
            dialogUsers = cachedDialog.get('users', None)
            dialogUsersIds = cachedDialog.get('usersIds', None)
            dialog = cachedDialog.get('dialog', None)
            dialogAttributes = cachedDialog.get('attributes', None)
            if dialogUsers is None or dialogUsersIds is None or dialog is None or dialogAttributes is None:
                if dialogAttributes is None:
                    try:
                        dialogAttributes = groupDialogAttributes.objects.get(slug__exact = dialogSlug)
                    except groupDialogAttributes.DoesNotExist:
                        dialogAttributes = None
                    if dialogAttributes is not None:
                        cachedDialog.update({'attributes': dialogAttributes})
                        if dialog is None:
                            dialog = dialogAttributes.dialog
                            cachedDialog.update({'dialog': dialog})
                        if dialogUsers is None:
                            dialogUsers = dialog.users.all()
                            dialogUsersIdsQuerySet = dialogUsers.values("id")
                            dialogUsersIds = {str(user.get("id")) for user in dialogUsersIdsQuerySet}
                            cachedDialog.update({'users': dialogUsers, 'usersIds': dialogUsersIds})
                        else:
                            if dialogUsersIds is None:
                                dialogUsersIdsQuerySet = dialogUsers.values("id")
                                dialogUsersIds = {str(user.get("id")) for user in dialogUsersIdsQuerySet}
                                cachedDialog.update({'usersIds': dialogUsersIds})
                        dialogsCache.set(dialogSlug, cachedDialog)
        if dialogAttributes is None:
            pass
        requestUser = request.user
        myId = str(requestUser.id)
        reqGet = request.GET
        if 'list' in reqGet:
            page = int(reqGet.get('list', 0))
            if page > 0:
                dialogId = str(dialog.id)
                messages, unreadByUsersDict = loadDialogSimpleShort(myId, dialogId, page)
                if messages is not None:
                    return JsonResponse({"messagesItems": {"messages": messages, "unread": unreadByUsersDict}})
                return JsonResponse({"messagesItems": None})
        page = 0
        if myId in dialogUsersIds:
            dialogId = str(dialog.id)
            if request.is_ajax():
                dialogAvatar = dialogAttributes.dialogsListThumb
                if 'from-dialog' in reqGet:

                    messagesFilesStorage = caches['filesStorageMessages']
                    cachedFilesDict = messagesFilesStorage.get(dialogId, None)
                    if cachedFilesDict is not None:
                        cachedFilesDictDict = cachedFilesDict.get(myId, None)
                        if cachedFilesDictDict is not None:
                            cachedFilesDictDictImages = cachedFilesDictDict.get("images", None)
                        else:
                            cachedFilesDictDictImages = None
                    else:
                        cachedFilesDictDictImages = None
                    if 'get-messages' in reqGet:
                        messages, unreadByUsersDict, usersIdsToGetNamesSet = loadDialogSimple(myId, dialogId, page)
                        mainThumbsDict, smallThumbsDict, namesDictList, onlineStatusDict, activeStatusDict = getMainAndSmallThumbs((dialogUsersIds | usersIdsToGetNamesSet), requestUser, myId)
                        if dialogAvatar:
                            return JsonResponse({"newDialog": {
                                    'dialogGroupLink': reverse('dialogMessagesFileLoaderGroup', kwargs = {'dialogSlugName': dialogAttributes.slug, 'type': 'image'}),
                                    'messages': messages,
                                    'onlineStatusDict': onlineStatusDict,
                                    'activeStatusDict': activeStatusDict,
                                    'dialogId': dialogId,
                                    'dialogUsersIds': tuple(dialogUsersIds),
                                    'unreadMessagesDictList': unreadByUsersDict,
                                    'mainThumbsDict': mainThumbsDict,
                                    'smallThumbsDict': smallThumbsDict,
                                    'avatar': dialogAvatar.url,
                                    'canInvite': dialogAttributes.membersCanInvite,
                                    'admins': dialogAttributes.owners_id,
                                    'imagesToSend': cachedFilesDictDictImages
                                    }
                                })
                        return JsonResponse({"newDialog": {
                                'dialogGroupLink': reverse('dialogMessagesFileLoaderGroup', kwargs = {'dialogSlugName': dialogAttributes.slug, 'type': 'image'}),
                                'dialogSlug': dialogAttributes.slug,
                                'messages': messages,
                                'onlineStatusDict': onlineStatusDict,
                                'activeStatusDict': activeStatusDict,
                                'dialogId': dialogId,
                                'dialogUsersIds': tuple(dialogUsersIds),
                                'unreadMessagesDictList': unreadByUsersDict,
                                'mainThumbsDict': mainThumbsDict,
                                'smallThumbsDict': smallThumbsDict,
                                'canInvite': dialogAttributes.membersCanInvite,
                                'admins': dialogAttributes.owners_id,
                                'imagesToSend': cachedFilesDictDictImages
                                }
                            })
                    mainThumbsDict, smallThumbsDict, namesDictList, onlineStatusDict, activeStatusDict = getMainAndSmallThumbs(dialogUsersIds, requestUser, myId)
                    if dialogAvatar:
                        return JsonResponse({"newDialog": {
                                'dialogGroupLink': reverse('dialogMessagesFileLoaderGroup', kwargs = {'dialogSlugName': dialogAttributes.slug, 'type': 'image'}),
                                'onlineStatusDict': onlineStatusDict,
                                'activeStatusDict': activeStatusDict,
                                'dialogId': dialogId,
                                'dialogUsersIds': tuple(dialogUsersIds),
                                'mainThumbsDict': mainThumbsDict,
                                'smallThumbsDict': smallThumbsDict,
                                'avatar': dialogAvatar.url,
                                'canInvite': dialogAttributes.membersCanInvite,
                                'admins': dialogAttributes.owners_id,
                                'imagesToSend': cachedFilesDictDictImages
                                }
                            })
                    return JsonResponse({"newDialog": {
                            'dialogGroupLink': reverse('dialogMessagesFileLoaderGroup', kwargs = {'dialogSlugName': dialogAttributes.slug, 'type': 'image'}),
                            'dialogSlug': dialogAttributes.slug,
                            'onlineStatusDict': onlineStatusDict,
                            'activeStatusDict': activeStatusDict,
                            'dialogId': dialogId,
                            'dialogUsersIds': tuple(dialogUsersIds),
                            'mainThumbsDict': mainThumbsDict,
                            'smallThumbsDict': smallThumbsDict,
                            'canInvite': dialogAttributes.membersCanInvite,
                            'admins': dialogAttributes.owners_id,
                            'imagesToSend': cachedFilesDictDictImages
                            }
                        })
                messagesFilesStorage = caches['filesStorageMessages']
                cachedFilesDict = messagesFilesStorage.get(dialogId, None)
                if cachedFilesDict is not None:
                    cachedFilesDictDict = cachedFilesDict.get(myId, None)
                    if cachedFilesDictDict is not None:
                        cachedFilesDictDictImages = cachedFilesDictDict.get("images", None)
                    else:
                        cachedFilesDictDictImages = None
                else:
                    cachedFilesDictDictImages = None
                messages, unreadByUsersDict, usersIdsToGetNamesSet = loadDialogSimple(myId, dialogId, page)
                mainThumbsDict, smallThumbsDict, namesDictList, onlineStatusDict, activeStatusDict = getMainAndSmallThumbs((dialogUsersIds | usersIdsToGetNamesSet), requestUser, myId)
                if 'global' in reqGet:
                    if dialogAvatar:
                        return JsonResponse({"newPage": json.dumps({
                            'newTemplate': rts('psixiSocial/psixiSocial.html', {
                                'parentTemplate': 'base/classicPage/emptyParent.html',
                                'content': ('psixiSocial', 'dialog', 'group', True),
                                'menu': cachedFragments().menuGetter('menuSerialized'),
                                'mainThumbsDict': mainThumbsDict,
                                'smallThumbsDict': smallThumbsDict,
                                'onlineStatusDict': onlineStatusDict,
                                'activeStatusDict': activeStatusDict,
                                'requestUserId': myId,
                                'messagesListDict': messages,
                                'unreadUsersIdsDictList': unreadByUsersDict,
                                'unreadUsersNamesDictList': namesDictList,
                                'csrf': checkCSRF(request, cookies),
                                'time': datetime.now(tz = tz.gettz()).microsecond,
                                'form': None,
                                'accessToSend': True,
                                'sendersIds': list(dialogUsersIds),
                                'dialogId': dialogId,
                                'groupDialogAvatar': dialogAvatar.url,
                                'canInvite': dialogAttributes.membersCanInvite,
                                'admins': dialogAttributes.owners_id,
                                'dialogSlug': dialogAttributes.slug,
                                'imagesToSend': cachedFilesDictDictImages
                            })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        'newTemplate': rts('psixiSocial/psixiSocial.html', {
                            'parentTemplate': 'base/classicPage/emptyParent.html',
                            'content': ('psixiSocial', 'dialog', 'group', True),
                            'menu': cachedFragments().menuGetter('menuSerialized'),
                            'mainThumbsDict': mainThumbsDict,
                            'smallThumbsDict': smallThumbsDict,
                            'onlineStatusDict': onlineStatusDict,
                            'activeStatusDict': activeStatusDict,
                            'requestUserId': myId,
                            'messagesListDict': messages,
                            'unreadUsersIdsDictList': unreadByUsersDict,
                            'unreadUsersNamesDictList': namesDictList,
                            'csrf': checkCSRF(request, cookies),
                            'time': datetime.now(tz = tz.gettz()).microsecond,
                            'form': None,
                            'accessToSend': True,
                            'sendersIds': list(dialogUsersIds),
                            'dialogId': dialogId,
                            'groupDialogAvatar': None,
                            'canInvite': dialogAttributes.membersCanInvite,
                            'admins': dialogAttributes.owners_id,
                            'dialogSlug': dialogAttributes.slug,
                            'imagesToSend': cachedFilesDictDictImages
                        })
                        })})
                if dialogAvatar:
                    return JsonResponse({"newPage": json.dumps({
                        'templateHead': rts('psixiSocial/head/dialogs/dialogWrite.html'),
                        'templateBody': rts('psixiSocial/body/dialogs/dialogWriteGroupXHR.html', {
                            'requestUserId': myId,
                            'mainThumbsDict': mainThumbsDict,
                            'smallThumbsDict': smallThumbsDict,
                            'form': None,
                            'accessToSend': True,
                            'groupDialogAvatar': dialogAvatar.url,
                            'canInvite': dialogAttributes.membersCanInvite,
                            'admins': dialogAttributes.owners_id,
                            'imagesToSend': cachedFilesDictDictImages
                            }),
                        'templateScripts': rts('psixiSocial/scripts/dialogs/dialogWriteGroupXHR.html', {
                            'onlineStatusDict': onlineStatusDict,
                            'activeStatusDict': activeStatusDict,
                            'smallThumbsDict': smallThumbsDict,
                            'messagesListDict': messages,
                            'requestUserId': myId,
                            'csrf': checkCSRF(request, cookies),
                            'sendersIds': list(dialogUsersIds),
                            'dialogId': dialogId,
                            'time': datetime.now(tz = tz.gettz()).microsecond,
                            'unreadUsersIdsDictList': unreadUsers,
                            'unreadUsersNamesDictList': namesDictList,
                            'dialogSlug': dialogAttributes.slug
                            }),
                        'content': ('psixiSocial', 'dialog', 'group'),
                        'menu': cachedFragments().menuGetter('menu'),
                        })})
                return JsonResponse({"newPage": json.dumps({
                    'templateHead': rts('psixiSocial/head/dialogs/dialogWrite.html'),
                    'templateBody': rts('psixiSocial/body/dialogs/dialogWriteGroupXHR.html', {
                        'mainThumbsDict': mainThumbsDict,
                        'smallThumbsDict': smallThumbsDict,
                        'form': None,
                        'accessToSend': True
                        }),
                    'templateScripts': rts('psixiSocial/scripts/dialogs/dialogWriteGroupXHR.html', {
                        'onlineStatusDict': onlineStatusDict,
                        'activeStatusDict': activeStatusDict,
                        'smallThumbsDict': smallThumbsDict,
                        'messagesListDict': messages,
                        'requestUserId': myId,
                        'csrf': checkCSRF(request, cookies),
                        'sendersIds': list(dialogUsersIds),
                        'dialogId': dialogId,
                        'time': datetime.now(tz = tz.gettz()).microsecond,
                        'unreadUsersIdsDictList': unreadUsers,
                        'unreadUsersNamesDictList': namesDictList,
                        'dialogSlug': dialogAttributes.slug,
                        'imagesToSend': cachedFilesDictDictImages
                        }),
                    'content': ('psixiSocial', 'dialog', 'group'),
                    'menu': cachedFragments().menuGetter('menu'),
                    })})
            messagesFilesStorage = caches['filesStorageMessages']
            cachedFilesDict = messagesFilesStorage.get(dialogId, None)
            if cachedFilesDict is not None:
                cachedFilesDictDict = cachedFilesDict.get(myId, None)
                if cachedFilesDictDict is not None:
                    cachedFilesDictDictImages = cachedFilesDictDict.get("images", None)
                else:
                    cachedFilesDictDictImages = None
            else:
                cachedFilesDictDictImages = None
            cookies = request.COOKIES
            if cookies.get('js', None) is not None:
                messages, unreadByUsersDict, usersIdsToGetNamesSet = loadDialogSimple(myId, dialogId, page)
                if len(usersIdsToGetNamesSet) > 0:
                    mainThumbsDict, smallThumbsDict, namesDictList, onlineStatusDict, activeStatusDict = getMainAndSmallThumbs((dialogUsersIds | usersIdsToGetNamesSet), requestUser, myId)
                    return render(request, "psixiSocial/psixiSocial.html", {
                        'parentTemplate': 'base/classicPage/base.html',
                        'content': ('psixiSocial', 'dialog', 'group', True),
                        'menu': cachedFragments().menuGetter('menuSerialized'),
                        'mainThumbsDict': mainThumbsDict,
                        'smallThumbsDict': smallThumbsDict,
                        'onlineStatusDict': onlineStatusDict,
                        'activeStatusDict': activeStatusDict,
                        'requestUserId': myId,
                        'messagesListDict': messages,
                        'unreadUsersIdsDictList': unreadByUsersDict,
                        'unreadUsersNamesDictList': namesDictList,
                        'csrf': checkCSRF(request, cookies),
                        'form': None,
                        'accessToSend': True,
                        'sendersIds': list(dialogUsersIds),
                        'dialogId': dialogId,
                        'time': datetime.now(tz = tz.gettz()).microsecond,
                        'dialogSlug': dialogAttributes.slug,
                        'imagesToSend': cachedFilesDictDictImages
                        })
                mainThumbsDict, smallThumbsDict, namesDictList, onlineStatusDict, activeStatusDict = getMainAndSmallThumbs(dialogUsersIds, requestUser, myId)
                return render(request, "psixiSocial/psixiSocial.html", {
                    'parentTemplate': 'base/classicPage/base.html',
                    'content': ('psixiSocial', 'dialog', 'group', True),
                    'menu': cachedFragments().menuGetter('menuSerialized'),
                    'mainThumbsDict': mainThumbsDict,
                    'smallThumbsDict': smallThumbsDict,
                    'onlineStatusDict': onlineStatusDict,
                    'activeStatusDict': activeStatusDict,
                    'requestUserId': myId,
                    'messagesListDict': messages,
                    'unreadUsersIdsDictList': unreadByUsersDict,
                    'unreadUsersNamesDictList': namesDictList,
                    'csrf': checkCSRF(request, cookies),
                    'form': None,
                    'accessToSend': True,
                    'sendersIds': list(dialogUsersIds),
                    'dialogId': dialogId,
                    'time': datetime.now(tz = tz.gettz()).microsecond,
                    'dialogSlug': dialogAttributes.slug,
                    'imagesToSend': cachedFilesDictDictImages
                    })
            messages, images, unreadByUsersDict, usersIdsToGetNamesSet = loadDialog(myId, dialogId, page)
            mainThumbsDict, smallThumbsDict, namesDictList, onlineStatusDict, activeStatusDict = getMainAndSmallThumbs((dialogUsersIds | usersIdsToGetNamesSet), requestUser, myId)
            return render(request, "psixiSocial/psixiSocial.html", {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'dialog', 'group', False),
                'menu': cachedFragments().menuGetter('menuSerialized'),
                'mainThumbsDict': mainThumbsDict,
                'smallThumbsDict': smallThumbsDict,
                'onlineStatusDict': onlineStatusDict,
                'activeStatusDict': activeStatusDict,
                'requestUserId': myId,
                'messagesListDict': None,
                'messagesQuerySet': messages,
                'imagesQuerySet': images,
                'unreadUsersIdsDictList': unreadByUsersDict,
                'unreadUsersNamesDictList': namesDictList,
                'csrf': checkCSRF(request, cookies),
                'form': None,
                'accessToSend': True,
                'sendersIds': list(dialogUsersIds),
                'dialogId': dialogId,
                'time': datetime.now(tz = tz.gettz()).microsecond,
                'dialogSlug': dialogAttributes.slug,
                'imagesToSend': cachedFilesDictDictImages
                })
        # Страница без формы отправки
    elif method == "POST":
        dialogsCache = caches['dialogGroup']
        cachedDialog = dialogsCache.get(dialogSlug, None)
        if cachedDialog is None:
            try:
                dialogAttributes = groupDialogAttributes.objects.get(slug__exact = dialogSlug)
            except groupDialogAttributes.DoesNotExist:
                dialogAttributes = None
            if dialogAttributes is not None:
                dialog = dialogAttributes.dialog
                dialogUsers = dialog.users.all()
                dialogUsersIdsQuerySet = dialogUsers.values("id")
                dialogUsersIds = {str(user.get("id")) for user in dialogUsersIdsQuerySet}
                dialogsCache.set(dialogSlug, {'attributes': dialogAttributes, 'dialog': dialog, 'users': dialogUsers, 'usersIds': dialogUsersIds})
        else:
            dialogUsers = cachedDialog.get('users', None)
            dialogUsersIds = cachedDialog.get('usersIds', None)
            dialog = cachedDialog.get('dialog', None)
            dialogAttributes = cachedDialog.get('attributes', None)
            if dialogUsers is None or dialogUsersIds is None or dialog is None or dialogAttributes is None:
                if dialogAttributes is None:
                    try:
                        dialogAttributes = groupDialogAttributes.objects.get(slug__exact = dialogSlug)
                    except groupDialogAttributes.DoesNotExist:
                        dialogAttributes = None
                    if dialogAttributes is not None:
                        cachedDialog.update({'attributes': dialogAttributes})
                        if dialog is None:
                            dialog = dialogAttributes.dialog
                            cachedDialog.update({'dialog': dialog})
                        if dialogUsers is None:
                            dialogUsers = dialog.users.all()
                            dialogUsersIdsQuerySet = dialogUsers.values("id")
                            dialogUsersIds = {str(user.get("id")) for user in dialogUsersIdsQuerySet}
                            cachedDialog.update({'users': dialogUsers, 'usersIds': dialogUsersIds})
                        else:
                            if dialogUsersIds is None:
                                dialogUsersIdsQuerySet = dialogUsers.values("id")
                                dialogUsersIds = {str(user.get("id")) for user in dialogUsersIdsQuerySet}
                                cachedDialog.update({'usersIds': dialogUsersIds})
                        dialogsCache.set(dialogSlug, cachedDialog)
        if dialogAttributes is None:
            pass
        requestUser = request.user
        myId = str(requestUser.id)
        if myId in dialogUsersIds:
            dialogId = str(dialog.id)
            if request.is_ajax():
                messageTextBody = request.POST.get('body', None)
                messageTextBodyLen = len(messageTextBody)
                if messageTextBody is not None and messageTextBodyLen <= 1000:
                    lastMessageCache = caches['lastMessage']
                    lastMessage = lastMessageCache.get(dialogId, None)
                    save = True
                    messagesFilesStorage = caches['filesStorageMessages']
                    cachedFilesDict = messagesFilesStorage.get(dialogId, None)
                    myProfileUrl = requestUser.profileUrl
                    currTime = datetime.now(tz = tz.gettz())
                    if cachedFilesDict is not None:
                        cachedFilesDictDict = cachedFilesDict.get(myId, None)
                        if cachedFilesDictDict is not None:
                            cachedFilesDictDictImages = cachedFilesDictDict.get("images", None)
                            if cachedFilesDictDictImages is not None:
                                try:
                                    with atomic():
                                        if lastMessage is None:
                                            lastMessage = groupMessage.objects.filter(inDialog__exact = dialog)
                                            if lastMessage.count() > 0:
                                                lastMessage = lastMessage.last()
                                                if str(lastMessage.by_id) == myId:
                                                    newMessage = groupMessage.objects.create(by = requestUser, body = messageTextBody, messageHead = False, time = currTime, inDialog = dialog)
                                                else:
                                                    newMessage = groupMessage.objects.create(by = requestUser, body = messageTextBody, messageHead = True, time = currTime, inDialog = dialog)
                                            else:
                                                newMessage = groupMessage.objects.create(by = requestUser, body = messageTextBody, messageHead = True, time = currTime, inDialog = dialog)
                                        else:
                                            if str(lastMessage.by_id) == myId:
                                                newMessage = groupMessage.objects.create(by = requestUser, body = messageTextBody, messageHead = False, time = currTime, inDialog = dialog)
                                            else:
                                                newMessage = groupMessage.objects.create(by = requestUser, body = messageTextBody, messageHead = True, time = currTime, inDialog = dialog)
                                        newImagesAttachment = imagesAttachmentMessageGroup(message = newMessage, by = requestUser, itemsLength = 0)
                                        cachedFilesDictDictImagesList = tuple(cachedFilesDictDictImages.values())
                                        cachedFilesDictDictImagesLength = len(cachedFilesDictDictImagesList)
                                        if cachedFilesDictDictImagesLength <= 3:
                                            if cachedFilesDictDictImagesLength == 1:
                                                imageItemTupleItem1 = cachedFilesDictDictImagesList[0]
                                                file1 = ContentFile(base64.b64decode(imageItemTupleItem1[0]))
                                                file1.name = '{0}.{1}'.format(imageItemTupleItem1[1], imageItemTupleItem1[2])
                                                newImagesAttachment.image1 = file1
                                            elif cachedFilesDictDictImagesLength == 2:
                                                imageItemTupleItem1 = cachedFilesDictDictImagesList[0]
                                                file1 = ContentFile(base64.b64decode(imageItemTupleItem1[0]))
                                                file1.name = '{0}.{1}'.format(imageItemTupleItem1[1], imageItemTupleItem1[2])
                                                newImagesAttachment.image1 = file1
                                                imageItemTupleItem2 = cachedFilesDictDictImagesList[1]
                                                file2 = ContentFile(base64.b64decode(imageItemTupleItem2[0]))
                                                file2.name = '{0}.{1}'.format(imageItemTupleItem2[1], imageItemTupleItem2[2])
                                                newImagesAttachment.image2 = file2
                                            else:
                                                imageItemTupleItem1 = cachedFilesDictDictImagesList[0]
                                                file1 = ContentFile(base64.b64decode(imageItemTupleItem1[0]))
                                                file1.name = '{0}.{1}'.format(imageItemTupleItem1[1], imageItemTupleItem1[2])
                                                newImagesAttachment.image1 = file1
                                                imageItemTupleItem2 = cachedFilesDictDictImagesList[1]
                                                file2 = ContentFile(base64.b64decode(imageItemTupleItem2[0]))
                                                file2.name = '{0}.{1}'.format(imageItemTupleItem2[1], imageItemTupleItem2[2])
                                                newImagesAttachment.image2 = file2
                                                imageItemTupleItem3 = cachedFilesDictDictImagesList[2]
                                                file3 = ContentFile(base64.b64decode(imageItemTupleItem3[0]))
                                                file3.name = '{0}.{1}'.format(imageItemTupleItem3[1], imageItemTupleItem3[2])
                                                newImagesAttachment.image3 = file3
                                        else:
                                            if cachedFilesDictDictImagesLength == 4:
                                                imageItemTupleItem1 = cachedFilesDictDictImagesList[0]
                                                file1 = ContentFile(base64.b64decode(imageItemTupleItem1[0]))
                                                file1.name = '{0}.{1}'.format(imageItemTupleItem1[1], imageItemTupleItem1[2])
                                                newImagesAttachment.image1 = file1
                                                imageItemTupleItem2 = cachedFilesDictDictImagesList[1]
                                                file2 = ContentFile(base64.b64decode(imageItemTupleItem2[0]))
                                                file2.name = '{0}.{1}'.format(imageItemTupleItem2[1], imageItemTupleItem2[2])
                                                newImagesAttachment.image2 = file2
                                                imageItemTupleItem3 = cachedFilesDictDictImagesList[2]
                                                file3 = ContentFile(base64.b64decode(imageItemTupleItem3[0]))
                                                file3.name = '{0}.{1}'.format(imageItemTupleItem3[1], imageItemTupleItem3[2])
                                                newImagesAttachment.image3 = file3
                                                imageItemTupleItem4 = cachedFilesDictDictImagesList[3]
                                                file4 = ContentFile(base64.b64decode(imageItemTupleItem4[0]))
                                                file4.name = '{0}.{1}'.format(imageItemTupleItem4[1], imageItemTupleItem4[2])
                                                newImagesAttachment.image4 = file4
                                            elif cachedFilesDictDictImagesLength == 5:
                                                imageItemTupleItem1 = cachedFilesDictDictImagesList[0]
                                                file1 = ContentFile(base64.b64decode(imageItemTupleItem1[0]))
                                                file1.name = '{0}.{1}'.format(imageItemTupleItem1[1], imageItemTupleItem1[2])
                                                newImagesAttachment.image1 = file1
                                                imageItemTupleItem2 = cachedFilesDictDictImagesList[1]
                                                file2 = ContentFile(base64.b64decode(imageItemTupleItem2[0]))
                                                file2.name = '{0}.{1}'.format(imageItemTupleItem2[1], imageItemTupleItem2[2])
                                                newImagesAttachment.image2 = file2
                                                imageItemTupleItem3 = cachedFilesDictDictImagesList[2]
                                                file3 = ContentFile(base64.b64decode(imageItemTupleItem3[0]))
                                                file3.name = '{0}.{1}'.format(imageItemTupleItem3[1], imageItemTupleItem3[2])
                                                newImagesAttachment.image3 = file3
                                                imageItemTupleItem4 = cachedFilesDictDictImagesList[3]
                                                file4 = ContentFile(base64.b64decode(imageItemTupleItem4[0]))
                                                file4.name = '{0}.{1}'.format(imageItemTupleItem4[1], imageItemTupleItem4[2])
                                                newImagesAttachment.image4 = file4
                                                imageItemTupleItem5 = cachedFilesDictDictImagesList[4]
                                                file5 = ContentFile(base64.b64decode(imageItemTupleItem5[0]))
                                                file5.name = '{0}.{1}'.format(imageItemTupleItem5[1], imageItemTupleItem5[2])
                                                newImagesAttachment.image5 = file5
                                            else:
                                                imageItemTupleItem1 = cachedFilesDictDictImagesList[0]
                                                file1 = ContentFile(base64.b64decode(imageItemTupleItem1[0]))
                                                file1.name = '{0}.{1}'.format(imageItemTupleItem1[1], imageItemTupleItem1[2])
                                                newImagesAttachment.image1 = file1
                                                imageItemTupleItem2 = cachedFilesDictDictImagesList[1]
                                                file2 = ContentFile(base64.b64decode(imageItemTupleItem2[0]))
                                                file2.name = '{0}.{1}'.format(imageItemTupleItem2[1], imageItemTupleItem2[2])
                                                newImagesAttachment.image2 = file2
                                                imageItemTupleItem3 = cachedFilesDictDictImagesList[2]
                                                file3 = ContentFile(base64.b64decode(imageItemTupleItem3[0]))
                                                file3.name = '{0}.{1}'.format(imageItemTupleItem3[1], imageItemTupleItem3[2])
                                                newImagesAttachment.image3 = file3
                                                imageItemTupleItem4 = cachedFilesDictDictImagesList[3]
                                                file4 = ContentFile(base64.b64decode(imageItemTupleItem4[0]))
                                                file4.name = '{0}.{1}'.format(imageItemTupleItem4[1], imageItemTupleItem4[2])
                                                newImagesAttachment.image4 = file4
                                                imageItemTupleItem5 = cachedFilesDictDictImagesList[4]
                                                file5 = ContentFile(base64.b64decode(imageItemTupleItem5[0]))
                                                file5.name = '{0}.{1}'.format(imageItemTupleItem5[1], imageItemTupleItem5[2])
                                                newImagesAttachment.image5 = file5
                                                imageItemTupleItem6 = cachedFilesDictDictImagesList[5]
                                                file6 = ContentFile(base64.b64decode(imageItemTupleItem6[0]))
                                                file6.name = '{0}.{1}'.format(imageItemTupleItem6[1], imageItemTupleItem6[2])
                                                newImagesAttachment.image6 = file6
                                        newImagesAttachment.itemsLength = cachedFilesDictDictImagesLength
                                        newMessage.imagesAttachment = newImagesAttachment
                                        newMessage.members.set(dialogUsers)
                                        newMessage.save()
                                        newImagesAttachment.save()
                                        dialogAttributes.lastUpdate = currTime
                                        dialogAttributes.save()
                                        lastMessageCache.set(dialogId, newMessage)
                                        dialogsCache = caches["dialogsCache"]
                                        cachedDialogsMany = dialogsCache.get_many(dialogUsersIds)
                                        dialogsToSet = {}
                                        for id, cachedDialog in cachedDialogsMany.items():
                                            dialogsDict = cachedDialog.get("dialogs", None)
                                            lastMessagesDictList = cachedDialog.get("last", None)
                                            if dialogsDict is not None or lastMessagesDictList is not None:
                                                if dialogsDict is not None:
                                                    if dialogId not in dialogsDict.keys():
                                                        dialogsDict.update({dialogId: dialog})
                                                    dialogsDict.move_to_end(dialogId, last = False)
                                                    cachedDialog.update({"dialogs": dialogsDict})
                                                if lastMessagesDictList is not None:
                                                    lastMessagesList = lastMessagesDictList.get(dialogId, None)
                                                    if lastMessagesList is None:
                                                        lastMessagesList = [newMessage]
                                                    else:
                                                        lastMessagesList.append(newMessage)
                                                        if len(lastMessagesList) > 6:
                                                            del lastMessagesList[0]
                                                    lastMessagesDictList.update({dialogId: lastMessagesList})
                                                    cachedDialog.update({"last": lastMessagesDictList})
                                                dialogsToSet.update({id: cachedDialog})
                                        if len(dialogsToSet.keys()) > 0:
                                            dialogsCache.set_many(dialogsToSet)
                                        idsToGetUpdates = dialogUsersIds
                                        idsToGetUpdates.remove(myId)
                                        messageIdStr = str(newMessage.id)
                                        unreadMessagesCache = caches['unreadGroupMessages']
                                        unreadMessagesDict = unreadMessagesCache.get(dialogId, None)
                                        if unreadMessagesDict is None:
                                            unreadMessagesCache.set(dialogId, {messageIdStr: idsToGetUpdates})
                                        else:
                                            unreadMessagesDict.update({messageIdStr: idsToGetUpdates})
                                            unreadMessagesCache.set(dialogId, unreadMessagesDict)
                                        updateDataDialog = ('groupMessage', 'new', myId, dialogId, dialogAttributes.slug, dialogAttributes.name)
                                        newUpdatesCache = caches['newUpdates']
                                        updatesDataMany = newUpdatesCache.get_many(idsToGetUpdates)
                                        updatesStatus = {}
                                        for id in idsToGetUpdates:
                                            updatesListTuple = updatesDataMany.get(id, None)
                                            if updatesListTuple is None:
                                                updatesDataMany.update({id: [updateDataDialog]})
                                                updatesStatus[id] = None
                                            else:
                                                find = False
                                                for update in updatesListTuple:
                                                    if update[0] == 'groupMessage' and update[3] == dialogId:
                                                        find = True
                                                        if update[1] == "hidden":
                                                            updatesStatus[id] = False
                                                        else:
                                                            updatesStatus[id] = True
                                                        break
                                                if find is False:
                                                    updatesListTuple.append(updateDataDialog)
                                                    updatesDataMany.update({id: updatesListTuple})
                                        newUpdatesCache.set_many(updatesDataMany)
                                except Error:
                                    save = False
                                if save is True:
                                    del cachedFilesDict[myId]
                                    messagesFilesStorage.set(dialogId, cachedFilesDict)
                            else:
                                if messageTextBodyLen > 0:
                                    try:
                                        with atomic():
                                            if lastMessage is None:
                                                lastMessage = groupMessage.objects.filter(inDialog__exact = dialog)
                                                if lastMessage.count() > 0:
                                                    lastMessage = lastMessage.last()
                                                    if str(lastMessage.by_id) == myId:
                                                        newMessage = groupMessage.objects.create(by = requestUser, body = messageTextBody, messageHead = False, time = currTime, inDialog = dialog)
                                                    else:
                                                        newMessage = groupMessage.objects.create(by = requestUser, body = messageTextBody, messageHead = True, time = currTime, inDialog = dialog)
                                                else:
                                                    newMessage = groupMessage.objects.create(by = requestUser, body = messageTextBody, messageHead = True, time = currTime, inDialog = dialog)
                                            else:
                                                if str(lastMessage.by_id) == myId:
                                                    newMessage = groupMessage.objects.create(by = requestUser, body = messageTextBody, messageHead = False, time = currTime, inDialog = dialog)
                                                else:
                                                    newMessage = groupMessage.objects.create(by = requestUser, body = messageTextBody, messageHead = True, time = currTime, inDialog = dialog)
                                            newMessage.members.set(dialogUsers)
                                            newMessage.save()
                                            dialogAttributes.lastUpdate = currTime
                                            dialogAttributes.save()
                                            lastMessageCache.set(dialogId, newMessage)
                                            dialogsCache = caches["dialogsCache"]
                                            cachedDialogsMany = dialogsCache.get_many(dialogUsersIds)
                                            dialogsToSet = {}
                                            for id, cachedDialog in cachedDialogsMany.items():
                                                dialogsDict = cachedDialog.get("dialogs", None)
                                                lastMessagesDictList = cachedDialog.get("last", None)
                                                if dialogsDict is not None or lastMessagesDictList is not None:
                                                    if dialogsDict is not None:
                                                        if dialogId not in dialogsDict.keys():
                                                            dialogsDict.update({dialogId: dialog})
                                                        dialogsDict.move_to_end(dialogId, last = False)
                                                        cachedDialog.update({"dialogs": dialogsDict})
                                                    if lastMessagesDictList is not None:
                                                        lastMessagesList = lastMessagesDictList.get(dialogId, None)
                                                        if lastMessagesList is None:
                                                            lastMessagesList = [newMessage]
                                                        else:
                                                            lastMessagesList.append(newMessage)
                                                            if len(lastMessagesList) > 6:
                                                                del lastMessagesList[0]
                                                        lastMessagesDictList.update({dialogId: lastMessagesList})
                                                        cachedDialog.update({"last": lastMessagesDictList})
                                                    dialogsToSet.update({id: cachedDialog})
                                            if len(dialogsToSet.keys()) > 0:
                                                dialogsCache.set_many(dialogsToSet)
                                            idsToGetUpdates = dialogUsersIds
                                            idsToGetUpdates.remove(myId)
                                            messageIdStr = str(newMessage.id)
                                            unreadMessagesCache = caches['unreadGroupMessages']
                                            unreadMessagesDict = unreadMessagesCache.get(dialogId, None)
                                            if unreadMessagesDict is None:
                                                unreadMessagesCache.set(dialogId, {messageIdStr: idsToGetUpdates})
                                            else:
                                                unreadMessagesDict.update({messageIdStr: idsToGetUpdates})
                                                unreadMessagesCache.set(dialogId, unreadMessagesDict)
                                            updateDataDialog = ('groupMessage', 'new', myId, dialogId, dialogAttributes.slug, dialogAttributes.name)
                                            newUpdatesCache = caches['newUpdates']
                                            updatesDataMany = newUpdatesCache.get_many(idsToGetUpdates)
                                            updatesStatus = {}
                                            for id in idsToGetUpdates:
                                                updatesListTuple = updatesDataMany.get(id, None)
                                                if updatesListTuple is None:
                                                    updatesDataMany.update({id: [updateDataDialog]})
                                                    updatesStatus[id] = None
                                                else:
                                                    find = False
                                                    for update in updatesListTuple:
                                                        if update[0] == 'groupMessage' and update[3] == dialogId:
                                                            find = True
                                                            if update[1] == "hidden":
                                                                updatesStatus[id] = False
                                                            else:
                                                                updatesStatus[id] = True
                                                            break
                                                    if find is False:
                                                        updatesListTuple.append(updateDataDialog)
                                                        updatesDataMany.update({id: updatesListTuple})
                                            newUpdatesCache.set_many(updatesDataMany)
                                        newImagesAttachment = None
                                    except Error:
                                        save = False
                                else:
                                    save = False
                        else:
                            if messageTextBodyLen > 0:
                                try:
                                    with atomic():
                                        if lastMessage is None:
                                            lastMessage = groupMessage.objects.filter(inDialog__exact = dialog)
                                            if lastMessage.count() > 0:
                                                lastMessage = lastMessage.last()
                                                if str(lastMessage.by_id) == myId:
                                                    newMessage = groupMessage.objects.create(by = requestUser, body = messageTextBody, messageHead = False, time = currTime, inDialog = dialog)
                                                else:
                                                    newMessage = groupMessage.objects.create(by = requestUser, body = messageTextBody, messageHead = True, time = currTime, inDialog = dialog)
                                            else:
                                                newMessage = groupMessage.objects.create(by = requestUser, body = messageTextBody, messageHead = True, time = currTime, inDialog = dialog)
                                        else:
                                            if str(lastMessage.by_id) == myId:
                                                newMessage = groupMessage.objects.create(by = requestUser, body = messageTextBody, messageHead = False, time = currTime, inDialog = dialog)
                                            else:
                                                newMessage = groupMessage.objects.create(by = requestUser, body = messageTextBody, messageHead = True, time = currTime, inDialog = dialog)
                                        newMessage.members.set(dialogUsers)
                                        newMessage.save()
                                        dialogAttributes.lastUpdate = currTime
                                        dialogAttributes.save()
                                        lastMessageCache.set(dialogId, newMessage)
                                        dialogsCache = caches["dialogsCache"]
                                        cachedDialogsMany = dialogsCache.get_many(dialogUsersIds)
                                        dialogsToSet = {}
                                        for id, cachedDialog in cachedDialogsMany.items():
                                            dialogsDict = cachedDialog.get("dialogs", None)
                                            lastMessagesDictList = cachedDialog.get("last", None)
                                            if dialogsDict is not None or lastMessagesDictList is not None:
                                                if dialogsDict is not None:
                                                    if dialogId not in dialogsDict.keys():
                                                        dialogsDict.update({dialogId: dialog})
                                                    dialogsDict.move_to_end(dialogId, last = False)
                                                    cachedDialog.update({"dialogs": dialogsDict})
                                                if lastMessagesDictList is not None:
                                                    lastMessagesList = lastMessagesDictList.get(dialogId, None)
                                                    if lastMessagesList is None:
                                                        lastMessagesList = [newMessage]
                                                    else:
                                                        lastMessagesList.append(newMessage)
                                                        if len(lastMessagesList) > 6:
                                                            del lastMessagesList[0]
                                                    lastMessagesDictList.update({dialogId: lastMessagesList})
                                                    cachedDialog.update({"last": lastMessagesDictList})
                                                dialogsToSet.update({id: cachedDialog})
                                        if len(dialogsToSet.keys()) > 0:
                                            dialogsCache.set_many(dialogsToSet)
                                        idsToGetUpdates = dialogUsersIds
                                        idsToGetUpdates.remove(myId)
                                        messageIdStr = str(newMessage.id)
                                        unreadMessagesCache = caches['unreadGroupMessages']
                                        unreadMessagesDict = unreadMessagesCache.get(dialogId, None)
                                        if unreadMessagesDict is None:
                                            unreadMessagesCache.set(dialogId, {messageIdStr: idsToGetUpdates})
                                        else:
                                            unreadMessagesDict.update({messageIdStr: idsToGetUpdates})
                                            unreadMessagesCache.set(dialogId, unreadMessagesDict)
                                        updateDataDialog = ('groupMessage', 'new', myId, dialogId, dialogAttributes.slug, dialogAttributes.name)
                                        newUpdatesCache = caches['newUpdates']
                                        updatesDataMany = newUpdatesCache.get_many(idsToGetUpdates)
                                        updatesStatus = {}
                                        for id in idsToGetUpdates:
                                            updatesListTuple = updatesDataMany.get(id, None)
                                            if updatesListTuple is None:
                                                updatesDataMany.update({id: [updateDataDialog]})
                                                updatesStatus[id] = None
                                            else:
                                                find = False
                                                for update in updatesListTuple:
                                                    if update[0] == 'groupMessage' and update[3] == dialogId:
                                                        find = True
                                                        if update[1] == "hidden":
                                                            updatesStatus[id] = False
                                                        else:
                                                            updatesStatus[id] = True
                                                        break
                                                if find is False:
                                                    updatesListTuple.append(updateDataDialog)
                                                    updatesDataMany.update({id: updatesListTuple})
                                        newUpdatesCache.set_many(updatesDataMany)
                                    newImagesAttachment = None
                                except Error:
                                    save = False
                            else:
                                save = False
                    else:
                        if messageTextBodyLen > 0:
                            try:
                                with atomic():
                                    if lastMessage is None:
                                        lastMessage = groupMessage.objects.filter(inDialog__exact = dialog)
                                        if lastMessage.count() > 0:
                                            lastMessage = lastMessage.last()
                                            if str(lastMessage.by_id) == myId:
                                                newMessage = groupMessage.objects.create(by = requestUser, body = messageTextBody, messageHead = False, time = currTime, inDialog = dialog)
                                            else:
                                                newMessage = groupMessage.objects.create(by = requestUser, body = messageTextBody, messageHead = True, time = currTime, inDialog = dialog)
                                        else:
                                            newMessage = groupMessage.objects.create(by = requestUser, body = messageTextBody, messageHead = True, time = currTime, inDialog = dialog)
                                    else:
                                        if str(lastMessage.by_id) == myId:
                                            newMessage = groupMessage.objects.create(by = requestUser, body = messageTextBody, messageHead = False, time = currTime, inDialog = dialog)
                                        else:
                                            newMessage = groupMessage.objects.create(by = requestUser, body = messageTextBody, messageHead = True, time = currTime, inDialog = dialog)
                                    newMessage.members.set(dialogUsers)
                                    newMessage.save()
                                    dialogAttributes.lastUpdate = currTime
                                    dialogAttributes.save()
                                    lastMessageCache.set(dialogId, newMessage)
                                    dialogsCache = caches["dialogsCache"]
                                    cachedDialogsMany = dialogsCache.get_many(dialogUsersIds)
                                    dialogsToSet = {}
                                    for id, cachedDialog in cachedDialogsMany.items():
                                        dialogsDict = cachedDialog.get("dialogs", None)
                                        lastMessagesDictList = cachedDialog.get("last", None)
                                        if dialogsDict is not None or lastMessagesDictList is not None:
                                            if dialogsDict is not None:
                                                if dialogId not in dialogsDict.keys():
                                                    dialogsDict.update({dialogId: dialog})
                                                dialogsDict.move_to_end(dialogId, last = False)
                                                cachedDialog.update({"dialogs": dialogsDict})
                                            if lastMessagesDictList is not None:
                                                lastMessagesList = lastMessagesDictList.get(dialogId, None)
                                                if lastMessagesList is None:
                                                    lastMessagesList = [newMessage]
                                                else:
                                                    lastMessagesList.append(newMessage)
                                                    if len(lastMessagesList) > 6:
                                                        del lastMessagesList[0]
                                                lastMessagesDictList.update({dialogId: lastMessagesList})
                                                cachedDialog.update({"last": lastMessagesDictList})
                                            dialogsToSet.update({id: cachedDialog})
                                    if len(dialogsToSet.keys()) > 0:
                                        dialogsCache.set_many(dialogsToSet)
                                    idsToGetUpdates = dialogUsersIds
                                    idsToGetUpdates.remove(myId)
                                    messageIdStr = str(newMessage.id)
                                    unreadMessagesCache = caches['unreadGroupMessages']
                                    unreadMessagesDict = unreadMessagesCache.get(dialogId, None)
                                    if unreadMessagesDict is None:
                                        unreadMessagesCache.set(dialogId, {messageIdStr: idsToGetUpdates})
                                    else:
                                        unreadMessagesDict.update({messageIdStr: idsToGetUpdates})
                                        unreadMessagesCache.set(dialogId, unreadMessagesDict)
                                    updateDataDialog = ('groupMessage', 'new', myId, dialogId, dialogAttributes.slug, dialogAttributes.name)
                                    newUpdatesCache = caches['newUpdates']
                                    updatesDataMany = newUpdatesCache.get_many(idsToGetUpdates)
                                    updatesStatus = {}
                                    for id in idsToGetUpdates:
                                        updatesListTuple = updatesDataMany.get(id, None)
                                        if updatesListTuple is None:
                                            updatesDataMany.update({id: [updateDataDialog]})
                                            updatesStatus[id] = None
                                        else:
                                            find = False
                                            for update in updatesListTuple:
                                                if update[0] == 'groupMessage' and update[3] == dialogId:
                                                    find = True
                                                    if update[1] == "hidden":
                                                        updatesStatus[id] = False
                                                    else:
                                                        updatesStatus[id] = True
                                                    break
                                            if find is False:
                                                updatesListTuple.append(updateDataDialog)
                                                updatesDataMany.update({id: updatesListTuple})
                                    newUpdatesCache.set_many(updatesDataMany)
                                newImagesAttachment = None
                            except Error:
                                save = False
                        else:
                            save = False
                    if save is True:
                        dialogClass = 'dialogMessage-new-%s' % dialogId
                        notifyFullUpdate = {
                        'notifyCounterChange': {
                            'method': 'incrementNotifyValue',
                            'baseValue': 1,
                            'multiValue': 1,
                            'class': dialogClass
                            }
                        }
                        notifySimpleUpdate = {
                            'notifyCounterChange': {
                                'method': 'incrementNotifyValue',
                                'baseValue': 1,
                                'class': dialogClass
                                }
                            }
                        notifySpecialUpdate = {
                            'notifyCounterChange': {
                                'method': 'incrementNotifyValue',
                                'baseValue': 0,
                                'multiValue': 1,
                                'class': dialogClass
                                }
                            }
                        if newImagesAttachment is None:
                            messageDict = {
                                'method': 'messageGroup',
                                'dialogLink': request.path,
                                'id': messageIdStr,
                                'body': newMessage.body,
                                'slug': myProfileUrl,
                                'value': dialogClass,
                                'dialogId': dialogId,
                                'byId': myId,
                                'eventType': 'notify',
                                # Дальше для метода messageMany
                                'count': 0,
                                'profileLink': reverse("profile", kwargs = {'currentUser': myProfileUrl}),
                                'username': requestUser.username,
                                'forgetLink': reverse("deleteNotifyEvent", kwargs = {'type': 'dialogMessage', 'state': 'new', 'senderId': myId})
                                }
                        else:
                            if cachedFilesDictDictImagesLength <= 3:
                                if cachedFilesDictDictImagesLength == 1:
                                    imagesList = [newImagesAttachment.image1.url]
                                elif cachedFilesDictDictImagesLength == 2:
                                    imagesList = [
                                        newImagesAttachment.image1.url,
                                        newImagesAttachment.image2.url
                                    ]
                                else:
                                    imagesList = [
                                        newImagesAttachment.image1.url,
                                        newImagesAttachment.image2.url,
                                        newImagesAttachment.image3.url
                                    ]
                            else:
                                if cachedFilesDictDictImagesLength == 4:
                                    imagesList = [
                                        newImagesAttachment.image1.url,
                                        newImagesAttachment.image2.url,
                                        newImagesAttachment.image3.url,
                                        newImagesAttachment.image4.url
                                    ]
                                elif cachedFilesDictDictImagesLength == 5:
                                    imagesList = [
                                        newImagesAttachment.image1.url,
                                        newImagesAttachment.image2.url,
                                        newImagesAttachment.image3.url,
                                        newImagesAttachment.image4.url,
                                        newImagesAttachment.image5.url
                                    ]
                                else:
                                    imagesList = [
                                        newImagesAttachment.image1.url,
                                        newImagesAttachment.image2.url,
                                        newImagesAttachment.image3.url,
                                        newImagesAttachment.image4.url,
                                        newImagesAttachment.image5.url,
                                        newImagesAttachment.image6.url
                                    ]
                            messageDict = {
                                'method': 'messageGroup',
                                'dialogLink': request.path,
                                'id': messageIdStr,
                                'body': newMessage.body,
                                'images': imagesList,
                                'slug': myProfileUrl,
                                'value': dialogClass,
                                'dialogId': dialogId,
                                'byId': myId,
                                'eventType': 'notify',
                                # Дальше для метода messageMany
                                'count': 0,
                                'profileLink': reverse("profile", kwargs = {'currentUser': myProfileUrl}),
                                'username': requestUser.username,
                                'forgetLink': reverse("deleteNotifyEvent", kwargs = {'type': 'dialogMessage', 'state': 'new', 'senderId': myId})
                                }
                        usersInGroupChannelSet = caches["groupDialogsChannelsLayers"].get(dialogId, None)
                        if usersInGroupChannelSet is not None:
                            if len(dialogUsersIds - usersInGroupChannelSet) > 0:
                                layer = get_channel_layer().group_send
                                specialData = None
                                fullData = None
                                simpleData = None
                                unreadMessagesCounter = {} # {userId: unreadMessagesLength} - для наращивания счётчика оповещений, если оповещение было скрыто
                                updatesStatusItems = updatesStatus.items()
                                for id, status in updatesStatusItems:
                                    if status is True:
                                        if id in usersInGroupChannelSet:
                                            messageDict.update({id: notifySimpleUpdate})
                                        else:
                                            if simpleData is None:
                                                simpleData = json.dumps(messageDict.copy().update(notifySimpleUpdate))
                                            async_to_sync(layer)(id, {"type": "sendToClient", "data": {"item": simpleData}})
                                    elif status is False:
                                        if len(unreadMessagesCounter.keys()) == 0:
                                            unreadMessagesDictItems = unreadMessagesDict.items()
                                            for messageId, messageRecipients in unreadMessagesDictItems:
                                                for id in messageRecipients:
                                                    counter = unreadMessagesCounter.get(id, None)
                                                    if counter is not None:
                                                        counter += 1
                                                        unreadMessagesCounter[id] = counter
                                                    else:
                                                        unreadMessagesCounter[id] = 1
                                        if id in usersInGroupChannelSet:
                                            messageDict.update({id: notifySpecialUpdate})
                                        else:
                                            if specialData is None:
                                                specialData = json.dumps(messageDict.copy().update(notifySpecialUpdate))
                                            async_to_sync(layer)(id, {"type": "sendToClient", "data": {"item": specialData}})
                                    else: # None
                                        if id in usersInGroupChannelSet:
                                            messageDict.update({id: notifyFullUpdate})
                                        else:
                                            if fullData is None:
                                                fullData = json.dumps(messageDict.copy().update(notifyFullUpdate))
                                            async_to_sync(layer)(id, {"type": "sendToClient", "data": {"item": fullData}})
                                async_to_sync(layer)(dialogId, {"type": "sendGroupMessage", "channelName": dialogId, "data": messageDict})
                            else:
                                unreadMessagesCounter = {} # {userId: unreadMessagesLength} - для наращивания счётчика оповещений, если оповещение было скрыто
                                updatesStatusItems = updatesStatus.items()
                                for id, status in updatesStatusItems:
                                    if status is True:

                                        messageDict.update({id: notifySimpleUpdate})
                                    elif status is False:
                                        if len(unreadMessagesCounter.keys()) == 0:
                                            unreadMessagesDictItems = unreadMessagesDict.items()
                                            for messageId, messageRecipients in unreadMessagesDictItems:
                                                for id in messageRecipients:
                                                    counter = unreadMessagesCounter.get(id, None)
                                                    if counter is not None:
                                                        counter += 1
                                                        unreadMessagesCounter[id] = counter
                                                    else:
                                                        unreadMessagesCounter[id] = 1
                                        messageDict.update({id: notifySpecialUpdate})
                                    else: # None
                                        messageDict.update({id: notifyFullUpdate})
                                async_to_sync(get_channel_layer().group_send)(dialogId, {"type": "sendGroupMessage", "data": messageDict})
                        else:
                            layer = get_channel_layer().group_send
                            specialData = None
                            fullData = None
                            simpleData = None
                            unreadMessagesCounter = {} # {userId: unreadMessagesLength} - для наращивания счётчика оповещений, если оповещение было скрыто
                            updatesStatusItems = updatesStatus.items()
                            for id, status in updatesStatusItems:
                                if status is True:
                                    if simpleData is None:
                                        simpleData = json.dumps(messageDict.copy().update(notifySimpleUpdate))
                                    async_to_sync(layer)(id, {"type": "sendToClient", "data": {"item": simpleData}})
                                elif status is False:
                                    if len(unreadMessagesCounter.keys()) == 0:
                                        unreadMessagesDictItems = unreadMessagesDict.items()
                                        for messageId, messageRecipients in unreadMessagesDictItems:
                                            for id in messageRecipients:
                                                counter = unreadMessagesCounter.get(id, None)
                                                if counter is not None:
                                                    counter += 1
                                                    unreadMessagesCounter[id] = counter
                                                else:
                                                    unreadMessagesCounter[id] = 1
                                    if specialData is None:
                                        specialData = json.dumps(messageDict.copy().update(notifySpecialUpdate))
                                    async_to_sync(layer)(id, {"type": "sendToClient", "data": {"item": specialData}})
                                else: # None
                                    if fullData is None:
                                        fullData = json.dumps(messageDict.copy().update(notifyFullUpdate))
                                    async_to_sync(layer)(id, {"type": "sendToClient", "data": {"item": fullData}})
                        return JsonResponse({"data": json.dumps(messageDict)})
                    # Database Error
                    return JsonResponse({"data": None})
            reqFiles = request.FILES
            if len(reqFiles.keys()) > 0:
                form = sendMessageForm(request.POST, reqFiles)
                filesList = reqFiles.getlist('insertion')
            else:
                form = sendMessageForm(request.POST)
                filesList = None
            messagesFilesStorage = caches['filesStorageMessages']
            cachedFilesDict = messagesFilesStorage.get(dialogId, None)
            if cachedFilesDict is not None:
                cachedFilesDictDict = cachedFilesDict.get(myId, None)
                if cachedFilesDictDict is not None:
                    cachedFilesDictDictImages = cachedFilesDictDict.get("images", None)
                else:
                    cachedFilesDictDictImages = None
            else:
                cachedFilesDictDictImages = None
            if form.is_valid():
                lastMessageCache = caches['lastMessage']
                lastMessage = lastMessageCache.get(dialogId, None)
                save = True
                myProfileUrl = requestUser.profileUrl
                currTime = datetime.now(tz = tz.gettz())
                if cachedFilesDictDictImages is not None:
                    cachedImagesList = list(cachedFilesDictDictImages.values())
                else:
                    cachedImagesList = None
                if filesList is not None or cachedImagesList is not None:
                    if filesList is not None and cachedImagesList is not None:
                        imagesToSave = cachedImagesList + filesList
                    elif cachedImagesList is None:
                        imagesToSave = filesList
                    else:
                        imagesToSave = cachedImagesList
                    try:
                        with atomic():
                            if lastMessage is None:
                                lastMessage = groupMessage.objects.filter(inDialog__exact = dialog)
                                if lastMessage.count() > 0:
                                    lastMessage = lastMessage.last()
                                    if str(lastMessage.by_id) == myId:
                                        newMessage = groupMessage.objects.create(by = requestUser, body = form.cleaned_data['body'], messageHead = False, time = currTime, inDialog = dialog)
                                    else:
                                        newMessage = groupMessage.objects.create(by = requestUser, body = form.cleaned_data['body'], messageHead = True, time = currTime, inDialog = dialog)
                                else:
                                    newMessage = groupMessage.objects.create(by = requestUser, body = form.cleaned_data['body'], messageHead = True, time = currTime, inDialog = dialog)
                            else:
                                if str(lastMessage.by_id) == myId:
                                    newMessage = groupMessage.objects.create(by = requestUser, body = form.cleaned_data['body'], messageHead = False, time = currTime, inDialog = dialog)
                                else:
                                    newMessage = groupMessage.objects.create(by = requestUser, body = form.cleaned_data['body'], messageHead = True, time = currTime, inDialog = dialog)
                            newMessage.members.set(dialogUsers)
                            newImagesAttachment = imagesAttachmentMessageGroup.objects.create(message = newMessage, by = requestUser, itemsLength = 0)
                            cachedImagesLength = len(imagesToSave)
                            if cachedImagesLength <= 3:
                                if cachedImagesLength == 1:
                                    newImagesAttachment.image1 = imagesToSave[0]
                                elif cachedImagesLength == 2:
                                    newImagesAttachment.image1 = imagesToSave[0]
                                    newImagesAttachment.image2 = imagesToSave[1]
                                else:
                                    newImagesAttachment.image1 = imagesToSave[0]
                                    newImagesAttachment.image2 = imagesToSave[1]
                                    newImagesAttachment.image3 = imagesToSave[2]
                            else:
                                if cachedImagesLength == 4:
                                    newImagesAttachment.image1 = imagesToSave[0]
                                    newImagesAttachment.image2 = imagesToSave[1]
                                    newImagesAttachment.image3 = imagesToSave[2]
                                    newImagesAttachment.image4 = imagesToSave[3]
                                elif cachedImagesLength == 5:
                                    newImagesAttachment.image1 = imagesToSave[0]
                                    newImagesAttachment.image2 = imagesToSave[1]
                                    newImagesAttachment.image3 = imagesToSave[2]
                                    newImagesAttachment.image4 = imagesToSave[3]
                                    newImagesAttachment.image5 = imagesToSave[4]
                                else:
                                    newImagesAttachment.image1 = imagesToSave[0]
                                    newImagesAttachment.image2 = imagesToSave[1]
                                    newImagesAttachment.image3 = imagesToSave[2]
                                    newImagesAttachment.image4 = imagesToSave[3]
                                    newImagesAttachment.image5 = imagesToSave[4]
                                    newImagesAttachment.image6 = imagesToSave[5]
                            newImagesAttachment.itemsLength = cachedImagesLength
                            newMessage.imagesAttachment = newImagesAttachment
                            dialog.save()
                            newMessage.save()
                            newImagesAttachment.save()
                            dialogAttributes.lastUpdate = currTime
                            dialogAttributes.save()
                            lastMessageCache.set(dialogId, newMessage)
                            dialogsCache = caches["dialogsCache"]
                            cachedDialogsMany = dialogsCache.get_many(dialogUsersIds)
                            dialogsToSet = {}
                            for id, cachedDialog in cachedDialogsMany.items():
                                dialogsDict = cachedDialog.get("dialogs", None)
                                lastMessagesDictList = cachedDialog.get("last", None)
                                if dialogsDict is not None or lastMessagesDictList is not None:
                                    if dialogsDict is not None:
                                        if dialogId not in dialogsDict.keys():
                                            dialogsDict.update({dialogId: dialog})
                                        dialogsDict.move_to_end(dialogId, last = False)
                                        cachedDialog.update({"dialogs": dialogsDict})
                                    if lastMessagesDictList is not None:
                                        lastMessagesList = lastMessagesDictList.get(dialogId, None)
                                        if lastMessagesList is None:
                                            lastMessagesList = [newMessage]
                                        else:
                                            lastMessagesList.append(newMessage)
                                            if len(lastMessagesList) > 6:
                                                del lastMessagesList[0]
                                        lastMessagesDictList.update({dialogId: lastMessagesList})
                                        cachedDialog.update({"last": lastMessagesDictList})
                                    dialogsToSet.update({id: cachedDialog})
                            if len(dialogsToSet.keys()) > 0:
                                dialogsCache.set_many(dialogsToSet)
                            idsToGetUpdates = dialogUsersIds
                            idsToGetUpdates.remove(myId)
                            messageIdStr = str(newMessage.id)
                            unreadMessagesCache = caches['unreadGroupMessages']
                            unreadMessagesDict = unreadMessagesCache.get(dialogId, None)
                            if unreadMessagesDict is None:
                                unreadMessagesCache.set(dialogId, {messageIdStr: idsToGetUpdates})
                            else:
                                unreadMessagesDict.update({messageIdStr: idsToGetUpdates})
                                unreadMessagesCache.set(dialogId, unreadMessagesDict)
                            updateDataDialog = ('groupMessage', 'new', myId, dialogId, dialogAttributes.slug, dialogAttributes.name)
                            newUpdatesCache = caches['newUpdates']
                            updatesDataMany = newUpdatesCache.get_many(idsToGetUpdates)
                            updatesStatus = {}
                            for id in idsToGetUpdates:
                                updatesListTuple = updatesDataMany.get(id, None)
                                if updatesListTuple is None:
                                    updatesDataMany.update({id: [updateDataDialog]})
                                    updatesStatus[id] = None
                                else:
                                    find = False
                                    for update in updatesListTuple:
                                        if update[0] == 'groupMessage' and update[3] == dialogId:
                                            find = True
                                            if update[1] == "hidden":
                                                updatesStatus[id] = False
                                            else:
                                                updatesStatus[id] = True
                                            break
                                    if find is False:
                                        updatesListTuple.append(updateDataDialog)
                                        updatesDataMany.update({id: updatesListTuple})
                            newUpdatesCache.set_many(updatesDataMany)
                    except Error:
                        save = False
                    if save is True:
                        if cachedImagesList is not None:
                            del cachedFilesDict[myId]
                            messagesFilesStorage.set(dialogId, cachedFilesDict)
                else:
                    try:
                        with atomic():
                            if lastMessage is None:
                                lastMessage = groupMessage.objects.filter(inDialog__exact = dialog)
                                if lastMessage.count() > 0:
                                    lastMessage = lastMessage.last()
                                    if str(lastMessage.by_id) == myId:
                                        newMessage = groupMessage.objects.create(by = requestUser, body = form.cleaned_data['body'], messageHead = False, time = currTime, inDialog = dialog)
                                    else:
                                        newMessage = groupMessage.objects.create(by = requestUser, body = form.cleaned_data['body'], messageHead = True, time = currTime, inDialog = dialog)
                                else:
                                    newMessage = groupMessage.objects.create(by = requestUser, body = form.cleaned_data['body'], messageHead = True, time = currTime, inDialog = dialog)
                            else:
                                if str(lastMessage.by_id) == myId:
                                    newMessage = groupMessage.objects.create(by = requestUser, body = form.cleaned_data['body'], messageHead = False, time = currTime, inDialog = dialog)
                                else:
                                    newMessage = groupMessage.objects.create(by = requestUser, body = form.cleaned_data['body'], messageHead = True, time = currTime, inDialog = dialog)
                            newMessage.members.set(dialogUsers)
                            dialog.save()
                            newMessage.save()
                            dialogAttributes.lastUpdate = currTime
                            dialogAttributes.save()
                            lastMessageCache.set(dialogId, newMessage)
                            dialogsCache = caches["dialogsCache"]
                            cachedDialogsMany = dialogsCache.get_many(dialogUsersIds)
                            dialogsToSet = {}
                            for id, cachedDialog in cachedDialogsMany.items():
                                dialogsDict = cachedDialog.get("dialogs", None)
                                lastMessagesDictList = cachedDialog.get("last", None)
                                if dialogsDict is not None or lastMessagesDictList is not None:
                                    if dialogsDict is not None:
                                        if dialogId not in dialogsDict.keys():
                                            dialogsDict.update({dialogId: dialog})
                                        dialogsDict.move_to_end(dialogId, last = False)
                                        cachedDialog.update({"dialogs": dialogsDict})
                                    if lastMessagesDictList is not None:
                                        lastMessagesList = lastMessagesDictList.get(dialogId, None)
                                        if lastMessagesList is None:
                                            lastMessagesList = [newMessage]
                                        else:
                                            lastMessagesList.append(newMessage)
                                            if len(lastMessagesList) > 6:
                                                del lastMessagesList[0]
                                        lastMessagesDictList.update({dialogId: lastMessagesList})
                                        cachedDialog.update({"last": lastMessagesDictList})
                                    dialogsToSet.update({id: cachedDialog})
                            if len(dialogsToSet.keys()) > 0:
                                dialogsCache.set_many(dialogsToSet)
                            idsToGetUpdates = dialogUsersIds
                            idsToGetUpdates.remove(myId)
                            messageIdStr = str(newMessage.id)
                            unreadMessagesCache = caches['unreadGroupMessages']
                            unreadMessagesDict = unreadMessagesCache.get(dialogId, None)
                            if unreadMessagesDict is None:
                                unreadMessagesCache.set(dialogId, {messageIdStr: idsToGetUpdates})
                            else:
                                unreadMessagesDict.update({messageIdStr: idsToGetUpdates})
                                unreadMessagesCache.set(dialogId, unreadMessagesDict)
                            updateDataDialog = ('groupMessage', 'new', myId, dialogId, dialogAttributes.slug, dialogAttributes.name)
                            newUpdatesCache = caches['newUpdates']
                            updatesDataMany = newUpdatesCache.get_many(idsToGetUpdates)
                            updatesStatus = {}
                            for id in idsToGetUpdates:
                                updatesListTuple = updatesDataMany.get(id, None)
                                if updatesListTuple is None:
                                    updatesDataMany.update({id: [updateDataDialog]})
                                    updatesStatus[id] = None
                                else:
                                    find = False
                                    for update in updatesListTuple:
                                        if update[0] == 'groupMessage' and update[3] == dialogId:
                                            find = True
                                            if update[1] == "hidden":
                                                updatesStatus[id] = False
                                            else:
                                                updatesStatus[id] = True
                                            break
                                    if find is False:
                                        updatesListTuple.append(updateDataDialog)
                                        updatesDataMany.update({id: updatesListTuple})
                            newUpdatesCache.set_many(updatesDataMany)
                        newImagesAttachment = None
                    except Error:
                        save = False
                if save is True:
                    dialogClass = 'dialogMessage-new-%s' % dialogId
                    notifyFullUpdate = {
                    'notifyCounterChange': {
                        'method': 'incrementNotifyValue',
                        'baseValue': 1,
                        'multiValue': 1,
                        'class': dialogClass
                        }
                    }
                    notifySimpleUpdate = {
                        'notifyCounterChange': {
                            'method': 'incrementNotifyValue',
                            'baseValue': 1,
                            'class': dialogClass
                            }
                        }
                    notifySpecialUpdate = {
                        'notifyCounterChange': {
                            'method': 'incrementNotifyValue',
                            'baseValue': 0,
                            'multiValue': 1,
                            'class': dialogClass
                            }
                        }
                    if newImagesAttachment is None:
                        messageDict = {
                            'method': 'messageGroup',
                            'dialogLink': request.path,
                            'id': messageIdStr,
                            'body': newMessage.body,
                            'slug': myProfileUrl,
                            'value': dialogClass,
                            'dialogId': dialogId,
                            'byId': myId,
                            'eventType': 'notify',
                            # Дальше для метода messageMany
                            'count': 0,
                            'profileLink': reverse("profile", kwargs = {'currentUser': myProfileUrl}),
                            'username': requestUser.username,
                            'forgetLink': reverse("deleteNotifyEvent", kwargs = {'type': 'dialogMessage', 'state': 'new', 'senderId': myId})
                            }
                    else:
                        cachedImagesLength = len(imagesToSave)
                        if cachedImagesLength <= 3:
                            if cachedImagesLength == 1:
                                imagesList = [newImagesAttachment.image1.url]
                            elif cachedImagesLength == 2:
                                imagesList = [
                                    newImagesAttachment.image1.url,
                                    newImagesAttachment.image2.url
                                ]
                            else:
                                imagesList = [
                                    newImagesAttachment.image1.url,
                                    newImagesAttachment.image2.url,
                                    newImagesAttachment.image3.url
                                ]
                        else:
                            if cachedImagesLength == 4:
                                imagesList = [
                                    newImagesAttachment.image1.url,
                                    newImagesAttachment.image2.url,
                                    newImagesAttachment.image3.url,
                                    newImagesAttachment.image4.url
                                ]
                            elif cachedImagesLength == 5:
                                imagesList = [
                                    newImagesAttachment.image1.url,
                                    newImagesAttachment.image2.url,
                                    newImagesAttachment.image3.url,
                                    newImagesAttachment.image4.url,
                                    newImagesAttachment.image5.url
                                ]
                            else:
                                imagesList = [
                                    newImagesAttachment.image1.url,
                                    newImagesAttachment.image2.url,
                                    newImagesAttachment.image3.url,
                                    newImagesAttachment.image4.url,
                                    newImagesAttachment.image5.url,
                                    newImagesAttachment.image6.url
                                ]
                        messageDict = {
                            'method': 'messageGroup',
                            'dialogLink': request.path,
                            'id': messageIdStr,
                            'body': newMessage.body,
                            'images': imagesList,
                            'slug': myProfileUrl,
                            'value': dialogClass,
                            'dialogId': dialogId,
                            'byId': myId,
                            'eventType': 'notify',
                            # Дальше для метода messageMany
                            'count': 0,
                            'profileLink': reverse("profile", kwargs = {'currentUser': myProfileUrl}),
                            'username': requestUser.username,
                            'forgetLink': reverse("deleteNotifyEvent", kwargs = {'type': 'dialogMessage', 'state': 'new', 'senderId': myId})
                            }
                    usersInGroupChannelSet = caches["groupDialogsChannelsLayers"].get(dialogId, None)
                    if usersInGroupChannelSet is not None:
                        if len(dialogUsersIds - usersInGroupChannelSet) > 0:
                            layer = get_channel_layer().group_send
                            specialData = None
                            fullData = None
                            simpleData = None
                            unreadMessagesCounter = {} # {userId: unreadMessagesLength} - для наращивания счётчика оповещений, если оповещение было скрыто
                            updatesStatusItems = updatesStatus.items()
                            for id, status in updatesStatusItems:
                                if status is True:
                                    if id in usersInGroupChannelSet:
                                        messageDict.update({id: notifySimpleUpdate})
                                    else:
                                        if simpleData is None:
                                            simpleData = json.dumps(messageDict.copy().update(notifySimpleUpdate))
                                        async_to_sync(layer)(id, {"type": "sendToClient", "data": {"item": simpleData}})
                                elif status is False:
                                    if len(unreadMessagesCounter.keys()) == 0:
                                        unreadMessagesDictItems = unreadMessagesDict.items()
                                        for messageId, messageRecipients in unreadMessagesDictItems:
                                            for id in messageRecipients:
                                                counter = unreadMessagesCounter.get(id, None)
                                                if counter is not None:
                                                    counter += 1
                                                    unreadMessagesCounter[id] = counter
                                                else:
                                                    unreadMessagesCounter[id] = 1
                                    if id in usersInGroupChannelSet:
                                        messageDict.update({id: notifySpecialUpdate})
                                    else:
                                        if specialData is None:
                                            specialData = json.dumps(messageDict.copy().update(notifySpecialUpdate))
                                        async_to_sync(layer)(id, {"type": "sendToClient", "data": {"item": specialData}})
                                else: # None
                                    if id in usersInGroupChannelSet:
                                        messageDict.update({id: notifyFullUpdate})
                                    else:
                                        if fullData is None:
                                            fullData = json.dumps(messageDict.copy().update(notifyFullUpdate))
                                        async_to_sync(layer)(id, {"type": "sendToClient", "data": {"item": fullData}})
                            async_to_sync(layer)(dialogId, {"type": "sendGroupMessage", "channelName": dialogId, "data": messageDict})
                        else:
                            unreadMessagesCounter = {} # {userId: unreadMessagesLength} - для наращивания счётчика оповещений, если оповещение было скрыто
                            updatesStatusItems = updatesStatus.items()
                            for id, status in updatesStatusItems:
                                if status is True:
                                    messageDict.update({id: notifySimpleUpdate})
                                elif status is False:
                                    if len(unreadMessagesCounter.keys()) == 0:
                                        unreadMessagesDictItems = unreadMessagesDict.items()
                                        for messageId, messageRecipients in unreadMessagesDictItems:
                                            for id in messageRecipients:
                                                counter = unreadMessagesCounter.get(id, None)
                                                if counter is not None:
                                                    counter += 1
                                                    unreadMessagesCounter[id] = counter
                                                else:
                                                    unreadMessagesCounter[id] = 1
                                    messageDict.update({id: notifySpecialUpdate})
                                else: # None
                                    messageDict.update({id: notifyFullUpdate})
                            async_to_sync(get_channel_layer().group_send)(dialogId, {"type": "sendGroupMessage", "data": messageDict})
                    else:
                        layer = get_channel_layer().group_send
                        specialData = None
                        fullData = None
                        simpleData = None
                        unreadMessagesCounter = {} # {userId: unreadMessagesLength} - для наращивания счётчика оповещений, если оповещение было скрыто
                        updatesStatusItems = updatesStatus.items()
                        for id, status in updatesStatusItems:
                            if status is True:
                                if simpleData is None:
                                    simpleData = json.dumps(messageDict.copy().update(notifySimpleUpdate))
                                async_to_sync(layer)(id, {"type": "sendToClient", "data": {"item": simpleData}})
                            elif status is False:
                                if len(unreadMessagesCounter.keys()) == 0:
                                    unreadMessagesDictItems = unreadMessagesDict.items()
                                    for messageId, messageRecipients in unreadMessagesDictItems:
                                        for id in messageRecipients:
                                            counter = unreadMessagesCounter.get(id, None)
                                            if counter is not None:
                                                counter += 1
                                                unreadMessagesCounter[id] = counter
                                            else:
                                                unreadMessagesCounter[id] = 1
                                if specialData is None:
                                    specialData = json.dumps(messageDict.copy().update(notifySpecialUpdate))
                                async_to_sync(layer)(id, {"type": "sendToClient", "data": {"item": specialData}})
                            else: # None
                                if fullData is None:
                                    fullData = json.dumps(messageDict.copy().update(notifyFullUpdate))
                                async_to_sync(layer)(id, {"type": "sendToClient", "data": {"item": fullData}})
                    cookies = request.COOKIES
                    if cookies.get('js', None) is not None:
                        messages, unreadByUsersDict, usersIdsToGetNamesSet = loadDialogSimple(myId, dialogId, 0)
                        if len(usersIdsToGetNamesSet) > 0:
                            mainThumbsDict, smallThumbsDict, namesDictList, onlineStatusDict, activeStatusDict = getMainAndSmallThumbs((dialogUsersIds | usersIdsToGetNamesSet), requestUser, myId)
                            return render(request, "psixiSocial/psixiSocial.html", {
                                'parentTemplate': 'base/classicPage/base.html',
                                'content': ('psixiSocial', 'dialog', 'group', True),
                                'menu': cachedFragments().menuGetter('menuSerialized'),
                                'mainThumbsDict': mainThumbsDict,
                                'smallThumbsDict': smallThumbsDict,
                                'onlineStatusDict': onlineStatusDict,
                                'activeStatusDict': activeStatusDict,
                                'requestUserId': myId,
                                'messagesListDict': messages,
                                'unreadUsersIdsDictList': unreadByUsersDict,
                                'unreadUsersNamesDictList': namesDictList,
                                'csrf': checkCSRF(request, cookies),
                                'form': None,
                                'accessToSend': True,
                                'sendersIds': list(dialogUsersIds),
                                'dialogId': dialogId,
                                'time': datetime.now(tz = tz.gettz()).microsecond,
                                'dialogSlug': dialogAttributes.slug,
                                'imagesToSend': None
                                })
                        mainThumbsDict, smallThumbsDict, namesDictList, onlineStatusDict, activeStatusDict = getMainAndSmallThumbs(dialogUsersIds, requestUser, myId)
                        return render(request, "psixiSocial/psixiSocial.html", {
                            'parentTemplate': 'base/classicPage/base.html',
                            'content': ('psixiSocial', 'dialog', 'group', True),
                            'menu': cachedFragments().menuGetter('menuSerialized'),
                            'mainThumbsDict': mainThumbsDict,
                            'smallThumbsDict': smallThumbsDict,
                            'onlineStatusDict': onlineStatusDict,
                            'activeStatusDict': activeStatusDict,
                            'requestUserId': myId,
                            'messagesListDict': messages,
                            'unreadUsersIdsDictList': unreadByUsersDict,
                            'unreadUsersNamesDictList': namesDictList,
                            'csrf': checkCSRF(request, cookies),
                            'form': None,
                            'accessToSend': True,
                            'sendersIds': list(dialogUsersIds),
                            'dialogId': dialogId,
                            'time': datetime.now(tz = tz.gettz()).microsecond,
                            'dialogSlug': dialogAttributes.slug,
                            'imagesToSend': None
                            })
                    messages, images, unreadByUsersDict, usersIdsToGetNamesSet = loadDialog(myId, dialogId, 0)
                    mainThumbsDict, smallThumbsDict, namesDictList, onlineStatusDict, activeStatusDict = getMainAndSmallThumbs((dialogUsersIds | usersIdsToGetNamesSet), requestUser, myId)
                    return render(request, "psixiSocial/psixiSocial.html", {
                        'parentTemplate': 'base/classicPage/base.html',
                        'content': ('psixiSocial', 'dialog', 'group', False),
                        'menu': cachedFragments().menuGetter('menuSerialized'),
                        'mainThumbsDict': mainThumbsDict,
                        'smallThumbsDict': smallThumbsDict,
                        'onlineStatusDict': onlineStatusDict,
                        'activeStatusDict': activeStatusDict,
                        'requestUserId': myId,
                        'messagesListDict': None,
                        'messagesQuerySet': messages,
                        'imagesQuerySet': images,
                        'unreadUsersIdsDictList': unreadByUsersDict,
                        'unreadUsersNamesDictList': namesDictList,
                        'csrf': checkCSRF(request, cookies),
                        'form': None,
                        'accessToSend': True,
                        'sendersIds': list(dialogUsersIds),
                        'dialogId': dialogId,
                        'time': datetime.now(tz = tz.gettz()).microsecond,
                        'dialogSlug': dialogAttributes.slug,
                        'imagesToSend': None
                        })
                # Ошибка БД
                cookies = request.COOKIES
                if cookies.get('js', None) is not None:
                    messages, unreadByUsersDict, usersIdsToGetNamesSet = loadDialogSimple(myId, dialogId, 0)
                    if len(usersIdsToGetNamesSet) > 0:
                        mainThumbsDict, smallThumbsDict, namesDictList, onlineStatusDict, activeStatusDict = getMainAndSmallThumbs((dialogUsersIds | usersIdsToGetNamesSet), requestUser, myId)
                        return render(request, "psixiSocial/psixiSocial.html", {
                            'parentTemplate': 'base/classicPage/base.html',
                            'content': ('psixiSocial', 'dialog', 'group', True),
                            'menu': cachedFragments().menuGetter('menuSerialized'),
                            'mainThumbsDict': mainThumbsDict,
                            'smallThumbsDict': smallThumbsDict,
                            'onlineStatusDict': onlineStatusDict,
                            'activeStatusDict': activeStatusDict,
                            'requestUserId': myId,
                            'messagesListDict': messages,
                            'unreadUsersIdsDictList': unreadByUsersDict,
                            'unreadUsersNamesDictList': namesDictList,
                            'csrf': checkCSRF(request, cookies),
                            'form': None,
                            'accessToSend': True,
                            'sendersIds': list(dialogUsersIds),
                            'dialogId': dialogId,
                            'time': datetime.now(tz = tz.gettz()).microsecond,
                            'dialogSlug': dialogAttributes.slug,
                            'imagesToSend': cachedFilesDictDictImages
                            })
                    mainThumbsDict, smallThumbsDict, namesDictList, onlineStatusDict, activeStatusDict = getMainAndSmallThumbs(dialogUsersIds, requestUser, myId)
                    return render(request, "psixiSocial/psixiSocial.html", {
                        'parentTemplate': 'base/classicPage/base.html',
                        'content': ('psixiSocial', 'dialog', 'group', True),
                        'menu': cachedFragments().menuGetter('menuSerialized'),
                        'mainThumbsDict': mainThumbsDict,
                        'smallThumbsDict': smallThumbsDict,
                        'onlineStatusDict': onlineStatusDict,
                        'activeStatusDict': activeStatusDict,
                        'requestUserId': myId,
                        'messagesListDict': messages,
                        'unreadUsersIdsDictList': unreadByUsersDict,
                        'unreadUsersNamesDictList': namesDictList,
                        'csrf': checkCSRF(request, cookies),
                        'form': None,
                        'accessToSend': True,
                        'sendersIds': list(dialogUsersIds),
                        'dialogId': dialogId,
                        'time': datetime.now(tz = tz.gettz()).microsecond,
                        'dialogSlug': dialogAttributes.slug,
                        'imagesToSend': cachedFilesDictDictImages
                        })
                messages, images, unreadByUsersDict, usersIdsToGetNamesSet = loadDialog(myId, dialogId, 0)
                mainThumbsDict, smallThumbsDict, namesDictList, onlineStatusDict, activeStatusDict = getMainAndSmallThumbs((dialogUsersIds | usersIdsToGetNamesSet), requestUser, myId)
                return render(request, "psixiSocial/psixiSocial.html", {
                    'parentTemplate': 'base/classicPage/base.html',
                    'content': ('psixiSocial', 'dialog', 'group', False),
                    'menu': cachedFragments().menuGetter('menuSerialized'),
                    'mainThumbsDict': mainThumbsDict,
                    'smallThumbsDict': smallThumbsDict,
                    'onlineStatusDict': onlineStatusDict,
                    'activeStatusDict': activeStatusDict,
                    'requestUserId': myId,
                    'messagesListDict': None,
                    'messagesQuerySet': messages,
                    'imagesQuerySet': images,
                    'unreadUsersIdsDictList': unreadByUsersDict,
                    'unreadUsersNamesDictList': namesDictList,
                    'csrf': checkCSRF(request, cookies),
                    'form': None,
                    'accessToSend': True,
                    'sendersIds': list(dialogUsersIds),
                    'dialogId': dialogId,
                    'time': datetime.now(tz = tz.gettz()).microsecond,
                    'dialogSlug': dialogAttributes.slug,
                    'imagesToSend': cachedFilesDictDictImages
                    })
            cookies = request.COOKIES
            if cookies.get('js', None) is not None:
                messages, unreadByUsersDict, usersIdsToGetNamesSet = loadDialogSimple(myId, dialogId, 0)
                if len(usersIdsToGetNamesSet) > 0:
                    mainThumbsDict, smallThumbsDict, namesDictList, onlineStatusDict, activeStatusDict = getMainAndSmallThumbs((dialogUsersIds | usersIdsToGetNamesSet), requestUser, myId)
                    return render(request, "psixiSocial/psixiSocial.html", {
                        'parentTemplate': 'base/classicPage/base.html',
                        'content': ('psixiSocial', 'dialog', 'group', True),
                        'menu': cachedFragments().menuGetter('menuSerialized'),
                        'mainThumbsDict': mainThumbsDict,
                        'smallThumbsDict': smallThumbsDict,
                        'onlineStatusDict': onlineStatusDict,
                        'activeStatusDict': activeStatusDict,
                        'requestUserId': myId,
                        'messagesListDict': messages,
                        'unreadUsersIdsDictList': unreadByUsersDict,
                        'unreadUsersNamesDictList': namesDictList,
                        'csrf': checkCSRF(request, cookies),
                        'form': form,
                        'accessToSend': True,
                        'sendersIds': list(dialogUsersIds),
                        'dialogId': dialogId,
                        'time': datetime.now(tz = tz.gettz()).microsecond,
                        'dialogSlug': dialogAttributes.slug,
                        'imagesToSend': cachedFilesDictDictImages
                        })
                mainThumbsDict, smallThumbsDict, namesDictList, onlineStatusDict, activeStatusDict = getMainAndSmallThumbs(dialogUsersIds, requestUser, myId)
                return render(request, "psixiSocial/psixiSocial.html", {
                    'parentTemplate': 'base/classicPage/base.html',
                    'content': ('psixiSocial', 'dialog', 'group', True),
                    'menu': cachedFragments().menuGetter('menuSerialized'),
                    'mainThumbsDict': mainThumbsDict,
                    'smallThumbsDict': smallThumbsDict,
                    'onlineStatusDict': onlineStatusDict,
                    'activeStatusDict': activeStatusDict,
                    'requestUserId': myId,
                    'messagesListDict': messages,
                    'unreadUsersIdsDictList': unreadByUsersDict,
                    'unreadUsersNamesDictList': namesDictList,
                    'csrf': checkCSRF(request, cookies),
                    'form': form,
                    'accessToSend': True,
                    'sendersIds': list(dialogUsersIds),
                    'dialogId': dialogId,
                    'time': datetime.now(tz = tz.gettz()).microsecond,
                    'dialogSlug': dialogAttributes.slug,
                    'imagesToSend': cachedFilesDictDictImages
                    })
            messages, images, unreadByUsersDict, usersIdsToGetNamesSet = loadDialog(myId, dialogId, 0)
            mainThumbsDict, smallThumbsDict, namesDictList, onlineStatusDict, activeStatusDict = getMainAndSmallThumbs((dialogUsersIds | usersIdsToGetNamesSet), requestUser, myId)
            return render(request, "psixiSocial/psixiSocial.html", {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'dialog', 'group', False),
                'menu': cachedFragments().menuGetter('menuSerialized'),
                'mainThumbsDict': mainThumbsDict,
                'smallThumbsDict': smallThumbsDict,
                'onlineStatusDict': onlineStatusDict,
                'activeStatusDict': activeStatusDict,
                'requestUserId': myId,
                'messagesListDict': None,
                'messagesQuerySet': messages,
                'imagesQuerySet': images,
                'unreadUsersIdsDictList': unreadByUsersDict,
                'unreadUsersNamesDictList': namesDictList,
                'csrf': checkCSRF(request, cookies),
                'form': form,
                'accessToSend': True,
                'sendersIds': list(dialogUsersIds),
                'dialogId': dialogId,
                'time': datetime.now(tz = tz.gettz()).microsecond,
                'dialogSlug': dialogAttributes.slug,
                'imagesToSend': cachedFilesDictDictImages
                })
        # Пользователь не участник диалога

def loadDialog(requestUserId, dialogId, page):
    ''' Вывод множественного QuerySet сообщений с удалением id сообщения из кеша непрочитанных сообщений.
        return messagesQuerySet, 
            set(unreadMessageId, unreadMessageId),
            {unreadMessageId: [userId, userId, userId]}, 
            set(userId, userId, userId) - пользователи, которые не читали какие-то сообщения. '''
    allMessages = groupMessage.objects.filter(inDialog__exact = dialogId).filter(members__exact = requestUserId).order_by("time")
    if allMessages.exists():
        messagesPaginator = Paginator(allMessages, 10, orphans = 15)
        newPage = messagesPaginator.num_pages - page
        if newPage > 0:
            currentPage = messagesPaginator.page(newPage)
            messagesMany = currentPage.object_list.values("id", "by_id", "body", "time", "messageHead", "imagesAttachment_id")
            imageAttachmentIds = messagesMany.values("imagesAttachment_id")
            if imageAttachmentIds.count() > 0:
                imageAttachments = imagesAttachmentMessageGroup.objects.filter(message_id__in = imageAttachmentIds).values("message_id", "image1", "image2", "image3", "image4", "image5", "image6", "itemsLength")
            else:
                imageAttachments = None
            unreadMessagesCache = caches['unreadGroupMessages']
            unreadMessages = unreadMessagesCache.get(dialogId, None)
            if unreadMessages is not None:
                unreadMessagesDict = {}
                allUsersHasUnreadSet = set()
                toSendDict = {}
                changes = False
                for message in messagesMany:
                    id = str(message.get("id"))
                    unreadMessage = unreadMessages.get(id, None)
                    if unreadMessage is not None:
                        if requestUserId in unreadMessage:
                            unreadMessage.remove(requestUserId)
                            for userId in unreadMessage:
                                messagesSet = toSendDict.get(userId, None)
                                if messagesSet is not None:
                                    messagesSet.add(id)
                                    toSendDict.update({userId: messagesSet})
                                else:
                                    toSendDict.update({userId: {id,}})
                            if len(unreadMessage) > 0:
                                unreadMessages[id] = unreadMessage
                            else:
                                del unreadMessages[id]
                            if changes is False:
                                changes = True
                        allUsersHasUnreadSet.update(unreadMessage)
                        unreadMessagesDict.update({id: list(unreadMessage)})
                if changes is True:
                    for userId, messageIdsSet in toSendDict.items():
                        Group('personalMessage_%d' % userId).send({"text": json.dumps({
                            "personalMessageRead": tuple(messageIdsSet)
                            })})
                    if len(unreadMessages.keys()) > 0:
                        unreadMessagesCache.set(dialogId, unreadMessages)
                    else:
                        unreadMessagesCache.delete(dialogId)
                        updates = caches["newUpdates"]
                        myUpdates = updates.get(requestUserId, None)
                        if myUpdates is not None:
                            for update in myUpdates:
                                pass
                return messagesMany, imageAttachments, unreadMessagesDict, allUsersHasUnreadSet
            return messagesMany, imageAttachments, None, set()
        return None, None, None, set()
    return None, None, None, set()

def loadDialogSimple(requestUserId, dialogId, page):
    ''' Вывод множественного QuerySet сообщений (облегчённая версия).
        return messagesQuerySet, 
            set(unreadMessageId, unreadMessageId),
            {unreadMessageId: [userId, userId, userId]}, 
            set(userId, userId, userId) - пользователи, которые не читали какие-то сообщения. '''
    allMessages = groupMessage.objects.filter(inDialog__exact = dialogId).filter(members__exact = requestUserId).order_by("time")
    if allMessages.exists():
        messagesPaginator = Paginator(allMessages, 10, orphans = 15)
        newPage = messagesPaginator.num_pages - page
        if newPage > 0:
            currentPage = messagesPaginator.page(newPage)
            messagesMany = currentPage.object_list.values("id", "by_id", "body", "time", "messageHead", "imagesAttachment_id")
            imageAttachmentIds = messagesMany.values("imagesAttachment_id")
            unreadMessagesCache = caches['unreadGroupMessages']
            unreadMessages = unreadMessagesCache.get(dialogId, None)
            messagesList = []
            if unreadMessages is not None:
                allUsersHasUnreadSet = set()
                unreadMessagesDict = {}
                if imageAttachmentIds.count() > 0:
                    imageAttachmentMany = imagesAttachmentMessageGroup.objects.filter(message_id__in = imageAttachmentIds).values("message_id", "image1", "image2", "image3", "image4", "image5", "image6", "itemsLength")
                    for messageDict in messagesMany:
                        id = str(messageDict.get("id"))
                        unreadMessage = unreadMessages.get(id, None)
                        if unreadMessage is not None:
                            allUsersHasUnreadSet.update(unreadMessage)
                            unreadMessagesDict.update({id: list(unreadMessage)})
                        try:
                            imagesInstance = imageAttachmentMany.get(message_id__exact = id)
                        except imagesAttachmentMessageGroup.DoesNotExist:
                            imagesInstance = None
                        if imagesInstance is not None:
                            media = settings.MEDIA_URL
                            imagesLen = imagesInstance.get('itemsLength')
                            if imagesLen > 3:
                                if imagesLen == 4:
                                    imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2')), '{0}{1}'.format(media, imagesInstance.get('image3')), '{0}{1}'.format(media, imagesInstance.get('image4'))]
                                elif imagesLen == 5:
                                    imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2')), '{0}{1}'.format(media, imagesInstance.get('image3')), '{0}{1}'.format(media, imagesInstance.get('image4')), '{0}{1}'.format(media, imagesInstance.get('image5'))]
                                else:
                                    imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2')), '{0}{1}'.format(media, imagesInstance.get('image3')), '{0}{1}'.format(media, imagesInstance.get('image4')), '{0}{1}'.format(media, imagesInstance.get('image5')), '{0}{1}'.format(media, imagesInstance.get('image6'))]
                            else:
                                if imagesLen == 1:
                                    imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1'))]
                                elif imagesLen == 2:
                                    imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2'))]
                                else:
                                    imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2')), '{0}{1}'.format(media, imagesInstance.get('image3'))]
                            messagesList.append({
                                'id': id,
                                'head': int(messageDict.get('messageHead')),
                                'body': messageDict.get('body'),
                                'images': imagesList,
                                'by': messageDict.get('by_id'),
                                'time': messageDict.get('time').microsecond
                                })
                        else:
                            messagesList.append({
                                'id': id,
                                'head': int(messageDict.get('messageHead')),
                                'body': messageDict.get('body'),
                                'by': messageDict.get('by_id'),
                                'time': messageDict.get('time').microsecond
                                })
                    return messagesList, unreadMessagesDict, allUsersHasUnreadSet
                for messageDict in messagesMany:
                    id = str(messageDict.get("id"))
                    unreadMessage = unreadMessages.get(id, None)
                    if unreadMessage is not None:
                        allUsersHasUnreadSet.update(unreadMessage)
                        unreadMessagesDict.update({id: list(unreadMessage)})
                        messagesList.append({
                            'id': id,
                            'head': int(messageDict.get('messageHead')),
                            'body': messageDict.get('body'),
                            'by': messageDict.get('by_id'),
                            'time': messageDict.get('time').microsecond
                            })
                return messagesList, unreadMessagesDict, allUsersHasUnreadSet
            if imageAttachmentIds.count() > 0:
                imageAttachmentMany = imagesAttachmentMessageGroup.objects.filter(message_id__in = imageAttachmentIds).values("message_id", "image1", "image2", "image3", "image4", "image5", "image6", "itemsLength")
                for messageDict in messagesMany:
                    id = str(messageDict.get("id"))
                    try:
                        imagesInstance = imageAttachmentMany.get(message_id__exact = id)
                    except imagesAttachmentMessageGroup.DoesNotExist:
                        imagesInstance = None
                    if imagesInstance is not None:
                        media = settings.MEDIA_URL
                        imagesLen = imagesInstance.get('itemsLength')
                        if imagesLen > 3:
                            if imagesLen == 4:
                                imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2')), '{0}{1}'.format(media, imagesInstance.get('image3')), '{0}{1}'.format(media, imagesInstance.get('image4'))]
                            elif imagesLen == 5:
                                imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2')), '{0}{1}'.format(media, imagesInstance.get('image3')), '{0}{1}'.format(media, imagesInstance.get('image4')), '{0}{1}'.format(media, imagesInstance.get('image5'))]
                            else:
                                imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2')), '{0}{1}'.format(media, imagesInstance.get('image3')), '{0}{1}'.format(media, imagesInstance.get('image4')), '{0}{1}'.format(media, imagesInstance.get('image5')), '{0}{1}'.format(media, imagesInstance.get('image6'))]
                        else:
                            if imagesLen == 1:
                                imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1'))]
                            elif imagesLen == 2:
                                imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2'))]
                            else:
                                imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2')), '{0}{1}'.format(media, imagesInstance.get('image3'))]
                        messagesList.append({
                            'id': id,
                            'head': int(messageDict.get('messageHead')),
                            'body': messageDict.get('body'),
                            'images': imagesList,
                            'by': messageDict.get('by_id'),
                            'time': messageDict.get('time').microsecond
                            })
                    else:
                        messagesList.append({
                            'id': id,
                            'head': int(messageDict.get('messageHead')),
                            'body': messageDict.get('body'),
                            'by': messageDict.get('by_id'),
                            'time': messageDict.get('time').microsecond
                            })
                return messagesList, None, set()
            for messageDict in messagesMany:
                messagesList.append({
                    'id': str(messageDict.get('id')),
                    'head': int(messageDict.get('messageHead')),
                    'body': messageDict.get('body'),
                    'by': messageDict.get('by_id'),
                    'time': messageDict.get('time').microsecond
                    })
            return messagesList, None, set()
        return [], None, set()
    return [], None, set()

def loadDialogSimpleShort(requestUserId, dialogId, page):
    ''' Вывод множественного QuerySet сообщений (облегчённая версия 2 - только для подгрузки).
        return messagesQuerySet, 
            set(unreadMessageId, unreadMessageId),
            {unreadMessageId: [userId, userId, userId]}, 
            set(userId, userId, userId) - пользователи, которые не читали какие-то сообщения. '''
    allMessages = groupMessage.objects.filter(inDialog__exact = dialogId).filter(members__exact = requestUserId).order_by("time")
    if allMessages.exists():
        messagesPaginator = Paginator(allMessages, 10, orphans = 15)
        newPage = messagesPaginator.num_pages - page
        if newPage > 0:
            currentPage = messagesPaginator.page(newPage)
            messagesMany = currentPage.object_list.values("id", "by_id", "body", "time", "imagesAttachment_id")
            imageAttachmentIds = messagesMany.values("imagesAttachment_id")
            unreadMessagesCache = caches['unreadGroupMessages']
            unreadMessages = unreadMessagesCache.get(dialogId, None)
            messagesList = []
            if imageAttachmentIds.count() > 0:
                imageAttachmentMany = imagesAttachmentMessageGroup.objects.filter(message_id__in = imageAttachmentIds).values("message_id", "image1", "image2", "image3", "image4", "image5", "image6", "itemsLength")
                if unreadMessages is not None:
                    unreadMessagesDict = {}
                    for messageDict in messagesMany:
                        id = str(messageDict.get("id"))
                        unreadMessage = unreadMessages.get(id, None)
                        if unreadMessage is not None:
                            unreadMessagesDict.update({id: list(unreadMessage)})
                        try:
                            imagesInstance = imageAttachmentMany.get(message_id__exact = id)
                        except imagesAttachmentMessageGroup.DoesNotExist:
                            imagesInstance = None
                        if imagesInstance is not None:
                            media = settings.MEDIA_URL
                            imagesLen = imagesInstance.get('itemsLength')
                            if imagesLen > 3:
                                if imagesLen == 4:
                                    imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2')), '{0}{1}'.format(media, imagesInstance.get('image3')), '{0}{1}'.format(media, imagesInstance.get('image4'))]
                                elif imagesLen == 5:
                                    imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2')), '{0}{1}'.format(media, imagesInstance.get('image3')), '{0}{1}'.format(media, imagesInstance.get('image4')), '{0}{1}'.format(media, imagesInstance.get('image5'))]
                                else:
                                    imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2')), '{0}{1}'.format(media, imagesInstance.get('image3')), '{0}{1}'.format(media, imagesInstance.get('image4')), '{0}{1}'.format(media, imagesInstance.get('image5')), '{0}{1}'.format(media, imagesInstance.get('image6'))]
                            else:
                                if imagesLen == 1:
                                    imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1'))]
                                elif imagesLen == 2:
                                    imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2'))]
                                else:
                                    imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2')), '{0}{1}'.format(media, imagesInstance.get('image3'))]
                            messagesList.append({
                                'id': id,
                                'body': messageDict.get('body'),
                                'images': imagesList,
                                'by': messageDict.get('by_id'),
                                'time': messageDict.get('time').microsecond
                                })
                        else:
                            messagesList.append({
                                'id': id,
                                'body': messageDict.get('body'),
                                'by': messageDict.get('by_id'),
                                'time': messageDict.get('time').microsecond
                                })
                    return messagesList, unreadMessagesDict
            if unreadMessages is not None:
                unreadMessagesDict = {}
                for messageDict in messagesMany:
                    id = str(messageDict.get("id"))
                    unreadMessage = unreadMessages.get(id, None)
                    if unreadMessage is not None:
                        unreadMessagesDict.update({id: list(unreadMessage)})
                    try:
                        imagesInstance = imageAttachmentMany.get(message_id__exact = id)
                    except imagesAttachmentMessageGroup.DoesNotExist:
                        imagesInstance = None
                    if imagesInstance is not None:
                        media = settings.MEDIA_URL
                        imagesLen = imagesInstance.get('itemsLength')
                        if imagesLen > 3:
                            if imagesLen == 4:
                                imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2')), '{0}{1}'.format(media, imagesInstance.get('image3')), '{0}{1}'.format(media, imagesInstance.get('image4'))]
                            elif imagesLen == 5:
                                imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2')), '{0}{1}'.format(media, imagesInstance.get('image3')), '{0}{1}'.format(media, imagesInstance.get('image4')), '{0}{1}'.format(media, imagesInstance.get('image5'))]
                            else:
                                imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2')), '{0}{1}'.format(media, imagesInstance.get('image3')), '{0}{1}'.format(media, imagesInstance.get('image4')), '{0}{1}'.format(media, imagesInstance.get('image5')), '{0}{1}'.format(media, imagesInstance.get('image6'))]
                        else:
                            if imagesLen == 1:
                                imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1'))]
                            elif imagesLen == 2:
                                imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2'))]
                            else:
                                imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2')), '{0}{1}'.format(media, imagesInstance.get('image3'))]
                        messagesList.append({
                            'id': id,
                            'body': messageDict.get('body'),
                            'images': imagesList,
                            'by': messageDict.get('by_id'),
                            'time': messageDict.get('time').microsecond
                            })
                    else:
                        messagesList.append({
                            'id': id,
                            'body': messageDict.get('body'),
                            'by': messageDict.get('by_id'),
                            'time': messageDict.get('time').microsecond
                            })
                return messagesList, unreadMessagesDict
            if imageAttachmentIds.count() > 0:
                imageAttachmentMany = imagesAttachmentMessageGroup.objects.filter(message_id__in = imageAttachmentIds).values("message_id", "image1", "image2", "image3", "image4", "image5", "image6", "itemsLength")
                for messageDict in messagesMany:
                    id = str(messageDict.get("id"))
                    try:
                        imagesInstance = imageAttachmentMany.get(message_id__exact = id)
                    except imagesAttachmentMessageGroup.DoesNotExist:
                        imagesInstance = None
                    if imagesInstance is not None:
                        media = settings.MEDIA_URL
                        imagesLen = imagesInstance.get('itemsLength')
                        if imagesLen > 3:
                            if imagesLen == 4:
                                imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2')), '{0}{1}'.format(media, imagesInstance.get('image3')), '{0}{1}'.format(media, imagesInstance.get('image4'))]
                            elif imagesLen == 5:
                                imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2')), '{0}{1}'.format(media, imagesInstance.get('image3')), '{0}{1}'.format(media, imagesInstance.get('image4')), '{0}{1}'.format(media, imagesInstance.get('image5'))]
                            else:
                                imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2')), '{0}{1}'.format(media, imagesInstance.get('image3')), '{0}{1}'.format(media, imagesInstance.get('image4')), '{0}{1}'.format(media, imagesInstance.get('image5')), '{0}{1}'.format(media, imagesInstance.get('image6'))]
                        else:
                            if imagesLen == 1:
                                imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1'))]
                            elif imagesLen == 2:
                                imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2'))]
                            else:
                                imagesList = ['{0}{1}'.format(media, imagesInstance.get('image1')), '{0}{1}'.format(media, imagesInstance.get('image2')), '{0}{1}'.format(media, imagesInstance.get('image3'))]
                        messagesList.append({
                            'id': id,
                            'body': messageDict.get('body'),
                            'images': imagesList,
                            'by': messageDict.get('by_id'),
                            'time': messageDict.get('time').microsecond
                            })
                    else:
                        messagesList.append({
                            'id': id,
                            'body': messageDict.get('body'),
                            'by': messageDict.get('by_id'),
                            'time': messageDict.get('time').microsecond
                            })
                return messagesList, None
            for messageDict in messagesMany:
                messagesList.append({
                    'id': str(messageDict.get('id')),
                    'body': messageDict.get('body'),
                    'by': messageDict.get('by_id'),
                    'time': messageDict.get('time').microsecond
                    })
            return messagesList, None
        return None, None
    return None, None

def filesLoaderGroup(request, dialogSlugName, type, **pars):
    if request.is_ajax() and request.method == "POST":
        dialogsCache = caches['dialogGroup']
        cachedDialog = dialogsCache.get(dialogSlugName, None)
        if cachedDialog is None:
            try:
                dialogAttributes = groupDialogAttributes.objects.get(slug__exact = dialogSlugName)
            except groupDialogAttributes.DoesNotExist:
                dialogAttributes = None
            if dialogAttributes is not None:
                dialog = dialogAttributes.dialog
                dialogUsers = dialog.users.all()
                dialogUsersIdsQuerySet = dialogUsers.values("id")
                dialogUsersIds = {str(user.get("id")) for user in dialogUsersIdsQuerySet}
                dialogsCache.set(dialogSlugName, {'attributes': dialogAttributes, 'dialog': dialog, 'users': dialogUsers, 'usersIds': dialogUsersIds})
        else:
            dialogUsers = cachedDialog.get('users', None)
            dialogUsersIds = cachedDialog.get('usersIds', None)
            dialog = cachedDialog.get('dialog', None)
            dialogAttributes = cachedDialog.get('attributes', None)
            if dialogUsers is None or dialogUsersIds is None or dialog is None or dialogAttributes is None:
                if dialogAttributes is None:
                    try:
                        dialogAttributes = groupDialogAttributes.objects.get(slug__exact = dialogSlugName)
                    except groupDialogAttributes.DoesNotExist:
                        dialogAttributes = None
                    if dialogAttributes is not None:
                        cachedDialog.update({'attributes': dialogAttributes})
                        if dialog is None:
                            dialog = dialogAttributes.dialog
                            cachedDialog.update({'dialog': dialog})
                        if dialogUsers is None:
                            dialogUsers = dialog.users.all()
                            dialogUsersIdsQuerySet = dialogUsers.values("id")
                            dialogUsersIds = {str(user.get("id")) for user in dialogUsersIdsQuerySet}
                            cachedDialog.update({'users': dialogUsers, 'usersIds': dialogUsersIds})
                        else:
                            if dialogUsersIds is None:
                                dialogUsersIdsQuerySet = dialogUsers.values("id")
                                dialogUsersIds = {str(user.get("id")) for user in dialogUsersIdsQuerySet}
                                cachedDialog.update({'usersIds': dialogUsersIds})
                        dialogsCache.set(dialogSlugName, cachedDialog)
        curUser = request.user
        myId = str(curUser.id)
        if myId in dialogUsersIds:
            file = request.FILES.get('insertion', None)
            if file is None:
                return JsonResponse({"loadFiles": None})
            if type == "image":
                if file.size > 10242880:
                    return JsonResponse({"loadFiles": "tooBig"})
                fileValidatorInstance = FileExtensionValidator(['jpeg', 'jpg', 'gif', 'png'])
                try:
                    fileValidatorInstance(file)
                except ValidationError as error:
                    return JsonResponse({"loadFiles": '. '.join(error.messages)})
                try:
                    validate_image_file_extension(file)
                except ValidationError as error:
                    '{0}. {1}'.format(error, '. '.join(error.messages))
                messagesFilesStorage = caches['filesStorageMessages']
                dialogId = str(dialog.id)
                removeFiles = pars.pop('remove', None)
                cachedFilesDict = messagesFilesStorage.get(dialogId, None)
                if removeFiles is not None:
                    if cachedFilesDict is None:
                        return JsonResponse({"removed": None})
                    else:
                        cachedFilesDictDict = cachedFilesDict.get(myId, None)
                        if cachedFilesDictDict is None:
                            return JsonResponse({"removed": None})
                        else:
                            cachedFilesDictDictImages = cachedFilesDictDict.get("images", None)
                            if cachedFilesDictDictImages is None:
                                return JsonResponse({"removed": None})
                            else:
                                removedNames = set()
                                for name in removeFiles:
                                    removed = cachedFilesDictDict.pop(name, None)
                                    if removed is not None:
                                        removedNames.add(removed)
                                if len(removedNames) > 0:
                                    if len(cachedFilesDictDict) == 0:
                                        del cachedFilesDict[myId]
                                    messagesFilesStorage.set(dialogId, cachedFilesDict)
                                    return JsonResponse({"removed": removedNames})
                name = file.name
                dotIndex = name.index(".")
                encodedImage = base64.b64encode(file.read()).decode("utf-8")
                if cachedFilesDict is None:
                    messagesFilesStorage.set(dialogId, {myId: {"images": {name: (encodedImage, name[:dotIndex], name[dotIndex + 1:])}}})
                else:
                    cachedFilesDictDict = cachedFilesDict.get(myId, None)
                    if cachedFilesDictDict is None:
                        cachedFilesDict.update({myId: {"images": {name: (encodedImage, name[:dotIndex], name[dotIndex + 1:])}}})
                    else:
                        cachedFilesDictDictImages = cachedFilesDictDict.get("images", None)
                        if cachedFilesDictDictImages is None:
                            cachedFilesDictDict.update({"images": {name: (encodedImage, name[:dotIndex], name[dotIndex + 1:])}})
                        else:
                            cachedFilesDictDictImages.update({name: (encodedImage, name[:dotIndex], name[dotIndex + 1:])})
                    messagesFilesStorage.set(dialogId, cachedFilesDict)
                Group("service_%d" % myId).send({"text": json.dumps({
                    'method': 'dialogAttachment',
                    'image': {name: encodedImage},
                    'byId': myId
                    })})
                return JsonResponse({"loadFiles": True})
        return JsonResponse({"loadFiles": False})

