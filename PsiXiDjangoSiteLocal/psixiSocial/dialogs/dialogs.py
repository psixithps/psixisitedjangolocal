import json
from functools import lru_cache
from datetime import datetime
from dateutil import tz
from django.core.cache import caches
from collections import OrderedDict
from django.core.paginator import Paginator, EmptyPage
from django.db.models import Q
from psixiSocial.models import dialogMessage, personalMessage, mainUserProfile, userProfilePrivacy, groupMessage
from psixiSocial.privacyIdentifier import checkPrivacy
from psixiSocial.login import login_decorator
from django.http import JsonResponse
from django.shortcuts import render, reverse
from django.template.loader import render_to_string as rts
from psixiSocial.translators import usersListText

def getSmallDialogThumbsSimple(ids, requestUser, requestUserId):
    ''' Вывод 'dialogListThumbs' в упрощённом виде, без онлайн статуса '''
    smallDialogThumbsCache = caches['dialogListThumbs']
    smallDialogThumbsMany = smallDialogThumbsCache.get_many(ids)
    smallDialogThumbsManyKeys = set(smallDialogThumbsMany.keys())
    thumbsSurplus = ids - smallDialogThumbsManyKeys
    thumbsSurplus1 = set()
    for id in smallDialogThumbsManyKeys:
        thumb = smallDialogThumbsMany[id].get(requestUserId, None)
        if thumb is None:
            thumbsSurplus1.add(id)
    thumbsSurplus = thumbsSurplus | thumbsSurplus1
    if len(thumbsSurplus) > 0:
        privacyCache = caches['privacyCache']
        privacyDictMany = privacyCache.get_many(thumbsSurplus)
        privacyManyKeys = set(privacyDictMany.keys())
        privacySurplus = thumbsSurplus - privacyManyKeys
        privacySurplus1 = set()
        for id in privacyManyKeys:
            privacyDict = privacyDictMany[id]
            if requestUserId not in privacyDict:
                privacySurplus1.add(id)
            else:
                if 'viewMainInfo' or 'viewTHPSInfo' or 'viewAvatar' not in privacyDict.keys():
                    privacySurplus1.add(id)
        privacySurplus = privacySurplus | privacySurplus1
        if len(privacySurplus) > 0:
            users = mainUserProfile.objects.filter(id__in = privacySurplus)
            privacySettings = userProfilePrivacy.objects.filter(user__in = users)
            friendsIdsCache = caches['friendsIdCache']
            friendsIds = friendsIdsCache.get(requestUserId, None)
            if friendsIds is None:
                friendsCache = caches['friendsCache']
                friends = friendsCache.get(requestUserId, None)
                if friends is None:
                    friends = requestUser.friends.all()
                    if friends.count():
                        friendsCache.set(requestUserId, friends)
                        friendsIdsQuerySet = friends.values("id")
                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                    else:
                        friendsCache.set(requestUserId, False)
                        friendsIds = set()
                else:
                    if friends is False:
                        friendsIds = set()
                    else:
                        friendsIds = {friend.id for friend in friends}
                friendsIdsCache.set(requestUserId, friendsIds)
            if len(friendsIds) > 0:
                friendsShipSurplus = privacySurplus - friendsIds
                if len(friendsShipSurplus) > 0:
                    friendShipCache = caches['friendShipStatusCache']
                    friendShipDict = friendShipCache.get(requestUserId, {})
                    friendShipManyKeys = set(friendShipDict.keys())
                    friendShipRequestsSurplus = friendsShipSurplus - friendShipManyKeys
                    if len(friendShipRequestsSurplus) > 0:
                        friendShipRequests = caches['friendshipRequests'].get_many(friendShipRequestsSurplus)
            else:
                if len(privacySurplus) > 0:
                    friendShipCache = caches['friendShipStatusCache']
                    friendShipDict = friendShipCache.get(requestUserId, {})
                    friendShipManyKeys = set(friendShipDict.keys())
                    friendShipRequestsSurplus = privacySurplus - friendShipManyKeys
                    if len(friendShipRequestsSurplus) > 0:
                        friendShipRequests = caches['friendshipRequests'].get_many(friendShipRequestsSurplus)
            privacyToSet = {}
            friendShipToSet = {}
    thumbItemsToSet = {}
    thumbsResult = {}
    for id in ids:
        thumbs = smallDialogThumbsMany.get(id, None)
        if thumbs is None:
            user = users.get(id__exact = id)
            privacyDict = privacyDictMany.get(id, None)
            if privacyDict is None:
                if id in friendsIds:
                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                else:
                    friendShipStatus = friendShipDict.get(id, None)
                    if friendShipStatus is None:
                        if friendShipRequests is None:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            friendShipToSet.update({requestUserId: {id: False}})
                        else:
                            friendShipRequest = friendShipRequests.get(id, None)
                            if friendShipRequest is None:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                friendShipToSet.update({requestUserId: {id: False}})
                            elif friendShipRequest == "request":
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                friendShipToSet.update({requestUserId: {id: "request"}})
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                friendShipToSet.update({requestUserId: {id: "myRequest"}})
                    elif friendShipStatus is False:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                    elif friendShipStatus == "request":
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                    else:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                viewAvatar = privacyChecker.checkAvatarPrivacy()
                privacyToSet.update({id: {requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar}}})
            else:
                userPrivacyDict = privacyDict.get(requestUserId, None)
                if userPrivacyDict is None:
                    if id in friendsIds:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                    else:
                        friendShipStatus = friendShipDict.get(id, None)
                        if friendShipStatus is None:
                            if friendShipRequests is None:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                friendShipToSet.update({requestUserId: {id: False}})
                            else:
                                friendShipRequest = friendShipRequests.get(id, None)
                                if friendShipRequest is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                elif friendShipRequest == "request":
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: "request"}})
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: "myRequest"}})
                        elif friendShipStatus is False:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        elif friendShipStatus == "request":
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    privacyDict.update({requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar}})
                    privacyToSet.update({id: privacyDict})
                else:
                    viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                    viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                    viewAvatar = userPrivacyDict.get('viewAvatar', None)
                    if viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            friendShipStatus = friendShipDict.get(id, None)
                            if friendShipStatus is None:
                                if friendShipRequests is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                else:
                                    friendShipRequest = friendShipRequests.get(id, None)
                                    if friendShipRequest is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    elif friendShipRequest == "request":
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "request"}})
                                    else:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "myRequest"}})
                            elif friendShipStatus is False:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            elif friendShipStatus == "request":
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        if viewMainInfo is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                        if viewTHPSInfo is None:
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                        if viewAvatar is None:
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            userPrivacyDict.update({'viewAvatar': viewAvatar})
                        privacyToSet.update({id: privacyDict})
            if viewMainInfo:
                firstName = user.first_name or None
                lastName = user.last_name or None
                if firstName is not None and lastName is not None:
                    name = '{0} {1}'.format(firstName, lastName)
                    smallName = user.username
                else:
                    if firstName is not None:
                        name = firstName
                        smallName = user.username
                    else:
                        name = user.username
                        smallName = None
            else:
                name = user.username
                smallName = None
            if viewAvatar:
                avatar = user.avatarThumbSmaller or None
                if avatar is not None:
                    if smallName is not None:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><p><a href = "{1}">{3}</a></p><small>{4}</small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, smallName)
                    else:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><p><a href = "{1}">{3}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                else:
                    if smallName is not None:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2} [{3}]</p></div></a><p><a href = "{1}">{2}</a></p><small></small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name, smallName)
                    else:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2}</p></div></a><p><a href = "{1}">{2}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
            else:
                if smallName is not None:
                    listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2} [{3}]</p></div></a><p><a href = "{1}">{2}</a></p><small></small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name, smallName)
                else:
                    listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2}</p></div></a><p><a href = "{1}">{2}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
            thumbsResult.update({id: listThumbItem})
            thumbItemsToSet.update({id: {requestUserId: {'item': listThumbItem}}})
        else:
            thumb = thumbs.get(requestUserId, None)
            if thumb is None:
                user = users.get(id__exact = id)
                privacyDict = privacyDictMany.get(id, None)
                if privacyDict is None:
                    if id in friendsIds:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                    else:
                        friendShipStatus = friendShipDict.get(id, None)
                        if friendShipStatus is None:
                            if friendShipRequests is None:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                friendShipToSet.update({requestUserId: {id: False}})
                            else:
                                friendShipRequest = friendShipRequests.get(id, None)
                                if friendShipRequest is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                elif friendShipRequest == "request":
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: "request"}})
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: "myRequest"}})
                        elif friendShipStatus is False:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        elif friendShipStatus == "request":
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    privacyToSet.update({id: {requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar}}})
                else:
                    userPrivacyDict = privacyDict.get(requestUserId, None)
                    if userPrivacyDict is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            friendShipStatus = friendShipDict.get(id, None)
                            if friendShipStatus is None:
                                if friendShipRequests is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                else:
                                    friendShipRequest = friendShipRequests.get(id, None)
                                    if friendShipRequest is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    elif friendShipRequest == "request":
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "request"}})
                                    else:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "myRequest"}})
                            elif friendShipStatus is False:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            elif friendShipStatus == "request":
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        privacyDict.update({requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar}})
                        privacyToSet.update({id: privacyDict})
                    else:
                        viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                        viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                        viewAvatar = userPrivacyDict.get('viewAvatar', None)
                        if viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                friendShipStatus = friendShipDict.get(id, None)
                                if friendShipStatus is None:
                                    if friendShipRequests is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    else:
                                        friendShipRequest = friendShipRequests.get(id, None)
                                        if friendShipRequest is None:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: False}})
                                        elif friendShipRequest == "request":
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: "request"}})
                                        else:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: "myRequest"}})
                                elif friendShipStatus is False:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                elif friendShipStatus == "request":
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            if viewMainInfo is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                            if viewTHPSInfo is None:
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                            if viewAvatar is None:
                                viewAvatar = privacyChecker.checkAvatarPrivacy()
                                userPrivacyDict.update({'viewAvatar': viewAvatar})
                            privacyToSet.update({id: privacyDict})
                if viewMainInfo:
                    firstName = user.first_name or None
                    lastName = user.last_name or None
                    if firstName is not None and lastName is not None:
                        name = '{0} {1}'.format(firstName, lastName)
                        smallName = user.username
                    else:
                        if firstName is not None:
                            name = firstName
                            smallName = user.username
                        else:
                            name = user.username
                            smallName = None
                else:
                    name = user.username
                    smallName = None
                if viewAvatar:
                    avatar = user.avatarThumbSmaller or None
                    if avatar is not None:
                        if smallName is not None:
                            listThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><p><a href = "{1}">{3}</a></p><small>{4}</small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, smallName)
                        else:
                            listThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><p><a href = "{1}">{3}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                    else:
                        if smallName is not None:
                            listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2} [{3}]</p></div></a><p><a href = "{1}">{2}</a></p><small></small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name, smallName)
                        else:
                            listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2}</p></div></a><p><a href = "{1}">{2}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                else:
                    if smallName is not None:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2} [{3}]</p></div></a><p><a href = "{1}">{2}</a></p><small></small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name, smallName)
                    else:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2}</p></div></a><p><a href = "{1}">{2}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                thumbsResult.update({id: listThumbItem})
                thumbs.update({requestUserId: {'item': listThumbItem}})
                thumbItemsToSet.update({id: thumbs})
            else:
                thumbsResult.update({id: thumb['item']})
    if len(thumbItemsToSet.keys()) > 0:
        smallDialogThumbsCache.set_many(thumbItemsToSet)
        if len(privacyToSet.keys()) > 0:
            privacyCache.set_many(privacyToSet)
            if len(friendShipToSet.keys()) > 0:
                friendShipCache.set_many(friendShipToSet)
    return thumbsResult

def getSmallDialogThumbs(idsSet, requestUser, requestUserId):
    onlineUsers = set(caches['onlineUsers'].get_many(idsSet).keys())
    iffyTimeMany = caches['iffyUsersOnlineStatus'].get_many((idsSet - onlineUsers))
    activeUsers = set(caches['chatStatusActive'].get_many((onlineUsers | set(iffyTimeMany.keys()))).keys())
    chatStatusMiddleMany = caches['chatStatusMiddle'].get_many((idsSet - activeUsers))
    smallDialogThumbsCache = caches['dialogListThumbs']
    smallDialogThumbsMany = smallDialogThumbsCache.get_many(idsSet)
    smallDialogThumbsManyKeys = set(smallDialogThumbsMany.keys())
    thumbsSurplus = idsSet - smallDialogThumbsManyKeys
    for id, thumbsDict in smallDialogThumbsMany.items():
        thumb = thumbsDict.get(requestUserId, None)
        if thumb is None:
            thumbsSurplus.add(id)
        else:
            keys = thumb.keys()
            if 'showOnlineStatus' not in keys or 'showActiveStatus' not in keys:
                thumbsSurplus.add(id)
    isThumbsSurplus = len(thumbsSurplus) > 0
    if isThumbsSurplus:
        users = mainUserProfile.objects.filter(id__in = thumbsSurplus)
        privacyCache = caches['privacyCache']
        privacyDictMany = privacyCache.get_many(thumbsSurplus)
        privacySurplus = thumbsSurplus - set(privacyDictMany.keys())
        for id, privacyDict in privacyDictMany.items():
            myPrivacyDict = privacyDict.get(requestUserId, None)
            if myPrivacyDict is None:
                privacySurplus.add(id)
            else:
                keys =  myPrivacyDict.keys()
                if 'viewMainInfo' not in keys or 'viewTHPSInfo' not in keys or 'viewAvatar' not in keys or 'viewOnlineStatus' not in keys or 'viewActiveStatus' not in keys:
                    privacySurplus.add(id)
        isPrivacySurplus = len(privacySurplus) > 0
        if isPrivacySurplus:
            privacySettings = userProfilePrivacy.objects.filter(user__in = users)
            friendsIdsCache = caches['friendsIdCache']
            friendsIds = friendsIdsCache.get(requestUserId, None)
            if friendsIds is None:
                friendsCache = caches['friendsCache']
                friends = friendsCache.get(requestUserId, None)
                if friends is None:
                    friends = requestUser.friends.all()
                    if friends.count():
                        friendsCache.set(requestUserId, friends)
                        friendsIdsQuerySet = friends.values("id")
                        friendsIds = {user.get("id") for user in friends.values("id")}
                    else:
                        friendsCache.set(requestUserId, False)
                        friendsIds = set()
                else:
                    if friends is False:
                        friendsIds = set()
                    else:
                        friendsIdsQuerySet = friends.values("id")
                        friendsIds = {friend.get("id") for friend in friendsIdsQuerySet}
                friendsIdsCache.set(requestUserId, friendsIds)
            friendShipSurplus = privacySurplus - friendsIds
            isFriendShipSurplus = len(friendShipSurplus) > 0
            if isFriendShipSurplus:
                friendShipCache = caches['friendShipStatusCache']
                friendShipMany = friendShipCache.get_many(friendShipSurplus)
                friendShipRequestsSurplus = friendShipSurplus - set(friendShipMany.keys())
                for id, friendShipDict in friendShipMany.items():
                    if requestUserId not in friendShipDict.keys():
                        friendShipRequestsSurplus.add(id)
                isFriendShipRequestsSurplus = len(friendShipRequestsSurplus) > 0
                if isFriendShipRequestsSurplus:
                    friendShipRequestsSurplus.add(requestUserId)
                    friendShipRequestsMany = caches['friendshipRequests'].get_many(friendShipRequestsSurplus)
                    friendShipToSet = {}
            privacyToSet = {}
        thumbItemsToSet = {}
    thumbsResult = {}
    onlineStatusDict = {}
    activeStatusDict = {}
    for id in idsSet:
        thumbs = smallDialogThumbsMany.get(id, None)
        if thumbs is None:
            user = users.get(id__exact = id)
            privacyDict = privacyDictMany.get(id, None)
            if privacyDict is None:
                if id in friendsIds:
                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                else:
                    friendShipStatusDict = friendShipMany.get(id, None)
                    if friendShipStatusDict is None:
                        friendShipStatus = False
                        friendShipRequestsListList = friendShipRequestsMany.pop(requestUserId, None)
                        if friendShipRequestsListList is not None:
                            for friendShipRequestsList in friendShipRequestsListList:
                                if friendShipRequestsList[0] == id:
                                    friendShipStatus = "request"
                        if friendShipStatus is False:
                            friendShipRequestsListList = friendShipRequestsMany.pop(id, None)
                            if friendShipRequestsListList is not None:
                                for friendShipRequestsList in friendShipRequestsListList:
                                    if friendShipRequestsList[0] == requestUserId:
                                        friendShipStatus = "myRequest"
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                        friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                    else:
                        friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                        if friendShipStatus is None:
                            friendShipStatus = False
                            friendShipRequestsListList = friendShipRequestsMany.pop(requestUserId, None)
                            if friendShipRequestsListList is not None:
                                for friendShipRequestsList in friendShipRequestsListList:
                                    if friendShipRequestsList[0] == id:
                                        friendShipStatus = "request"
                            if friendShipStatus is False:
                                friendShipRequestsListList = friendShipRequestsMany.pop(id, None)
                                if friendShipRequestsListList is not None:
                                    for friendShipRequestsList in friendShipRequestsListList:
                                        if friendShipRequestsList[0] == requestUserId:
                                            friendShipStatus = "myRequest"
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                            friendShipStatusDict.update({requestUserId: friendShipStatus})
                            friendShipToSet.update({id: friendShipStatusDict})
                        else:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                viewAvatar = privacyChecker.checkAvatarPrivacy()
                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                privacyToSet.update({id: {requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
            else:
                userPrivacyDict = privacyDict.get(requestUserId, None)
                if userPrivacyDict is None:
                    if id in friendsIds:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                    else:
                        friendShipStatusDict = friendShipMany.get(id, None)
                        if friendShipStatusDict is None:
                            friendShipStatus = False
                            friendShipRequestsListList = friendShipRequestsMany.pop(requestUserId, None)
                            if friendShipRequestsListList is not None:
                                for friendShipRequestsList in friendShipRequestsListList:
                                    if friendShipRequestsList[0] == id:
                                        friendShipStatus = "request"
                            if friendShipStatus is False:
                                friendShipRequestsListList = friendShipRequestsMany.pop(id, None)
                                if friendShipRequestsListList is not None:
                                    for friendShipRequestsList in friendShipRequestsListList:
                                        if friendShipRequestsList[0] == requestUserId:
                                            friendShipStatus = "myRequest"
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                            friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                        else:
                            friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                            if friendShipStatus is None:
                                friendShipStatus = False
                                friendShipRequestsListList = friendShipRequestsMany.pop(requestUserId, None)
                                if friendShipRequestsListList is not None:
                                    for friendShipRequestsList in friendShipRequestsListList:
                                        if friendShipRequestsList[0] == id:
                                            friendShipStatus = "request"
                                if friendShipStatus is False:
                                    friendShipRequestsListList = friendShipRequestsMany.pop(id, None)
                                    if friendShipRequestsListList is not None:
                                        for friendShipRequestsList in friendShipRequestsListList:
                                            if friendShipRequestsList[0] == requestUserId:
                                                friendShipStatus = "myRequest"
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                                friendShipStatusDict.update({requestUserId: friendShipStatus})
                                friendShipToSet.update({id: friendShipStatusDict})
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                    privacyDict.update({requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                    privacyToSet.update({id: privacyDict})
                else:
                    viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                    viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                    viewAvatar = userPrivacyDict.get('viewAvatar', None)
                    viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                    viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                    if viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None or viewActiveStatus is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            friendShipStatusDict = friendShipMany.get(id, None)
                            if friendShipStatusDict is None:
                                friendShipStatus = False
                                friendShipRequestsListList = friendShipRequestsMany.pop(requestUserId, None)
                                if friendShipRequestsListList is not None:
                                    for friendShipRequestsList in friendShipRequestsListList:
                                        if friendShipRequestsList[0] == id:
                                            friendShipStatus = "request"
                                if friendShipStatus is False:
                                    friendShipRequestsListList = friendShipRequestsMany.pop(id, None)
                                    if friendShipRequestsListList is not None:
                                        for friendShipRequestsList in friendShipRequestsListList:
                                            if friendShipRequestsList[0] == requestUserId:
                                                friendShipStatus = "myRequest"
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                                friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                            else:
                                friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                                if friendShipStatus is None:
                                    friendShipStatus = False
                                    friendShipRequestsListList = friendShipRequestsMany.pop(requestUserId, None)
                                    if friendShipRequestsListList is not None:
                                        for friendShipRequestsList in friendShipRequestsListList:
                                            if friendShipRequestsList[0] == id:
                                                friendShipStatus = "request"
                                    if friendShipStatus is False:
                                        friendShipRequestsListList = friendShipRequestsMany.pop(id, None)
                                        if friendShipRequestsListList is not None:
                                            for friendShipRequestsList in friendShipRequestsListList:
                                                if friendShipRequestsList[0] == requestUserId:
                                                    friendShipStatus = "myRequest"
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                                    friendShipStatusDict.update({requestUserId: friendShipStatus})
                                    friendShipToSet.update({id: friendShipStatusDict})
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                        if viewMainInfo is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                        if viewTHPSInfo is None:
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                        if viewAvatar is None:
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            userPrivacyDict.update({'viewAvatar': viewAvatar})
                        if viewOnlineStatus is None:
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                        if viewActiveStatus is None:
                            viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                            userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                        privacyToSet.update({id: privacyDict})
            if viewMainInfo:
                firstName = user.first_name or None
                lastName = user.last_name or None
                if firstName is not None and lastName is not None:
                    name = '{0} {1}'.format(firstName, lastName)
                    smallName = user.username
                else:
                    if firstName is not None:
                        name = firstName
                        smallName = user.username
                    else:
                        name = user.username
                        smallName = None
            else:
                name = user.username
                smallName = None
            if viewAvatar:
                avatar = user.avatarThumbSmaller or None
                if avatar is not None:
                    if smallName is not None:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><p><a href = "{1}">{3}</a></p><small>{4}</small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, smallName)
                    else:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><p><a href = "{1}">{3}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                else:
                    if smallName is not None:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2} [{3}]</p></div></a><p><a href = "{1}">{2}</a></p><small>{3}</small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name, smallName)
                    else:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2}</p></div></a><p><a href = "{1}">{2}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
            else:
                if smallName is not None:
                    listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2} [{3}]</p></div></a><p><a href = "{1}">{2}</a></p><small>{3}</small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name, smallName)
                else:
                    listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2}</p></div></a><p><a href = "{1}">{2}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
            if viewOnlineStatus or viewActiveStatus:
                if viewOnlineStatus and viewActiveStatus:
                    if id in onlineUsers:
                        if id in activeUsers:
                            activeStatusDict.update({id: 'stateActive'})
                            activeUsers.remove(id)
                        else:
                            middleStatus = chatStatusMiddleMany.pop(id, None)
                            if middleStatus is not None:
                                activeStatusDict.update({id: {'stateMiddle': middleStatus}})
                        onlineStatusDict.update({id: 'online'})
                        onlineUsers.remove(id)
                    else:
                        iffyStatus = iffyTimeMany.pop(id, None)
                        if iffyStatus is not None:
                            if id in activeUsers:
                                activeStatusDict.update({id: 'stateActive'})
                                activeUsers.remove(id)
                            else:
                                middleStatus = chatStatusMiddleMany.pop(id, None)
                                if middleStatus is not None:
                                    activeStatusDict.update({id: {'stateMiddle': middleStatus}})
                            onlineStatusDict.update({id: {'iffy': iffyStatus}})
                else:
                    if viewOnlineStatus:
                        if id in onlineUsers:
                            onlineStatusDict.update({id: 'online'})
                            onlineUsers.remove(id)
                        else:
                            iffyStatus = iffyTimeMany.pop(id, None)
                            if iffyStatus is not None:
                                onlineStatusDict.update({id: {'iffy': iffyStatus}})
                    else:
                        if id in activeUsers:
                            activeStatusDict.update({id: 'stateActive'})
                            activeUsers.remove(id)
                        else:
                            middleStatus = chatStatusMiddleMany.pop(id, None)
                            if middleStatus is not None:
                                activeStatusDict.update({id: {'stateMiddle': middleStatus}})
            thumbsResult.update({id: listThumbItem})
            thumbItemsToSet.update({id: {requestUserId: {'item': listThumbItem, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus}}})
        else:
            thumb = thumbs.get(requestUserId, None)
            if thumb is None:
                user = users.get(id__exact = id)
                privacyDict = privacyDictMany.get(id, None)
                if privacyDict is None:
                    if id in friendsIds:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                    else:
                        friendShipStatusDict = friendShipMany.get(id, None)
                        if friendShipStatusDict is None:
                            friendShipStatus = False
                            friendShipRequestsListList = friendShipRequestsMany.pop(requestUserId, None)
                            if friendShipRequestsListList is not None:
                                for friendShipRequestsList in friendShipRequestsListList:
                                    if friendShipRequestsList[0] == id:
                                        friendShipStatus = "request"
                            if friendShipStatus is False:
                                friendShipRequestsListList = friendShipRequestsMany.pop(id, None)
                                if friendShipRequestsListList is not None:
                                    for friendShipRequestsList in friendShipRequestsListList:
                                        if friendShipRequestsList[0] == requestUserId:
                                            friendShipStatus = "myRequest"
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                            friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                        else:
                            friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                            if friendShipStatus is None:
                                friendShipStatus = False
                                friendShipRequestsListList = friendShipRequestsMany.pop(requestUserId, None)
                                if friendShipRequestsListList is not None:
                                    for friendShipRequestsList in friendShipRequestsListList:
                                        if friendShipRequestsList[0] == id:
                                            friendShipStatus = "request"
                                if friendShipStatus is False:
                                    friendShipRequestsListList = friendShipRequestsMany.pop(id, None)
                                    if friendShipRequestsListList is not None:
                                        for friendShipRequestsList in friendShipRequestsListList:
                                            if friendShipRequestsList[0] == requestUserId:
                                                friendShipStatus = "myRequest"
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                                friendShipStatusDict.update({requestUserId: friendShipStatus})
                                friendShipToSet.update({id: friendShipStatusDict})
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                    privacyToSet.update({id: {requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
                else:
                    userPrivacyDict = privacyDict.get(requestUserId, None)
                    if userPrivacyDict is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            friendShipStatusDict = friendShipMany.get(id, None)
                            if friendShipStatusDict is None:
                                friendShipStatus = False
                                friendShipRequestsListList = friendShipRequestsMany.pop(requestUserId, None)
                                if friendShipRequestsListList is not None:
                                    for friendShipRequestsList in friendShipRequestsListList:
                                        if friendShipRequestsList[0] == id:
                                            friendShipStatus = "request"
                                if friendShipStatus is False:
                                    friendShipRequestsListList = friendShipRequestsMany.pop(id, None)
                                    if friendShipRequestsListList is not None:
                                        for friendShipRequestsList in friendShipRequestsListList:
                                            if friendShipRequestsList[0] == requestUserId:
                                                friendShipStatus = "myRequest"
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                                friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                            else:
                                friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                                if friendShipStatus is None:
                                    friendShipStatus = False
                                    friendShipRequestsListList = friendShipRequestsMany.pop(requestUserId, None)
                                    if friendShipRequestsListList is not None:
                                        for friendShipRequestsList in friendShipRequestsListList:
                                            if friendShipRequestsList[0] == id:
                                                friendShipStatus = "request"
                                    if friendShipStatus is False:
                                        friendShipRequestsListList = friendShipRequestsMany.pop(id, None)
                                        if friendShipRequestsListList is not None:
                                            for friendShipRequestsList in friendShipRequestsListList:
                                                if friendShipRequestsList[0] == requestUserId:
                                                    friendShipStatus = "myRequest"
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                                    friendShipStatusDict.update({requestUserId: friendShipStatus})
                                    friendShipToSet.update({id: friendShipStatusDict})
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                        privacyDict.update({requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
                        privacyToSet.update({id: privacyDict})
                    else:
                        viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                        viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                        viewAvatar = userPrivacyDict.get('viewAvatar', None)
                        viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                        viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                        if viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None or viewActiveStatus is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                friendShipStatusDict = friendShipMany.get(id, None)
                                if friendShipStatusDict is None:
                                    friendShipStatus = False
                                    friendShipRequestsListList = friendShipRequestsMany.pop(requestUserId, None)
                                    if friendShipRequestsListList is not None:
                                        for friendShipRequestsList in friendShipRequestsListList:
                                            if friendShipRequestsList[0] == id:
                                                friendShipStatus = "request"
                                    if friendShipStatus is False:
                                        friendShipRequestsListList = friendShipRequestsMany.pop(id, None)
                                        if friendShipRequestsListList is not None:
                                            for friendShipRequestsList in friendShipRequestsListList:
                                                if friendShipRequestsList[0] == requestUserId:
                                                    friendShipStatus = "myRequest"
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                                    friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                                else:
                                    friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                                    if friendShipStatus is None:
                                        friendShipStatus = False
                                        friendShipRequestsListList = friendShipRequestsMany.pop(requestUserId, None)
                                        if friendShipRequestsListList is not None:
                                            for friendShipRequestsList in friendShipRequestsListList:
                                                if friendShipRequestsList[0] == id:
                                                    friendShipStatus = "request"
                                        if friendShipStatus is False:
                                            friendShipRequestsListList = friendShipRequestsMany.pop(id, None)
                                            if friendShipRequestsListList is not None:
                                                for friendShipRequestsList in friendShipRequestsListList:
                                                    if friendShipRequestsList[0] == requestUserId:
                                                        friendShipStatus = "myRequest"
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                                        friendShipStatusDict.update({requestUserId: friendShipStatus})
                                        friendShipToSet.update({id: friendShipStatusDict})
                                    else:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                            if viewMainInfo is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                            if viewTHPSInfo is None:
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                            if viewAvatar is None:
                                viewAvatar = privacyChecker.checkAvatarPrivacy()
                                userPrivacyDict.update({'viewAvatar': viewAvatar})
                            if viewOnlineStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                            if viewActiveStatus is None:
                                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                            privacyToSet.update({id: privacyDict})
                if viewMainInfo:
                    firstName = user.first_name or None
                    lastName = user.last_name or None
                    if firstName is not None and lastName is not None:
                        name = '{0} {1}'.format(firstName, lastName)
                        smallName = user.username
                    else:
                        if firstName is not None:
                            name = firstName
                            smallName = user.username
                        else:
                            name = user.username
                            smallName = None
                else:
                    name = user.username
                    smallName = None
                if viewAvatar:
                    avatar = user.avatarThumbSmaller or None
                    if avatar is not None:
                        if smallName is not None:
                            listThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><p><a href = "{1}">{3}</a></p><small>{4}</small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, smallName)
                        else:
                            listThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><p><a href = "{1}">{3}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                    else:
                        if smallName is not None:
                            listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2} [{3}]</p></div></a><p><a href = "{1}">{2}</a></p><small>{3}</small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name, smallName)
                        else:
                            listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2}</p></div></a><p><a href = "{1}">{2}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                else:
                    if smallName is not None:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2} [{3}]</p></div></a><p><a href = "{1}">{2}</a></p><small>{3}</small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name, smallName)
                    else:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2}</p></div></a><p><a href = "{1}">{2}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                if viewOnlineStatus or viewActiveStatus:
                    if viewOnlineStatus and viewActiveStatus:
                        if id in onlineUsers:
                            if id in activeUsers:
                                activeStatusDict.update({id: 'stateActive'})
                                activeUsers.remove(id)
                            else:
                                middleStatus = chatStatusMiddleMany.pop(id, None)
                                if middleStatus is not None:
                                    activeStatusDict.update({id: {'stateMiddle': middleStatus}})
                            onlineStatusDict.update({id: 'online'})
                            onlineUsers.remove(id)
                        else:
                            iffyStatus = iffyTimeMany.pop(id, None)
                            if iffyStatus is not None:
                                if id in activeUsers:
                                    activeStatusDict.update({id: 'stateActive'})
                                    activeUsers.remove(id)
                                else:
                                    middleStatus = chatStatusMiddleMany.pop(id, None)
                                    if middleStatus is not None:
                                        activeStatusDict.update({id: {'stateMiddle': middleStatus}})
                                onlineStatusDict.update({id: {'iffy': iffyStatus}})
                    else:
                        if viewOnlineStatus:
                            if id in onlineUsers:
                                onlineStatusDict.update({id: 'online'})
                                onlineUsers.remove(id)
                            else:
                                iffyStatus = iffyTimeMany.pop(id, None)
                                if iffyStatus is not None:
                                    onlineStatusDict.update({id: {'iffy': iffyStatus}})
                        else:
                            if id in activeUsers:
                                activeStatusDict.update({id: 'stateActive'})
                                activeUsers.remove(id)
                            else:
                                middleStatus = chatStatusMiddleMany.pop(id, None)
                                if middleStatus is not None:
                                    activeStatusDict.update({id: {'stateMiddle': middleStatus}})
                thumbsResult.update({id: listThumbItem})
                thumbs.update({requestUserId: {'item': listThumbItem, 'showOnlineStatus': viewOnlineStatus}})
                thumbItemsToSet.update({id: thumbs})
            else:
                showOnlineStatus = thumb.get('showOnlineStatus', None)
                showActiveStatus = thumb.get('showActiveStatus', None)
                if showOnlineStatus is None or showActiveStatus is None:
                    privacyDict = privacyDictMany.get(id, None)
                    if privacyDict is None:
                        user = users.get(id__exact = id)
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            friendShipStatusDict = friendShipMany.get(id, None)
                            if friendShipStatusDict is None:
                                friendShipStatus = False
                                friendShipRequestsListList = friendShipRequestsMany.pop(requestUserId, None)
                                if friendShipRequestsListList is not None:
                                    for friendShipRequestsList in friendShipRequestsListList:
                                        if friendShipRequestsList[0] == id:
                                            friendShipStatus = "request"
                                if friendShipStatus is False:
                                    friendShipRequestsListList = friendShipRequestsMany.pop(id, None)
                                    if friendShipRequestsListList is not None:
                                        for friendShipRequestsList in friendShipRequestsListList:
                                            if friendShipRequestsList[0] == requestUserId:
                                                friendShipStatus = "myRequest"
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                                friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                            else:
                                friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                                if friendShipStatus is None:
                                    friendShipStatus = False
                                    friendShipRequestsListList = friendShipRequestsMany.pop(requestUserId, None)
                                    if friendShipRequestsListList is not None:
                                        for friendShipRequestsList in friendShipRequestsListList:
                                            if friendShipRequestsList[0] == id:
                                                friendShipStatus = "request"
                                    if friendShipStatus is False:
                                        friendShipRequestsListList = friendShipRequestsMany.pop(id, None)
                                        if friendShipRequestsListList is not None:
                                            for friendShipRequestsList in friendShipRequestsListList:
                                                if friendShipRequestsList[0] == requestUserId:
                                                    friendShipStatus = "myRequest"
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                                    friendShipStatusDict.update({requestUserId: friendShipStatus})
                                    friendShipToSet.update({id: friendShipStatusDict})
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                        if showOnlineStatus is None and showActiveStatus is None:
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                            privacyToSet.update({id: {requestUserId: {'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
                        else:
                            if showOnlineStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                privacyToSet.update({id: {requestUserId: {'viewOnlineStatus': viewOnlineStatus}}})
                            else:
                                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                privacyToSet.update({id: {requestUserId: {'viewActiveStatus': viewActiveStatus}}})
                    else:
                        userPrivacyDict = privacyDict.get(requestUserId, None)
                        if userPrivacyDict is None:
                            user = users.get(id__exact = id)
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                friendShipStatusDict = friendShipMany.get(id, None)
                                if friendShipStatusDict is None:
                                    friendShipStatus = False
                                    friendShipRequestsListList = friendShipRequestsMany.pop(requestUserId, None)
                                    if friendShipRequestsListList is not None:
                                        for friendShipRequestsList in friendShipRequestsListList:
                                            if friendShipRequestsList[0] == id:
                                                friendShipStatus = "request"
                                    if friendShipStatus is False:
                                        friendShipRequestsListList = friendShipRequestsMany.pop(id, None)
                                        if friendShipRequestsListList is not None:
                                            for friendShipRequestsList in friendShipRequestsListList:
                                                if friendShipRequestsList[0] == requestUserId:
                                                    friendShipStatus = "myRequest"
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                                    friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                                else:
                                    friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                                    if friendShipStatus is None:
                                        friendShipStatus = False
                                        friendShipRequestsListList = friendShipRequestsMany.pop(requestUserId, None)
                                        if friendShipRequestsListList is not None:
                                            for friendShipRequestsList in friendShipRequestsListList:
                                                if friendShipRequestsList[0] == id:
                                                    friendShipStatus = "request"
                                        if friendShipStatus is False:
                                            friendShipRequestsListList = friendShipRequestsMany.pop(id, None)
                                            if friendShipRequestsListList is not None:
                                                for friendShipRequestsList in friendShipRequestsListList:
                                                    if friendShipRequestsList[0] == requestUserId:
                                                        friendShipStatus = "myRequest"
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                                        friendShipStatusDict.update({requestUserId: friendShipStatus})
                                        friendShipToSet.update({id: friendShipStatusDict})
                                    else:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                            if showOnlineStatus is None and showActiveStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                privacyDict.update({requestUserId: {'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
                            else:
                                if showOnlineStatus is None:
                                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                    privacyDict.update({requestUserId: {'viewOnlineStatus': viewOnlineStatus}})
                                else:
                                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                    privacyDict.update({requestUserId: {'viewActiveStatus': viewActiveStatus}})
                            privacyToSet.update({id: privacyDict})
                        else:
                            if showOnlineStatus is None:
                                viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                            else:
                                viewOnlineStatus = showOnlineStatus
                            if showActiveStatus is None:
                                viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                            else:
                                viewActiveStatus = showActiveStatus
                            if viewOnlineStatus is None or viewActiveStatus is None:
                                user = users.get(id__exact = id)
                                if id in friendsIds:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                else:
                                    friendShipStatusDict = friendShipMany.get(id, None)
                                    if friendShipStatusDict is None:
                                        friendShipStatus = False
                                        friendShipRequestsListList = friendShipRequestsMany.pop(requestUserId, None)
                                        if friendShipRequestsListList is not None:
                                            for friendShipRequestsList in friendShipRequestsListList:
                                                if friendShipRequestsList[0] == id:
                                                    friendShipStatus = "request"
                                        if friendShipStatus is False:
                                            friendShipRequestsListList = friendShipRequestsMany.pop(id, None)
                                            if friendShipRequestsListList is not None:
                                                for friendShipRequestsList in friendShipRequestsListList:
                                                    if friendShipRequestsList[0] == requestUserId:
                                                        friendShipStatus = "myRequest"
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                                        friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                                    else:
                                        friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                                        if friendShipStatus is None:
                                            friendShipStatus = False
                                            friendShipRequestsListList = friendShipRequestsMany.pop(requestUserId, None)
                                            if friendShipRequestsListList is not None:
                                                for friendShipRequestsList in friendShipRequestsListList:
                                                    if friendShipRequestsList[0] == id:
                                                        friendShipStatus = "request"
                                            if friendShipStatus is False:
                                                friendShipRequestsListList = friendShipRequestsMany.pop(id, None)
                                                if friendShipRequestsListList is not None:
                                                    for friendShipRequestsList in friendShipRequestsListList:
                                                        if friendShipRequestsList[0] == requestUserId:
                                                            friendShipStatus = "myRequest"
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                                            friendShipStatusDict.update({requestUserId: friendShipStatus})
                                            friendShipToSet.update({id: friendShipStatusDict})
                                        else:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                                if viewOnlineStatus is None and viewActiveStatus is None:
                                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                    userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus})
                                else:
                                    if viewOnlineStatus is None:
                                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                        userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                                    else:
                                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                        userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                                privacyToSet.update({id: privacyDict})
                    if viewOnlineStatus or viewActiveStatus:
                        if viewOnlineStatus and viewActiveStatus:
                            if id in onlineUsers:
                                if id in activeUsers:
                                    activeStatusDict.update({id: 'stateActive'})
                                    activeUsers.remove(id)
                                else:
                                    middleStatus = chatStatusMiddleMany.pop(id, None)
                                    if middleStatus is not None:
                                        activeStatusDict.update({id: {'stateMiddle': middleStatus}})
                                onlineStatusDict.update({id: 'online'})
                                onlineUsers.remove(id)
                            else:
                                iffyStatus = iffyTimeMany.pop(id, None)
                                if iffyStatus is not None:
                                    if id in activeUsers:
                                        activeStatusDict.update({id: 'stateActive'})
                                        activeUsers.remove(id)
                                    else:
                                        middleStatus = chatStatusMiddleMany.pop(id, None)
                                        if middleStatus is not None:
                                            activeStatusDict.update({id: {'stateMiddle': middleStatus}})
                                    onlineStatusDict.update({id: {'iffy': iffyStatus}})
                        else:
                            if viewOnlineStatus:
                                if id in onlineUsers:
                                    onlineStatusDict.update({id: 'online'})
                                    onlineUsers.remove(id)
                                else:
                                    iffyStatus = iffyTimeMany.pop(id, None)
                                    if iffyStatus is not None:
                                        onlineStatusDict.update({id: {'iffy': iffyStatus}})
                            else:
                                if id in activeUsers:
                                    activeStatusDict.update({id: 'stateActive'})
                                    activeUsers.remove(id)
                                else:
                                    middleStatus = chatStatusMiddleMany.pop(id, None)
                                    if middleStatus is not None:
                                        activeStatusDict.update({id: {'stateMiddle': middleStatus}})
                    if showOnlineStatus is None and showActiveStatus is None:
                        thumb.update({'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus})
                    else:
                        if showOnlineStatus is None:
                            thumb.update({'showOnlineStatus': viewOnlineStatus})
                        else:
                            thumb.update({'showActiveStatus': viewActiveStatus})
                    thumbItemsToSet.update({id: thumbs})
                else:
                    if showOnlineStatus or showActiveStatus:
                        if showOnlineStatus and showActiveStatus:
                            if id in onlineUsers:
                                if id in activeUsers:
                                    activeStatusDict.update({id: 'stateActive'})
                                    activeUsers.remove(id)
                                else:
                                    middleStatus = chatStatusMiddleMany.pop(id, None)
                                    if middleStatus is not None:
                                        activeStatusDict.update({id: {'stateMiddle': middleStatus}})
                                onlineStatusDict.update({id: 'online'})
                                onlineUsers.remove(id)
                            else:
                                iffyStatus = iffyTimeMany.pop(id, None)
                                if iffyStatus is not None:
                                    if id in activeUsers:
                                        activeStatusDict.update({id: 'stateActive'})
                                        activeUsers.remove(id)
                                    else:
                                        middleStatus = chatStatusMiddleMany.pop(id, None)
                                        if middleStatus is not None:
                                            activeStatusDict.update({id: {'stateMiddle': middleStatus}})
                                    onlineStatusDict.update({id: {'iffy': iffyStatus}})
                        else:
                            if showOnlineStatus:
                                if id in onlineUsers:
                                    onlineStatusDict.update({id: 'online'})
                                    onlineUsers.remove(id)
                                else:
                                    iffyStatus = iffyTimeMany.pop(id, None)
                                    if iffyStatus is not None:
                                        onlineStatusDict.update({id: {'iffy': iffyStatus}})
                            else:
                                if id in activeUsers:
                                    activeStatusDict.update({id: 'stateActive'})
                                    activeUsers.remove(id)
                                else:
                                    middleStatus = chatStatusMiddleMany.pop(id, None)
                                    if middleStatus is not None:
                                        activeStatusDict.update({id: {'stateMiddle': middleStatus}})
                thumbsResult.update({id: thumb['item']})
    if isThumbsSurplus:
        smallDialogThumbsCache.set_many(thumbItemsToSet)
        if isPrivacySurplus:
            privacyCache.set_many(privacyToSet)
            if isFriendShipRequestsSurplus:
                friendShipCache.set_many(friendShipToSet)
    return thumbsResult, onlineStatusDict, activeStatusDict

def getMainDialogThumbs(idsSet, requestUser, requestUserId):
    ''' Вывод больших 'dialogThumbs' и 2 словарей со статусом (онлайн и активность) '''
    textDict = None
    onlineStatusText = None
    iffyStatusText = None
    activeStatusText = None
    middleStatusText = None
    leaveStatusText = None
    onlineUsersSet = set(caches['onlineUsers'].get_many(idsSet).keys())
    iffyTimeMany = caches['iffyUsersOnlineStatus'].get_many((idsSet - onlineUsersSet))
    chatStatusActiveSet = set(caches['chatStatusActive'].get_many((onlineUsersSet | set(iffyTimeMany.keys()))).keys())
    chatStatusMiddleMany = caches['chatStatusMiddle'].get_many((idsSet - chatStatusActiveSet))
    smallDialogThumbsCache = caches['dialogThumbs']
    smallDialogThumbsMany = smallDialogThumbsCache.get_many(idsSet)
    smallDialogThumbsManyKeys = set(smallDialogThumbsMany.keys())
    thumbsSurplus = idsSet - smallDialogThumbsManyKeys
    thumbsSurplus1 = set()
    for id in smallDialogThumbsManyKeys:
        thumb = smallDialogThumbsMany[id].get(requestUserId, None)
        if thumb is None:
            thumbsSurplus1.add(id)
        else:
            if 'showOnlineStatus' or 'showActiveStatus' not in thumb:
                thumbsSurplus1.add(id)
    thumbsSurplus = thumbsSurplus | thumbsSurplus1
    if len(thumbsSurplus) > 0:
        privacyCache = caches['privacyCache']
        privacyDictMany = privacyCache.get_many(thumbsSurplus)
        privacyManyKeys = set(privacyDictMany.keys())
        privacySurplus = thumbsSurplus - privacyManyKeys
        privacySurplus1 = set()
        for id in privacyManyKeys:
            privacyDict = privacyDictMany[id]
            if requestUserId not in privacyDict:
                privacySurplus1.add(id)
            else:
                if 'viewMainInfo' or 'viewTHPSInfo' or 'viewAvatar' or 'viewOnlineStatus' or 'viewActiveStatus' not in privacyDict:
                    privacySurplus1.add(id)
        privacySurplus = privacySurplus | privacySurplus1
        if len(privacySurplus) > 0:
            users = mainUserProfile.objects.filter(id__in = privacySurplus)
            privacySettings = userProfilePrivacy.objects.filter(user__in = users)
            friendsIdsCache = caches['friendsIdCache']
            friendsIds = friendsIdsCache.get(requestUserId, None)
            if friendsIds is None:
                friendsCache = caches['friendsCache']
                friends = friendsCache.get(requestUserId, None)
                if friends is None:
                    friends = requestUser.friends.all()
                    if friends.count():
                        friendsCache.set(requestUserId, friends)
                        friendsIdsQuerySet = friends.values("id")
                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                    else:
                        friendsCache.set(requestUserId, False)
                        friendsIds = set()
                else:
                    if friends is False:
                        friendsIds = set()
                    else:
                        friendsIds = {friend.id for friend in friends}
                friendsIdsCache.set(requestUserId, friendsIds)
            if len(friendsIds) > 0:
                friendsShipSurplus = privacySurplus - friendsIds
                if len(friendsShipSurplus) > 0:
                    friendShipCache = caches['friendShipStatusCache']
                    friendShipDict = friendShipCache.get(requestUserId, {})
                    friendShipManyKeys = set(friendShipDict.keys())
                    friendShipRequestsSurplus = friendsShipSurplus - friendShipManyKeys
                    if len(friendShipRequestsSurplus) > 0:
                        friendShipRequests = caches['friendshipRequests'].get_many(friendShipRequestsSurplus)
            else:
                if len(privacySurplus) > 0:
                    friendShipCache = caches['friendShipStatusCache']
                    friendShipDict = friendShipCache.get(requestUserId, {})
                    friendShipManyKeys = set(friendShipDict.keys())
                    friendShipRequestsSurplus = privacySurplus - friendShipManyKeys
                    if len(friendShipRequestsSurplus) > 0:
                        friendShipRequests = caches['friendshipRequests'].get_many(friendShipRequestsSurplus)
            privacyToSet = {}
            friendShipToSet = {}
    thumbItemsToSet = {}
    thumbsResult = {}
    onlineStatusDict = {}
    activeStatusDict = {}
    for id in idsSet:
        thumbs = smallDialogThumbsMany.get(id, None)
        if thumbs is None:
            user = users.get(id__exact = id)
            privacyDict = privacyDictMany.get(id, None)
            if privacyDict is None:
                if id in friendsIds:
                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                else:
                    friendShipStatus = friendShipDict.get(id, None)
                    if friendShipStatus is None:
                        if friendShipRequests is None:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            friendShipToSet.update({requestUserId: {id: False}})
                        else:
                            friendShipRequest = friendShipRequests.get(id, None)
                            if friendShipRequest is None:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                friendShipToSet.update({requestUserId: {id: False}})
                            elif friendShipRequest == "request":
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                friendShipToSet.update({requestUserId: {id: "request"}})
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                friendShipToSet.update({requestUserId: {id: "myRequest"}})
                    elif friendShipStatus is False:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                    elif friendShipStatus == "request":
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                    else:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                viewAvatar = privacyChecker.checkAvatarPrivacy()
                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                privacyToSet.update({id: {requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
            else:
                userPrivacyDict = privacyDict.get(requestUserId, None)
                if userPrivacyDict is None:
                    if id in friendsIds:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                    else:
                        friendShipStatus = friendShipDict.get(id, None)
                        if friendShipStatus is None:
                            if friendShipRequests is None:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                friendShipToSet.update({requestUserId: {id: False}})
                            else:
                                friendShipRequest = friendShipRequests.get(id, None)
                                if friendShipRequest is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                elif friendShipRequest == "request":
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: "request"}})
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: "myRequest"}})
                        elif friendShipStatus is False:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        elif friendShipStatus == "request":
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                    privacyDict.update({requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
                    privacyToSet.update({id: privacyDict})
                else:
                    viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                    viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                    viewAvatar = userPrivacyDict.get('viewAvatar', None)
                    viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                    viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                    if viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None or viewActiveStatus is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            friendShipStatus = friendShipDict.get(id, None)
                            if friendShipStatus is None:
                                if friendShipRequests is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                else:
                                    friendShipRequest = friendShipRequests.get(id, None)
                                    if friendShipRequest is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    elif friendShipRequest == "request":
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "request"}})
                                    else:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "myRequest"}})
                            elif friendShipStatus is False:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            elif friendShipStatus == "request":
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        if viewMainInfo is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                        if viewTHPSInfo is None:
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                        if viewAvatar is None:
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            userPrivacyDict.update({'viewAvatar': viewAvatar})
                        if viewOnlineStatus is None:
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                        if viewActiveStatus is None:
                            viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                            userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                        privacyToSet.update({id: privacyDict})
            if viewMainInfo:
                firstName = user.first_name or None
                lastName = user.last_name or None
                if firstName and lastName:
                    name = '{0} {1}'.format(firstName, lastName)
                    smallName = user.username
                else:
                    if firstName:
                        name = firstName
                        smallName = user.username
                    else:
                        name = user.username
                        smallName = None
            else:
                name = user.username
                smallName = None
            if viewAvatar:
                avatar = user.avatarThumbMiddleSmall or None
                if avatar is not None:
                    if smallName:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, smallName)
                    else:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
            else:
                if smallName:
                    itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                else:
                    itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
            itemString1 = '</div>'
            if viewOnlineStatus and viewActiveStatus:
                if id in onlineUsersSet:
                    if onlineStatusText is None:
                        if textDict is None:
                            textDict = usersListText(lang = 'ru').getDict()
                        onlineStatusText = textDict.pop('onlineStatus')
                    if id in chatStatusActiveSet:
                        if activeStatusText is None:
                            if textDict is None:
                                textDict = usersListText(lang = 'ru').getDict()
                            activeStatusText = textDict.pop('activeStatus')
                        thumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), itemString1)})
                        activeStatusDict.update({id: 'stateActive'})
                        chatStatusActiveSet.remove(id)
                    else:
                        middleTupleStr = chatStatusMiddleMany.pop(id, None)
                        if middleTupleStr is not None:
                            if middleStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                middleStatusText = textDict.pop('middleStatus')
                            thumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "middle">{1}</p></noscript>'.format(onlineStatusText, middleStatusText), itemString1)})
                            activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                        else:
                            if leaveStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                leaveStatusText = textDict.pop('leaveStatus')
                            thumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "leave">{1}</p></noscript>'.format(onlineStatusText, leaveStatusText), itemString1)})
                    onlineStatusDict.update({id: 'online'})
                    onlineUsersSet.remove(id)
                else:
                    iffyTimeStr = iffyTimeMany.pop(id, None)
                    if iffyTimeStr is not None:
                        if iffyStatusText is None:
                            if textDict is None:
                                textDict = usersListText(lang = 'ru').getDict()
                            iffyStatusText = textDict.pop('uknownStatus')
                        if id in chatStatusActiveSet:
                            if activeStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                activeStatusText = textDict.pop('activeStatus')
                            thumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "active">{1}</p></noscript>'.format(iffyStatusText, activeStatusText), itemString1)})
                            activeStatusDict.update({id: 'stateActive'})
                            chatStatusActiveSet.remove(id)
                        else:
                            middleTupleStr = chatStatusMiddleMany.pop(id, None)
                            if middleTupleStr is not None:
                                if middleStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    middleStatusText = textDict.pop('middleStatus')
                                thumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "middle">{1}</p></noscript>'.format(iffyStatusText, middleStatusText), itemString1)})
                                activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                            else:
                                if leaveStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    leaveStatusText = textDict.pop('leaveStatus')
                                thumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "leave">{1}</p></noscript>'.format(iffyStatusText, leaveStatusText), itemString1)})
                        onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                    else:
                        thumbsResult.update({id: (itemString0, itemString1)})
            else:
                if viewOnlineStatus:
                    if id in onlineUsersSet:
                        if onlineStatusText is None:
                            if textDict is None:
                                textDict = usersListText(lang = 'ru').getDict()
                            onlineStatusText = textDict.pop('onlineStatus')
                        thumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p></noscript>'.format(onlineStatusText), itemString1)})
                        onlineStatusDict.update({id: 'online'})
                        onlineUsersSet.remove(id)
                    else:
                        iffyTimeStr = iffyTimeMany.pop(id, None)
                        if iffyTimeStr is not None:
                            if iffyStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                iffyStatusText = textDict.pop('uknownStatus')
                            thumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText), itemString1)})
                            onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                        else:
                            thumbsResult.update({id: (itemString0, itemString1)})
                elif viewActiveStatus:
                    if id in chatStatusActiveSet:
                        if activeStatusText is None:
                            if textDict is None:
                                textDict = usersListText(lang = 'ru').getDict()
                            activeStatusText = textDict.pop('activeStatus')
                        thumbsResult.update({id: (itemString0, '<noscript><p class = "active">{0}</p></noscript>'.format(activeStatusText), itemString1)})
                        activeStatusDict.update({id: 'stateActive'})
                        chatStatusActiveSet.remove(id)
                    else:
                        middleTupleStr = chatStatusMiddleMany.pop(id, None)
                        if middleTupleStr is not None:
                            if middleStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                middleStatusText = textDict.pop('middleStatus')
                            thumbsResult.update({id: (itemString0, '<noscript><p class = "middle">{0}</p></noscript>'.format(middleStatusText), itemString1)})
                            activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                        else:
                            thumbsResult.update({id: (itemString0, itemString1)})
                else:
                    thumbsResult.update({id: (itemString0, itemString1)})
            thumbItemsToSet.update({id: {requestUserId: {'item': (itemString0, itemString1), 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus}}})
        else:
            thumb = thumbs.get(requestUserId, None)
            if thumb is None:
                user = users.get(id__exact = id)
                privacyDict = privacyDictMany.get(id, None)
                if privacyDict is None:
                    if id in friendsIds:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                    else:
                        friendShipStatus = friendShipDict.get(id, None)
                        if friendShipStatus is None:
                            if friendShipRequests is None:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                friendShipToSet.update({requestUserId: {id: False}})
                            else:
                                friendShipRequest = friendShipRequests.get(id, None)
                                if friendShipRequest is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                elif friendShipRequest == "request":
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: "request"}})
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: "myRequest"}})
                        elif friendShipStatus is False:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        elif friendShipStatus == "request":
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                    privacyToSet.update({id: {requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
                else:
                    userPrivacyDict = privacyDict.get(requestUserId, None)
                    if userPrivacyDict is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            friendShipStatus = friendShipDict.get(id, None)
                            if friendShipStatus is None:
                                if friendShipRequests is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                else:
                                    friendShipRequest = friendShipRequests.get(id, None)
                                    if friendShipRequest is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    elif friendShipRequest == "request":
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "request"}})
                                    else:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "myRequest"}})
                            elif friendShipStatus is False:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            elif friendShipStatus == "request":
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                        privacyDict.update({requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
                        privacyToSet.update({id: privacyDict})
                    else:
                        viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                        viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                        viewAvatar = userPrivacyDict.get('viewAvatar', None)
                        viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                        viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                        if viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None or viewActiveStatus is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                friendShipStatus = friendShipDict.get(id, None)
                                if friendShipStatus is None:
                                    if friendShipRequests is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    else:
                                        friendShipRequest = friendShipRequests.get(id, None)
                                        if friendShipRequest is None:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: False}})
                                        elif friendShipRequest == "request":
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: "request"}})
                                        else:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: "myRequest"}})
                                elif friendShipStatus is False:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                elif friendShipStatus == "request":
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            if viewMainInfo is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                            if viewTHPSInfo is None:
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                            if viewAvatar is None:
                                viewAvatar = privacyChecker.checkAvatarPrivacy()
                                userPrivacyDict.update({'viewAvatar': viewAvatar})
                            if viewOnlineStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                            if viewActiveStatus is None:
                                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                            privacyToSet.update({id: privacyDict})
                if viewMainInfo:
                    firstName = user.first_name or None
                    lastName = user.last_name or None
                    if firstName and lastName:
                        name = '{0} {1}'.format(firstName, lastName)
                        smallName = user.username
                    else:
                        if firstName:
                            name = firstName
                            smallName = user.username
                        else:
                            name = user.username
                            smallName = None
                else:
                    name = user.username
                    smallName = None
                if viewAvatar:
                    avatar = user.avatarThumbMiddleSmall or None
                    if avatar is not None:
                        if smallName:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, smallName)
                        else:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                itemString1 = '</div>'
                if viewOnlineStatus and viewActiveStatus:
                    if id in onlineUsersSet:
                        if onlineStatusText is None:
                            if textDict is None:
                                textDict = usersListText(lang = 'ru').getDict()
                            onlineStatusText = textDict.pop('onlineStatus')
                        if id in chatStatusActiveSet:
                            if activeStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                activeStatusText = textDict.pop('activeStatus')
                            thumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), itemString1)})
                            activeStatusDict.update({id: 'stateActive'})
                            chatStatusActiveSet.remove(id)
                        else:
                            middleTupleStr = chatStatusMiddleMany.pop(id, None)
                            if middleTupleStr is not None:
                                if middleStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    middleStatusText = textDict.pop('middleStatus')
                                thumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "middle">{1}</p></noscript>'.format(onlineStatusText, middleStatusText), itemString1)})
                                activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                            else:
                                if leaveStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    leaveStatusText = textDict.pop('leaveStatus')
                                thumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "leave">{1}</p></noscript>'.format(onlineStatusText, leaveStatusText), itemString1)})
                        onlineStatusDict.update({id: 'online'})
                        onlineUsersSet.remove(id)
                    else:
                        iffyTimeStr = iffyTimeMany.pop(id, None)
                        if iffyTimeStr is not None:
                            if iffyStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                iffyStatusText = textDict.pop('uknownStatus')
                            if id in chatStatusActiveSet:
                                if activeStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    activeStatusText = textDict.pop('activeStatus')
                                thumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "active">{1}</p></noscript>'.format(iffyStatusText, activeStatusText), itemString1)})
                                activeStatusDict.update({id: 'stateActive'})
                                chatStatusActiveSet.remove(id)
                            else:
                                middleTupleStr = chatStatusMiddleMany.pop(id, None)
                                if middleTupleStr is not None:
                                    if middleStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        middleStatusText = textDict.pop('middleStatus')
                                    thumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "middle">{1}</p></noscript>'.format(iffyStatusText, middleStatusText), itemString1)})
                                    activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                                else:
                                    if leaveStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        leaveStatusText = textDict.pop('leaveStatus')
                                    thumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "leave">{1}</p></noscript>'.format(iffyStatusText, leaveStatusText), itemString1)})
                            onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                        else:
                            thumbsResult.update({id: (itemString0, itemString1)})
                else:
                    if viewOnlineStatus:
                        if id in onlineUsersSet:
                            if onlineStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                onlineStatusText = textDict.pop('onlineStatus')
                            thumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p></noscript>'.format(onlineStatusText), itemString1)})
                            onlineStatusDict.update({id: 'online'})
                            onlineUsersSet.remove(id)
                        else:
                            iffyTimeStr = iffyTimeMany.pop(id, None)
                            if iffyTimeStr is not None:
                                if iffyStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    iffyStatusText = textDict.pop('uknownStatus')
                                thumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText), itemString1)})
                                onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                            else:
                                thumbsResult.update({id: (itemString0, itemString1)})
                    elif viewActiveStatus:
                        if id in chatStatusActiveSet:
                            if activeStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                activeStatusText = textDict.pop('activeStatus')
                            thumbsResult.update({id: (itemString0, '<noscript><p class = "active">{0}</p></noscript>'.format(activeStatusText), itemString1)})
                            activeStatusDict.update({id: 'stateActive'})
                            chatStatusActiveSet.remove(id)
                        else:
                            middleTupleStr = chatStatusMiddleMany.pop(id, None)
                            if middleTupleStr is not None:
                                if middleStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    middleStatusText = textDict.pop('middleStatus')
                                thumbsResult.update({id: (itemString0, '<noscript><p class = "middle">{0}</p></noscript>'.format(middleStatusText), itemString1)})
                                activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                            else:
                                thumbsResult.update({id: (itemString0, itemString1)})
                    else:
                        thumbsResult.update({id: (itemString0, itemString1)})
                thumbs.update({requestUserId: {'item': (itemString0, itemString1), 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus}})
                thumbItemsToSet.update({id: thumbs})
            else:
                showOnlineStatus = thumb.get('showOnlineStatus', None)
                showActiveStatus = thumb.get('showActiveStatus', None)
                thumbItem = thumb['item']
                if showOnlineStatus is None or showActiveStatus is None:
                    privacyDict = privacyDictMany.get(id, None)
                    if privacyDict is None:
                        user = users.get(id__exact = id)
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            friendShipStatus = friendShipDict.get(id, None)
                            if friendShipStatus is None:
                                if friendShipRequests is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                else:
                                    friendShipRequest = friendShipRequests.get(id, None)
                                    if friendShipRequest is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    elif friendShipRequest == "request":
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "request"}})
                                    else:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "myRequest"}})
                            elif friendShipStatus is False:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            elif friendShipStatus == "request":
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        if showOnlineStatus is None and showActiveStatus is None:
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                            privacyToSet.update({id: {requestUserId: {'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
                            thumb.update({'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus})
                        else:
                            if showOnlineStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                privacyToSet.update({id: {requestUserId: {'viewOnlineStatus': viewActiveStatus}}})
                                thumb.update({'showOnlineStatus': viewOnlineStatus})
                            elif showActiveStatus is None:
                                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                privacyToSet.update({id: {requestUserId: {'viewActiveStatus': viewActiveStatus}}})
                                thumb.update({'showActiveStatus': viewActiveStatus})
                    else:
                        userPrivacyDict = privacyDict.get(requestUserId, None)
                        if userPrivacyDict is None:
                            user = users.get(id__exact = id)
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                friendShipStatus = friendShipDict.get(id, None)
                                if friendShipStatus is None:
                                    if friendShipRequests is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    else:
                                        friendShipRequest = friendShipRequests.get(id, None)
                                        if friendShipRequest is None:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: False}})
                                        elif friendShipRequest == "request":
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: "request"}})
                                        else:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: "myRequest"}})
                                elif friendShipStatus is False:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                elif friendShipStatus == "request":
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            if showOnlineStatus is None and showActiveStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                privacyDict.update({requestUserId: {'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
                                thumb.update({'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus})
                            else:
                                if showOnlineStatus is None:
                                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                    privacyDict.update({requestUserId: {'viewOnlineStatus': viewOnlineStatus}})
                                    thumb.update({'showOnlineStatus': viewOnlineStatus})
                                elif showActiveStatus is None:
                                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                    privacyDict.update({requestUserId: {'viewActiveStatus': viewActiveStatus}})
                                    thumb.update({'showActiveStatus': viewActiveStatus})
                            privacyToSet.update({id: privacyDict})
                        else:
                            viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                            viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                            if viewOnlineStatus is None or viewActiveStatus is None:
                                user = users.get(id__exact = id)
                                if id in friendsIds:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                else:
                                    friendShipStatus = friendShipDict.get(id, None)
                                    if friendShipStatus is None:
                                        if friendShipRequests is None:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: False}})
                                        else:
                                            friendShipRequest = friendShipRequests.get(id, None)
                                            if friendShipRequest is None:
                                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                                friendShipToSet.update({requestUserId: {id: False}})
                                            elif friendShipRequest == "request":
                                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                                friendShipToSet.update({requestUserId: {id: "request"}})
                                            else:
                                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                                friendShipToSet.update({requestUserId: {id: "myRequest"}})
                                    elif friendShipStatus is False:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    elif friendShipStatus == "request":
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    else:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                if viewOnlineStatus is None and viewActiveStatus is None:
                                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                    userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus})
                                    thumb.update({'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus})
                                else:
                                    if viewOnlineStatus is None:
                                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                        userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                                        thumb.update({'showOnlineStatus': viewOnlineStatus})
                                    elif showActiveStatus is None:
                                        viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                        userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                                        thumb.update({'showActiveStatus': viewActiveStatus})
                                privacyToSet.update({id: privacyDict})
                    thumbs.update({requestUserId: thumb})
                    thumbItemsToSet.update({id: thumbs})
                    if viewOnlineStatus and viewActiveStatus:
                        if id in onlineUsersSet:
                            if onlineStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                onlineStatusText = textDict.pop('onlineStatus')
                            if id in chatStatusActiveSet:
                                if activeStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    activeStatusText = textDict.pop('activeStatus')
                                thumbsResult.update({id: (thumbItem[0], '<noscript><p class = "online">{0}</p><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), thumbItem[1])})
                                activeStatusDict.update({id: 'stateActive'})
                                chatStatusActiveSet.remove(id)
                            else:
                                middleTupleStr = chatStatusMiddleMany.pop(id, None)
                                if middleTupleStr is not None:
                                    if middleStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        middleStatusText = textDict.pop('middleStatus')
                                    thumbsResult.update({id: (thumbItem[0], '<noscript><p class = "online">{0}</p><p class = "middle">{1}</p></noscript>'.format(onlineStatusText, middleStatusText), thumbItem[1])})
                                    activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                                else:
                                    if leaveStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        leaveStatusText = textDict.pop('leaveStatus')
                                    thumbsResult.update({id: (thumbItem[0], '<noscript><p class = "online">{0}</p><p class = "leave">{1}</p></noscript>'.format(onlineStatusText, leaveStatusText), thumbItem[1])})
                            onlineStatusDict.update({id: 'online'})
                            onlineUsersSet.remove(id)
                        else:
                            iffyTimeStr = iffyTimeMany.pop(id, None)
                            if iffyTimeStr is not None:
                                if iffyStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    iffyStatusText = textDict.pop('uknownStatus')
                                if id in chatStatusActiveSet:
                                    if activeStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        activeStatusText = textDict.pop('activeStatus')
                                    thumbsResult.update({id: (thumbItem[0], '<noscript><p class = "iffy">{0}</p><p class = "active">{1}</p></noscript>'.format(iffyStatusText, activeStatusText), thumbItem[1])})
                                    activeStatusDict.update({id: 'stateActive'})
                                    chatStatusActiveSet.remove(id)
                                else:
                                    middleTupleStr = chatStatusMiddleMany.pop(id, None)
                                    if middleTupleStr is not None:
                                        if middleStatusText is None:
                                            if textDict is None:
                                                textDict = usersListText(lang = 'ru').getDict()
                                            middleStatusText = textDict.pop('middleStatus')
                                        thumbsResult.update({id: (thumbItem[0], '<noscript><p class = "iffy">{0}</p><p class = "middle">{1}</p></noscript>'.format(iffyStatusText, middleStatusText), thumbItem[1])})
                                        activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                                    else:
                                        if leaveStatusText is None:
                                            if textDict is None:
                                                textDict = usersListText(lang = 'ru').getDict()
                                            leaveStatusText = textDict.pop('leaveStatus')
                                        thumbsResult.update({id: (thumbItem[0], '<noscript><p class = "iffy">{0}</p><p class = "leave">{1}</p></noscript>'.format(iffyStatusText, leaveStatusText), thumbItem[1])})
                                onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                            else:
                                thumbsResult.update({id: (thumbItem[0], thumbItem[1])})
                    else:
                        if viewOnlineStatus:
                            if id in onlineUsersSet:
                                if onlineStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    onlineStatusText = textDict.pop('onlineStatus')
                                thumbsResult.update({id: (thumbItem[0], '<noscript><p class = "online">{0}</p></noscript>'.format(onlineStatusText), thumbItem[1])})
                                onlineStatusDict.update({id: 'online'})
                                onlineUsersSet.remove(id)
                            else:
                                iffyTimeStr = iffyTimeMany.pop(id, None)
                                if iffyTimeStr is not None:
                                    if iffyStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        iffyStatusText = textDict.pop('uknownStatus')
                                    thumbsResult.update({id: (thumbItem[0], '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText), thumbItem[1])})
                                    onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                                else:
                                    thumbsResult.update({id: (thumbItem[0], thumbItem[1])})
                        elif viewActiveStatus:
                            if id in chatStatusActiveSet:
                                if activeStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    activeStatusText = textDict.pop('activeStatus')
                                thumbsResult.update({id: (thumbItem[0], '<noscript><p class = "active">{0}</p></noscript>'.format(activeStatusText), thumbItem[1])})
                                activeStatusDict.update({id: 'stateActive'})
                                chatStatusActiveSet.remove(id)
                            else:
                                middleTupleStr = chatStatusMiddleMany.pop(id, None)
                                if middleTupleStr is not None:
                                    if middleStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        middleStatusText = textDict.pop('middleStatus')
                                    thumbsResult.update({id: (thumbItem[0], '<noscript><p class = "middle">{0}</p></noscript>'.format(middleStatusText), thumbItem[1])})
                                    activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                                else:
                                    thumbsResult.update({id: (thumbItem[0], thumbItem[1])})
                        else:
                            thumbsResult.update({id: (thumbItem[0], thumbItem[1])})
                else:
                    if viewOnlineStatus and viewActiveStatus:
                        if id in onlineUsersSet:
                            if onlineStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                onlineStatusText = textDict.pop('onlineStatus')
                            if id in chatStatusActiveSet:
                                if activeStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    activeStatusText = textDict.pop('activeStatus')
                                thumbsResult.update({id: (thumbItem[0], '<noscript><p class = "online">{0}</p><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), thumbItem[1])})
                                activeStatusDict.update({id: 'stateActive'})
                                chatStatusActiveSet.remove(id)
                            else:
                                middleTupleStr = chatStatusMiddleMany.pop(id, None)
                                if middleTupleStr is not None:
                                    if middleStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        middleStatusText = textDict.pop('middleStatus')
                                    thumbsResult.update({id: (thumbItem[0], '<noscript><p class = "online">{0}</p><p class = "middle">{1}</p></noscript>'.format(onlineStatusText, middleStatusText), thumbItem[1])})
                                    activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                                else:
                                    if leaveStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        leaveStatusText = textDict.pop('leaveStatus')
                                    thumbsResult.update({id: (thumbItem[0], '<noscript><p class = "online">{0}</p><p class = "leave">{1}</p></noscript>'.format(onlineStatusText, leaveStatusText), thumbItem[1])})
                            onlineStatusDict.update({id: 'online'})
                            onlineUsersSet.remove(id)
                        else:
                            iffyTimeStr = iffyTimeMany.pop(id, None)
                            if iffyTimeStr is not None:
                                if iffyStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    iffyStatusText = textDict.pop('uknownStatus')
                                if id in chatStatusActiveSet:
                                    if activeStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        activeStatusText = textDict.pop('activeStatus')
                                    thumbsResult.update({id: (thumbItem[0], '<noscript><p class = "iffy">{0}</p><p class = "active">{1}</p></noscript>'.format(iffyStatusText, activeStatusText), thumbItem[1])})
                                    activeStatusDict.update({id: 'stateActive'})
                                    chatStatusActiveSet.remove(id)
                                else:
                                    middleTupleStr = chatStatusMiddleMany.pop(id, None)
                                    if middleTupleStr is not None:
                                        if middleStatusText is None:
                                            if textDict is None:
                                                textDict = usersListText(lang = 'ru').getDict()
                                            middleStatusText = textDict.pop('middleStatus')
                                        thumbsResult.update({id: (thumbItem[0], '<noscript><p class = "iffy">{0}</p><p class = "middle">{1}</p></noscript>'.format(iffyStatusText, middleStatusText), thumbItem[1])})
                                        activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                                    else:
                                        if leaveStatusText is None:
                                            if textDict is None:
                                                textDict = usersListText(lang = 'ru').getDict()
                                            leaveStatusText = textDict.pop('leaveStatus')
                                        thumbsResult.update({id: (thumbItem[0], '<noscript><p class = "iffy">{0}</p><p class = "leave">{1}</p></noscript>'.format(iffyStatusText, leaveStatusText), thumbItem[1])})
                                onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                            else:
                                thumbsResult.update({id: (thumbItem[0], thumbItem[1])})
                    else:
                        if viewOnlineStatus:
                            if id in onlineUsersSet:
                                if onlineStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    onlineStatusText = textDict.pop('onlineStatus')
                                thumbsResult.update({id: (thumbItem[0], '<noscript><p class = "online">{0}</p></noscript>'.format(onlineStatusText), thumbItem[1])})
                                onlineStatusDict.update({id: 'online'})
                                onlineUsersSet.remove(id)
                            else:
                                iffyTimeStr = iffyTimeMany.pop(id, None)
                                if iffyTimeStr is not None:
                                    if iffyStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        iffyStatusText = textDict.pop('uknownStatus')
                                    thumbsResult.update({id: (thumbItem[0], '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText), thumbItem[1])})
                                    onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                                else:
                                    thumbsResult.update({id: (thumbItem[0], thumbItem[1])})
                        elif viewActiveStatus:
                            if id in chatStatusActiveSet:
                                if activeStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    activeStatusText = textDict.pop('activeStatus')
                                thumbsResult.update({id: (thumbItem[0], '<noscript><p class = "active">{0}</p></noscript>'.format(activeStatusText), thumbItem[1])})
                                activeStatusDict.update({id: 'stateActive'})
                                chatStatusActiveSet.remove(id)
                            else:
                                middleTupleStr = chatStatusMiddleMany.pop(id, None)
                                if middleTupleStr is not None:
                                    if middleStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        middleStatusText = textDict.pop('middleStatus')
                                    thumbsResult.update({id: (thumbItem[0], '<noscript><p class = "middle">{0}</p></noscript>'.format(middleStatusText), thumbItem[1])})
                                    activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                                else:
                                    thumbsResult.update({id: (thumbItem[0], thumbItem[1])})
                        else:
                            thumbsResult.update({id: (thumbItem[0], thumbItem[1])})
    if len(thumbItemsToSet.keys()) > 0:
        smallDialogThumbsCache.set_many(thumbItemsToSet)
        if len(privacyToSet.keys()) > 0:
            privacyCache.set_many(privacyToSet)
            if len(friendShipToSet.keys()) > 0:
                friendShipCache.set_many(friendShipToSet)
    return thumbsResult, onlineStatusDict, activeStatusDict

def getMainDialogThumbsXHR(idsSet, requestUser, requestUserId):
    ''' Вывод больших 'dialogThumbs' и 2 словарей со статусом (онлайн и активность) '''
    onlineUsersSet = set(caches['onlineUsers'].get_many(idsSet).keys())
    iffyTimeMany = caches['iffyUsersOnlineStatus'].get_many((idsSet - onlineUsersSet))
    chatStatusActiveSet = set(caches['chatStatusActive'].get_many((onlineUsersSet | set(iffyTimeMany.keys()))).keys())
    chatStatusMiddleMany = caches['chatStatusMiddle'].get_many((idsSet - chatStatusActiveSet))
    smallDialogThumbsCache = caches['dialogThumbs']
    smallDialogThumbsMany = smallDialogThumbsCache.get_many(idsSet)
    smallDialogThumbsManyKeys = set(smallDialogThumbsMany.keys())
    thumbsSurplus = idsSet - smallDialogThumbsManyKeys
    thumbsSurplus1 = set()
    for id in smallDialogThumbsManyKeys:
        thumb = smallDialogThumbsMany[id].get(requestUserId, None)
        if thumb is None:
            thumbsSurplus1.add(id)
        else:
            if 'showOnlineStatus' or 'showActiveStatus' not in thumb:
                thumbsSurplus1.add(id)
    thumbsSurplus = thumbsSurplus | thumbsSurplus1
    if len(thumbsSurplus) > 0:
        privacyCache = caches['privacyCache']
        privacyDictMany = privacyCache.get_many(thumbsSurplus)
        privacyManyKeys = set(privacyDictMany.keys())
        privacySurplus = thumbsSurplus - privacyManyKeys
        privacySurplus1 = set()
        for id in privacyManyKeys:
            privacyDict = privacyDictMany[id]
            if requestUserId not in privacyDict:
                privacySurplus1.add(id)
            else:
                if 'viewMainInfo' or 'viewTHPSInfo' or 'viewAvatar' or 'viewOnlineStatus' or 'viewActiveStatus' not in privacyDict:
                    privacySurplus1.add(id)
        privacySurplus = privacySurplus | privacySurplus1
        if len(privacySurplus) > 0:
            users = mainUserProfile.objects.filter(id__in = privacySurplus)
            privacySettings = userProfilePrivacy.objects.filter(user__in = users)
            friendsIdsCache = caches['friendsIdCache']
            friendsIds = friendsIdsCache.get(requestUserId, None)
            if friendsIds is None:
                friendsCache = caches['friendsCache']
                friends = friendsCache.get(requestUserId, None)
                if friends is None:
                    friends = requestUser.friends.all()
                    if friends.count():
                        friendsCache.set(requestUserId, friends)
                        friendsIdsQuerySet = friends.values("id")
                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                    else:
                        friendsCache.set(requestUserId, False)
                        friendsIds = set()
                else:
                    if friends is False:
                        friendsIds = set()
                    else:
                        friendsIds = {friend.id for friend in friends}
                friendsIdsCache.set(requestUserId, friendsIds)
            if len(friendsIds) > 0:
                friendsShipSurplus = privacySurplus - friendsIds
                if len(friendsShipSurplus) > 0:
                    friendShipCache = caches['friendShipStatusCache']
                    friendShipDict = friendShipCache.get(requestUserId, {})
                    friendShipManyKeys = set(friendShipDict.keys())
                    friendShipRequestsSurplus = friendsShipSurplus - friendShipManyKeys
                    if len(friendShipRequestsSurplus) > 0:
                        friendShipRequests = caches['friendshipRequests'].get_many(friendShipRequestsSurplus)
            else:
                if len(privacySurplus) > 0:
                    friendShipCache = caches['friendShipStatusCache']
                    friendShipDict = friendShipCache.get(requestUserId, {})
                    friendShipManyKeys = set(friendShipDict.keys())
                    friendShipRequestsSurplus = privacySurplus - friendShipManyKeys
                    if len(friendShipRequestsSurplus) > 0:
                        friendShipRequests = caches['friendshipRequests'].get_many(friendShipRequestsSurplus)
            privacyToSet = {}
            friendShipToSet = {}
    thumbItemsToSet = {}
    thumbsResult = {}
    onlineStatusDict = {}
    activeStatusDict = {}
    for id in idsSet:
        thumbs = smallDialogThumbsMany.get(id, None)
        if thumbs is None:
            user = users.get(id__exact = id)
            privacyDict = privacyDictMany.get(id, None)
            if privacyDict is None:
                if id in friendsIds:
                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                else:
                    friendShipStatus = friendShipDict.get(id, None)
                    if friendShipStatus is None:
                        if friendShipRequests is None:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            friendShipToSet.update({requestUserId: {id: False}})
                        else:
                            friendShipRequest = friendShipRequests.get(id, None)
                            if friendShipRequest is None:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                friendShipToSet.update({requestUserId: {id: False}})
                            elif friendShipRequest == "request":
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                friendShipToSet.update({requestUserId: {id: "request"}})
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                friendShipToSet.update({requestUserId: {id: "myRequest"}})
                    elif friendShipStatus is False:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                    elif friendShipStatus == "request":
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                    else:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                viewAvatar = privacyChecker.checkAvatarPrivacy()
                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                privacyToSet.update({id: {requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
            else:
                userPrivacyDict = privacyDict.get(requestUserId, None)
                if userPrivacyDict is None:
                    if id in friendsIds:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                    else:
                        friendShipStatus = friendShipDict.get(id, None)
                        if friendShipStatus is None:
                            if friendShipRequests is None:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                friendShipToSet.update({requestUserId: {id: False}})
                            else:
                                friendShipRequest = friendShipRequests.get(id, None)
                                if friendShipRequest is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                elif friendShipRequest == "request":
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: "request"}})
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: "myRequest"}})
                        elif friendShipStatus is False:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        elif friendShipStatus == "request":
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                    privacyDict.update({requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
                    privacyToSet.update({id: privacyDict})
                else:
                    viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                    viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                    viewAvatar = userPrivacyDict.get('viewAvatar', None)
                    viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                    viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                    if viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None or viewActiveStatus is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            friendShipStatus = friendShipDict.get(id, None)
                            if friendShipStatus is None:
                                if friendShipRequests is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                else:
                                    friendShipRequest = friendShipRequests.get(id, None)
                                    if friendShipRequest is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    elif friendShipRequest == "request":
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "request"}})
                                    else:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "myRequest"}})
                            elif friendShipStatus is False:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            elif friendShipStatus == "request":
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        if viewMainInfo is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                        if viewTHPSInfo is None:
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                        if viewAvatar is None:
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            userPrivacyDict.update({'viewAvatar': viewAvatar})
                        if viewOnlineStatus is None:
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                        if viewActiveStatus is None:
                            viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                            userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                        privacyToSet.update({id: privacyDict})
            if viewMainInfo:
                firstName = user.first_name or None
                lastName = user.last_name or None
                if firstName and lastName:
                    name = '{0} {1}'.format(firstName, lastName)
                    smallName = user.username
                else:
                    if firstName:
                        name = firstName
                        smallName = user.username
                    else:
                        name = user.username
                        smallName = None
            else:
                name = user.username
                smallName = None
            if viewAvatar:
                avatar = user.avatarThumbMiddleSmall or None
                if avatar is not None:
                    if smallName:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, smallName)
                    else:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
            else:
                if smallName:
                    itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                else:
                    itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
            mainThumbItem = (itemString0, '</div>')
            if viewOnlineStatus:
                if id in onlineUsersSet:
                    onlineStatusDict.update({id: 'online'})
                    onlineUsersSet.remove(id)
                else:
                    iffyTimeStr = iffyTimeMany.pop(id, None)
                    if iffyTimeStr is not None:
                        onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
            if viewActiveStatus:
                if id in chatStatusActiveSet:
                    activeStatusDict.update({id: 'stateActive'})
                    chatStatusActiveSet.remove(id)
                else:
                    middleTupleStr = chatStatusMiddleMany.pop(id, None)
                    if middleTupleStr is not None:
                        activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
            thumbsResult.update({id: mainThumbItem})
            thumbItemsToSet.update({id: {requestUserId: {'item': mainThumbItem, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus}}})
        else:
            thumb = thumbs.get(requestUserId, None)
            if thumb is None:
                user = users.get(id__exact = id)
                privacyDict = privacyDictMany.get(id, None)
                if privacyDict is None:
                    if id in friendsIds:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                    else:
                        friendShipStatus = friendShipDict.get(id, None)
                        if friendShipStatus is None:
                            if friendShipRequests is None:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                friendShipToSet.update({requestUserId: {id: False}})
                            else:
                                friendShipRequest = friendShipRequests.get(id, None)
                                if friendShipRequest is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                elif friendShipRequest == "request":
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: "request"}})
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: "myRequest"}})
                        elif friendShipStatus is False:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        elif friendShipStatus == "request":
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                    privacyToSet.update({id: {requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
                else:
                    userPrivacyDict = privacyDict.get(requestUserId, None)
                    if userPrivacyDict is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            friendShipStatus = friendShipDict.get(id, None)
                            if friendShipStatus is None:
                                if friendShipRequests is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                else:
                                    friendShipRequest = friendShipRequests.get(id, None)
                                    if friendShipRequest is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    elif friendShipRequest == "request":
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "request"}})
                                    else:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "myRequest"}})
                            elif friendShipStatus is False:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            elif friendShipStatus == "request":
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                        privacyDict.update({requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
                        privacyToSet.update({id: privacyDict})
                    else:
                        viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                        viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                        viewAvatar = userPrivacyDict.get('viewAvatar', None)
                        viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                        viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                        if viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None or viewActiveStatus is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                friendShipStatus = friendShipDict.get(id, None)
                                if friendShipStatus is None:
                                    if friendShipRequests is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    else:
                                        friendShipRequest = friendShipRequests.get(id, None)
                                        if friendShipRequest is None:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: False}})
                                        elif friendShipRequest == "request":
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: "request"}})
                                        else:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: "myRequest"}})
                                elif friendShipStatus is False:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                elif friendShipStatus == "request":
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            if viewMainInfo is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                            if viewTHPSInfo is None:
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                            if viewAvatar is None:
                                viewAvatar = privacyChecker.checkAvatarPrivacy()
                                userPrivacyDict.update({'viewAvatar': viewAvatar})
                            if viewOnlineStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                            if viewActiveStatus is None:
                                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                            privacyToSet.update({id: privacyDict})
                if viewMainInfo:
                    firstName = user.first_name or None
                    lastName = user.last_name or None
                    if firstName and lastName:
                        name = '{0} {1}'.format(firstName, lastName)
                        smallName = user.username
                    else:
                        if firstName:
                            name = firstName
                            smallName = user.username
                        else:
                            name = user.username
                            smallName = None
                else:
                    name = user.username
                    smallName = None
                if viewAvatar:
                    avatar = user.avatarThumbMiddleSmall or None
                    if avatar is not None:
                        if smallName:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, smallName)
                        else:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                mainThumbItem = (itemString0, '</div>')
                if viewOnlineStatus:
                    if id in onlineUsersSet:
                        onlineStatusDict.update({id: 'online'})
                        onlineUsersSet.remove(id)
                    else:
                        iffyTimeStr = iffyTimeMany.pop(id, None)
                        if iffyTimeStr is not None:
                            onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                if viewActiveStatus:
                    if id in chatStatusActiveSet:
                        activeStatusDict.update({id: 'stateActive'})
                        chatStatusActiveSet.remove(id)
                    else:
                        middleTupleStr = chatStatusMiddleMany.pop(id, None)
                        if middleTupleStr is not None:
                            activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                thumbsResult.update({id: mainThumbItem})
                thumbs.update({requestUserId: {'item': mainThumbItem, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus}})
                thumbItemsToSet.update({id: thumbs})
            else:
                showOnlineStatus = thumb.get('showOnlineStatus', None)
                showActiveStatus = thumb.get('showActiveStatus', None)
                thumbsResult.update({id: thumb['item']})
                if showOnlineStatus is None or showActiveStatus is None:
                    privacyDict = privacyDictMany.get(id, None)
                    if privacyDict is None:
                        user = users.get(id__exact = id)
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            friendShipStatus = friendShipDict.get(id, None)
                            if friendShipStatus is None:
                                if friendShipRequests is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                else:
                                    friendShipRequest = friendShipRequests.get(id, None)
                                    if friendShipRequest is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    elif friendShipRequest == "request":
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "request"}})
                                    else:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "myRequest"}})
                            elif friendShipStatus is False:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            elif friendShipStatus == "request":
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        if showOnlineStatus is None and showActiveStatus is None:
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                            privacyToSet.update({id: {requestUserId: {'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
                            thumb.update({'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus})
                        else:
                            if showOnlineStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                privacyToSet.update({id: {requestUserId: {'viewOnlineStatus': viewActiveStatus}}})
                                thumb.update({'showOnlineStatus': viewOnlineStatus})
                            elif showActiveStatus is None:
                                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                privacyToSet.update({id: {requestUserId: {'viewActiveStatus': viewActiveStatus}}})
                                thumb.update({'showActiveStatus': viewActiveStatus})
                    else:
                        userPrivacyDict = privacyDict.get(requestUserId, None)
                        if userPrivacyDict is None:
                            user = users.get(id__exact = id)
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                friendShipStatus = friendShipDict.get(id, None)
                                if friendShipStatus is None:
                                    if friendShipRequests is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    else:
                                        friendShipRequest = friendShipRequests.get(id, None)
                                        if friendShipRequest is None:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: False}})
                                        elif friendShipRequest == "request":
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: "request"}})
                                        else:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: "myRequest"}})
                                elif friendShipStatus is False:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                elif friendShipStatus == "request":
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            if showOnlineStatus is None and showActiveStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                privacyDict.update({requestUserId: {'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
                                thumb.update({'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus})
                            else:
                                if showOnlineStatus is None:
                                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                    privacyDict.update({requestUserId: {'viewOnlineStatus': viewOnlineStatus}})
                                    thumb.update({'showOnlineStatus': viewOnlineStatus})
                                elif showActiveStatus is None:
                                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                    privacyDict.update({requestUserId: {'viewActiveStatus': viewActiveStatus}})
                                    thumb.update({'showActiveStatus': viewActiveStatus})
                            privacyToSet.update({id: privacyDict})
                        else:
                            viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                            viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                            if viewOnlineStatus is None or viewActiveStatus is None:
                                user = users.get(id__exact = id)
                                if id in friendsIds:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                else:
                                    friendShipStatus = friendShipDict.get(id, None)
                                    if friendShipStatus is None:
                                        if friendShipRequests is None:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: False}})
                                        else:
                                            friendShipRequest = friendShipRequests.get(id, None)
                                            if friendShipRequest is None:
                                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                                friendShipToSet.update({requestUserId: {id: False}})
                                            elif friendShipRequest == "request":
                                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                                friendShipToSet.update({requestUserId: {id: "request"}})
                                            else:
                                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                                friendShipToSet.update({requestUserId: {id: "myRequest"}})
                                    elif friendShipStatus is False:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    elif friendShipStatus == "request":
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    else:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                if viewOnlineStatus is None and viewActiveStatus is None:
                                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                    userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus})
                                    thumb.update({'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus})
                                else:
                                    if viewOnlineStatus is None:
                                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                        userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                                        thumb.update({'showOnlineStatus': viewOnlineStatus})
                                    elif showActiveStatus is None:
                                        viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                        userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                                        thumb.update({'showActiveStatus': viewActiveStatus})
                                privacyToSet.update({id: privacyDict})
                    thumbs.update({requestUserId: thumb})
                    thumbItemsToSet.update({id: thumbs})
                    if showOnlineStatus:
                        if id in onlineUsersSet:
                            onlineStatusDict.update({id: 'online'})
                        else:
                            iffyTimeStr = iffyTimeMany.pop(id, None)
                            if iffyTimeStr is not None:
                                onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                    if showActiveStatus:
                        if id in chatStatusActiveSet:
                            activeStatusDict.update({id: 'stateActive'})
                        else:
                            middleTupleStr = chatStatusMiddleMany.pop(id, None)
                            if middleTupleStr is not None:
                                activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                else:
                    if showOnlineStatus:
                        if id in onlineUsersSet:
                            onlineStatusDict.update({id: 'online'})
                        else:
                            iffyTimeStr = iffyTimeMany.pop(id, None)
                            if iffyTimeStr is not None:
                                onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                    if showActiveStatus:
                        if id in chatStatusActiveSet:
                            activeStatusDict.update({id: 'stateActive'})
                        else:
                            middleTupleStr = chatStatusMiddleMany.pop(id, None)
                            if middleTupleStr is not None:
                                activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
    if len(thumbItemsToSet.keys()) > 0:
        smallDialogThumbsCache.set_many(thumbItemsToSet)
        if len(privacyToSet.keys()) > 0:
            privacyCache.set_many(privacyToSet)
            if len(friendShipToSet.keys()) > 0:
                friendShipCache.set_many(friendShipToSet)
    return thumbsResult, onlineStatusDict, activeStatusDict

def getStatus(idsTuple, requestUser, requestUserId, auth):
    ''' Вывод словарей статуса. Используется для групповых диалогов. '''
    idsSet = set(idsTuple)
    onlineUsers = set(caches['onlineUsers'].get_many(idsSet).keys())
    iffyUsers = caches['iffyUsersOnlineStatus'].get_many((idsSet - onlineUsers))
    usersToGetActiveStatus = onlineUsers | set(iffyUsers.keys())
    if len(usersToGetActiveStatus) > 0:
        activeUsers = set(caches['chatStatusActive'].get_many(usersToGetActiveStatus).keys())
        middleUsers = caches['chatStatusMiddle'].get_many((usersToGetActiveStatus - activeUsers))
        onlineStatusDict = {}
        activeStatusDict = {}
        privacyCache = caches['privacyCache']
        privacyMany = privacyCache.get_many(usersToGetActiveStatus)
        missingPrivacyKeysSet = usersToGetActiveStatus - set(privacyMany.keys())
        for id, privacyDict in privacyMany.items():
            privacyDictUser = privacyDict.get(requestUserId)
            if privacyDictUser is None:
                missingPrivacyKeysSet.add(id)
            else:
                keys = privacyDictUser.keys()
                if 'viewOnlineStatus' not in keys or 'viewActiveStatus' not in keys:
                    missingPrivacyKeysSet.add(id)
        isMissingPrivacyKeys = len(missingPrivacyKeysSet) > 0
        if isMissingPrivacyKeys:
            users = mainUserProfile.object.filter(id__in = missingPrivacyKeysSet)
            privacys = userProfilePrivacy.objects.filter(user__in = users)
            friendsIdsCache = caches['friendsIdCache']
            friendsIdsSet = friendsIdsCache.get(requestUserId, None)
            if friendsIdsSet is None:
                friendsCache = caches['friendsCache']
                friendsQuerySet = friendsCache.get(requestUserId, None)
                if friendsQuerySet is None:
                    friendsQuerySet = requestUser.friends.all()
                    if friendsQuerySet.count() > 0:
                        friendsCache.set(requestUserId, friendsQuerySet)
                        friendsIdsSet = {friends.get("id") for friend in friends.values("id")}
                    else:
                        friendsCache.set(requestUserId, False)
                        friendsIdsSet = False
                else:
                    if friendsQuerySet is False:
                        friendsIds = False
                    else:
                        friendsIds = {friend.get("id") for friend in friendsQuerySet.values("id")}
                friendsIdsCache.set(requestUserId, friendsIds)
            friendShipStatusSurplus = missingPrivacyKeysSet - friendsIds
            if len(friendShipStatusSurplus) > 0:
                friendShipStatusCache = caches['friendShipStatusCache']
                friendShipStatusMany = friendShipStatusCache.get_many(friendShipStatusSurplus)
                friendShipRerquestsSurplus = friendShipStatusSurplus - set(friendShipStatusMany.keys())
                for id, friendShipDict in friendShipStatusMany.items():
                    if requestUserId not in friendShipDict.keys():
                        friendShipRerquestsSurplus.add(id)
                isFriendShipRequestsSurplus = len(friendShipRequestsSurplus) > 0
                if isFriendShipRequestsSurplus:
                    friendShipRerquestsSurplus.add(requestUserId)
                    friendShipRequestsMany = caches['friendshipRequests'].get_many(friendShipRerquestsSurplus)
                    friendShipToSet = {}
            privacyToSet = {}
        for id in usersToGetActiveStatus:
            privacyDict = privacyMany.get(id, None)
            if privacyDict is None:
                if id in friendsIds:
                    privacyChecker = checkPrivacy(privacy = privacys.get(user__exact = users.get(id__exact = id)), isFriend = True, isAuthenticated = auth)
                else:
                    friendShipStatusDict = friendShipStatusMany.get(id, None)
                    if friendShipStatusDict is None:
                        friendShipStatus = False
                        friendShipRequestsListList = friendShipRequestsMany.get(id, None)
                        if friendShipRequestsListList is not None:
                            for friendShipRequestsList in friendShipRequestsListList:
                                if friendShipRequestsList[0] == requestUserId:
                                    friendShipStatus = "myRequest"
                        if friendShipStatus is False:
                            friendShipRequestsListList = friendShipRequestsMany.get(requestUserId, None)
                            if friendShipRequestsListList is not None:
                                for friendShipRequestsList in friendShipRequestsListList:
                                    if friendShipRequestsList[0] == id:
                                        friendShipStatus = "request"
                        privacyChecker = checkPrivacy(privacy = privacys.get(user__exact = users.get(id__exact = id)), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = auth)
                        friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                    else:
                        friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                        if friendShipStatus is None:
                            friendShipStatus = False
                            friendShipRequestsListList = friendShipRequestsMany.get(id, None)
                            if friendShipRequestsListList is not None:
                                for friendShipRequestsList in friendShipRequestsListList:
                                    if friendShipRequestsList[0] == requestUserId:
                                        friendShipStatus = "myRequest"
                            if friendShipStatus is False:
                                friendShipRequestsListList = friendShipRequestsMany.get(requestUserId, None)
                                if friendShipRequestsListList is not None:
                                    for friendShipRequestsList in friendShipRequestsListList:
                                        if friendShipRequestsList[0] == id:
                                            friendShipStatus = "request"
                            privacyChecker = checkPrivacy(privacy = privacys.get(user__exact = users.get(id__exact = id)), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = auth)
                            friendShipStatusDict.update({requestUserId: friendShipStatus})
                            friendShipToSet.update({id: friendShipStatusDict})
                        else:
                            privacyChecker = checkPrivacy(privacy = privacys.get(user__exact = users.get(id__exact = id)), isFriend = friendShipStatus, isAuthenticated = auth)
                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                privacyToSet.update({id: {requestUserId: {'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
            else:
                privacyUserDict = privacyDict.get(requestUserId, None)
                if privacyUserDict is None:
                    if id in friendsIds:
                        privacyChecker = checkPrivacy(privacy = privacys.get(user__exact = users.get(id__exact = id)), isFriend = True, isAuthenticated = auth)
                    else:
                        friendShipStatusDict = friendShipStatusMany.get(id, None)
                        if friendShipStatusDict is None:
                            friendShipStatus = False
                            friendShipRequestsListList = friendShipRequestsMany.get(id, None)
                            if friendShipRequestsListList is not None:
                                for friendShipRequestsList in friendShipRequestsListList:
                                    if friendShipRequestsList[0] == requestUserId:
                                        friendShipStatus = "myRequest"
                            if friendShipStatus is False:
                                friendShipRequestsListList = friendShipRequestsMany.get(requestUserId, None)
                                if friendShipRequestsListList is not None:
                                    for friendShipRequestsList in friendShipRequestsListList:
                                        if friendShipRequestsList[0] == id:
                                            friendShipStatus = "request"
                            privacyChecker = checkPrivacy(privacy = privacys.get(user__exact = users.get(id__exact = id)), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = auth)
                            friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                        else:
                            friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                            if friendShipStatus is None:
                                friendShipStatus = False
                                friendShipRequestsListList = friendShipRequestsMany.get(id, None)
                                if friendShipRequestsListList is not None:
                                    for friendShipRequestsList in friendShipRequestsListList:
                                        if friendShipRequestsList[0] == requestUserId:
                                            friendShipStatus = "myRequest"
                                if friendShipStatus is False:
                                    friendShipRequestsListList = friendShipRequestsMany.get(requestUserId, None)
                                    if friendShipRequestsListList is not None:
                                        for friendShipRequestsList in friendShipRequestsListList:
                                            if friendShipRequestsList[0] == id:
                                                friendShipStatus = "request"
                                privacyChecker = checkPrivacy(privacy = privacys.get(user__exact = users.get(id__exact = id)), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = auth)
                                friendShipStatusDict.update({requestUserId: friendShipStatus})
                                friendShipToSet.update({id: friendShipStatusDict})
                            else:
                                privacyChecker = checkPrivacy(privacy = privacys.get(user__exact = users.get(id__exact = id)), isFriend = friendShipStatus, isAuthenticated = auth)
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                    privacyDict.update({requestUserId: {'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
                    privacyToSet.update({id: privacyDict})
                else:
                    viewOnlineStatus = privacyUserDict.get('viewOnlineStatus', None)
                    viewActiveStatus = privacyUserDict.get('viewActiveStatus', None)
                    if viewActiveStatus is None or viewActiveStatus is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacys.get(user__exact = users.get(id__exact = id)), isFriend = True, isAuthenticated = auth)
                        else:
                            friendShipStatusDict = friendShipStatusMany.get(id, None)
                            if friendShipStatusDict is None:
                                friendShipStatus = False
                                friendShipRequestsListList = friendShipRequestsMany.get(id, None)
                                if friendShipRequestsListList is not None:
                                    for friendShipRequestsList in friendShipRequestsListList:
                                        if friendShipRequestsList[0] == requestUserId:
                                            friendShipStatus = "myRequest"
                                if friendShipStatus is False:
                                    friendShipRequestsListList = friendShipRequestsMany.get(requestUserId, None)
                                    if friendShipRequestsListList is not None:
                                        for friendShipRequestsList in friendShipRequestsListList:
                                            if friendShipRequestsList[0] == id:
                                                friendShipStatus = "request"
                                privacyChecker = checkPrivacy(privacy = privacys.get(user__exact = users.get(id__exact = id)), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = auth)
                                friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                            else:
                                friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                                if friendShipStatus is None:
                                    friendShipStatus = False
                                    friendShipRequestsListList = friendShipRequestsMany.get(id, None)
                                    if friendShipRequestsListList is not None:
                                        for friendShipRequestsList in friendShipRequestsListList:
                                            if friendShipRequestsList[0] == requestUserId:
                                                friendShipStatus = "myRequest"
                                    if friendShipStatus is False:
                                        friendShipRequestsListList = friendShipRequestsMany.get(requestUserId, None)
                                        if friendShipRequestsListList is not None:
                                            for friendShipRequestsList in friendShipRequestsListList:
                                                if friendShipRequestsList[0] == id:
                                                    friendShipStatus = "request"
                                    privacyChecker = checkPrivacy(privacy = privacys.get(user__exact = users.get(id__exact = id)), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = auth)
                                    friendShipStatusDict.update({requestUserId: friendShipStatus})
                                    friendShipToSet.update({id: friendShipStatusDict})
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacys.get(user__exact = users.get(id__exact = id)), isFriend = friendShipStatus, isAuthenticated = auth)
                        if viewOnlineStatus is None:
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            privacyUserDict.update({'viewOnlineStatus': viewOnlineStatus})
                        if viewActiveStatus is None:
                            viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                            privacyUserDict.update({'viewActiveStatus': viewActiveStatus})
                        privacyToSet.update({id: privacyDict})
            if viewOnlineStatus or viewActiveStatus:
                if viewOnlineStatus and viewActiveStatus:
                    if id in onlineUsers:
                        if id in activeUsers:
                            activeStatusDict.update({id: 'stateActive'})
                            activeUsers.remove(id)
                        else:
                            middleStatus = middleUsers.pop(id, None)
                            if middleStatus is not None:
                                activeStatusDict.update({id: {'stateMiddle': middleStatus}})
                        onlineStatusDict.update({id: 'online'})
                        onlineUsers.remove(id)
                    else:
                        iffyStatus = iffyUsers.pop(id, None)
                        if iffyStatus is not None:
                            if id in activeUsers:
                                activeStatusDict.update({id: 'stateActive'})
                                activeUsers.remove(id)
                            else:
                                middleStatus = middleUsers.pop(id, None)
                                if middleStatus is not None:
                                    activeStatusDict.update({id: {'stateMiddle': middleStatus}})
                            onlineStatusDict.update({id: {'iffy': iffyStatus}})
                else:
                    if viewOnlineStatus:
                        if id in onlineUsers:
                            onlineStatusDict.update({id: 'online'})
                            onlineUsers.remove(id)
                        else:
                            iffyStatus = iffyUsers.pop(id, None)
                            if iffyStatus is not None:
                                onlineStatusDict.update({id: {'iffy': iffyStatus}})
                    else:
                        if id in activeUsers:
                            activeStatusDict.update({id: 'stateActive'})
                            activeUsers.remove(id)
                        else:
                            middleStatus = middleUsers.pop(id, None)
                            if middleStatus is not None:
                                activeStatusDict.update({id: {'stateMiddle': middleStatus}})
        if isMissingPrivacyKeys:
            privacyCache.set_many(privacyToSet)
            if isFriendShipRequestsSurplus:
                friendShipStatusCache.set_many(friendShipToSet)
        return onlineStatusDict, activeStatusDict
    return None

class cachedFragments():

    @property
    @lru_cache(maxsize = 16)
    def type1(self):
        menu = OrderedDict({
            0: {
                'textname': 'Диалоги',
                1: {
                    'textname': 'Мой профиль',
                    'url': reverse('profile', kwargs = {'currentUser': 'im'}),
                    0: {
                        'textname': 'Друзья',
                        0: {
                            'textname': 'Все',
                            'url': reverse('friends', kwargs = {'user': 'my', 'category': 'all', 'sep': '',  'page': ''})
                            },
                        1: {
                            'textname': 'Сейчас в сети',
                            'url': reverse('friends', kwargs = {'user': 'my', 'category': 'online', 'sep': '',  'page': ''})
                            }
                        },
                    1: {
                        'textname': 'Записи в блоге'
                        },
                    2: {
                        'textname': 'Записи в libraryTHPS'
                        },
                    3: {
                        'textname': 'Редактировать',
                        0: {
                            'textname': 'Основные',
                            'url': reverse('profileEditUrl', kwargs = {'editType': 'main'})
                            },
                        1: {
                            'textname': 'Дополнительно',
                            'url': reverse('profileEditUrl', kwargs = {'editType': 'detail'})
                            },
                        2: {
                            'textname': 'Обратная связь',
                            'url': reverse('profileEditUrl', kwargs = {'editType': 'contacts'})
                            },
                        3: {
                            'textname': 'Настройки приватности',
                            'url': reverse('editPrivacyUrl', kwargs = {'editPlace': 'profile'}),
                            0: {
                                'textname': 'Права для исключительных пользователей',
                                'url': reverse('editPrivacyExclusiveUrl', kwargs = {'editPlace': 'profile'})
                                }
                            },
                        4: {
                            'textname': 'THPS профиль',
                            'url': reverse('profileEditUrl', kwargs = {'editType': 'thps'})
                            }
                        },
                    },
                2: {
                    'textname': 'Пользователи',
                    0: {
                        'textname': 'Все',
                        'url': reverse('users', kwargs = {'category': 'all', 'sep': '', 'page': ''})
                        },
                    1: {
                        'textname': 'Сейчас в сети',
                        'url': reverse('users', kwargs = {'category': 'online', 'sep': '', 'page': ''})
                        }
                    },
                3: {
                    'textname': 'Выход',
                    'url': reverse("logOut")
                    }
            }
            })
        return menu

    @property
    @lru_cache(maxsize = 16)
    def type1Serialized(self):
        menu = self.type1
        if isinstance(menu, OrderedDict):
            return json.dumps(menu)
        return json.dumps(menu())

    def menuGetter(self, menuType):
        menuOrMethod = getattr(self, menuType)
        if isinstance(menuOrMethod, OrderedDict):
            return menuOrMethod
        if isinstance(menuOrMethod, str):
            return menuOrMethod
        return menuOrMethod()

@login_decorator
def dialogPage(request, sep, page):
    if request.method == "GET":
        user = request.user
        id = str(user.id)
        dialogsCache = caches['dialogsCache']
        dialogsDictDict = dialogsCache.get(id, None)
        if dialogsDictDict is None:
            myDialogs = dialogMessage.objects.filter(users__exact = user).order_by("-lastUpdate")
            myDialogsIterable = myDialogs.values("id", "type")
            allDialogIds = myDialogsIterable.values("id")
            allMessagesPersonal = personalMessage.objects.filter(inDialog_id__in = allDialogIds)
            allMessagesGroup = groupMessage.objects.filter(inDialog_id__in = allDialogIds)
            dialogsDict = OrderedDict()
            lastMessagesDict = {}
            for dialog in myDialogsIterable:
                dialogId = str(dialog.get("id"))
                dialogsDict.update({dialogId: myDialogs.get(id__exact = dialogId)})
                if dialog.get("type") is True:
                    messages = allMessagesPersonal.filter(inDialog_id__exact = dialogId).order_by("-time")
                else:
                    messages = allMessagesGroup.filter(Q(inDialog_id__exact = dialogId) & Q(members_id__exact = id)).order_by("-time")
                if messages.count():
                    lastMessagesDict.update({dialogId: messages[0:6:-1]})
                else:
                    lastMessagesDict.update({dialogId: None})
            dialogsCache.set(id, {"dialogs": dialogsDict, "last": lastMessagesDict})
        else:
            dialogsDict = dialogsDictDict.get("dialogs", None)
            lastMessagesDict = dialogsDictDict.get("last", None)
            if dialogsDict is None and lastMessagesDict is None:
                myDialogs = dialogMessage.objects.filter(users__exact = user).order_by("-lastUpdate")
                myDialogsIterable = myDialogs.values("id", "type")
                allDialogIds = myDialogsIterable.values("id")
                allMessagesPersonal = personalMessage.objects.filter(inDialog_id__in = allDialogIds)
                allMessagesGroup = groupMessage.objects.filter(inDialog_id__in = allDialogIds)
                dialogsDict = OrderedDict()
                lastMessagesDict = {}
                for dialog in myDialogsIterable:
                    dialogId = str(dialog.get("id"))
                    dialogsDict.update({dialogId: myDialogs.get(id__exact = dialogId)})
                    if dialog.get("type") is True:
                        messages = allMessagesPersonal.filter(inDialog_id__exact = dialogId).order_by("-time")
                    else:
                        messages = allMessagesGroup.filter(Q(inDialog_id__exact = dialogId) & Q(members_id__exact = id)).order_by("-time")
                    if messages.count():
                        lastMessagesDict.update({dialogId: messages[0:6:-1]})
                    else:
                        lastMessagesDict.update({dialogId: None})
                dialogsCache.set(id, {"dialogs": dialogsDict, "last": lastMessagesDict})
            else:
                if dialogsDict is None or lastMessagesDict is None:
                    if dialogsDict is None:
                        myDialogs = dialogMessage.objects.filter(users__exact = user).order_by("-lastUpdate")
                        allDialogIds = myDialogsIterable.values("id")
                        dialogsDict = OrderedDict({str(dialogId.get("id")): myDialogs.get(id__exact = dialogId) for dialogId in allDialogIds})
                        dialogsDictDict.update({"dialogs": dialogsDict})
                    elif lastMessagesDict is None:
                        allDialogIds = dialogsDict.keys()
                        allMessagesPersonal = personalMessage.objects.filter(inDialog_id__in = allDialogIds)
                        allMessagesGroup = groupMessage.objects.filter(inDialog_id__in = allDialogIds)
                        lastMessagesDict = {}
                        for dialogId in allDialogIds:
                            messagesPersonal = allMessagesPersonal.filter(inDialog_id__exact = dialogId).order_by("-time")
                            if messagesPersonal.count():
                                lastMessagesDict.update({dialogId: messagesPersonal[0:6:-1]})
                            else:
                                messagesGroup = allMessagesGroup.filter(Q(inDialog_id__exact = dialogId) & Q(members_id__exact = id)).order_by("-time")
                                if messagesGroup.count():
                                    lastMessagesDict.update({dialogId: messagesGroup[0:6:-1]})
                                else:
                                    lastMessagesDict.update({dialogId: None})
                        dialogsDictDict.update({"last": lastMessagesDict})
                    dialogsCache.set(id, dialogsDictDict)
        dialogsDictValues = dialogsDict.values()
        if len(dialogsDictValues) > 0:
            dialogsPaginator = Paginator(tuple(dialogsDictValues), 10)
            if request.is_ajax():
                get = request.GET
                try:
                    pageItems = dialogsPaginator.page(get.get("page", 1))
                except EmptyPage:
                    pageItems = dialogsPaginator.page(dialogsPaginator.num_pages)
                dialogsTupleQuerySet = pageItems.object_list
                if 'from-dialog' in get:
                    dialogsList = []
                    idsToGetSmallThumbsAll = set()
                    personalDialogsIds = set()
                    groupDialogsIds = set()
                    for dialog in dialogsTupleQuerySet:
                        dialogId = str(dialog.id)
                        lastMessagesList = lastMessagesDict.get(dialogId, None)
                        if lastMessagesList is not None and len(lastMessagesList) > 0:
                            if dialog.type is True:
                                lastMessagePersonalQuerySet = lastMessagesList[-1]
                                targetUserQuerySet = lastMessagePersonalQuerySet.to
                                senderUserQuerySet = lastMessagePersonalQuerySet.by
                                senderId = senderUserQuerySet.id
                                if senderId == id:
                                    opponentId = targetUserQuerySet.id
                                    idsToGetSmallThumbsAll.add(opponentId)
                                    imagesInstance = lastMessagePersonalQuerySet.imagesAttachment
                                    if imagesInstance is not None:
                                        dialogsList.append({'type': True, 'dialog': dialogId, 'body': lastMessagePersonalQuerySet.body, 'time': str(lastMessagePersonalQuerySet.time), 'by': senderId, 'opponent': opponentId, 'link': reverse("personalMessage", kwargs = {'toUser': targetUserQuerySet.profileUrl}), 'id': str(lastMessagePersonalQuerySet.id), 'images': imagesInstance.itemsLength})
                                    else:
                                        dialogsList.append({'type': True, 'dialog': dialogId, 'body': lastMessagePersonalQuerySet.body, 'time': str(lastMessagePersonalQuerySet.time), 'by': senderId, 'opponent': opponentId, 'link': reverse("personalMessage", kwargs = {'toUser': targetUserQuerySet.profileUrl}), 'id': str(lastMessagePersonalQuerySet.id)})
                                else:
                                    idsToGetSmallThumbsAll.add(senderId)
                                    imagesInstance = lastMessagePersonalQuerySet.imagesAttachment
                                    if imagesInstance is not None:
                                        dialogsList.append({'type': True, 'dialog': dialogId, 'body': lastMessagePersonalQuerySet.body, 'time': str(lastMessagePersonalQuerySet.time), 'by': senderId, 'opponent': senderId, 'link': reverse("personalMessage", kwargs = {'toUser': senderUserQuerySet.profileUrl}), 'id': str(lastMessagePersonalQuerySet.id), 'images': imagesInstance.itemsLength})
                                    else:
                                        dialogsList.append({'type': True, 'dialog': dialogId, 'body': lastMessagePersonalQuerySet.body, 'time': str(lastMessagePersonalQuerySet.time), 'by': senderId, 'opponent': senderId, 'link': reverse("personalMessage", kwargs = {'toUser': senderUserQuerySet.profileUrl}), 'id': str(lastMessagePersonalQuerySet.id)})
                                personalDialogsIds.add(dialogId)
                            else:
                                lastMessageGroupQuerySet = lastMessagesList[-1]
                                imagesInstance = lastMessageGroupQuerySet.imagesAttachment
                                groupDialogAttributesQuerySet = dialog.attrs
                                groupDialogUsersIds = set(dialog.get("id") for dialog in dialog.users.values("id"))
                                groupDialogAvatar = groupDialogAttributesQuerySet.dialogsMinimizedThumb or None
                                lastMessageBy = lastMessageGroupQuerySet.by_id
                                idsToGetSmallThumbsAll.update(groupDialogUsersIds)
                                if lastMessageBy not in groupDialogUsersIds:
                                    idsToGetSmallThumbsAll.add(lastMessageBy)
                                if imagesInstance is not None:
                                    if groupDialogAvatar:
                                        dialogsList.append({'type': False, 'dialog': dialogId, 'by': lastMessageBy, 'name': groupDialogAttributesQuerySet.name, 'avatar': groupDialogAvatar.url, 'body': lastMessageGroupQuerySet.body, 'link': reverse("groupMessage", kwargs = {'dialogSlug': groupDialogAttributesQuerySet.slug}), 'id': str(lastMessageGroupQuerySet.id), 'images': imagesInstance.itemsLength, 'users': tuple(groupDialogUsersIds)})
                                    else:
                                        dialogsList.append({'type': False, 'dialog': dialogId, 'by': lastMessageBy, 'name': groupDialogAttributesQuerySet.name, 'body': lastMessageGroupQuerySet.body, 'link': reverse("groupMessage", kwargs = {'dialogSlug': groupDialogAttributesQuerySet.slug}), 'id': str(lastMessageGroupQuerySet.id), 'images': imagesInstance.itemsLength, 'users': tuple(groupDialogUsersIds)})
                                else:
                                    if groupDialogAvatar:
                                        dialogsList.append({'type': False, 'dialog': dialogId, 'by': lastMessageBy, 'name': groupDialogAttributesQuerySet.name, 'avatar': groupDialogAvatar.url, 'body': lastMessageGroupQuerySet.body, 'link': reverse("groupMessage", kwargs = {'dialogSlug': groupDialogAttributesQuerySet.slug}), 'id': str(lastMessageGroupQuerySet.id), 'users': tuple(groupDialogUsersIds)})
                                    else:
                                        dialogsList.append({'type': False, 'dialog': dialogId, 'by': lastMessageBy, 'name': groupDialogAttributesQuerySet.name, 'body': lastMessageGroupQuerySet.body, 'link': reverse("groupMessage", kwargs = {'dialogSlug': groupDialogAttributesQuerySet.slug}), 'id': str(lastMessageGroupQuerySet.id), 'users': tuple(groupDialogUsersIds)})
                                groupDialogsIds.add(dialogId)
                    myMessageText = "i'm:"
                    if len(idsToGetSmallThumbsAll) > 0:
                        unreadMessages = []
                        if len(personalDialogsIds) > 0:
                            unreadPersonalMessagesManyVaues = caches['unreadPersonalMessages'].get_many(personalDialogsIds).values()
                            if len(unreadPersonalMessagesManyVaues) > 0:
                                unreadMessages.extend(list(tuple(tuple(unreadPersonalMessagesManyVaues)[0].values())[0]))
                        if len(groupDialogsIds) > 0:
                            unreadGroupMessagesManyValues = caches['unreadGroupMessages'].get_many(groupDialogsIds).values()
                            if len(unreadGroupMessagesManyValues) > 0:
                                unreadMessages.extend(list(tuple(unreadGroupMessagesManyValues)[0].keys()))
                        thumbsDict, onlineStatusDict, activeStatusDict = getSmallDialogThumbs(idsToGetSmallThumbsAll, user, id)
                        return JsonResponse({'dialogsMinimized': {
                            'dialogsListDict': dialogsList,
                            'thumbsDict': thumbsDict,
                            'onlineStatusDict': onlineStatusDict,
                            'activeStatusDict': activeStatusDict,
                            'unreadMessages': unreadMessages,
                            'myMessageText': myMessageText
                            }})
                    return JsonResponse({'dialogsMinimized': {
                        'dialogsListDict': dialogsList,
                        'thumbsDict': None,
                        'onlineStatusDict': None,
                        'activeStatusDict': None,
                        'unreadMessages': None,
                        'myMessageText': myMessageText
                        }})
                dialogsList = []
                dialogIdsList = []
                dialogLinksList = []
                idsToGetSmallThumbsAll = set()
                idsToGetMainThumbsList = []
                for dialog in dialogsTupleQuerySet:
                    dialogId = str(dialog.id)
                    lastMessages = lastMessagesDict.get(dialogId, None)
                    if lastMessages is not None:
                        dialogIdsList.append(dialogId)
                        lastMessagesList = []
                        messageBy = None
                        if dialog.type is True:
                            for lastMessage in lastMessages:
                                if messageBy is None:
                                    messageBy = lastMessage.by_id
                                    idsToGetSmallThumbsAll.add(messageBy)
                                    if messageBy == id:
                                        idsToGetMainThumbsList.append(lastMessage.to_id)
                                        slugName = lastMessage.to.profileUrl
                                    else:
                                        idsToGetMainThumbsList.append(messageBy)
                                        slugName = lastMessage.by.profileUrl
                                else:
                                    idsToGetSmallThumbsAll.add(lastMessage.by_id)
                                lastMessagesList.append(lastMessage)
                            dialogsList.append(lastMessagesList)
                            dialogLinksList.append(reverse("personalMessage", kwargs = {'toUser': slugName}))
                        else:
                            for lastMessage in lastMessages:
                                if messageBy is None:
                                    messageBy = lastMessage.by_id
                                    idsToGetSmallThumbsAll.add(messageBy)
                                    if messageBy == id:
                                        idsToGetMainThumbsList.append(lastMessage.to_id)
                                    else:
                                        idsToGetMainThumbsList.append(messageBy)
                                else:
                                    idsToGetSmallThumbsAll.add(lastMessage.by_id)
                                lastMessagesList.append(lastMessage)
                            dialogsList.append(lastMessagesList)
                            dialogLinksList.append(reverse("groupMessage", kwargs = {'dialogSlug': dialog.attrs.slug}))
                if len(idsToGetSmallThumbsAll) > 0:
                    idsToGetMainThumbsSet = set(idsToGetMainThumbsList)
                    mainThumbsDict, onlineStatusDict, activeStatusDict = getMainDialogThumbsXHR(idsToGetMainThumbsSet, user, id)
                    idsToGetSmallThumbsAll.add(id)
                    smallThumbsDict = getSmallDialogThumbsSimple(idsToGetSmallThumbsAll, user, id)
                    idsToGetMainThumbsSet.add(id)
                    unreadPersonalMessagesMany = caches['unreadPersonalMessages'].get_many(idsToGetMainThumbsSet)
                    unreadMessagesManyValues = unreadPersonalMessagesMany.values()
                    unreadMessages = set()
                    for unreadMessagesUser in unreadMessagesManyValues:
                        for unreadMessage in unreadMessagesUser:
                            unreadMessages.add(unreadMessage[1])
                    if "global" in get:
                        return JsonResponse({"newPage": json.dumps({
                            'newTemplate': rts('psixiSocial/psixiSocial.html', {
                                'parentTemplate': 'base/classicPage/emptyParent.html',
                                'content': ('psixiSocial', 'dialogs'),
                                'dialogsListList': dialogsList,
                                'dialogIdsList': dialogIdsList,
                                'dialogsLinksList': dialogLinksList,
                                'mainThumbsDict': mainThumbsDict,
                                'mainThumbsIdsOrderedList': idsToGetMainThumbsList,
                                'smallThumbsDict': smallThumbsDict,
                                'onlineStatusDict': onlineStatusDict,
                                'activeStatusDict': activeStatusDict,
                                'unreadMessages': unreadMessages,
                                'requestUserId': id,
                                'time': str(datetime.now(tz = tz.gettz()))
                                }),
                            'menu': cachedFragments().menuGetter("type1Serialized"),
                            'url': request.path,
                            'titl': 'Диалоги'
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        'content': ('psixiSocial', 'dialogs'),
                        'menu': cachedFragments().menuGetter("type1"),
                        'url': request.path,
                        'titl': 'Диалоги',
                        'templateHead': rts('psixiSocial/head/dialogs/dialogsList.html'),
                        'templateBody': rts('psixiSocial/body/dialogs/dialogsList.html', {
                            'dialogsListList': dialogsList,
                            'dialogIdsList': dialogIdsList,
                            'dialogsLinksList': dialogLinksList,
                            'mainThumbsDict': mainThumbsDict,
                            'mainThumbsIdsOrderedList': idsToGetMainThumbsList,
                            'smallThumbsDict': smallThumbsDict,
                            'onlineStatusDict': onlineStatusDict,
                            'activeStatusDict': activeStatusDict,
                            'unreadMessages': unreadMessages,
                            'requestUserId': id
                            }),
                        'templateScripts': rts('psixiSocial/scripts/dialogs/dialogsList.html', {
                            'requestUserId': id,
                            'smallThumbsDict': smallThumbsDict,
                            'onlineStatusDict': onlineStatusDict,
                            'activeStatusDict': activeStatusDict,
                            'time': str(datetime.now(tz = tz.gettz()))
                            })
                        })})
            try:
                pageItems = dialogsPaginator.page(request.GET.get("page", 1))
            except EmptyPage:
                pageItems = dialogsPaginator.page(dialogsPaginator.num_pages)
            dialogsTupleQuerySet = pageItems.object_list
            dialogsList = []
            dialogIdsList = []
            dialogLinksList = []
            idsToGetSmallThumbsAll = set()
            idsToGetMainThumbsList = []
            for dialog in dialogsTupleQuerySet:
                dialogId = str(dialog.id)
                lastMessages = lastMessagesDict.get(dialogId, None)
                if lastMessages is not None:
                    dialogIdsList.append(dialogId)
                    lastMessagesList = []
                    if dialog.type is True:
                        messageBy = None
                        for lastMessage in lastMessages:
                            if messageBy is None:
                                messageBy = lastMessage.by_id
                                idsToGetSmallThumbsAll.add(messageBy)
                                if messageBy == id:
                                    idsToGetMainThumbsList.append(lastMessage.to_id)
                                    slugName = lastMessage.to.profileUrl
                                else:
                                    idsToGetMainThumbsList.append(messageBy)
                                    slugName = lastMessage.by.profileUrl
                            else:
                                idsToGetSmallThumbsAll.add(lastMessage.by_id)
                            lastMessagesList.append(lastMessage)
                        dialogsList.append(lastMessagesList)
                        dialogLinksList.append(reverse("personalMessage", kwargs = {'toUser': slugName}))
                    else:
                        for lastMessage in lastMessages:
                            messageBy = lastMessage.by_id
                            if messageBy not in idsToGetSmallThumbsAll:
                                idsToGetSmallThumbsAll.add(messageBy)
                                idsToGetMainThumbsList.append(messageBy)
                            lastMessagesList.append(lastMessage)
                        dialogsList.append(lastMessagesList)
                        dialogLinksList.append(reverse("groupMessage", kwargs = {'dialogSlug': dialog.attrs.slug}))
            if request.COOKIES.get("js") is None:
                if len(idsToGetSmallThumbsAll) > 0:
                    idsToGetMainThumbsSet = set(idsToGetMainThumbsList)
                    mainThumbsDict, onlineStatusDict, activeStatusDict = getMainDialogThumbs(idsToGetMainThumbsSet, user, id)
                    idsToGetSmallThumbsAll.add(id)
                    smallThumbsDict = getSmallDialogThumbs(idsToGetSmallThumbsAll, user, id)
                    idsToGetMainThumbsSet.add(id)
            else:
                if len(idsToGetSmallThumbsAll) > 0:
                    idsToGetMainThumbsSet = set(idsToGetMainThumbsList)
                    mainThumbsDict, onlineStatusDict, activeStatusDict = getMainDialogThumbsXHR(idsToGetMainThumbsSet, user, id)
                    idsToGetSmallThumbsAll.add(id)
                    smallThumbsDict = getSmallDialogThumbsSimple(idsToGetSmallThumbsAll, user, id)
                    idsToGetMainThumbsSet.add(id)
            unreadPersonalMessagesMany = caches['unreadPersonalMessages'].get_many(idsToGetMainThumbsSet)
            unreadMessagesManyValues = unreadPersonalMessagesMany.values()
            unreadMessages = set()
            for unreadMessagesUser in unreadMessagesManyValues:
                for unreadMessage in unreadMessagesUser:
                    unreadMessages.add(unreadMessage[1])
            return render(request, "psixiSocial/psixiSocial.html", {
                'parentTemplate': 'base/classicPage/base.html',
                'menu': cachedFragments().menuGetter("type1Serialized"),
                'content': ('psixiSocial', 'dialogs'),
                'titl': '',
                'requestUserId': id,
                'dialogsListList': dialogsList,
                'dialogIdsList': dialogIdsList,
                'dialogsLinksList': dialogLinksList,
                'mainThumbsDict': mainThumbsDict,
                'smallThumbsDict': smallThumbsDict,
                'mainThumbsIdsOrderedList': idsToGetMainThumbsList,
                'unreadMessages': unreadMessages,
                'onlineStatusDict': onlineStatusDict,
                'activeStatusDict': activeStatusDict,
                'time': datetime.now(tz = tz.gettz())
                })
        # Пусто
        if request.is_ajax():
            pass


def getNewDialogXHR(request):
    ''' Выдача одиночного элемента "диалог", xhr запрос создаётся, когда в текущем списке диалогов не находится диалога, с которого поступило сообщение. '''
    if request.is_ajax():
        user = request.user
        if user.is_authenticated:
            id = str(user.id)
            toUserId = request.GET.get('id', None)
            if toUserId:
                smallDialogThumbsCache = caches['dialogListThumbs']
                thumbsDict = smallDialogThumbsCache.get(toUserId, None)
                if thumbsDict is None:
                    privacyCache = caches['privacyCache']
                    privacyDict = privacyCache.get(toUserId, None)
                    if privacyDict is None:
                        friendsShipCache = caches['friendShipStatusCache']
                        friendsShipDict = friendsShipCache.get(id, None)
                        if friendsShipDict is None:
                            friendsIdsCache = caches['friendsIdCache']
                            friendsIds = friendsIdsCache.get(id, None)
                            if friendsIds is None:
                                friendsCache = caches['friendsCache']
                                friends = friendsCache.get(id, None)
                                if friends is None:
                                    friends = user.friends.all()
                                    if friends.count():
                                        friendsIdsQuerySet = friends.values("id")
                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                        friendsCache.set(id, friends)
                                    else:
                                        friendsIds = set()
                                        friendsCache.set(id, False)
                                else:
                                    if friends is False:
                                        friendsIds = set()
                                    else:
                                        friendsIdsQuerySet = friends.values("id")
                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                friendsIdsCache.set(id, friendsIds)
                            if toUserId in friendsIds:
                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                friendShipRequestsCache = caches['friendshipRequests']
                                friendShipRequestsMy = friendShipRequestsCache.get(id, tuple())
                                friendShipStatus = False
                                for friendRequest in friendShipRequestsMy:
                                    if friendRequest[0] == toUserId:
                                        friendShipStatus = "request"
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                if friendShipStatus is False:
                                    friendShipRequestsFriend = friendShipRequestsCache.get(userIdInt, tuple())
                                    for friendRequest in friendShipRequestsFriend:
                                        if friendRequest[0] == senderId:
                                            friendShipStatus = "myRequest"
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            friendsShipCache.set(id, {toUserId: friendShipStatus})
                        else:
                            friendsShipStatus = friendsShipDict.get(toUserId, None)
                            if friendsShipStatus is False:
                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            elif friendsShipStatus is True:
                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            elif friendsShipStatus == "request":
                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            elif friendsShipStatus == "myRequest":
                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            else:
                                friendShipRequestsCache = caches['friendshipRequests']
                                friendShipRequestsMy = friendShipRequestsCache.get(id, tuple())
                                friendShipStatus = False
                                for friendRequest in friendShipRequestsMy:
                                    if friendRequest[0] == toUserId:
                                        friendShipStatus = "request"
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                if friendShipStatus is False:
                                    friendShipRequestsFriend = friendShipRequestsCache.get(userIdInt, tuple())
                                    for friendRequest in friendShipRequestsFriend:
                                        if friendRequest[0] == senderId:
                                            friendShipStatus = "myRequest"
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                friendsShipDict.update({toUserId: friendShipStatus})
                                friendsShipCache.set(id, friendsShipCache)
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        privacyCache.set(toUserId, {id: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                    else:
                        userPrivacyDict = privacyDict.get(id, None)
                        if userPrivacyDict is None:
                            friendsShipCache = caches['friendShipStatusCache']
                            friendsShipDict = friendsShipCache.get(id, None)
                            if friendsShipDict is None:
                                friendsIdsCache = caches['friendsIdCache']
                                friendsIds = friendsIdsCache.get(id, None)
                                if friendsIds is None:
                                    friendsCache = caches['friendsCache']
                                    friends = friendsCache.get(id, None)
                                    if friends is None:
                                        friends = user.friends.all()
                                        if friends.count():
                                            friendsIdsQuerySet = friends.values("id")
                                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                            friendsCache.set(id, friends)
                                        else:
                                            friendsIds = set()
                                            friendsCache.set(id, False)
                                    else:
                                        if friends is False:
                                            friendsIds = set()
                                        else:
                                            friendsIdsQuerySet = friends.values("id")
                                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    friendsIdsCache.set(id, friendsIds)
                                if toUserId in friendsIds:
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                else:
                                    friendShipRequestsCache = caches['friendshipRequests']
                                    friendShipRequestsMy = friendShipRequestsCache.get(id, tuple())
                                    friendShipStatus = False
                                    for friendRequest in friendShipRequestsMy:
                                        if friendRequest[0] == toUserId:
                                            friendShipStatus = "request"
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    if friendShipStatus is False:
                                        friendShipRequestsFriend = friendShipRequestsCache.get(userIdInt, tuple())
                                        for friendRequest in friendShipRequestsFriend:
                                            if friendRequest[0] == senderId:
                                                friendShipStatus = "myRequest"
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                friendsShipCache.set(id, {toUserId: friendShipStatus})
                            else:
                                friendsShipStatus = friendsShipDict.get(toUserId, None)
                                if friendsShipStatus is False:
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                elif friendsShipStatus is True:
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                elif friendsShipStatus == "request":
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                elif friendsShipStatus == "myRequest":
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                else:
                                    friendShipRequestsCache = caches['friendshipRequests']
                                    friendShipRequestsMy = friendShipRequestsCache.get(id, tuple())
                                    friendShipStatus = False
                                    for friendRequest in friendShipRequestsMy:
                                        if friendRequest[0] == toUserId:
                                            friendShipStatus = "request"
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    if friendShipStatus is False:
                                        friendShipRequestsFriend = friendShipRequestsCache.get(userIdInt, tuple())
                                        for friendRequest in friendShipRequestsFriend:
                                            if friendRequest[0] == senderId:
                                                friendShipStatus = "myRequest"
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    friendsShipDict.update({toUserId: friendShipStatus})
                                    friendsShipCache.set(id, friendsShipCache)
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            privacyDict.update({id: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                            privacyCache.set(toUserId, privacyDict)
                        else:
                            viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                            viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                            viewAvatar = userPrivacyDict.get('viewAvatar', None)
                            viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                            if viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None:
                                friendsShipCache = caches['friendShipStatusCache']
                                friendsShipDict = friendsShipCache.get(id, None)
                                if friendsShipDict is None:
                                    friendsIdsCache = caches['friendsIdCache']
                                    friendsIds = friendsIdsCache.get(id, None)
                                    if friendsIds is None:
                                        friendsCache = caches['friendsCache']
                                        friends = friendsCache.get(id, None)
                                        if friends is None:
                                            friends = user.friends.all()
                                            if friends.count():
                                                friendsIdsQuerySet = friends.values("id")
                                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                                friendsCache.set(id, friends)
                                            else:
                                                friendsIds = set()
                                                friendsCache.set(id, False)
                                        else:
                                            if friends is False:
                                                friendsIds = set()
                                            else:
                                                friendsIdsQuerySet = friends.values("id")
                                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                        friendsIdsCache.set(id, friendsIds)
                                    if toUserId in friendsIds:
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    else:
                                        friendShipRequestsCache = caches['friendshipRequests']
                                        friendShipRequestsMy = friendShipRequestsCache.get(id, tuple())
                                        friendShipStatus = False
                                        for friendRequest in friendShipRequestsMy:
                                            if friendRequest[0] == toUserId:
                                                friendShipStatus = "request"
                                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        if friendShipStatus is False:
                                            friendShipRequestsFriend = friendShipRequestsCache.get(userIdInt, tuple())
                                            for friendRequest in friendShipRequestsFriend:
                                                if friendRequest[0] == senderId:
                                                    friendShipStatus = "myRequest"
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    friendsShipCache.set(id, {toUserId: friendShipStatus})
                                else:
                                    friendsShipStatus = friendsShipDict.get(toUserId, None)
                                    if friendsShipStatus is False:
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    elif friendsShipStatus is True:
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    elif friendsShipStatus == "request":
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    elif friendsShipStatus == "myRequest":
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    else:
                                        friendShipRequestsCache = caches['friendshipRequests']
                                        friendShipRequestsMy = friendShipRequestsCache.get(id, tuple())
                                        friendShipStatus = False
                                        for friendRequest in friendShipRequestsMy:
                                            if friendRequest[0] == toUserId:
                                                friendShipStatus = "request"
                                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        if friendShipStatus is False:
                                            friendShipRequestsFriend = friendShipRequestsCache.get(userIdInt, tuple())
                                            for friendRequest in friendShipRequestsFriend:
                                                if friendRequest[0] == senderId:
                                                    friendShipStatus = "myRequest"
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        friendsShipDict.update({toUserId: friendShipStatus})
                                        friendsShipCache.set(id, friendsShipCache)
                                if viewMainInfo is None:
                                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                    userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                                if viewTHPSInfo is None:
                                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                    userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                                if viewAvatar is None:
                                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                                    userPrivacyDict.update({'viewAvatar': viewAvatar})
                                if viewOnlineStatus is None:
                                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                    userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                                privacyCache.set(toUserId, privacyDict)
                    if viewMainInfo:
                        firstName = user.first_name or None
                        lastName = user.last_name or None
                        if firstName is not None and lastName is not None:
                            name = '{0} {1}'.format(firstName, lastName)
                            smallName = user.username
                        else:
                            if firstName is not None:
                                name = firstName
                                smallName = user.username
                            else:
                                name = user.username
                                smallName = None
                    else:
                        name = user.username
                        smallName = None
                    if viewAvatar:
                        avatar = user.avatarThumbSmaller or None
                        if avatar is not None:
                            if smallName is not None:
                                listThumbItem = '<div class = "user-thumb"><div class = "avatar-wrap"><img src = "{0}" title = "{1} [{2}]" /></div><p>{1}</p><small>{2}</small></div>'.format(avatar.url, name, smallName)
                            else:
                                listThumbItem = '<div class = "user-thumb"><div class = "avatar-wrap"><img src = "{0}" title = "{1}" /></div><p>{1}</p></div>'.format(avatar.url, name)
                        else:
                            if smallName is not None:
                                listThumbItem = '<div class = "user-thumb"><div class = "no-avatar"><p>{0} [{1}]</p></div><p>{0}</p><small>{1}</small></div>'.format(name, smallName)
                            else:
                                listThumbItem = '<div class = "user-thumb"><div class = "no-avatar"><p>{0}</p></div><p>{0}</p></div>'.format(name)
                    else:
                        if smallName is not None:
                            listThumbItem = '<div class = "user-thumb"><div class = "no-avatar"><p>{0} [{1}]</p></div><p>{0}</p><small>{1}</small></div>'.format(name, smallName)
                        else:
                            listThumbItem = '<div class = "user-thumb"><div class = "no-avatar"><p>{0}</p></div><p>{0}</p></div>'.format(name)
                    if viewOnlineStatus:
                        if toUserId in caches['onlineUsers'].get('online', set()):
                            status = True
                        else:
                            iffyStatusCache = caches['iffyUsersOnlineStatus']
                            iffyUsers = iffyStatusCache.get('iffy', set())
                            if toUserId in iffyUsers:
                                iffyTime = iffyStatusCache.get(toUserId, None)
                                if iffyTime is not None:
                                    status = {'iffy': iffyTime}
                                else:
                                    status = True
                            else:
                                status = False
                    else:
                        status = False
                    smallDialogThumbsCache.set({toUserId, {id: {'item': listThumbItem, 'showOnlineStatus': viewOnlineStatus}}})
                else:
                    thumb = thumbsDict.get(id, None)
                    if thumb is None:
                        privacyCache = caches['privacyCache']
                        privacyDict = privacyCache.get(toUserId, None)
                        if privacyDict is None:
                            friendsShipCache = caches['friendShipStatusCache']
                            friendsShipDict = friendsShipCache.get(id, None)
                            if friendsShipDict is None:
                                friendsIdsCache = caches['friendsIdCache']
                                friendsIds = friendsIdsCache.get(id, None)
                                if friendsIds is None:
                                    friendsCache = caches['friendsCache']
                                    friends = friendsCache.get(id, None)
                                    if friends is None:
                                        friends = user.friends.all()
                                        if friends.count():
                                            friendsIdsQuerySet = friends.values("id")
                                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                            friendsCache.set(id, friends)
                                        else:
                                            friendsIds = set()
                                            friendsCache.set(id, False)
                                    else:
                                        if friends is False:
                                            friendsIds = set()
                                        else:
                                            friendsIdsQuerySet = friends.values("id")
                                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    friendsIdsCache.set(id, friendsIds)
                                if toUserId in friendsIds:
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                else:
                                    friendShipRequestsCache = caches['friendshipRequests']
                                    friendShipRequestsMy = friendShipRequestsCache.get(id, tuple())
                                    friendShipStatus = False
                                    for friendRequest in friendShipRequestsMy:
                                        if friendRequest[0] == toUserId:
                                            friendShipStatus = "request"
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    if friendShipStatus is False:
                                        friendShipRequestsFriend = friendShipRequestsCache.get(userIdInt, tuple())
                                        for friendRequest in friendShipRequestsFriend:
                                            if friendRequest[0] == senderId:
                                                friendShipStatus = "myRequest"
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                friendsShipCache.set(id, {toUserId: friendShipStatus})
                            else:
                                friendsShipStatus = friendsShipDict.get(toUserId, None)
                                if friendsShipStatus is False:
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                elif friendsShipStatus is True:
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                elif friendsShipStatus == "request":
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                elif friendsShipStatus == "myRequest":
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                else:
                                    friendShipRequestsCache = caches['friendshipRequests']
                                    friendShipRequestsMy = friendShipRequestsCache.get(id, tuple())
                                    friendShipStatus = False
                                    for friendRequest in friendShipRequestsMy:
                                        if friendRequest[0] == toUserId:
                                            friendShipStatus = "request"
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    if friendShipStatus is False:
                                        friendShipRequestsFriend = friendShipRequestsCache.get(userIdInt, tuple())
                                        for friendRequest in friendShipRequestsFriend:
                                            if friendRequest[0] == senderId:
                                                friendShipStatus = "myRequest"
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    friendsShipDict.update({toUserId: friendShipStatus})
                                    friendsShipCache.set(id, friendsShipCache)
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            privacyCache.set(toUserId, {id: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                        else:
                            userPrivacyDict = privacyDict.get(id, None)
                            if userPrivacyDict is None:
                                friendsShipCache = caches['friendShipStatusCache']
                                friendsShipDict = friendsShipCache.get(id, None)
                                if friendsShipDict is None:
                                    friendsIdsCache = caches['friendsIdCache']
                                    friendsIds = friendsIdsCache.get(id, None)
                                    if friendsIds is None:
                                        friendsCache = caches['friendsCache']
                                        friends = friendsCache.get(id, None)
                                        if friends is None:
                                            friends = user.friends.all()
                                            if friends.count():
                                                friendsIdsQuerySet = friends.values("id")
                                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                                friendsCache.set(id, friends)
                                            else:
                                                friendsIds = set()
                                                friendsCache.set(id, False)
                                        else:
                                            if friends is False:
                                                friendsIds = set()
                                            else:
                                                friendsIdsQuerySet = friends.values("id")
                                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                        friendsIdsCache.set(id, friendsIds)
                                    if toUserId in friendsIds:
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    else:
                                        friendShipRequestsCache = caches['friendshipRequests']
                                        friendShipRequestsMy = friendShipRequestsCache.get(id, tuple())
                                        friendShipStatus = False
                                        for friendRequest in friendShipRequestsMy:
                                            if friendRequest[0] == toUserId:
                                                friendShipStatus = "request"
                                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        if friendShipStatus is False:
                                            friendShipRequestsFriend = friendShipRequestsCache.get(userIdInt, tuple())
                                            for friendRequest in friendShipRequestsFriend:
                                                if friendRequest[0] == senderId:
                                                    friendShipStatus = "myRequest"
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    friendsShipCache.set(id, {toUserId: friendShipStatus})
                                else:
                                    friendsShipStatus = friendsShipDict.get(toUserId, None)
                                    if friendsShipStatus is False:
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    elif friendsShipStatus is True:
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    elif friendsShipStatus == "request":
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    elif friendsShipStatus == "myRequest":
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    else:
                                        friendShipRequestsCache = caches['friendshipRequests']
                                        friendShipRequestsMy = friendShipRequestsCache.get(id, tuple())
                                        friendShipStatus = False
                                        for friendRequest in friendShipRequestsMy:
                                            if friendRequest[0] == toUserId:
                                                friendShipStatus = "request"
                                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        if friendShipStatus is False:
                                            friendShipRequestsFriend = friendShipRequestsCache.get(userIdInt, tuple())
                                            for friendRequest in friendShipRequestsFriend:
                                                if friendRequest[0] == senderId:
                                                    friendShipStatus = "myRequest"
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        friendsShipDict.update({toUserId: friendShipStatus})
                                        friendsShipCache.set(id, friendsShipCache)
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                viewAvatar = privacyChecker.checkAvatarPrivacy()
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                privacyDict.update({id: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                                privacyCache.set(toUserId, privacyDict)
                            else:
                                viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                                viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                                viewAvatar = userPrivacyDict.get('viewAvatar', None)
                                viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                                if viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None:
                                    friendsShipCache = caches['friendShipStatusCache']
                                    friendsShipDict = friendsShipCache.get(id, None)
                                    if friendsShipDict is None:
                                        friendsIdsCache = caches['friendsIdCache']
                                        friendsIds = friendsIdsCache.get(id, None)
                                        if friendsIds is None:
                                            friendsCache = caches['friendsCache']
                                            friends = friendsCache.get(id, None)
                                            if friends is None:
                                                friends = user.friends.all()
                                                if friends.count():
                                                    friendsIdsQuerySet = friends.values("id")
                                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                                    friendsCache.set(id, friends)
                                                else:
                                                    friendsIds = set()
                                                    friendsCache.set(id, False)
                                            else:
                                                if friends is False:
                                                    friendsIds = set()
                                                else:
                                                    friendsIdsQuerySet = friends.values("id")
                                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                            friendsIdsCache.set(id, friendsIds)
                                        if toUserId in friendsIds:
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        else:
                                            friendShipRequestsCache = caches['friendshipRequests']
                                            friendShipRequestsMy = friendShipRequestsCache.get(id, tuple())
                                            friendShipStatus = False
                                            for friendRequest in friendShipRequestsMy:
                                                if friendRequest[0] == toUserId:
                                                    friendShipStatus = "request"
                                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                            if friendShipStatus is False:
                                                friendShipRequestsFriend = friendShipRequestsCache.get(userIdInt, tuple())
                                                for friendRequest in friendShipRequestsFriend:
                                                    if friendRequest[0] == senderId:
                                                        friendShipStatus = "myRequest"
                                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        friendsShipCache.set(id, {toUserId: friendShipStatus})
                                    else:
                                        friendsShipStatus = friendsShipDict.get(toUserId, None)
                                        if friendsShipStatus is False:
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        elif friendsShipStatus is True:
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        elif friendsShipStatus == "request":
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        elif friendsShipStatus == "myRequest":
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        else:
                                            friendShipRequestsCache = caches['friendshipRequests']
                                            friendShipRequestsMy = friendShipRequestsCache.get(id, tuple())
                                            friendShipStatus = False
                                            for friendRequest in friendShipRequestsMy:
                                                if friendRequest[0] == toUserId:
                                                    friendShipStatus = "request"
                                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                            if friendShipStatus is False:
                                                friendShipRequestsFriend = friendShipRequestsCache.get(userIdInt, tuple())
                                                for friendRequest in friendShipRequestsFriend:
                                                    if friendRequest[0] == senderId:
                                                        friendShipStatus = "myRequest"
                                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                            friendsShipDict.update({toUserId: friendShipStatus})
                                            friendsShipCache.set(id, friendsShipCache)
                                    if viewMainInfo is None:
                                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                        userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                                    if viewTHPSInfo is None:
                                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                        userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                                    if viewAvatar is None:
                                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                                        userPrivacyDict.update({'viewAvatar': viewAvatar})
                                    if viewOnlineStatus is None:
                                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                        userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                                    privacyCache.set(toUserId, privacyDict)
                        if viewMainInfo:
                            firstName = user.first_name or None
                            lastName = user.last_name or None
                            if firstName is not None and lastName is not None:
                                name = '{0} {1}'.format(firstName, lastName)
                                smallName = user.username
                            else:
                                if firstName is not None:
                                    name = firstName
                                    smallName = user.username
                                else:
                                    name = user.username
                                    smallName = None
                        else:
                            name = user.username
                            smallName = None
                        if viewAvatar:
                            avatar = user.avatarThumbSmaller or None
                            if avatar is not None:
                                if smallName is not None:
                                    listThumbItem = '<div class = "user-thumb"><div class = "avatar-wrap"><img src = "{0}" title = "{1} [{2}]" /></div><p>{1}</p><small>{2}</small></div>'.format(avatar.url, name, smallName)
                                else:
                                    listThumbItem = '<div class = "user-thumb"><div class = "avatar-wrap"><img src = "{0}" title = "{1}" /></div><p>{1}</p></div>'.format(avatar.url, name)
                            else:
                                if smallName is not None:
                                    listThumbItem = '<div class = "user-thumb"><div class = "no-avatar"><p>{0} [{1}]</p></div><p>{0}</p><small>{1}</small></div>'.format(name, smallName)
                                else:
                                    listThumbItem = '<div class = "user-thumb"><div class = "no-avatar"><p>{0}</p></div><p>{0}</p></div>'.format(name)
                        else:
                            if smallName is not None:
                                listThumbItem = '<div class = "user-thumb"><div class = "no-avatar"><p>{0} [{1}]</p></div><p>{0}</p><small>{1}</small></div>'.format(name, smallName)
                            else:
                                listThumbItem = '<div class = "user-thumb"><div class = "no-avatar"><p>{0}</p></div><p>{0}</p></div>'.format(name)
                        if viewOnlineStatus:
                            if toUserId in caches['onlineUsers'].get('online', set()):
                                status = True
                            else:
                                iffyStatusCache = caches['iffyUsersOnlineStatus']
                                iffyUsers = iffyStatusCache.get('iffy', set())
                                if toUserId in iffyUsers:
                                    iffyTime = iffyStatusCache.get(toUserId, None)
                                    if iffyTime is not None:
                                        status = {'iffy': iffyTime}
                                    else:
                                        status = True
                                else:
                                    status = False
                        else:
                            status = False
                        thumbsDict.update({id: {'item': listThumbItem, 'showOnlineStatus': viewOnlineStatus}})
                        smallDialogThumbsCache.set(toUserId, thumbsDict)
                    else:
                        showOnlineStatus = thumb.get('showOnlineStatus', None)
                        if showOnlineStatus is None:
                            privacyCache = caches['privacyCache']
                            privacyDict = privacyCache.get(toUserId, None)
                            if privacyDict is None:
                                friendsShipCache = caches['friendShipStatusCache']
                                friendsShipDict = friendsShipCache.get(id, None)
                                if friendsShipDict is None:
                                    friendsIdsCache = caches['friendsIdCache']
                                    friendsIds = friendsIdsCache.get(id, None)
                                    if friendsIds is None:
                                        friendsCache = caches['friendsCache']
                                        friends = friendsCache.get(id, None)
                                        if friends is None:
                                            friends = user.friends.all()
                                            if friends.count():
                                                friendsIdsQuerySet = friends.values("id")
                                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                                friendsCache.set(id, friends)
                                            else:
                                                friendsIds = set()
                                                friendsCache.set(id, False)
                                        else:
                                            if friends is False:
                                                friendsIds = set()
                                            else:
                                                friendsIdsQuerySet = friends.values("id")
                                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                        friendsIdsCache.set(id, friendsIds)
                                    if toUserId in friendsIds:
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    else:
                                        friendShipRequestsCache = caches['friendshipRequests']
                                        friendShipRequestsMy = friendShipRequestsCache.get(id, tuple())
                                        friendShipStatus = False
                                        for friendRequest in friendShipRequestsMy:
                                            if friendRequest[0] == toUserId:
                                                friendShipStatus = "request"
                                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        if friendShipStatus is False:
                                            friendShipRequestsFriend = friendShipRequestsCache.get(userIdInt, tuple())
                                            for friendRequest in friendShipRequestsFriend:
                                                if friendRequest[0] == senderId:
                                                    friendShipStatus = "myRequest"
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    friendsShipCache.set(id, {toUserId: friendShipStatus})
                                else:
                                    friendsShipStatus = friendsShipDict.get(toUserId, None)
                                    if friendsShipStatus is False:
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    elif friendsShipStatus is True:
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    elif friendsShipStatus == "request":
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    elif friendsShipStatus == "myRequest":
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    else:
                                        friendShipRequestsCache = caches['friendshipRequests']
                                        friendShipRequestsMy = friendShipRequestsCache.get(id, tuple())
                                        friendShipStatus = False
                                        for friendRequest in friendShipRequestsMy:
                                            if friendRequest[0] == toUserId:
                                                friendShipStatus = "request"
                                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        if friendShipStatus is False:
                                            friendShipRequestsFriend = friendShipRequestsCache.get(userIdInt, tuple())
                                            for friendRequest in friendShipRequestsFriend:
                                                if friendRequest[0] == senderId:
                                                    friendShipStatus = "myRequest"
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        friendsShipDict.update({toUserId: friendShipStatus})
                                        friendsShipCache.set(id, friendsShipCache)
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                privacyCache.set(toUserId, {id: {'viewOnlineStatus': viewOnlineStatus}})
                            else:
                                userPrivacyDict = privacyDict.get(id, None)
                                if userPrivacyDict is None:
                                    friendsShipCache = caches['friendShipStatusCache']
                                    friendsShipDict = friendsShipCache.get(id, None)
                                    if friendsShipDict is None:
                                        friendsIdsCache = caches['friendsIdCache']
                                        friendsIds = friendsIdsCache.get(id, None)
                                        if friendsIds is None:
                                            friendsCache = caches['friendsCache']
                                            friends = friendsCache.get(id, None)
                                            if friends is None:
                                                friends = user.friends.all()
                                                if friends.count():
                                                    friendsIdsQuerySet = friends.values("id")
                                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                                    friendsCache.set(id, friends)
                                                else:
                                                    friendsIds = set()
                                                    friendsCache.set(id, False)
                                            else:
                                                if friends is False:
                                                    friendsIds = set()
                                                else:
                                                    friendsIdsQuerySet = friends.values("id")
                                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                            friendsIdsCache.set(id, friendsIds)
                                        if toUserId in friendsIds:
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        else:
                                            friendShipRequestsCache = caches['friendshipRequests']
                                            friendShipRequestsMy = friendShipRequestsCache.get(id, tuple())
                                            friendShipStatus = False
                                            for friendRequest in friendShipRequestsMy:
                                                if friendRequest[0] == toUserId:
                                                    friendShipStatus = "request"
                                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                            if friendShipStatus is False:
                                                friendShipRequestsFriend = friendShipRequestsCache.get(userIdInt, tuple())
                                                for friendRequest in friendShipRequestsFriend:
                                                    if friendRequest[0] == senderId:
                                                        friendShipStatus = "myRequest"
                                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        friendsShipCache.set(id, {toUserId: friendShipStatus})
                                    else:
                                        friendsShipStatus = friendsShipDict.get(toUserId, None)
                                        if friendsShipStatus is False:
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        elif friendsShipStatus is True:
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        elif friendsShipStatus == "request":
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        elif friendsShipStatus == "myRequest":
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        else:
                                            friendShipRequestsCache = caches['friendshipRequests']
                                            friendShipRequestsMy = friendShipRequestsCache.get(id, tuple())
                                            friendShipStatus = False
                                            for friendRequest in friendShipRequestsMy:
                                                if friendRequest[0] == toUserId:
                                                    friendShipStatus = "request"
                                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                            if friendShipStatus is False:
                                                friendShipRequestsFriend = friendShipRequestsCache.get(userIdInt, tuple())
                                                for friendRequest in friendShipRequestsFriend:
                                                    if friendRequest[0] == senderId:
                                                        friendShipStatus = "myRequest"
                                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                            friendsShipDict.update({toUserId: friendShipStatus})
                                            friendsShipCache.set(id, friendsShipCache)
                                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                    privacyDict.update({id: {'viewOnlineStatus': viewOnlineStatus}})
                                    privacyCache.set(toUserId, privacyDict)
                                else:
                                    viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                                    if viewOnlineStatus is None:
                                        friendsShipCache = caches['friendShipStatusCache']
                                        friendsShipDict = friendsShipCache.get(id, None)
                                        if friendsShipDict is None:
                                            friendsIdsCache = caches['friendsIdCache']
                                            friendsIds = friendsIdsCache.get(id, None)
                                            if friendsIds is None:
                                                friendsCache = caches['friendsCache']
                                                friends = friendsCache.get(id, None)
                                                if friends is None:
                                                    friends = user.friends.all()
                                                    if friends.count():
                                                        friendsIdsQuerySet = friends.values("id")
                                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                                        friendsCache.set(id, friends)
                                                    else:
                                                        friendsIds = set()
                                                        friendsCache.set(id, False)
                                                else:
                                                    if friends is False:
                                                        friendsIds = set()
                                                    else:
                                                        friendsIdsQuerySet = friends.values("id")
                                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                                friendsIdsCache.set(id, friendsIds)
                                            if toUserId in friendsIds:
                                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                            else:
                                                friendShipRequestsCache = caches['friendshipRequests']
                                                friendShipRequestsMy = friendShipRequestsCache.get(id, tuple())
                                                friendShipStatus = False
                                                for friendRequest in friendShipRequestsMy:
                                                    if friendRequest[0] == toUserId:
                                                        friendShipStatus = "request"
                                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                                if friendShipStatus is False:
                                                    friendShipRequestsFriend = friendShipRequestsCache.get(userIdInt, tuple())
                                                    for friendRequest in friendShipRequestsFriend:
                                                        if friendRequest[0] == senderId:
                                                            friendShipStatus = "myRequest"
                                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                            friendsShipCache.set(id, {toUserId: friendShipStatus})
                                        else:
                                            friendsShipStatus = friendsShipDict.get(toUserId, None)
                                            if friendsShipStatus is False:
                                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            elif friendsShipStatus is True:
                                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                            elif friendsShipStatus == "request":
                                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                            elif friendsShipStatus == "myRequest":
                                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            else:
                                                friendShipRequestsCache = caches['friendshipRequests']
                                                friendShipRequestsMy = friendShipRequestsCache.get(id, tuple())
                                                friendShipStatus = False
                                                for friendRequest in friendShipRequestsMy:
                                                    if friendRequest[0] == toUserId:
                                                        friendShipStatus = "request"
                                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                                if friendShipStatus is False:
                                                    friendShipRequestsFriend = friendShipRequestsCache.get(userIdInt, tuple())
                                                    for friendRequest in friendShipRequestsFriend:
                                                        if friendRequest[0] == senderId:
                                                            friendShipStatus = "myRequest"
                                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                                friendsShipDict.update({toUserId: friendShipStatus})
                                                friendsShipCache.set(id, friendsShipCache)
                                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                        userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                                        privacyCache.set(toUserId, privacyDict)
                            if viewOnlineStatus:
                                if toUserId in caches['onlineUsers'].get('online', set()):
                                    status = True
                                else:
                                    iffyStatusCache = caches['iffyUsersOnlineStatus']
                                    iffyUsers = iffyStatusCache.get('iffy', set())
                                    if toUserId in iffyUsers:
                                        iffyTime = iffyStatusCache.get(toUserId, None)
                                        if iffyTime is not None:
                                            status = {'iffy': iffyTime}
                                        else:
                                            status = True
                                    else:
                                        status = False
                            else:
                                status = False
                            thumb.update({'showOnlineStatus': viewOnlineStatus})
                            smallDialogThumbsCache.set(toUserId, thumbsDict)
                        else:
                            if showOnlineStatus:
                                if toUserId in caches['onlineUsers'].get('online', set()):
                                    status = True
                                else:
                                    iffyStatusCache = caches['iffyUsersOnlineStatus']
                                    iffyUsers = iffyStatusCache.get('iffy', set())
                                    if toUserId in iffyUsers:
                                        iffyTime = iffyStatusCache.get(toUserId, None)
                                        if iffyTime is not None:
                                            status = {'iffy': iffyTime}
                                        else:
                                            status = True
                                    else:
                                        status = False
                            else:
                                status = False
                        listThumbItem = thumb['item']
                dialogsCache = caches['dialogsCache']
                dialogsDictDict = dialogsCache.get(id, None)
                if dialogsDictDict is None:
                    myDialogs = dialogMessage.objects.filter(users__exact = user).order_by("-lastUpdate")
                    dialogsDict = OrderedDict()
                    lastMessagesDict = {}
                    for dialog in myDialogs:
                        dialogId = str(dialog.id)
                        dialogsDict.update({dialogId: dialog})
                        messages = personalMessage.objects.filter(inDialog__exact = dialog).order_by("-time")
                        lastMessagesList = []
                        for num in range(2, -1, -1):
                            try:
                                message = messages[num]
                            except IndexError:
                                message = None
                            if message is not None:
                                lastMessagesList.append(message)
                        lastMessagesDict.update({dialogId: lastMessagesList})
                    dialogsCache.set(id, {"dialogs": dialogsDict, "last": lastMessagesDict})
                else:
                    dialogsDict = dialogsDictDict.get("dialogs", None)
                    lastMessagesDict = dialogsDictDict.get("last", None)
                    if dialogsDict is None and lastMessagesDict is None:
                        myDialogs = dialogMessage.objects.filter(users__exact = user).order_by("-lastUpdate")
                        dialogsDict = OrderedDict()
                        lastMessagesDict = {}
                        for dialog in myDialogs:
                            dialogId = str(dialog.id)
                            dialogsDict.update({dialogId: dialog})
                            messages = personalMessage.objects.filter(inDialog__exact = dialog).order_by("-time")
                            lastMessagesList = []
                            for num in range(2, -1, -1):
                                try:
                                    message = messages[num]
                                except IndexError:
                                    message = None
                                if message is not None:
                                    lastMessagesList.append(message)
                            lastMessagesDict.update({dialogId: lastMessagesList})
                        dialogsDictDict.update({"dialogs": dialogsDict, "last": lastMessagesDict})
                    elif dialogsDict is None:
                        myDialogs = dialogMessage.objects.filter(users__exact = user).order_by("-lastUpdate")
                        dialogsDict = OrderedDict({str(dialog.id): dialog for dialog in myDialogs})
                        dialogsDictDict.update({"dialogs": dialogsDict})
                    elif lastMessagesDict is None:
                        lastMessagesDict = {}
                        for dialogId, dialog in dialogsDict.items():
                            messages = personalMessage.objects.filter(inDialog__exact = dialog).order_by("-time")
                            lastMessagesList = []
                            for num in range(2, -1, -1):
                                try:
                                    message = messages[num]
                                except IndexError:
                                    message = None
                                if message is not None:
                                    lastMessagesList.append(message)
                            lastMessagesDict.update({dialogId: lastMessagesList})
                        dialogsDictDict.update({"last": lastMessagesDict})
                lastMessage = lastMessagesDict[tuple(dialogsDict.keys())[0]][-1]
                return JsonResponse({"newDialog": {
                                     "status": status,
                                     "thumb": listThumbItem,
                                     "body": lastMessage.body,
                                     "link": reverse("personalMessage", kwargs = {'toUser': lastMessage.by.profileUrl})
                    }})