import json, base64
from copy import copy
from functools import lru_cache
from django.conf import settings
from django.core.files.base import ContentFile
from django.core.validators import FileExtensionValidator, validate_image_file_extension, ValidationError
from datetime import datetime
from dateutil import tz
from collections import OrderedDict
from channels.layers import get_channel_layer
from psixiSocial.models import personalMessage, dialogMessage, mainUserProfile, THPSProfile, userProfilePrivacy, imagesAttachmentMessagePersonal
from psixiSocial.forms import sendMessageForm
from psixiSocial.login import login_decorator
from psixiSocial.privacyIdentifier import checkPrivacy
from django.db.transaction import atomic, Error
from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from django.http import JsonResponse
from django.template.loader import render_to_string as rts
from django.urls import reverse
from django.core.cache import caches
from base.helpers import checkCSRF
from psixiSocial.translators import usersListText
from asgiref.sync import async_to_sync


class cachedFragments():
    def __init__(self, *args, **kwargs):
        self.myUsername = kwargs.pop('myUsername', '')
        self.mySlug = kwargs.pop('mySlug', '')
        self.username = kwargs.pop('username', '')
        self.profileUrl = kwargs.pop('profileUrl', '')
        self.url = kwargs.pop('url')

    @property
    @lru_cache(maxsize = 16)
    def menu(self):
        menu = OrderedDict({
            0: {
                'textname': 'Переписка между %s <---> %s' % (self.myUsername, self.username),
                'url': self.url
                },
            1: {
                'textname': 'Мой профиль',
                'url': reverse("profile", kwargs = {'currentUser': self.profileUrl}),
                0: {
                    'textname': 'Диалоги',
                    'url': reverse('dialogs', kwargs = {'sep': '', 'page': ''})
                    },
                1: {
                    'textname': 'Друзья',
                    0: {
                        'textname': 'Все',
                        'url': reverse('friends', kwargs = {'user': 'my', 'category': 'all', 'sep': '',  'page': ''}),
                        'handler': 'localNavigation'
                        },
                    1: {
                        'textname': 'Сейчас в сети',
                        'url': reverse('friends', kwargs = {'user': 'my', 'category': 'online', 'sep': '',  'page': ''}),
                        'handler': 'localNavigation'
                        }
                    },
                2: {
                    'textname': 'Записи в блоге'
                    },
                3: {
                    'textname': 'Записи в libraryTHPS'
                    },
                4: {
                    'textname': 'Редактировать',
                    0: {
                        'textname': 'Основные',
                        'url': reverse('profileEditUrl', kwargs = {'editType': 'main'})
                        },
                    1: {
                        'textname': 'Дополнительно',
                        'url': reverse('profileEditUrl', kwargs = {'editType': 'detail'})
                        },
                    2: {
                        'textname': 'Обратная связь',
                        'url': reverse('profileEditUrl', kwargs = {'editType': 'contacts'})
                        },
                    3: {
                        'textname': 'Настройки приватности',
                        'url': reverse('editPrivacyUrl', kwargs = {'editPlace': 'profile'}),
                        0: {
                            'textname': 'Права для исключительных пользователей',
                            'url': reverse('editPrivacyExclusiveUrl', kwargs = {'editPlace': 'profile'})
                            }
                        },
                    4: {
                        'textname': 'THPS профиль',
                        'url': reverse('profileEditUrl', kwargs = {'editType': 'thps'})
                        }
                    },
                },
            2: {
                'textname': 'Профиль %s' % self.username,
                'url': reverse('profile', kwargs = {'currentUser': self.profileUrl}),
                0: {
                    'textname': 'Друзья',
                    0: {
                        'textname': 'Все',
                        'url': reverse('friends', kwargs = {'user': self.profileUrl, 'category': 'all', 'sep': '', 'page': ''}),
                        'handler': 'localNavigation'
                        },
                    1: {
                        'textname': 'Сейчас в сети',
                        'url': reverse('friends', kwargs = {'user': self.profileUrl, 'category': 'online', 'sep': '', 'page': ''}),
                        'handler': 'localNavigation'
                        }
                    },
                1: {
                    'textname': 'Записи в блоге'
                    },
                2: {
                    'textname': 'Записи в libraryTHPS'
                    },
                },
            3: {
                'textname': 'Выход',
                'url': reverse("logOut")
                }
            })
        return menu

    @property
    @lru_cache(maxsize = 16)
    def menuSerialized(self):
        menu = self.menu
        if isinstance(menu, OrderedDict):
            return json.dumps(menu)
        return json.dumps(menu())

    def menuGetter(self, methodName):
        methodOrCached = getattr(self, methodName)
        if isinstance(methodOrCached, OrderedDict):
            return methodOrCached
        if isinstance(methodOrCached, str):
            return methodOrCached
        return methodOrCached()

@login_decorator
def dialogPersonal(request, toUser):
    method = request.method
    if method == "GET":
        curUser = request.user
        myId = str(curUser.id)
        try:
            toUserInstance = mainUserProfile.objects.get(profileUrl__exact = toUser)
            toUserId = str(toUserInstance.id)
        except mainUserProfile.DoesNotExist:
            return render(request, 'base/404.html', {'msg': 'Пользователь %s не найден' % toUser})
        privacyCache = caches['privacyCache']
        privacyDictDict = privacyCache.get_many({toUserId, myId})
        privacyDict = privacyDictDict.get(toUserId, None)
        friendShipStatus = None
        privacyDictHasChanges = False
        privacyDictMyHasChanges = False
        friendsIds = None
        if privacyDict is None:
            privacySettings = userProfilePrivacy.objects.get(user__exact = toUserInstance)
            friendShipCache = caches['friendShipStatusCache']
            friendShipStatusDict = friendShipCache.get(myId, None)
            if friendShipStatusDict is None:
                friendsIdsCache = caches['friendsIdCache']
                friendsIds = friendsIdsCache.get(myId, None)
                if friendsIds is None:
                    friendsCache = caches['friendsCache']
                    cachedFriends = friendsCache.get(myId, None)
                    if cachedFriends is None:
                        cachedFriends = curUser.friends.all()
                        if cachedFriends.count():
                            friendsCache.set(myId, cachedFriends)
                            friendsIdsQuerySet = cachedFriends.values("id")
                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                        else:
                            friendsCache.set(myId, False)
                            friendsIds = set()
                    else:
                        if cachedFriends is not False:
                            friendsIdsQuerySet = cachedFriends.values("id")
                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                        else:
                            friendsIds = set()
                    friendsIdsCache.set(myId, friendsIds)
                if toUserId in friendsIds:
                    privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = True, isAuthenticated = True)
                    friendShipCache.set(myId, {toUserId: True})
                else:
                    friendShipRequestsCache = caches['friendshipRequests']
                    friendShipRequests = friendShipRequestsCache.get(myId, None)
                    friendShipStatus = False
                    privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = False, isAuthenticated = True)
                    if friendShipRequests is not None:
                        for friendShipRequest in friendShipRequests:
                            if friendShipRequest[0] == toUserId:
                                friendShipStatus = "request"
                                privacyChecker.isFriend = True
                                break
                        if friendShipStatus is False:
                            friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                            if friendShipRequests is not None:
                                for friendShipRequest in friendShipRequests:
                                    if friendShipRequest[0] == myId:
                                        friendShipStatus = "myRequest"
                                        break
                    friendShipCache.set(myId, {toUserId: friendShipStatus})
            else:
                friendShipStatus = friendShipStatusDict.get(toUserId, None)
                if friendShipStatus is None:
                    friendsIdsCache = caches['friendsIdCache']
                    friendsIds = friendsIdsCache.get(myId, None)
                    if friendsIds is None:
                        friendsCache = caches['friendsCache']
                        cachedFriends = friendsCache.get(myId, None)
                        if cachedFriends is None:
                            cachedFriends = curUser.friends.all()
                            if cachedFriends.count():
                                friendsCache.set(myId, cachedFriends)
                                friendsIdsQuerySet = cachedFriends.values("id")
                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                            else:
                                friendsCache.set(myId, False)
                                friendsIds = set()
                        else:
                            if cachedFriends is not False:
                                friendsIdsQuerySet = cachedFriends.values("id")
                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                            else:
                                friendsIds = set()
                        friendsIdsCache.set(myId, friendsIds)
                    if toUserId in friendsIds:
                        privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = True, isAuthenticated = True)
                        friendShipStatusDict.update({toUserId: True})
                    else:
                        friendShipRequestsCache = caches['friendshipRequests']
                        friendShipRequests = friendShipRequestsCache.get(myId, None)
                        friendShipStatus = False
                        privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = False, isAuthenticated = True)
                        if friendShipRequests is not None:
                            for friendShipRequest in friendShipRequests:
                                if friendShipRequest[0] == toUserId:
                                    friendShipStatus = "request"
                                    privacyChecker.isFriend = True
                                    break
                            if friendShipStatus is False:
                                friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                if friendShipRequests is not None:
                                    for friendShipRequest in friendShipRequests:
                                        if friendShipRequest[0] == myId:
                                            friendShipStatus = "myRequest"
                                            break
                        friendShipStatusDict.update({toUserId: friendShipStatus})
                    friendShipCache.set(myId, friendShipStatusDict)
                else:
                    privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
            viewProfile = privacyChecker.checkViewProfilePrivacy()
            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
            viewAvatar = privacyChecker.checkAvatarPrivacy()
            sendMessages = privacyChecker.sendPersonalMessages()
            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
            viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
            privacyDictHasChanges = None
            privacyDictDict = {toUserId: {myId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'sendPersonalMessages': sendMessages, 'viewOnlineStatus': viewOnlineStatus}}}
        else:
            userPrivacyDict = privacyDict.get(myId, None)
            if userPrivacyDict is None:
                privacySettings = userProfilePrivacy.objects.get(user__exact = toUserInstance)
                friendShipCache = caches['friendShipStatusCache']
                friendShipStatusDict = friendShipCache.get(myId, None)
                if friendShipStatusDict is None:
                    friendsIdsCache = caches['friendsIdCache']
                    friendsIds = friendsIdsCache.get(myId, None)
                    if friendsIds is None:
                        friendsCache = caches['friendsCache']
                        cachedFriends = friendsCache.get(myId, None)
                        if cachedFriends is None:
                            cachedFriends = curUser.friends.all()
                            if cachedFriends.count():
                                friendsCache.set(myId, cachedFriends)
                                friendsIdsQuerySet = cachedFriends.values("id")
                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                            else:
                                friendsCache.set(myId, False)
                                friendsIds = set()
                        else:
                            if cachedFriends is not False:
                                friendsIdsQuerySet = cachedFriends.values("id")
                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                            else:
                                friendsIds = set()
                        friendsIdsCache.set(myId, friendsIds)
                    if toUserId in friendsIds:
                        privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = True, isAuthenticated = True)
                        friendShipCache.set(myId, {toUserId: True})
                    else:
                        friendShipRequestsCache = caches['friendshipRequests']
                        friendShipRequests = friendShipRequestsCache.get(myId, None)
                        friendShipStatus = False
                        privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = False, isAuthenticated = True)
                        if friendShipRequests is not None:
                            for friendShipRequest in friendShipRequests:
                                if friendShipRequest[0] == toUserId:
                                    friendShipStatus = "request"
                                    privacyChecker.isFriend = True
                                    break
                            if friendShipStatus is False:
                                friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                if friendShipRequests is not None:
                                    for friendShipRequest in friendShipRequests:
                                        if friendShipRequest[0] == myId:
                                            friendShipStatus = "myRequest"
                                            break
                        friendShipCache.set(myId, {toUserId: friendShipStatus})
                else:
                    friendShipStatus = friendShipStatusDict.get(toUserId, None)
                    if friendShipStatus is None:
                        friendsIdsCache = caches['friendsIdCache']
                        friendsIds = friendsIdsCache.get(myId, None)
                        if friendsIds is None:
                            friendsCache = caches['friendsCache']
                            cachedFriends = friendsCache.get(myId, None)
                            if cachedFriends is None:
                                cachedFriends = curUser.friends.all()
                                if cachedFriends.count():
                                    friendsCache.set(myId, cachedFriends)
                                    friendsIdsQuerySet = cachedFriends.values("id")
                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                else:
                                    friendsCache.set(myId, False)
                                    friendsIds = set()
                            else:
                                if cachedFriends is not False:
                                    friendsIdsQuerySet = cachedFriends.values("id")
                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                else:
                                    friendsIds = set()
                            friendsIdsCache.set(myId, friendsIds)
                        if toUserId in friendsIds:
                            privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = True, isAuthenticated = True)
                            friendShipStatusDict.update({toUserId: True})
                        else:
                            friendShipRequestsCache = caches['friendshipRequests']
                            friendShipRequests = friendShipRequestsCache.get(myId, None)
                            friendShipStatus = False
                            privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = False, isAuthenticated = True)
                            if friendShipRequests is not None:
                                for friendShipRequest in friendShipRequests:
                                    if friendShipRequest[0] == toUserId:
                                        friendShipStatus = "request"
                                        privacyChecker.isFriend = True
                                        break
                                if friendShipStatus is False:
                                    friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                    if friendShipRequests is not None:
                                        for friendShipRequest in friendShipRequests:
                                            if friendShipRequest[0] == myId:
                                                friendShipStatus = "myRequest"
                                                break
                            friendShipStatusDict.update({toUserId: friendShipStatus})
                        friendShipCache.set(myId, friendShipStatusDict)
                    else:
                        privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                viewProfile = privacyChecker.checkViewProfilePrivacy()
                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                viewAvatar = privacyChecker.checkAvatarPrivacy()
                sendMessages = privacyChecker.sendPersonalMessages()
                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                privacyDictHasChanges = True
                privacyDict.update({myId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'sendPersonalMessages': sendMessages, 'viewOnlineStatus': viewOnlineStatus}})
            else:
                viewProfile = userPrivacyDict.get('viewProfile', None)
                viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                viewAvatar = userPrivacyDict.get('viewAvatar', None)
                sendMessages = userPrivacyDict.get('sendPersonalMessages', None)
                viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or sendMessages is None or viewOnlineStatus is None or viewActiveStatus is None:
                    privacySettings = userProfilePrivacy.objects.get(user__exact = toUserInstance)
                    friendShipCache = caches['friendShipStatusCache']
                    friendShipStatusDict = friendShipCache.get(myId, None)
                    if friendShipStatusDict is None:
                        friendsIdsCache = caches['friendsIdCache']
                        friendsIds = friendsIdsCache.get(myId, None)
                        if friendsIds is None:
                            friendsCache = caches['friendsCache']
                            cachedFriends = friendsCache.get(myId, None)
                            if cachedFriends is None:
                                cachedFriends = curUser.friends.all()
                                if cachedFriends.count():
                                    friendsCache.set(myId, cachedFriends)
                                    friendsIdsQuerySet = cachedFriends.values("id")
                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                else:
                                    friendsCache.set(myId, False)
                                    friendsIds = set()
                            else:
                                if cachedFriends is not False:
                                    friendsIdsQuerySet = cachedFriends.values("id")
                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                else:
                                    friendsIds = set()
                            friendsIdsCache.set(myId, friendsIds)
                        if toUserId in friendsIds:
                            privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = True, isAuthenticated = True)
                            friendShipCache.set(myId, {toUserId: True})
                        else:
                            friendShipRequestsCache = caches['friendshipRequests']
                            friendShipRequests = friendShipRequestsCache.get(myId, None)
                            friendShipStatus = False
                            privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = False, isAuthenticated = True)
                            if friendShipRequests is not None:
                                for friendShipRequest in friendShipRequests:
                                    if friendShipRequest[0] == toUserId:
                                        friendShipStatus = "request"
                                        privacyChecker.isFriend = True
                                        break
                                if friendShipStatus is False:
                                    friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                    if friendShipRequests is not None:
                                        for friendShipRequest in friendShipRequests:
                                            if friendShipRequest[0] == myId:
                                                friendShipStatus = "myRequest"
                                                break
                            friendShipCache.set(myId, {toUserId: friendShipStatus})
                    else:
                        friendShipStatus = friendShipStatusDict.get(toUserId, None)
                        if friendShipStatus is None:
                            friendsIdsCache = caches['friendsIdCache']
                            friendsIds = friendsIdsCache.get(myId, None)
                            if friendsIds is None:
                                friendsCache = caches['friendsCache']
                                cachedFriends = friendsCache.get(myId, None)
                                if cachedFriends is None:
                                    cachedFriends = curUser.friends.all()
                                    if cachedFriends.count():
                                        friendsCache.set(myId, cachedFriends)
                                        friendsIdsQuerySet = cachedFriends.values("id")
                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    else:
                                        friendsCache.set(myId, False)
                                        friendsIds = set()
                                else:
                                    if cachedFriends is not False:
                                        friendsIdsQuerySet = cachedFriends.values("id")
                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    else:
                                        friendsIds = set()
                                friendsIdsCache.set(myId, friendsIds)
                            if toUserId in friendsIds:
                                privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                friendShipStatusDict.update({toUserId: True})
                            else:
                                friendShipRequestsCache = caches['friendshipRequests']
                                friendShipRequests = friendShipRequestsCache.get(myId, None)
                                friendShipStatus = False
                                privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                if friendShipRequests is not None:
                                    for friendShipRequest in friendShipRequests:
                                        if friendShipRequest[0] == toUserId:
                                            friendShipStatus = "request"
                                            privacyChecker.isFriend = True
                                            break
                                    if friendShipStatus is False:
                                        friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                        if friendShipRequests is not None:
                                            for friendShipRequest in friendShipRequests:
                                                if friendShipRequest[0] == myId:
                                                    friendShipStatus = "myRequest"
                                                    break
                                friendShipStatusDict.update({toUserId: friendShipStatus})
                            friendShipCache.set(myId, friendShipStatusDict)
                        else:
                            privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                    if viewProfile is None:
                        viewProfile = privacyChecker.checkViewProfilePrivacy()
                        userPrivacyDict.update({'viewProfile': viewProfile})
                    if viewMainInfo is None:
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                    if viewTHPSInfo is None:
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                    if viewAvatar is None:
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        userPrivacyDict.update({'viewAvatar': viewAvatar})
                    if sendMessages is None:
                        sendMessages = privacyChecker.sendPersonalMessages()
                        userPrivacyDict.update({'sendPersonalMessages': sendMessages})
                    if viewOnlineStatus is None:
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                    if viewActiveStatus is None:
                        viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                        userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                    privacyDictHasChanges = True
        privacyDictMy = privacyDictDict.get(myId, None)
        if privacyDictMy is None:
            privacySettings = userProfilePrivacy.objects.get(user__exact = curUser)
            if friendShipStatus is None:
                friendShipCache = caches['friendShipStatusCache']
                friendShipStatusDict = friendShipCache.get(myId, None)
                if friendShipStatusDict is None:
                    if friendsIds is None:
                        friendsIdsCache = caches['friendsIdCache']
                        friendsIds = friendsIdsCache.get(myId, None)
                        if friendsIds is None:
                            friendsCache = caches['friendsCache']
                            cachedFriends = friendsCache.get(myId, None)
                            if cachedFriends is None:
                                cachedFriends = curUser.friends.all()
                                if cachedFriends.count():
                                    friendsCache.set(myId, cachedFriends)
                                    friendsIdsQuerySet = cachedFriends.values("id")
                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                else:
                                    friendsCache.set(myId, False)
                                    friendsIds = set()
                            else:
                                if cachedFriends is not False:
                                    friendsIdsQuerySet = cachedFriends.values("id")
                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                else:
                                    friendsIds = set()
                            friendsIdsCache.set(myId, friendsIds)
                    if toUserId in friendsIds:
                        friendShipCache.set(myId, {toUserId: True})
                    else:
                        friendShipRequestsCache = caches['friendshipRequests']
                        friendShipRequests = friendShipRequestsCache.get(myId, None)
                        friendShipStatus = False
                        if friendShipRequests is not None:
                            for friendShipRequest in friendShipRequests:
                                if friendShipRequest[0] == toUserId:
                                    friendShipStatus = "request"
                                    break
                            if friendShipStatus is False:
                                friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                if friendShipRequests is not None:
                                    for friendShipRequest in friendShipRequests:
                                        if friendShipRequest[0] == myId:
                                            friendShipStatus = "myRequest"
                                            break
                        friendShipCache.set(myId, {toUserId: friendShipStatus})
                else:
                    friendShipStatus = friendShipStatusDict.get(toUserId, None)
                    if friendShipStatus is None:
                        if friendsIds is None:
                            friendsIdsCache = caches['friendsIdCache']
                            friendsIds = friendsIdsCache.get(myId, None)
                            if friendsIds is None:
                                friendsCache = caches['friendsCache']
                                cachedFriends = friendsCache.get(myId, None)
                                if cachedFriends is None:
                                    cachedFriends = curUser.friends.all()
                                    if cachedFriends.count():
                                        friendsCache.set(myId, cachedFriends)
                                        friendsIdsQuerySet = cachedFriends.values("id")
                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    else:
                                        friendsCache.set(myId, False)
                                        friendsIds = set()
                                else:
                                    if cachedFriends is not False:
                                        friendsIdsQuerySet = cachedFriends.values("id")
                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    else:
                                        friendsIds = set()
                                friendsIdsCache.set(myId, friendsIds)
                        if toUserId in friendsIds:
                            friendShipStatusDict.update({toUserId: True})
                        else:
                            friendShipRequestsCache = caches['friendshipRequests']
                            friendShipRequests = friendShipRequestsCache.get(myId, None)
                            friendShipStatus = False
                            if friendShipRequests is not None:
                                for friendShipRequest in friendShipRequests:
                                    if friendShipRequest[0] == toUserId:
                                        friendShipStatus = "request"
                                        break
                                if friendShipStatus is False:
                                    friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                    if friendShipRequests is not None:
                                        for friendShipRequest in friendShipRequests:
                                            if friendShipRequest[0] == myId:
                                                friendShipStatus = "myRequest"
                                                break
            privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
            sendMessagesMyself = privacyChecker.sendPersonalMessages()
            privacyDictMyHasChanges = None
            privacyDictDict.update({myId: {toUserId: {'sendPersonalMessages': sendMessagesMyself}}})
        else:
            userPrivacyDictMy = privacyDictMy.get(toUserId, None)
            if userPrivacyDictMy is None:
                privacySettings = userProfilePrivacy.objects.get(user__exact = curUser)
                if friendShipStatus is None:
                    friendShipCache = caches['friendShipStatusCache']
                    friendShipStatusDict = friendShipCache.get(myId, None)
                    if friendShipStatusDict is None:
                        if friendsIds is None:
                            friendsIdsCache = caches['friendsIdCache']
                            friendsIds = friendsIdsCache.get(myId, None)
                            if friendsIds is None:
                                friendsCache = caches['friendsCache']
                                cachedFriends = friendsCache.get(myId, None)
                                if cachedFriends is None:
                                    cachedFriends = curUser.friends.all()
                                    if cachedFriends.count():
                                        friendsCache.set(myId, cachedFriends)
                                        friendsIdsQuerySet = cachedFriends.values("id")
                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    else:
                                        friendsCache.set(myId, False)
                                        friendsIds = set()
                                else:
                                    if cachedFriends is not False:
                                        friendsIdsQuerySet = cachedFriends.values("id")
                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    else:
                                        friendsIds = set()
                                friendsIdsCache.set(myId, friendsIds)
                        if toUserId in friendsIds:
                            friendShipCache.set(myId, {toUserId: True})
                        else:
                            friendShipRequestsCache = caches['friendshipRequests']
                            friendShipRequests = friendShipRequestsCache.get(myId, None)
                            friendShipStatus = False
                            if friendShipRequests is not None:
                                for friendShipRequest in friendShipRequests:
                                    if friendShipRequest[0] == toUserId:
                                        friendShipStatus = "request"
                                        break
                                if friendShipStatus is False:
                                    friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                    if friendShipRequests is not None:
                                        for friendShipRequest in friendShipRequests:
                                            if friendShipRequest[0] == myId:
                                                friendShipStatus = "myRequest"
                                                break
                            friendShipCache.set(myId, {toUserId: friendShipStatus})
                    else:
                        friendShipStatus = friendShipStatusDict.get(toUserId, None)
                        if friendShipStatus is None:
                            if friendsIds is None:
                                friendsIdsCache = caches['friendsIdCache']
                                friendsIds = friendsIdsCache.get(myId, None)
                                if friendsIds is None:
                                    friendsCache = caches['friendsCache']
                                    cachedFriends = friendsCache.get(myId, None)
                                    if cachedFriends is None:
                                        cachedFriends = curUser.friends.all()
                                        if cachedFriends.count():
                                            friendsCache.set(myId, cachedFriends)
                                            friendsIdsQuerySet = cachedFriends.values("id")
                                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                        else:
                                            friendsCache.set(myId, False)
                                            friendsIds = set()
                                    else:
                                        if cachedFriends is not False:
                                            friendsIdsQuerySet = cachedFriends.values("id")
                                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                        else:
                                            friendsIds = set()
                                    friendsIdsCache.set(myId, friendsIds)
                            if toUserId in friendsIds:
                                friendShipStatusDict.update({toUserId: True})
                            else:
                                friendShipRequestsCache = caches['friendshipRequests']
                                friendShipRequests = friendShipRequestsCache.get(myId, None)
                                friendShipStatus = False
                                if friendShipRequests is not None:
                                    for friendShipRequest in friendShipRequests:
                                        if friendShipRequest[0] == toUserId:
                                            friendShipStatus = "request"
                                            break
                                    if friendShipStatus is False:
                                        friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                        if friendShipRequests is not None:
                                            for friendShipRequest in friendShipRequests:
                                                if friendShipRequest[0] == myId:
                                                    friendShipStatus = "myRequest"
                                                    break
                privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                sendMessagesMyself = privacyChecker.sendPersonalMessages()
                privacyDictMyHasChanges = True
                privacyDictMy.update({toUserId: {'sendPersonalMessages': sendMessagesMyself}})
            else:
                sendMessagesMyself = userPrivacyDictMy.get('sendPersonalMessages', None)
                if sendMessagesMyself is None:
                    privacySettings = userProfilePrivacy.objects.get(user__exact = curUser)
                    if friendShipStatus is None:
                        friendShipCache = caches['friendShipStatusCache']
                        friendShipStatusDict = friendShipCache.get(myId, None)
                        if friendShipStatusDict is None:
                            if friendsIds is None:
                                friendsIdsCache = caches['friendsIdCache']
                                friendsIds = friendsIdsCache.get(myId, None)
                                if friendsIds is None:
                                    friendsCache = caches['friendsCache']
                                    cachedFriends = friendsCache.get(myId, None)
                                    if cachedFriends is None:
                                        cachedFriends = curUser.friends.all()
                                        if cachedFriends.count():
                                            friendsCache.set(myId, cachedFriends)
                                            friendsIdsQuerySet = cachedFriends.values("id")
                                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                        else:
                                            friendsCache.set(myId, False)
                                            friendsIds = set()
                                    else:
                                        if cachedFriends is not False:
                                            friendsIdsQuerySet = cachedFriends.values("id")
                                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                        else:
                                            friendsIds = set()
                                    friendsIdsCache.set(myId, friendsIds)
                            if toUserId in friendsIds:
                                friendShipCache.set(myId, {toUserId: True})
                            else:
                                friendShipRequestsCache = caches['friendshipRequests']
                                friendShipRequests = friendShipRequestsCache.get(myId, None)
                                friendShipStatus = False
                                if friendShipRequests is not None:
                                    for friendShipRequest in friendShipRequests:
                                        if friendShipRequest[0] == toUserId:
                                            friendShipStatus = "request"
                                            break
                                    if friendShipStatus is False:
                                        friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                        if friendShipRequests is not None:
                                            for friendShipRequest in friendShipRequests:
                                                if friendShipRequest[0] == myId:
                                                    friendShipStatus = "myRequest"
                                                    break
                                friendShipCache.set(myId, {toUserId: friendShipStatus})
                        else:
                            friendShipStatus = friendShipStatusDict.get(toUserId, None)
                            if friendShipStatus is None:
                                if friendsIds is None:
                                    friendsIdsCache = caches['friendsIdCache']
                                    friendsIds = friendsIdsCache.get(myId, None)
                                    if friendsIds is None:
                                        friendsCache = caches['friendsCache']
                                        cachedFriends = friendsCache.get(myId, None)
                                        if cachedFriends is None:
                                            cachedFriends = curUser.friends.all()
                                            if cachedFriends.count():
                                                friendsCache.set(myId, cachedFriends)
                                                friendsIdsQuerySet = cachedFriends.values("id")
                                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                            else:
                                                friendsCache.set(myId, False)
                                                friendsIds = set()
                                        else:
                                            if cachedFriends is not False:
                                                friendsIdsQuerySet = cachedFriends.values("id")
                                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                            else:
                                                friendsIds = set()
                                        friendsIdsCache.set(myId, friendsIds)
                                if toUserId in friendsIds:
                                    friendShipStatusDict.update({toUserId: True})
                                else:
                                    friendShipRequestsCache = caches['friendshipRequests']
                                    friendShipRequests = friendShipRequestsCache.get(myId, None)
                                    friendShipStatus = False
                                    if friendShipRequests is not None:
                                        for friendShipRequest in friendShipRequests:
                                            if friendShipRequest[0] == toUserId:
                                                friendShipStatus = "request"
                                                break
                                        if friendShipStatus is False:
                                            friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                            if friendShipRequests is not None:
                                                for friendShipRequest in friendShipRequests:
                                                    if friendShipRequest[0] == myId:
                                                        friendShipStatus = "myRequest"
                                                        break
                    privacyChecker = checkPrivacy(user = toUserInstance, privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                    sendMessagesMyself = privacyChecker.sendPersonalMessages()
                    userPrivacyDictMy.update({'sendPersonalMessages': sendMessagesMyself})
                    privacyDictMyHasChanges = True
        if privacyDictHasChanges or privacyDictMyHasChanges:
            if privacyDictHasChanges and privacyDictMyHasChanges:
                privacyCache.set_many({myId: privacyDictMy, toUserId: privacyDict})
            else:
                if privacyDictHasChanges:
                    privacyCache.set(toUserId, privacyDict)
                elif privacyDictMyHasChanges:
                    privacyCache.set(myId, privacyDictMy)
        else:
            if privacyDictHasChanges is None and privacyDictMyHasChanges is None:
                privacyCache.set_many(privacyDictDict)
        dialogCache = caches['dialogSingle']
        cachedDialog = dialogCache.get(myId, None)
        if cachedDialog is None:
            dialog = dialogMessage.objects.filter(users__exact = curUser).filter(users__exact = toUserInstance).filter(type__exact = True)
            dialogIsExists = dialog.count() > 0
            if dialogIsExists:
                dialog = dialog[0]
                dialogCache.set(myId, {'instance': dialog, 'withUserId': toUserId})
        else:
            withUser = cachedDialog.get('withUserId', None)
            if withUser is None:
                dialog = dialogMessage.objects.filter(users__exact = curUser).filter(users__exact = toUserInstance).filter(type__exact = True)
                dialogIsExists = dialog.count() > 0
                if dialogIsExists:
                    dialog = dialog[0]
                    dialogCache.set(myId, {'instance': dialog, 'withUserId': toUserId})
            else:
                dialog = cachedDialog.get('instance', None)
                if dialog is None:
                    dialog = dialogMessage.objects.filter(users__exact = curUser).filter(users__exact = toUserInstance).filter(type__exact = True)
                    dialogIsExists = dialog.count() > 0
                    if dialogIsExists:
                        dialog = dialog[0]
                        dialogCache.set(myId, {'instance': dialog, 'withUserId': toUserId})
                else:
                    if withUser != toUserId:
                        dialog = dialogMessage.objects.filter(users__exact = curUser).filter(users__exact = toUserInstance).filter(type__exact = True)
                        dialogIsExists = dialog.count() > 0
                        if dialogIsExists:
                            dialog = dialog[0]
                            dialogCache.set(myId, {'instance': dialog, 'withUserId': toUserId})
                    else:
                        dialogIsExists = True
        reqGet = request.GET
        if dialogIsExists:
            dialogId = str(dialog.id)
            if 'list' in reqGet:
                currentPage = reqGet.get('list', 0)
                currentPage = json.loads(currentPage)
                messages, unreadMessagesDictList = loadDialogSimpleShort(dialogId, myId, toUserId, currentPage)
                if messages is not None:
                    return JsonResponse({"messagesItems": {"messages": messages, "unread": unreadMessagesDictList}})
                return JsonResponse({"messagesItems": None})
            currentPage = 0
            toUserUsername = toUserInstance.username
            myUserProfileUrl = curUser.profileUrl
            myProfilePath = reverse("profile", kwargs = {'currentUser': myUserProfileUrl})
            toUserProfilePath = reverse("profile", kwargs = {'currentUser': toUserInstance.profileUrl})
            myUsername = curUser.username
            messagesFilesStorage = caches['filesStorageMessages']
            cachedFilesDict = messagesFilesStorage.get(dialogId, None)
            if cachedFilesDict is not None:
                cachedFilesDictDict = cachedFilesDict.get(myId, None)
                if cachedFilesDictDict is not None:
                    cachedFilesDictDictImages = cachedFilesDictDict.get("images", None)
                else:
                    cachedFilesDictDictImages = None
            else:
                cachedFilesDictDictImages = None
            fragments = cachedFragments(myUsername = myUsername, mySlug = myUserProfileUrl,
                                    username = toUserUsername, profileUrl = toUserInstance.profileUrl, url = request.path)
            myFirstName = None
            myLastName = None
            toUserFirstName = None
            toUserLastName = None
            #thpsProfiles = THPSProfile.objects.filter(toUser__in = {myId, toUserId})
            #try:
                #myTHPSProfile = thpsProfiles.objects.get(toUser__exact = myId)
            #except:
                #myTHPSProfile = None
            #try:
                #toUserTHPSProfile = thpsProfiles.objects.get(toUser__exact = toUserId)
            #except:
                #toUserTHPSProfile = None
            #if myTHPSProfile and toUserTHPSProfile:
                #myNick = myTHPSProfile.nickName
                #toUserNick = toUserThpsProfile.nickName
            #else:
                #if myTHPSProfile:
                    #myNick = myTHPSProfile.nickName
                    #toUserNick = None
                #elif toUserTHPSProfile:
                    #myNick = None
                    #toUserNick = toUserThpsProfile.nickName
                #else:
                    #myNick = None
                    #toUserNick = None
            myListThumbChanged, toUserListThumbChanged = False, False
            listThumbsCache = caches['messagesListThumbs']
            listThumbsManyMany = listThumbsCache.get_many({myId, toUserId})
            myListThumbsDict = listThumbsManyMany.get(myId, None)
            toUserListThumbsDict = listThumbsManyMany.get(toUserId, None)
            if myListThumbsDict is None:
                myListThumbChanged = True
                myFirstName = curUser.first_name or None
                myLastName = curUser.last_name or None
                if myFirstName is not None and myLastName is not None:
                    name = '{0} {1}'.format(myFirstName, myLastName)
                    smallName = curUser.username
                else:
                    if myFirstName is not None:
                        name = myFirstName
                        smallName = curUser.username
                    else:
                        name = curUser.username
                        smallName = None
                avatar = curUser.avatarThumbSmaller or None
                if avatar is not None:
                    if smallName is not None:
                        myListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><p><a href = "{1}">{3}</a></p><small><a href = "{1}">{4}</a></small></div>'.format(myId, myProfilePath, avatar.url, name, smallName)
                    else:
                        myListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><p><a href = "{1}">{3}</a></p></div>'.format(myId, myProfilePath, avatar.url, name)
                else:
                    if smallName is not None:
                        myListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><p><a href = "{3}">{1}</a></p><small><a href = "{3}">{2}</a></small></div>'.format(myId, name, smallName, myProfilePath)
                    else:
                        myListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><p><a href = "{2}">{1}</a></p></div>'.format(myId, name, myProfilePath)
                myListThumbsDict = {myId: myListThumbItem}
            else:
                myListThumbItemDict = myListThumbsDict.get(myId, None)
                if myListThumbItemDict is None:
                    myListThumbChanged = True
                    myFirstName = curUser.first_name or None
                    myLastName = curUser.last_name or None
                    if myFirstName and myLastName:
                        name = '{0} {1}'.format(myFirstName, myLastName)
                        smallName = curUser.username
                    else:
                        if myFirstName:
                            name = myFirstName
                            smallName = curUser.username
                        else:
                            name = curUser.username
                            smallName = None
                    avatar = curUser.avatarThumbSmaller or None
                    if avatar is not None:
                        if smallName:
                            myListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><p><a href = "{1}">{3}</a></p><small><a href = "{1}">{4}</a></small></div>'.format(myId, myProfilePath, avatar.url, name, smallName)
                        else:
                            myListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><p><a href = "{1}">{3}</a></p></div>'.format(myId, myProfilePath, avatar.url, name)
                    else:
                        if smallName:
                            myListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><p><a href = "{3}">{1}</a></p><small><a href = "{3}">{2}</a></small></div>'.format(myId, name, smallName, myProfilePath)
                        else:
                            myListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><p><a href = "{2}">{1}</a></p></div>'.format(myId, name, myProfilePath)
                    myListThumbsDict.update({myId: myListThumbItem})
                else:
                    myListThumbItem = myListThumbItemDict
            if toUserListThumbsDict is None:
                toUserListThumbChanged = True
                if viewMainInfo:
                    toUserFirstName = toUserInstance.first_name or None
                    toUserLastName = toUserInstance.last_name or None
                    if toUserFirstName and toUserLastName:
                        name = '{0} {1}'.format(toUserFirstName, toUserLastName)
                        smallName = toUserInstance.username
                    else:
                        if toUserFirstName:
                            name = toUserFirstName
                            smallName = toUserInstance.username
                        else:
                            name = toUserInstance.username
                            smallName = None
                else:
                    name = toUserInstance.username
                    smallName = None
                if viewAvatar:
                    avatar = toUserInstance.avatarThumbSmaller or None
                    if avatar is not None:
                        if smallName:
                            toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><p><a href = "{1}">{3}</a></p><small><a href = "{1}">{4}</a></small></div>'.format(toUserId, toUserProfilePath, avatar.url, name, smallName)
                        else:
                            toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><p><a href = "{1}">{3}</a></p></div>'.format(toUserId, toUserProfilePath, avatar.url, name)
                    else:
                        if smallName:
                            toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><p><a href = "{3}">{1}</a></p><small><a href = "{3}">{2}</a></small></div>'.format(toUserId, name, smallName, toUserProfilePath)
                        else:
                            toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><p><a href = "{2}">{1}</a></p></div>'.format(toUserId, name, toUserProfilePath)
                else:
                    if smallName:
                        toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><p><a href = "{3}">{1}</a></p><small><a href = "{3}">{2}</a></small></div>'.format(toUserId, name, smallName, toUserProfilePath)
                    else:
                        toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><p><a href = "{2}">{1}</a></p></div>'.format(toUserId, name, toUserProfilePath)
                toUserListThumbsDict = {myId: toUserListThumbItem}
            else:
                toUserListThumbItemDict = toUserListThumbsDict.get(myId, None)
                if toUserListThumbItemDict is None:
                    toUserListThumbChanged = True
                    if viewMainInfo:
                        toUserFirstName = toUserInstance.first_name or None
                        toUserLastName = toUserInstance.last_name or None
                        if toUserFirstName and toUserLastName:
                            name = '{0} {1}'.format(toUserFirstName, toUserLastName)
                            smallName = toUserInstance.username
                        else:
                            if toUserFirstName:
                                name = toUserFirstName
                                smallName = toUserInstance.username
                            else:
                                name = toUserInstance.username
                                smallName = None
                    else:
                        name = toUserInstance.username
                        smallName = None
                    if viewAvatar:
                        avatar = toUserInstance.avatarThumbSmaller or None
                        if avatar is not None:
                            if smallName:
                                toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><p><a href = "{1}">{3}</a></p><small><a href = "{1}">{4}</a></small></div>'.format(toUserId, toUserProfilePath, avatar.url, name, smallName)
                            else:
                                toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><p><a href = "{1}">{3}</a></p></div>'.format(toUserId, toUserProfilePath, avatar.url, name)
                        else:
                            if smallName:
                                toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><p><a href = "{3}">{1}</a></p><small><a href = "{3}">{2}</a></small></div>'.format(toUserId, name, smallName, toUserProfilePath)
                            else:
                                toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><p><a href = "{2}">{1}</a></p></div>'.format(toUserId, name, toUserProfilePath)
                    else:
                        if smallName:
                            toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><p><a href = "{3}">{1}</a></p><small><a href = "{3}">{2}</a></small></div>'.format(toUserId, name, smallName, toUserProfilePath)
                        else:
                            toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><p><a href = "{2}">{1}</a></p></div>'.format(toUserId, name, toUserProfilePath)
                    toUserListThumbsDict.update({myId: toUserListThumbItem})
                else:
                    toUserListThumbItem = toUserListThumbItemDict
            if myListThumbChanged and toUserListThumbChanged:
                listThumbsCache.set_many({myId: myListThumbsDict, toUserId: toUserListThumbsDict})
            else:
                if myListThumbChanged:
                    listThumbsCache.set(myId, myListThumbsDict)
                elif toUserListThumbChanged:
                    listThumbsCache.set(toUserId, toUserListThumbsDict)
            if request.is_ajax():
                if viewOnlineStatus:
                    if caches['onlineUsers'].get(toUserId, None) is not None:
                        onlineStatusDict = {toUserId: 'online'}
                        if viewActiveStatus:
                            if caches['chatStatusActive'].get(toUserId, None) is not None:
                                activeStatusDict = {toUserId: 'stateActive'}
                            else:
                                middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                if middleDataTuple is not None:
                                    activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                else:
                                    activeStatusDict = None
                        else:
                            activeStatusDict = None
                    else:
                        iffyTimeStr = caches['iffyUsersOnlineStatus'].get(toUserId, None)
                        if iffyTimeStr is not None:
                            onlineStatusDict = {toUserId: {'iffy': iffyTimeStr}}
                            if viewActiveStatus:
                                if caches['chatStatusActive'].get(toUserId, None) is not None:
                                    activeStatusDict = {toUserId: 'stateActive'}
                                else:
                                    middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                    if middleDataTuple is not None:
                                        activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                    else:
                                        activeStatusDict = None
                            else:
                                activeStatusDict = None
                        else:
                            onlineStatusDict = None
                            activeStatusDict = None
                else:
                    onlineStatusDict = None
                    if viewActiveStatus:
                        if caches['chatStatusActive'].get(toUserId, None) is not None:
                            activeStatusDict = {toUserId: 'stateActive'}
                        else:
                            middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                            if middleDataTuple is not None:
                                activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                            else:
                                activeStatusDict = None
                    else:
                        activeStatusDict = None
                myMainThumbChanged, toUserMainThumbChanged = False, False
                mainThumbsCache = caches['dialogThumbs']
                mainThumbsManyMany = mainThumbsCache.get_many({myId, toUserId})
                myMainThumbsDict = mainThumbsManyMany.get(myId, None)
                toUserMainThumbsDict = mainThumbsManyMany.get(toUserId, None)
                if myMainThumbsDict is None:
                    myMainThumbChanged = True
                    if myFirstName is None:
                        myFirstName = curUser.first_name or None
                    if myLastName is None:
                        myLastName = curUser.last_name or None
                    if myFirstName is not None and myLastName is not None:
                        name = '{0} {1}'.format(myFirstName, myLastName)
                        smallName = curUser.username
                    else:
                        if myFirstName:
                            name = myFirstName
                            smallName = curUser.username
                        else:
                            name = curUser.username
                            smallName = None
                    avatar = curUser.avatarThumbMiddleSmall or None
                    if avatar is not None:
                        if smallName:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(myId, myProfilePath, avatar.url, name, smallName)
                        else:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(myId, myProfilePath, avatar.url, name)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(myId, name, smallName, myProfilePath)
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(myId, name, myProfilePath)
                    myMainThumb = (itemString0, '</div>')
                    myMainThumbsDict = {myId: {'item': myMainThumb, 'showOnlineStatus': True, 'showActiveStatus': True}}
                else:
                    myMainThumb = myMainThumbsDict.get(myId, None)
                    if myMainThumb is None:
                        myMainThumbChanged = True
                        if myFirstName is None:
                            myFirstName = curUser.first_name or None
                        if myLastName is None:
                            myLastName = curUser.last_name or None
                        if myFirstName and myLastName:
                            name = '{0} {1}'.format(myFirstName, myLastName)
                            smallName = curUser.username
                        else:
                            if myFirstName:
                                name = myFirstName
                                smallName = curUser.username
                            else:
                                name = curUser.username
                                smallName = None
                        avatar = curUser.avatarThumbMiddleSmall or None
                        if avatar is not None:
                            if smallName:
                                itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(myId, myProfilePath, avatar.url, name, smallName)
                            else:
                                itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(myId, myProfilePath, avatar.url, name)
                        else:
                            if smallName:
                                itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(myId, name, smallName, myProfilePath)
                            else:
                                itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(myId, name, myProfilePath)
                        myMainThumb = (itemString0, '</div>')
                        myMainThumbsDict.update({myId: {'item': myMainThumb, 'showOnlineStatus': True, 'showActiveStatus': True}})
                    else:
                        myMainThumb = myMainThumb['item']
                if toUserMainThumbsDict is None:
                    toUserMainThumbChanged = True
                    if viewMainInfo:
                        if toUserFirstName is None:
                            toUserFirstName = toUserInstance.first_name or None
                        if toUserLastName is None:
                            toUserLastName = toUserInstance.last_name or None
                        if toUserFirstName and toUserLastName:
                            name = '{0} {1}'.format(toUserFirstName, toUserLastName)
                            smallName = toUserInstance.username
                        else:
                            if toUserFirstName:
                                name = toUserFirstName
                                smallName = toUserInstance.username
                            else:
                                name = toUserInstance.username
                                smallName = None
                    else:
                        name = toUserInstance.username
                        smallName = None
                    if viewAvatar:
                        avatar = toUserInstance.avatarThumbMiddleSmall or None
                        if avatar is not None:
                            if smallName:
                                itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(toUserId, toUserProfilePath, avatar.url, name, smallName)
                            else:
                                itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(toUserId, toUserProfilePath, avatar.url, name)
                        else:
                            if smallName:
                                itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                            else:
                                itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                    toUserMainThumb = (itemString0, '</div>')
                    toUserMainThumbsDict = {myId: {'item': toUserMainThumb, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus}}
                else:
                    toUserMainThumb = toUserMainThumbsDict.get(myId, None)
                    if toUserMainThumb is None:
                        toUserMainThumbChanged = True
                        if viewMainInfo:
                            if toUserFirstName is None:
                                toUserFirstName = toUserInstance.first_name or None
                            if toUserLastName is None:
                                toUserLastName = toUserInstance.last_name or None
                            if toUserFirstName and toUserLastName:
                                name = '{0} {1}'.format(toUserFirstName, toUserLastName)
                                smallName = toUserInstance.username
                            else:
                                if toUserFirstName:
                                    name = toUserFirstName
                                    smallName = toUserInstance.username
                                else:
                                    name = toUserInstance.username
                                    smallName = None
                        else:
                            name = toUserInstance.username
                            smallName = None
                        if viewAvatar:
                            avatar = toUserInstance.avatarThumbMiddleSmall or None
                            if avatar is not None:
                                if smallName:
                                    itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(toUserId, toUserProfilePath, avatar.url, name, smallName)
                                else:
                                    itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(toUserId, toUserProfilePath, avatar.url, name)
                            else:
                                if smallName:
                                    itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                                else:
                                    itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                        else:
                            if smallName:
                                itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                            else:
                                itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                        toUserMainThumb = (itemString0, '</div>')
                        toUserMainThumbsDict = {myId: {'item': toUserMainThumb, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus}}
                    else:
                        toUserMainThumb = toUserMainThumb.pop('item')
                if myMainThumbChanged or toUserMainThumbChanged:
                    if myMainThumbChanged and toUserMainThumbChanged:
                        mainThumbsCache.set_many({myId: myMainThumbsDict, toUserId: toUserMainThumbsDict})
                    else:
                        if myMainThumbChanged:
                            mainThumbsCache.set(myId, myMainThumbsDict)
                        else:
                            mainThumbsCache.set(toUserId, toUserMainThumbsDict)
                if 'from-dialog' in reqGet:
                    if 'get-messages' in reqGet:
                        messages, unreadMessagesDictList = loadDialogSimple(dialogId, myId, toUserId, currentPage)
                        return JsonResponse({"newDialog": {
                            'requestUserId': myId,
                            'opponentUserId': toUserId,
                            'dialogId': dialogId,
                            'myMainThumb': myMainThumb,
                            'toUserMainThumb': toUserMainThumb,
                            'myListThumb': myListThumbItem,
                            'toUserListThumb': toUserListThumbItem,
                            'onlineStatusDict': onlineStatusDict,
                            'activeStatusDict': activeStatusDict,
                            'imagesToSend': cachedFilesDictDictImages,
                            'messages': messages,
                            'unreadMessagesDictList': unreadMessagesDictList,
                            'accessToSend': sendMessages,
                            'sendImageLink': reverse("dialogMessagesFileLoaderPersonal", kwargs = {'toUserId': toUserId, 'type': 'image'})
                            }})
                    return JsonResponse({"newDialog": {
                        'requestUserId': myId,
                        'opponentUserId': toUserId,
                        'dialogId': dialogId,
                        'myMainThumb': myMainThumb,
                        'toUserMainThumb': toUserMainThumb,
                        'myListThumb': myListThumbItem,
                        'toUserListThumb': toUserListThumbItem,
                        'onlineStatusDict': onlineStatusDict,
                        'activeStatusDict': activeStatusDict,
                        'imagesToSend': cachedFilesDictDictImages,
                        'accessToSend': sendMessages,
                        'sendImageLink': reverse("dialogMessagesFileLoaderPersonal", kwargs = {'toUserId': toUserId, 'type': 'image'})
                        }})
                messages, unreadMessagesDictList = loadDialogSimple(dialogId, myId, toUserId, currentPage)
                if "global" in reqGet:
                    return JsonResponse({"newPage": json.dumps({
                        'newTemplate': rts("psixiSocial/psixiSocial.html", {
                            'parentTemplate': 'base/classicPage/emptyParent.html',
                            'content': ('psixiSocial', 'dialog', 'personal', True),
                            'menu': fragments.menuGetter('menuSerialized'),
                            'messagesListDict': messages,
                            'unreadMessagesDictList': unreadMessagesDictList,
                            'myListThumb': myListThumbItem,
                            'toUserListThumb': toUserListThumbItem,
                            'csrf': checkCSRF(request, cookies),
                            'time': datetime.now(tz = tz.gettz()).microsecond,
                            'imagesToSend': cachedFilesDictDictImages,
                            'requestUserId': myId,
                            'opponentUserId': toUserId,
                            'dialogId': dialogId,
                            'myMainThumb': myMainThumb,
                            'toUserMainThumb': toUserMainThumb,
                            'onlineStatusDict': onlineStatusDict,
                            'activeStatusDict': activeStatusDict,
                            'form': None,
                            'accessToSend': sendMessages
                            }),
                        'url': request.path
                        })})
                messages, unreadMessagesDictList = loadDialogSimple(dialogId, myId, toUserId, currentPage)
                return JsonResponse({"newPage": json.dumps({
                    'templateHead': rts("psixiSocial/head/dialogs/dialogWrite.html"),
                    'templateBody': rts("psixiSocial/body/dialogs/dialogWritePersonalXHR.html", {
                        'csrf': '',
                        'imagesToSend': cachedFilesDictDictImages,
                        'myMainThumb': myMainThumb,
                        'toUserMainThumb': toUserMainThumb,
                        'myListThumb': myListThumbItem,
                        'toUserListThumb': toUserListThumbItem,
                        'requestUserId': myId,
                        'form': None,
                        'accessToSend': sendMessages
                        }),
                    'templateScripts': rts("psixiSocial/scripts/dialogs/dialogWritePersonal.html", {
                        'messagesListDict': messages,
                        'unreadMessagesDictList': unreadMessagesDictList,
                        'myListThumb': myListThumbItem,
                        'toUserListThumb': toUserListThumbItem,
                        'time': datetime.now(tz = tz.gettz()).microsecond,
                        'requestUserId': myId,
                        'opponentUserId': toUserId,
                        'dialogId': dialogId,
                        'onlineStatusDict': onlineStatusDict,
                        'activeStatusDict': activeStatusDict
                        }),
                    'menu': fragments.menuGetter('menu'),
                    'url': request.path,
                    'titl': ''
                    })})
            myMainThumbChanged, toUserMainThumbChanged = False, False
            mainThumbsCache = caches['dialogThumbs']
            mainThumbsManyMany = mainThumbsCache.get_many({myId, toUserId})
            myMainThumbsDict = mainThumbsManyMany.get(myId, None)
            toUserMainThumbsDict = mainThumbsManyMany.get(toUserId, None)
            cookies = request.COOKIES
            if cookies.get('js', None) is not None:
                if myMainThumbsDict is None:
                    myMainThumbChanged = True
                    if myFirstName is None:
                        myFirstName = curUser.first_name or None
                    if myLastName is None:
                        myLastName = curUser.last_name or None
                    if myFirstName is not None and myLastName is not None:
                        name = '{0} {1}'.format(myFirstName, myLastName)
                        smallName = curUser.username
                    else:
                        if myFirstName:
                            name = myFirstName
                            smallName = curUser.username
                        else:
                            name = curUser.username
                            smallName = None
                    avatar = curUser.avatarThumbMiddleSmall or None
                    if avatar is not None:
                        if smallName:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(myId, myProfilePath, avatar.url, name, smallName)
                        else:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(myId, myProfilePath, avatar.url, name)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(myId, name, smallName, myProfilePath)
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(myId, name, myProfilePath)
                    myMainThumb = (itemString0, '</div>')
                    myMainThumbsDict = {myId: {'item': myMainThumb, 'showOnlineStatus': True, 'showActiveStatus': True}}
                else:
                    myMainThumb = myMainThumbsDict.get(myId, None)
                    if myMainThumb is None:
                        myMainThumbChanged = True
                        if myFirstName is None:
                            myFirstName = curUser.first_name or None
                        if myLastName is None:
                            myLastName = curUser.last_name or None
                        if myFirstName and myLastName:
                            name = '{0} {1}'.format(myFirstName, myLastName)
                            smallName = curUser.username
                        else:
                            if myFirstName:
                                name = myFirstName
                                smallName = curUser.username
                            else:
                                name = curUser.username
                                smallName = None
                        avatar = curUser.avatarThumbMiddleSmall or None
                        if avatar is not None:
                            if smallName:
                                itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(myId, myProfilePath, avatar.url, name, smallName)
                            else:
                                itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(myId, myProfilePath, avatar.url, name)
                        else:
                            if smallName:
                                itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(myId, name, smallName, myProfilePath)
                            else:
                                itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(myId, name, myProfilePath)
                        myMainThumb = (itemString0, '</div>')
                        myMainThumbsDict.update({myId: {'item': myMainThumb, 'showOnlineStatus': True, 'showActiveStatus': True}})
                    else:
                        myMainThumb = myMainThumb['item']
                if toUserMainThumbsDict is None:
                    toUserMainThumbChanged = True
                    if viewMainInfo:
                        if toUserFirstName is None:
                            toUserFirstName = toUserInstance.first_name or None
                        if toUserLastName is None:
                            toUserLastName = toUserInstance.last_name or None
                        if toUserFirstName and toUserLastName:
                            name = '{0} {1}'.format(toUserFirstName, toUserLastName)
                            smallName = toUserInstance.username
                        else:
                            if toUserFirstName:
                                name = toUserFirstName
                                smallName = toUserInstance.username
                            else:
                                name = toUserInstance.username
                                smallName = None
                    else:
                        name = toUserInstance.username
                        smallName = None
                    if viewAvatar:
                        avatar = toUserInstance.avatarThumbMiddleSmall or None
                        if avatar is not None:
                            if smallName:
                                itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(toUserId, toUserProfilePath, avatar.url, name, smallName)
                            else:
                                itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(toUserId, toUserProfilePath, avatar.url, name)
                        else:
                            if smallName:
                                itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                            else:
                                itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                    toUserMainThumb = (itemString0, '</div>')
                    toUserMainThumbsDict = {myId: {'item': toUserMainThumb, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus}}
                else:
                    toUserMainThumb = toUserMainThumbsDict.get(myId, None)
                    if toUserMainThumb is None:
                        toUserMainThumbChanged = True
                        if viewMainInfo:
                            if toUserFirstName is None:
                                toUserFirstName = toUserInstance.first_name or None
                            if toUserLastName is None:
                                toUserLastName = toUserInstance.last_name or None
                            if toUserFirstName and toUserLastName:
                                name = '{0} {1}'.format(toUserFirstName, toUserLastName)
                                smallName = toUserInstance.username
                            else:
                                if toUserFirstName:
                                    name = toUserFirstName
                                    smallName = toUserInstance.username
                                else:
                                    name = toUserInstance.username
                                    smallName = None
                        else:
                            name = toUserInstance.username
                            smallName = None
                        if viewAvatar:
                            avatar = toUserInstance.avatarThumbMiddleSmall or None
                            if avatar is not None:
                                if smallName:
                                    itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(toUserId, toUserProfilePath, avatar.url, name, smallName)
                                else:
                                    itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(toUserId, toUserProfilePath, avatar.url, name)
                            else:
                                if smallName:
                                    itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                                else:
                                    itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                        else:
                            if smallName:
                                itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                            else:
                                itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                        toUserMainThumb = (itemString0, '</div>')
                        toUserMainThumbsDict = {myId: {'item': toUserMainThumb, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus}}
                    else:
                        toUserMainThumb = toUserMainThumb.pop('item')
                if viewOnlineStatus:
                    if caches['onlineUsers'].get(toUserId, None) is not None:
                        onlineStatusDict = {toUserId: 'online'}
                        if viewActiveStatus:
                            if caches['chatStatusActive'].get(toUserId, None) is not None:
                                activeStatusDict = {toUserId: 'stateActive'}
                            else:
                                middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                if middleDataTuple is not None:
                                    activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                else:
                                    activeStatusDict = None
                        else:
                            activeStatusDict = None
                    else:
                        iffyTimeStr = caches['iffyUsersOnlineStatus'].get(toUserId, None)
                        if iffyTimeStr is not None:
                            onlineStatusDict = {toUserId: {'iffy': iffyTimeStr}}
                            if viewActiveStatus:
                                if caches['chatStatusActive'].get(toUserId, None) is not None:
                                    activeStatusDict = {toUserId: 'stateActive'}
                                else:
                                    middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                    if middleDataTuple is not None:
                                        activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                    else:
                                        activeStatusDict = None
                            else:
                                activeStatusDict = None
                        else:
                            onlineStatusDict = None
                            activeStatusDict = None
                else:
                    onlineStatusDict = None
                    if viewActiveStatus:
                        if caches['chatStatusActive'].get(toUserId, None) is not None:
                            activeStatusDict = {toUserId: 'stateActive'}
                        else:
                            middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                            if middleDataTuple is not None:
                                activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                            else:
                                activeStatusDict = None
                    else:
                        activeStatusDict = None
                if myMainThumbChanged or toUserMainThumbChanged:
                    if myMainThumbChanged and toUserMainThumbChanged:
                        mainThumbsCache.set_many({myId: myMainThumbsDict, toUserId: toUserMainThumbsDict})
                    else:
                        if myMainThumbChanged:
                            mainThumbsCache.set(myId, myMainThumbsDict)
                        else:
                            mainThumbsCache.set(toUserId, toUserMainThumbsDict)
                messages, unreadMessagesDictList = loadDialogSimple(dialogId, myId, toUserId, currentPage)
                return render(request, "psixiSocial/psixiSocial.html", {
                    'parentTemplate': 'base/classicPage/base.html',
                    'content': ('psixiSocial', 'dialog', 'personal', True),
                    'menu': fragments.menuGetter('menuSerialized'),
                    'messagesListDict': messages,
                    'unreadMessagesDictList': unreadMessagesDictList,
                    'myListThumb': myListThumbItem,
                    'toUserListThumb': toUserListThumbItem,
                    'csrf': checkCSRF(request, cookies),
                    'time': datetime.now(tz = tz.gettz()).microsecond,
                    'imagesToSend': cachedFilesDictDictImages,
                    'requestUserId': myId,
                    'opponentUserId': toUserId,
                    'dialogId': dialogId,
                    'myMainThumb': myMainThumb,
                    'toUserMainThumb': toUserMainThumb,
                    'onlineStatusDict': onlineStatusDict,
                    'activeStatusDict': activeStatusDict,
                    'form': None,
                    'accessToSend': sendMessages
                    })
            onlineStatusText = None
            activeStatusText = None
            if myMainThumbsDict is None:
                myMainThumbChanged = True
                if myFirstName is None:
                    myFirstName = curUser.first_name or None
                if myLastName is None:
                    myLastName = curUser.last_name or None
                if myFirstName is not None and myLastName is not None:
                    name = '{0} {1}'.format(myFirstName, myLastName)
                    smallName = curUser.username
                else:
                    if myFirstName:
                        name = myFirstName
                        smallName = curUser.username
                    else:
                        name = curUser.username
                        smallName = None
                avatar = curUser.avatarThumbMiddleSmall or None
                if avatar is not None:
                    if smallName:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(myId, myProfilePath, avatar.url, name, smallName)
                    else:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(myId, myProfilePath, avatar.url, name)
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(myId, name, smallName, myProfilePath)
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(myId, name, myProfilePath)
                itemString1 = '</div>'
                myMainThumbsDict = {myId: {'item': (itemString0, itemString1), 'showOnlineStatus': True, 'showActiveStatus': True}}
                textDict = usersListText(lang = 'ru').getDict()
                onlineStatusText = textDict['onlineStatus']
                activeStatusText = textDict['activeStatus']
                myMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), itemString1)
            else:
                myMainThumb = myMainThumbsDict.get(myId, None)
                if myMainThumb is None:
                    myMainThumbChanged = True
                    if myFirstName is None:
                        myFirstName = curUser.first_name or None
                    if myLastName is None:
                        myLastName = curUser.last_name or None
                    if myFirstName and myLastName:
                        name = '{0} {1}'.format(myFirstName, myLastName)
                        smallName = curUser.username
                    else:
                        if myFirstName:
                            name = myFirstName
                            smallName = curUser.username
                        else:
                            name = curUser.username
                            smallName = None
                    avatar = curUser.avatarThumbMiddleSmall or None
                    if avatar is not None:
                        if smallName:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(myId, myProfilePath, avatar.url, name, smallName)
                        else:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(myId, myProfilePath, avatar.url, name)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(myId, name, smallName, myProfilePath)
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(myId, name, myProfilePath)
                    itemString1 = '</div>'
                    myMainThumbsDict.update({myId: {'item': (itemString0, itemString1), 'showOnlineStatus': True, 'showActiveStatus': True}})
                    textDict = usersListText(lang = 'ru').getDict()
                    onlineStatusText = textDict['onlineStatus']
                    activeStatusText = textDict['activeStatus']
                    myMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), itemString1)
                else:
                    textDict = usersListText(lang = 'ru').getDict()
                    onlineStatusText = textDict['onlineStatus']
                    activeStatusText = textDict['activeStatus']
                    myMainThumb = myMainThumb['item']
                    myMainThumb = (myMainThumb[0], '<noscript><p class = "online">{0}</p><br><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), myMainThumb[1])
            if toUserMainThumbsDict is None:
                toUserMainThumbChanged = True
                if viewMainInfo:
                    if toUserFirstName is None:
                        toUserFirstName = toUserInstance.first_name or None
                    if toUserLastName is None:
                        toUserLastName = toUserInstance.last_name or None
                    if toUserFirstName and toUserLastName:
                        name = '{0} {1}'.format(toUserFirstName, toUserLastName)
                        smallName = toUserInstance.username
                    else:
                        if toUserFirstName:
                            name = toUserFirstName
                            smallName = toUserInstance.username
                        else:
                            name = toUserInstance.username
                            smallName = None
                else:
                    name = toUserInstance.username
                    smallName = None
                if viewAvatar:
                    avatar = toUserInstance.avatarThumbMiddleSmall or None
                    if avatar is not None:
                        if smallName:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(toUserId, toUserProfilePath, avatar.url, name, smallName)
                        else:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(toUserId, toUserProfilePath, avatar.url, name)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                itemString1 = '</div>'
                itemTuple = (itemString0, itemString1)
                toUserMainThumb = {'item': itemTuple, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus}
                toUserMainThumbsDict = {myId: toUserMainThumb}
                if viewOnlineStatus:
                    if caches['onlineUsers'].get(toUserId, None) is not None:
                        if onlineStatusText is None:
                            onlineStatusText = textDict['onlineStatus']
                        if viewActiveStatus:
                            if caches['chatStatusActive'].get(toUserId, None) is not None:
                                if activeStatusText is None:
                                    activeStatusText = textDict['activeStatus']
                                toUserMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), itemString1)
                                activeStatusDict = {toUserId: 'stateActive'}
                            else:
                                middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                if middleDataTuple is not None:
                                    middleStatusText = textDict['middleStatus']
                                    toUserMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "middle">{1}</p></noscript>'.format(onlineStatusText, middleStatusText), itemString1)
                                    activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                else:
                                    leaveStatusText = textDict['leaveStatus']
                                    toUserMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "leave">{1}</p></noscript>'.format(onlineStatusText, leaveStatusText), itemString1)
                                    activeStatusDict = None
                        else:
                            activeStatusDict = None
                        onlineStatusDict = {toUserId: 'online'}
                    else:
                        iffyTimeStr = caches['iffyUsersOnlineStatus'].get(toUserId, None)
                        if iffyTimeStr is not None:
                            iffyStatusText = textDict['uknownStatus']
                            if viewActiveStatus:
                                if caches['chatStatusActive'].get(toUserId, None) is not None:
                                    if activeStatusText is None:
                                        activeStatusText = textDict['activeStatus']
                                    toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p><br><p class = "active">{1}</p></noscript>'.format(iffyStatusText, activeStatusText), itemString1)
                                    activeStatusDict = {toUserId: 'stateActive'}
                                else:
                                    middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                    if middleDataTuple is not None:
                                        middleStatusText = textDict['middleStatus']
                                        toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p><br><p class = "middle">{1}</p></noscript>'.format(iffyStatusText, middleStatusText), itemString1)
                                        activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                    else:
                                        leaveStatusText = textDict['leaveStatus']
                                        toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p><br><p class = "leave">{1}</p></noscript>'.format(iffyStatusText, leaveStatusText), itemString1)
                                        activeStatusDict = None
                            else:
                                toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText), itemString1)
                                activeStatusDict = None
                            toUserIffy = iffyStatusCache.get(toUserId, None)
                            if toUserIffy is not None:
                                onlineStatusDict = {toUserId: {'iffy': toUserIffy}}
                        else:
                            onlineStatusDict = None
                            activeStatusDict = None
                            toUserMainThumb = itemTuple
                else:
                    onlineStatusDict = None
                    if viewActiveStatus:
                        if caches['chatStatusActive'].get(toUserId, None) is not None:
                            if activeStatusText is None:
                                activeStatusText = textDict['activeStatus']
                            toUserMainThumb = (itemString0, '<noscript><p class = "active">{0}</p></noscript>'.format(activeStatusText), itemString1)
                            activeStatusDict = {toUserId: 'stateActive'}
                        else:
                            middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                            if middleDataTuple is not None:
                                middleStatusText = textDict['middleStatus']
                                toUserMainThumb = (itemString0, '<noscript><p class = "middle">{0}</p></noscript>'.format(middleStatusText), itemString1)
                                activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                            else:
                                toUserMainThumb = itemTuple
                                activeStatusDict = None
                    else:
                        toUserMainThumb = itemTuple
                        activeStatusDict = None
            else:
                toUserMainThumb = toUserMainThumbsDict.get(myId, None)
                if toUserMainThumb is None:
                    toUserMainThumbChanged = True
                    if viewMainInfo:
                        if toUserFirstName is None:
                            toUserFirstName = toUserInstance.first_name or None
                        if toUserLastName is None:
                            toUserLastName = toUserInstance.last_name or None
                        if toUserFirstName and toUserLastName:
                            name = '{0} {1}'.format(toUserFirstName, toUserLastName)
                            smallName = toUserInstance.username
                        else:
                            if toUserFirstName:
                                name = toUserFirstName
                                smallName = toUserInstance.username
                            else:
                                name = toUserInstance.username
                                smallName = None
                    else:
                        name = toUserInstance.username
                        smallName = None
                    if viewAvatar:
                        avatar = toUserInstance.avatarThumbMiddleSmall or None
                        if avatar is not None:
                            if smallName:
                                itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(toUserId, toUserProfilePath, avatar.url, name, smallName)
                            else:
                                itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(toUserId, toUserProfilePath, avatar.url, name)
                        else:
                            if smallName:
                                itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                            else:
                                itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                    itemString1 = '</div>'
                    itemTuple = (itemString0, itemString1)
                    toUserMainThumb = {'item': itemTuple, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus}
                    toUserMainThumbsDict.update({myId: toUserMainThumb})
                    if viewOnlineStatus:
                        if caches['onlineUsers'].get(toUserId, None) is not None:
                            if onlineStatusText is None:
                                onlineStatusText = textDict['onlineStatus']
                            if viewActiveStatus:
                                if caches['chatStatusActive'].get(toUserId, None) is not None:
                                    if activeStatusText is None:
                                        activeStatusText = textDict['activeStatus']
                                    toUserMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), itemString1)
                                    activeStatusDict = {toUserId: 'stateActive'}
                                else:
                                    middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                    if middleDataTuple is not None:
                                        middleStatusText = textDict['middleStatus']
                                        toUserMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "middle">{1}</p></noscript>'.format(onlineStatusText, middleStatusText), itemString1)
                                        activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                    else:
                                        leaveStatusText = textDict['leaveStatus']
                                        toUserMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "leave">{1}</p></noscript>'.format(onlineStatusText, leaveStatusText), itemString1)
                                        activeStatusDict = None
                            else:
                                activeStatusDict = None
                            onlineStatusDict = {toUserId: 'online'}
                        else:
                            iffyTimeStr = caches['iffyUsersOnlineStatus'].get(toUserId, None)
                            if iffyTimeStr is not None:
                                iffyStatusText = textDict['uknownStatus']
                                if viewActiveStatus:
                                    if caches['chatStatusActive'].get(toUserId, None) is not None:
                                        if activeStatusText is None:
                                            activeStatusText = textDict['activeStatus']
                                        toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p><br><p class = "active">{1}</p></noscript>'.format(iffyStatusText, activeStatusText), itemString1)
                                        activeStatusDict = {toUserId: 'stateActive'}
                                    else:
                                        middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                        if middleDataTuple is not None:
                                            middleStatusText = textDict['middleStatus']
                                            toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p><br><p class = "middle">{1}</p></noscript>'.format(iffyStatusText, middleStatusText), itemString1)
                                            activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                        else:
                                            leaveStatusText = textDict['leaveStatus']
                                            toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p><br><p class = "leave">{1}</p></noscript>'.format(iffyStatusText, leaveStatusText), itemString1)
                                            activeStatusDict = None
                                else:
                                    toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText), itemString1)
                                    activeStatusDict = None
                                toUserIffy = iffyStatusCache.get(toUserId, None)
                                if toUserIffy is not None:
                                    onlineStatusDict = {toUserId: {'iffy': toUserIffy}}
                            else:
                                onlineStatusDict = None
                                activeStatusDict = None
                                toUserMainThumb = itemTuple
                    else:
                        onlineStatusDict = None
                        if viewActiveStatus:
                            if caches['chatStatusActive'].get(toUserId, None) is not None:
                                if activeStatusText is None:
                                    activeStatusText = textDict['activeStatus']
                                toUserMainThumb = (itemString0, '<noscript><p class = "active">{0}</p></noscript>'.format(activeStatusText), itemString1)
                                activeStatusDict = {toUserId: 'stateActive'}
                            else:
                                middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                if middleDataTuple is not None:
                                    middleStatusText = textDict['middleStatus']
                                    toUserMainThumb = (itemString0, '<noscript><p class = "middle">{0}</p></noscript>'.format(middleStatusText), itemString1)
                                    activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                else:
                                    toUserMainThumb = itemTuple
                                    activeStatusDict = None
                        else:
                            toUserMainThumb = itemTuple
                            activeStatusDict = None
                else:
                    toUserMainThumb = toUserMainThumb['item']
                    if viewOnlineStatus:
                        if caches['onlineUsers'].get(toUserId, None) is not None:
                            if onlineStatusText is None:
                                onlineStatusText = textDict['onlineStatus']
                            if viewActiveStatus:
                                if caches['chatStatusActive'].get(toUserId, None) is not None:
                                    if activeStatusText is None:
                                        activeStatusText = textDict['activeStatus']
                                    toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "online">{0}</p><br><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), toUserMainThumb[1])
                                    activeStatusDict = {toUserId: 'stateActive'}
                                else:
                                    middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                    if middleDataTuple is not None:
                                        middleStatusText = textDict['middleStatus']
                                        toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "online">{0}</p><br><p class = "middle">{1}</p></noscript>'.format(onlineStatusText, middleStatusText), toUserMainThumb[1])
                                        activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                    else:
                                        leaveStatusText = textDict['leaveStatus']
                                        toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "online">{0}</p><br><p class = "leave">{1}</p></noscript>'.format(onlineStatusText, leaveStatusText), toUserMainThumb[1])
                                        activeStatusDict = None
                            else:
                                activeStatusDict = None
                            onlineStatusDict = {toUserId: 'online'}
                        else:
                            iffyTimeStr = caches['iffyUsersOnlineStatus'].get(toUserId, None)
                            if iffyTimeStr is not None:
                                iffyStatusText = textDict['uknownStatus']
                                if viewActiveStatus:
                                    if caches['chatStatusActive'].get(toUserId, None) is not None:
                                        if activeStatusText is None:
                                            activeStatusText = textDict['activeStatus']
                                        toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "iffy">{0}</p><br><p class = "active">{1}</p></noscript>'.format(iffyStatusText, activeStatusText), toUserMainThumb[1])
                                        activeStatusDict = {toUserId: 'stateActive'}
                                    else:
                                        middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                        if middleDataTuple is not None:
                                            middleStatusText = textDict['middleStatus']
                                            toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "iffy">{0}</p><br><p class = "middle">{1}</p></noscript>'.format(iffyStatusText, middleStatusText), toUserMainThumb[1])
                                            activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                        else:
                                            leaveStatusText = textDict['leaveStatus']
                                            toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "iffy">{0}</p><br><p class = "leave">{1}</p></noscript>'.format(iffyStatusText, leaveStatusText), toUserMainThumb[1])
                                            activeStatusDict = None
                                else:
                                    toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText), toUserMainThumb[1])
                                    activeStatusDict = None
                                toUserIffy = iffyStatusCache.get(toUserId, None)
                                if toUserIffy is not None:
                                    onlineStatusDict = {toUserId: {'iffy': toUserIffy}}
                            else:
                                onlineStatusDict = None
                                activeStatusDict = None
                    else:
                        onlineStatusDict = None
                        if viewActiveStatus:
                            if caches['chatStatusActive'].get(toUserId, None) is not None:
                                if activeStatusText is None:
                                    activeStatusText = textDict['activeStatus']
                                toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "active">{0}</p></noscript>'.format(activeStatusText), toUserMainThumb[1])
                                activeStatusDict = {toUserId: 'stateActive'}
                            else:
                                middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                if middleDataTuple is not None:
                                    middleStatusText = textDict['middleStatus']
                                    toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "middle">{0}</p></noscript>'.format(middleStatusText), toUserMainThumb[1])
                                    activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                else:
                                    activeStatusDict = None
                        else:
                            activeStatusDict = None
            if myMainThumbChanged or toUserMainThumbChanged:
                if myMainThumbChanged and toUserMainThumbChanged:
                    mainThumbsCache.set_many({myId: myMainThumbsDict, toUserId: toUserMainThumbsDict})
                else:
                    if myMainThumbChanged:
                        mainThumbsCache.set(myId, myMainThumbsDict)
                    else:
                        mainThumbsCache.set(toUserId, toUserMainThumbsDict)
            messages, images, unread = loadDialog(dialogId, myId, toUserId, currentPage)
            return render(request, "psixiSocial/psixiSocial.html", {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'dialog', 'personal', False),
                'menu': fragments.menuGetter('menuSerialized'),
                'messagesListDict': None,
                'messagesQuerySet': messages,
                'imagesQuerySet': images,
                'unreadMessagesSet': unread,
                'unreadMessagesDictList': None,
                'myListThumb': myListThumbItem,
                'toUserListThumb': toUserListThumbItem,
                'csrf': checkCSRF(request, cookies),
                'time': datetime.now(tz = tz.gettz()).microsecond,
                'imagesToSend': cachedFilesDictDictImages,
                'requestUserId': myId,
                'opponentUserId': toUserId,
                'dialogId': dialogId,
                'myMainThumb': myMainThumb,
                'toUserMainThumb': toUserMainThumb,
                'onlineStatusDict': onlineStatusDict,
                'activeStatusDict': activeStatusDict,
                'form': None,
                'accessToSend': sendMessages
                })
        toUserUsername = toUserInstance.username
        myUserProfileUrl = curUser.profileUrl
        myProfilePath = reverse("profile", kwargs = {'currentUser': myUserProfileUrl})
        toUserProfilePath = reverse("profile", kwargs = {'currentUser': toUserInstance.profileUrl})
        myUsername = curUser.username
        fragments = cachedFragments(myUsername = myUsername, mySlug = myUserProfileUrl,
                                username = toUserUsername, profileUrl = toUserInstance.profileUrl, url = request.path)
        myFirstName = curUser.first_name or None
        myLastName = curUser.last_name or None
        if viewMainInfo:
            toUserFirstName = toUserInstance.first_name or None
            toUserLastName = toUserInstance.last_name or None
        else:
            toUserFirstName = None
            toUserLastName = None
        thpsProfiles = THPSProfile.objects.filter(toUser__in = {myId, toUserId})
        try:
            myTHPSProfile = thpsProfiles.objects.get(toUser__exact = myId)
        except:
            myTHPSProfile = None
        try:
            toUserTHPSProfile = thpsProfiles.objects.get(toUser__exact = toUserId)
        except:
            toUserTHPSProfile = None
        if myTHPSProfile and toUserTHPSProfile:
            myNick = myTHPSProfile.nickName
            toUserNick = toUserThpsProfile.nickName
        else:
            if myTHPSProfile:
                myNick = myTHPSProfile.nickName
                toUserNick = None
            elif toUserTHPSProfile:
                myNick = None
                toUserNick = toUserThpsProfile.nickName
            else:
                myNick = None
                toUserNick = None
        myListThumbChanged, toUserListThumbChanged = False, False
        listThumbsCache = caches['messagesListThumbs']
        listThumbsManyMany = listThumbsCache.get_many({myId, toUserId})
        myListThumbsDict = listThumbsManyMany.get(myId, None)
        toUserListThumbsDict = listThumbsManyMany.get(toUserId, None)
        if myListThumbsDict is None:
            myListThumbChanged = True
            if myFirstName is not None and myLastName is not None:
                name = '{0} {1}'.format(myFirstName, myLastName)
                smallName = curUser.username
            else:
                if myFirstName is not None:
                    name = myFirstName
                    smallName = curUser.username
                else:
                    name = curUser.username
                    smallName = None
            avatar = curUser.avatarThumbSmaller or None
            if avatar is not None:
                if smallName is not None:
                    myListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><p><a href = "{1}">{3}</a></p><small><a href = "{1}">{4}</a></small></div>'.format(myId, myProfilePath, avatar.url, name, smallName)
                else:
                    myListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><p><a href = "{1}">{3}</a></p></div>'.format(myId, myProfilePath, avatar.url, name)
            else:
                if smallName is not None:
                    myListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><p><a href = "{3}">{1}</a></p><small><a href = "{3}">{2}</a></small></div>'.format(myId, name, smallName, myProfilePath)
                else:
                    myListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><p><a href = "{2}">{1}</a></p></div>'.format(myId, name, myProfilePath)
            myListThumbsDict = {myId: myListThumbItem}
        else:
            myListThumbItemDict = myListThumbsDict.get(myId, None)
            if myListThumbItemDict is None:
                myListThumbChanged = True
                if myFirstName and myLastName:
                    name = '{0} {1}'.format(myFirstName, myLastName)
                    smallName = curUser.username
                else:
                    if myFirstName:
                        name = myFirstName
                        smallName = curUser.username
                    else:
                        name = curUser.username
                        smallName = None
                avatar = curUser.avatarThumbSmaller or None
                if avatar is not None:
                    if smallName:
                        myListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><p><a href = "{1}">{3}</a></p><small><a href = "{1}">{4}</a></small></div>'.format(myId, myProfilePath, avatar.url, name, smallName)
                    else:
                        myListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><p><a href = "{1}">{3}</a></p></div>'.format(myId, myProfilePath, avatar.url, name)
                else:
                    if smallName:
                        myListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><p><a href = "{3}">{1}</a></p><small><a href = "{3}">{2}</a></small></div>'.format(myId, name, smallName, myProfilePath)
                    else:
                        myListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><p><a href = "{2}">{1}</a></p></div>'.format(myId, name, myProfilePath)
                myListThumbsDict = {myId: myListThumbItem}
            else:
                myListThumbItem = myListThumbItemDict
        if toUserListThumbsDict is None:
            toUserListThumbChanged = True
            if viewMainInfo:
                if toUserFirstName and toUserLastName:
                    name = '{0} {1}'.format(toUserFirstName, toUserLastName)
                    smallName = toUserInstance.username
                else:
                    if toUserFirstName:
                        name = toUserFirstName
                        smallName = toUserInstance.username
                    else:
                        name = toUserInstance.username
                        smallName = None
            else:
                name = toUserInstance.username
                smallName = None
            if viewAvatar:
                avatar = toUserInstance.avatarThumbSmaller or None
                if avatar is not None:
                    if smallName:
                        toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><p><a href = "{1}">{3}</a></p><small><a href = "{1}">{4}</a></small></div>'.format(toUserId, toUserProfilePath, avatar.url, name, smallName)
                    else:
                        toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><p><a href = "{1}">{3}</a></p></div>'.format(toUserId, toUserProfilePath, avatar.url, name)
                else:
                    if smallName:
                        toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><p><a href = "{3}">{1}</a></p><small><a href = "{3}">{2}</a></small></div>'.format(toUserId, name, smallName, toUserProfilePath)
                    else:
                        toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><p><a href = "{2}">{1}</a></p></div>'.format(toUserId, name, toUserProfilePath)
            else:
                if smallName:
                    toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><p><a href = "{3}">{1}</a></p><small><a href = "{3}">{2}</a></small></div>'.format(toUserId, name, smallName, toUserProfilePath)
                else:
                    toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><p><a href = "{2}">{1}</a></p></div>'.format(toUserId, name, toUserProfilePath)
            toUserListThumbsDict = {myId: toUserListThumbItem}
        else:
            toUserListThumbItemDict = toUserListThumbsDict.get(myId, None)
            if toUserListThumbItemDict is None:
                toUserListThumbChanged = True
                if viewMainInfo:
                    if toUserFirstName and toUserLastName:
                        name = '{0} {1}'.format(toUserFirstName, toUserLastName)
                        smallName = toUserInstance.username
                    else:
                        if toUserFirstName:
                            name = toUserFirstName
                            smallName = toUserInstance.username
                        else:
                            name = toUserInstance.username
                            smallName = None
                else:
                    name = toUserInstance.username
                    smallName = None
                if viewAvatar:
                    avatar = toUserInstance.avatarThumbSmaller or None
                    if avatar is not None:
                        if smallName:
                            toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><p><a href = "{1}">{3}</a></p><small><a href = "{1}">{4}</a></small></div>'.format(toUserId, toUserProfilePath, avatar.url, name, smallName)
                        else:
                            toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><p><a href = "{1}">{3}</a></p></div>'.format(toUserId, toUserProfilePath, avatar.url, name)
                    else:
                        if smallName:
                            toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><p><a href = "{3}">{1}</a></p><small><a href = "{3}">{2}</a></small></div>'.format(toUserId, name, smallName, toUserProfilePath)
                        else:
                            toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><p><a href = "{2}">{1}</a></p></div>'.format(toUserId, name, toUserProfilePath)
                else:
                    if smallName:
                        toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><p><a href = "{3}">{1}</a></p><small><a href = "{3}">{2}</a></small></div>'.format(toUserId, name, smallName, toUserProfilePath)
                    else:
                        toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><p><a href = "{2}">{1}</a></p></div>'.format(toUserId, name, toUserProfilePath)
                toUserListThumbsDict = {myId: toUserListThumbItem}
            else:
                toUserListThumbItem = toUserListThumbItemDict
        if myListThumbChanged and toUserListThumbChanged:
            listThumbsCache.set_many({myId: myListThumbsDict, toUserId: toUserListThumbsDict})
        else:
            if myListThumbChanged:
                listThumbsCache.set(myId, myListThumbsDict)
            elif toUserListThumbChanged:
                listThumbsCache.set(toUserId, toUserListThumbsDict)
        if request.is_ajax():
            if caches['onlineUsers'].get(toUserId, None) is not None:
                onlineStatusDict = {toUserId: 'online'}
                if caches['chatStatusActive'].get(toUserId, None) is not None:
                    activeStatusDict = {toUserId: 'stateActive'}
                else:
                    middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                    if middleDataTuple is not None:
                        activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                    else:
                        activeStatusDict = None
            else:
                iffyTimeStr = caches['iffyUsersOnlineStatus'].get(toUserId, None)
                if iffyTimeStr is not None:
                    onlineStatusDict = {toUserId: {'iffy': iffyTimeStr}}
                    if caches['chatStatusActive'].get(toUserId, None) is not None:
                        activeStatusDict = {toUserId: 'stateActive'}
                    else:
                        middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                        if middleDataTuple is not None:
                            activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                        else:
                            activeStatusDict = None
                else:
                    onlineStatusDict = None
                    activeStatusDict = None
            myMainThumbChanged, toUserMainThumbChanged = False, False
            mainThumbsCache = caches['dialogThumbs']
            mainThumbsManyMany = mainThumbsCache.get_many({myId, toUserId})
            myMainThumbsDict = mainThumbsManyMany.get(myId, None)
            toUserMainThumbsDict = mainThumbsManyMany.get(toUserId, None)
            if myMainThumbsDict is None:
                myMainThumbChanged = True
                if myFirstName is None:
                    myFirstName = curUser.first_name or None
                if myLastName is None:
                    myLastName = curUser.last_name or None
                if myFirstName is not None and myLastName is not None:
                    name = '{0} {1}'.format(myFirstName, myLastName)
                    smallName = curUser.username
                else:
                    if myFirstName:
                        name = myFirstName
                        smallName = curUser.username
                    else:
                        name = curUser.username
                        smallName = None
                avatar = curUser.avatarThumbMiddleSmall or None
                if avatar is not None:
                    if smallName:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(myId, myProfilePath, avatar.url, name, smallName)
                    else:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(myId, myProfilePath, avatar.url, name)
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(myId, name, smallName, myProfilePath)
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(myId, name, myProfilePath)
                myMainThumb = (itemString0, '</div>')
                myMainThumbsDict = {myId: {'item': myMainThumb, 'showOnlineStatus': True, 'showActiveStatus': True}}
            else:
                myMainThumb = myMainThumbsDict.get(myId, None)
                if myMainThumb is None:
                    myMainThumbChanged = True
                    if myFirstName is None:
                        myFirstName = curUser.first_name or None
                    if myLastName is None:
                        myLastName = curUser.last_name or None
                    if myFirstName and myLastName:
                        name = '{0} {1}'.format(myFirstName, myLastName)
                        smallName = curUser.username
                    else:
                        if myFirstName:
                            name = myFirstName
                            smallName = curUser.username
                        else:
                            name = curUser.username
                            smallName = None
                    avatar = curUser.avatarThumbMiddleSmall or None
                    if avatar is not None:
                        if smallName:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(myId, myProfilePath, avatar.url, name, smallName)
                        else:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(myId, myProfilePath, avatar.url, name)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(myId, name, smallName, myProfilePath)
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(myId, name, myProfilePath)
                    myMainThumb = (itemString0, '</div>')
                    myMainThumbsDict.update({myId: {'item': myMainThumb, 'showOnlineStatus': True, 'showActiveStatus': True}})
                else:
                    myMainThumb = myMainThumb["item"]
            if toUserMainThumbsDict is None:
                toUserMainThumbChanged = True
                if viewMainInfo:
                    if toUserFirstName is None:
                        toUserFirstName = toUserInstance.first_name or None
                    if toUserLastName is None:
                        toUserLastName = toUserInstance.last_name or None
                    if toUserFirstName and toUserLastName:
                        name = '{0} {1}'.format(toUserFirstName, toUserLastName)
                        smallName = toUserInstance.username
                    else:
                        if toUserFirstName:
                            name = toUserFirstName
                            smallName = toUserInstance.username
                        else:
                            name = toUserInstance.username
                            smallName = None
                else:
                    name = toUserInstance.username
                    smallName = None
                if viewAvatar:
                    avatar = toUserInstance.avatarThumbMiddleSmall or None
                    if avatar is not None:
                        if smallName:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(toUserId, toUserProfilePath, avatar.url, name, smallName)
                        else:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(toUserId, toUserProfilePath, avatar.url, name)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                toUserMainThumb = (itemString0, '</div>')
                toUserMainThumbsDict = {myId: {'item': toUserMainThumb, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus}}
            else:
                toUserMainThumb = toUserMainThumbsDict.get(myId, None)
                if toUserMainThumb is None:
                    toUserMainThumbChanged = True
                    if viewMainInfo:
                        if toUserFirstName is None:
                            toUserFirstName = toUserInstance.first_name or None
                        if toUserLastName is None:
                            toUserLastName = toUserInstance.last_name or None
                        if toUserFirstName and toUserLastName:
                            name = '{0} {1}'.format(toUserFirstName, toUserLastName)
                            smallName = toUserInstance.username
                        else:
                            if toUserFirstName:
                                name = toUserFirstName
                                smallName = toUserInstance.username
                            else:
                                name = toUserInstance.username
                                smallName = None
                    else:
                        name = toUserInstance.username
                        smallName = None
                    if viewAvatar:
                        avatar = toUserInstance.avatarThumbMiddleSmall or None
                        if avatar is not None:
                            if smallName:
                                itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(toUserId, toUserProfilePath, avatar.url, name, smallName)
                            else:
                                itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(toUserId, toUserProfilePath, avatar.url, name)
                        else:
                            if smallName:
                                itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                            else:
                                itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                    toUserMainThumb = (itemString0, '</div>')
                    toUserMainThumbsDict.update({myId: {'item': toUserMainThumb, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus}})
                else:
                    toUserMainThumb = toUserMainThumb["item"]
            if myMainThumbChanged and toUserMainThumbChanged:
                mainThumbsCache.set_many({myId: myMainThumbsDict, toUserId: toUserMainThumbsDict})
            else:
                if myMainThumbChanged:
                    mainThumbsCache.set(myId, myMainThumbsDict)
                elif toUserMainThumbChanged:
                    mainThumbsCache.set(toUserId, toUserMainThumbsDict)
            if 'list' in reqGet:
                return JsonResponse({"messagesItems": None})
            if "global" in reqGet:
                return JsonResponse({"newPage": json.dumps({
                    'newTemplate': rts("psixiSocial/psixiSocial.html", {
                        'parentTemplate': 'base/classicPage/emptyParent.html',
                        'content': ('psixiSocial', 'dialog', 'personal', True),
                        'menu': fragments.menuGetter('menuSerialized'),
                        'messagesListDict': None,
                        'unreadMessagesDictList': None,
                        'myListThumb': myListThumbItem,
                        'toUserListThumb': toUserListThumbItem,
                        'csrf': '',
                        'time': datetime.now(tz = tz.gettz()).microsecond,
                        'imagesToSend': None,
                        'requestUserId': myId,
                        'opponentUserId': toUserId,
                        'myMainThumb': myMainThumb,
                        'toUserMainThumb': toUserMainThumb,
                        'onlineStatusDict': onlineStatusDict,
                        'activeStatusDict': activeStatusDict,
                        'form': None,
                        'accessToSend': sendMessages
                        }),
                    'url': request.path
                    })})
            return JsonResponse({"newPage": json.dumps({
                'templateHead': rts("psixiSocial/head/dialogs/dialogWrite.html"),
                'templateBody': rts("psixiSocial/body/dialogs/dialogWritePersonalXHR.html", {
                    'csrf': '',
                    'imagesToSend': None,
                    'myMainThumb': myMainThumb,
                    'toUserMainThumb': toUserMainThumb,
                    'myListThumb': myListThumbItem,
                    'toUserListThumb': toUserListThumbItem,
                    'form': None,
                    'accessToSend': sendMessages
                    }),
                'templateScripts': rts("psixiSocial/scripts/dialogs/dialogWritePersonal.html", {
                    'messagesListDict': None,
                    'unreadMessagesDictList': None,
                    'myListThumb': myListThumbItem,
                    'toUserListThumb': toUserListThumbItem,
                    'time': datetime.now(tz = tz.gettz()).microsecond,
                    'requestUserId': myId,
                    'opponentUserId': toUserId,
                    'onlineStatusDict': onlineStatusDict,
                    'activeStatusDict': activeStatusDict
                    }),
                'menu': fragments.menuGetter('menu'),
                'url': request.path,
                'titl': ''
                })})
        onlineStatusText = None
        activeStatusText = None
        myMainThumbChanged, toUserMainThumbChanged = False, False
        mainThumbsCache = caches['dialogThumbs']
        mainThumbsManyMany = mainThumbsCache.get_many({myId, toUserId})
        myMainThumbsDict = mainThumbsManyMany.get(myId, None)
        toUserMainThumbsDict = mainThumbsManyMany.get(toUserId, None)
        if myMainThumbsDict is None:
            myMainThumbChanged = True
            if myFirstName is None:
                myFirstName = curUser.first_name or None
            if myLastName is None:
                myLastName = curUser.last_name or None
            if myFirstName is not None and myLastName is not None:
                name = '{0} {1}'.format(myFirstName, myLastName)
                smallName = curUser.username
            else:
                if myFirstName:
                    name = myFirstName
                    smallName = curUser.username
                else:
                    name = curUser.username
                    smallName = None
            avatar = curUser.avatarThumbMiddleSmall or None
            if avatar is not None:
                if smallName:
                    itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(myId, myProfilePath, avatar.url, name, smallName)
                else:
                    itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(myId, myProfilePath, avatar.url, name)
            else:
                if smallName:
                    itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(myId, name, smallName, myProfilePath)
                else:
                    itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(myId, name, myProfilePath)
            itemString1 = '</div>'
            myMainThumbsDict = {myId: {'item': (itemString0, itemString1), 'showOnlineStatus': True, 'showActiveStatus': True}}
            textDict = usersListText(lang = 'ru').getDict()
            onlineStatusText = textDict['onlineStatus']
            activeStatusText = textDict['activeStatus']
            myMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), itemString1)
        else:
            myMainThumb = myMainThumbsDict.get(myId, None)
            if myMainThumb is None:
                myMainThumbChanged = True
                if myFirstName is None:
                    myFirstName = curUser.first_name or None
                if myLastName is None:
                    myLastName = curUser.last_name or None
                if myFirstName and myLastName:
                    name = '{0} {1}'.format(myFirstName, myLastName)
                    smallName = curUser.username
                else:
                    if myFirstName:
                        name = myFirstName
                        smallName = curUser.username
                    else:
                        name = curUser.username
                        smallName = None
                avatar = curUser.avatarThumbMiddleSmall or None
                if avatar is not None:
                    if smallName:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(myId, myProfilePath, avatar.url, name, smallName)
                    else:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(myId, myProfilePath, avatar.url, name)
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(myId, name, smallName, myProfilePath)
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(myId, name, myProfilePath)
                itemString1 = '</div>'
                myMainThumbsDict.update({myId: {'item': (itemString0, itemString1), 'showOnlineStatus': True, 'showActiveStatus': True}})
                textDict = usersListText(lang = 'ru').getDict()
                onlineStatusText = textDict['onlineStatus']
                activeStatusText = textDict['activeStatus']
                myMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), itemString1)
            else:
                textDict = usersListText(lang = 'ru').getDict()
                onlineStatusText = textDict['onlineStatus']
                activeStatusText = textDict['activeStatus']
                myMainThumb = myMainThumb['item']
                myMainThumb = (myMainThumb[0], '<noscript><p class = "online">{0}</p><br><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), myMainThumb[1])
        if toUserMainThumbsDict is None:
            toUserMainThumbChanged = True
            if viewMainInfo:
                if toUserFirstName is None:
                    toUserFirstName = toUserInstance.first_name or None
                if toUserLastName is None:
                    toUserLastName = toUserInstance.last_name or None
                if toUserFirstName and toUserLastName:
                    name = '{0} {1}'.format(toUserFirstName, toUserLastName)
                    smallName = toUserInstance.username
                else:
                    if toUserFirstName:
                        name = toUserFirstName
                        smallName = toUserInstance.username
                    else:
                        name = toUserInstance.username
                        smallName = None
            else:
                name = toUserInstance.username
                smallName = None
            if viewAvatar:
                avatar = toUserInstance.avatarThumbMiddleSmall or None
                if avatar is not None:
                    if smallName:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(toUserId, toUserProfilePath, avatar.url, name, smallName)
                    else:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(toUserId, toUserProfilePath, avatar.url, name)
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
            else:
                if smallName:
                    itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                else:
                    itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
            itemString1 = '</div>'
            itemTuple = (itemString0, itemString1)
            toUserMainThumb = {'item': itemTuple, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus}
            toUserMainThumbsDict = {myId: toUserMainThumb}
            if viewOnlineStatus:
                if caches['onlineUsers'].get(toUserId, None) is not None:
                    if onlineStatusText is None:
                        onlineStatusText = textDict['onlineStatus']
                    if viewActiveStatus:
                        if caches['chatStatusActive'].get(toUserId, None) is not None:
                            if activeStatusText is None:
                                activeStatusText = textDict['activeStatus']
                            toUserMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), itemString1)
                            activeStatusDict = {toUserId: 'stateActive'}
                        else:
                            middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                            if middleDataTuple is not None:
                                middleStatusText = textDict['middleStatus']
                                toUserMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "middle">{1}</p></noscript>'.format(onlineStatusText, middleStatusText), itemString1)
                                activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                            else:
                                leaveStatusText = textDict['leaveStatus']
                                toUserMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "leave">{1}</p></noscript>'.format(onlineStatusText, leaveStatusText), itemString1)
                                activeStatusDict = None
                    else:
                        activeStatusDict = None
                    onlineStatusDict = {toUserId: 'online'}
                else:
                    iffyTimeStr = caches['iffyUsersOnlineStatus'].get(toUserId, None)
                    if iffyTimeStr is not None:
                        iffyStatusText = textDict['uknownStatus']
                        if viewActiveStatus:
                            if caches['chatStatusActive'].get(toUserId, None) is not None:
                                if activeStatusText is None:
                                    activeStatusText = textDict['activeStatus']
                                toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p><br><p class = "active">{1}</p></noscript>'.format(iffyStatusText, activeStatusText), itemString1)
                                activeStatusDict = {toUserId: 'stateActive'}
                            else:
                                middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                if middleDataTuple is not None:
                                    middleStatusText = textDict['middleStatus']
                                    toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p><br><p class = "middle">{1}</p></noscript>'.format(iffyStatusText, middleStatusText), itemString1)
                                    activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                else:
                                    leaveStatusText = textDict['leaveStatus']
                                    toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p><br><p class = "leave">{1}</p></noscript>'.format(iffyStatusText, leaveStatusText), itemString1)
                                    activeStatusDict = None
                        else:
                            toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText), itemString1)
                            activeStatusDict = None
                        toUserIffy = iffyStatusCache.get(toUserId, None)
                        if toUserIffy is not None:
                            onlineStatusDict = {toUserId: {'iffy': toUserIffy}}
                    else:
                        onlineStatusDict = None
                        activeStatusDict = None
                        toUserMainThumb = itemTuple
            else:
                onlineStatusDict = None
                if viewActiveStatus:
                    if caches['chatStatusActive'].get(toUserId, None) is not None:
                        if activeStatusText is None:
                            activeStatusText = textDict['activeStatus']
                        toUserMainThumb = (itemString0, '<noscript><p class = "active">{0}</p></noscript>'.format(activeStatusText), itemString1)
                        activeStatusDict = {toUserId: 'stateActive'}
                    else:
                        middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                        if middleDataTuple is not None:
                            middleStatusText = textDict['middleStatus']
                            toUserMainThumb = (itemString0, '<noscript><p class = "middle">{0}</p></noscript>'.format(middleStatusText), itemString1)
                            activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                        else:
                            toUserMainThumb = itemTuple
                            activeStatusDict = None
                else:
                    toUserMainThumb = itemTuple
                    activeStatusDict = None
        else:
            toUserMainThumb = toUserMainThumbsDict.get(myId, None)
            if toUserMainThumb is None:
                toUserMainThumbChanged = True
                if viewMainInfo:
                    if toUserFirstName is None:
                        toUserFirstName = toUserInstance.first_name or None
                    if toUserLastName is None:
                        toUserLastName = toUserInstance.last_name or None
                    if toUserFirstName and toUserLastName:
                        name = '{0} {1}'.format(toUserFirstName, toUserLastName)
                        smallName = toUserInstance.username
                    else:
                        if toUserFirstName:
                            name = toUserFirstName
                            smallName = toUserInstance.username
                        else:
                            name = toUserInstance.username
                            smallName = None
                else:
                    name = toUserInstance.username
                    smallName = None
                if viewAvatar:
                    avatar = toUserInstance.avatarThumbMiddleSmall or None
                    if avatar is not None:
                        if smallName:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(toUserId, toUserProfilePath, avatar.url, name, smallName)
                        else:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(toUserId, toUserProfilePath, avatar.url, name)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                itemString1 = '</div>'
                itemTuple = (itemString0, itemString1)
                toUserMainThumb = {'item': itemTuple, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus}
                toUserMainThumbsDict.update({myId: toUserMainThumb})
                if viewOnlineStatus:
                    if caches['onlineUsers'].get(toUserId, None) is not None:
                        if onlineStatusText is None:
                            onlineStatusText = textDict['onlineStatus']
                        if viewActiveStatus:
                            if caches['chatStatusActive'].get(toUserId, None) is not None:
                                if activeStatusText is None:
                                    activeStatusText = textDict['activeStatus']
                                toUserMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), itemString1)
                                activeStatusDict = {toUserId: 'stateActive'}
                            else:
                                middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                if middleDataTuple is not None:
                                    middleStatusText = textDict['middleStatus']
                                    toUserMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "middle">{1}</p></noscript>'.format(onlineStatusText, middleStatusText), itemString1)
                                    activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                else:
                                    leaveStatusText = textDict['leaveStatus']
                                    toUserMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "leave">{1}</p></noscript>'.format(onlineStatusText, leaveStatusText), itemString1)
                                    activeStatusDict = None
                        else:
                            activeStatusDict = None
                        onlineStatusDict = {toUserId: 'online'}
                    else:
                        iffyTimeStr = caches['iffyUsersOnlineStatus'].get(toUserId, None)
                        if iffyTimeStr is not None:
                            iffyStatusText = textDict['uknownStatus']
                            if viewActiveStatus:
                                if caches['chatStatusActive'].get(toUserId, None) is not None:
                                    if activeStatusText is None:
                                        activeStatusText = textDict['activeStatus']
                                    toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p><br><p class = "active">{1}</p></noscript>'.format(iffyStatusText, activeStatusText), itemString1)
                                    activeStatusDict = {toUserId: 'stateActive'}
                                else:
                                    middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                    if middleDataTuple is not None:
                                        middleStatusText = textDict['middleStatus']
                                        toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p><br><p class = "middle">{1}</p></noscript>'.format(iffyStatusText, middleStatusText), itemString1)
                                        activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                    else:
                                        leaveStatusText = textDict['leaveStatus']
                                        toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p><br><p class = "leave">{1}</p></noscript>'.format(iffyStatusText, leaveStatusText), itemString1)
                                        activeStatusDict = None
                            else:
                                toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText), itemString1)
                                activeStatusDict = None
                            toUserIffy = iffyStatusCache.get(toUserId, None)
                            if toUserIffy is not None:
                                onlineStatusDict = {toUserId: {'iffy': toUserIffy}}
                        else:
                            onlineStatusDict = None
                            activeStatusDict = None
                            toUserMainThumb = itemTuple
                else:
                    onlineStatusDict = None
                    if viewActiveStatus:
                        if caches['chatStatusActive'].get(toUserId, None) is not None:
                            if activeStatusText is None:
                                activeStatusText = textDict['activeStatus']
                            toUserMainThumb = (itemString0, '<noscript><p class = "active">{0}</p></noscript>'.format(activeStatusText), itemString1)
                            activeStatusDict = {toUserId: 'stateActive'}
                        else:
                            middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                            if middleDataTuple is not None:
                                middleStatusText = textDict['middleStatus']
                                toUserMainThumb = (itemString0, '<noscript><p class = "middle">{0}</p></noscript>'.format(middleStatusText), itemString1)
                                activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                            else:
                                toUserMainThumb = itemTuple
                                activeStatusDict = None
                    else:
                        toUserMainThumb = itemTuple
                        activeStatusDict = None
            else:
                toUserMainThumb = toUserMainThumb['item']
                if viewOnlineStatus:
                    if caches['onlineUsers'].get(toUserId, None) is not None:
                        if onlineStatusText is None:
                            onlineStatusText = textDict['onlineStatus']
                        if viewActiveStatus:
                            if caches['chatStatusActive'].get(toUserId, None) is not None:
                                if activeStatusText is None:
                                    activeStatusText = textDict['activeStatus']
                                toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "online">{0}</p><br><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), toUserMainThumb[1])
                                activeStatusDict = {toUserId: 'stateActive'}
                            else:
                                middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                if middleDataTuple is not None:
                                    middleStatusText = textDict['middleStatus']
                                    toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "online">{0}</p><br><p class = "middle">{1}</p></noscript>'.format(onlineStatusText, middleStatusText), toUserMainThumb[1])
                                    activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                else:
                                    leaveStatusText = textDict['leaveStatus']
                                    toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "online">{0}</p><br><p class = "leave">{1}</p></noscript>'.format(onlineStatusText, leaveStatusText), toUserMainThumb[1])
                                    activeStatusDict = None
                        else:
                            activeStatusDict = None
                        onlineStatusDict = {toUserId: 'online'}
                    else:
                        iffyTimeStr = caches['iffyUsersOnlineStatus'].get(toUserId, None)
                        if iffyTimeStr is not None:
                            iffyStatusText = textDict['uknownStatus']
                            if viewActiveStatus:
                                if caches['chatStatusActive'].get(toUserId, None) is not None:
                                    if activeStatusText is None:
                                        activeStatusText = textDict['activeStatus']
                                    toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "iffy">{0}</p><br><p class = "active">{1}</p></noscript>'.format(iffyStatusText, activeStatusText), toUserMainThumb[1])
                                    activeStatusDict = {toUserId: 'stateActive'}
                                else:
                                    middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                    if middleDataTuple is not None:
                                        middleStatusText = textDict['middleStatus']
                                        toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "iffy">{0}</p><br><p class = "middle">{1}</p></noscript>'.format(iffyStatusText, middleStatusText), toUserMainThumb[1])
                                        activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                    else:
                                        leaveStatusText = textDict['leaveStatus']
                                        toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "iffy">{0}</p><br><p class = "leave">{1}</p></noscript>'.format(iffyStatusText, leaveStatusText), toUserMainThumb[1])
                                        activeStatusDict = None
                            else:
                                toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText), toUserMainThumb[1])
                                activeStatusDict = None
                            toUserIffy = iffyStatusCache.get(toUserId, None)
                            if toUserIffy is not None:
                                onlineStatusDict = {toUserId: {'iffy': toUserIffy}}
                        else:
                            onlineStatusDict = None
                            activeStatusDict = None
                else:
                    onlineStatusDict = None
                    if viewActiveStatus:
                        if caches['chatStatusActive'].get(toUserId, None) is not None:
                            if activeStatusText is None:
                                activeStatusText = textDict['activeStatus']
                            toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "active">{0}</p></noscript>'.format(activeStatusText), toUserMainThumb[1])
                            activeStatusDict = {toUserId: 'stateActive'}
                        else:
                            middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                            if middleDataTuple is not None:
                                middleStatusText = textDict['middleStatus']
                                toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "middle">{0}</p></noscript>'.format(middleStatusText), toUserMainThumb[1])
                                activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                            else:
                                activeStatusDict = None
                    else:
                        activeStatusDict = None
        if myMainThumbChanged and toUserMainThumbChanged:
            mainThumbsCache.set_many({myId: myMainThumbsDict, toUserId: toUserMainThumbsDict})
        else:
            if myMainThumbChanged:
                mainThumbsCache.set(myId, myMainThumbsDict)
            elif toUserMainThumbChanged:
                mainThumbsCache.set(toUserId, toUserMainThumbsDict)
        return render(request, "psixiSocial/psixiSocial.html", {
            'parentTemplate': 'base/classicPage/base.html',
            'content': ('psixiSocial', 'dialog', 'personal', False),
            'menu': fragments.menuGetter('menuSerialized'),
            'messagesListDict': None,
            'messagesQuerySet': None,
            'imagesQuerySet': None,
            'unreadMessagesSet': None,
            'unreadMessagesDictList': None,
            'myListThumb': myListThumbItem,
            'toUserListThumb': toUserListThumbItem,
            'csrf': checkCSRF(request, request.COOKIES),
            'time': datetime.now(tz = tz.gettz()).microsecond,
            'imagesToSend': None,
            'requestUserId': myId,
            'opponentUserId': toUserId,
            'myMainThumb': myMainThumb,
            'toUserMainThumb': toUserMainThumb,
            'onlineStatusDict': onlineStatusDict,
            'activeStatusDict': activeStatusDict,
            'form': None,
            'accessToSend': sendMessages
            })
    elif method == "POST":
        curUser = request.user
        myId = str(curUser.id)
        try:
            toUserInstance = mainUserProfile.objects.get(profileUrl = toUser)
        except mainUserProfile.DoesNotExist:
            pass
        toUserId = str(toUserInstance.id)
        privacyCache = caches['privacyCache']
        privacyDictDict = privacyCache.get_many({toUserId, myId}, None)
        privacyDict = privacyDictDict.get(toUserId, None)
        friendShipStatus = None
        privacyDictHasChanges = False
        privacyDictMyHasChanges = False
        friendsIds = None
        if privacyDict is None:
            privacySettings = userProfilePrivacy.objects.get(user__exact = toUserInstance)
            friendShipCache = caches['friendShipStatusCache']
            friendShipStatusDict = friendShipCache.get(myId, None)
            if friendShipStatusDict is None:
                friendsIdsCache = caches['friendsIdCache']
                friendsIds = friendsIdsCache.get(myId, None)
                if friendsIds is None:
                    friendsCache = caches['friendsCache']
                    cachedFriends = friendsCache.get(myId, None)
                    if cachedFriends is None:
                        cachedFriends = curUser.friends.all()
                        if cachedFriends.count():
                            friendsCache.set(myId, cachedFriends)
                            friendsIdsQuerySet = cachedFriends.values("id")
                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                        else:
                            friendsCache.set(myId, False)
                            friendsIds = set()
                    else:
                        if cachedFriends is not False:
                            friendsIdsQuerySet = cachedFriends.values("id")
                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                        else:
                            friendsIds = set()
                    friendsIdsCache.set(myId, friendsIds)
                if toUserId in friendsIds:
                    privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = True, isAuthenticated = True)
                    friendShipCache.set(myId, {toUserId: True})
                else:
                    friendShipRequestsCache = caches['friendshipRequests']
                    friendShipRequests = friendShipRequestsCache.get(myId, None)
                    friendShipStatus = False
                    privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = False, isAuthenticated = True)
                    if friendShipRequests is not None:
                        for friendShipRequest in friendShipRequests:
                            if friendShipRequest[0] == toUserId:
                                friendShipStatus = "request"
                                privacyChecker.isFriend = True
                                break
                        if friendShipStatus is False:
                            friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                            if friendShipRequests is not None:
                                for friendShipRequest in friendShipRequests:
                                    if friendShipRequest[0] == myId:
                                        friendShipStatus = "myRequest"
                                        break
                    friendShipCache.set(myId, {toUserId: friendShipStatus})
            else:
                friendShipStatus = friendShipStatusDict.get(toUserId, None)
                if friendShipStatus is None:
                    friendsIdsCache = caches['friendsIdCache']
                    friendsIds = friendsIdsCache.get(myId, None)
                    if friendsIds is None:
                        friendsCache = caches['friendsCache']
                        cachedFriends = friendsCache.get(myId, None)
                        if cachedFriends is None:
                            cachedFriends = curUser.friends.all()
                            if cachedFriends.count():
                                friendsCache.set(myId, cachedFriends)
                                friendsIdsQuerySet = cachedFriends.values("id")
                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                            else:
                                friendsCache.set(myId, False)
                                friendsIds = set()
                        else:
                            if cachedFriends is not False:
                                friendsIdsQuerySet = cachedFriends.values("id")
                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                            else:
                                friendsIds = set()
                        friendsIdsCache.set(myId, friendsIds)
                    if toUserId in friendsIds:
                        privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = True, isAuthenticated = True)
                        friendShipStatusDict.update({toUserId: True})
                    else:
                        friendShipRequestsCache = caches['friendshipRequests']
                        friendShipRequests = friendShipRequestsCache.get(myId, None)
                        friendShipStatus = False
                        privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = False, isAuthenticated = True)
                        if friendShipRequests is not None:
                            for friendShipRequest in friendShipRequests:
                                if friendShipRequest[0] == toUserId:
                                    friendShipStatus = "request"
                                    privacyChecker.isFriend = True
                                    break
                            if friendShipStatus is False:
                                friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                if friendShipRequests is not None:
                                    for friendShipRequest in friendShipRequests:
                                        if friendShipRequest[0] == myId:
                                            friendShipStatus = "myRequest"
                                            break
                        friendShipStatusDict.update({toUserId: friendShipStatus})
                    friendShipCache.set(myId, friendShipStatusDict)
                else:
                    privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
            viewProfile = privacyChecker.checkViewProfilePrivacy()
            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
            viewAvatar = privacyChecker.checkAvatarPrivacy()
            sendMessages = privacyChecker.sendPersonalMessages()
            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
            viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
            privacyDictHasChanges = None
            privacyDictDict = {toUserId: {myId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'sendPersonalMessages': sendMessages, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}}
        else:
            userPrivacyDict = privacyDict.get(myId, None)
            if userPrivacyDict is None:
                privacySettings = userProfilePrivacy.objects.get(user__exact = toUserInstance)
                friendShipCache = caches['friendShipStatusCache']
                friendShipStatusDict = friendShipCache.get(myId, None)
                if friendShipStatusDict is None:
                    friendsIdsCache = caches['friendsIdCache']
                    friendsIds = friendsIdsCache.get(myId, None)
                    if friendsIds is None:
                        friendsCache = caches['friendsCache']
                        cachedFriends = friendsCache.get(myId, None)
                        if cachedFriends is None:
                            cachedFriends = curUser.friends.all()
                            if cachedFriends.count():
                                friendsCache.set(myId, cachedFriends)
                                friendsIdsQuerySet = cachedFriends.values("id")
                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                            else:
                                friendsCache.set(myId, False)
                                friendsIds = set()
                        else:
                            if cachedFriends is not False:
                                friendsIdsQuerySet = cachedFriends.values("id")
                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                            else:
                                friendsIds = set()
                        friendsIdsCache.set(myId, friendsIds)
                    if toUserId in friendsIds:
                        privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = True, isAuthenticated = True)
                        friendShipCache.set(myId, {toUserId: True})
                    else:
                        friendShipRequestsCache = caches['friendshipRequests']
                        friendShipRequests = friendShipRequestsCache.get(myId, None)
                        friendShipStatus = False
                        privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = False, isAuthenticated = True)
                        if friendShipRequests is not None:
                            for friendShipRequest in friendShipRequests:
                                if friendShipRequest[0] == toUserId:
                                    friendShipStatus = "request"
                                    privacyChecker.isFriend = True
                                    break
                            if friendShipStatus is False:
                                friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                if friendShipRequests is not None:
                                    for friendShipRequest in friendShipRequests:
                                        if friendShipRequest[0] == myId:
                                            friendShipStatus = "myRequest"
                                            break
                        friendShipCache.set(myId, {toUserId: friendShipStatus})
                else:
                    friendShipStatus = friendShipStatusDict.get(toUserId, None)
                    if friendShipStatus is None:
                        friendsIdsCache = caches['friendsIdCache']
                        friendsIds = friendsIdsCache.get(myId, None)
                        if friendsIds is None:
                            friendsCache = caches['friendsCache']
                            cachedFriends = friendsCache.get(myId, None)
                            if cachedFriends is None:
                                cachedFriends = curUser.friends.all()
                                if cachedFriends.count():
                                    friendsCache.set(myId, cachedFriends)
                                    friendsIdsQuerySet = cachedFriends.values("id")
                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                else:
                                    friendsCache.set(myId, False)
                                    friendsIds = set()
                            else:
                                if cachedFriends is not False:
                                    friendsIdsQuerySet = cachedFriends.values("id")
                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                else:
                                    friendsIds = set()
                            friendsIdsCache.set(myId, friendsIds)
                        if toUserId in friendsIds:
                            privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = True, isAuthenticated = True)
                            friendShipStatusDict.update({toUserId: True})
                        else:
                            friendShipRequestsCache = caches['friendshipRequests']
                            friendShipRequests = friendShipRequestsCache.get(myId, None)
                            friendShipStatus = False
                            privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = False, isAuthenticated = True)
                            if friendShipRequests is not None:
                                for friendShipRequest in friendShipRequests:
                                    if friendShipRequest[0] == toUserId:
                                        friendShipStatus = "request"
                                        privacyChecker.isFriend = True
                                        break
                                if friendShipStatus is False:
                                    friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                    if friendShipRequests is not None:
                                        for friendShipRequest in friendShipRequests:
                                            if friendShipRequest[0] == myId:
                                                friendShipStatus = "myRequest"
                                                break
                            friendShipStatusDict.update({toUserId: friendShipStatus})
                        friendShipCache.set(myId, friendShipStatusDict)
                    else:
                        privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                viewProfile = privacyChecker.checkViewProfilePrivacy()
                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                viewAvatar = privacyChecker.checkAvatarPrivacy()
                sendMessages = privacyChecker.sendPersonalMessages()
                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                privacyDictHasChanges = True
                privacyDict.update({myId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'sendPersonalMessages': sendMessages, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
            else:
                viewProfile = userPrivacyDict.get('viewProfile', None)
                viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                viewAvatar = userPrivacyDict.get('viewAvatar', None)
                sendMessages = userPrivacyDict.get('sendPersonalMessages', None)
                viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                viewActiveStatus = userPrivacyDict.get('viewActiveStsatus', None)
                if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or sendMessages is None or viewOnlineStatus is None or viewActiveStatus is None:
                    privacySettings = userProfilePrivacy.objects.get(user__exact = toUserInstance)
                    friendShipCache = caches['friendShipStatusCache']
                    friendShipStatusDict = friendShipCache.get(myId, None)
                    if friendShipStatusDict is None:
                        friendsIdsCache = caches['friendsIdCache']
                        friendsIds = friendsIdsCache.get(myId, None)
                        if friendsIds is None:
                            friendsCache = caches['friendsCache']
                            cachedFriends = friendsCache.get(myId, None)
                            if cachedFriends is None:
                                cachedFriends = curUser.friends.all()
                                if cachedFriends.count():
                                    friendsCache.set(myId, cachedFriends)
                                    friendsIdsQuerySet = cachedFriends.values("id")
                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                else:
                                    friendsCache.set(myId, False)
                                    friendsIds = set()
                            else:
                                if cachedFriends is not False:
                                    friendsIdsQuerySet = cachedFriends.values("id")
                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                else:
                                    friendsIds = set()
                            friendsIdsCache.set(myId, friendsIds)
                        if toUserId in friendsIds:
                            privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = True, isAuthenticated = True)
                            friendShipCache.set(myId, {toUserId: True})
                        else:
                            friendShipRequestsCache = caches['friendshipRequests']
                            friendShipRequests = friendShipRequestsCache.get(myId, None)
                            friendShipStatus = False
                            privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = False, isAuthenticated = True)
                            if friendShipRequests is not None:
                                for friendShipRequest in friendShipRequests:
                                    if friendShipRequest[0] == toUserId:
                                        friendShipStatus = "request"
                                        privacyChecker.isFriend = True
                                        break
                                if friendShipStatus is False:
                                    friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                    if friendShipRequests is not None:
                                        for friendShipRequest in friendShipRequests:
                                            if friendShipRequest[0] == myId:
                                                friendShipStatus = "myRequest"
                                                break
                            friendShipCache.set(myId, {toUserId: friendShipStatus})
                    else:
                        friendShipStatus = friendShipStatusDict.get(toUserId, None)
                        if friendShipStatus is None:
                            friendsIdsCache = caches['friendsIdCache']
                            friendsIds = friendsIdsCache.get(myId, None)
                            if friendsIds is None:
                                friendsCache = caches['friendsCache']
                                cachedFriends = friendsCache.get(myId, None)
                                if cachedFriends is None:
                                    cachedFriends = curUser.friends.all()
                                    if cachedFriends.count():
                                        friendsCache.set(myId, cachedFriends)
                                        friendsIdsQuerySet = cachedFriends.values("id")
                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    else:
                                        friendsCache.set(myId, False)
                                        friendsIds = set()
                                else:
                                    if cachedFriends is not False:
                                        friendsIdsQuerySet = cachedFriends.values("id")
                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    else:
                                        friendsIds = set()
                                friendsIdsCache.set(myId, friendsIds)
                            if toUserId in friendsIds:
                                privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                friendShipStatusDict.update({toUserId: True})
                            else:
                                friendShipRequestsCache = caches['friendshipRequests']
                                friendShipRequests = friendShipRequestsCache.get(myId, None)
                                friendShipStatus = False
                                privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                if friendShipRequests is not None:
                                    for friendShipRequest in friendShipRequests:
                                        if friendShipRequest[0] == toUserId:
                                            friendShipStatus = "request"
                                            privacyChecker.isFriend = True
                                            break
                                    if friendShipStatus is False:
                                        friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                        if friendShipRequests is not None:
                                            for friendShipRequest in friendShipRequests:
                                                if friendShipRequest[0] == myId:
                                                    friendShipStatus = "myRequest"
                                                    break
                                friendShipStatusDict.update({toUserId: friendShipStatus})
                            friendShipCache.set(myId, friendShipStatusDict)
                        else:
                            privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                    if viewProfile is None:
                        viewProfile = privacyChecker.checkViewProfilePrivacy()
                        userPrivacyDict.update({'viewProfile': viewProfile})
                    if viewMainInfo is None:
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                    if viewTHPSInfo is None:
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                    if viewAvatar is None:
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        userPrivacyDict.update({'viewAvatar': viewAvatar})
                    if sendMessages is None:
                        sendMessages = privacyChecker.sendPersonalMessages()
                        userPrivacyDict.update({'sendPersonalMessages': sendMessages})
                    if viewOnlineStatus is None:
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                    if viewActiveStatus is None:
                        viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                        userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                    privacyDictHasChanges = True
        privacyDictMy = privacyDictDict.get(myId, None)
        if privacyDictMy is None:
            privacySettings = userProfilePrivacy.objects.get(user__exact = curUser)
            if friendShipStatus is None:
                friendShipCache = caches['friendShipStatusCache']
                friendShipStatusDict = friendShipCache.get(myId, None)
                if friendShipStatusDict is None:
                    if friendsIds is None:
                        friendsIdsCache = caches['friendsIdCache']
                        friendsIds = friendsIdsCache.get(myId, None)
                        if friendsIds is None:
                            friendsCache = caches['friendsCache']
                            cachedFriends = friendsCache.get(myId, None)
                            if cachedFriends is None:
                                cachedFriends = curUser.friends.all()
                                if cachedFriends.count():
                                    friendsCache.set(myId, cachedFriends)
                                    friendsIdsQuerySet = cachedFriends.values("id")
                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                else:
                                    friendsCache.set(myId, False)
                                    friendsIds = set()
                            else:
                                if cachedFriends is not False:
                                    friendsIdsQuerySet = cachedFriends.values("id")
                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                else:
                                    friendsIds = set()
                            friendsIdsCache.set(myId, friendsIds)
                    if toUserId in friendsIds:
                        friendShipCache.set(myId, {toUserId: True})
                    else:
                        friendShipRequestsCache = caches['friendshipRequests']
                        friendShipRequests = friendShipRequestsCache.get(myId, None)
                        friendShipStatus = False
                        if friendShipRequests is not None:
                            for friendShipRequest in friendShipRequests:
                                if friendShipRequest[0] == toUserId:
                                    friendShipStatus = "request"
                                    break
                            if friendShipStatus is False:
                                friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                if friendShipRequests is not None:
                                    for friendShipRequest in friendShipRequests:
                                        if friendShipRequest[0] == myId:
                                            friendShipStatus = "myRequest"
                                            break
                        friendShipCache.set(myId, {toUserId: friendShipStatus})
                else:
                    friendShipStatus = friendShipStatusDict.get(toUserId, None)
                    if friendShipStatus is None:
                        if friendsIds is None:
                            friendsIdsCache = caches['friendsIdCache']
                            friendsIds = friendsIdsCache.get(myId, None)
                            if friendsIds is None:
                                friendsCache = caches['friendsCache']
                                cachedFriends = friendsCache.get(myId, None)
                                if cachedFriends is None:
                                    cachedFriends = curUser.friends.all()
                                    if cachedFriends.count():
                                        friendsCache.set(myId, cachedFriends)
                                        friendsIdsQuerySet = cachedFriends.values("id")
                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    else:
                                        friendsCache.set(myId, False)
                                        friendsIds = set()
                                else:
                                    if cachedFriends is not False:
                                        friendsIdsQuerySet = cachedFriends.values("id")
                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    else:
                                        friendsIds = set()
                                friendsIdsCache.set(myId, friendsIds)
                        if toUserId in friendsIds:
                            friendShipStatusDict.update({toUserId: True})
                        else:
                            friendShipRequestsCache = caches['friendshipRequests']
                            friendShipRequests = friendShipRequestsCache.get(myId, None)
                            friendShipStatus = False
                            if friendShipRequests is not None:
                                for friendShipRequest in friendShipRequests:
                                    if friendShipRequest[0] == toUserId:
                                        friendShipStatus = "request"
                                        break
                                if friendShipStatus is False:
                                    friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                    if friendShipRequests is not None:
                                        for friendShipRequest in friendShipRequests:
                                            if friendShipRequest[0] == myId:
                                                friendShipStatus = "myRequest"
                                                break
            privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
            sendMessagesMyself = privacyChecker.sendPersonalMessages()
            privacyDictMyHasChanges = None
            privacyDictDict.update({myId: {toUserId: {'sendPersonalMessages': sendMessagesMyself}}})
        else:
            userPrivacyDictMy = privacyDictMy.get(toUserId, None)
            if userPrivacyDictMy is None:
                privacySettings = userProfilePrivacy.objects.get(user__exact = curUser)
                if friendShipStatus is None:
                    friendShipCache = caches['friendShipStatusCache']
                    friendShipStatusDict = friendShipCache.get(myId, None)
                    if friendShipStatusDict is None:
                        if friendsIds is None:
                            friendsIdsCache = caches['friendsIdCache']
                            friendsIds = friendsIdsCache.get(myId, None)
                            if friendsIds is None:
                                friendsCache = caches['friendsCache']
                                cachedFriends = friendsCache.get(myId, None)
                                if cachedFriends is None:
                                    cachedFriends = curUser.friends.all()
                                    if cachedFriends.count():
                                        friendsCache.set(myId, cachedFriends)
                                        friendsIdsQuerySet = cachedFriends.values("id")
                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    else:
                                        friendsCache.set(myId, False)
                                        friendsIds = set()
                                else:
                                    if cachedFriends is not False:
                                        friendsIdsQuerySet = cachedFriends.values("id")
                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    else:
                                        friendsIds = set()
                                friendsIdsCache.set(myId, friendsIds)
                        if toUserId in friendsIds:
                            friendShipCache.set(myId, {toUserId: True})
                        else:
                            friendShipRequestsCache = caches['friendshipRequests']
                            friendShipRequests = friendShipRequestsCache.get(myId, None)
                            friendShipStatus = False
                            if friendShipRequests is not None:
                                for friendShipRequest in friendShipRequests:
                                    if friendShipRequest[0] == toUserId:
                                        friendShipStatus = "request"
                                        break
                                if friendShipStatus is False:
                                    friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                    if friendShipRequests is not None:
                                        for friendShipRequest in friendShipRequests:
                                            if friendShipRequest[0] == myId:
                                                friendShipStatus = "myRequest"
                                                break
                            friendShipCache.set(myId, {toUserId: friendShipStatus})
                    else:
                        friendShipStatus = friendShipStatusDict.get(toUserId, None)
                        if friendShipStatus is None:
                            if friendsIds is None:
                                friendsIdsCache = caches['friendsIdCache']
                                friendsIds = friendsIdsCache.get(myId, None)
                                if friendsIds is None:
                                    friendsCache = caches['friendsCache']
                                    cachedFriends = friendsCache.get(myId, None)
                                    if cachedFriends is None:
                                        cachedFriends = curUser.friends.all()
                                        if cachedFriends.count():
                                            friendsCache.set(myId, cachedFriends)
                                            friendsIdsQuerySet = cachedFriends.values("id")
                                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                        else:
                                            friendsCache.set(myId, False)
                                            friendsIds = set()
                                    else:
                                        if cachedFriends is not False:
                                            friendsIdsQuerySet = cachedFriends.values("id")
                                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                        else:
                                            friendsIds = set()
                                    friendsIdsCache.set(myId, friendsIds)
                            if toUserId in friendsIds:
                                friendShipStatusDict.update({toUserId: True})
                            else:
                                friendShipRequestsCache = caches['friendshipRequests']
                                friendShipRequests = friendShipRequestsCache.get(myId, None)
                                friendShipStatus = False
                                if friendShipRequests is not None:
                                    for friendShipRequest in friendShipRequests:
                                        if friendShipRequest[0] == toUserId:
                                            friendShipStatus = "request"
                                            break
                                    if friendShipStatus is False:
                                        friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                        if friendShipRequests is not None:
                                            for friendShipRequest in friendShipRequests:
                                                if friendShipRequest[0] == myId:
                                                    friendShipStatus = "myRequest"
                                                    break
                privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                sendMessagesMyself = privacyChecker.sendPersonalMessages()
                privacyDictMyHasChanges = True
                privacyDictMy.update({toUserId: {'sendPersonalMessages': sendMessagesMyself}})
            else:
                sendMessagesMyself = userPrivacyDictMy.get('sendPersonalMessages', None)
                if sendMessagesMyself is None:
                    privacySettings = userProfilePrivacy.objects.get(user__exact = curUser)
                    if friendShipStatus is None:
                        friendShipCache = caches['friendShipStatusCache']
                        friendShipStatusDict = friendShipCache.get(myId, None)
                        if friendShipStatusDict is None:
                            if friendsIds is None:
                                friendsIdsCache = caches['friendsIdCache']
                                friendsIds = friendsIdsCache.get(myId, None)
                                if friendsIds is None:
                                    friendsCache = caches['friendsCache']
                                    cachedFriends = friendsCache.get(myId, None)
                                    if cachedFriends is None:
                                        cachedFriends = curUser.friends.all()
                                        if cachedFriends.count():
                                            friendsCache.set(myId, cachedFriends)
                                            friendsIdsQuerySet = cachedFriends.values("id")
                                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                        else:
                                            friendsCache.set(myId, False)
                                            friendsIds = set()
                                    else:
                                        if cachedFriends is not False:
                                            friendsIdsQuerySet = cachedFriends.values("id")
                                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                        else:
                                            friendsIds = set()
                                    friendsIdsCache.set(myId, friendsIds)
                            if toUserId in friendsIds:
                                friendShipCache.set(myId, {toUserId: True})
                            else:
                                friendShipRequestsCache = caches['friendshipRequests']
                                friendShipRequests = friendShipRequestsCache.get(myId, None)
                                friendShipStatus = False
                                if friendShipRequests is not None:
                                    for friendShipRequest in friendShipRequests:
                                        if friendShipRequest[0] == toUserId:
                                            friendShipStatus = "request"
                                            break
                                    if friendShipStatus is False:
                                        friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                        if friendShipRequests is not None:
                                            for friendShipRequest in friendShipRequests:
                                                if friendShipRequest[0] == myId:
                                                    friendShipStatus = "myRequest"
                                                    break
                                friendShipCache.set(myId, {toUserId: friendShipStatus})
                        else:
                            friendShipStatus = friendShipStatusDict.get(toUserId, None)
                            if friendShipStatus is None:
                                if friendsIds is None:
                                    friendsIdsCache = caches['friendsIdCache']
                                    friendsIds = friendsIdsCache.get(myId, None)
                                    if friendsIds is None:
                                        friendsCache = caches['friendsCache']
                                        cachedFriends = friendsCache.get(myId, None)
                                        if cachedFriends is None:
                                            cachedFriends = curUser.friends.all()
                                            if cachedFriends.count():
                                                friendsCache.set(myId, cachedFriends)
                                                friendsIdsQuerySet = cachedFriends.values("id")
                                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                            else:
                                                friendsCache.set(myId, False)
                                                friendsIds = set()
                                        else:
                                            if cachedFriends is not False:
                                                friendsIdsQuerySet = cachedFriends.values("id")
                                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                            else:
                                                friendsIds = set()
                                        friendsIdsCache.set(myId, friendsIds)
                                if toUserId in friendsIds:
                                    friendShipStatusDict.update({toUserId: True})
                                else:
                                    friendShipRequestsCache = caches['friendshipRequests']
                                    friendShipRequests = friendShipRequestsCache.get(myId, None)
                                    friendShipStatus = False
                                    if friendShipRequests is not None:
                                        for friendShipRequest in friendShipRequests:
                                            if friendShipRequest[0] == toUserId:
                                                friendShipStatus = "request"
                                                break
                                        if friendShipStatus is False:
                                            friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                            if friendShipRequests is not None:
                                                for friendShipRequest in friendShipRequests:
                                                    if friendShipRequest[0] == myId:
                                                        friendShipStatus = "myRequest"
                                                        break
                    privacyChecker = checkPrivacy(user = toUserInstance, privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                    sendMessagesMyself = privacyChecker.sendPersonalMessages()
                    userPrivacyDictMy.update({'sendPersonalMessages': sendMessagesMyself})
                    privacyDictMyHasChanges = True
        if privacyDictHasChanges or privacyDictMyHasChanges:
            if privacyDictHasChanges and privacyDictMyHasChanges:
                privacyCache.set_many({myId: privacyDictMy, toUserId: privacyDict})
            else:
                if privacyDictHasChanges:
                    privacyCache.set(toUserId, privacyDict)
                elif privacyDictMyHasChanges:
                    privacyCache.set(myId, privacyDictMy)
        else:
            if privacyDictHasChanges is None and privacyDictMyHasChanges is None:
                privacyCache.set_many(privacyDictDict)
        dialogCache = caches['dialogSingle']
        cachedDialog = dialogCache.get(myId, None)
        time = None
        if cachedDialog is None:
            dialog = dialogMessage.objects.filter(users__exact = curUser).filter(users__exact = toUserInstance).filter(type__exact = True)
            if dialog.count() == 0:
                time = datetime.now()
                dialog = dialogMessage.objects.create(type = True)
                dialog.users.add(curUser, toUserInstance)
                dialogId = str(dialog.id)
                existsMessagesInDialog = False
            else:
                dialog = dialog[0]
                dialogId = str(dialog.id)
                existsMessagesInDialog = True
            dialogCache.set(myId, {'instance': dialog, 'withUserId': toUserId})
        else:
            withUser = cachedDialog.get('withUserId', None)
            if withUser is None:
                dialog = dialogMessage.objects.filter(users__exact = curUser).filter(users__exact = toUserInstance).filter(type__exact = True)
                if dialog.count() == 0:
                    time = datetime.now()
                    dialog = dialogMessage.objects.create(type = True)
                    dialog.users.add(curUser, toUserInstance)
                    dialogId = str(dialog.id)
                    existsMessagesInDialog = False
                else:
                    dialog = dialog[0]
                    dialogId = str(dialog.id)
                    existsMessagesInDialog = True
                dialogCache.set(myId, {'instance': dialog, 'withUserId': toUserId})
            else:
                dialog = cachedDialog.get('instance', None)
                if dialog is None:
                    dialog = dialogMessage.objects.filter(users__exact = curUser).filter(users__exact = toUserInstance).filter(type__exact = True)
                    if dialog.count() == 0:
                        time = datetime.now()
                        dialog = dialogMessage.objects.create(type = True)
                        dialog.users.add(curUser, toUserInstance)
                        dialogId = str(dialog.id)
                        existsMessagesInDialog = False
                    else:
                        dialog = dialog[0]
                        dialogId = str(dialog.id)
                        existsMessagesInDialog = True
                    dialogCache.set(myId, {'instance': dialog, 'withUserId': toUserId})
                else:
                    if withUser != toUserId:
                        dialog = dialogMessage.objects.filter(users__exact = curUser).filter(users__exact = toUserInstance).filter(type__exact = True)
                        if dialog.count() == 0:
                            time = datetime.now()
                            dialog = dialogMessage.objects.create(type = True)
                            dialog.users.add(curUser, toUserInstance)
                            dialogId = str(dialog.id)
                            existsMessagesInDialog = False
                        else:
                            dialog = dialog[0]
                            dialogId = str(dialog.id)
                            existsMessagesInDialog = True
                        dialogCache.set(myId, {'instance': dialog, 'withUserId': toUserId})
                    else:
                        dialogId = str(dialog.id)
                        existsMessagesInDialog = True
        if request.is_ajax():
            curUser = request.user
            messageTextBody = request.POST.get('body', None)
            messageTextBodyLen = len(messageTextBody)
            if messageTextBody is not None and messageTextBodyLen <= 1000:
                lastMessageCache = caches['lastMessage']
                lastMessage = lastMessageCache.get(dialogId, None)
                dialogsCache = caches["dialogsCache"]
                cachedDialogsMany = dialogsCache.get_many({myId, toUserId})
                cachedDialogsDictDictMy = cachedDialogsMany.get(myId, None)
                cachedDialogsDictDictToUser = cachedDialogsMany.get(toUserId, None)
                newUpdates = caches["newUpdates"]
                oldData = newUpdates.get(toUserId, None)
                unreadMessagesCache = caches["unreadPersonalMessages"]
                oldUnreadMessagesDictDict = unreadMessagesCache.get(dialogId, None)
                if oldUnreadMessagesDictDict is not None:
                    oldUnreadMessages = oldUnreadMessagesDictDict.get(toUserId, None)
                save = True
                if time is None:
                    time = datetime.now(tz = tz.gettz())
                messagesFilesStorage = caches['filesStorageMessages']
                cachedFilesDict = messagesFilesStorage.get(dialogId, None)
                if cachedFilesDict is not None:
                    cachedFilesDictDict = cachedFilesDict.get(myId, None)
                    if cachedFilesDictDict is not None:
                        cachedFilesDictDictImages = cachedFilesDictDict.get("images", None)
                        if cachedFilesDictDictImages is not None:
                            try:
                                with atomic():
                                    if lastMessage is None:
                                        lastMessage = personalMessage.objects.filter(inDialog__exact = dialog)
                                        if lastMessage.count() > 0:
                                            lastMessage = lastMessage.last()
                                            if str(lastMessage.by_id) == myId:
                                                newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = False, inDialog = dialog, time = time)
                                            else:
                                                newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = True, inDialog = dialog, time = time)
                                        else:
                                            newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = True, inDialog = dialog, time = time)
                                    else:
                                        if str(lastMessage.by_id) == myId:
                                            newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = False, inDialog = dialog, time = time)
                                        else:
                                            newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = True, inDialog = dialog, time = time)
                                    newImagesAttachment = imagesAttachmentMessagePersonal(message = newMessage, by = curUser, itemsLength = 0)
                                    cachedFilesDictDictImagesList = tuple(cachedFilesDictDictImages.values())
                                    cachedFilesDictDictImagesLength = len(cachedFilesDictDictImagesList)
                                    if cachedFilesDictDictImagesLength <= 3:
                                        if cachedFilesDictDictImagesLength == 1:
                                            imageItemTupleItem1 = cachedFilesDictDictImagesList[0]
                                            file1 = ContentFile(base64.b64decode(imageItemTupleItem1[0]))
                                            file1.name = '{0}.{1}'.format(imageItemTupleItem1[1], imageItemTupleItem1[2])
                                            newImagesAttachment.image1 = file1
                                        elif cachedFilesDictDictImagesLength == 2:
                                            imageItemTupleItem1 = cachedFilesDictDictImagesList[0]
                                            file1 = ContentFile(base64.b64decode(imageItemTupleItem1[0]))
                                            file1.name = '{0}.{1}'.format(imageItemTupleItem1[1], imageItemTupleItem1[2])
                                            newImagesAttachment.image1 = file1
                                            imageItemTupleItem2 = cachedFilesDictDictImagesList[1]
                                            file2 = ContentFile(base64.b64decode(imageItemTupleItem2[0]))
                                            file2.name = '{0}.{1}'.format(imageItemTupleItem2[1], imageItemTupleItem2[2])
                                            newImagesAttachment.image2 = file2
                                        else:
                                            imageItemTupleItem1 = cachedFilesDictDictImagesList[0]
                                            file1 = ContentFile(base64.b64decode(imageItemTupleItem1[0]))
                                            file1.name = '{0}.{1}'.format(imageItemTupleItem1[1], imageItemTupleItem1[2])
                                            newImagesAttachment.image1 = file1
                                            imageItemTupleItem2 = cachedFilesDictDictImagesList[1]
                                            file2 = ContentFile(base64.b64decode(imageItemTupleItem2[0]))
                                            file2.name = '{0}.{1}'.format(imageItemTupleItem2[1], imageItemTupleItem2[2])
                                            newImagesAttachment.image2 = file2
                                            imageItemTupleItem3 = cachedFilesDictDictImagesList[2]
                                            file3 = ContentFile(base64.b64decode(imageItemTupleItem3[0]))
                                            file3.name = '{0}.{1}'.format(imageItemTupleItem3[1], imageItemTupleItem3[2])
                                            newImagesAttachment.image3 = file3
                                    else:
                                        if cachedFilesDictDictImagesLength == 4:
                                            imageItemTupleItem1 = cachedFilesDictDictImagesList[0]
                                            file1 = ContentFile(base64.b64decode(imageItemTupleItem1[0]))
                                            file1.name = '{0}.{1}'.format(imageItemTupleItem1[1], imageItemTupleItem1[2])
                                            newImagesAttachment.image1 = file1
                                            imageItemTupleItem2 = cachedFilesDictDictImagesList[1]
                                            file2 = ContentFile(base64.b64decode(imageItemTupleItem2[0]))
                                            file2.name = '{0}.{1}'.format(imageItemTupleItem2[1], imageItemTupleItem2[2])
                                            newImagesAttachment.image2 = file2
                                            imageItemTupleItem3 = cachedFilesDictDictImagesList[2]
                                            file3 = ContentFile(base64.b64decode(imageItemTupleItem3[0]))
                                            file3.name = '{0}.{1}'.format(imageItemTupleItem3[1], imageItemTupleItem3[2])
                                            newImagesAttachment.image3 = file3
                                            imageItemTupleItem4 = cachedFilesDictDictImagesList[3]
                                            file4 = ContentFile(base64.b64decode(imageItemTupleItem4[0]))
                                            file4.name = '{0}.{1}'.format(imageItemTupleItem4[1], imageItemTupleItem4[2])
                                            newImagesAttachment.image4 = file4
                                        elif cachedFilesDictDictImagesLength == 5:
                                            imageItemTupleItem1 = cachedFilesDictDictImagesList[0]
                                            file1 = ContentFile(base64.b64decode(imageItemTupleItem1[0]))
                                            file1.name = '{0}.{1}'.format(imageItemTupleItem1[1], imageItemTupleItem1[2])
                                            newImagesAttachment.image1 = file1
                                            imageItemTupleItem2 = cachedFilesDictDictImagesList[1]
                                            file2 = ContentFile(base64.b64decode(imageItemTupleItem2[0]))
                                            file2.name = '{0}.{1}'.format(imageItemTupleItem2[1], imageItemTupleItem2[2])
                                            newImagesAttachment.image2 = file2
                                            imageItemTupleItem3 = cachedFilesDictDictImagesList[2]
                                            file3 = ContentFile(base64.b64decode(imageItemTupleItem3[0]))
                                            file3.name = '{0}.{1}'.format(imageItemTupleItem3[1], imageItemTupleItem3[2])
                                            newImagesAttachment.image3 = file3
                                            imageItemTupleItem4 = cachedFilesDictDictImagesList[3]
                                            file4 = ContentFile(base64.b64decode(imageItemTupleItem4[0]))
                                            file4.name = '{0}.{1}'.format(imageItemTupleItem4[1], imageItemTupleItem4[2])
                                            newImagesAttachment.image4 = file4
                                            imageItemTupleItem5 = cachedFilesDictDictImagesList[4]
                                            file5 = ContentFile(base64.b64decode(imageItemTupleItem5[0]))
                                            file5.name = '{0}.{1}'.format(imageItemTupleItem5[1], imageItemTupleItem5[2])
                                            newImagesAttachment.image5 = file5
                                        else:
                                            imageItemTupleItem1 = cachedFilesDictDictImagesList[0]
                                            file1 = ContentFile(base64.b64decode(imageItemTupleItem1[0]))
                                            file1.name = '{0}.{1}'.format(imageItemTupleItem1[1], imageItemTupleItem1[2])
                                            newImagesAttachment.image1 = file1
                                            imageItemTupleItem2 = cachedFilesDictDictImagesList[1]
                                            file2 = ContentFile(base64.b64decode(imageItemTupleItem2[0]))
                                            file2.name = '{0}.{1}'.format(imageItemTupleItem2[1], imageItemTupleItem2[2])
                                            newImagesAttachment.image2 = file2
                                            imageItemTupleItem3 = cachedFilesDictDictImagesList[2]
                                            file3 = ContentFile(base64.b64decode(imageItemTupleItem3[0]))
                                            file3.name = '{0}.{1}'.format(imageItemTupleItem3[1], imageItemTupleItem3[2])
                                            newImagesAttachment.image3 = file3
                                            imageItemTupleItem4 = cachedFilesDictDictImagesList[3]
                                            file4 = ContentFile(base64.b64decode(imageItemTupleItem4[0]))
                                            file4.name = '{0}.{1}'.format(imageItemTupleItem4[1], imageItemTupleItem4[2])
                                            newImagesAttachment.image4 = file4
                                            imageItemTupleItem5 = cachedFilesDictDictImagesList[4]
                                            file5 = ContentFile(base64.b64decode(imageItemTupleItem5[0]))
                                            file5.name = '{0}.{1}'.format(imageItemTupleItem5[1], imageItemTupleItem5[2])
                                            newImagesAttachment.image5 = file5
                                            imageItemTupleItem6 = cachedFilesDictDictImagesList[5]
                                            file6 = ContentFile(base64.b64decode(imageItemTupleItem6[0]))
                                            file6.name = '{0}.{1}'.format(imageItemTupleItem6[1], imageItemTupleItem6[2])
                                            newImagesAttachment.image6 = file6
                                    newImagesAttachment.itemsLength = cachedFilesDictDictImagesLength
                                    newMessage.imagesAttachment = newImagesAttachment
                                    if existsMessagesInDialog is False:
                                        dialog.save()
                                    newMessage.save()
                                    newImagesAttachment.save()
                                    lastMessageCache.set(dialogId, newMessage)
                                    messageIdStr = str(newMessage.id)
                                    if oldUnreadMessagesDictDict is None:
                                        unreadMessagesCache.set(dialogId, {toUserId: {messageIdStr,}})
                                    else:
                                        if oldUnreadMessages is None:
                                            oldUnreadMessagesDictDict.update({toUserId: {messageIdStr,}})
                                            unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                                        else:
                                            oldUnreadMessages.add(messageIdStr)
                                            unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                                    if oldData is not None:
                                        finded = None
                                        for item in oldData:
                                            if dialogId == item[3]:
                                                finded = item
                                                break
                                        if finded is None:
                                            oldData.append(('personalMessage', 'new', myId, dialogId))
                                            newUpdates.set(toUserId, oldData)
                                        else:
                                            if finded[1] == "hidden":
                                                oldData[oldData.index(finded)][1] = "new"
                                                newUpdates.set(toUserId, oldData)
                                                finded = "hidden"
                                    else:
                                        newUpdates.set(toUserId, [('personalMessage', 'new', myId, dialogId)])
                                    if cachedDialogsDictDictMy is not None and cachedDialogsDictDictToUser is not None:
                                        lastMessagesList = None
                                        cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                        if cachedDialogsDictMy is not None:
                                            if dialogId not in cachedDialogsDictMy.keys():
                                                cachedDialogsDictMy.update({dialogId: dialog})
                                            cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                        lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                        if lastMessagesDictListMy is not None:
                                            lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                            if lastMessagesList is not None:
                                                lastMessagesList.append(newMessage)
                                                if len(lastMessagesList) > 6:
                                                    del lastMessagesList[0]
                                            else:
                                                lastMessagesList = [newMessage]
                                            lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                        cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                        if cachedDialogsDictToUser is not None:
                                            if dialogId not in cachedDialogsDictToUser.keys():
                                                cachedDialogsDictToUser.update({dialogId: dialog})
                                            cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                        lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                        if lastMessagesDictListToUser is not None:
                                            if lastMessagesList is None:
                                                lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                                if lastMessagesList is not None:
                                                    lastMessagesList.append(newMessage)
                                                    if len(lastMessagesList) > 6:
                                                        del lastMessagesList[0]
                                                else:
                                                    lastMessagesList = [newMessage]
                                            lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                        dialogsCache.set_many({myId: cachedDialogsDictDictMy, toUserId: cachedDialogsDictDictToUser})
                                    else:
                                        if cachedDialogsDictDictMy is not None:
                                            cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                            if cachedDialogsDictMy is not None:
                                                if dialogId not in cachedDialogsDictMy.keys():
                                                    cachedDialogsDictMy.update({dialogId: dialog})
                                                cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                            lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                            if lastMessagesDictListMy is not None:
                                                lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                                if lastMessagesList is not None:
                                                    lastMessagesList.append(newMessage)
                                                    if len(lastMessagesList) > 6:
                                                        del lastMessagesList[0]
                                                else:
                                                    lastMessagesList = [newMessage]
                                                lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                            dialogsCache.set(myId, cachedDialogsDictDictMy)
                                        elif cachedDialogsDictDictToUser is not None:
                                            cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                            if cachedDialogsDictToUser is not None:
                                                if dialogId not in cachedDialogsDictToUser.keys():
                                                    cachedDialogsDictToUser.update({dialogId: dialog})
                                                cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                            lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                            if lastMessagesDictListToUser is not None:
                                                lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                                if lastMessagesList is not None:
                                                    lastMessagesList.append(newMessage)
                                                    if len(lastMessagesList) > 6:
                                                        del lastMessagesList[0]
                                                else:
                                                    lastMessagesList = [newMessage]
                                                lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                            dialogsCache.set(toUserId, cachedDialogsDictDictToUser)
                            except Error:
                                save = False
                            if save is True:
                                del cachedFilesDict[myId]
                                messagesFilesStorage.set(dialogId, cachedFilesDict)
                        else:
                            if existsMessagesInDialog is False:
                                try:
                                    with atomic():
                                        if lastMessage is None:
                                            lastMessage = personalMessage.objects.filter(inDialog__exact = dialog)
                                            if lastMessage.count() > 0:
                                                lastMessage = lastMessage.last()
                                                if str(lastMessage.by_id) == myId:
                                                    newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = False, inDialog = dialog, time = time)
                                                else:
                                                    newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = True, inDialog = dialog, time = time)
                                            else:
                                                newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = True, inDialog = dialog, time = time)
                                        else:
                                            if str(lastMessage.by_id) == myId:
                                                newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = False, inDialog = dialog, time = time)
                                            else:
                                                newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = True, inDialog = dialog, time = time)
                                        dialog.save()
                                        newMessage.save()
                                        lastMessageCache.set(dialogId, newMessage)
                                        messageIdStr = str(newMessage.id)
                                        if oldUnreadMessagesDictDict is None:
                                            unreadMessagesCache.set(dialogId, {toUserId: {messageIdStr,}})
                                        else:
                                            if oldUnreadMessages is None:
                                                oldUnreadMessagesDictDict.update({toUserId: {messageIdStr,}})
                                                unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                                            else:
                                                oldUnreadMessages.add(messageIdStr)
                                                unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                                        if oldData is not None:
                                            finded = None
                                            for item in oldData:
                                                if dialogId == item[3]:
                                                    finded = item
                                                    break
                                            if finded is None:
                                                oldData.append(('personalMessage', 'new', myId, dialogId))
                                                newUpdates.set(toUserId, oldData)
                                            else:
                                                if finded[1] == "hidden":
                                                    oldData[oldData.index(finded)][1] = "new"
                                                    newUpdates.set(toUserId, oldData)
                                                    finded = "hidden"
                                        else:
                                            newUpdates.set(toUserId, [('personalMessage', 'new', myId, dialogId)])
                                        if cachedDialogsDictDictMy is not None and cachedDialogsDictDictToUser is not None:
                                            lastMessagesList = None
                                            cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                            if cachedDialogsDictMy is not None:
                                                if dialogId not in cachedDialogsDictMy.keys():
                                                    cachedDialogsDictMy.update({dialogId: dialog})
                                                cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                            lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                            if lastMessagesDictListMy is not None:
                                                lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                                if lastMessagesList is not None:
                                                    lastMessagesList.append(newMessage)
                                                    if len(lastMessagesList) > 6:
                                                        del lastMessagesList[0]
                                                else:
                                                    lastMessagesList = [newMessage]
                                                lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                            cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                            if cachedDialogsDictToUser is not None:
                                                if dialogId not in cachedDialogsDictToUser.keys():
                                                    cachedDialogsDictToUser.update({dialogId: dialog})
                                                cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                            lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                            if lastMessagesDictListToUser is not None:
                                                if lastMessagesList is None:
                                                    lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                                    if lastMessagesList is not None:
                                                        lastMessagesList.append(newMessage)
                                                        if len(lastMessagesList) > 6:
                                                            del lastMessagesList[0]
                                                    else:
                                                        lastMessagesList = [newMessage]
                                                lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                            dialogsCache.set_many({myId: cachedDialogsDictDictMy, toUserId: cachedDialogsDictDictToUser})
                                        else:
                                            if cachedDialogsDictDictMy is not None:
                                                cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                                if cachedDialogsDictMy is not None:
                                                    if dialogId not in cachedDialogsDictMy.keys():
                                                        cachedDialogsDictMy.update({dialogId: dialog})
                                                    cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                                lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                                if lastMessagesDictListMy is not None:
                                                    lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                                    if lastMessagesList is not None:
                                                        lastMessagesList.append(newMessage)
                                                        if len(lastMessagesList) > 6:
                                                            del lastMessagesList[0]
                                                    else:
                                                        lastMessagesList = [newMessage]
                                                    lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                                dialogsCache.set(myId, cachedDialogsDictDictMy)
                                            elif cachedDialogsDictDictToUser is not None:
                                                cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                                if cachedDialogsDictToUser is not None:
                                                    if dialogId not in cachedDialogsDictToUser.keys():
                                                        cachedDialogsDictToUser.update({dialogId: dialog})
                                                    cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                                lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                                if lastMessagesDictListToUser is not None:
                                                    lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                                    if lastMessagesList is not None:
                                                        lastMessagesList.append(newMessage)
                                                        if len(lastMessagesList) > 6:
                                                            del lastMessagesList[0]
                                                    else:
                                                        lastMessagesList = [newMessage]
                                                    lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                                dialogsCache.set(toUserId, cachedDialogsDictDictToUser)
                                except Error:
                                    save = False
                            else:
                                try:
                                    with atomic():
                                        newMessage.save()
                                        lastMessageCache.set(dialogId, newMessage)
                                        messageIdStr = str(newMessage.id)
                                        if oldUnreadMessagesDictDict is None:
                                            unreadMessagesCache.set(dialogId, {toUserId: {messageIdStr,}})
                                        else:
                                            if oldUnreadMessages is None:
                                                oldUnreadMessagesDictDict.update({toUserId: {messageIdStr,}})
                                                unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                                            else:
                                                oldUnreadMessages.add(messageIdStr)
                                                unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                                        if oldData is not None:
                                            finded = None
                                            for item in oldData:
                                                if dialogId == item[3]:
                                                    finded = item
                                                    break
                                            if finded is None:
                                                oldData.append(('personalMessage', 'new', myId, dialogId))
                                                newUpdates.set(toUserId, oldData)
                                            else:
                                                if finded[1] == "hidden":
                                                    oldData[oldData.index(finded)][1] = "new"
                                                    newUpdates.set(toUserId, oldData)
                                                    finded = "hidden"
                                        else:
                                            newUpdates.set(toUserId, [('personalMessage', 'new', myId, dialogId)])
                                        if cachedDialogsDictDictMy is not None and cachedDialogsDictDictToUser is not None:
                                            lastMessagesList = None
                                            cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                            if cachedDialogsDictMy is not None:
                                                if dialogId not in cachedDialogsDictMy.keys():
                                                    cachedDialogsDictMy.update({dialogId: dialog})
                                                cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                            lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                            if lastMessagesDictListMy is not None:
                                                lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                                if lastMessagesList is not None:
                                                    lastMessagesList.append(newMessage)
                                                    if len(lastMessagesList) > 6:
                                                        del lastMessagesList[0]
                                                else:
                                                    lastMessagesList = [newMessage]
                                                lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                            cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                            if cachedDialogsDictToUser is not None:
                                                if dialogId not in cachedDialogsDictToUser.keys():
                                                    cachedDialogsDictToUser.update({dialogId: dialog})
                                                cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                            lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                            if lastMessagesDictListToUser is not None:
                                                if lastMessagesList is None:
                                                    lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                                    if lastMessagesList is not None:
                                                        lastMessagesList.append(newMessage)
                                                        if len(lastMessagesList) > 6:
                                                            del lastMessagesList[0]
                                                    else:
                                                        lastMessagesList = [newMessage]
                                                lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                            dialogsCache.set_many({myId: cachedDialogsDictDictMy, toUserId: cachedDialogsDictDictToUser})
                                        else:
                                            if cachedDialogsDictDictMy is not None:
                                                cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                                if cachedDialogsDictMy is not None:
                                                    if dialogId not in cachedDialogsDictMy.keys():
                                                        cachedDialogsDictMy.update({dialogId: dialog})
                                                    cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                                lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                                if lastMessagesDictListMy is not None:
                                                    lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                                    if lastMessagesList is not None:
                                                        lastMessagesList.append(newMessage)
                                                        if len(lastMessagesList) > 6:
                                                            del lastMessagesList[0]
                                                    else:
                                                        lastMessagesList = [newMessage]
                                                    lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                                dialogsCache.set(myId, cachedDialogsDictDictMy)
                                            elif cachedDialogsDictDictToUser is not None:
                                                cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                                if cachedDialogsDictToUser is not None:
                                                    if dialogId not in cachedDialogsDictToUser.keys():
                                                        cachedDialogsDictToUser.update({dialogId: dialog})
                                                    cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                                lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                                if lastMessagesDictListToUser is not None:
                                                    lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                                    if lastMessagesList is not None:
                                                        lastMessagesList.append(newMessage)
                                                        if len(lastMessagesList) > 6:
                                                            del lastMessagesList[0]
                                                    else:
                                                        lastMessagesList = [newMessage]
                                                    lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                                dialogsCache.set(toUserId, cachedDialogsDictDictToUser)
                                except Error:
                                    save = False
                            newImagesAttachment = None
                    else:
                        if existsMessagesInDialog is False:
                            try:
                                with atomic():
                                    if lastMessage is None:
                                        lastMessage = personalMessage.objects.filter(inDialog__exact = dialog)
                                        if lastMessage.count() > 0:
                                            lastMessage = lastMessage.last()
                                            if str(lastMessage.by_id) == myId:
                                                newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = False, inDialog = dialog, time = time)
                                            else:
                                                newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = True, inDialog = dialog, time = time)
                                        else:
                                            newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = True, inDialog = dialog, time = time)
                                    else:
                                        if str(lastMessage.by_id) == myId:
                                            newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = False, inDialog = dialog, time = time)
                                        else:
                                            newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = True, inDialog = dialog, time = time)
                                    dialog.save()
                                    newMessage.save()
                                    lastMessageCache.set(dialogId, newMessage)
                                    messageIdStr = str(newMessage.id)
                                    if oldUnreadMessagesDictDict is None:
                                        unreadMessagesCache.set(dialogId, {toUserId: {messageIdStr,}})
                                    else:
                                        if oldUnreadMessages is None:
                                            oldUnreadMessagesDictDict.update({toUserId: {messageIdStr,}})
                                            unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                                        else:
                                            oldUnreadMessages.add(messageIdStr)
                                            unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                                    if oldData is not None:
                                        finded = None
                                        for item in oldData:
                                            if dialogId == item[3]:
                                                finded = item
                                                break
                                        if finded is None:
                                            oldData.append(('personalMessage', 'new', myId, dialogId))
                                            newUpdates.set(toUserId, oldData)
                                        else:
                                            if finded[1] == "hidden":
                                                oldData[oldData.index(finded)][1] = "new"
                                                newUpdates.set(toUserId, oldData)
                                                finded = "hidden"
                                    else:
                                        newUpdates.set(toUserId, [('personalMessage', 'new', myId, dialogId)])
                                    if cachedDialogsDictDictMy is not None and cachedDialogsDictDictToUser is not None:
                                        lastMessagesList = None
                                        cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                        if cachedDialogsDictMy is not None:
                                            if dialogId not in cachedDialogsDictMy.keys():
                                                cachedDialogsDictMy.update({dialogId: dialog})
                                            cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                        lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                        if lastMessagesDictListMy is not None:
                                            lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                            if lastMessagesList is not None:
                                                lastMessagesList.append(newMessage)
                                                if len(lastMessagesList) > 6:
                                                    del lastMessagesList[0]
                                            else:
                                                lastMessagesList = [newMessage]
                                            lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                        cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                        if cachedDialogsDictToUser is not None:
                                            if dialogId not in cachedDialogsDictToUser.keys():
                                                cachedDialogsDictToUser.update({dialogId: dialog})
                                            cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                        lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                        if lastMessagesDictListToUser is not None:
                                            if lastMessagesList is None:
                                                lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                                if lastMessagesList is not None:
                                                    lastMessagesList.append(newMessage)
                                                    if len(lastMessagesList) > 6:
                                                        del lastMessagesList[0]
                                                else:
                                                    lastMessagesList = [newMessage]
                                            lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                        dialogsCache.set_many({myId: cachedDialogsDictDictMy, toUserId: cachedDialogsDictDictToUser})
                                    else:
                                        if cachedDialogsDictDictMy is not None:
                                            cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                            if cachedDialogsDictMy is not None:
                                                if dialogId not in cachedDialogsDictMy.keys():
                                                    cachedDialogsDictMy.update({dialogId: dialog})
                                                cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                            lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                            if lastMessagesDictListMy is not None:
                                                lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                                if lastMessagesList is not None:
                                                    lastMessagesList.append(newMessage)
                                                    if len(lastMessagesList) > 6:
                                                        del lastMessagesList[0]
                                                else:
                                                    lastMessagesList = [newMessage]
                                                lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                            dialogsCache.set(myId, cachedDialogsDictDictMy)
                                        elif cachedDialogsDictDictToUser is not None:
                                            cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                            if cachedDialogsDictToUser is not None:
                                                if dialogId not in cachedDialogsDictToUser.keys():
                                                    cachedDialogsDictToUser.update({dialogId: dialog})
                                                cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                            lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                            if lastMessagesDictListToUser is not None:
                                                lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                                if lastMessagesList is not None:
                                                    lastMessagesList.append(newMessage)
                                                    if len(lastMessagesList) > 6:
                                                        del lastMessagesList[0]
                                                else:
                                                    lastMessagesList = [newMessage]
                                                lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                            dialogsCache.set(toUserId, cachedDialogsDictDictToUser)
                            except Error:
                                save = False
                        else:
                            try:
                                with atomic():
                                    if lastMessage is None:
                                        lastMessage = personalMessage.objects.filter(inDialog__exact = dialog)
                                        if lastMessage.count() > 0:
                                            lastMessage = lastMessage.last()
                                            if str(lastMessage.by_id) == myId:
                                                newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = False, inDialog = dialog, time = time)
                                            else:
                                                newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = True, inDialog = dialog, time = time)
                                        else:
                                            newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = True, inDialog = dialog, time = time)
                                    else:
                                        if str(lastMessage.by_id) == myId:
                                            newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = False, inDialog = dialog, time = time)
                                        else:
                                            newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = True, inDialog = dialog, time = time)
                                    newMessage.save()
                                    lastMessageCache.set(dialogId, newMessage)
                                    messageIdStr = str(newMessage.id)
                                    if oldUnreadMessagesDictDict is None:
                                        unreadMessagesCache.set(dialogId, {toUserId: {messageIdStr,}})
                                    else:
                                        if oldUnreadMessages is None:
                                            oldUnreadMessagesDictDict.update({toUserId: {messageIdStr,}})
                                            unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                                        else:
                                            oldUnreadMessages.add(messageIdStr)
                                            unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                                    if oldData is not None:
                                        finded = None
                                        for item in oldData:
                                            if dialogId == item[3]:
                                                finded = item
                                                break
                                        if finded is None:
                                            oldData.append(('personalMessage', 'new', myId, dialogId))
                                            newUpdates.set(toUserId, oldData)
                                        else:
                                            if finded[1] == "hidden":
                                                oldData[oldData.index(finded)][1] = "new"
                                                newUpdates.set(toUserId, oldData)
                                                finded = "hidden"
                                    else:
                                        newUpdates.set(toUserId, [('personalMessage', 'new', myId, dialogId)])
                                    if cachedDialogsDictDictMy is not None and cachedDialogsDictDictToUser is not None:
                                        lastMessagesList = None
                                        cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                        if cachedDialogsDictMy is not None:
                                            if dialogId not in cachedDialogsDictMy.keys():
                                                cachedDialogsDictMy.update({dialogId: dialog})
                                            cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                        lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                        if lastMessagesDictListMy is not None:
                                            lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                            if lastMessagesList is not None:
                                                lastMessagesList.append(newMessage)
                                                if len(lastMessagesList) > 6:
                                                    del lastMessagesList[0]
                                            else:
                                                lastMessagesList = [newMessage]
                                            lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                        cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                        if cachedDialogsDictToUser is not None:
                                            if dialogId not in cachedDialogsDictToUser.keys():
                                                cachedDialogsDictToUser.update({dialogId: dialog})
                                            cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                        lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                        if lastMessagesDictListToUser is not None:
                                            if lastMessagesList is None:
                                                lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                                if lastMessagesList is not None:
                                                    lastMessagesList.append(newMessage)
                                                    if len(lastMessagesList) > 6:
                                                        del lastMessagesList[0]
                                                else:
                                                    lastMessagesList = [newMessage]
                                            lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                        dialogsCache.set_many({myId: cachedDialogsDictDictMy, toUserId: cachedDialogsDictDictToUser})
                                    else:
                                        if cachedDialogsDictDictMy is not None:
                                            cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                            if cachedDialogsDictMy is not None:
                                                if dialogId not in cachedDialogsDictMy.keys():
                                                    cachedDialogsDictMy.update({dialogId: dialog})
                                                cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                            lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                            if lastMessagesDictListMy is not None:
                                                lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                                if lastMessagesList is not None:
                                                    lastMessagesList.append(newMessage)
                                                    if len(lastMessagesList) > 6:
                                                        del lastMessagesList[0]
                                                else:
                                                    lastMessagesList = [newMessage]
                                                lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                            dialogsCache.set(myId, cachedDialogsDictDictMy)
                                        elif cachedDialogsDictDictToUser is not None:
                                            cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                            if cachedDialogsDictToUser is not None:
                                                if dialogId not in cachedDialogsDictToUser.keys():
                                                    cachedDialogsDictToUser.update({dialogId: dialog})
                                                cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                            lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                            if lastMessagesDictListToUser is not None:
                                                lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                                if lastMessagesList is not None:
                                                    lastMessagesList.append(newMessage)
                                                    if len(lastMessagesList) > 6:
                                                        del lastMessagesList[0]
                                                else:
                                                    lastMessagesList = [newMessage]
                                                lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                            dialogsCache.set(toUserId, cachedDialogsDictDictToUser)
                            except Error:
                                save = False
                        newImagesAttachment = None
                else:
                    if messageTextBodyLen > 0:
                        if existsMessagesInDialog is False:
                            try:
                                with atomic():
                                    if lastMessage is None:
                                        lastMessage = personalMessage.objects.filter(inDialog__exact = dialog)
                                        if lastMessage.count() > 0:
                                            lastMessage = lastMessage.last()
                                            if str(lastMessage.by_id) == myId:
                                                newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = False, inDialog = dialog, time = time)
                                            else:
                                                newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = True, inDialog = dialog, time = time)
                                        else:
                                            newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = True, inDialog = dialog, time = time)
                                    else:
                                        if str(lastMessage.by_id) == myId:
                                            newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = False, inDialog = dialog, time = time)
                                        else:
                                            newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = True, inDialog = dialog, time = time)
                                    dialog.save()
                                    newMessage.save()
                                    lastMessageCache.set(dialogId, newMessage)
                                    messageIdStr = str(newMessage.id)
                                    if oldUnreadMessagesDictDict is None:
                                        unreadMessagesCache.set(dialogId, {toUserId: {messageIdStr,}})
                                    else:
                                        if oldUnreadMessages is None:
                                            oldUnreadMessagesDictDict.update({toUserId: {messageIdStr,}})
                                            unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                                        else:
                                            oldUnreadMessages.add(messageIdStr)
                                            unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                                    if oldData is not None:
                                        finded = None
                                        for item in oldData:
                                            if dialogId == item[3]:
                                                finded = item
                                                break
                                        if finded is None:
                                            oldData.append(('personalMessage', 'new', myId, dialogId))
                                            newUpdates.set(toUserId, oldData)
                                        else:
                                            if finded[1] == "hidden":
                                                oldData[oldData.index(finded)][1] = "new"
                                                newUpdates.set(toUserId, oldData)
                                                finded = "hidden"
                                    else:
                                        newUpdates.set(toUserId, [('personalMessage', 'new', myId, dialogId)])
                                    if cachedDialogsDictDictMy is not None and cachedDialogsDictDictToUser is not None:
                                        lastMessagesList = None
                                        cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                        if cachedDialogsDictMy is not None:
                                            if dialogId not in cachedDialogsDictMy.keys():
                                                cachedDialogsDictMy.update({dialogId: dialog})
                                            cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                        lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                        if lastMessagesDictListMy is not None:
                                            lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                            if lastMessagesList is not None:
                                                lastMessagesList.append(newMessage)
                                                if len(lastMessagesList) > 6:
                                                    del lastMessagesList[0]
                                            else:
                                                lastMessagesList = [newMessage]
                                            lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                        cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                        if cachedDialogsDictToUser is not None:
                                            if dialogId not in cachedDialogsDictToUser.keys():
                                                cachedDialogsDictToUser.update({dialogId: dialog})
                                            cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                        lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                        if lastMessagesDictListToUser is not None:
                                            if lastMessagesList is None:
                                                lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                                if lastMessagesList is not None:
                                                    lastMessagesList.append(newMessage)
                                                    if len(lastMessagesList) > 6:
                                                        del lastMessagesList[0]
                                                else:
                                                    lastMessagesList = [newMessage]
                                            lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                        dialogsCache.set_many({myId: cachedDialogsDictDictMy, toUserId: cachedDialogsDictDictToUser})
                                    else:
                                        if cachedDialogsDictDictMy is not None:
                                            cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                            if cachedDialogsDictMy is not None:
                                                if dialogId not in cachedDialogsDictMy.keys():
                                                    cachedDialogsDictMy.update({dialogId: dialog})
                                                cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                            lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                            if lastMessagesDictListMy is not None:
                                                lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                                if lastMessagesList is not None:
                                                    lastMessagesList.append(newMessage)
                                                    if len(lastMessagesList) > 6:
                                                        del lastMessagesList[0]
                                                else:
                                                    lastMessagesList = [newMessage]
                                                lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                            dialogsCache.set(myId, cachedDialogsDictDictMy)
                                        elif cachedDialogsDictDictToUser is not None:
                                            cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                            if cachedDialogsDictToUser is not None:
                                                if dialogId not in cachedDialogsDictToUser.keys():
                                                    cachedDialogsDictToUser.update({dialogId: dialog})
                                                cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                            lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                            if lastMessagesDictListToUser is not None:
                                                lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                                if lastMessagesList is not None:
                                                    lastMessagesList.append(newMessage)
                                                    if len(lastMessagesList) > 6:
                                                        del lastMessagesList[0]
                                                else:
                                                    lastMessagesList = [newMessage]
                                                lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                            dialogsCache.set(toUserId, cachedDialogsDictDictToUser)
                            except Error:
                                save = False
                        else:
                            try:
                                with atomic():
                                    if lastMessage is None:
                                        lastMessage = personalMessage.objects.filter(inDialog__exact = dialog)
                                        if lastMessage.count() > 0:
                                            lastMessage = lastMessage.last()
                                            if str(lastMessage.by_id) == myId:
                                                newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = False, inDialog = dialog, time = time)
                                            else:
                                                newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = True, inDialog = dialog, time = time)
                                        else:
                                            newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = True, inDialog = dialog, time = time)
                                    else:
                                        if str(lastMessage.by_id) == myId:
                                            newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = False, inDialog = dialog, time = time)
                                        else:
                                            newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = messageTextBody, messageHead = True, inDialog = dialog, time = time)
                                    newMessage.save()
                                    lastMessageCache.set(dialogId, newMessage)
                                    messageIdStr = str(newMessage.id)
                                    if oldUnreadMessagesDictDict is None:
                                        unreadMessagesCache.set(dialogId, {toUserId: {messageIdStr,}})
                                    else:
                                        if oldUnreadMessages is None:
                                            oldUnreadMessagesDictDict.update({toUserId: {messageIdStr,}})
                                            unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                                        else:
                                            oldUnreadMessages.add(messageIdStr)
                                            unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                                    if oldData is not None:
                                        finded = None
                                        for item in oldData:
                                            if dialogId == item[3]:
                                                finded = item
                                                break
                                        if finded is None:
                                            oldData.append(('personalMessage', 'new', myId, dialogId))
                                            newUpdates.set(toUserId, oldData)
                                        else:
                                            if finded[1] == "hidden":
                                                oldData[oldData.index(finded)][1] = "new"
                                                newUpdates.set(toUserId, oldData)
                                                finded = "hidden"
                                    else:
                                        newUpdates.set(toUserId, [('personalMessage', 'new', myId, dialogId)])
                                    if cachedDialogsDictDictMy is not None and cachedDialogsDictDictToUser is not None:
                                        lastMessagesList = None
                                        cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                        if cachedDialogsDictMy is not None:
                                            if dialogId not in cachedDialogsDictMy.keys():
                                                cachedDialogsDictMy.update({dialogId: dialog})
                                            cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                        lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                        if lastMessagesDictListMy is not None:
                                            lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                            if lastMessagesList is not None:
                                                lastMessagesList.append(newMessage)
                                                if len(lastMessagesList) > 6:
                                                    del lastMessagesList[0]
                                            else:
                                                lastMessagesList = [newMessage]
                                            lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                        cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                        if cachedDialogsDictToUser is not None:
                                            if dialogId not in cachedDialogsDictToUser.keys():
                                                cachedDialogsDictToUser.update({dialogId: dialog})
                                            cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                        lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                        if lastMessagesDictListToUser is not None:
                                            if lastMessagesList is None:
                                                lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                                if lastMessagesList is not None:
                                                    lastMessagesList.append(newMessage)
                                                    if len(lastMessagesList) > 6:
                                                        del lastMessagesList[0]
                                                else:
                                                    lastMessagesList = [newMessage]
                                            lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                        dialogsCache.set_many({myId: cachedDialogsDictDictMy, toUserId: cachedDialogsDictDictToUser})
                                    else:
                                        if cachedDialogsDictDictMy is not None:
                                            cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                            if cachedDialogsDictMy is not None:
                                                if dialogId not in cachedDialogsDictMy.keys():
                                                    cachedDialogsDictMy.update({dialogId: dialog})
                                                cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                            lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                            if lastMessagesDictListMy is not None:
                                                lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                                if lastMessagesList is not None:
                                                    lastMessagesList.append(newMessage)
                                                    if len(lastMessagesList) > 6:
                                                        del lastMessagesList[0]
                                                else:
                                                    lastMessagesList = [newMessage]
                                                lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                            dialogsCache.set(myId, cachedDialogsDictDictMy)
                                        elif cachedDialogsDictDictToUser is not None:
                                            cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                            if cachedDialogsDictToUser is not None:
                                                if dialogId not in cachedDialogsDictToUser.keys():
                                                    cachedDialogsDictToUser.update({dialogId: dialog})
                                                cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                            lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                            if lastMessagesDictListToUser is not None:
                                                lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                                if lastMessagesList is not None:
                                                    lastMessagesList.append(newMessage)
                                                    if len(lastMessagesList) > 6:
                                                        del lastMessagesList[0]
                                                else:
                                                    lastMessagesList = [newMessage]
                                                lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                            dialogsCache.set(toUserId, cachedDialogsDictDictToUser)
                            except Error:
                                save = False
                        newImagesAttachment = None
                if save is True:
                    myUsername = curUser.username
                    mySlug = curUser.profileUrl
                    myDialogClass = 'dialogMessage-new-%s' % myId
                    if newImagesAttachment is None:
                        myMessageListDict = [
                        {
                            'method': 'messagePersonal',
                            'dialogLink': reverse("personalMessage", kwargs = {'toUser': toUser}),
                            'id': messageIdStr,
                            'body': newMessage.body,
                            'slug': mySlug,
                            'value': 'dialogMessage-new-%s' % toUserId,
                            'dialogId': dialogId,
                            'byId': myId,
                            'eventType': 'notify',
                            # Дальше для метода messageMany
                            'count': 0,
                            'profileLink': reverse("profile", kwargs = {'currentUser': mySlug}),
                            'username': myUsername,
                            'forgetLink': reverse("deleteNotifyEvent", kwargs = {'type': 'dialogMessage', 'state': 'new', 'senderId': myId})
                            }
                        ]
                        if oldData is not None:
                            if finded is None:
                                messageListDict = copy(myMessageListDict)
                                messageListDict[0].update({
                                'dialogLink': request.path,
                                'value': myDialogClass,
                                'notifyCounterChange': {
                                    'method': 'incrementNotifyValue',
                                    'baseValue': 1,
                                    'multiValue': 1,
                                    'class': myDialogClass
                                    }
                                })
                            else:
                                if finded == "hidden":
                                    messageListDict = copy(myMessageListDict)
                                    messageListDict[0].update({
                                    'dialogLink': request.path,
                                    'value': myDialogClass,
                                    'notifyCounterChange': {
                                        'method': 'incrementNotifyValue',
                                        'baseValue': len(oldUnreadMessages),
                                        'multiValue': 1,
                                        'class': myDialogClass
                                        }
                                    })
                                else:
                                    messageListDict = copy(myMessageListDict)
                                    messageListDict[0].update({
                                    'dialogLink': request.path,
                                    'class': myDialogClass,
                                    'notifyCounterChange': {
                                        'method': 'incrementNotifyValue',
                                        'baseValue': 1,
                                        'class': myDialogClass
                                        }
                                    })
                        else:
                            messageListDict = copy(myMessageListDict)
                            messageListDict[0].update({
                            'dialogLink': request.path,
                            'class': myDialogClass,
                            'notifyCounterChange': {
                                'method': 'incrementNotifyValue',
                                'baseValue': 1,
                                'multiValue': 1,
                                'class': myDialogClass
                                }
                            })
                    else:
                        if cachedFilesDictDictImagesLength <= 3:
                            if cachedFilesDictDictImagesLength == 1:
                                imagesList = [newImagesAttachment.image1.url]
                            elif cachedFilesDictDictImagesLength == 2:
                                imagesList = [
                                    newImagesAttachment.image1.url,
                                    newImagesAttachment.image2.url
                                ]
                            else:
                                imagesList = [
                                    newImagesAttachment.image1.url,
                                    newImagesAttachment.image2.url,
                                    newImagesAttachment.image3.url
                                ]
                        else:
                            if cachedFilesDictDictImagesLength == 4:
                                imagesList = [
                                    newImagesAttachment.image1.url,
                                    newImagesAttachment.image2.url,
                                    newImagesAttachment.image3.url,
                                    newImagesAttachment.image4.url
                                ]
                            elif cachedFilesDictDictImagesLength == 5:
                                imagesList = [
                                    newImagesAttachment.image1.url,
                                    newImagesAttachment.image2.url,
                                    newImagesAttachment.image3.url,
                                    newImagesAttachment.image4.url,
                                    newImagesAttachment.image5.url
                                ]
                            else:
                                imagesList = [
                                    newImagesAttachment.image1.url,
                                    newImagesAttachment.image2.url,
                                    newImagesAttachment.image3.url,
                                    newImagesAttachment.image4.url,
                                    newImagesAttachment.image5.url,
                                    newImagesAttachment.image6.url
                                ]
                        myMessageListDict = [
                        {
                            'method': 'messagePersonal',
                            'dialogLink': reverse("personalMessage", kwargs = {'toUser': toUser}),
                            'id': messageIdStr,
                            'body': newMessage.body,
                            'images': imagesList,
                            'slug': mySlug,
                            'value': 'dialogMessage-new-%s' % toUserId,
                            'dialogId': dialogId,
                            'byId': myId,
                            'eventType': 'notify',
                            # Дальше для метода messageMany
                            'count': 0,
                            'profileLink': reverse("profile", kwargs = {'currentUser': mySlug}),
                            'username': myUsername,
                            'forgetLink': reverse("deleteNotifyEvent", kwargs = {'type': 'dialogMessage', 'state': 'new', 'senderId': myId})
                            }
                        ]
                        if oldData is not None:
                            if finded is None:
                                messageListDict = copy(myMessageListDict)
                                messageListDict[0].update({
                                'dialogLink': request.path,
                                'class': myDialogClass,
                                'notifyCounterChange': {
                                    'method': 'incrementNotifyValue',
                                    'baseValue': 1,
                                    'multiValue': 1,
                                    'class': myDialogClass
                                    }
                                })
                            else:
                                if finded == "hidden":
                                    messageListDict = copy(myMessageListDict)
                                    messageListDict[0].update({
                                    'dialogLink': request.path,
                                    'class': myDialogClass,
                                    'notifyCounterChange': {
                                        'method': 'incrementNotifyValue',
                                        'baseValue': len(oldUnreadMessages),
                                        'multiValue': 1,
                                        'class': myDialogClass
                                        }
                                    })
                                else:
                                    messageListDict = copy(myMessageListDict)
                                    messageListDict[0].update({
                                    'dialogLink': request.path,
                                    'class': myDialogClass,
                                    'notifyCounterChange': {
                                        'method': 'incrementNotifyValue',
                                        'baseValue': 1,
                                        'class': myDialogClass
                                        }
                                    })
                        else:
                            messageListDict = copy(myMessageListDict)
                            messageListDict[0].update({
                            'dialogLink': request.path,
                            'class': myDialogClass,
                            'notifyCounterChange': {
                                'method': 'incrementNotifyValue',
                                'baseValue': 1,
                                'multiValue': 1,
                                'class': myDialogClass
                                }
                            })
                    layer = get_channel_layer().group_send
                    messageListStr =  json.dumps(messageListDict)
                    async_to_sync(layer)(myId, {"type": "sendToClient", "data": {"item": json.dumps(myMessageListDict)}})
                    async_to_sync(layer)(toUserId, {"type": "sendToClient", "data": {"item": messageListStr}})
                    return JsonResponse({"data": messageListStr})
                # Database save error
                return JsonResponse({"data": None})
            # UNVALID FORM <return>
            if existsMessagesInDialog:
                pass
        messagesFilesStorage = caches['filesStorageMessages']
        cachedFilesDict = messagesFilesStorage.get(dialogId, None)
        if cachedFilesDict is not None:
            cachedFilesDictDict = cachedFilesDict.get(myId, None)
            if cachedFilesDictDict is not None:
                cachedFilesDictDictImages = cachedFilesDictDict.get("images", None)
                if cachedFilesDictDictImages is not None:
                    cachedImagesList = list(cachedFilesDictDictImages.values())
                else:
                    cachedImagesList = None
            else:
                cachedImagesList = None
        else:
            cachedImagesList = None
        reqFiles = request.FILES
        if len(reqFiles.keys()) > 0:
            form = sendMessageForm(request.POST, reqFiles)
            filesList = reqFiles.getlist('insertion')
        else:
            form = sendMessageForm(request.POST)
            filesList = None
        curUser = request.user
        myFirstName = None
        myLastName = None
        toUserFirstName = None
        toUserLastName = None
        myListThumbChanged, toUserListThumbChanged = False, False
        listThumbsCache = caches['messagesListThumbs']
        listThumbsManyMany = listThumbsCache.get_many({myId, toUserId})
        myListThumbsDict = listThumbsManyMany.get(myId, None)
        toUserListThumbsDict = listThumbsManyMany.get(toUserId, None)
        myProfilePath = None
        toUserProfilePath = None
        if myListThumbsDict is None:
            myProfilePath = reverse("profile", kwargs = {'currentUser': curUser.profileUrl})
            myListThumbChanged = True
            myFirstName = curUser.first_name or None
            myLastName = curUser.last_name or None
            if myFirstName is not None and myLastName is not None:
                name = '{0} {1}'.format(myFirstName, myLastName)
                smallName = curUser.username
            else:
                if myFirstName is not None:
                    name = myFirstName
                    smallName = curUser.username
                else:
                    name = curUser.username
                    smallName = None
            avatar = curUser.avatarThumbSmaller or None
            if avatar is not None:
                if smallName is not None:
                    myListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><p><a href = "{1}">{3}</a></p><small><a href = "{1}">{4}</a></small></div>'.format(myId, myProfilePath, avatar.url, name, smallName)
                else:
                    myListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><p><a href = "{1}">{3}</a></p></div>'.format(myId, myProfilePath, avatar.url, name)
            else:
                if smallName is not None:
                    myListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><p><a href = "{3}">{1}</a></p><small><a href = "{3}">{2}</a></small></div>'.format(myId, name, smallName, myProfilePath)
                else:
                    myListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><p><a href = "{2}">{1}</a></p></div>'.format(myId, name, myProfilePath)
            myListThumbsDict = {myId: myListThumbItem}
        else:
            myListThumbItemDict = myListThumbsDict.get(myId, None)
            if myListThumbItemDict is None:
                myProfilePath = reverse("profile", kwargs = {'currentUser': curUser.profileUrl})
                myListThumbChanged = True
                myFirstName = curUser.first_name or None
                myLastName = curUser.last_name or None
                if myFirstName and myLastName:
                    name = '{0} {1}'.format(myFirstName, myLastName)
                    smallName = curUser.username
                else:
                    if myFirstName:
                        name = myFirstName
                        smallName = curUser.username
                    else:
                        name = curUser.username
                        smallName = None
                avatar = curUser.avatarThumbSmaller or None
                if avatar is not None:
                    if smallName:
                        myListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><p><a href = "{1}">{3}</a></p><small><a href = "{1}">{4}</a></small></div>'.format(myId, myProfilePath, avatar.url, name, smallName)
                    else:
                        myListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><p><a href = "{1}">{3}</a></p></div>'.format(myId, myProfilePath, avatar.url, name)
                else:
                    if smallName:
                        myListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><p><a href = "{3}">{1}</a></p><small><a href = "{3}">{2}</a></small></div>'.format(myId, name, smallName, myProfilePath)
                    else:
                        myListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><p><a href = "{2}">{1}</a></p></div>'.format(myId, name, myProfilePath)
                myListThumbsDict.update({myId: myListThumbItem})
            else:
                myListThumbItem = myListThumbItemDict
        if toUserListThumbsDict is None:
            toUserProfilePath = reverse("profile", kwargs = {'currentUser': toUserInstance.profileUrl})
            toUserListThumbChanged = True
            if viewMainInfo:
                toUserFirstName = toUserInstance.first_name or None
                toUserLastName = toUserInstance.last_name or None
                if toUserFirstName and toUserLastName:
                    name = '{0} {1}'.format(toUserFirstName, toUserLastName)
                    smallName = toUserInstance.username
                else:
                    if toUserFirstName:
                        name = toUserFirstName
                        smallName = toUserInstance.username
                    else:
                        name = toUserInstance.username
                        smallName = None
            else:
                name = toUserInstance.username
                smallName = None
            if viewAvatar:
                avatar = toUserInstance.avatarThumbSmaller or None
                if avatar is not None:
                    if smallName:
                        toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><p><a href = "{1}">{3}</a></p><small><a href = "{1}">{4}</a></small></div>'.format(toUserId, toUserProfilePath, avatar.url, name, smallName)
                    else:
                        toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><p><a href = "{1}">{3}</a></p></div>'.format(toUserId, toUserProfilePath, avatar.url, name)
                else:
                    if smallName:
                        toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><p><a href = "{3}">{1}</a></p><small><a href = "{3}">{2}</a></small></div>'.format(toUserId, name, smallName, toUserProfilePath)
                    else:
                        toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><p><a href = "{2}">{1}</a></p></div>'.format(toUserId, name, toUserProfilePath)
            else:
                if smallName:
                    toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><p><a href = "{3}">{1}</a></p><small><a href = "{3}">{2}</a></small></div>'.format(toUserId, name, smallName, toUserProfilePath)
                else:
                    toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><p><a href = "{2}">{1}</a></p></div>'.format(toUserId, name, toUserProfilePath)
            toUserListThumbsDict = {myId: toUserListThumbItem}
        else:
            toUserListThumbItemDict = toUserListThumbsDict.get(myId, None)
            if toUserListThumbItemDict is None:
                toUserProfilePath = reverse("profile", kwargs = {'currentUser': toUserInstance.profileUrl})
                toUserListThumbChanged = True
                if viewMainInfo:
                    toUserFirstName = toUserInstance.first_name or None
                    toUserLastName = toUserInstance.last_name or None
                    if toUserFirstName and toUserLastName:
                        name = '{0} {1}'.format(toUserFirstName, toUserLastName)
                        smallName = toUserInstance.username
                    else:
                        if toUserFirstName:
                            name = toUserFirstName
                            smallName = toUserInstance.username
                        else:
                            name = toUserInstance.username
                            smallName = None
                else:
                    name = toUserInstance.username
                    smallName = None
                if viewAvatar:
                    avatar = toUserInstance.avatarThumbSmaller or None
                    if avatar is not None:
                        if smallName:
                            toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><p><a href = "{1}">{3}</a></p><small><a href = "{1}">{4}</a></small></div>'.format(toUserId, toUserProfilePath, avatar.url, name, smallName)
                        else:
                            toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><p><a href = "{1}">{3}</a></p></div>'.format(toUserId, toUserProfilePath, avatar.url, name)
                    else:
                        if smallName:
                            toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><p><a href = "{3}">{1}</a></p><small><a href = "{3}">{2}</a></small></div>'.format(toUserId, name, smallName, toUserProfilePath)
                        else:
                            toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><p><a href = "{2}">{1}</a></p></div>'.format(toUserId, name, toUserProfilePath)
                else:
                    if smallName:
                        toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><p><a href = "{3}">{1}</a></p><small><a href = "{3}">{2}</a></small></div>'.format(toUserId, name, smallName, toUserProfilePath)
                    else:
                        toUserListThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><p><a href = "{2}">{1}</a></p></div>'.format(toUserId, name, toUserProfilePath)
                toUserListThumbsDict.update({myId: toUserListThumbItem})
            else:
                toUserListThumbItem = toUserListThumbItemDict
        if myListThumbChanged and toUserListThumbChanged:
            listThumbsCache.set_many({myId: myListThumbsDict, toUserId: toUserListThumbsDict})
        else:
            if myListThumbChanged:
                listThumbsCache.set(myId, myListThumbsDict)
            elif toUserListThumbChanged:
                listThumbsCache.set(toUserId, toUserListThumbsDict)
        onlineStatusText = None
        activeStatusText = None
        myMainThumbChanged, toUserMainThumbChanged = False, False
        mainThumbsCache = caches['dialogThumbs']
        mainThumbsManyMany = mainThumbsCache.get_many({myId, toUserId})
        myMainThumbsDict = mainThumbsManyMany.get(myId, None)
        toUserMainThumbsDict = mainThumbsManyMany.get(toUserId, None)
        javaScriptStatus = request.COOKIES.get('js', None)
        if javaScriptStatus is not None:
            if myMainThumbsDict is None:
                myMainThumbChanged = True
                if myFirstName is None:
                    myFirstName = curUser.first_name or None
                if myLastName is None:
                    myLastName = curUser.last_name or None
                if myFirstName is not None and myLastName is not None:
                    name = '{0} {1}'.format(myFirstName, myLastName)
                    smallName = curUser.username
                else:
                    if myFirstName:
                        name = myFirstName
                        smallName = curUser.username
                    else:
                        name = curUser.username
                        smallName = None
                avatar = curUser.avatarThumbMiddleSmall or None
                if avatar is not None:
                    if smallName:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(myId, myProfilePath, avatar.url, name, smallName)
                    else:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(myId, myProfilePath, avatar.url, name)
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(myId, name, smallName, myProfilePath)
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(myId, name, myProfilePath)
                myMainThumb = (itemString0, '</div>')
                myMainThumbsDict = {myId: {'item': myMainThumb, 'showOnlineStatus': True, 'showActiveStatus': True}}
            else:
                myMainThumb = myMainThumbsDict.get(myId, None)
                if myMainThumb is None:
                    myMainThumbChanged = True
                    if myFirstName is None:
                        myFirstName = curUser.first_name or None
                    if myLastName is None:
                        myLastName = curUser.last_name or None
                    if myFirstName and myLastName:
                        name = '{0} {1}'.format(myFirstName, myLastName)
                        smallName = curUser.username
                    else:
                        if myFirstName:
                            name = myFirstName
                            smallName = curUser.username
                        else:
                            name = curUser.username
                            smallName = None
                    avatar = curUser.avatarThumbMiddleSmall or None
                    if avatar is not None:
                        if smallName:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(myId, myProfilePath, avatar.url, name, smallName)
                        else:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(myId, myProfilePath, avatar.url, name)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(myId, name, smallName, myProfilePath)
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(myId, name, myProfilePath)
                    myMainThumb = (itemString0, '</div>')
                    myMainThumbsDict.update({myId: {'item': myMainThumb, 'showOnlineStatus': True}})
                else:
                    myMainThumb = myMainThumb['item']
            if toUserMainThumbsDict is None:
                toUserMainThumbChanged = True
                if viewMainInfo:
                    if toUserFirstName is None:
                        toUserFirstName = toUserInstance.first_name or None
                    if toUserLastName is None:
                        toUserLastName = toUserInstance.last_name or None
                    if toUserFirstName and toUserLastName:
                        name = '{0} {1}'.format(toUserFirstName, toUserLastName)
                        smallName = toUserInstance.username
                    else:
                        if toUserFirstName:
                            name = toUserFirstName
                            smallName = toUserInstance.username
                        else:
                            name = toUserInstance.username
                            smallName = None
                else:
                    name = toUserInstance.username
                    smallName = None
                if viewAvatar:
                    avatar = toUserInstance.avatarThumbMiddleSmall or None
                    if avatar is not None:
                        if smallName:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(toUserId, toUserProfilePath, avatar.url, name, smallName)
                        else:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(toUserId, toUserProfilePath, avatar.url, name)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                toUserMainThumb = (itemString0, '</div>')
                toUserMainThumbsDict = {myId: {'item': toUserMainThumb, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus}}
            else:
                toUserMainThumb = toUserMainThumbsDict.get(myId, None)
                if toUserMainThumb is None:
                    toUserMainThumbChanged = True
                    if viewMainInfo:
                        if toUserFirstName is None:
                            toUserFirstName = toUserInstance.first_name or None
                        if toUserLastName is None:
                            toUserLastName = toUserInstance.last_name or None
                        if toUserFirstName and toUserLastName:
                            name = '{0} {1}'.format(toUserFirstName, toUserLastName)
                            smallName = toUserInstance.username
                        else:
                            if toUserFirstName:
                                name = toUserFirstName
                                smallName = toUserInstance.username
                            else:
                                name = toUserInstance.username
                                smallName = None
                    else:
                        name = toUserInstance.username
                        smallName = None
                    if viewAvatar:
                        avatar = toUserInstance.avatarThumbMiddleSmall or None
                        if avatar is not None:
                            if smallName:
                                itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(toUserId, toUserProfilePath, avatar.url, name, smallName)
                            else:
                                itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(toUserId, toUserProfilePath, avatar.url, name)
                        else:
                            if smallName:
                                itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                            else:
                                itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                    toUserMainThumb = (itemString0, '</div>')
                    toUserMainThumbsDict = {myId: {'item': toUserMainThumb, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus}}
                else:
                    toUserMainThumb = toUserMainThumb.pop('item')
            if viewOnlineStatus:
                if caches['onlineUsers'].get(toUserId, None) is not None:
                    onlineStatusDict = {toUserId: 'online'}
                    if viewActiveStatus:
                        if caches['chatStatusActive'].get(toUserId, None) is not None:
                            activeStatusDict = {toUserId: 'stateActive'}
                        else:
                            middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                            if middleDataTuple is not None:
                                activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                            else:
                                activeStatusDict = None
                    else:
                        activeStatusDict = None
                else:
                    iffyTimeStr = caches['iffyUsersOnlineStatus'].get(toUserId, None)
                    if iffyTimeStr is not None:
                        onlineStatusDict = {toUserId: {'iffy': iffyTimeStr}}
                        if viewActiveStatus:
                            if caches['chatStatusActive'].get(toUserId, None) is not None:
                                activeStatusDict = {toUserId: 'stateActive'}
                            else:
                                middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                if middleDataTuple is not None:
                                    activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                else:
                                    activeStatusDict = None
                        else:
                            activeStatusDict = None
                    else:
                        onlineStatusDict = None
                        activeStatusDict = None
            else:
                onlineStatusDict = None
                if viewActiveStatus:
                    if caches['chatStatusActive'].get(toUserId, None) is not None:
                        activeStatusDict = {toUserId: 'stateActive'}
                    else:
                        middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                        if middleDataTuple is not None:
                            activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                        else:
                            activeStatusDict = None
                else:
                    activeStatusDict = None
        else:
            onlineStatusText = None
            activeStatusText = None
            if myMainThumbsDict is None:
                myMainThumbChanged = True
                if myFirstName is None:
                    myFirstName = curUser.first_name or None
                if myLastName is None:
                    myLastName = curUser.last_name or None
                if myFirstName is not None and myLastName is not None:
                    name = '{0} {1}'.format(myFirstName, myLastName)
                    smallName = curUser.username
                else:
                    if myFirstName:
                        name = myFirstName
                        smallName = curUser.username
                    else:
                        name = curUser.username
                        smallName = None
                avatar = curUser.avatarThumbMiddleSmall or None
                if avatar is not None:
                    if smallName:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(myId, myProfilePath, avatar.url, name, smallName)
                    else:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(myId, myProfilePath, avatar.url, name)
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(myId, name, smallName, myProfilePath)
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(myId, name, myProfilePath)
                itemString1 = '</div>'
                myMainThumbsDict = {myId: {'item': (itemString0, itemString1), 'showOnlineStatus': True, 'showActiveStatus': True}}
                textDict = usersListText(lang = 'ru').getDict()
                onlineStatusText = textDict['onlineStatus']
                activeStatusText = textDict['activeStatus']
                myMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), itemString1)
            else:
                myMainThumb = myMainThumbsDict.get(myId, None)
                if myMainThumb is None:
                    myMainThumbChanged = True
                    if myFirstName is None:
                        myFirstName = curUser.first_name or None
                    if myLastName is None:
                        myLastName = curUser.last_name or None
                    if myFirstName and myLastName:
                        name = '{0} {1}'.format(myFirstName, myLastName)
                        smallName = curUser.username
                    else:
                        if myFirstName:
                            name = myFirstName
                            smallName = curUser.username
                        else:
                            name = curUser.username
                            smallName = None
                    avatar = curUser.avatarThumbMiddleSmall or None
                    if avatar is not None:
                        if smallName:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(myId, myProfilePath, avatar.url, name, smallName)
                        else:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(myId, myProfilePath, avatar.url, name)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(myId, name, smallName, myProfilePath)
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(myId, name, myProfilePath)
                    itemString1 = '</div>'
                    myMainThumbsDict.update({myId: {'item': (itemString0, itemString1), 'showOnlineStatus': True, 'showActiveStatus': True}})
                    textDict = usersListText(lang = 'ru').getDict()
                    onlineStatusText = textDict['onlineStatus']
                    activeStatusText = textDict['activeStatus']
                    myMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), itemString1)
                else:
                    textDict = usersListText(lang = 'ru').getDict()
                    onlineStatusText = textDict['onlineStatus']
                    activeStatusText = textDict['activeStatus']
                    myMainThumb = myMainThumb['item']
                    myMainThumb = (myMainThumb[0], '<noscript><p class = "online">{0}</p><br><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), myMainThumb[1])
            if toUserMainThumbsDict is None:
                toUserMainThumbChanged = True
                if viewMainInfo:
                    if toUserFirstName is None:
                        toUserFirstName = toUserInstance.first_name or None
                    if toUserLastName is None:
                        toUserLastName = toUserInstance.last_name or None
                    if toUserFirstName and toUserLastName:
                        name = '{0} {1}'.format(toUserFirstName, toUserLastName)
                        smallName = toUserInstance.username
                    else:
                        if toUserFirstName:
                            name = toUserFirstName
                            smallName = toUserInstance.username
                        else:
                            name = toUserInstance.username
                            smallName = None
                else:
                    name = toUserInstance.username
                    smallName = None
                if viewAvatar:
                    avatar = toUserInstance.avatarThumbMiddleSmall or None
                    if avatar is not None:
                        if smallName:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(toUserId, toUserProfilePath, avatar.url, name, smallName)
                        else:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(toUserId, toUserProfilePath, avatar.url, name)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                itemString1 = '</div>'
                itemTuple = (itemString0, itemString1)
                toUserMainThumb = {'item': itemTuple, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus}
                toUserMainThumbsDict = {myId: toUserMainThumb}
                if viewOnlineStatus:
                    if caches['onlineUsers'].get(toUserId, None) is not None:
                        if onlineStatusText is None:
                            onlineStatusText = textDict['onlineStatus']
                        if viewActiveStatus:
                            if caches['chatStatusActive'].get(toUserId, None) is not None:
                                if activeStatusText is None:
                                    activeStatusText = textDict['activeStatus']
                                toUserMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), itemString1)
                                activeStatusDict.update({toUserId: 'stateActive'})
                            else:
                                middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                if middleDataTuple is not None:
                                    middleStatusText = textDict['middleStatus']
                                    toUserMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "middle">{1}</p></noscript>'.format(onlineStatusText, middleStatusText), itemString1)
                                    activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                else:
                                    leaveStatusText = textDict['leaveStatus']
                                    toUserMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "leave">{1}</p></noscript>'.format(onlineStatusText, leaveStatusText), itemString1)
                                    activeStatusDict = None
                        else:
                            activeStatusDict = None
                        onlineStatusDict = {toUserId: 'online'}
                    else:
                        iffyTimeStr = caches['iffyUsersOnlineStatus'].get(toUserId, None)
                        if iffyTimeStr is not None:
                            iffyStatusText = textDict['uknownStatus']
                            if viewActiveStatus:
                                if caches['chatStatusActive'].get(toUserId, None) is not None:
                                    if activeStatusText is None:
                                        activeStatusText = textDict['activeStatus']
                                    toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p><br><p class = "active">{1}</p></noscript>'.format(iffyStatusText, activeStatusText), itemString1)
                                    activeStatusDict = {toUserId: 'stateActive'}
                                else:
                                    middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                    if middleDataTuple is not None:
                                        middleStatusText = textDict['middleStatus']
                                        toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p><br><p class = "middle">{1}</p></noscript>'.format(iffyStatusText, middleStatusText), itemString1)
                                        activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                    else:
                                        leaveStatusText = textDict['leaveStatus']
                                        toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p><br><p class = "leave">{1}</p></noscript>'.format(iffyStatusText, leaveStatusText), itemString1)
                                        activeStatusDict = None
                            else:
                                toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText), itemString1)
                                activeStatusDict = None
                            toUserIffy = iffyStatusCache.get(toUserId, None)
                            if toUserIffy is not None:
                                onlineStatusDict = {toUserId: {'iffy': toUserIffy}}
                        else:
                            onlineStatusDict = None
                            activeStatusDict = None
                            toUserMainThumb = itemTuple
                else:
                    onlineStatusDict = None
                    if viewActiveStatus:
                        if caches['chatStatusActive'].get(toUserId, None) is not None:
                            if activeStatusText is None:
                                activeStatusText = textDict['activeStatus']
                            toUserMainThumb = (itemString0, '<noscript><p class = "active">{0}</p></noscript>'.format(activeStatusText), itemString1)
                            activeStatusDict = {toUserId: 'stateActive'}
                        else:
                            middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                            if middleDataTuple is not None:
                                middleStatusText = textDict['middleStatus']
                                toUserMainThumb = (itemString0, '<noscript><p class = "middle">{0}</p></noscript>'.format(middleStatusText), itemString1)
                                activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                            else:
                                toUserMainThumb = itemTuple
                                activeStatusDict = None
                    else:
                        toUserMainThumb = itemTuple
                        activeStatusDict = None
            else:
                toUserMainThumb = toUserMainThumbsDict.get(myId, None)
                if toUserMainThumb is None:
                    toUserMainThumbChanged = True
                    if viewMainInfo:
                        if toUserFirstName is None:
                            toUserFirstName = toUserInstance.first_name or None
                        if toUserLastName is None:
                            toUserLastName = toUserInstance.last_name or None
                        if toUserFirstName and toUserLastName:
                            name = '{0} {1}'.format(toUserFirstName, toUserLastName)
                            smallName = toUserInstance.username
                        else:
                            if toUserFirstName:
                                name = toUserFirstName
                                smallName = toUserInstance.username
                            else:
                                name = toUserInstance.username
                                smallName = None
                    else:
                        name = toUserInstance.username
                        smallName = None
                    if viewAvatar:
                        avatar = toUserInstance.avatarThumbMiddleSmall or None
                        if avatar is not None:
                            if smallName:
                                itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(toUserId, toUserProfilePath, avatar.url, name, smallName)
                            else:
                                itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(toUserId, toUserProfilePath, avatar.url, name)
                        else:
                            if smallName:
                                itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                            else:
                                itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(toUserId, name, smallName, toUserProfilePath)
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(toUserId, name, toUserProfilePath)
                    itemString1 = '</div>'
                    itemTuple = (itemString0, itemString1)
                    toUserMainThumb = {'item': itemTuple, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus}
                    toUserMainThumbsDict.update({myId: toUserMainThumb})
                    if viewOnlineStatus:
                        if caches['onlineUsers'].get(toUserId, None) is not None:
                            if onlineStatusText is None:
                                onlineStatusText = textDict['onlineStatus']
                            if viewActiveStatus:
                                if caches['chatStatusActive'].get(toUserId, None) is not None:
                                    if activeStatusText is None:
                                        activeStatusText = textDict['activeStatus']
                                    toUserMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), itemString1)
                                    activeStatusDict.update({toUserId: 'stateActive'})
                                else:
                                    middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                    if middleDataTuple is not None:
                                        middleStatusText = textDict['middleStatus']
                                        toUserMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "middle">{1}</p></noscript>'.format(onlineStatusText, middleStatusText), itemString1)
                                        activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                    else:
                                        leaveStatusText = textDict['leaveStatus']
                                        toUserMainThumb = (itemString0, '<noscript><p class = "online">{0}</p><br><p class = "leave">{1}</p></noscript>'.format(onlineStatusText, leaveStatusText), itemString1)
                                        activeStatusDict = None
                            else:
                                activeStatusDict = None
                            onlineStatusDict = {toUserId: 'online'}
                        else:
                            iffyTimeStr = caches['iffyUsersOnlineStatus'].get(toUserId, None)
                            if iffyTimeStr is not None:
                                iffyStatusText = textDict['uknownStatus']
                                if viewActiveStatus:
                                    if caches['chatStatusActive'].get(toUserId, None) is not None:
                                        if activeStatusText is None:
                                            activeStatusText = textDict['activeStatus']
                                        toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p><br><p class = "active">{1}</p></noscript>'.format(iffyStatusText, activeStatusText), itemString1)
                                        activeStatusDict = {toUserId: 'stateActive'}
                                    else:
                                        middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                        if middleDataTuple is not None:
                                            middleStatusText = textDict['middleStatus']
                                            toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p><br><p class = "middle">{1}</p></noscript>'.format(iffyStatusText, middleStatusText), itemString1)
                                            activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                        else:
                                            leaveStatusText = textDict['leaveStatus']
                                            toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p><br><p class = "leave">{1}</p></noscript>'.format(iffyStatusText, leaveStatusText), itemString1)
                                            activeStatusDict = None
                                else:
                                    toUserMainThumb = (itemString0, '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText), itemString1)
                                    activeStatusDict = None
                                toUserIffy = iffyStatusCache.get(toUserId, None)
                                if toUserIffy is not None:
                                    onlineStatusDict = {toUserId: {'iffy': toUserIffy}}
                            else:
                                onlineStatusDict = None
                                activeStatusDict = None
                                toUserMainThumb = itemTuple
                    else:
                        onlineStatusDict = None
                        if viewActiveStatus:
                            if caches['chatStatusActive'].get(toUserId, None) is not None:
                                if activeStatusText is None:
                                    activeStatusText = textDict['activeStatus']
                                toUserMainThumb = (itemString0, '<noscript><p class = "active">{0}</p></noscript>'.format(activeStatusText), itemString1)
                                activeStatusDict = {toUserId: 'stateActive'}
                            else:
                                middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                if middleDataTuple is not None:
                                    middleStatusText = textDict['middleStatus']
                                    toUserMainThumb = (itemString0, '<noscript><p class = "middle">{0}</p></noscript>'.format(middleStatusText), itemString1)
                                    activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                else:
                                    toUserMainThumb = itemTuple
                                    activeStatusDict = None
                        else:
                            toUserMainThumb = itemTuple
                            activeStatusDict = None
                else:
                    toUserMainThumb = toUserMainThumb['item']
                    if viewOnlineStatus:
                        if caches['onlineUsers'].get(toUserId, None) is not None:
                            if onlineStatusText is None:
                                onlineStatusText = textDict['onlineStatus']
                            if viewActiveStatus:
                                if caches['chatStatusActive'].get(toUserId, None) is not None:
                                    if activeStatusText is None:
                                        activeStatusText = textDict['activeStatus']
                                    toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "online">{0}</p><br><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), toUserMainThumb[1])
                                    activeStatusDict.update({toUserId: 'stateActive'})
                                else:
                                    middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                    if middleDataTuple is not None:
                                        middleStatusText = textDict['middleStatus']
                                        toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "online">{0}</p><br><p class = "middle">{1}</p></noscript>'.format(onlineStatusText, middleStatusText), toUserMainThumb[1])
                                        activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                    else:
                                        leaveStatusText = textDict['leaveStatus']
                                        toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "online">{0}</p><br><p class = "leave">{1}</p></noscript>'.format(onlineStatusText, leaveStatusText), toUserMainThumb[1])
                                        activeStatusDict = None
                            else:
                                activeStatusDict = None
                            onlineStatusDict = {toUserId: 'online'}
                        else:
                            iffyTimeStr = caches['iffyUsersOnlineStatus'].get(toUserId, None)
                            if iffyTimeStr is not None:
                                iffyStatusText = textDict['uknownStatus']
                                if viewActiveStatus:
                                    if caches['chatStatusActive'].get(toUserId, None) is not None:
                                        if activeStatusText is None:
                                            activeStatusText = textDict['activeStatus']
                                        toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "iffy">{0}</p><br><p class = "active">{1}</p></noscript>'.format(iffyStatusText, activeStatusText), toUserMainThumb[1])
                                        activeStatusDict = {toUserId: 'stateActive'}
                                    else:
                                        middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                        if middleDataTuple is not None:
                                            middleStatusText = textDict['middleStatus']
                                            toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "iffy">{0}</p><br><p class = "middle">{1}</p></noscript>'.format(iffyStatusText, middleStatusText), toUserMainThumb[1])
                                            activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                        else:
                                            leaveStatusText = textDict['leaveStatus']
                                            toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "iffy">{0}</p><br><p class = "leave">{1}</p></noscript>'.format(iffyStatusText, leaveStatusText), toUserMainThumb[1])
                                            activeStatusDict = None
                                else:
                                    toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText), toUserMainThumb[1])
                                    activeStatusDict = None
                                toUserIffy = iffyStatusCache.get(toUserId, None)
                                if toUserIffy is not None:
                                    onlineStatusDict = {toUserId: {'iffy': toUserIffy}}
                            else:
                                onlineStatusDict = None
                                activeStatusDict = None
                    else:
                        onlineStatusDict = None
                        if viewActiveStatus:
                            if caches['chatStatusActive'].get(toUserId, None) is not None:
                                if activeStatusText is None:
                                    activeStatusText = textDict['activeStatus']
                                toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "active">{0}</p></noscript>'.format(activeStatusText), toUserMainThumb[1])
                                activeStatusDict = {toUserId: 'stateActive'}
                            else:
                                middleDataTuple = caches['chatStatusMiddle'].get(toUserId, None)
                                if middleDataTuple is not None:
                                    middleStatusText = textDict['middleStatus']
                                    toUserMainThumb = (toUserMainThumb[0], '<noscript><p class = "middle">{0}</p></noscript>'.format(middleStatusText), toUserMainThumb[1])
                                    activeStatusDict = {toUserId: {'stateMiddle': middleDataTuple}}
                                else:
                                    activeStatusDict = None
                        else:
                            activeStatusDict = None
        if myMainThumbChanged or toUserMainThumbChanged:
            if myMainThumbChanged and toUserMainThumbChanged:
                mainThumbsCache.set_many({myId: myMainThumbsDict, toUserId: toUserMainThumbsDict})
            else:
                if myMainThumbChanged:
                    mainThumbsCache.set(myId, myMainThumbsDict)
                else:
                    mainThumbsCache.set(toUserId, toUserMainThumbsDict)
        if form.is_valid():
            myUsername = curUser.username
            mySlug = curUser.profileUrl
            data = form.cleaned_data
            lastMessageCache = caches['lastMessage']
            lastMessage = lastMessageCache.get(dialogId, None)
            dialogsCache = caches["dialogsCache"]
            cachedDialogsMany = dialogsCache.get_many({myId, toUserId})
            cachedDialogsDictDictMy = cachedDialogsMany.get(myId, None)
            cachedDialogsDictDictToUser = cachedDialogsMany.get(toUserId, None)
            newUpdates = caches["newUpdates"]
            oldData = newUpdates.get(toUserId, None)
            save = True
            unreadMessagesCache = caches["unreadPersonalMessages"]
            oldUnreadMessagesDictDict = unreadMessagesCache.get(dialogId, None)
            if oldUnreadMessagesDictDict is not None:
                oldUnreadMessages = oldUnreadMessagesDictDict.get(toUserId, None)
            if time is None:
                time = datetime.now(tz = tz.gettz())
            if existsMessagesInDialog is False:
                if filesList is not None or cachedImagesList is not None:
                    if filesList is not None and cachedImagesList is not None:
                        imagesToSave = cachedImagesList + filesList
                    elif cachedImagesList is None:
                        imagesToSave = filesList
                    else:
                        imagesToSave = cachedImagesList
                    try:
                        with atomic():
                            if lastMessage is None:
                                lastMessage = personalMessage.objects.filter(inDialog__exact = dialog)
                                if lastMessage.count() > 0:
                                    lastMessage = lastMessage.last()
                                    if str(lastMessage.by_id) == myId:
                                        newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = False, inDialog = dialog, time = time)
                                    else:
                                        newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = True, inDialog = dialog, time = time)
                                else:
                                    newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = True, inDialog = dialog, time = time)
                            else:
                                if str(lastMessage.by_id) == myId:
                                    newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = False, inDialog = dialog, time = time)
                                else:
                                    newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = True, inDialog = dialog, time = time)
                            newImagesAttachment = imagesAttachmentMessagePersonal.objects.create(message = newMessage, by = curUser, itemsLength = 0)
                            cachedImagesLength = len(imagesToSave)
                            if cachedImagesLength <= 3:
                                if cachedImagesLength == 1:
                                    newImagesAttachment.image1 = imagesToSave[0]
                                elif cachedImagesLength == 2:
                                    newImagesAttachment.image1 = imagesToSave[0]
                                    newImagesAttachment.image2 = imagesToSave[1]
                                else:
                                    newImagesAttachment.image1 = imagesToSave[0]
                                    newImagesAttachment.image2 = imagesToSave[1]
                                    newImagesAttachment.image3 = imagesToSave[2]
                            else:
                                if cachedImagesLength == 4:
                                    newImagesAttachment.image1 = imagesToSave[0]
                                    newImagesAttachment.image2 = imagesToSave[1]
                                    newImagesAttachment.image3 = imagesToSave[2]
                                    newImagesAttachment.image4 = imagesToSave[3]
                                elif cachedImagesLength == 5:
                                    newImagesAttachment.image1 = imagesToSave[0]
                                    newImagesAttachment.image2 = imagesToSave[1]
                                    newImagesAttachment.image3 = imagesToSave[2]
                                    newImagesAttachment.image4 = imagesToSave[3]
                                    newImagesAttachment.image5 = imagesToSave[4]
                                else:
                                    newImagesAttachment.image1 = imagesToSave[0]
                                    newImagesAttachment.image2 = imagesToSave[1]
                                    newImagesAttachment.image3 = imagesToSave[2]
                                    newImagesAttachment.image4 = imagesToSave[3]
                                    newImagesAttachment.image5 = imagesToSave[4]
                                    newImagesAttachment.image6 = imagesToSave[5]
                            newImagesAttachment.itemsLength = cachedImagesLength
                            newMessage.imagesAttachment = newImagesAttachment
                            dialog.save()
                            newMessage.save()
                            newImagesAttachment.save()
                            lastMessageCache.set(dialogId, newMessage)
                            messageIdStr = str(newMessage.id)
                            if oldUnreadMessagesDictDict is None:
                                unreadMessagesCache.set(dialogId, {toUserId: {messageIdStr,}})
                            else:
                                if oldUnreadMessages is None:
                                    oldUnreadMessagesDictDict.update({toUserId: {messageIdStr,}})
                                    unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                                else:
                                    oldUnreadMessages.add(messageIdStr)
                                    unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                            if oldData is not None:
                                finded = None
                                for item in oldData:
                                    if dialogId == item[3]:
                                        finded = item
                                        break
                                if finded is None:
                                    oldData.append(('personalMessage', 'new', myId, dialogId))
                                    newUpdates.set(toUserId, oldData)
                                else:
                                    if finded[1] == "hidden":
                                        oldData[oldData.index(finded)][1] = "new"
                                        newUpdates.set(toUserId, oldData)
                                        finded = "hidden"
                            else:
                                newUpdates.set(toUserId, [('personalMessage', 'new', myId, dialogId)])
                            if cachedDialogsDictDictMy is not None and cachedDialogsDictDictToUser is not None:
                                lastMessagesList = None
                                cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                if cachedDialogsDictMy is not None:
                                    if dialogId not in cachedDialogsDictMy.keys():
                                        cachedDialogsDictMy.update({dialogId: dialog})
                                    cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                if lastMessagesDictListMy is not None:
                                    lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                    if lastMessagesList is not None:
                                        lastMessagesList.append(newMessage)
                                        if len(lastMessagesList) > 6:
                                            del lastMessagesList[0]
                                    else:
                                        lastMessagesList = [newMessage]
                                    lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                if cachedDialogsDictToUser is not None:
                                    if dialogId not in cachedDialogsDictToUser.keys():
                                        cachedDialogsDictToUser.update({dialogId: dialog})
                                    cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                if lastMessagesDictListToUser is not None:
                                    if lastMessagesList is None:
                                        lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                        if lastMessagesList is not None:
                                            lastMessagesList.append(newMessage)
                                            if len(lastMessagesList) > 6:
                                                del lastMessagesList[0]
                                        else:
                                            lastMessagesList = [newMessage]
                                    lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                dialogsCache.set_many({myId: cachedDialogsDictDictMy, toUserId: cachedDialogsDictDictToUser})
                            else:
                                if cachedDialogsDictDictMy is not None:
                                    cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                    if cachedDialogsDictMy is not None:
                                        if dialogId not in cachedDialogsDictMy.keys():
                                            cachedDialogsDictMy.update({dialogId: dialog})
                                        cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                    lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                    if lastMessagesDictListMy is not None:
                                        lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                        if lastMessagesList is not None:
                                            lastMessagesList.append(newMessage)
                                            if len(lastMessagesList) > 6:
                                                del lastMessagesList[0]
                                        else:
                                            lastMessagesList = [newMessage]
                                        lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                    dialogsCache.set(myId, cachedDialogsDictDictMy)
                                elif cachedDialogsDictDictToUser is not None:
                                    cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                    if cachedDialogsDictToUser is not None:
                                        if dialogId not in cachedDialogsDictToUser.keys():
                                            cachedDialogsDictToUser.update({dialogId: dialog})
                                        cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                    lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                    if lastMessagesDictListToUser is not None:
                                        lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                        if lastMessagesList is not None:
                                            lastMessagesList.append(newMessage)
                                            if len(lastMessagesList) > 6:
                                                del lastMessagesList[0]
                                        else:
                                            lastMessagesList = [newMessage]
                                        lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                    dialogsCache.set(toUserId, cachedDialogsDictDictToUser)
                    except Error:
                        save = False
                    if save is True:
                        if cachedImagesList is not None:
                            del cachedFilesDict[myId]
                            messagesFilesStorage.set(dialogId, cachedFilesDict)
                else:
                    try:
                        with atomic():
                            if lastMessage is None:
                                lastMessage = personalMessage.objects.filter(inDialog__exact = dialog)
                                if lastMessage.count() > 0:
                                    lastMessage = lastMessage.last()
                                    if str(lastMessage.by_id) == myId:
                                        newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = False, inDialog = dialog, time = time)
                                    else:
                                        newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = True, inDialog = dialog, time = time)
                                else:
                                    newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = True, inDialog = dialog, time = time)
                            else:
                                if str(lastMessage.by_id) == myId:
                                    newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = False, inDialog = dialog, time = time)
                                else:
                                    newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = True, inDialog = dialog, time = time)
                            dialog.save()
                            newMessage.save()
                            lastMessageCache.set(dialogId, newMessage)
                            messageIdStr = str(newMessage.id)
                            if oldUnreadMessagesDictDict is None:
                                unreadMessagesCache.set(dialogId, {toUserId: {messageIdStr,}})
                            else:
                                if oldUnreadMessages is None:
                                    oldUnreadMessagesDictDict.update({toUserId: {messageIdStr,}})
                                    unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                                else:
                                    oldUnreadMessages.add(messageIdStr)
                                    unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                            if oldData is not None:
                                finded = None
                                for item in oldData:
                                    if dialogId == item[3]:
                                        finded = item
                                        break
                                if finded is None:
                                    oldData.append(('personalMessage', 'new', myId, dialogId))
                                    newUpdates.set(toUserId, oldData)
                                else:
                                    if finded[1] == "hidden":
                                        oldData[oldData.index(finded)][1] = "new"
                                        newUpdates.set(toUserId, oldData)
                                        finded = "hidden"
                            else:
                                newUpdates.set(toUserId, [('personalMessage', 'new', myId, dialogId)])
                            if cachedDialogsDictDictMy is not None and cachedDialogsDictDictToUser is not None:
                                lastMessagesList = None
                                cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                if cachedDialogsDictMy is not None:
                                    if dialogId not in cachedDialogsDictMy.keys():
                                        cachedDialogsDictMy.update({dialogId: dialog})
                                    cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                if lastMessagesDictListMy is not None:
                                    lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                    if lastMessagesList is not None:
                                        lastMessagesList.append(newMessage)
                                        if len(lastMessagesList) > 6:
                                            del lastMessagesList[0]
                                    else:
                                        lastMessagesList = [newMessage]
                                    lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                if cachedDialogsDictToUser is not None:
                                    if dialogId not in cachedDialogsDictToUser.keys():
                                        cachedDialogsDictToUser.update({dialogId: dialog})
                                    cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                if lastMessagesDictListToUser is not None:
                                    if lastMessagesList is None:
                                        lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                        if lastMessagesList is not None:
                                            lastMessagesList.append(newMessage)
                                            if len(lastMessagesList) > 6:
                                                del lastMessagesList[0]
                                        else:
                                            lastMessagesList = [newMessage]
                                    lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                dialogsCache.set_many({myId: cachedDialogsDictDictMy, toUserId: cachedDialogsDictDictToUser})
                            else:
                                if cachedDialogsDictDictMy is not None:
                                    cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                    if cachedDialogsDictMy is not None:
                                        if dialogId not in cachedDialogsDictMy.keys():
                                            cachedDialogsDictMy.update({dialogId: dialog})
                                        cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                    lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                    if lastMessagesDictListMy is not None:
                                        lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                        if lastMessagesList is not None:
                                            lastMessagesList.append(newMessage)
                                            if len(lastMessagesList) > 6:
                                                del lastMessagesList[0]
                                        else:
                                            lastMessagesList = [newMessage]
                                        lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                    dialogsCache.set(myId, cachedDialogsDictDictMy)
                                elif cachedDialogsDictDictToUser is not None:
                                    cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                    if cachedDialogsDictToUser is not None:
                                        if dialogId not in cachedDialogsDictToUser.keys():
                                            cachedDialogsDictToUser.update({dialogId: dialog})
                                        cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                    lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                    if lastMessagesDictListToUser is not None:
                                        lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                        if lastMessagesList is not None:
                                            lastMessagesList.append(newMessage)
                                            if len(lastMessagesList) > 6:
                                                del lastMessagesList[0]
                                        else:
                                            lastMessagesList = [newMessage]
                                        lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                    dialogsCache.set(toUserId, cachedDialogsDictDictToUser)
                        newImagesAttachment = None
                    except Error:
                        save = False
            else:
                if filesList is not None or cachedImagesList is not None:
                    if filesList is not None and cachedImagesList is not None:
                        imagesToSave = cachedImagesList + filesList
                    elif cachedImagesList is None:
                        imagesToSave = filesList
                    else:
                        imagesToSave = cachedImagesList
                    try:
                        with atomic():
                            if lastMessage is None:
                                lastMessage = personalMessage.objects.filter(inDialog__exact = dialog)
                                if lastMessage.count() > 0:
                                    lastMessage = lastMessage.last()
                                    if str(lastMessage.by_id) == myId:
                                        newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = False, inDialog = dialog, time = time)
                                    else:
                                        newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = True, inDialog = dialog, time = time)
                                else:
                                    newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = True, inDialog = dialog, time = time)
                            else:
                                if str(lastMessage.by_id) == myId:
                                    newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = False, inDialog = dialog, time = time)
                                else:
                                    newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = True, inDialog = dialog, time = time)
                            newImagesAttachment = imagesAttachmentMessagePersonal.objects.create(message = newMessage, by = curUser, itemsLength = 0)
                            cachedImagesLength = len(imagesToSave)
                            if cachedImagesLength <= 3:
                                if cachedImagesLength == 1:
                                    newImagesAttachment.image1 = imagesToSave[0]
                                elif cachedImagesLength == 2:
                                    newImagesAttachment.image1 = imagesToSave[0]
                                    newImagesAttachment.image2 = imagesToSave[1]
                                else:
                                    newImagesAttachment.image1 = imagesToSave[0]
                                    newImagesAttachment.image2 = imagesToSave[1]
                                    newImagesAttachment.image3 = imagesToSave[2]
                            else:
                                if cachedImagesLength == 4:
                                    newImagesAttachment.image1 = imagesToSave[0]
                                    newImagesAttachment.image2 = imagesToSave[1]
                                    newImagesAttachment.image3 = imagesToSave[2]
                                    newImagesAttachment.image4 = imagesToSave[3]
                                elif cachedImagesLength == 5:
                                    newImagesAttachment.image1 = imagesToSave[0]
                                    newImagesAttachment.image2 = imagesToSave[1]
                                    newImagesAttachment.image3 = imagesToSave[2]
                                    newImagesAttachment.image4 = imagesToSave[3]
                                    newImagesAttachment.image5 = imagesToSave[4]
                                else:
                                    newImagesAttachment.image1 = imagesToSave[0]
                                    newImagesAttachment.image2 = imagesToSave[1]
                                    newImagesAttachment.image3 = imagesToSave[2]
                                    newImagesAttachment.image4 = imagesToSave[3]
                                    newImagesAttachment.image5 = imagesToSave[4]
                                    newImagesAttachment.image6 = imagesToSave[5]
                            newImagesAttachment.itemsLength = cachedImagesLength
                            newMessage.imagesAttachment = newImagesAttachment
                            if lastMessage is None:
                                lastMessage = personalMessage.objects.filter(inDialog__exact = dialog)
                                if lastMessage.count() > 0:
                                    lastMessage = lastMessage.last()
                                    if str(lastMessage.by_id) == myId:
                                        newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = False, inDialog = dialog, time = time)
                                    else:
                                        newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = True, inDialog = dialog, time = time)
                                else:
                                    newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = True, inDialog = dialog, time = time)
                            else:
                                if str(lastMessage.by_id) == myId:
                                    newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = False, inDialog = dialog, time = time)
                                else:
                                    newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = True, inDialog = dialog, time = time)
                            newMessage.save()
                            newImagesAttachment.save()
                            lastMessageCache.set(dialogId, newMessage)
                            messageIdStr = str(newMessage.id)
                            if oldUnreadMessagesDictDict is None:
                                unreadMessagesCache.set(dialogId, {toUserId: {messageIdStr,}})
                            else:
                                if oldUnreadMessages is None:
                                    oldUnreadMessagesDictDict.update({toUserId: {messageIdStr,}})
                                    unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                                else:
                                    oldUnreadMessages.add(messageIdStr)
                                    unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                            if oldData is not None:
                                finded = None
                                for item in oldData:
                                    if dialogId == item[3]:
                                        finded = item
                                        break
                                if finded is None:
                                    oldData.append(('personalMessage', 'new', myId, dialogId))
                                    newUpdates.set(toUserId, oldData)
                                else:
                                    if finded[1] == "hidden":
                                        oldData[oldData.index(finded)][1] = "new"
                                        newUpdates.set(toUserId, oldData)
                                        finded = "hidden"
                            else:
                                newUpdates.set(toUserId, [('personalMessage', 'new', myId, dialogId)])
                            if cachedDialogsDictDictMy is not None and cachedDialogsDictDictToUser is not None:
                                lastMessagesList = None
                                cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                if cachedDialogsDictMy is not None:
                                    if dialogId not in cachedDialogsDictMy.keys():
                                        cachedDialogsDictMy.update({dialogId: dialog})
                                    cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                if lastMessagesDictListMy is not None:
                                    lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                    if lastMessagesList is not None:
                                        lastMessagesList.append(newMessage)
                                        if len(lastMessagesList) > 6:
                                            del lastMessagesList[0]
                                    else:
                                        lastMessagesList = [newMessage]
                                    lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                if cachedDialogsDictToUser is not None:
                                    if dialogId not in cachedDialogsDictToUser.keys():
                                        cachedDialogsDictToUser.update({dialogId: dialog})
                                    cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                if lastMessagesDictListToUser is not None:
                                    if lastMessagesList is None:
                                        lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                        if lastMessagesList is not None:
                                            lastMessagesList.append(newMessage)
                                            if len(lastMessagesList) > 6:
                                                del lastMessagesList[0]
                                        else:
                                            lastMessagesList = [newMessage]
                                    lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                dialogsCache.set_many({myId: cachedDialogsDictDictMy, toUserId: cachedDialogsDictDictToUser})
                            else:
                                if cachedDialogsDictDictMy is not None:
                                    cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                    if cachedDialogsDictMy is not None:
                                        if dialogId not in cachedDialogsDictMy.keys():
                                            cachedDialogsDictMy.update({dialogId: dialog})
                                        cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                    lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                    if lastMessagesDictListMy is not None:
                                        lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                        if lastMessagesList is not None:
                                            lastMessagesList.append(newMessage)
                                            if len(lastMessagesList) > 6:
                                                del lastMessagesList[0]
                                        else:
                                            lastMessagesList = [newMessage]
                                        lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                    dialogsCache.set(myId, cachedDialogsDictDictMy)
                                elif cachedDialogsDictDictToUser is not None:
                                    cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                    if cachedDialogsDictToUser is not None:
                                        if dialogId not in cachedDialogsDictToUser.keys():
                                            cachedDialogsDictToUser.update({dialogId: dialog})
                                        cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                    lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                    if lastMessagesDictListToUser is not None:
                                        lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                        if lastMessagesList is not None:
                                            lastMessagesList.append(newMessage)
                                            if len(lastMessagesList) > 6:
                                                del lastMessagesList[0]
                                        else:
                                            lastMessagesList = [newMessage]
                                        lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                    dialogsCache.set(toUserId, cachedDialogsDictDictToUser)
                    except Error:
                        save = False
                    if save is True:
                        if cachedImagesList is not None:
                            del cachedFilesDict[myId]
                            messagesFilesStorage.set(dialogId, cachedFilesDict)
                else:
                    try:
                        with atomic():
                            if lastMessage is None:
                                lastMessage = personalMessage.objects.filter(inDialog__exact = dialog)
                                if lastMessage.count() > 0:
                                    lastMessage = lastMessage.last()
                                    if str(lastMessage.by_id) == myId:
                                        newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = False, inDialog = dialog, time = time)
                                    else:
                                        newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = True, inDialog = dialog, time = time)
                                else:
                                    newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = True, inDialog = dialog, time = time)
                            else:
                                if str(lastMessage.by_id) == myId:
                                    newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = False, inDialog = dialog, time = time)
                                else:
                                    newMessage = personalMessage.objects.create(by = curUser, to = toUserInstance, body = data['body'], messageHead = True, inDialog = dialog, time = time)
                            newMessage.save()
                            lastMessageCache.set(dialogId, newMessage)
                            messageIdStr = str(newMessage.id)
                            if oldUnreadMessagesDictDict is None:
                                unreadMessagesCache.set(dialogId, {toUserId: {messageIdStr,}})
                            else:
                                if oldUnreadMessages is None:
                                    oldUnreadMessagesDictDict.update({toUserId: {messageIdStr,}})
                                    unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                                else:
                                    oldUnreadMessages.add(messageIdStr)
                                    unreadMessagesCache.set(dialogId, oldUnreadMessagesDictDict)
                            if oldData is not None:
                                finded = None
                                for item in oldData:
                                    if dialogId == item[3]:
                                        finded = item
                                        break
                                if finded is None:
                                    oldData.append(('personalMessage', 'new', myId, dialogId))
                                    newUpdates.set(toUserId, oldData)
                                else:
                                    if finded[1] == "hidden":
                                        oldData[oldData.index(finded)][1] = "new"
                                        newUpdates.set(toUserId, oldData)
                                        finded = "hidden"
                            else:
                                newUpdates.set(toUserId, [('personalMessage', 'new', myId, dialogId)])
                            if cachedDialogsDictDictMy is not None and cachedDialogsDictDictToUser is not None:
                                lastMessagesList = None
                                cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                if cachedDialogsDictMy is not None:
                                    if dialogId not in cachedDialogsDictMy.keys():
                                        cachedDialogsDictMy.update({dialogId: dialog})
                                    cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                if lastMessagesDictListMy is not None:
                                    lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                    if lastMessagesList is not None:
                                        lastMessagesList.append(newMessage)
                                        if len(lastMessagesList) > 6:
                                            del lastMessagesList[0]
                                    else:
                                        lastMessagesList = [newMessage]
                                    lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                if cachedDialogsDictToUser is not None:
                                    if dialogId not in cachedDialogsDictToUser.keys():
                                        cachedDialogsDictToUser.update({dialogId: dialog})
                                    cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                if lastMessagesDictListToUser is not None:
                                    if lastMessagesList is None:
                                        lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                        if lastMessagesList is not None:
                                            lastMessagesList.append(newMessage)
                                            if len(lastMessagesList) > 6:
                                                del lastMessagesList[0]
                                        else:
                                            lastMessagesList = [newMessage]
                                    lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                dialogsCache.set_many({myId: cachedDialogsDictDictMy, toUserId: cachedDialogsDictDictToUser})
                            else:
                                if cachedDialogsDictDictMy is not None:
                                    cachedDialogsDictMy = cachedDialogsDictDictMy.get("dialogs", None)
                                    if cachedDialogsDictMy is not None:
                                        if dialogId not in cachedDialogsDictMy.keys():
                                            cachedDialogsDictMy.update({dialogId: dialog})
                                        cachedDialogsDictMy.move_to_end(dialogId, last = False)
                                    lastMessagesDictListMy = cachedDialogsDictDictMy.get("last", None)
                                    if lastMessagesDictListMy is not None:
                                        lastMessagesList = lastMessagesDictListMy.get(dialogId, None)
                                        if lastMessagesList is not None:
                                            lastMessagesList.append(newMessage)
                                            if len(lastMessagesList) > 6:
                                                del lastMessagesList[0]
                                        else:
                                            lastMessagesList = [newMessage]
                                        lastMessagesDictListMy.update({dialogId: lastMessagesList})
                                    dialogsCache.set(myId, cachedDialogsDictDictMy)
                                elif cachedDialogsDictDictToUser is not None:
                                    cachedDialogsDictToUser = cachedDialogsDictDictToUser.get("dialogs", None)
                                    if cachedDialogsDictToUser is not None:
                                        if dialogId not in cachedDialogsDictToUser.keys():
                                            cachedDialogsDictToUser.update({dialogId: dialog})
                                        cachedDialogsDictToUser.move_to_end(dialogId, last = False)
                                    lastMessagesDictListToUser = cachedDialogsDictDictToUser.get("last", None)
                                    if lastMessagesDictListToUser is not None:
                                        lastMessagesList = lastMessagesDictListToUser.get(dialogId, None)
                                        if lastMessagesList is not None:
                                            lastMessagesList.append(newMessage)
                                            if len(lastMessagesList) > 6:
                                                del lastMessagesList[0]
                                        else:
                                            lastMessagesList = [newMessage]
                                        lastMessagesDictListToUser.update({dialogId: lastMessagesList})
                                    dialogsCache.set(toUserId, cachedDialogsDictDictToUser)
                        newImagesAttachment = None
                    except Error:
                        save = False
            if save is True:
                myUsername = curUser.username
                mySlug = curUser.profileUrl
                myDialogClass = 'dialogMessage-new-%s' % myId
                if newImagesAttachment is None:
                    myMessageListDict = [
                    {
                        'method': 'messagePersonal',
                        'dialogLink': reverse("personalMessage", kwargs = {'toUser': toUser}),
                        'id': messageIdStr,
                        'body': newMessage.body,
                        'slug': mySlug,
                        'value': 'dialogMessage-new-%s' % toUserId,
                        'dialogId': dialogId,
                        'byId': myId,
                        'eventType': 'notify',
                        # Дальше для метода messageMany
                        'count': 0,
                        'profileLink': reverse("profile", kwargs = {'currentUser': mySlug}),
                        'username': myUsername,
                        'forgetLink': reverse("deleteNotifyEvent", kwargs = {'type': 'dialogMessage', 'state': 'new', 'senderId': myId})
                        }
                    ]
                    if oldData is not None:
                        if finded is None:
                            messageListDict = copy(myMessageListDict)
                            messageListDict[0].update({
                            'dialogLink': request.path,
                            'class': myDialogClass,
                            'notifyCounterChange': {
                                'method': 'incrementNotifyValue',
                                'baseValue': 1,
                                'multiValue': 1,
                                'class': myDialogClass
                                }
                            })
                        else:
                            if finded == "hidden":
                                messageListDict = copy(myMessageListDict)
                                messageListDict[0].update({
                                'dialogLink': request.path,
                                'class': myDialogClass,
                                'notifyCounterChange': {
                                    'method': 'incrementNotifyValue',
                                    'baseValue': len(oldUnreadMessages),
                                    'multiValue': 1,
                                    'class': myDialogClass
                                    }
                                })
                            else:
                                messageListDict = copy(myMessageListDict)
                                messageListDict[0].update({
                                'dialogLink': request.path,
                                'class': myDialogClass,
                                'notifyCounterChange': {
                                    'method': 'incrementNotifyValue',
                                    'baseValue': 1,
                                    'class': myDialogClass
                                    }
                                })
                    else:
                        messageListDict = copy(myMessageListDict)
                        messageListDict[0].update({
                        'dialogLink': request.path,
                        'class': myDialogClass,
                        'notifyCounterChange': {
                            'method': 'incrementNotifyValue',
                            'baseValue': 1,
                            'multiValue': 1,
                            'class': myDialogClass
                            }
                        })
                else:
                    if cachedImagesLength <= 3:
                        if cachedImagesLength == 1:
                            imagesList = [newImagesAttachment.image1.url]
                        elif cachedImagesLength == 2:
                            imagesList = [
                                newImagesAttachment.image1.url,
                                newImagesAttachment.image2.url
                            ]
                        else:
                            imagesList = [
                                newImagesAttachment.image1.url,
                                newImagesAttachment.image2.url,
                                newImagesAttachment.image3.url
                            ]
                    else:
                        if cachedImagesLength == 4:
                            imagesList = [
                                newImagesAttachment.image1.url,
                                newImagesAttachment.image2.url,
                                newImagesAttachment.image3.url,
                                newImagesAttachment.image4.url
                            ]
                        elif cachedImagesLength == 5:
                            imagesList = [
                                newImagesAttachment.image1.url,
                                newImagesAttachment.image2.url,
                                newImagesAttachment.image3.url,
                                newImagesAttachment.image4.url,
                                newImagesAttachment.image5.url
                            ]
                        else:
                            imagesList = [
                                newImagesAttachment.image1.url,
                                newImagesAttachment.image2.url,
                                newImagesAttachment.image3.url,
                                newImagesAttachment.image4.url,
                                newImagesAttachment.image5.url,
                                newImagesAttachment.image6.url
                            ]
                    myMessageListDict = [
                    {
                        'method': 'messagePersonal',
                        'dialogLink': reverse("personalMessage", kwargs = {'toUser': toUser}),
                        'id': messageIdStr,
                        'body': newMessage.body,
                        'images': imagesList,
                        'slug': mySlug,
                        'value': 'dialogMessage-new-%s' % toUserId,
                        'dialogId': dialogId,
                        'byId': myId,
                        'eventType': 'notify',
                        # Дальше для метода messageMany
                        'count': 0,
                        'profileLink': reverse("profile", kwargs = {'currentUser': mySlug}),
                        'username': myUsername,
                        'forgetLink': reverse("deleteNotifyEvent", kwargs = {'type': 'dialogMessage', 'state': 'new', 'senderId': myId})
                        }
                    ]
                    if oldData is not None:
                        if finded is None:
                            messageListDict = copy(myMessageListDict)
                            messageListDict[0].update({
                            'dialogLink': request.path,
                            'class': myDialogClass,
                            'notifyCounterChange': {
                                'method': 'incrementNotifyValue',
                                'baseValue': 1,
                                'multiValue': 1,
                                'class': myDialogClass
                                }
                            })
                        else:
                            if finded == "hidden":
                                messageListDict = copy(myMessageListDict)
                                messageListDict[0].update({
                                'dialogLink': request.path,
                                'class': myDialogClass,
                                'notifyCounterChange': {
                                    'method': 'incrementNotifyValue',
                                    'baseValue': len(oldUnreadMessages),
                                    'multiValue': 1,
                                    'class': myDialogClass
                                    }
                                })
                            else:
                                messageListDict = copy(myMessageListDict)
                                messageListDict[0].update({
                                'dialogLink': request.path,
                                'class': myDialogClass,
                                'notifyCounterChange': {
                                    'method': 'incrementNotifyValue',
                                    'baseValue': 1,
                                    'class': myDialogClass
                                    }
                                })
                    else:
                        messageListDict = copy(myMessageListDict)
                        messageListDict[0].update({
                        'dialogLink': request.path,
                        'class': myDialogClass,
                        'notifyCounterChange': {
                            'method': 'incrementNotifyValue',
                            'baseValue': 1,
                            'multiValue': 1,
                            'class': myDialogClass
                            }
                        })
                layer = get_channel_layer().group_send
                messageListStr =  json.dumps(messageListDict)
                async_to_sync(layer)(myId, {"type": "sendToClient", "data": {"item": json.dumps(myMessageListDict)}})
                async_to_sync(layer)(toUserId, {"type": "sendToClient", "data": {"item": messageListStr}})
                curUserUsername = curUser.username
                curUserProfileUrl = curUser.profileUrl
                myId = str(curUser.id)
                toUserId = str(toUserInstance.id)
                toUserUsername = toUserInstance.username
                fragments = cachedFragments(myUsername = curUserUsername, mySlug = curUserProfileUrl,
                                            username = toUserUsername, profileUrl = toUser, url = request.path)
                if existsMessagesInDialog:
                    if javaScriptStatus:
                        messages, unreadMessagesDictList = loadDialogSimple(dialogId, myId, toUserId, 0)
                        return render(request, "psixiSocial/psixiSocial.html", {
                            'parentTemplate': 'base/classicPage/base.html',
                            'content': ('psixiSocial', 'dialog', 'personal', True),
                            'menu': fragments.menuGetter('menuSerialized'),
                            'messagesListDict': messages,
                            'unreadMessagesDictList': unreadMessagesDictList,
                            'myListThumb': myListThumbItem,
                            'toUserListThumb': toUserListThumbItem,
                            'myMainThumb': myMainThumb,
                            'toUserMainThumb': toUserMainThumb,
                            'form': None,
                            'accessToSend': sendMessages,
                            'csrf': checkCSRF(request, cookies),
                            'time': time.microsecond,
                            'requestUserId': myId,
                            'opponentUserId': toUserId,
                            'dialogId': dialogId,
                            'onlineStatusDict': onlineStatusDict,
                            'activeStatusDict': activeStatusDict
                            })
                    messages, images, unread = loadDialog(dialogId, myId, toUserId, 0)
                    return render(request, "psixiSocial/psixiSocial.html", {
                        'parentTemplate': 'base/classicPage/base.html',
                        'content': ('psixiSocial', 'dialog', 'personal', False),
                        'menu': fragments.menuGetter('menuSerialized'),
                        'messagesListDict': None,
                        'messagesQuerySet': messages,
                        'imagesQuerySet': images,
                        'unreadMessagesSet': unread,
                        'unreadMessagesDictList': None,
                        'myListThumb': myListThumbItem,
                        'toUserListThumb': toUserListThumbItem,
                        'myMainThumb': myMainThumb,
                        'toUserMainThumb': toUserMainThumb,
                        'form': None,
                        'accessToSend': sendMessages,
                        'csrf': checkCSRF(request, cookies),
                        'time': time.microsecond,
                        'requestUserId': myId,
                        'opponentUserId': toUserId,
                        'dialogId': dialogId,
                        'onlineStatusDict': onlineStatusDict,
                        'activeStatusDict': activeStatusDict
                        })
                return render(request, "psixiSocial/psixiSocial.html", {
                    'parentTemplate': 'base/classicPage/base.html',
                    'content': ('psixiSocial', 'dialog', 'personal', True),
                    'menu': fragments.menuGetter('menuSerialized'),
                    'messagesListDict': None,
                    'unreadMessagesDictList': None,
                    'myListThumb': myListThumbItem,
                    'toUserListThumb': toUserListThumbItem,
                    'myMainThumb': myMainThumb,
                    'toUserMainThumb': toUserMainThumb,
                    'form': None,
                    'accessToSend': sendMessages,
                    'csrf': checkCSRF(request, cookies),
                    'time': time.microsecond,
                    'requestUserId': myId,
                    'opponentUserId': toUserId,
                    'dialogId': dialogId,
                    'onlineStatusDict': onlineStatusDict,
                    'activeStatusDict': activeStatusDict
                    })
            # Database Error
            curUserUsername = curUser.username
            curUserProfileUrl = curUser.profileUrl
            myId = str(curUser.id)
            toUserId = str(toUserInstance.id)
            toUserUsername = toUserInstance.username
            fragments = cachedFragments(myUsername = curUserUsername, mySlug = curUserProfileUrl,
                                        username = toUserUsername, profileUrl = toUser, url = request.path)
            if existsMessagesInDialog:
                if javaScriptStatus:
                    messages, unreadMessagesDictList = loadDialogSimple(dialogId, myId, toUserId, 0)
                    return render(request, "psixiSocial/psixiSocial.html", {
                        'parentTemplate': 'base/classicPage/base.html',
                        'content': ('psixiSocial', 'dialog', 'personal', True),
                        'menu': fragments.menuGetter('menuSerialized'),
                        'messagesListDict': messages,
                        'unreadMessagesDictList': unreadMessagesDictList,
                        'myListThumb': myListThumbItem,
                        'toUserListThumb': toUserListThumbItem,
                        'myMainThumb': myMainThumb,
                        'toUserMainThumb': toUserMainThumb,
                        'form': None,
                        'accessToSend': sendMessages,
                        'csrf': checkCSRF(request, cookies),
                        'time': time.microsecond,
                        'requestUserId': myId,
                        'opponentUserId': toUserId,
                        'dialogId': dialogId,
                        'onlineStatusDict': onlineStatusDict,
                        'activeStatusDict': activeStatusDict
                        })
                messages, unread = loadDialog(dialogId, myId, toUserId, 0)
                return render(request, "psixiSocial/psixiSocial.html", {
                    'parentTemplate': 'base/classicPage/base.html',
                    'content': ('psixiSocial', 'dialog', 'personal', False),
                    'menu': fragments.menuGetter('menuSerialized'),
                    'messagesListDict': None,
                    'messagesQuerySet': messages,
                    'imagesQuerySet': images,
                    'unreadMessagesSet': unread,
                    'unreadMessagesDictList': None,
                    'myListThumb': myListThumbItem,
                    'toUserListThumb': toUserListThumbItem,
                    'myMainThumb': myMainThumb,
                    'toUserMainThumb': toUserMainThumb,
                    'form': None,
                    'accessToSend': sendMessages,
                    'csrf': checkCSRF(request, cookies),
                    'time': time.microsecond,
                    'requestUserId': myId,
                    'opponentUserId': toUserId,
                    'dialogId': dialogId,
                    'onlineStatusDict': onlineStatusDict,
                    'activeStatusDict': activeStatusDict
                    })
            return render(request, "psixiSocial/psixiSocial.html", {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'dialog', 'personal', True),
                'menu': fragments.menuGetter('menuSerialized'),
                'messagesListDict': None,
                'unreadMessagesDictList': None,
                'myListThumb': myListThumbItem,
                'toUserListThumb': toUserListThumbItem,
                'myMainThumb': myMainThumb,
                'toUserMainThumb': toUserMainThumb,
                'form': None,
                'accessToSend': sendMessages,
                'csrf': checkCSRF(request, cookies),
                'time': time.microsecond,
                'requestUserId': myId,
                'opponentUserId': toUserId,
                'dialogId': dialogId,
                'onlineStatusDict': onlineStatusDict,
                'activeStatusDict': activeStatusDict
                })
        # UNVALID FORM
        curUserUsername = curUser.username
        curUserProfileUrl = curUser.profileUrl
        myId = str(curUser.id)
        toUserId = str(toUserInstance.id)
        toUserUsername = toUserInstance.username
        fragments = cachedFragments(myUsername = curUserUsername, mySlug = curUserProfileUrl,
                                    username = toUserUsername, profileUrl = toUser, url = request.path)
        if existsMessagesInDialog:
            if javaScriptStatus:
                messages, unreadMessagesDictList = loadDialogSimple(dialogId, myId, toUserId, 0)
                return render(request, "psixiSocial/psixiSocial.html", {
                    'parentTemplate': 'base/classicPage/base.html',
                    'content': ('psixiSocial', 'dialog', 'personal', True),
                    'menu': fragments.menuGetter('menuSerialized'),
                    'messagesListDict': messages,
                    'unreadMessagesDictList': unreadMessagesDictList,
                    'myListThumb': myListThumbItem,
                    'toUserListThumb': toUserListThumbItem,
                    'myMainThumb': myMainThumb,
                    'toUserMainThumb': toUserMainThumb,
                    'form': form,
                    'accessToSend': sendMessages,
                    'csrf': checkCSRF(request, cookies),
                    'time': datetime.now(tz = tz.gettz()),
                    'requestUserId': myId,
                    'opponentUserId': toUserId,
                    'dialogId': dialogId,
                    })
            messages, images, unread = loadDialog(dialogId, myId, toUserId, 0)
            return render(request, "psixiSocial/psixiSocial.html", {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'dialog', 'personal', False),
                'menu': fragments.menuGetter('menuSerialized'),
                'messagesListDict': None,
                'messagesQuerySet': messages,
                'imagesQuerySet': images,
                'unreadMessagesSet': unread,
                'unreadMessagesDictList': None,
                'myListThumb': myListThumbItem,
                'toUserListThumb': toUserListThumbItem,
                'myMainThumb': myMainThumb,
                'toUserMainThumb': toUserMainThumb,
                'form': form,
                'accessToSend': sendMessages,
                'csrf': checkCSRF(request, cookies),
                'time': datetime.now(tz = tz.gettz()),
                'requestUserId': myId,
                'opponentUserId': toUserId,
                'dialogId': dialogId,
                })
        return render(request, "psixiSocial/psixiSocial.html", {
            'parentTemplate': 'base/classicPage/base.html',
            'content': ('psixiSocial', 'dialog', 'personal', True),
            'menu': fragments.menuGetter('menuSerialized'),
            'messagesListDict': None,
            'unreadMessagesDictList': None,
            'myListThumb': myListThumbItem,
            'toUserListThumb': toUserListThumbItem,
            'myMainThumb': myMainThumb,
            'toUserMainThumb': toUserMainThumb,
            'form': form,
            'accessToSend': sendMessages,
            'csrf': checkCSRF(request, cookies),
            'time': datetime.now(tz = tz.gettz()),
            'requestUserId': myId,
            'opponentUserId': toUserId,
            'dialogId': dialogId,
            })

def loadDialog(dialogId, im, otherId, page):
    ''' Функция "прочитает" сообщения, удалив значения из кеша непрочитанных сообщений.
    '''
    allMesages = personalMessage.objects.filter(inDialog_id__exact = dialogId).order_by("time")
    if allMesages.exists():
        messagesPaginator = Paginator(allMesages, 10, orphans = 15)
        newPage = messagesPaginator.num_pages - page
        if newPage > 0:
            messagesMany = messagesPaginator.page(newPage).object_list.values("id", "by_id", "body", "time", "messageHead", "imagesAttachment")
            imageAttachmentIds = messagesMany.values("imagesAttachment")
            if imageAttachmentIds.count() > 0:
                imageAttachments = imagesAttachmentMessagePersonal.objects.filter(message_id__in = imageAttachmentIds).values("message_id", "image1", "image2", "image3", "image4", "image5", "image6", "itemsLength")
            else:
                imageAttachments = None
            unreadMessagesCache = caches["unreadPersonalMessages"]
            unreadMessagesDictDict = unreadMessagesCache.get(dialogId, None)
            if unreadMessagesDictDict is not None:
                unreadMessagesByUserSet = unreadMessagesDictDict.get(im, None)
                unreadMessagesMySet = unreadMessagesDictDict.get(otherId, None)
                if unreadMessagesByUserSet is not None or unreadMessagesMySet is not None:
                    if unreadMessagesByUserSet is not None and unreadMessagesMySet is not None:
                        myMessagesSet = set()
                        removeMessagesList = []
                        for messageItem in messagesMany:
                            messageId = str(messageItem.get('id'))
                            if messageId in unreadMessagesByUserSet:
                                unreadMessagesByUserSet.remove(messageId)
                                removeMessagesList.append(messageId)
                            if messageId in unreadMessagesMySet:
                                myMessagesSet.add(messageId)
                        if len(removeMessagesList) > 0:
                            layer = get_channel_layer().group_send
                            async_to_sync(layer)(otherId, {"data": {"item": json.dumps({"personalMessageRead": removeMessagesList})}})
                        return messagesMany, imageAttachments, myMessagesSet
                    else:
                        if unreadMessagesByUserSet is not None:
                            removeMessagesList = []
                            for messageItem in messagesMany:
                                messageId = str(messageItem.get('id'))
                                if messageId in unreadMessagesByUserSet:
                                    unreadMessagesByUserSet.remove(messageId)
                                    removeMessagesList.append(messageId)
                            if len(removeMessagesList) > 0:
                                if len(unreadMessagesByUserSet) > 0:
                                    unreadMessagesCache.set(dialogId, unreadMessagesDictDict)
                                else:
                                    unreadMessagesCache.delete(dialogId)
                                async_to_sync(layer)(otherId, {"data": {"item": json.dumps({"personalMessageRead": removeMessagesList})}})
                            return messagesMany, imageAttachments
                        else:
                            myMessagesSet = set()
                            for messageItem in messagesMany:
                                messageId = str(messageItem.get('id'))
                                if messageId in unreadMessagesMySet:
                                    myMessagesSet.add(messageId)
                            return messagesMany, imageAttachments, myMessagesSet
                return messagesMany, imageAttachments, None
            return messagesMany, imageAttachments, None
        return None, None, None
    return None, None, None

def loadDialogSimple(dialogId, im, otherId, page):
    ''' Вывод сообщений при включённом javaScript.
   "Чтение" сообщений инициируется со стороны клиента по webSocket.'''
    allMesages = personalMessage.objects.filter(inDialog_id__exact = dialogId).order_by("time")
    if allMesages.exists():
        messagesPaginator = Paginator(allMesages, 10, orphans = 15)
        newPage = messagesPaginator.num_pages - page
        if newPage > 0:
            messagesList = []
            unreadMessagesDictDict = caches["unreadPersonalMessages"].get(dialogId, None)
            messagesMany = messagesPaginator.page(newPage).object_list.values("id", "by_id", "body", "time", "messageHead", "imagesAttachment")
            imageAttachmentIds = messagesMany.values("imagesAttachment")
            if unreadMessagesDictDict is not None:
                unreadMessagesByUserSet = unreadMessagesDictDict.get(im, None)
                unreadMessagesMySet = unreadMessagesDictDict.get(otherId, None)
                if unreadMessagesByUserSet is not None or unreadMessagesMySet is not None:
                    if unreadMessagesByUserSet is not None and unreadMessagesMySet is not None:
                        myUnreadMessagesList = []
                        opponentUnreadMessagesList = []
                        if imageAttachmentIds.count() > 0:
                            imageAttachmentMany = imagesAttachmentMessagePersonal.objects.filter(message_id__in = imageAttachmentIds).values("message_id", "image1", "image2", "image3", "image4", "image5", "image6", "itemsLength")
                            for messageItem in messagesMany:
                                messageId = str(messageItem.get("id"))
                                if messageId in unreadMessagesByUserSet:
                                    opponentUnreadMessagesList.append(messageId)
                                    unreadMessagesByUserSet.remove(messageId)
                                elif messageId in unreadMessagesMySet:
                                    myUnreadMessagesList.append(messageId)
                                    unreadMessagesMySet.remove(messageId)
                                try:
                                    imagesData = imageAttachmentMany.get(message_id__exact = messageId)
                                except imagesAttachmentMessagePersonal.DoesNotExist:
                                    imagesData = None
                                if imagesData is not None:
                                    media = settings.MEDIA_URL
                                    imagesLen = imagesData.get('itemsLength')
                                    if imagesLen > 3:
                                        if imagesLen == 4:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4'))]
                                        elif imagesLen == 5:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4')), '{0}{1}'.format(media, imagesData.get('image5'))]
                                        else:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4')), '{0}{1}'.format(media, imagesData.get('image5')), '{0}{1}'.format(media, imagesData.get('image6'))]
                                    else:
                                        if imagesLen == 1:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1'))]
                                        elif imagesLen == 2:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2'))]
                                        else:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3'))]
                                    messagesList.append({
                                        'id': messageId,
                                        'body': messageItem.get('body'),
                                        'head': int(messageItem.get('messageHead')),
                                        'by': messageItem.get('by_id'),
                                        'time': str(messageItem.get('time')),
                                        'images': imagesList
                                        })
                                else:
                                    messagesList.append({
                                        'id': messageId,
                                        'body': messageItem.get('body'),
                                        'head': int(messageItem.get('messageHead')),
                                        'by': messageItem.get('by_id'),
                                        'time': str(messageItem.get('time'))
                                        })
                            return messagesList, {im: myUnreadMessagesList, otherId: opponentUnreadMessagesList}
                        for messageItem in messagesMany:
                            if messageId in unreadMessagesByUserSet:
                                opponentUnreadMessagesList.append(messageId)
                                unreadMessagesByUserSet.remove(messageId)
                            elif messageId in unreadMessagesMySet:
                                myUnreadMessagesList.append(messageId)
                                unreadMessagesMySet.remove(messageId)
                                messagesList.append({
                                    'id': str(messageItem.get("id")),
                                    'body': messageItem.get('body'),
                                    'head': int(messageItem.get('messageHead')),
                                    'by': messageItem.get('by_id'),
                                    'time': str(messageItem.get('time'))
                                    })
                        return messagesList, {im: myUnreadMessagesList, otherId: opponentUnreadMessagesList}
                    if unreadMessagesByUserSet is not None:
                        opponentUnreadMessagesList = []
                        if imageAttachmentIds.count() > 0:
                            imageAttachmentMany = imagesAttachmentMessagePersonal.objects.filter(message_id__in = imageAttachmentIds).values("message_id", "image1", "image2", "image3", "image4", "image5", "image6", "itemsLength")
                            for messageItem in messagesMany:
                                messageId = str(messageItem.get("id"))
                                if messageId in unreadMessagesByUserSet:
                                    opponentUnreadMessagesList.append(messageId)
                                    unreadMessagesByUserSet.remove(messageId)
                                try:
                                    imagesData = imageAttachmentMany.get(message_id__exact = messageId)
                                except imagesAttachmentMessagePersonal.DoesNotExist:
                                    imagesData = None
                                if imagesData is not None:
                                    media = settings.MEDIA_URL
                                    imagesLen = imagesData.get('itemsLength')
                                    if imagesLen > 3:
                                        if imagesLen == 4:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4'))]
                                        elif imagesLen == 5:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4')), '{0}{1}'.format(media, imagesData.get('image5'))]
                                        else:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4')), '{0}{1}'.format(media, imagesData.get('image5')), '{0}{1}'.format(media, imagesData.get('image6'))]
                                    else:
                                        if imagesLen == 1:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1'))]
                                        elif imagesLen == 2:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2'))]
                                        else:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3'))]
                                    messagesList.append({
                                        'id': messageId,
                                        'body': messageItem.get('body'),
                                        'head': int(messageItem.get('messageHead')),
                                        'by': messageItem.get('by_id'),
                                        'time': str(messageItem.get('time')),
                                        'images': imagesList
                                        })
                                else:
                                    messagesList.append({
                                        'id': messageId,
                                        'body': messageItem.get('body'),
                                        'head': int(messageItem.get('messageHead')),
                                        'by': messageItem.get('by_id'),
                                        'time': str(messageItem.get('time'))
                                        })
                            return messagesList, {otherId: opponentUnreadMessagesList}
                        for messageItem in messagesMany:
                            if messageId in unreadMessagesByUserSet:
                                opponentUnreadMessagesList.append(messageId)
                                unreadMessagesByUserSet.remove(messageId)
                                messagesList.append({
                                    'id': str(messageItem.get("id")),
                                    'body': messageItem.get('body'),
                                    'head': int(messageItem.get('messageHead')),
                                    'by': messageItem.get('by_id'),
                                    'time': str(messageItem.get('time'))
                                    })
                        return messagesList, {otherId: opponentUnreadMessagesList}
                    myUnreadMessagesList = []
                    if imageAttachmentIds.count() > 0:
                        imageAttachmentMany = imagesAttachmentMessagePersonal.objects.filter(message_id__in = imageAttachmentIds).values("message_id", "image1", "image2", "image3", "image4", "image5", "image6", "itemsLength")
                        for messageItem in messagesMany:
                            messageId = str(messageItem.get("id"))
                            if messageId in unreadMessagesMySet:
                                myUnreadMessagesList.append(messageId)
                                unreadMessagesMySet.remove(messageId)
                            try:
                                imagesData = imageAttachmentMany.get(message_id__exact = messageId)
                            except imagesAttachmentMessagePersonal.DoesNotExist:
                                imagesData = None
                            if imagesData is not None:
                                media = settings.MEDIA_URL
                                imagesLen = imagesData.get('itemsLength')
                                if imagesLen > 3:
                                    if imagesLen == 4:
                                        imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4'))]
                                    elif imagesLen == 5:
                                        imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4')), '{0}{1}'.format(media, imagesData.get('image5'))]
                                    else:
                                        imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4')), '{0}{1}'.format(media, imagesData.get('image5')), '{0}{1}'.format(media, imagesData.get('image6'))]
                                else:
                                    if imagesLen == 1:
                                        imagesList = ['{0}{1}'.format(media, imagesData.get('image1'))]
                                    elif imagesLen == 2:
                                        imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2'))]
                                    else:
                                        imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3'))]
                                messagesList.append({
                                    'id': messageId,
                                    'body': messageItem.get('body'),
                                    'head': int(messageItem.get('messageHead')),
                                    'by': messageItem.get('by_id'),
                                    'time': str(messageItem.get('time')),
                                    'images': imagesList
                                    })
                            else:
                                messagesList.append({
                                    'id': messageId,
                                    'body': messageItem.get('body'),
                                    'head': int(messageItem.get('messageHead')),
                                    'by': messageItem.get('by_id'),
                                    'time': str(messageItem.get('time'))
                                    })
                        return messagesList, {im: myUnreadMessagesList}
                    for messageItem in messagesMany:
                        if messageId in unreadMessagesMySet:
                            myUnreadMessagesList.append(messageId)
                            unreadMessagesMySet.remove(messageId)
                            messagesList.append({
                                'id': str(messageItem.get("id")),
                                'body': messageItem.get('body'),
                                'head': int(messageItem.get('messageHead')),
                                'by': messageItem.get('by_id'),
                                'time': str(messageItem.get('time'))
                                })
                    return messagesList, {im: myUnreadMessagesList}
                if imageAttachmentIds.count() > 0:
                    imageAttachmentMany = imagesAttachmentMessagePersonal.objects.filter(message_id__in = imageAttachmentIds).values("message_id", "image1", "image2", "image3", "image4", "image5", "image6", "itemsLength")
                    for messageItem in messagesMany:
                        messageId = str(messageItem.get('id'))
                        try:
                            imagesData = imageAttachmentMany.get(message_id__exact = messageId)
                        except imagesAttachmentMessagePersonal.DoesNotExist:
                            imagesData = None
                        imagesData = messageItem.get('imagesAttachment', None)
                        if imagesData is not None:
                            media = settings.MEDIA_URL
                            imagesLen = imagesData.get('itemsLength')
                            if imagesLen > 3:
                                if imagesLen == 4:
                                    imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4'))]
                                elif imagesLen == 5:
                                    imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4')), '{0}{1}'.format(media, imagesData.get('image5'))]
                                else:
                                    imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4')), '{0}{1}'.format(media, imagesData.get('image5')), '{0}{1}'.format(media, imagesData.get('image6'))]
                            else:
                                if imagesLen == 1:
                                    imagesList = ['{0}{1}'.format(media, imagesData.get('image1'))]
                                elif imagesLen == 2:
                                    imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2'))]
                                else:
                                    imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3'))]
                            messagesList.append({
                                'id': messageId,
                                'body': messageItem.get('body'),
                                'head': int(messageItem.get('messageHead')),
                                'by': messageItem.get('by_id'),
                                'time': str(messageItem.get('time')),
                                'images': imagesList
                                })
                        else:
                            messagesList.append({
                                'id': messageId,
                                'body': messageItem.get('body'),
                                'head': int(messageItem.get('messageHead')),
                                'by': messageItem.get('by_id'),
                                'time': str(messageItem.get('time'))
                                })
                    return messagesList, None
                for messageItem in messagesMany:
                    messagesList.append({
                        'id': str(messageItem.get("id")),
                        'body': messageItem.get('body'),
                        'head': int(messageItem.get('messageHead')),
                        'by': messageItem.get('by_id'),
                        'time': str(messageItem.get('time'))
                        })
                return messagesList, None
            if imageAttachmentIds.count() > 0:
                imageAttachmentMany = imagesAttachmentMessagePersonal.objects.filter(message_id__in = imageAttachmentIds).values("message_id", "image1", "image2", "image3", "image4", "image5", "image6", "itemsLength")
                for messageItem in messagesMany:
                    messageId = str(messageItem.get("id"))
                    try:
                        imagesData = imageAttachmentMany.get(message_id__exact = messageId)
                    except imagesAttachmentMessagePersonal.DoesNotExist:
                        imagesData = None
                    if imagesData is not None:
                        media = settings.MEDIA_URL
                        imagesLen = imagesData.get('itemsLength')
                        if imagesLen > 3:
                            if imagesLen == 4:
                                imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4'))]
                            elif imagesLen == 5:
                                imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4')), '{0}{1}'.format(media, imagesData.get('image5'))]
                            else:
                                imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4')), '{0}{1}'.format(media, imagesData.get('image5')), '{0}{1}'.format(media, imagesData.get('image6'))]
                        else:
                            if imagesLen == 1:
                                imagesList = ['{0}{1}'.format(media, imagesData.get('image1'))]
                            elif imagesLen == 2:
                                imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2'))]
                            else:
                                imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3'))]
                        messagesList.append({
                            'id': messageId,
                            'body': messageItem.get('body'),
                            'head': int(messageItem.get('messageHead')),
                            'by': messageItem.get('by_id'),
                            'time': str(messageItem.get('time')),
                            'images': imagesList
                            })
                    else:
                        messagesList.append({
                            'id': messageId,
                            'body': messageItem.get('body'),
                            'head': int(messageItem.get('messageHead')),
                            'by': messageItem.get('by_id'),
                            'time': str(messageItem.get('time'))
                            })
                return messagesList, None
            for messageItem in messagesMany:
                messagesList.append({
                    'id': str(messageItem.get("id")),
                    'body': messageItem.get('body'),
                    'head': int(messageItem.get('messageHead')),
                    'by': messageItem.get('by_id'),
                    'time': str(messageItem.get('time'))
                    })
            return messagesList, None
        return [], None
    return [], None

def loadDialogSimpleShort(dialogId, im, otherId, page):
    ''' Вывод сообщений при включённом javaScript.
   "Чтение" сообщений инициируется со стороны клиента по webSocket.'''
    allMesages = personalMessage.objects.filter(inDialog_id__exact = dialogId).order_by("time")
    if allMesages.exists():
        messagesPaginator = Paginator(allMesages, 10, orphans = 15)
        newPage = messagesPaginator.num_pages - page
        if newPage > 0:
            messagesList = []
            unreadMessagesDictDict = caches["unreadPersonalMessages"].get(dialogId, None)
            messagesMany = messagesPaginator.page(newPage).object_list.values("id", "by_id", "body", "time", "imagesAttachment")
            imageAttachmentIds = messagesMany.values("imagesAttachment")
            if unreadMessagesDictDict is not None:
                unreadMessagesByUserSet = unreadMessagesDictDict.get(im, None)
                unreadMessagesMySet = unreadMessagesDictDict.get(otherId, None)
                if unreadMessagesByUserSet is not None or unreadMessagesMySet is not None:
                    if unreadMessagesByUserSet is not None and unreadMessagesMySet is not None:
                        myUnreadMessagesList = []
                        opponentUnreadMessagesList = []
                        if imageAttachmentIds.count() > 0:
                            imageAttachmentMany = imagesAttachmentMessagePersonal.objects.filter(message_id__in = imageAttachmentIds).values("message_id", "image1", "image2", "image3", "image4", "image5", "image6", "itemsLength")
                            for messageItem in messagesMany:
                                messageId = str(messageItem.get("id"))
                                if messageId in unreadMessagesByUserSet:
                                    opponentUnreadMessagesList.append(messageId)
                                    unreadMessagesByUserSet.remove(messageId)
                                elif messageId in unreadMessagesMySet:
                                    myUnreadMessagesList.append(messageId)
                                    unreadMessagesMySet.remove(messageId)
                                try:
                                    imagesData = imageAttachmentMany.get(message_id__exact = messageId)
                                except imagesAttachmentMessagePersonal.DoesNotExist:
                                    imagesData = None
                                if imagesData is not None:
                                    media = settings.MEDIA_URL
                                    imagesLen = imagesData.get('itemsLength')
                                    if imagesLen > 3:
                                        if imagesLen == 4:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4'))]
                                        elif imagesLen == 5:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4')), '{0}{1}'.format(media, imagesData.get('image5'))]
                                        else:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4')), '{0}{1}'.format(media, imagesData.get('image5')), '{0}{1}'.format(media, imagesData.get('image6'))]
                                    else:
                                        if imagesLen == 1:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1'))]
                                        elif imagesLen == 2:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2'))]
                                        else:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3'))]
                                    messagesList.append({
                                        'id': messageId,
                                        'body': messageItem.get('body'),
                                        'by': messageItem.get('by_id'),
                                        'time': str(messageItem.get('time')),
                                        'images': imagesList
                                        })
                                else:
                                    messagesList.append({
                                        'id': messageId,
                                        'body': messageItem.get('body'),
                                        'by': messageItem.get('by_id'),
                                        'time': str(messageItem.get('time'))
                                        })
                            return messagesList, {im: myUnreadMessagesList, otherId: opponentUnreadMessagesList}
                        for messageItem in messagesMany:
                            if messageId in unreadMessagesByUserSet:
                                opponentUnreadMessagesList.append(messageId)
                                unreadMessagesByUserSet.remove(messageId)
                            elif messageId in unreadMessagesMySet:
                                myUnreadMessagesList.append(messageId)
                                unreadMessagesMySet.remove(messageId)
                                messagesList.append({
                                    'id': str(messageItem.get("id")),
                                    'body': messageItem.get('body'),
                                    'by': messageItem.get('by_id'),
                                    'time': str(messageItem.get('time'))
                                    })
                        return messagesList, {im: myUnreadMessagesList, otherId: opponentUnreadMessagesList}
                    if unreadMessagesByUserSet is not None:
                        opponentUnreadMessagesList = []
                        if imageAttachmentIds.count() > 0:
                            imageAttachmentMany = imagesAttachmentMessagePersonal.objects.filter(message_id__in = imageAttachmentIds).values("message_id", "image1", "image2", "image3", "image4", "image5", "image6", "itemsLength")
                            for messageItem in messagesMany:
                                messageId = str(messageItem.get("id"))
                                if messageId in unreadMessagesByUserSet:
                                    opponentUnreadMessagesList.append(messageId)
                                    unreadMessagesByUserSet.remove(messageId)
                                try:
                                    imagesData = imageAttachmentMany.get(message_id__exact = messageId)
                                except imagesAttachmentMessagePersonal.DoesNotExist:
                                    imagesData = None
                                if imagesData is not None:
                                    media = settings.MEDIA_URL
                                    imagesLen = imagesData.get('itemsLength')
                                    if imagesLen > 3:
                                        if imagesLen == 4:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4'))]
                                        elif imagesLen == 5:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4')), '{0}{1}'.format(media, imagesData.get('image5'))]
                                        else:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4')), '{0}{1}'.format(media, imagesData.get('image5')), '{0}{1}'.format(media, imagesData.get('image6'))]
                                    else:
                                        if imagesLen == 1:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1'))]
                                        elif imagesLen == 2:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2'))]
                                        else:
                                            imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3'))]
                                    messagesList.append({
                                        'id': messageId,
                                        'body': messageItem.get('body'),
                                        'by': messageItem.get('by_id'),
                                        'time': str(messageItem.get('time')),
                                        'images': imagesList
                                        })
                                else:
                                    messagesList.append({
                                        'id': messageId,
                                        'body': messageItem.get('body'),
                                        'by': messageItem.get('by_id'),
                                        'time': str(messageItem.get('time'))
                                        })
                            return messagesList, {otherId: opponentUnreadMessagesList}
                        for messageItem in messagesMany:
                            if messageId in unreadMessagesByUserSet:
                                opponentUnreadMessagesList.append(messageId)
                                unreadMessagesByUserSet.remove(messageId)
                                messagesList.append({
                                    'id': str(messageItem.get("id")),
                                    'body': messageItem.get('body'),
                                    'by': messageItem.get('by_id'),
                                    'time': str(messageItem.get('time'))
                                    })
                        return messagesList, {otherId: opponentUnreadMessagesList}
                    myUnreadMessagesList = []
                    if imageAttachmentIds.count() > 0:
                        imageAttachmentMany = imagesAttachmentMessagePersonal.objects.filter(message_id__in = imageAttachmentIds).values("message_id", "image1", "image2", "image3", "image4", "image5", "image6", "itemsLength")
                        for messageItem in messagesMany:
                            messageId = str(messageItem.get("id"))
                            if messageId in unreadMessagesMySet:
                                myUnreadMessagesList.append(messageId)
                                unreadMessagesMySet.remove(messageId)
                            try:
                                imagesData = imageAttachmentMany.get(message_id__exact = messageId)
                            except imagesAttachmentMessagePersonal.DoesNotExist:
                                imagesData = None
                            if imagesData is not None:
                                media = settings.MEDIA_URL
                                imagesLen = imagesData.get('itemsLength')
                                if imagesLen > 3:
                                    if imagesLen == 4:
                                        imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4'))]
                                    elif imagesLen == 5:
                                        imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4')), '{0}{1}'.format(media, imagesData.get('image5'))]
                                    else:
                                        imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4')), '{0}{1}'.format(media, imagesData.get('image5')), '{0}{1}'.format(media, imagesData.get('image6'))]
                                else:
                                    if imagesLen == 1:
                                        imagesList = ['{0}{1}'.format(media, imagesData.get('image1'))]
                                    elif imagesLen == 2:
                                        imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2'))]
                                    else:
                                        imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3'))]
                                messagesList.append({
                                    'id': messageId,
                                    'body': messageItem.get('body'),
                                    'by': messageItem.get('by_id'),
                                    'time': str(messageItem.get('time')),
                                    'images': imagesList
                                    })
                            else:
                                messagesList.append({
                                    'id': messageId,
                                    'body': messageItem.get('body'),
                                    'by': messageItem.get('by_id'),
                                    'time': str(messageItem.get('time'))
                                    })
                        return messagesList, {im: myUnreadMessagesList}
                    for messageItem in messagesMany:
                        if messageId in unreadMessagesMySet:
                            myUnreadMessagesList.append(messageId)
                            unreadMessagesMySet.remove(messageId)
                            messagesList.append({
                                'id': str(messageItem.get("id")),
                                'body': messageItem.get('body'),
                                'by': messageItem.get('by_id'),
                                'time': str(messageItem.get('time'))
                                })
                    return messagesList, {im: myUnreadMessagesList}
                if imageAttachmentIds.count() > 0:
                    imageAttachmentMany = imagesAttachmentMessagePersonal.objects.filter(message_id__in = imageAttachmentIds).values("message_id", "image1", "image2", "image3", "image4", "image5", "image6", "itemsLength")
                    for messageItem in messagesMany:
                        messageId = str(messageItem.get('id'))
                        try:
                            imagesData = imageAttachmentMany.get(message_id__exact = messageId)
                        except imagesAttachmentMessagePersonal.DoesNotExist:
                            imagesData = None
                        imagesData = messageItem.get('imagesAttachment', None)
                        if imagesData is not None:
                            media = settings.MEDIA_URL
                            imagesLen = imagesData.get('itemsLength')
                            if imagesLen > 3:
                                if imagesLen == 4:
                                    imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4'))]
                                elif imagesLen == 5:
                                    imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4')), '{0}{1}'.format(media, imagesData.get('image5'))]
                                else:
                                    imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4')), '{0}{1}'.format(media, imagesData.get('image5')), '{0}{1}'.format(media, imagesData.get('image6'))]
                            else:
                                if imagesLen == 1:
                                    imagesList = ['{0}{1}'.format(media, imagesData.get('image1'))]
                                elif imagesLen == 2:
                                    imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2'))]
                                else:
                                    imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3'))]
                            messagesList.append({
                                'id': messageId,
                                'body': messageItem.get('body'),
                                'by': messageItem.get('by_id'),
                                'time': str(messageItem.get('time')),
                                'images': imagesList
                                })
                        else:
                            messagesList.append({
                                'id': messageId,
                                'body': messageItem.get('body'),
                                'by': messageItem.get('by_id'),
                                'time': str(messageItem.get('time'))
                                })
                    return messagesList, None
                for messageItem in messagesMany:
                    messagesList.append({
                        'id': str(messageItem.get("id")),
                        'body': messageItem.get('body'),
                        'by': messageItem.get('by_id'),
                        'time': str(messageItem.get('time'))
                        })
                return messagesList, None
            if imageAttachmentIds.count() > 0:
                imageAttachmentMany = imagesAttachmentMessagePersonal.objects.filter(message_id__in = imageAttachmentIds).values("message_id", "image1", "image2", "image3", "image4", "image5", "image6", "itemsLength")
                for messageItem in messagesMany:
                    messageId = str(messageItem.get("id"))
                    try:
                        imagesData = imageAttachmentMany.get(message_id__exact = messageId)
                    except imagesAttachmentMessagePersonal.DoesNotExist:
                        imagesData = None
                    if imagesData is not None:
                        media = settings.MEDIA_URL
                        imagesLen = imagesData.get('itemsLength')
                        if imagesLen > 3:
                            if imagesLen == 4:
                                imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4'))]
                            elif imagesLen == 5:
                                imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4')), '{0}{1}'.format(media, imagesData.get('image5'))]
                            else:
                                imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3')), '{0}{1}'.format(media, imagesData.get('image4')), '{0}{1}'.format(media, imagesData.get('image5')), '{0}{1}'.format(media, imagesData.get('image6'))]
                        else:
                            if imagesLen == 1:
                                imagesList = ['{0}{1}'.format(media, imagesData.get('image1'))]
                            elif imagesLen == 2:
                                imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2'))]
                            else:
                                imagesList = ['{0}{1}'.format(media, imagesData.get('image1')), '{0}{1}'.format(media, imagesData.get('image2')), '{0}{1}'.format(media, imagesData.get('image3'))]
                        messagesList.append({
                            'id': messageId,
                            'body': messageItem.get('body'),
                            'by': messageItem.get('by_id'),
                            'time': str(messageItem.get('time')),
                            'images': imagesList
                            })
                    else:
                        messagesList.append({
                            'id': messageId,
                            'body': messageItem.get('body'),
                            'by': messageItem.get('by_id'),
                            'time': str(messageItem.get('time'))
                            })
                return messagesList, None
            for messageItem in messagesMany:
                messagesList.append({
                    'id': str(messageItem.get("id")),
                    'body': messageItem.get('body'),
                    'by': messageItem.get('by_id'),
                    'time': str(messageItem.get('time'))
                    })
            return messagesList, None
        return [], None
    return [], None

def filesLoaderPersonal(request, toUserId, type, **pars):
    if request.is_ajax() and request.method == "POST":
        try:
            toUserInstance = mainUserProfile.objects.get(pk = toUserId)
        except mainUserProfile.DoesNotExist:
            return JsonResponse({})
        curUser = request.user
        myId = str(curUser.id)
        privacyCache = caches['privacyCache']
        privacyDictDict = privacyCache.get_many({toUserId, myId})
        privacyDict = privacyDictDict.get(toUserId, None)
        friendShipStatus = None
        privacyDictHasChanges = False
        privacyDictMyHasChanges = False
        friendsIds = None
        if privacyDict is None:
            privacySettings = userProfilePrivacy.objects.get(user__exact = toUserInstance)
            friendShipCache = caches['friendShipStatusCache']
            friendShipStatusDict = friendShipCache.get(myId, None)
            if friendShipStatusDict is None:
                friendsIdsCache = caches['friendsIdCache']
                friendsIds = friendsIdsCache.get(myId, None)
                if friendsIds is None:
                    friendsCache = caches['friendsCache']
                    cachedFriends = friendsCache.get(myId, None)
                    if cachedFriends is None:
                        cachedFriends = curUser.friends.all()
                        if cachedFriends.count():
                            friendsCache.set(myId, cachedFriends)
                        else:
                            friendsCache.set(myId, False)
                    if cachedFriends is not False:
                        friendsIdsQuerySet = cachedFriends.values("id")
                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                    else:
                        friendsIds = set()
                    friendsIdsCache.set(myId, friendsIds)
                if toUserId in friendsIds:
                    privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = True, isAuthenticated = True)
                    friendShipCache.set(myId, {toUserId: True})
                else:
                    friendShipRequestsCache = caches['friendshipRequests']
                    friendShipRequests = friendShipRequestsCache.get(myId, None)
                    friendShipStatus = False
                    privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = False, isAuthenticated = True)
                    if friendShipRequests is not None:
                        for friendShipRequest in friendShipRequests:
                            if friendShipRequest[0] == toUserId:
                                friendShipStatus = "request"
                                privacyChecker.isFriend = True
                                break
                        if friendShipStatus is False:
                            friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                            if friendShipRequests is not None:
                                for friendShipRequest in friendShipRequests:
                                    if friendShipRequest[0] == myId:
                                        friendShipStatus = "myRequest"
                                        break
                    friendShipCache.set(myId, {toUserId: friendShipStatus})
            else:
                friendShipStatus = friendShipStatusDict.get(toUserId, None)
                if friendShipStatus is None:
                    friendsIdsCache = caches['friendsIdCache']
                    friendsIds = friendsIdsCache.get(myId, None)
                    if friendsIds is None:
                        friendsCache = caches['friendsCache']
                        cachedFriends = friendsCache.get(myId, None)
                        if cachedFriends is None:
                            cachedFriends = curUser.friends.all()
                            if cachedFriends.count():
                                friendsCache.set(myId, cachedFriends)
                            else:
                                friendsCache.set(myId, False)
                        if cachedFriends is not False:
                            friendsIdsQuerySet = cachedFriends.values("id")
                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                        else:
                            friendsIds = set()
                        friendsIdsCache.set(myId, friendsIds)
                    if toUserId in friendsIds:
                        privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = True, isAuthenticated = True)
                        friendShipStatusDict.update({toUserId: True})
                    else:
                        friendShipRequestsCache = caches['friendshipRequests']
                        friendShipRequests = friendShipRequestsCache.get(myId, None)
                        friendShipStatus = False
                        privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = False, isAuthenticated = True)
                        if friendShipRequests is not None:
                            for friendShipRequest in friendShipRequests:
                                if friendShipRequest[0] == toUserId:
                                    friendShipStatus = "request"
                                    privacyChecker.isFriend = True
                                    break
                            if friendShipStatus is False:
                                friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                if friendShipRequests is not None:
                                    for friendShipRequest in friendShipRequests:
                                        if friendShipRequest[0] == myId:
                                            friendShipStatus = "myRequest"
                                            break
                        friendShipStatusDict.update({toUserId: friendShipStatus})
                    friendShipCache.set(myId, friendShipStatusDict)
                else:
                    privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
            sendMessages = privacyChecker.sendPersonalMessages()
            privacyDictHasChanges = None
            privacyDictDict = {toUserId: {myId: {'sendPersonalMessages': sendMessages}}}
        else:
            userPrivacyDict = privacyDict.get(myId, None)
            if userPrivacyDict is None:
                privacySettings = userProfilePrivacy.objects.get(user__exact = toUserInstance)
                friendShipCache = caches['friendShipStatusCache']
                friendShipStatusDict = friendShipCache.get(myId, None)
                if friendShipStatusDict is None:
                    friendsIdsCache = caches['friendsIdCache']
                    friendsIds = friendsIdsCache.get(myId, None)
                    if friendsIds is None:
                        friendsCache = caches['friendsCache']
                        cachedFriends = friendsCache.get(myId, None)
                        if cachedFriends is None:
                            cachedFriends = curUser.friends.all()
                            if cachedFriends.count():
                                friendsCache.set(myId, cachedFriends)
                            else:
                                friendsCache.set(myId, False)
                        if cachedFriends is not False:
                            friendsIdsQuerySet = cachedFriends.values("id")
                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                        else:
                            friendsIds = set()
                        friendsIdsCache.set(myId, friendsIds)
                    if toUserId in friendsIds:
                        privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = True, isAuthenticated = True)
                        friendShipCache.set(myId, {toUserId: True})
                    else:
                        friendShipRequestsCache = caches['friendshipRequests']
                        friendShipRequests = friendShipRequestsCache.get(myId, None)
                        friendShipStatus = False
                        privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = False, isAuthenticated = True)
                        if friendShipRequests is not None:
                            for friendShipRequest in friendShipRequests:
                                if friendShipRequest[0] == toUserId:
                                    friendShipStatus = "request"
                                    privacyChecker.isFriend = True
                                    break
                            if friendShipStatus is False:
                                friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                if friendShipRequests is not None:
                                    for friendShipRequest in friendShipRequests:
                                        if friendShipRequest[0] == myId:
                                            friendShipStatus = "myRequest"
                                            break
                        friendShipCache.set(myId, {toUserId: friendShipStatus})
                else:
                    friendShipStatus = friendShipStatusDict.get(toUserId, None)
                    if friendShipStatus is None:
                        friendsIdsCache = caches['friendsIdCache']
                        friendsIds = friendsIdsCache.get(myId, None)
                        if friendsIds is None:
                            friendsCache = caches['friendsCache']
                            cachedFriends = friendsCache.get(myId, None)
                            if cachedFriends is None:
                                cachedFriends = curUser.friends.all()
                                if cachedFriends.count():
                                    friendsCache.set(myId, cachedFriends)
                                else:
                                    friendsCache.set(myId, False)
                            if cachedFriends is not False:
                                friendsIdsQuerySet = cachedFriends.values("id")
                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                            else:
                                friendsIds = set()
                            friendsIdsCache.set(myId, friendsIds)
                        if toUserId in friendsIds:
                            privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = True, isAuthenticated = True)
                            friendShipStatusDict.update({toUserId: True})
                        else:
                            friendShipRequestsCache = caches['friendshipRequests']
                            friendShipRequests = friendShipRequestsCache.get(myId, None)
                            friendShipStatus = False
                            privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = False, isAuthenticated = True)
                            if friendShipRequests is not None:
                                for friendShipRequest in friendShipRequests:
                                    if friendShipRequest[0] == toUserId:
                                        friendShipStatus = "request"
                                        privacyChecker.isFriend = True
                                        break
                                if friendShipStatus is False:
                                    friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                    if friendShipRequests is not None:
                                        for friendShipRequest in friendShipRequests:
                                            if friendShipRequest[0] == myId:
                                                friendShipStatus = "myRequest"
                                                break
                            friendShipStatusDict.update({toUserId: friendShipStatus})
                        friendShipCache.set(myId, friendShipStatusDict)
                    else:
                        privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                sendMessages = privacyChecker.sendPersonalMessages()
                privacyDictHasChanges = True
                privacyDict.update({myId: {'sendPersonalMessages': sendMessages}})
            else:
                sendMessages = userPrivacyDict.get('sendPersonalMessages', None)
                if sendMessages is None:
                    privacySettings = userProfilePrivacy.objects.get(user__exact = toUserInstance)
                    friendShipCache = caches['friendShipStatusCache']
                    friendShipStatusDict = friendShipCache.get(myId, None)
                    if friendShipStatusDict is None:
                        friendsIdsCache = caches['friendsIdCache']
                        friendsIds = friendsIdsCache.get(myId, None)
                        if friendsIds is None:
                            friendsCache = caches['friendsCache']
                            cachedFriends = friendsCache.get(myId, None)
                            if cachedFriends is None:
                                cachedFriends = curUser.friends.all()
                                if cachedFriends.count():
                                    friendsCache.set(myId, cachedFriends)
                                else:
                                    friendsCache.set(myId, False)
                            if cachedFriends is not False:
                                friendsIdsQuerySet = cachedFriends.values("id")
                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                            else:
                                friendsIds = set()
                            friendsIdsCache.set(myId, friendsIds)
                        if toUserId in friendsIds:
                            privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = True, isAuthenticated = True)
                            friendShipCache.set(myId, {toUserId: True})
                        else:
                            friendShipRequestsCache = caches['friendshipRequests']
                            friendShipRequests = friendShipRequestsCache.get(myId, None)
                            friendShipStatus = False
                            privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = False, isAuthenticated = True)
                            if friendShipRequests is not None:
                                for friendShipRequest in friendShipRequests:
                                    if friendShipRequest[0] == toUserId:
                                        friendShipStatus = "request"
                                        privacyChecker.isFriend = True
                                        break
                                if friendShipStatus is False:
                                    friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                    if friendShipRequests is not None:
                                        for friendShipRequest in friendShipRequests:
                                            if friendShipRequest[0] == myId:
                                                friendShipStatus = "myRequest"
                                                break
                            friendShipCache.set(myId, {toUserId: friendShipStatus})
                    else:
                        friendShipStatus = friendShipStatusDict.get(toUserId, None)
                        if friendShipStatus is None:
                            friendsIdsCache = caches['friendsIdCache']
                            friendsIds = friendsIdsCache.get(myId, None)
                            if friendsIds is None:
                                friendsCache = caches['friendsCache']
                                cachedFriends = friendsCache.get(myId, None)
                                if cachedFriends is None:
                                    cachedFriends = curUser.friends.all()
                                    if cachedFriends.count():
                                        friendsCache.set(myId, cachedFriends)
                                    else:
                                        friendsCache.set(myId, False)
                                if cachedFriends is not False:
                                    friendsIdsQuerySet = cachedFriends.values("id")
                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                else:
                                    friendsIds = set()
                                friendsIdsCache.set(myId, friendsIds)
                            if toUserId in friendsIds:
                                privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                friendShipStatusDict.update({toUserId: True})
                            else:
                                friendShipRequestsCache = caches['friendshipRequests']
                                friendShipRequests = friendShipRequestsCache.get(myId, None)
                                friendShipStatus = False
                                privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                if friendShipRequests is not None:
                                    for friendShipRequest in friendShipRequests:
                                        if friendShipRequest[0] == toUserId:
                                            friendShipStatus = "request"
                                            privacyChecker.isFriend = True
                                            break
                                    if friendShipStatus is False:
                                        friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                        if friendShipRequests is not None:
                                            for friendShipRequest in friendShipRequests:
                                                if friendShipRequest[0] == myId:
                                                    friendShipStatus = "myRequest"
                                                    break
                                friendShipStatusDict.update({toUserId: friendShipStatus})
                            friendShipCache.set(myId, friendShipStatusDict)
                        else:
                            privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                    if sendMessages is None:
                        sendMessages = privacyChecker.sendPersonalMessages()
                        userPrivacyDict.update({'sendPersonalMessages': sendMessages})
                    privacyDictHasChanges = True
        privacyDictMy = privacyDictDict.get(myId, None)
        if privacyDictMy is None:
            privacySettings = userProfilePrivacy.objects.get(user__exact = curUser)
            if friendShipStatus is None:
                friendShipCache = caches['friendShipStatusCache']
                friendShipStatusDict = friendShipCache.get(myId, None)
                if friendShipStatusDict is None:
                    friendsIdsCache = caches['friendsIdCache']
                    friendsIds = friendsIdsCache.get(myId, None)
                    if friendsIds is None:
                        friendsCache = caches['friendsCache']
                        cachedFriends = friendsCache.get(myId, None)
                        if cachedFriends is None:
                            cachedFriends = curUser.friends.all()
                            if cachedFriends.count():
                                friendsCache.set(myId, cachedFriends)
                            else:
                                friendsCache.set(myId, False)
                        if cachedFriends is not False:
                            friendsIdsQuerySet = cachedFriends.values("id")
                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                        else:
                            friendsIds = set()
                        friendsIdsCache.set(myId, friendsIds)
                    if toUserId in friendsIds:
                        friendShipCache.set(myId, {toUserId: True})
                    else:
                        friendShipRequestsCache = caches['friendshipRequests']
                        friendShipRequests = friendShipRequestsCache.get(myId, None)
                        friendShipStatus = False
                        if friendShipRequests is not None:
                            for friendShipRequest in friendShipRequests:
                                if friendShipRequest[0] == toUserId:
                                    friendShipStatus = "request"
                                    break
                            if friendShipStatus is False:
                                friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                if friendShipRequests is not None:
                                    for friendShipRequest in friendShipRequests:
                                        if friendShipRequest[0] == myId:
                                            friendShipStatus = "myRequest"
                                            break
                        friendShipCache.set(myId, {toUserId: friendShipStatus})
                else:
                    friendShipStatus = friendShipStatusDict.get(toUserId, None)
                    if friendShipStatus is None:
                        friendsIdsCache = caches['friendsIdCache']
                        friendsIds = friendsIdsCache.get(myId, None)
                        if friendsIds is None:
                            friendsCache = caches['friendsCache']
                            cachedFriends = friendsCache.get(myId, None)
                            if cachedFriends is None:
                                cachedFriends = curUser.friends.all()
                                if cachedFriends.count():
                                    friendsCache.set(myId, cachedFriends)
                                else:
                                    friendsCache.set(myId, False)
                            if cachedFriends is not False:
                                friendsIdsQuerySet = cachedFriends.values("id")
                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                            else:
                                friendsIds = set()
                            friendsIdsCache.set(myId, friendsIds)
                        if toUserId in friendsIds:
                            friendShipStatusDict.update({toUserId: True})
                        else:
                            friendShipRequestsCache = caches['friendshipRequests']
                            friendShipRequests = friendShipRequestsCache.get(myId, None)
                            friendShipStatus = False
                            if friendShipRequests is not None:
                                for friendShipRequest in friendShipRequests:
                                    if friendShipRequest[0] == toUserId:
                                        friendShipStatus = "request"
                                        break
                                if friendShipStatus is False:
                                    friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                    if friendShipRequests is not None:
                                        for friendShipRequest in friendShipRequests:
                                            if friendShipRequest[0] == myId:
                                                friendShipStatus = "myRequest"
                                                break
            privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
            sendMessagesMyself = privacyChecker.sendPersonalMessages()
            privacyDictMyHasChanges = None
            privacyDictDict.update({myId: {toUserId: {'sendPersonalMessages': sendMessagesMyself}}})
        else:
            userPrivacyDictMy = privacyDictMy.get(toUserId, None)
            if userPrivacyDictMy is None:
                privacySettings = userProfilePrivacy.objects.get(user__exact = curUser)
                if friendShipStatus is None:
                    friendShipCache = caches['friendShipStatusCache']
                    friendShipStatusDict = friendShipCache.get(myId, None)
                    if friendShipStatusDict is None:
                        friendsIdsCache = caches['friendsIdCache']
                        friendsIds = friendsIdsCache.get(myId, None)
                        if friendsIds is None:
                            friendsCache = caches['friendsCache']
                            cachedFriends = friendsCache.get(myId, None)
                            if cachedFriends is None:
                                cachedFriends = curUser.friends.all()
                                if cachedFriends.count():
                                    friendsCache.set(myId, cachedFriends)
                                else:
                                    friendsCache.set(myId, False)
                            if cachedFriends is not False:
                                friendsIdsQuerySet = cachedFriends.values("id")
                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                            else:
                                friendsIds = set()
                            friendsIdsCache.set(myId, friendsIds)
                        if toUserId in friendsIds:
                            friendShipCache.set(myId, {toUserId: True})
                        else:
                            friendShipRequestsCache = caches['friendshipRequests']
                            friendShipRequests = friendShipRequestsCache.get(myId, None)
                            friendShipStatus = False
                            if friendShipRequests is not None:
                                for friendShipRequest in friendShipRequests:
                                    if friendShipRequest[0] == toUserId:
                                        friendShipStatus = "request"
                                        break
                                if friendShipStatus is False:
                                    friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                    if friendShipRequests is not None:
                                        for friendShipRequest in friendShipRequests:
                                            if friendShipRequest[0] == myId:
                                                friendShipStatus = "myRequest"
                                                break
                            friendShipCache.set(myId, {toUserId: friendShipStatus})
                    else:
                        friendShipStatus = friendShipStatusDict.get(toUserId, None)
                        if friendShipStatus is None:
                            friendsIdsCache = caches['friendsIdCache']
                            friendsIds = friendsIdsCache.get(myId, None)
                            if friendsIds is None:
                                friendsCache = caches['friendsCache']
                                cachedFriends = friendsCache.get(myId, None)
                                if cachedFriends is None:
                                    cachedFriends = curUser.friends.all()
                                    if cachedFriends.count():
                                        friendsCache.set(myId, cachedFriends)
                                    else:
                                        friendsCache.set(myId, False)
                                if cachedFriends is not False:
                                    friendsIdsQuerySet = cachedFriends.values("id")
                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                else:
                                    friendsIds = set()
                                friendsIdsCache.set(myId, friendsIds)
                            if toUserId in friendsIds:
                                friendShipStatusDict.update({toUserId: True})
                            else:
                                friendShipRequestsCache = caches['friendshipRequests']
                                friendShipRequests = friendShipRequestsCache.get(myId, None)
                                friendShipStatus = False
                                if friendShipRequests is not None:
                                    for friendShipRequest in friendShipRequests:
                                        if friendShipRequest[0] == toUserId:
                                            friendShipStatus = "request"
                                            break
                                    if friendShipStatus is False:
                                        friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                        if friendShipRequests is not None:
                                            for friendShipRequest in friendShipRequests:
                                                if friendShipRequest[0] == myId:
                                                    friendShipStatus = "myRequest"
                                                    break
                privacyChecker = checkPrivacy(user = curUser, privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                sendMessagesMyself = privacyChecker.sendPersonalMessages()
                privacyDictMyHasChanges = True
                privacyDictMy.update({toUserId: {'sendPersonalMessages': sendMessagesMyself}})
            else:
                sendMessagesMyself = userPrivacyDictMy.get('sendPersonalMessages', None)
                if sendMessagesMyself is None:
                    privacySettings = userProfilePrivacy.objects.get(user__exact = curUser)
                    if friendShipStatus is None:
                        friendShipCache = caches['friendShipStatusCache']
                        friendShipStatusDict = friendShipCache.get(myId, None)
                        if friendShipStatusDict is None:
                            friendsIdsCache = caches['friendsIdCache']
                            friendsIds = friendsIdsCache.get(myId, None)
                            if friendsIds is None:
                                friendsCache = caches['friendsCache']
                                cachedFriends = friendsCache.get(myId, None)
                                if cachedFriends is None:
                                    cachedFriends = curUser.friends.all()
                                    if cachedFriends.count():
                                        friendsCache.set(myId, cachedFriends)
                                    else:
                                        friendsCache.set(myId, False)
                                if cachedFriends is not False:
                                    friendsIdsQuerySet = cachedFriends.values("id")
                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                else:
                                    friendsIds = set()
                                friendsIdsCache.set(myId, friendsIds)
                            if toUserId in friendsIds:
                                friendShipCache.set(myId, {toUserId: True})
                            else:
                                friendShipRequestsCache = caches['friendshipRequests']
                                friendShipRequests = friendShipRequestsCache.get(myId, None)
                                friendShipStatus = False
                                if friendShipRequests is not None:
                                    for friendShipRequest in friendShipRequests:
                                        if friendShipRequest[0] == toUserId:
                                            friendShipStatus = "request"
                                            break
                                    if friendShipStatus is False:
                                        friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                        if friendShipRequests is not None:
                                            for friendShipRequest in friendShipRequests:
                                                if friendShipRequest[0] == myId:
                                                    friendShipStatus = "myRequest"
                                                    break
                                friendShipCache.set(myId, {toUserId: friendShipStatus})
                        else:
                            friendShipStatus = friendShipStatusDict.get(toUserId, None)
                            if friendShipStatus is None:
                                friendsIdsCache = caches['friendsIdCache']
                                friendsIds = friendsIdsCache.get(myId, None)
                                if friendsIds is None:
                                    friendsCache = caches['friendsCache']
                                    cachedFriends = friendsCache.get(myId, None)
                                    if cachedFriends is None:
                                        cachedFriends = curUser.friends.all()
                                        if cachedFriends.count():
                                            friendsCache.set(myId, cachedFriends)
                                        else:
                                            friendsCache.set(myId, False)
                                    if cachedFriends is not False:
                                        friendsIdsQuerySet = cachedFriends.values("id")
                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    else:
                                        friendsIds = set()
                                    friendsIdsCache.set(myId, friendsIds)
                                if toUserId in friendsIds:
                                    friendShipStatusDict.update({toUserId: True})
                                else:
                                    friendShipRequestsCache = caches['friendshipRequests']
                                    friendShipRequests = friendShipRequestsCache.get(myId, None)
                                    friendShipStatus = False
                                    if friendShipRequests is not None:
                                        for friendShipRequest in friendShipRequests:
                                            if friendShipRequest[0] == toUserId:
                                                friendShipStatus = "request"
                                                break
                                        if friendShipStatus is False:
                                            friendShipRequests = friendShipRequestsCache.get(toUserId, None)
                                            if friendShipRequests is not None:
                                                for friendShipRequest in friendShipRequests:
                                                    if friendShipRequest[0] == myId:
                                                        friendShipStatus = "myRequest"
                                                        break
                    privacyChecker = checkPrivacy(user = toUserInstance, privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                    if sendMessagesMyself:
                        sendMessagesMyself = privacyChecker.sendPersonalMessages()
                        userPrivacyDictMy.update({'sendPersonalMessages': sendMessagesMyself})
                    privacyDictMyHasChanges = True
        if privacyDictHasChanges or privacyDictMyHasChanges:
            if privacyDictHasChanges and privacyDictMyHasChanges:
                privacyCache.set_many({myId: privacyDictMy, toUserId: privacyDict})
            else:
                if privacyDictHasChanges:
                    privacyCache.set(toUserId, privacyDict)
                elif privacyDictMyHasChanges:
                    privacyCache.set(myId, privacyDictMy)
        else:
            if privacyDictHasChanges is None and privacyDictMyHasChanges is None:
                privacyCache.set_many(privacyDictDict)
        if sendMessages and sendMessagesMyself:
            file = request.FILES.get('insertion', None)
            if file is None:
                return JsonResponse({"loadFiles": None})
            errors = False
            if type == "image":
                if file.size > 10242880:
                    return JsonResponse({"loadFiles": "tooBig"})
                fileValidatorInstance = FileExtensionValidator(['jpeg', 'jpg', 'gif', 'png'])
                try:
                    fileValidatorInstance(file)
                except ValidationError as error:
                    errors = '. '.join(error.messages)
                if errors is False:
                    try:
                        validate_image_file_extension(file)
                    except ValidationError as error:
                        '{0}. {1}'.format(errors, '. '.join(error.messages))
                if errors is not False:
                    return JsonResponse({"loadFiles": errors})
                dialogCache = caches['dialogSingle']
                cachedDialog = dialogCache.get(myId, None)
                if cachedDialog is None:
                    dialog = dialogMessage.objects.filter(users__exact = curUser).filter(users__exact = toUserInstance).filter(type__exact = True)
                    if dialog.count() == 0:
                        dialog = dialogMessage.objects.create(type = True)
                        dialog.users.add(curUser, toUserInstance)
                    else:
                        dialog = dialog[0]
                    dialogCache.set(myId, {'instance': dialog, 'withUserId': toUserId})
                else:
                    withUser = cachedDialog.get('withUserId', None)
                    if withUser is None:
                        dialog = dialogMessage.objects.filter(users__exact = curUser).filter(users__exact = toUserInstance).filter(type__exact = True)
                        if dialog.count() == 0:
                            dialog = dialogMessage.objects.create(type = True)
                            dialog.users.add(curUser, toUserInstance)
                        else:
                            dialog = dialog[0]
                        dialogCache.set(myId, {'instance': dialog, 'withUserId': toUserId})
                    else:
                        dialog = cachedDialog.get('instance', None)
                        if dialog is None:
                            dialog = dialogMessage.objects.filter(users__exact = curUser).filter(users__exact = toUserInstance).filter(type__exact = True)
                            if dialog.count() == 0:
                                dialog = dialogMessage.objects.create(type = True)
                                dialog.users.add(curUser, toUserInstance)
                            else:
                                dialog = dialog[0]
                            dialogCache.set(myId, {'instance': dialog, 'withUserId': toUserId})
                        else:
                            if withUser != toUserId:
                                dialog = dialogMessage.objects.filter(users__exact = curUser).filter(users__exact = toUserInstance).filter(type__exact = True)
                                if dialog.count() == 0:
                                    dialog = dialogMessage.objects.create(type = True)
                                    dialog.users.add(curUser, toUserInstance)
                                else:
                                    dialog = dialog[0]
                                dialogCache.set(myId, {'instance': dialog, 'withUserId': toUserId})
                messagesFilesStorage = caches['filesStorageMessages']
                dialogId = str(dialog.id)
                removeFiles = pars.pop('remove', None)
                if removeFiles is not None:
                    cachedFilesDict = messagesFilesStorage.get(dialogId, None)
                    if cachedFilesDict is None:
                        return JsonResponse({"removed": None})
                    else:
                        cachedFilesDictDict = cachedFilesDict.get(myId, None)
                        if cachedFilesDictDict is None:
                            return JsonResponse({"removed": None})
                        else:
                            cachedFilesDictDictImages = cachedFilesDictDict.get("images", None)
                            if cachedFilesDictDictImages is None:
                                return JsonResponse({"removed": None})
                            else:
                                removedNames = set()
                                for name in removeFiles:
                                    removed = cachedFilesDictDict.pop(name, None)
                                    if removed is not None:
                                        removedNames.add(removed)
                                if len(removedNames) > 0:
                                    if len(cachedFilesDictDict) == 0:
                                        del cachedFilesDict[myId]
                                    messagesFilesStorage.set(dialogId, cachedFilesDict)
                                    return JsonResponse({"removed": removedNames})
                cachedFilesDict = messagesFilesStorage.get(dialogId, None)
                name = file.name
                dotIndex = name.index(".")
                encodedImage = base64.b64encode(file.read()).decode("utf-8")
                if cachedFilesDict is None:
                    messagesFilesStorage.set(dialogId, {myId: {"images": {name: (encodedImage, name[:dotIndex], name[dotIndex + 1:])}}})
                else:
                    cachedFilesDictDict = cachedFilesDict.get(myId, None)
                    if cachedFilesDictDict is None:
                        cachedFilesDict.update({myId: {"images": {name: (encodedImage, name[:dotIndex], name[dotIndex + 1:])}}})
                    else:
                        cachedFilesDictDictImages = cachedFilesDictDict.get("images", None)
                        if cachedFilesDictDictImages is None:
                            cachedFilesDictDict.update({"images": {name: (encodedImage, name[:dotIndex], name[dotIndex + 1:])}})
                        else:
                            cachedFilesDictDictImages.update({name: (encodedImage, name[:dotIndex], name[dotIndex + 1:])})
                    try:
                        messagesFilesStorage.set(dialogId, cachedFilesDict)
                    except Error:
                        return JsonResponse({"loadFiles": "error"})
                    except:
                        return JsonResponse({"loadFiles": "error"})
                async_to_sync(layer)(myId, {"data": {"item": json.dumps({
                    'method': 'dialogAttachment',
                    'image': {name: encodedImage},
                    'byId': myId,
                    'toId': toUserId
                    })}})
                return JsonResponse({"loadFiles": True})
        return JsonResponse({"loadFiles": False})