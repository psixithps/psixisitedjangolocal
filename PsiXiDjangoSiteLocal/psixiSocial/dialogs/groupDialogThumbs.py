from django.core.cache import caches
from django.urls import reverse
from psixiSocial.models import mainUserProfile, userProfilePrivacy
from psixiSocial.privacyIdentifier import checkPrivacy
from psixiSocial.translators import usersListText

def getMainAndSmallThumbs(iterableIds, requestUser, requestUserId):
    ''' Создание/выдача или выдача из кеша двух словарей с thumb, 1 с именами пользователей, и 2 словаря статуса: 
    {userId: smallThumbHTMLString}, {userId: mainThumbHTMLString} '''
    textDict = None
    onlineStatusText = None
    iffyStatusText = None
    activeStatusText = None
    middleStatusText = None
    leaveStatusText = None
    mainThumbsResult = {}
    smallThumbsResult = {}
    namesResult = {}
    onlineStatusDict = {}
    activeStatusDict = {}
    onlineUsersSet = set(caches['onlineUsers'].get_many(iterableIds).keys())
    iffyTimeMany = caches['iffyUsersOnlineStatus'].get_many((iterableIds - onlineUsersSet))
    chatStatusActiveSet = set(caches['chatStatusActive'].get_many(iterableIds).keys())
    chatStatusMiddleMany = caches['chatStatusMiddle'].get_many((iterableIds - chatStatusActiveSet))
    smallDialogThumbsCache = caches['messagesListThumbs']
    mainDialogThumbsCache = caches['dialogThumbs']
    smallDialogThumbsMany = smallDialogThumbsCache.get_many(iterableIds)
    mainDialogThumbsMany = mainDialogThumbsCache.get_many(iterableIds)
    smallDialogThumbsManyKeys = set(smallDialogThumbsMany.keys())
    mainDialogThumbsManyKeys = set(mainDialogThumbsMany.keys())
    mainThumbsSurplus = iterableIds - mainDialogThumbsManyKeys
    smallThumbsSurplus = iterableIds - smallDialogThumbsManyKeys
    for id in smallDialogThumbsManyKeys:
        thumb = smallDialogThumbsMany[id].get(requestUserId, None)
        if thumb is None:
            smallThumbsSurplus.add(id)
    namesSurplus = set()
    for id in mainDialogThumbsManyKeys:
        thumb = mainDialogThumbsMany[id].get(requestUserId, None)
        if thumb is None:
            mainThumbsSurplus.add(id)
        else:
            keys = thumb.keys()
            if "name" not in keys or "smallName" not in keys:
                namesSurplus.add(id)
    thumbsSurplus = smallThumbsSurplus | mainThumbsSurplus | namesSurplus
    if len(thumbsSurplus) > 0:
        user = None
        users = mainUserProfile.objects.filter(id__in = thumbsSurplus)
        itemString1 = '</div>'
        if len(mainThumbsSurplus) > 0 or len(namesSurplus) > 0:
            mainThumbItemsToSet = {}
            if requestUserId in mainThumbsSurplus:
                user = users.get(id__exact = requestUserId)
                firstName = user.first_name or None
                lastName = user.last_name or None
                if firstName and lastName:
                    name = '{0} {1}'.format(firstName, lastName)
                    smallName = user.username
                else:
                    if firstName:
                        name = firstName
                        smallName = user.username
                    else:
                        name = user.username
                        smallName = ''
                avatar = user.avatarThumbMiddleSmall or None
                if avatar is not None:
                    if smallName:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, smallName)
                    else:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                itemTuple = (itemString0, itemString1)
                mainThumbsDict = mainDialogThumbsMany.get(requestUserId, None)
                if mainThumbsDict is None:
                    mainThumbItemsToSet.update({requestUserId: {requestUserId: {'item': itemTuple, 'showOnlineStatus': True, 'showActiveStatus': True, 'name': name, 'smallName': smallName}}})
                else:
                    mainThumbsDict.update({'item': itemTuple, 'showOnlineStatus': True, 'showActiveStatus': True, 'name': name, 'smallName': smallName})
                    mainThumbItemsToSet.update({requestUserId: mainThumbsDict})
                namesResult.update({requestUserId: [name, smallName]})
                mainThumbsResult.update({requestUserId: itemTuple})
            else:
               if requestUserId in namesSurplus:
                    user = users.get(id__exact = requestUserId)
                    firstName = user.first_name or None
                    lastName = user.last_name or None
                    if firstName and lastName:
                        name = '{0} {1}'.format(firstName, lastName)
                        smallName = user.username
                    else:
                        if firstName:
                            name = firstName
                            smallName = user.username
                        else:
                            name = user.username
                            smallName = ''
                    namesResult.update({requestUserId: [name, smallName]})
                    mainThumbs = mainDialogThumbsMany[requestUserId]
                    mainThumb = mainThumbs.get(requestUserId)
                    mainThumb.update({'name': name, 'smallName': smallName})
                    mainThumbItemsToSet.update({requestUserId: mainThumbs})
        if len(smallThumbsSurplus) > 0:
            smallThumbItemsToSet = {}
        privacyCache = caches['privacyCache']
        privacyDictMany = privacyCache.get_many(thumbsSurplus)
        privacyManyKeys = set(privacyDictMany.keys())
        privacySurplus = thumbsSurplus - privacyManyKeys
        for id in privacyManyKeys:
            privacyDict = privacyDictMany[id]
            if requestUserId not in privacyDict:
                privacySurplus.add(id)
            else:
                privacyDictKeys = privacyDict.keys()
                if 'viewMainInfo' not in privacyDictKeys or 'viewTHPSInfo' not in privacyDictKeys or 'viewAvatar' not in privacyDictKeys or 'viewOnlineStatus' not in privacyDictKeys or 'viewActiveStatus' not in privacyDictKeys:
                    privacySurplus.add(id)
        if len(privacySurplus) > 0:
            privacySettings = userProfilePrivacy.objects.filter(user_id__in = privacySurplus)
            friendsIdsCache = caches['friendsIdCache']
            friendsIds = friendsIdsCache.get(requestUserId, None)
            if friendsIds is None:
                friendsCache = caches['friendsCache']
                friends = friendsCache.get(requestUserId, None)
                if friends is None:
                    friends = requestUser.friends.all()
                    if friends.count():
                        friendsCache.set(requestUserId, friends)
                        friendsIdsQuerySet = friends.values("id")
                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                    else:
                        friendsCache.set(requestUserId, False)
                        friendsIds = set()
                else:
                    if friends is False:
                        friendsIds = set()
                    else:
                        friendsIdsQuerySet = friends.values("id")
                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                friendsIdsCache.set(requestUserId, friendsIds)
            if len(friendsIds) > 0:
                friendShipSurplus = privacySurplus - friendsIds
                if len(friendShipSurplus) > 0:
                    friendShipCache = caches['friendShipStatusCache']
                    friendShipDictMany = friendShipCache.get_many(friendShipSurplus)
                    friendShipManyKeys = set(friendShipDictMany.keys())
                    friendShipRequestsSurplus = friendShipSurplus - friendShipManyKeys
                    for id in friendShipManyKeys:
                        if requestUserId not in friendShipDictMany[id]:
                            friendShipRequestsSurplus.add(id)
                    if len(friendShipRequestsSurplus) > 0:
                        friendShipRequestsMany = caches['friendshipRequests'].get_many(friendShipRequestsSurplus)
            else:
                friendShipSurplus = set()
                friendShipCache = caches['friendShipStatusCache']
                friendShipDictMany = friendShipCache.get_many(privacySurplus)
                friendShipManyKeys = set(friendShipDictMany.keys())
                friendShipRequestsSurplus = privacySurplus - friendShipManyKeys
                for id in friendShipManyKeys:
                    if requestUserId not in friendShipDictMany[id]:
                        friendShipRequestsSurplus.add(id)
                if len(friendShipRequestsSurplus) > 0:
                    friendShipRequestsMany = caches['friendshipRequests'].get_many(friendShipRequestsSurplus)
            friendShipToSet = {}
            privacyToSet = {}

    for id in iterableIds:
        user = None
        name = None
        smallName = None
        profileUrl = None
        viewMainInfo = None
        mainThumbsDict = mainDialogThumbsMany.get(id, None)
        if mainThumbsDict is None:
            user = users.get(id__exact = id)
            privacyDict = privacyDictMany.get(id, None)
            if privacyDict is None:
                if id in friendsIds:
                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                else:
                    friendShipStatusDict = friendShipDictMany.get(id, None)
                    if friendShipStatusDict is None:
                        friendShipStatus = False
                        friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                        if friendShipRequestsMyList is not None:
                            for friendShipRequestsList in friendShipRequestsMyList:
                                if friendShipRequestsList[0] == id:
                                    friendShipStatus = "request"
                        if friendShipStatus is False:
                            friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                            if friendShipRequestsFriendList is not None:
                                for friendShipRequestsList in friendShipRequestsFriendList:
                                    if friendShipRequestsList[0] == requestUserId:
                                        friendShipStatus = "myRequest"
                        friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                    else:
                        friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                        if friendShipStatus is None:
                            friendShipStatus = False
                            friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                            if friendShipRequestsMyList is not None:
                                for friendShipRequestsList in friendShipRequestsMyList:
                                    if friendShipRequestsList[0] == id:
                                        friendShipStatus = "request"
                            if friendShipStatus is False:
                                friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                if friendShipRequestsFriendList is not None:
                                    for friendShipRequestsList in friendShipRequestsFriendList:
                                        if friendShipRequestsList[0] == requestUserId:
                                            friendShipStatus = "myRequest"
                            friendShipStatusDict.update({requestUserId: friendShipStatus})
                            friendShipToSet.update({id: friendShipStatusDict})
                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                viewAvatar = privacyChecker.checkAvatarPrivacy()
                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                privacyToSet.update({id: {requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
            else:
                userPrivacyDict = privacyDict.get(requestUserId, None)
                if userPrivacyDict is None:
                    if id in friendsIds:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                    else:
                        friendShipStatusDict = friendShipDictMany.get(id, None)
                        if friendShipStatusDict is None:
                            friendShipStatus = False
                            friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                            if friendShipRequestsMyList is not None:
                                for friendShipRequestsList in friendShipRequestsMyList:
                                    if friendShipRequestsList[0] == id:
                                        friendShipStatus = "request"
                            if friendShipStatus is False:
                                friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                if friendShipRequestsFriendList is not None:
                                    for friendShipRequestsList in friendShipRequestsFriendList:
                                        if friendShipRequestsList[0] == requestUserId:
                                            friendShipStatus = "myRequest"
                            friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                        else:
                            friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                            if friendShipStatus is None:
                                friendShipStatus = False
                                friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                if friendShipRequestsMyList is not None:
                                    for friendShipRequestsList in friendShipRequestsMyList:
                                        if friendShipRequestsList[0] == id:
                                            friendShipStatus = "request"
                                if friendShipStatus is False:
                                    friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                    if friendShipRequestsFriendList is not None:
                                        for friendShipRequestsList in friendShipRequestsFriendList:
                                            if friendShipRequestsList[0] == requestUserId:
                                                friendShipStatus = "myRequest"
                                friendShipStatusDict.update({requestUserId: friendShipStatus})
                                friendShipToSet.update({id: friendShipStatusDict})
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                    privacyDict.update({requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
                    privacyToSet.update({id: privacyDict})
                else:
                    viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                    viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                    viewAvatar = userPrivacyDict.get('viewAvatar', None)
                    viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                    viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                    if viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None or viewActiveStatus is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            friendShipStatusDict = friendShipDictMany.get(id, None)
                            if friendShipStatusDict is None:
                                friendShipStatus = False
                                friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                if friendShipRequestsMyList is not None:
                                    for friendShipRequestsList in friendShipRequestsMyList:
                                        if friendShipRequestsList[0] == id:
                                            friendShipStatus = "request"
                                if friendShipStatus is False:
                                    friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                    if friendShipRequestsFriendList is not None:
                                        for friendShipRequestsList in friendShipRequestsFriendList:
                                            if friendShipRequestsList[0] == requestUserId:
                                                friendShipStatus = "myRequest"
                                friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                            else:
                                friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                                if friendShipStatus is None:
                                    friendShipStatus = False
                                    friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                    if friendShipRequestsMyList is not None:
                                        for friendShipRequestsList in friendShipRequestsMyList:
                                            if friendShipRequestsList[0] == id:
                                                friendShipStatus = "request"
                                    if friendShipStatus is False:
                                        friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                        if friendShipRequestsFriendList is not None:
                                            for friendShipRequestsList in friendShipRequestsFriendList:
                                                if friendShipRequestsList[0] == requestUserId:
                                                    friendShipStatus = "myRequest"
                                    friendShipStatusDict.update({requestUserId: friendShipStatus})
                                    friendShipToSet.update({id: friendShipStatusDict})
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                        if viewMainInfo is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                        if viewTHPSInfo is None:
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                        if viewAvatar is None:
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            userPrivacyDict.update({'viewAvatar': viewAvatar})
                        if viewOnlineStatus is None:
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                        if viewActiveStatus is None:
                            viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                            userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                        privacyToSet.update({id: privacyDict})
            if viewMainInfo:
                firstName = user.first_name or None
                lastName = user.last_name or None
                if firstName and lastName:
                    name = '{0} {1}'.format(firstName, lastName)
                    smallName = user.username
                else:
                    if firstName:
                        name = firstName
                        smallName = user.username
                    else:
                        name = user.username
                        smallName = ''
            else:
                name = user.username
                smallName = ''
            if viewAvatar:
                avatar = user.avatarThumbMiddleSmall or None
                if avatar is not None:
                    if smallName:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, smallName)
                    else:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
            else:
                if smallName:
                    itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                else:
                    itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
            if viewOnlineStatus and viewActiveStatus:
                if id in onlineUsersSet:
                    if onlineStatusText is None:
                        if textDict is None:
                            textDict = usersListText(lang = 'ru').getDict()
                        onlineStatusText = textDict.pop('onlineStatus')
                    if id in chatStatusActiveSet:
                        if activeStatusText is None:
                            if textDict is None:
                                textDict = usersListText(lang = 'ru').getDict()
                            activeStatusText = textDict.pop('activeStatus')
                        mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), itemString1)})
                        activeStatusDict.update({id: 'stateActive'})
                    else:
                        middleTupleStr = chatStatusMiddleMany.get(id, None)
                        if middleTupleStr is not None:
                            if middleStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                middleStatusText = textDict.pop('middleStatus')
                            mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "middle">{1}</p></noscript>'.format(onlineStatusText, middleStatusText), itemString1)})
                            activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                        else:
                            if leaveStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                leaveStatusText = textDict.pop('leaveStatus')
                            mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "leave">{1}</p></noscript>'.format(onlineStatusText, leaveStatusText), itemString1)})
                    onlineStatusDict.update({id: 'online'})
                else:
                    iffyTimeStr = iffyTimeMany.get(id, None)
                    if iffyTimeStr is not None:
                        if iffyStatusText is None:
                            if textDict is None:
                                textDict = usersListText(lang = 'ru').getDict()
                            iffyStatusText = textDict.pop('uknownStatus')
                        if id in chatStatusActiveSet:
                            if activeStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                activeStatusText = textDict.pop('activeStatus')
                            mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "active">{1}</p></noscript>'.format(iffyStatusText, activeStatusText), itemString1)})
                            activeStatusDict.update({id: 'stateActive'})
                        else:
                            middleTupleStr = chatStatusMiddleMany.get(id, None)
                            if middleTupleStr is not None:
                                if middleStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    middleStatusText = textDict.pop('middleStatus')
                                mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "middle">{1}</p></noscript>'.format(iffyStatusText, middleStatusText), itemString1)})
                                activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                            else:
                                if leaveStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    leaveStatusText = textDict.pop('leaveStatus')
                                mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "leave">{1}</p></noscript>'.format(iffyStatusText, leaveStatusText), itemString1)})
                        onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                    else:
                        mainThumbsResult.update({id: (itemString0, itemString1)})
            else:
                if viewOnlineStatus:
                    if id in onlineUsersSet:
                        if onlineStatusText is None:
                            if textDict is None:
                                textDict = usersListText(lang = 'ru').getDict()
                            onlineStatusText = textDict.pop('onlineStatus')
                        mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p></noscript>'.format(onlineStatusText), itemString1)})
                        onlineStatusDict.update({id: 'online'})
                    else:
                        iffyTimeStr = iffyTimeMany.get(id, None)
                        if iffyTimeStr is not None:
                            if iffyStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                iffyStatusText = textDict.pop('uknownStatus')
                            mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText), itemString1)})
                            onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                        else:
                            mainThumbsResult.update({id: (itemString0, itemString1)})
                elif viewActiveStatus:
                    if id in chatStatusActiveSet:
                        if activeStatusText is None:
                            if textDict is None:
                                textDict = usersListText(lang = 'ru').getDict()
                            activeStatusText = textDict.pop('activeStatus')
                        mainThumbsResult.update({id: (itemString0, '<noscript><p class = "active">{0}</p></noscript>'.format(activeStatusText), itemString1)})
                        activeStatusDict.update({id: 'stateActive'})
                    else:
                        middleTupleStr = chatStatusMiddleMany.get(id, None)
                        if middleTupleStr is not None:
                            if middleStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                middleStatusText = textDict.pop('middleStatus')
                            mainThumbsResult.update({id: (itemString0, '<noscript><p class = "middle">{0}</p></noscript>'.format(middleStatusText), itemString1)})
                            activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                        else:
                            mainThumbsResult.update({id: (itemString0, itemString1)})
                else:
                    mainThumbsResult.update({id: (itemString0, itemString1)})
            if smallName:
                mainThumbItemsToSet.update({id: {requestUserId: {'item': (itemString0, itemString1), 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus, 'name': name, 'smallName': smallName}}})
                namesResult.update({id: [name, smallName]})
            else:
                mainThumbItemsToSet.update({id: {requestUserId: {'item': (itemString0, itemString1), 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus, 'name': name, 'smallName': ''}}})
                namesResult.update({id: [name, '']})
        else:
            mainThumb = mainThumbsDict.get(requestUserId, None)
            if mainThumb is None:
                user = users.get(id__exact = id)
                privacyDict = privacyDictMany.get(id, None)
                if privacyDict is None:
                    if id in friendsIds:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                    else:
                        friendShipStatusDict = friendShipDictMany.get(id, None)
                        if friendShipStatusDict is None:
                            friendShipStatus = False
                            friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                            if friendShipRequestsMyList is not None:
                                for friendShipRequestsList in friendShipRequestsMyList:
                                    if friendShipRequestsList[0] == id:
                                        friendShipStatus = "request"
                            if friendShipStatus is False:
                                friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                if friendShipRequestsFriendList is not None:
                                    for friendShipRequestsList in friendShipRequestsFriendList:
                                        if friendShipRequestsList[0] == requestUserId:
                                            friendShipStatus = "myRequest"
                            friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                        else:
                            friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                            if friendShipStatus is None:
                                friendShipStatus = False
                                friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                if friendShipRequestsMyList is not None:
                                    for friendShipRequestsList in friendShipRequestsMyList:
                                        if friendShipRequestsList[0] == id:
                                            friendShipStatus = "request"
                                if friendShipStatus is False:
                                    friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                    if friendShipRequestsFriendList is not None:
                                        for friendShipRequestsList in friendShipRequestsFriendList:
                                            if friendShipRequestsList[0] == requestUserId:
                                                friendShipStatus = "myRequest"
                                friendShipStatusDict.update({requestUserId: friendShipStatus})
                                friendShipToSet.update({id: friendShipStatusDict})
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                    privacyToSet.update({id: {requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
                else:
                    userPrivacyDict = privacyDict.get(requestUserId, None)
                    if userPrivacyDict is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            friendShipStatusDict = friendShipDictMany.get(id, None)
                            if friendShipStatusDict is None:
                                friendShipStatus = False
                                friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                if friendShipRequestsMyList is not None:
                                    for friendShipRequestsList in friendShipRequestsMyList:
                                        if friendShipRequestsList[0] == id:
                                            friendShipStatus = "request"
                                if friendShipStatus is False:
                                    friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                    if friendShipRequestsFriendList is not None:
                                        for friendShipRequestsList in friendShipRequestsFriendList:
                                            if friendShipRequestsList[0] == requestUserId:
                                                friendShipStatus = "myRequest"
                                friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                            else:
                                friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                                if friendShipStatus is None:
                                    friendShipStatus = False
                                    friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                    if friendShipRequestsMyList is not None:
                                        for friendShipRequestsList in friendShipRequestsMyList:
                                            if friendShipRequestsList[0] == id:
                                                friendShipStatus = "request"
                                    if friendShipStatus is False:
                                        friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                        if friendShipRequestsFriendList is not None:
                                            for friendShipRequestsList in friendShipRequestsFriendList:
                                                if friendShipRequestsList[0] == requestUserId:
                                                    friendShipStatus = "myRequest"
                                    friendShipStatusDict.update({requestUserId: friendShipStatus})
                                    friendShipToSet.update({id: friendShipStatusDict})
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                        privacyDict.update({requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
                        privacyToSet.update({id: privacyDict})
                    else:
                        viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                        viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                        viewAvatar = userPrivacyDict.get('viewAvatar', None)
                        viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                        viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                        if viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None or viewActiveStatus is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                friendShipStatusDict = friendShipDictMany.get(id, None)
                                if friendShipStatusDict is None:
                                    friendShipStatus = False
                                    friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                    if friendShipRequestsMyList is not None:
                                        for friendShipRequestsList in friendShipRequestsMyList:
                                            if friendShipRequestsList[0] == id:
                                                friendShipStatus = "request"
                                    if friendShipStatus is False:
                                        friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                        if friendShipRequestsFriendList is not None:
                                            for friendShipRequestsList in friendShipRequestsFriendList:
                                                if friendShipRequestsList[0] == requestUserId:
                                                    friendShipStatus = "myRequest"
                                    friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                                else:
                                    friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                                    if friendShipStatus is None:
                                        friendShipStatus = False
                                        friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                        if friendShipRequestsMyList is not None:
                                            for friendShipRequestsList in friendShipRequestsMyList:
                                                if friendShipRequestsList[0] == id:
                                                    friendShipStatus = "request"
                                        if friendShipStatus is False:
                                            friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                            if friendShipRequestsFriendList is not None:
                                                for friendShipRequestsList in friendShipRequestsFriendList:
                                                    if friendShipRequestsList[0] == requestUserId:
                                                        friendShipStatus = "myRequest"
                                        friendShipStatusDict.update({requestUserId: friendShipStatus})
                                        friendShipToSet.update({id: friendShipStatusDict})
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                            if viewMainInfo is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                            if viewTHPSInfo is None:
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                            if viewAvatar is None:
                                viewAvatar = privacyChecker.checkAvatarPrivacy()
                                userPrivacyDict.update({'viewAvatar': viewAvatar})
                            if viewOnlineStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                            if viewActiveStatus is None:
                                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                            privacyToSet.update({id: privacyDict})
                if viewMainInfo:
                    firstName = user.first_name or None
                    lastName = user.last_name or None
                    if firstName and lastName:
                        name = '{0} {1}'.format(firstName, lastName)
                        smallName = user.username
                    else:
                        if firstName:
                            name = firstName
                            smallName = user.username
                        else:
                            name = user.username
                            smallName = ''
                else:
                    name = user.username
                    smallName = ''
                if viewAvatar:
                    avatar = user.avatarThumbMiddleSmall or None
                    if avatar is not None:
                        if smallName:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, smallName)
                        else:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                if viewOnlineStatus and viewActiveStatus:
                    if id in onlineUsersSet:
                        if onlineStatusText is None:
                            if textDict is None:
                                textDict = usersListText(lang = 'ru').getDict()
                            onlineStatusText = textDict.pop('onlineStatus')
                        if id in chatStatusActiveSet:
                            if activeStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                activeStatusText = textDict.pop('activeStatus')
                            mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), itemString1)})
                            activeStatusDict.update({id: 'stateActive'})
                        else:
                            middleTupleStr = chatStatusMiddleMany.get(id, None)
                            if middleTupleStr is not None:
                                if middleStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    middleStatusText = textDict.pop('middleStatus')
                                mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "middle">{1}</p></noscript>'.format(onlineStatusText, middleStatusText), itemString1)})
                                activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                            else:
                                if leaveStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    leaveStatusText = textDict.pop('leaveStatus')
                                mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "leave">{1}</p></noscript>'.format(onlineStatusText, leaveStatusText), itemString1)})
                        onlineStatusDict.update({id: 'online'})
                    else:
                        iffyTimeStr = iffyTimeMany.get(id, None)
                        if iffyTimeStr is not None:
                            if iffyStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                iffyStatusText = textDict.pop('uknownStatus')
                            if id in chatStatusActiveSet:
                                if activeStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    activeStatusText = textDict.pop('activeStatus')
                                mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "active">{1}</p></noscript>'.format(iffyStatusText, activeStatusText), itemString1)})
                                activeStatusDict.update({id: 'stateActive'})
                            else:
                                middleTupleStr = chatStatusMiddleMany.get(id, None)
                                if middleTupleStr is not None:
                                    if middleStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        middleStatusText = textDict.pop('middleStatus')
                                    mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "middle">{1}</p></noscript>'.format(iffyStatusText, middleStatusText), itemString1)})
                                    activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                                else:
                                    if leaveStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        leaveStatusText = textDict.pop('leaveStatus')
                                    mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "leave">{1}</p></noscript>'.format(iffyStatusText, leaveStatusText), itemString1)})
                            onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                        else:
                            mainThumbsResult.update({id: (itemString0, itemString1)})
                else:
                    if viewOnlineStatus:
                        if id in onlineUsersSet:
                            if onlineStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                onlineStatusText = textDict.pop('onlineStatus')
                            mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p></noscript>'.format(onlineStatusText), itemString1)})
                            onlineStatusDict.update({id: 'online'})
                        else:
                            iffyTimeStr = iffyTimeMany.get(id, None)
                            if iffyTimeStr is not None:
                                if iffyStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    iffyStatusText = textDict.pop('uknownStatus')
                                mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText), itemString1)})
                                onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                            else:
                                mainThumbsResult.update({id: (itemString0, itemString1)})
                    elif viewActiveStatus:
                        if id in chatStatusActiveSet:
                            if activeStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                activeStatusText = textDict.pop('activeStatus')
                            mainThumbsResult.update({id: (itemString0, '<noscript><p class = "active">{0}</p></noscript>'.format(activeStatusText), itemString1)})
                            activeStatusDict.update({id: 'stateActive'})
                        else:
                            middleTupleStr = chatStatusMiddleMany.get(id, None)
                            if middleTupleStr is not None:
                                if middleStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    middleStatusText = textDict.pop('middleStatus')
                                mainThumbsResult.update({id: (itemString0, '<noscript><p class = "middle">{0}</p></noscript>'.format(middleStatusText), itemString1)})
                                activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                            else:
                                mainThumbsResult.update({id: (itemString0, itemString1)})
                    else:
                        mainThumbsResult.update({id: (itemString0, itemString1)})
                if smallName:
                    mainThumbsDict.update({requestUserId: {'item': (itemString0, itemString1), 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus, 'name': name, 'smallName': smallName}})
                    namesResult.update({id: [name, smallName]})
                else:
                    mainThumbsDict.update({requestUserId: {'item': (itemString0, itemString1), 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus, 'name': name, 'smallName': ''}})
                    namesResult.update({id: [name, '']})
                mainThumbItemsToSet.update({id: mainThumbsDict})
            else:
                primaryName = mainThumb.get('name', None)
                subName = mainThumb.get('smallName', None)
                thumbItem = mainThumb.get('item', None)
                if thumbItem is None:
                    user = users.get(id__exact = id)
                    privacyDict = privacyDictMany.get(id, None)
                    if privacyDict is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            friendShipStatusDict = friendShipDictMany.get(id, None)
                            if friendShipStatusDict is None:
                                friendShipStatus = False
                                friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                if friendShipRequestsMyList is not None:
                                    for friendShipRequestsList in friendShipRequestsMyList:
                                        if friendShipRequestsList[0] == id:
                                            friendShipStatus = "request"
                                if friendShipStatus is False:
                                    friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                    if friendShipRequestsFriendList is not None:
                                        for friendShipRequestsList in friendShipRequestsFriendList:
                                            if friendShipRequestsList[0] == requestUserId:
                                                friendShipStatus = "myRequest"
                                friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                            else:
                                friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                                if friendShipStatus is None:
                                    friendShipStatus = False
                                    friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                    if friendShipRequestsMyList is not None:
                                        for friendShipRequestsList in friendShipRequestsMyList:
                                            if friendShipRequestsList[0] == id:
                                                friendShipStatus = "request"
                                    if friendShipStatus is False:
                                        friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                        if friendShipRequestsFriendList is not None:
                                            for friendShipRequestsList in friendShipRequestsFriendList:
                                                if friendShipRequestsList[0] == requestUserId:
                                                    friendShipStatus = "myRequest"
                                    friendShipStatusDict.update({requestUserId: friendShipStatus})
                                    friendShipToSet.update({id: friendShipStatusDict})
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                        privacyDict = {'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}
                        if primaryName is None or subName is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            privacyDict.update({'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo})
                        privacyToSet.update({id: {requestUserId: privacyDict}})
                    else:
                        userPrivacyDict = privacyDict.get(requestUserId, None)
                        if userPrivacyDict is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                friendShipStatusDict = friendShipDictMany.get(id, None)
                                if friendShipStatusDict is None:
                                    friendShipStatus = False
                                    friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                    if friendShipRequestsMyList is not None:
                                        for friendShipRequestsList in friendShipRequestsMyList:
                                            if friendShipRequestsList[0] == id:
                                                friendShipStatus = "request"
                                    if friendShipStatus is False:
                                        friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                        if friendShipRequestsFriendList is not None:
                                            for friendShipRequestsList in friendShipRequestsFriendList:
                                                if friendShipRequestsList[0] == requestUserId:
                                                    friendShipStatus = "myRequest"
                                    friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                                else:
                                    friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                                    if friendShipStatus is None:
                                        friendShipStatus = False
                                        friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                        if friendShipRequestsMyList is not None:
                                            for friendShipRequestsList in friendShipRequestsMyList:
                                                if friendShipRequestsList[0] == id:
                                                    friendShipStatus = "request"
                                        if friendShipStatus is False:
                                            friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                            if friendShipRequestsFriendList is not None:
                                                for friendShipRequestsList in friendShipRequestsFriendList:
                                                    if friendShipRequestsList[0] == requestUserId:
                                                        friendShipStatus = "myRequest"
                                        friendShipStatusDict.update({requestUserId: friendShipStatus})
                                        friendShipToSet.update({id: friendShipStatusDict})
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                            userPrivacyDict = {'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}
                            if primaryName is None or subName is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo})
                            privacyDict.update({id: {requestUserId: userPrivacyDict}})
                        else:
                            if primaryName is None or subName is None:
                                viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                                viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                            else:
                                viewMainInfo = False
                                viewTHPSInfo = False

                            viewAvatar = userPrivacyDict.get('viewAvatar', None)
                            viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                            viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                            if viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None or viewActiveStatus is None:
                                if id in friendsIds:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                else:
                                    friendShipStatusDict = friendShipDictMany.get(id, None)
                                    if friendShipStatusDict is None:
                                        friendShipStatus = False
                                        friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                        if friendShipRequestsMyList is not None:
                                            for friendShipRequestsList in friendShipRequestsMyList:
                                                if friendShipRequestsList[0] == id:
                                                    friendShipStatus = "request"
                                        if friendShipStatus is False:
                                            friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                            if friendShipRequestsFriendList is not None:
                                                for friendShipRequestsList in friendShipRequestsFriendList:
                                                    if friendShipRequestsList[0] == requestUserId:
                                                        friendShipStatus = "myRequest"
                                        friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                                    else:
                                        friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                                        if friendShipStatus is None:
                                            friendShipStatus = False
                                            friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                            if friendShipRequestsMyList is not None:
                                                for friendShipRequestsList in friendShipRequestsMyList:
                                                    if friendShipRequestsList[0] == id:
                                                        friendShipStatus = "request"
                                            if friendShipStatus is False:
                                                friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                                if friendShipRequestsFriendList is not None:
                                                    for friendShipRequestsList in friendShipRequestsFriendList:
                                                        if friendShipRequestsList[0] == requestUserId:
                                                            friendShipStatus = "myRequest"
                                            friendShipStatusDict.update({requestUserId: friendShipStatus})
                                            friendShipToSet.update({id: friendShipStatusDict})
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                                if primaryName is None or subName is None:
                                    if viewMainInfo is None:
                                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                        userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                                    if viewTHPSInfo is None:
                                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                        userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                                if viewAvatar is None:
                                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                                    userPrivacyDict.update({'viewAvatar': viewAvatar})
                                if viewOnlineStatus is None:
                                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                    userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                                if viewActiveStatus is None:
                                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                    userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                                privacyToSet.update({id: privacyDict})
                    if primaryName is None or subName is None:
                        if primaryName is None and subName is None:
                            if viewMainInfo:
                                firstName = user.first_name or None
                                lastName = user.last_name or None
                                if firstName and lastName:
                                    name = '{0} {1}'.format(firstName, lastName)
                                    smallName = user.username
                                else:
                                    if firstName:
                                        name = firstName
                                        smallName = user.username
                                    else:
                                        name = user.username
                                        smallName = ''
                            else:
                                name = user.username
                                smallName = ''
                            if viewAvatar:
                                avatar = user.avatarThumbMiddleSmall or None
                                if avatar is not None:
                                    if smallName:
                                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, smallName)
                                    else:
                                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                                else:
                                    if smallName:
                                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                                    else:
                                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                            else:
                                if smallName:
                                    itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                                else:
                                    itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                        else:
                            if primaryName is None:
                                if viewMainInfo:
                                    firstName = user.first_name or None
                                    lastName = user.last_name or None
                                    if firstName and lastName:
                                        name = '{0} {1}'.format(firstName, lastName)
                                    else:
                                        if firstName:
                                            name = firstName
                                        else:
                                            name = user.username
                                else:
                                    name = user.username
                                if viewAvatar:
                                    avatar = user.avatarThumbMiddleSmall or None
                                    if avatar is not None:
                                        if subName:
                                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, subName)
                                        else:
                                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                                    else:
                                        if subName:
                                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, subName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                                        else:
                                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                                else:
                                    if subName:
                                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, subName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                                    else:
                                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                            else:
                                if viewMainInfo:
                                    firstName = user.first_name or None
                                    if firstName:
                                        smallName = user.username
                                    else:
                                        smallName = ''
                                else:
                                    smallName = ''
                                if viewAvatar:
                                    avatar = user.avatarThumbMiddleSmall or None
                                    if avatar is not None:
                                        if smallName:
                                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, primaryName, smallName)
                                        else:
                                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, primaryName)
                                    else:
                                        if smallName:
                                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, primaryName, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                                        else:
                                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, primaryName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                                else:
                                    if smallName:
                                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, primaryName, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                                    else:
                                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, primaryName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                    else:
                        if viewAvatar:
                            avatar = user.avatarThumbMiddleSmall or None
                            if avatar is not None:
                                if subName:
                                    itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, primaryName, subName)
                                else:
                                    itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, primaryName)
                            else:
                                if subName:
                                    itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, primaryName, subName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                                else:
                                    itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, primaryName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                        else:
                            if subName:
                                itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, primaryName, subName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                            else:
                                itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, primaryName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                    if viewOnlineStatus and viewActiveStatus:
                        if id in onlineUsersSet:
                            if onlineStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                onlineStatusText = textDict.pop('onlineStatus')
                            if id in chatStatusActiveSet:
                                if activeStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    activeStatusText = textDict.pop('activeStatus')
                                mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), itemString1)})
                                activeStatusDict.update({id: 'stateActive'})
                            else:
                                middleTupleStr = chatStatusMiddleMany.get(id, None)
                                if middleTupleStr is not None:
                                    if middleStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        middleStatusText = textDict.pop('middleStatus')
                                    mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "middle">{1}</p></noscript>'.format(onlineStatusText, middleStatusText), itemString1)})
                                    activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                                else:
                                    if leaveStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        leaveStatusText = textDict.pop('leaveStatus')
                                    mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "leave">{1}</p></noscript>'.format(onlineStatusText, leaveStatusText), itemString1)})
                            onlineStatusDict.update({id: 'online'})
                        else:
                            iffyTimeStr = iffyTimeMany.get(id, None)
                            if iffyTimeStr is not None:
                                if iffyStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    iffyStatusText = textDict.pop('uknownStatus')
                                if id in chatStatusActiveSet:
                                    if activeStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        activeStatusText = textDict.pop('activeStatus')
                                    mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "active">{1}</p></noscript>'.format(iffyStatusText, activeStatusText), itemString1)})
                                    activeStatusDict.update({id: 'stateActive'})
                                else:
                                    middleTupleStr = chatStatusMiddleMany.get(id, None)
                                    if middleTupleStr is not None:
                                        if middleStatusText is None:
                                            if textDict is None:
                                                textDict = usersListText(lang = 'ru').getDict()
                                            middleStatusText = textDict.pop('middleStatus')
                                        mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "middle">{1}</p></noscript>'.format(iffyStatusText, middleStatusText), itemString1)})
                                        activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                                    else:
                                        if leaveStatusText is None:
                                            if textDict is None:
                                                textDict = usersListText(lang = 'ru').getDict()
                                            leaveStatusText = textDict.pop('leaveStatus')
                                        mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "leave">{1}</p></noscript>'.format(iffyStatusText, leaveStatusText), itemString1)})
                                onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                            else:
                                mainThumbsResult.update({id: (itemString0, itemString1)})
                    else:
                        if viewOnlineStatus:
                            if id in onlineUsersSet:
                                if onlineStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    onlineStatusText = textDict.pop('onlineStatus')
                                mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p></noscript>'.format(onlineStatusText), itemString1)})
                                onlineStatusDict.update({id: 'online'})
                            else:
                                iffyTimeStr = iffyTimeMany.get(id, None)
                                if iffyTimeStr is not None:
                                    if iffyStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        iffyStatusText = textDict.pop('uknownStatus')
                                    mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText), itemString1)})
                                    onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                                else:
                                    mainThumbsResult.update({id: (itemString0, itemString1)})
                        elif viewActiveStatus:
                            if id in chatStatusActiveSet:
                                if activeStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    activeStatusText = textDict.pop('activeStatus')
                                mainThumbsResult.update({id: (itemString0, '<noscript><p class = "active">{0}</p></noscript>'.format(activeStatusText), itemString1)})
                                activeStatusDict.update({id: 'stateActive'})
                            else:
                                middleTupleStr = chatStatusMiddleMany.get(id, None)
                                if middleTupleStr is not None:
                                    if middleStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        middleStatusText = textDict.pop('middleStatus')
                                    mainThumbsResult.update({id: (itemString0, '<noscript><p class = "middle">{0}</p></noscript>'.format(middleStatusText), itemString1)})
                                    activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                                else:
                                    mainThumbsResult.update({id: (itemString0, itemString1)})
                    itemTuple = (itemString0, itemString1)
                    if primaryName or subName is None:
                        if primaryName and subName is None:
                            if smallName:
                                mainThumb.update({'item': itemTuple, 'name': name, 'smallName': smallName, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus})
                                namesResult.update({id: [name, smallName]})
                            else:
                                mainThumb.update({'item': itemTuple, 'name': name, 'smallName': '', 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus})
                                namesResult.update({id: [name, '']})
                        else:
                            if primaryName is None:
                                mainThumb.update({'item': itemTuple, 'name': name, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus})
                                namesResult.update({id: [name, subName]})
                            else:
                                if smallName:
                                    mainThumb.update({'item': itemTuple, 'smallName': smallName, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus})
                                    namesResult.update({id: [primaryName, smallName]})
                                else:
                                    mainThumb.update({'item': itemTuple, 'smallName': '', 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus})
                                    namesResult.update({id: [primaryName, '']})
                    mainThumbItemsToSet.update({id: mainThumbsDict})
                    mainThumbsResult.update({id: itemTuple})
                else:
                    viewOnlineStatus = mainThumb['showOnlineStatus']
                    viewActiveStatus = mainThumb['showActiveStatus']
                    if viewOnlineStatus and viewActiveStatus:
                        if id in onlineUsersSet:
                            if onlineStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                onlineStatusText = textDict.pop('onlineStatus')
                            if id in chatStatusActiveSet:
                                if activeStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    activeStatusText = textDict.pop('activeStatus')
                                mainThumbsResult.update({id: (thumbItem[0], '<noscript><p class = "online">{0}</p><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), thumbItem[1])})
                                activeStatusDict.update({id: 'stateActive'})
                            else:
                                middleTupleStr = chatStatusMiddleMany.get(id, None)
                                if middleTupleStr is not None:
                                    if middleStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        middleStatusText = textDict.pop('middleStatus')
                                    mainThumbsResult.update({id: (thumbItem[0], '<noscript><p class = "online">{0}</p><p class = "middle">{1}</p></noscript>'.format(onlineStatusText, middleStatusText), thumbItem[1])})
                                    activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                                else:
                                    if leaveStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        leaveStatusText = textDict.pop('leaveStatus')
                                    mainThumbsResult.update({id: (thumbItem[0], '<noscript><p class = "online">{0}</p><p class = "leave">{1}</p></noscript>'.format(onlineStatusText, leaveStatusText), thumbItem[1])})
                            onlineStatusDict.update({id: 'online'})
                        else:
                            iffyTimeStr = iffyTimeMany.get(id, None)
                            if iffyTimeStr is not None:
                                if iffyStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    iffyStatusText = textDict.pop('uknownStatus')
                                if id in chatStatusActiveSet:
                                    if activeStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        activeStatusText = textDict.pop('activeStatus')
                                    mainThumbsResult.update({id: (thumbItem[0], '<noscript><p class = "iffy">{0}</p><p class = "active">{1}</p></noscript>'.format(iffyStatusText, activeStatusText), thumbItem[1])})
                                    activeStatusDict.update({id: 'stateActive'})
                                else:
                                    middleTupleStr = chatStatusMiddleMany.get(id, None)
                                    if middleTupleStr is not None:
                                        if middleStatusText is None:
                                            if textDict is None:
                                                textDict = usersListText(lang = 'ru').getDict()
                                            middleStatusText = textDict.pop('middleStatus')
                                        mainThumbsResult.update({id: (thumbItem[0], '<noscript><p class = "iffy">{0}</p><p class = "middle">{1}</p></noscript>'.format(iffyStatusText, middleStatusText), thumbItem[1])})
                                        activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                                    else:
                                        if leaveStatusText is None:
                                            if textDict is None:
                                                textDict = usersListText(lang = 'ru').getDict()
                                            leaveStatusText = textDict.pop('leaveStatus')
                                        mainThumbsResult.update({id: (thumbItem[0], '<noscript><p class = "iffy">{0}</p><p class = "leave">{1}</p></noscript>'.format(iffyStatusText, leaveStatusText), thumbItem[1])})
                                onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                            else:
                                mainThumbsResult.update({id: thumbItem})
                    else:
                        if viewOnlineStatus:
                            if id in onlineUsersSet:
                                if onlineStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    onlineStatusText = textDict.pop('onlineStatus')
                                mainThumbsResult.update({id: (thumbItem[0], '<noscript><p class = "online">{0}</p></noscript>'.format(onlineStatusText), thumbItem[1])})
                                onlineStatusDict.update({id: 'online'})
                            else:
                                iffyTimeStr = iffyTimeMany.get(id, None)
                                if iffyTimeStr is not None:
                                    if iffyStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        iffyStatusText = textDict.pop('uknownStatus')
                                    mainThumbsResult.update({id: (thumbItem[0], '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText), thumbItem[1])})
                                    onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                                else:
                                    mainThumbsResult.update({id: thumbItem})
                        elif viewActiveStatus:
                            if id in chatStatusActiveSet:
                                if activeStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    activeStatusText = textDict.pop('activeStatus')
                                mainThumbsResult.update({id: (thumbItem[0], '<noscript><p class = "active">{0}</p></noscript>'.format(activeStatusText), thumbItem[1])})
                                activeStatusDict.update({id: 'stateActive'})
                            else:
                                middleTupleStr = chatStatusMiddleMany.get(id, None)
                                if middleTupleStr is not None:
                                    if middleStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        middleStatusText = textDict.pop('middleStatus')
                                    mainThumbsResult.update({id: (thumbItem[0], '<noscript><p class = "middle">{0}</p></noscript>'.format(middleStatusText), thumbItem[1])})
                                    activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                                else:
                                    mainThumbsResult.update({id: thumbItem})
                        else:
                            mainThumbsResult.update({id: (thumbItem[0], thumbItem[1])})
                    if primaryName is None or subName is None:
                        user = users.get(id__exact = id)
                        privacyDict = privacyDictMany.get(id, None)
                        if privacyDict is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                friendShipStatusDict = friendShipDictMany.get(id, None)
                                if friendShipStatusDict is None:
                                    friendShipStatus = False
                                    friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                    if friendShipRequestsMyList is not None:
                                        for friendShipRequestsList in friendShipRequestsMyList:
                                            if friendShipRequestsList[0] == id:
                                                friendShipStatus = "request"
                                    if friendShipStatus is False:
                                        friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                        if friendShipRequestsFriendList is not None:
                                            for friendShipRequestsList in friendShipRequestsFriendList:
                                                if friendShipRequestsList[0] == requestUserId:
                                                    friendShipStatus = "myRequest"
                                    friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                                else:
                                    friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                                    if friendShipStatus is None:
                                        friendShipStatus = False
                                        friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                        if friendShipRequestsMyList is not None:
                                            for friendShipRequestsList in friendShipRequestsMyList:
                                                if friendShipRequestsList[0] == id:
                                                    friendShipStatus = "request"
                                        if friendShipStatus is False:
                                            friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                            if friendShipRequestsFriendList is not None:
                                                for friendShipRequestsList in friendShipRequestsFriendList:
                                                    if friendShipRequestsList[0] == requestUserId:
                                                        friendShipStatus = "myRequest"
                                        friendShipStatusDict.update({requestUserId: friendShipStatus})
                                        friendShipToSet.update({id: friendShipStatusDict})
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            privacyToSet.update({id: {requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo}}})
                        else:
                            userPrivacyDict = privacyDict.get(requestUserId, None)
                            if userPrivacyDict is None:
                                if id in friendsIds:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                else:
                                    friendShipStatusDict = friendShipDictMany.get(id, None)
                                    if friendShipStatusDict is None:
                                        friendShipStatus = False
                                        friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                        if friendShipRequestsMyList is not None:
                                            for friendShipRequestsList in friendShipRequestsMyList:
                                                if friendShipRequestsList[0] == id:
                                                    friendShipStatus = "request"
                                        if friendShipStatus is False:
                                            friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                            if friendShipRequestsFriendList is not None:
                                                for friendShipRequestsList in friendShipRequestsFriendList:
                                                    if friendShipRequestsList[0] == requestUserId:
                                                        friendShipStatus = "myRequest"
                                        friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                                    else:
                                        friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                                        if friendShipStatus is None:
                                            friendShipStatus = False
                                            friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                            if friendShipRequestsMyList is not None:
                                                for friendShipRequestsList in friendShipRequestsMyList:
                                                    if friendShipRequestsList[0] == id:
                                                        friendShipStatus = "request"
                                            if friendShipStatus is False:
                                                friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                                if friendShipRequestsFriendList is not None:
                                                    for friendShipRequestsList in friendShipRequestsFriendList:
                                                        if friendShipRequestsList[0] == requestUserId:
                                                            friendShipStatus = "myRequest"
                                            friendShipStatusDict.update({requestUserId: friendShipStatus})
                                            friendShipToSet.update({id: friendShipStatusDict})
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                privacyDict.update({id: {requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo}}})
                            else:
                                viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                                viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                                if viewMainInfo is None or viewTHPSInfo is None:
                                    if id in friendsIds:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    else:
                                        friendShipStatusDict = friendShipDictMany.get(id, None)
                                        if friendShipStatusDict is None:
                                            friendShipStatus = False
                                            friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                            if friendShipRequestsMyList is not None:
                                                for friendShipRequestsList in friendShipRequestsMyList:
                                                    if friendShipRequestsList[0] == id:
                                                        friendShipStatus = "request"
                                            if friendShipStatus is False:
                                                friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                                if friendShipRequestsFriendList is not None:
                                                    for friendShipRequestsList in friendShipRequestsFriendList:
                                                        if friendShipRequestsList[0] == requestUserId:
                                                            friendShipStatus = "myRequest"
                                            friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                                        else:
                                            friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                                            if friendShipStatus is None:
                                                friendShipStatus = False
                                                friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                                if friendShipRequestsMyList is not None:
                                                    for friendShipRequestsList in friendShipRequestsMyList:
                                                        if friendShipRequestsList[0] == id:
                                                            friendShipStatus = "request"
                                                if friendShipStatus is False:
                                                    friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                                    if friendShipRequestsFriendList is not None:
                                                        for friendShipRequestsList in friendShipRequestsFriendList:
                                                            if friendShipRequestsList[0] == requestUserId:
                                                                friendShipStatus = "myRequest"
                                                friendShipStatusDict.update({requestUserId: friendShipStatus})
                                                friendShipToSet.update({id: friendShipStatusDict})
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                                    if viewMainInfo is None:
                                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                        userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                                    if viewTHPSInfo is None:
                                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                        userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                                    privacyToSet.update({id: privacyDict})
                        if primaryName is None and subName is None:
                            if viewMainInfo:
                                firstName = user.first_name or None
                                lastName = user.last_name or None
                                if firstName and lastName:
                                    name = '{0} {1}'.format(firstName, lastName)
                                    smallName = user.username
                                    mainThumb.update({'name': name, 'smallName': smallName})
                                    namesResult.update({id: [name, smallName]})
                                else:
                                    if firstName:
                                        name = firstName
                                        smallName = user.username
                                        mainThumb.update({'name': name, 'smallName': smallName})
                                        namesResult.update({id: [name, smallName]})
                                    else:
                                        name = user.username
                                        mainThumb.update({'name': name, 'smallName': ''})
                                        namesResult.update({id: [name, '']})
                            else:
                                name = user.username
                                mainThumb.update({'name': name, 'smallName': ''})
                                namesResult.update({id: [name, '']})
                        else:
                            if primaryName is None:
                                if viewMainInfo:
                                    firstName = user.first_name or None
                                    lastName = user.last_name or None
                                    if firstName and lastName:
                                        name = '{0} {1}'.format(firstName, lastName)
                                    else:
                                        if firstName:
                                            name = firstName
                                        else:
                                            name = user.username
                                else:
                                    name = user.username
                                mainThumb.update({'name': name})
                                namesResult.update({id: [name, subName]})
                            else:
                                if viewMainInfo:
                                    firstName = user.first_name or None
                                    if firstName:
                                        smallName = user.username
                                    else:
                                        smallName = ''
                                else:
                                    smallName = ''
                                mainThumb.update({'smallName': smallName})
                                namesResult.update({id: [primaryName, smallName]})
                        mainThumbItemsToSet.update({id: mainThumbsDict})
                    else:
                        namesResult.update({id: [primaryName, subName]})
        smallThumbsDict = smallDialogThumbsMany.get(id, None)
        if smallThumbsDict is None:
            if user is None:
                user = users.get(id__exact = id)
            if viewMainInfo is None:
                privacyDict = privacyDictMany.get(id, None)
                if privacyDict is None:
                    if id in friendsIds:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                    else:
                        friendShipStatusDict = friendShipDictMany.get(id, None)
                        if friendShipStatusDict is None:
                            friendShipStatus = False
                            friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                            if friendShipRequestsMyList is not None:
                                for friendShipRequestsList in friendShipRequestsMyList:
                                    if friendShipRequestsList[0] == id:
                                        friendShipStatus = "request"
                            if friendShipStatus is False:
                                friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                if friendShipRequestsFriendList is not None:
                                    for friendShipRequestsList in friendShipRequestsFriendList:
                                        if friendShipRequestsList[0] == requestUserId:
                                            friendShipStatus = "myRequest"
                            friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                        else:
                            friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                            if friendShipStatus is None:
                                friendShipStatus = False
                                friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                if friendShipRequestsMyList is not None:
                                    for friendShipRequestsList in friendShipRequestsMyList:
                                        if friendShipRequestsList[0] == id:
                                            friendShipStatus = "request"
                                if friendShipStatus is False:
                                    friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                    if friendShipRequestsFriendList is not None:
                                        for friendShipRequestsList in friendShipRequestsFriendList:
                                            if friendShipRequestsList[0] == requestUserId:
                                                friendShipStatus = "myRequest"
                                friendShipStatusDict.update({requestUserId: friendShipStatus})
                                friendShipToSet.update({id: friendShipStatusDict})
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    privacyToSet.update({id: {requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar}}})
                else:
                    userPrivacyDict = privacyDict.get(requestUserId, None)
                    if userPrivacyDict is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            friendShipStatusDict = friendShipDictMany.get(id, None)
                            if friendShipStatusDict is None:
                                friendShipStatus = False
                                friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                if friendShipRequestsMyList is not None:
                                    for friendShipRequestsList in friendShipRequestsMyList:
                                        if friendShipRequestsList[0] == id:
                                            friendShipStatus = "request"
                                if friendShipStatus is False:
                                    friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                    if friendShipRequestsFriendList is not None:
                                        for friendShipRequestsList in friendShipRequestsFriendList:
                                            if friendShipRequestsList[0] == requestUserId:
                                                friendShipStatus = "myRequest"
                                friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                            else:
                                friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                                if friendShipStatus is None:
                                    friendShipStatus = False
                                    friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                    if friendShipRequestsMyList is not None:
                                        for friendShipRequestsList in friendShipRequestsMyList:
                                            if friendShipRequestsList[0] == id:
                                                friendShipStatus = "request"
                                    if friendShipStatus is False:
                                        friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                        if friendShipRequestsFriendList is not None:
                                            for friendShipRequestsList in friendShipRequestsFriendList:
                                                if friendShipRequestsList[0] == requestUserId:
                                                    friendShipStatus = "myRequest"
                                    friendShipStatusDict.update({requestUserId: friendShipStatus})
                                    friendShipToSet.update({id: friendShipStatusDict})
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        privacyDict.update({requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar}})
                        privacyToSet.update({id: privacyDict})
                    else:
                        viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                        viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                        viewAvatar = userPrivacyDict.get('viewAvatar', None)
                        if viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                friendShipStatusDict = friendShipDictMany.get(id, None)
                                if friendShipStatusDict is None:
                                    friendShipStatus = False
                                    friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                    if friendShipRequestsMyList is not None:
                                        for friendShipRequestsList in friendShipRequestsMyList:
                                            if friendShipRequestsList[0] == id:
                                                friendShipStatus = "request"
                                    if friendShipStatus is False:
                                        friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                        if friendShipRequestsFriendList is not None:
                                            for friendShipRequestsList in friendShipRequestsFriendList:
                                                if friendShipRequestsList[0] == requestUserId:
                                                    friendShipStatus = "myRequest"
                                    friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                                else:
                                    friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                                    if friendShipStatus is None:
                                        friendShipStatus = False
                                        friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                        if friendShipRequestsMyList is not None:
                                            for friendShipRequestsList in friendShipRequestsMyList:
                                                if friendShipRequestsList[0] == id:
                                                    friendShipStatus = "request"
                                        if friendShipStatus is False:
                                            friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                            if friendShipRequestsFriendList is not None:
                                                for friendShipRequestsList in friendShipRequestsFriendList:
                                                    if friendShipRequestsList[0] == requestUserId:
                                                        friendShipStatus = "myRequest"
                                        friendShipStatusDict.update({requestUserId: friendShipStatus})
                                        friendShipToSet.update({id: friendShipStatusDict})
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                            if viewMainInfo is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                            if viewTHPSInfo is None:
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                            if viewAvatar is None:
                                viewAvatar = privacyChecker.checkAvatarPrivacy()
                                userPrivacyDict.update({'viewAvatar': viewAvatar})
                            privacyToSet.update({id: privacyDict})
            if viewMainInfo:
                if name is None:
                    firstName = user.first_name or None
                    lastName = user.last_name or None
                    if firstName is not None and lastName is not None:
                        name = '{0} {1}'.format(firstName, lastName)
                        smallName = user.username
                    else:
                        if firstName is not None:
                            name = firstName
                            smallName = user.username
                        else:
                            name = user.username
                            smallName = ''
            else:
                if name is None:
                    name = user.username
            if viewAvatar:
                avatar = user.avatarThumbSmaller or None
                if avatar is not None:
                    if smallName:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><p><a href = "{1}">{3}</a></p><small>{4}</small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, smallName)
                    else:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><p><a href = "{1}">{3}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                else:
                    if smallName:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2} [{3}]</p></div></a><p><a href = "{1}">{2}</a></p><small></small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name, smallName)
                    else:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2}</p></div></a><p><a href = "{1}">{2}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
            else:
                if smallName:
                    listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2} [{3}]</p></div></a><p><a href = "{1}">{2}</a></p><small></small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name, smallName)
                else:
                    listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2}</p></div></a><p><a href = "{1}">{2}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
            smallThumbsResult.update({id: listThumbItem})
            smallThumbItemsToSet.update({id: {requestUserId: listThumbItem}})
        else:
            smallThumb = smallThumbsDict.get(requestUserId, None)
            if smallThumb is None:
                if user is None:
                    user = users.get(id__exact = id)
                if viewMainInfo is None:
                    privacyDict = privacyDictMany.get(id, None)
                    if privacyDict is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            friendShipStatusDict = friendShipDictMany.get(id, None)
                            if friendShipStatusDict is None:
                                friendShipStatus = False
                                friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                if friendShipRequestsMyList is not None:
                                    for friendShipRequestsList in friendShipRequestsMyList:
                                        if friendShipRequestsList[0] == id:
                                            friendShipStatus = "request"
                                if friendShipStatus is False:
                                    friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                    if friendShipRequestsFriendList is not None:
                                        for friendShipRequestsList in friendShipRequestsFriendList:
                                            if friendShipRequestsList[0] == requestUserId:
                                                friendShipStatus = "myRequest"
                                friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                            else:
                                friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                                if friendShipStatus is None:
                                    friendShipStatus = False
                                    friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                    if friendShipRequestsMyList is not None:
                                        for friendShipRequestsList in friendShipRequestsMyList:
                                            if friendShipRequestsList[0] == id:
                                                friendShipStatus = "request"
                                    if friendShipStatus is False:
                                        friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                        if friendShipRequestsFriendList is not None:
                                            for friendShipRequestsList in friendShipRequestsFriendList:
                                                if friendShipRequestsList[0] == requestUserId:
                                                    friendShipStatus = "myRequest"
                                    friendShipStatusDict.update({requestUserId: friendShipStatus})
                                    friendShipToSet.update({id: friendShipStatusDict})
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        privacyToSet.update({id: {requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar}}})
                    else:
                        userPrivacyDict = privacyDict.get(requestUserId, None)
                        if userPrivacyDict is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                friendShipStatusDict = friendShipDictMany.get(id, None)
                                if friendShipStatusDict is None:
                                    friendShipStatus = False
                                    friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                    if friendShipRequestsMyList is not None:
                                        for friendShipRequestsList in friendShipRequestsMyList:
                                            if friendShipRequestsList[0] == id:
                                                friendShipStatus = "request"
                                    if friendShipStatus is False:
                                        friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                        if friendShipRequestsFriendList is not None:
                                            for friendShipRequestsList in friendShipRequestsFriendList:
                                                if friendShipRequestsList[0] == requestUserId:
                                                    friendShipStatus = "myRequest"
                                    friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                                else:
                                    friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                                    if friendShipStatus is None:
                                        friendShipStatus = False
                                        friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                        if friendShipRequestsMyList is not None:
                                            for friendShipRequestsList in friendShipRequestsMyList:
                                                if friendShipRequestsList[0] == id:
                                                    friendShipStatus = "request"
                                        if friendShipStatus is False:
                                            friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                            if friendShipRequestsFriendList is not None:
                                                for friendShipRequestsList in friendShipRequestsFriendList:
                                                    if friendShipRequestsList[0] == requestUserId:
                                                        friendShipStatus = "myRequest"
                                        friendShipStatusDict.update({requestUserId: friendShipStatus})
                                        friendShipToSet.update({id: friendShipStatusDict})
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            privacyDict.update({requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar}})
                            privacyToSet.update({id: privacyDict})
                        else:
                            viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                            viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                            viewAvatar = userPrivacyDict.get('viewAvatar', None)
                            if viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None:
                                if id in friendsIds:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                else:
                                    friendShipStatusDict = friendShipDictMany.get(id, None)
                                    if friendShipStatusDict is None:
                                        friendShipStatus = False
                                        friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                        if friendShipRequestsMyList is not None:
                                            for friendShipRequestsList in friendShipRequestsMyList:
                                                if friendShipRequestsList[0] == id:
                                                    friendShipStatus = "request"
                                        if friendShipStatus is False:
                                            friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                            if friendShipRequestsFriendList is not None:
                                                for friendShipRequestsList in friendShipRequestsFriendList:
                                                    if friendShipRequestsList[0] == requestUserId:
                                                        friendShipStatus = "myRequest"
                                        friendShipToSet.update({id: {requestUserId: friendShipStatus}})
                                    else:
                                        friendShipStatus = friendShipStatusDict.get(requestUserId, None)
                                        if friendShipStatus is None:
                                            friendShipStatus = False
                                            friendShipRequestsMyList = friendShipRequestsMany.get(requestUserId, None)
                                            if friendShipRequestsMyList is not None:
                                                for friendShipRequestsList in friendShipRequestsMyList:
                                                    if friendShipRequestsList[0] == id:
                                                        friendShipStatus = "request"
                                            if friendShipStatus is False:
                                                friendShipRequestsFriendList = friendShipRequestsMany.get(id, None)
                                                if friendShipRequestsFriendList is not None:
                                                    for friendShipRequestsList in friendShipRequestsFriendList:
                                                        if friendShipRequestsList[0] == requestUserId:
                                                            friendShipStatus = "myRequest"
                                            friendShipStatusDict.update({requestUserId: friendShipStatus})
                                            friendShipToSet.update({id: friendShipStatusDict})
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = friendShipStatus, isAuthenticated = True)
                                if viewMainInfo is None:
                                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                    userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                                if viewTHPSInfo is None:
                                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                    userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                                if viewAvatar is None:
                                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                                    userPrivacyDict.update({'viewAvatar': viewAvatar})
                                privacyToSet.update({id: privacyDict})
                if viewMainInfo:
                    if name is None:
                        firstName = user.first_name or None
                        lastName = user.last_name or None
                        if firstName is not None and lastName is not None:
                            name = '{0} {1}'.format(firstName, lastName)
                            smallName = user.username
                        else:
                            if firstName is not None:
                                name = firstName
                                smallName = user.username
                            else:
                                name = user.username
                                smallName = ''
                else:
                    if name is None:
                        name = user.username
                    if smallName is None:
                        smallName = ''
                if viewAvatar:
                    avatar = user.avatarThumbSmaller or None
                    if avatar is not None:
                        if smallName:
                            listThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><p><a href = "{1}">{3}</a></p><small>{4}</small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, smallName)
                        else:
                            listThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><p><a href = "{1}">{3}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                    else:
                        if smallName:
                            listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2} [{3}]</p></div></a><p><a href = "{1}">{2}</a></p><small></small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name, smallName)
                        else:
                            listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2}</p></div></a><p><a href = "{1}">{2}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                else:
                    if smallName:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2} [{3}]</p></div></a><p><a href = "{1}">{2}</a></p><small></small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name, smallName)
                    else:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2}</p></div></a><p><a href = "{1}">{2}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                smallThumbsResult.update({id: listThumbItem})
                smallThumbsDict.update({requestUserId: listThumbItem})
                smallThumbItemsToSet.update({id: smallThumbsDict})
            else:
                smallThumbsResult.update({id: smallThumb})
    if len(thumbsSurplus) > 0:
        if len(mainThumbsSurplus) > 0 or len(namesSurplus) > 0:
            mainDialogThumbsCache.set_many(mainThumbItemsToSet)
        if len(smallThumbsSurplus) > 0:
            smallDialogThumbsCache.set_many(smallThumbItemsToSet)
        if len(privacySurplus) > 0:
            privacyCache.set_many(privacyToSet)
            if len(friendShipSurplus) > 0:
                friendShipCache.set_many(friendShipToSet)

    return mainThumbsResult, smallThumbsResult, namesResult, onlineStatusDict, activeStatusDict

def getMainAndSmallThumbsXHR():
    ''' Создание/выдача или выдача из кеша двух словарей с thumb, 1 с именами пользователей, и 2 словаря статуса: 
    {userId: smallThumbHTMLString}, {userId: mainThumbHTMLString} '''
    textDict = None
    onlineStatusText = None
    iffyStatusText = None
    activeStatusText = None
    middleStatusText = None
    leaveStatusText = None
    mainThumbsResult = {}
    smallThumbsResult = {}
    namesResult = {}
    onlineStatusDict = {}
    activeStatusDict = {}
    onlineUsersSet = set(caches['onlineUsers'].get_many(iterableIds).keys())
    iffyTimeMany = caches['iffyUsersOnlineStatus'].get_many((iterableIds - onlineUsersSet))
    chatStatusActiveSet = set(caches['chatStatusActive'].get_many(iterableIds).keys())
    chatStatusMiddleMany = caches['chatStatusMiddle'].get_many((iterableIds - chatStatusActiveSet))
    smallDialogThumbsCache = caches['messagesListThumbs']
    mainDialogThumbsCache = caches['dialogThumbs']
    smallDialogThumbsMany = smallDialogThumbsCache.get_many(iterableIds)
    mainDialogThumbsMany = mainDialogThumbsCache.get_many(iterableIds)
    smallDialogThumbsManyKeys = set(smallDialogThumbsMany.keys())
    mainDialogThumbsManyKeys = set(mainDialogThumbsMany.keys())
    mainThumbsSurplus = iterableIds - mainDialogThumbsManyKeys
    smallThumbsSurplus = iterableIds - smallDialogThumbsManyKeys
    smallThumbsSurplus1 = set()
    for id in smallDialogThumbsManyKeys:
        thumb = smallDialogThumbsMany[id].get(requestUserId, None)
        if thumb is None:
            smallThumbsSurplus1.add(id)
    namesSurplus = set()
    mainThumbsSurplus1 = set()
    for id in mainDialogThumbsManyKeys:
        thumb = mainDialogThumbsMany[id].get(requestUserId, None)
        if thumb is None:
            mainThumbsSurplus1.add(id)
        else:
            keys = thumb.keys()
            if "name" not in keys or "smallName" not in keys:
                namesSurplus.add(id)
    mainThumbsSurplus = mainThumbsSurplus | mainThumbsSurplus1
    smallThumbsSurplus = smallThumbsSurplus | smallThumbsSurplus1
    thumbsSurplus = smallThumbsSurplus | mainThumbsSurplus | namesSurplus
    if len(thumbsSurplus) > 0:
        user = None
        users = mainUserProfile.objects.filter(id__in = thumbsSurplus)
        itemString1 = '</div>'
        if len(mainThumbsSurplus) > 0 or len(namesSurplus) > 0:
            mainThumbItemsToSet = {}
            if requestUserId in mainThumbsSurplus:
                user = users.get(id__exact = requestUserId)
                firstName = user.first_name or None
                lastName = user.last_name or None
                if firstName and lastName:
                    name = '{0} {1}'.format(firstName, lastName)
                    smallName = user.username
                else:
                    if firstName:
                        name = firstName
                        smallName = user.username
                    else:
                        name = user.username
                        smallName = ''
                avatar = user.avatarThumbMiddleSmall or None
                if avatar is not None:
                    if smallName:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, smallName)
                    else:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                itemTuple = (itemString0, itemString1)
                mainThumbsDict = mainDialogThumbsMany.get(requestUserId, None)
                if mainThumbsDict is None:
                    mainThumbItemsToSet.update({requestUserId: {requestUserId: {'item': itemTuple, 'showOnlineStatus': True, 'showActiveStatus': True, 'name': name, 'smallName': smallName}}})
                else:
                    mainThumb = mainThumbsDict.get(requestUserId, None)
                    if mainThumb is None:
                        pass
                    else:
                        pass
                namesResult.update({requestUserId: [name, smallName]})
                mainThumbsResult.update({requestUserId: itemTuple})
            else:
                if requestUserId in namesSurplus:
                    user = users.get(id__exact = requestUserId)
                    mainThumbsDict = mainDialogThumbsMany.get(requestUserId, None)
                    if mainThumbsDict is None:
                        pass
                    else:
                        mainThumb = mainThumbsDict.get(requestUserId, None)
                        if mainThumb is None:
                            pass

        if len(smallThumbsSurplus) > 0:
            smallThumbItemsToSet = {}
            if user is None:
                user = users.get(id__exact = requestUserId)

        privacyCache = caches['privacyCache']
        privacyDictMany = privacyCache.get_many(thumbsSurplus)
        privacyManyKeys = set(privacyDictMany.keys())
        privacySurplus = thumbsSurplus - privacyManyKeys
        privacySurplus1 = set()
        for id in privacyManyKeys:
            privacyDict = privacyDictMany[id]
            if requestUserId not in privacyDict:
                privacySurplus1.add(id)
            else:
                if ('viewMainInfo' or 'viewTHPSInfo' or 'viewAvatar' or 'viewOnlineStatus' or 'viewActiveStatus') not in privacyDict.keys():
                    privacySurplus1.add(id)
        privacySurplus = privacySurplus | privacySurplus1
        if len(privacySurplus) > 0:
            privacySettings = userProfilePrivacy.objects.filter(user_id__in = privacySurplus)
            friendsIdsCache = caches['friendsIdCache']
            friendsIds = friendsIdsCache.get(requestUserId, None)
            if friendsIds is None:
                friendsCache = caches['friendsCache']
                friends = friendsCache.get(requestUserId, None)
                if friends is None:
                    friends = requestUser.friends.all()
                    if friends.count():
                        friendsCache.set(requestUserId, friends)
                        friendsIdsQuerySet = friends.values("id")
                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                    else:
                        friendsCache.set(requestUserId, False)
                        friendsIds = set()
                else:
                    if friends is False:
                        friendsIds = set()
                    else:
                        friendsIdsQuerySet = friends.values("id")
                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                friendsIdsCache.set(requestUserId, friendsIds)
            if len(friendsIds) > 0:
                friendShipSurplus = privacySurplus - friendsIds
                if len(friendShipSurplus) > 0:
                    friendShipCache = caches['friendShipStatusCache']
                    friendShipDictMany = friendShipCache.get_many(friendShipSurplus)
                    friendShipManyKeys = set(friendShipDictMany.keys())
                    friendShipRequestsSurplus = friendShipSurplus - friendShipManyKeys
                    if len(friendShipRequestsSurplus) > 0:
                        friendShipRequestsMany = caches['friendshipRequests'].get_many(friendShipRequestsSurplus)
                        friendShipToSet = {}
            else:
                friendShipSurplus = set()
                if len(privacySurplus) > 0:
                    friendShipCache = caches['friendShipStatusCache']
                    friendShipDictMany = friendShipCache.get_many(privacySurplus)
                    friendShipManyKeys = set(friendShipDictMany.keys())
                    friendShipRequestsSurplus = privacySurplus - friendShipManyKeys
                    if len(friendShipRequestsSurplus) > 0:
                        friendShipRequestsMany = caches['friendshipRequests'].get_many(friendShipRequestsSurplus)
                        friendShipToSet = {}
            privacyToSet = {}

    for id in iterableIds:
        user = None
        name = None
        profileUrl = None
        viewMainInfo = None
        mainThumbsDict = mainDialogThumbsMany.get(id, None)
        if mainThumbsDict is None:
            user = users.get(id__exact = id)
            privacyDict = privacyDictMany.get(id, None)
            if privacyDict is None:
                if id in friendsIds:
                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                else:
                    friendShipStatus = friendShipDict.get(id, None)
                    if friendShipStatus is None:
                        if friendShipRequests is None:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            friendShipToSet.update({requestUserId: {id: False}})
                        else:
                            friendShipRequest = friendShipRequests.get(id, None)
                            if friendShipRequest is None:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                friendShipToSet.update({requestUserId: {id: False}})
                            elif friendShipRequest == "request":
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                friendShipToSet.update({requestUserId: {id: "request"}})
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                friendShipToSet.update({requestUserId: {id: "myRequest"}})
                    elif friendShipStatus is False:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                    elif friendShipStatus == "request":
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                    else:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                viewAvatar = privacyChecker.checkAvatarPrivacy()
                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                privacyToSet.update({id: {requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
            else:
                userPrivacyDict = privacyDict.get(requestUserId, None)
                if userPrivacyDict is None:
                    if id in friendsIds:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                    else:
                        friendShipStatus = friendShipDict.get(id, None)
                        if friendShipStatus is None:
                            if friendShipRequests is None:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                friendShipToSet.update({requestUserId: {id: False}})
                            else:
                                friendShipRequest = friendShipRequests.get(id, None)
                                if friendShipRequest is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                elif friendShipRequest == "request":
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: "request"}})
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: "myRequest"}})
                        elif friendShipStatus is False:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        elif friendShipStatus == "request":
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                    privacyDict.update({requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
                    privacyToSet.update({id: privacyDict})
                else:
                    viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                    viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                    viewAvatar = userPrivacyDict.get('viewAvatar', None)
                    viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                    viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                    if viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None or viewActiveStatus is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            friendShipStatus = friendShipDict.get(id, None)
                            if friendShipStatus is None:
                                if friendShipRequests is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                else:
                                    friendShipRequest = friendShipRequests.get(id, None)
                                    if friendShipRequest is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    elif friendShipRequest == "request":
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "request"}})
                                    else:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "myRequest"}})
                            elif friendShipStatus is False:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            elif friendShipStatus == "request":
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        if viewMainInfo is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                        if viewTHPSInfo is None:
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                        if viewAvatar is None:
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            userPrivacyDict.update({'viewAvatar': viewAvatar})
                        if viewOnlineStatus is None:
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                        if viewActiveStatus is None:
                            viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                            userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                        privacyToSet.update({id: privacyDict})
            if viewMainInfo:
                firstName = user.first_name or None
                lastName = user.last_name or None
                if firstName and lastName:
                    name = '{0} {1}'.format(firstName, lastName)
                    smallName = user.username
                else:
                    if firstName:
                        name = firstName
                        smallName = user.username
                    else:
                        name = user.username
                        smallName = ''
            else:
                name = user.username
                smallName = ''
            if viewAvatar:
                avatar = user.avatarThumbMiddleSmall or None
                if avatar is not None:
                    if smallName:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, smallName)
                    else:
                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
            else:
                if smallName:
                    itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                else:
                    itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
            if viewOnlineStatus and viewActiveStatus:
                if id in onlineUsersSet:
                    if onlineStatusText is None:
                        if textDict is None:
                            textDict = usersListText(lang = 'ru').getDict()
                        onlineStatusText = textDict.pop('onlineStatus')
                    if id in chatStatusActiveSet:
                        if activeStatusText is None:
                            if textDict is None:
                                textDict = usersListText(lang = 'ru').getDict()
                            activeStatusText = textDict.pop('activeStatus')
                        mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), itemString1)})
                        activeStatusDict.update({id: 'stateActive'})
                    else:
                        middleTupleStr = chatStatusMiddleMany.get(id, None)
                        if middleTupleStr is not None:
                            if middleStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                middleStatusText = textDict.pop('middleStatus')
                            mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "middle">{1}</p></noscript>'.format(onlineStatusText, middleStatusText), itemString1)})
                            activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                        else:
                            if leaveStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                leaveStatusText = textDict.pop('leaveStatus')
                            mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "leave">{1}</p></noscript>'.format(onlineStatusText, leaveStatusText), itemString1)})
                    onlineStatusDict.update({id: 'online'})
                else:
                    iffyTimeStr = iffyTimeMany.get(id, None)
                    if iffyTimeStr is not None:
                        if iffyStatusText is None:
                            if textDict is None:
                                textDict = usersListText(lang = 'ru').getDict()
                            iffyStatusText = textDict.pop('uknownStatus')
                        if id in chatStatusActiveSet:
                            if activeStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                activeStatusText = textDict.pop('activeStatus')
                            mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "active">{1}</p></noscript>'.format(iffyStatusText, activeStatusText), itemString1)})
                            activeStatusDict.update({id: 'stateActive'})
                        else:
                            middleTupleStr = chatStatusMiddleMany.get(id, None)
                            if middleTupleStr is not None:
                                if middleStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    middleStatusText = textDict.pop('middleStatus')
                                mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "middle">{1}</p></noscript>'.format(iffyStatusText, middleStatusText), itemString1)})
                                activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                            else:
                                if leaveStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    leaveStatusText = textDict.pop('leaveStatus')
                                mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "leave">{1}</p></noscript>'.format(iffyStatusText, leaveStatusText), itemString1)})
                        onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                    else:
                        mainThumbsResult.update({id: (itemString0, itemString1)})
            else:
                if viewOnlineStatus:
                    if id in onlineUsersSet:
                        if onlineStatusText is None:
                            if textDict is None:
                                textDict = usersListText(lang = 'ru').getDict()
                            onlineStatusText = textDict.pop('onlineStatus')
                        mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p></noscript>'.format(onlineStatusText), itemString1)})
                        onlineStatusDict.update({id: 'online'})
                    else:
                        iffyTimeStr = iffyTimeMany.get(id, None)
                        if iffyTimeStr is not None:
                            if iffyStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                iffyStatusText = textDict.pop('uknownStatus')
                            mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText), itemString1)})
                            onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                        else:
                            mainThumbsResult.update({id: (itemString0, itemString1)})
                elif viewActiveStatus:
                    if id in chatStatusActiveSet:
                        if activeStatusText is None:
                            if textDict is None:
                                textDict = usersListText(lang = 'ru').getDict()
                            activeStatusText = textDict.pop('activeStatus')
                        mainThumbsResult.update({id: (itemString0, '<noscript><p class = "active">{0}</p></noscript>'.format(activeStatusText), itemString1)})
                        activeStatusDict.update({id: 'stateActive'})
                    else:
                        middleTupleStr = chatStatusMiddleMany.get(id, None)
                        if middleTupleStr is not None:
                            if middleStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                middleStatusText = textDict.pop('middleStatus')
                            mainThumbsResult.update({id: (itemString0, '<noscript><p class = "middle">{0}</p></noscript>'.format(middleStatusText), itemString1)})
                            activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                        else:
                            mainThumbsResult.update({id: (itemString0, itemString1)})
                else:
                    mainThumbsResult.update({id: (itemString0, itemString1)})
            if smallName:
                mainThumbItemsToSet.update({id: {requestUserId: {'item': (itemString0, itemString1), 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus, 'name': name, 'smallName': smallName}}})
                namesResult.update({id: [name, smallName]})
            else:
                mainThumbItemsToSet.update({id: {requestUserId: {'item': (itemString0, itemString1), 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus, 'name': name, 'smallName': ''}}})
                namesResult.update({id: [name, '']})
        else:
            mainThumb = mainThumbsDict.get(requestUserId, None)
            if mainThumb is None:
                user = users.get(id__exact = id)
                privacyDict = privacyDictMany.get(id, None)
                if privacyDict is None:
                    if id in friendsIds:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                    else:
                        friendShipStatus = friendShipDict.get(id, None)
                        if friendShipStatus is None:
                            if friendShipRequests is None:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                friendShipToSet.update({requestUserId: {id: False}})
                            else:
                                friendShipRequest = friendShipRequests.get(id, None)
                                if friendShipRequest is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                elif friendShipRequest == "request":
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: "request"}})
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: "myRequest"}})
                        elif friendShipStatus is False:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        elif friendShipStatus == "request":
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                    privacyToSet.update({id: {requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
                else:
                    userPrivacyDict = privacyDict.get(requestUserId, None)
                    if userPrivacyDict is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            friendShipStatus = friendShipDict.get(id, None)
                            if friendShipStatus is None:
                                if friendShipRequests is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                else:
                                    friendShipRequest = friendShipRequests.get(id, None)
                                    if friendShipRequest is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    elif friendShipRequest == "request":
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "request"}})
                                    else:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "myRequest"}})
                            elif friendShipStatus is False:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            elif friendShipStatus == "request":
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                        privacyDict.update({requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
                        privacyToSet.update({id: privacyDict})
                    else:
                        viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                        viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                        viewAvatar = userPrivacyDict.get('viewAvatar', None)
                        viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                        viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                        if viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None or viewActiveStatus is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                friendShipStatus = friendShipDict.get(id, None)
                                if friendShipStatus is None:
                                    if friendShipRequests is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    else:
                                        friendShipRequest = friendShipRequests.get(id, None)
                                        if friendShipRequest is None:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: False}})
                                        elif friendShipRequest == "request":
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: "request"}})
                                        else:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: "myRequest"}})
                                elif friendShipStatus is False:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                elif friendShipStatus == "request":
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            if viewMainInfo is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                            if viewTHPSInfo is None:
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                            if viewAvatar is None:
                                viewAvatar = privacyChecker.checkAvatarPrivacy()
                                userPrivacyDict.update({'viewAvatar': viewAvatar})
                            if viewOnlineStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                            if viewActiveStatus is None:
                                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                            privacyToSet.update({id: privacyDict})
                if viewMainInfo:
                    firstName = user.first_name or None
                    lastName = user.last_name or None
                    if firstName and lastName:
                        name = '{0} {1}'.format(firstName, lastName)
                        smallName = user.username
                    else:
                        if firstName:
                            name = firstName
                            smallName = user.username
                        else:
                            name = user.username
                            smallName = ''
                else:
                    name = user.username
                    smallName = ''
                if viewAvatar:
                    avatar = user.avatarThumbMiddleSmall or None
                    if avatar is not None:
                        if smallName:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, smallName)
                        else:
                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                    else:
                        if smallName:
                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                        else:
                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                else:
                    if smallName:
                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                    else:
                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                if viewOnlineStatus and viewActiveStatus:
                    if id in onlineUsersSet:
                        if onlineStatusText is None:
                            if textDict is None:
                                textDict = usersListText(lang = 'ru').getDict()
                            onlineStatusText = textDict.pop('onlineStatus')
                        if id in chatStatusActiveSet:
                            if activeStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                activeStatusText = textDict.pop('activeStatus')
                            mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), itemString1)})
                            activeStatusDict.update({id: 'stateActive'})
                        else:
                            middleTupleStr = chatStatusMiddleMany.get(id, None)
                            if middleTupleStr is not None:
                                if middleStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    middleStatusText = textDict.pop('middleStatus')
                                mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "middle">{1}</p></noscript>'.format(onlineStatusText, middleStatusText), itemString1)})
                                activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                            else:
                                if leaveStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    leaveStatusText = textDict.pop('leaveStatus')
                                mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "leave">{1}</p></noscript>'.format(onlineStatusText, leaveStatusText), itemString1)})
                        onlineStatusDict.update({id: 'online'})
                    else:
                        iffyTimeStr = iffyTimeMany.get(id, None)
                        if iffyTimeStr is not None:
                            if iffyStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                iffyStatusText = textDict.pop('uknownStatus')
                            if id in chatStatusActiveSet:
                                if activeStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    activeStatusText = textDict.pop('activeStatus')
                                mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "active">{1}</p></noscript>'.format(iffyStatusText, activeStatusText), itemString1)})
                                activeStatusDict.update({id: 'stateActive'})
                            else:
                                middleTupleStr = chatStatusMiddleMany.get(id, None)
                                if middleTupleStr is not None:
                                    if middleStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        middleStatusText = textDict.pop('middleStatus')
                                    mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "middle">{1}</p></noscript>'.format(iffyStatusText, middleStatusText), itemString1)})
                                    activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                                else:
                                    if leaveStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        leaveStatusText = textDict.pop('leaveStatus')
                                    mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "leave">{1}</p></noscript>'.format(iffyStatusText, leaveStatusText), itemString1)})
                            onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                        else:
                            mainThumbsResult.update({id: (itemString0, itemString1)})
                else:
                    if viewOnlineStatus:
                        if id in onlineUsersSet:
                            if onlineStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                onlineStatusText = textDict.pop('onlineStatus')
                            mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p></noscript>'.format(onlineStatusText), itemString1)})
                            onlineStatusDict.update({id: 'online'})
                        else:
                            iffyTimeStr = iffyTimeMany.get(id, None)
                            if iffyTimeStr is not None:
                                if iffyStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    iffyStatusText = textDict.pop('uknownStatus')
                                mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText), itemString1)})
                                onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                            else:
                                mainThumbsResult.update({id: (itemString0, itemString1)})
                    elif viewActiveStatus:
                        if id in chatStatusActiveSet:
                            if activeStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                activeStatusText = textDict.pop('activeStatus')
                            mainThumbsResult.update({id: (itemString0, '<noscript><p class = "active">{0}</p></noscript>'.format(activeStatusText), itemString1)})
                            activeStatusDict.update({id: 'stateActive'})
                        else:
                            middleTupleStr = chatStatusMiddleMany.get(id, None)
                            if middleTupleStr is not None:
                                if middleStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    middleStatusText = textDict.pop('middleStatus')
                                mainThumbsResult.update({id: (itemString0, '<noscript><p class = "middle">{0}</p></noscript>'.format(middleStatusText), itemString1)})
                                activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                            else:
                                mainThumbsResult.update({id: (itemString0, itemString1)})
                    else:
                        mainThumbsResult.update({id: (itemString0, itemString1)})
                if smallName:
                    mainThumbsDict.update({requestUserId: {'item': (itemString0, itemString1), 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus, 'name': name, 'smallName': smallName}})
                    namesResult.update({id: [name, smallName]})
                else:
                    mainThumbsDict.update({requestUserId: {'item': (itemString0, itemString1), 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus, 'name': name, 'smallName': ''}})
                    namesResult.update({id: [name, '']})
                mainThumbItemsToSet.update({id: mainThumbsDict})
            else:
                primaryName = mainThumb.get('name', None)
                subName = mainThumb.get('smallName', None)
                thumbItem = mainThumb.get('item', None)
                if thumbItem is None:
                    user = users.get(id__exact = id)
                    privacyDict = privacyDictMany.get(id, None)
                    if privacyDict is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            friendShipStatus = friendShipDict.get(id, None)
                            if friendShipStatus is None:
                                if friendShipRequests is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                else:
                                    friendShipRequest = friendShipRequests.get(id, None)
                                    if friendShipRequest is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    elif friendShipRequest == "request":
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "request"}})
                                    else:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "myRequest"}})
                            elif friendShipStatus is False:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            elif friendShipStatus == "request":
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                        privacyDict = {'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}
                        if primaryName is None or subName is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            privacyDict.update({'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo})
                        privacyToSet.update({id: {requestUserId: privacyDict}})
                    else:
                        userPrivacyDict = privacyDict.get(requestUserId, None)
                        if userPrivacyDict is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                friendShipStatus = friendShipDict.get(id, None)
                                if friendShipStatus is None:
                                    if friendShipRequests is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    else:
                                        friendShipRequest = friendShipRequests.get(id, None)
                                        if friendShipRequest is None:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: False}})
                                        elif friendShipRequest == "request":
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: "request"}})
                                        else:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: "myRequest"}})
                                elif friendShipStatus is False:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                elif friendShipStatus == "request":
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                            userPrivacyDict = {'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}
                            if primaryName is None or subName is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo})
                            privacyDict.update({id: {requestUserId: userPrivacyDict}})
                        else:
                            if primaryName is None or subName is None:
                                viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                                viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                            else:
                                viewMainInfo = False
                                viewTHPSInfo = False

                            viewAvatar = userPrivacyDict.get('viewAvatar', None)
                            viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                            viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                            if viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None or viewActiveStatus is None:
                                if id in friendsIds:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                else:
                                    friendShipStatus = friendShipDict.get(id, None)
                                    if friendShipStatus is None:
                                        if friendShipRequests is None:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: False}})
                                        else:
                                            friendShipRequest = friendShipRequests.get(id, None)
                                            if friendShipRequest is None:
                                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                                friendShipToSet.update({requestUserId: {id: False}})
                                            elif friendShipRequest == "request":
                                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                                friendShipToSet.update({requestUserId: {id: "request"}})
                                            else:
                                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                                friendShipToSet.update({requestUserId: {id: "myRequest"}})
                                    elif friendShipStatus is False:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    elif friendShipStatus == "request":
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    else:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                if primaryName is None or subName is None:
                                    if viewMainInfo is None:
                                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                        userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                                    if viewTHPSInfo is None:
                                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                        userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                                if viewAvatar is None:
                                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                                    userPrivacyDict.update({'viewAvatar': viewAvatar})
                                if viewOnlineStatus is None:
                                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                    userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                                if viewActiveStatus is None:
                                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                    userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                                privacyToSet.update({id: privacyDict})
                    if primaryName is None or subName is None:
                        if primaryName is None and subName is None:
                            if viewMainInfo:
                                firstName = user.first_name or None
                                lastName = user.last_name or None
                                if firstName and lastName:
                                    name = '{0} {1}'.format(firstName, lastName)
                                    smallName = user.username
                                else:
                                    if firstName:
                                        name = firstName
                                        smallName = user.username
                                    else:
                                        name = user.username
                                        smallName = ''
                            else:
                                name = user.username
                                smallName = ''
                            if viewAvatar:
                                avatar = user.avatarThumbMiddleSmall or None
                                if avatar is not None:
                                    if smallName:
                                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, smallName)
                                    else:
                                        itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                                else:
                                    if smallName:
                                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                                    else:
                                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                            else:
                                if smallName:
                                    itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                                else:
                                    itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                        else:
                            if primaryName is None:
                                if viewMainInfo:
                                    firstName = user.first_name or None
                                    lastName = user.last_name or None
                                    if firstName and lastName:
                                        name = '{0} {1}'.format(firstName, lastName)
                                    else:
                                        if firstName:
                                            name = firstName
                                        else:
                                            name = user.username
                                else:
                                    name = user.username
                                if viewAvatar:
                                    avatar = user.avatarThumbMiddleSmall or None
                                    if avatar is not None:
                                        if subName:
                                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, subName)
                                        else:
                                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                                    else:
                                        if subName:
                                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, subName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                                        else:
                                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                                else:
                                    if subName:
                                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, name, subName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                                    else:
                                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, name, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                            else:
                                if viewMainInfo:
                                    firstName = user.first_name or None
                                    if firstName:
                                        smallName = user.username
                                    else:
                                        smallName = ''
                                else:
                                    smallName = ''
                                if viewAvatar:
                                    avatar = user.avatarThumbMiddleSmall or None
                                    if avatar is not None:
                                        if smallName:
                                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, primaryName, smallName)
                                        else:
                                            itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, primaryName)
                                    else:
                                        if smallName:
                                            itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, primaryName, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                                        else:
                                            itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, primaryName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                                else:
                                    if smallName:
                                        itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, primaryName, smallName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                                    else:
                                        itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, primaryName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                    else:
                        if viewAvatar:
                            avatar = user.avatarThumbMiddleSmall or None
                            if avatar is not None:
                                if subName:
                                    itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, primaryName, subName)
                                else:
                                    itemString0 = '<div value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(user.id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, primaryName)
                            else:
                                if subName:
                                    itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, primaryName, subName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                                else:
                                    itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, primaryName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                        else:
                            if subName:
                                itemString0 = '<div value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(user.id, primaryName, subName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                            else:
                                itemString0 = '<div value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(user.id, primaryName, reverse("profile", kwargs = {'currentUser': user.profileUrl}))
                    if viewOnlineStatus and viewActiveStatus:
                        if id in onlineUsersSet:
                            if onlineStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                onlineStatusText = textDict.pop('onlineStatus')
                            if id in chatStatusActiveSet:
                                if activeStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    activeStatusText = textDict.pop('activeStatus')
                                mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), itemString1)})
                                activeStatusDict.update({id: 'stateActive'})
                            else:
                                middleTupleStr = chatStatusMiddleMany.get(id, None)
                                if middleTupleStr is not None:
                                    if middleStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        middleStatusText = textDict.pop('middleStatus')
                                    mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "middle">{1}</p></noscript>'.format(onlineStatusText, middleStatusText), itemString1)})
                                    activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                                else:
                                    if leaveStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        leaveStatusText = textDict.pop('leaveStatus')
                                    mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p><p class = "leave">{1}</p></noscript>'.format(onlineStatusText, leaveStatusText), itemString1)})
                            onlineStatusDict.update({id: 'online'})
                        else:
                            iffyTimeStr = iffyTimeMany.get(id, None)
                            if iffyTimeStr is not None:
                                if iffyStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    iffyStatusText = textDict.pop('uknownStatus')
                                if id in chatStatusActiveSet:
                                    if activeStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        activeStatusText = textDict.pop('activeStatus')
                                    mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "active">{1}</p></noscript>'.format(iffyStatusText, activeStatusText), itemString1)})
                                    activeStatusDict.update({id: 'stateActive'})
                                else:
                                    middleTupleStr = chatStatusMiddleMany.get(id, None)
                                    if middleTupleStr is not None:
                                        if middleStatusText is None:
                                            if textDict is None:
                                                textDict = usersListText(lang = 'ru').getDict()
                                            middleStatusText = textDict.pop('middleStatus')
                                        mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "middle">{1}</p></noscript>'.format(iffyStatusText, middleStatusText), itemString1)})
                                        activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                                    else:
                                        if leaveStatusText is None:
                                            if textDict is None:
                                                textDict = usersListText(lang = 'ru').getDict()
                                            leaveStatusText = textDict.pop('leaveStatus')
                                        mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p><p class = "leave">{1}</p></noscript>'.format(iffyStatusText, leaveStatusText), itemString1)})
                                onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                            else:
                                mainThumbsResult.update({id: (itemString0, itemString1)})
                    else:
                        if viewOnlineStatus:
                            if id in onlineUsersSet:
                                if onlineStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    onlineStatusText = textDict.pop('onlineStatus')
                                mainThumbsResult.update({id: (itemString0, '<noscript><p class = "online">{0}</p></noscript>'.format(onlineStatusText), itemString1)})
                                onlineStatusDict.update({id: 'online'})
                            else:
                                iffyTimeStr = iffyTimeMany.get(id, None)
                                if iffyTimeStr is not None:
                                    if iffyStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        iffyStatusText = textDict.pop('uknownStatus')
                                    mainThumbsResult.update({id: (itemString0, '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText), itemString1)})
                                    onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                                else:
                                    mainThumbsResult.update({id: (itemString0, itemString1)})
                        elif viewActiveStatus:
                            if id in chatStatusActiveSet:
                                if activeStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    activeStatusText = textDict.pop('activeStatus')
                                mainThumbsResult.update({id: (itemString0, '<noscript><p class = "active">{0}</p></noscript>'.format(activeStatusText), itemString1)})
                                activeStatusDict.update({id: 'stateActive'})
                            else:
                                middleTupleStr = chatStatusMiddleMany.get(id, None)
                                if middleTupleStr is not None:
                                    if middleStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        middleStatusText = textDict.pop('middleStatus')
                                    mainThumbsResult.update({id: (itemString0, '<noscript><p class = "middle">{0}</p></noscript>'.format(middleStatusText), itemString1)})
                                    activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                                else:
                                    mainThumbsResult.update({id: (itemString0, itemString1)})
                    itemTuple = (itemString0, itemString1)
                    if primaryName or subName is None:
                        if primaryName and subName is None:
                            if smallName:
                                mainThumb.update({'item': itemTuple, 'name': name, 'smallName': smallName, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus})
                                namesResult.update({id: [name, smallName]})
                            else:
                                mainThumb.update({'item': itemTuple, 'name': name, 'smallName': '', 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus})
                                namesResult.update({id: [name, '']})
                        else:
                            if primaryName is None:
                                mainThumb.update({'item': itemTuple, 'name': name, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus})
                                namesResult.update({id: [name, subName]})
                            else:
                                if smallName:
                                    mainThumb.update({'item': itemTuple, 'smallName': smallName, 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus})
                                    namesResult.update({id: [primaryName, smallName]})
                                else:
                                    mainThumb.update({'item': itemTuple, 'smallName': '', 'showOnlineStatus': viewOnlineStatus, 'showActiveStatus': viewActiveStatus})
                                    namesResult.update({id: [primaryName, '']})
                    mainThumbItemsToSet.update({id: mainThumbsDict})
                    mainThumbsResult.update({id: itemTuple})
                else:
                    viewOnlineStatus = mainThumb['showOnlineStatus']
                    viewActiveStatus = mainThumb['showActiveStatus']
                    if viewOnlineStatus and viewActiveStatus:
                        if id in onlineUsersSet:
                            if onlineStatusText is None:
                                if textDict is None:
                                    textDict = usersListText(lang = 'ru').getDict()
                                onlineStatusText = textDict.pop('onlineStatus')
                            if id in chatStatusActiveSet:
                                if activeStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    activeStatusText = textDict.pop('activeStatus')
                                mainThumbsResult.update({id: (thumbItem[0], '<noscript><p class = "online">{0}</p><p class = "active">{1}</p></noscript>'.format(onlineStatusText, activeStatusText), thumbItem[1])})
                                activeStatusDict.update({id: 'stateActive'})
                            else:
                                middleTupleStr = chatStatusMiddleMany.get(id, None)
                                if middleTupleStr is not None:
                                    if middleStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        middleStatusText = textDict.pop('middleStatus')
                                    mainThumbsResult.update({id: (thumbItem[0], '<noscript><p class = "online">{0}</p><p class = "middle">{1}</p></noscript>'.format(onlineStatusText, middleStatusText), thumbItem[1])})
                                    activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                                else:
                                    if leaveStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        leaveStatusText = textDict.pop('leaveStatus')
                                    mainThumbsResult.update({id: (thumbItem[0], '<noscript><p class = "online">{0}</p><p class = "leave">{1}</p></noscript>'.format(onlineStatusText, leaveStatusText), thumbItem[1])})
                            onlineStatusDict.update({id: 'online'})
                        else:
                            iffyTimeStr = iffyTimeMany.get(id, None)
                            if iffyTimeStr is not None:
                                if iffyStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    iffyStatusText = textDict.pop('uknownStatus')
                                if id in chatStatusActiveSet:
                                    if activeStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        activeStatusText = textDict.pop('activeStatus')
                                    mainThumbsResult.update({id: (thumbItem[0], '<noscript><p class = "iffy">{0}</p><p class = "active">{1}</p></noscript>'.format(iffyStatusText, activeStatusText), thumbItem[1])})
                                    activeStatusDict.update({id: 'stateActive'})
                                else:
                                    middleTupleStr = chatStatusMiddleMany.get(id, None)
                                    if middleTupleStr is not None:
                                        if middleStatusText is None:
                                            if textDict is None:
                                                textDict = usersListText(lang = 'ru').getDict()
                                            middleStatusText = textDict.pop('middleStatus')
                                        mainThumbsResult.update({id: (thumbItem[0], '<noscript><p class = "iffy">{0}</p><p class = "middle">{1}</p></noscript>'.format(iffyStatusText, middleStatusText), thumbItem[1])})
                                        activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                                    else:
                                        if leaveStatusText is None:
                                            if textDict is None:
                                                textDict = usersListText(lang = 'ru').getDict()
                                            leaveStatusText = textDict.pop('leaveStatus')
                                        mainThumbsResult.update({id: (thumbItem[0], '<noscript><p class = "iffy">{0}</p><p class = "leave">{1}</p></noscript>'.format(iffyStatusText, leaveStatusText), thumbItem[1])})
                                onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                            else:
                                mainThumbsResult.update({id: thumbItem})
                    else:
                        if viewOnlineStatus:
                            if id in onlineUsersSet:
                                if onlineStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    onlineStatusText = textDict.pop('onlineStatus')
                                mainThumbsResult.update({id: (thumbItem[0], '<noscript><p class = "online">{0}</p></noscript>'.format(onlineStatusText), thumbItem[1])})
                                onlineStatusDict.update({id: 'online'})
                            else:
                                iffyTimeStr = iffyTimeMany.get(id, None)
                                if iffyTimeStr is not None:
                                    if iffyStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        iffyStatusText = textDict.pop('uknownStatus')
                                    mainThumbsResult.update({id: (thumbItem[0], '<noscript><p class = "iffy">{0}</p></noscript>'.format(iffyStatusText), thumbItem[1])})
                                    onlineStatusDict.update({id: {'iffy': iffyTimeStr}})
                                else:
                                    mainThumbsResult.update({id: thumbItem})
                        elif viewActiveStatus:
                            if id in chatStatusActiveSet:
                                if activeStatusText is None:
                                    if textDict is None:
                                        textDict = usersListText(lang = 'ru').getDict()
                                    activeStatusText = textDict.pop('activeStatus')
                                mainThumbsResult.update({id: (thumbItem[0], '<noscript><p class = "active">{0}</p></noscript>'.format(activeStatusText), thumbItem[1])})
                                activeStatusDict.update({id: 'stateActive'})
                            else:
                                middleTupleStr = chatStatusMiddleMany.get(id, None)
                                if middleTupleStr is not None:
                                    if middleStatusText is None:
                                        if textDict is None:
                                            textDict = usersListText(lang = 'ru').getDict()
                                        middleStatusText = textDict.pop('middleStatus')
                                    mainThumbsResult.update({id: (thumbItem[0], '<noscript><p class = "middle">{0}</p></noscript>'.format(middleStatusText), thumbItem[1])})
                                    activeStatusDict.update({id: {'stateMiddle': middleTupleStr}})
                                else:
                                    mainThumbsResult.update({id: thumbItem})
                        else:
                            mainThumbsResult.update({id: (thumbItem[0], thumbItem[1])})
                    if primaryName is None or subName is None:
                        user = users.get(id__exact = id)
                        privacyDict = privacyDictMany.get(id, None)
                        if privacyDict is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                friendShipStatus = friendShipDict.get(id, None)
                                if friendShipStatus is None:
                                    if friendShipRequests is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    else:
                                        friendShipRequest = friendShipRequests.get(id, None)
                                        if friendShipRequest is None:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: False}})
                                        elif friendShipRequest == "request":
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: "request"}})
                                        else:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: "myRequest"}})
                                elif friendShipStatus is False:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                elif friendShipStatus == "request":
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            privacyToSet.update({id: {requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo}}})
                        else:
                            userPrivacyDict = privacyDict.get(requestUserId, None)
                            if userPrivacyDict is None:
                                if id in friendsIds:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                else:
                                    friendShipStatus = friendShipDict.get(id, None)
                                    if friendShipStatus is None:
                                        if friendShipRequests is None:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: False}})
                                        else:
                                            friendShipRequest = friendShipRequests.get(id, None)
                                            if friendShipRequest is None:
                                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                                friendShipToSet.update({requestUserId: {id: False}})
                                            elif friendShipRequest == "request":
                                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                                friendShipToSet.update({requestUserId: {id: "request"}})
                                            else:
                                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                                friendShipToSet.update({requestUserId: {id: "myRequest"}})
                                    elif friendShipStatus is False:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    elif friendShipStatus == "request":
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    else:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                privacyDict.update({id: {requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo}}})
                            else:
                                viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                                viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                                if viewMainInfo is None or viewTHPSInfo is None:
                                    if id in friendsIds:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    else:
                                        friendShipStatus = friendShipDict.get(id, None)
                                        if friendShipStatus is None:
                                            if friendShipRequests is None:
                                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                                friendShipToSet.update({requestUserId: {id: False}})
                                            else:
                                                friendShipRequest = friendShipRequests.get(id, None)
                                                if friendShipRequest is None:
                                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                                    friendShipToSet.update({requestUserId: {id: False}})
                                                elif friendShipRequest == "request":
                                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                                    friendShipToSet.update({requestUserId: {id: "request"}})
                                                else:
                                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                                    friendShipToSet.update({requestUserId: {id: "myRequest"}})
                                        elif friendShipStatus is False:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        elif friendShipStatus == "request":
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        else:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    if viewMainInfo is None:
                                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                        userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                                    if viewTHPSInfo is None:
                                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                        userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                                    privacyToSet.update({id: privacyDict})
                        if primaryName is None and subName is None:
                            if viewMainInfo:
                                firstName = user.first_name or None
                                lastName = user.last_name or None
                                if firstName and lastName:
                                    name = '{0} {1}'.format(firstName, lastName)
                                    smallName = user.username
                                    mainThumb.update({'name': name, 'smallName': smallName})
                                    namesResult.update({id: [name, smallName]})
                                else:
                                    if firstName:
                                        name = firstName
                                        smallName = user.username
                                        mainThumb.update({'name': name, 'smallName': smallName})
                                        namesResult.update({id: [name, smallName]})
                                    else:
                                        name = user.username
                                        mainThumb.update({'name': name, 'smallName': ''})
                                        namesResult.update({id: [name, '']})
                            else:
                                name = user.username
                                mainThumb.update({'name': name, 'smallName': ''})
                                namesResult.update({id: [name, '']})
                        else:
                            if primaryName is None:
                                if viewMainInfo:
                                    firstName = user.first_name or None
                                    lastName = user.last_name or None
                                    if firstName and lastName:
                                        name = '{0} {1}'.format(firstName, lastName)
                                    else:
                                        if firstName:
                                            name = firstName
                                        else:
                                            name = user.username
                                else:
                                    name = user.username
                                mainThumb.update({'name': name})
                                namesResult.update({id: [name, subName]})
                            else:
                                if viewMainInfo:
                                    firstName = user.first_name or None
                                    if firstName:
                                        smallName = user.username
                                    else:
                                        smallName = ''
                                else:
                                    smallName = ''
                                mainThumb.update({'smallName': smallName})
                                namesResult.update({id: [primaryName, smallName]})
                        mainThumbItemsToSet.update({id: mainThumbsDict})
                    else:
                        namesResult.update({id: [primaryName, subName]})
        smallThumbsDict = smallDialogThumbsMany.get(id, None)
        if smallThumbsDict is None:
            if user is None:
                user = users.get(id__exact = id)
            if viewMainInfo is None:
                privacyDict = privacyDictMany.get(id, None)
                if privacyDict is None:
                    if id in friendsIds:
                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                    else:
                        friendShipStatus = friendShipDict.get(id, None)
                        if friendShipStatus is None:
                            if friendShipRequests is None:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                friendShipToSet.update({requestUserId: {id: False}})
                            else:
                                friendShipRequest = friendShipRequests.get(id, None)
                                if friendShipRequest is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                elif friendShipRequest == "request":
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: "request"}})
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: "myRequest"}})
                        elif friendShipStatus is False:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        elif friendShipStatus == "request":
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    privacyToSet.update({id: {requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar}}})
                else:
                    userPrivacyDict = privacyDict.get(requestUserId, None)
                    if userPrivacyDict is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            friendShipStatus = friendShipDict.get(id, None)
                            if friendShipStatus is None:
                                if friendShipRequests is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                else:
                                    friendShipRequest = friendShipRequests.get(id, None)
                                    if friendShipRequest is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    elif friendShipRequest == "request":
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "request"}})
                                    else:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "myRequest"}})
                            elif friendShipStatus is False:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            elif friendShipStatus == "request":
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        privacyDict.update({requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar}})
                        privacyToSet.update({id: privacyDict})
                    else:
                        viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                        viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                        viewAvatar = userPrivacyDict.get('viewAvatar', None)
                        if viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                friendShipStatus = friendShipDict.get(id, None)
                                if friendShipStatus is None:
                                    if friendShipRequests is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    else:
                                        friendShipRequest = friendShipRequests.get(id, None)
                                        if friendShipRequest is None:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: False}})
                                        elif friendShipRequest == "request":
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: "request"}})
                                        else:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: "myRequest"}})
                                elif friendShipStatus is False:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                elif friendShipStatus == "request":
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            if viewMainInfo is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                            if viewTHPSInfo is None:
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                            if viewAvatar is None:
                                viewAvatar = privacyChecker.checkAvatarPrivacy()
                                userPrivacyDict.update({'viewAvatar': viewAvatar})
                            privacyToSet.update({id: privacyDict})
            if viewMainInfo:
                if name is None:
                    firstName = user.first_name or None
                    lastName = user.last_name or None
                    if firstName is not None and lastName is not None:
                        name = '{0} {1}'.format(firstName, lastName)
                        smallName = user.username
                    else:
                        if firstName is not None:
                            name = firstName
                            smallName = user.username
                        else:
                            name = user.username
                            smallName = ''
            else:
                if name is None:
                    name = user.username
            if viewAvatar:
                avatar = user.avatarThumbSmaller or None
                if avatar is not None:
                    if smallName:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><p><a href = "{1}">{3}</a></p><small>{4}</small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, smallName)
                    else:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><p><a href = "{1}">{3}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                else:
                    if smallName:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2} [{3}]</p></div></a><p><a href = "{1}">{2}</a></p><small></small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name, smallName)
                    else:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2}</p></div></a><p><a href = "{1}">{2}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
            else:
                if smallName:
                    listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2} [{3}]</p></div></a><p><a href = "{1}">{2}</a></p><small></small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name, smallName)
                else:
                    listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2}</p></div></a><p><a href = "{1}">{2}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
            smallThumbsResult.update({id: listThumbItem})
            smallThumbItemsToSet.update({id: {requestUserId: listThumbItem}})
        else:
            smallThumb = smallThumbsDict.get(requestUserId, None)
            if smallThumb is None:
                if user is None:
                    user = users.get(id__exact = id)
                if viewMainInfo is None:
                    privacyDict = privacyDictMany.get(id, None)
                    if privacyDict is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                        else:
                            friendShipStatus = friendShipDict.get(id, None)
                            if friendShipStatus is None:
                                if friendShipRequests is None:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    friendShipToSet.update({requestUserId: {id: False}})
                                else:
                                    friendShipRequest = friendShipRequests.get(id, None)
                                    if friendShipRequest is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    elif friendShipRequest == "request":
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "request"}})
                                    else:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: "myRequest"}})
                            elif friendShipStatus is False:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            elif friendShipStatus == "request":
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        privacyToSet.update({id: {requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar}}})
                    else:
                        userPrivacyDict = privacyDict.get(requestUserId, None)
                        if userPrivacyDict is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                            else:
                                friendShipStatus = friendShipDict.get(id, None)
                                if friendShipStatus is None:
                                    if friendShipRequests is None:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                        friendShipToSet.update({requestUserId: {id: False}})
                                    else:
                                        friendShipRequest = friendShipRequests.get(id, None)
                                        if friendShipRequest is None:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: False}})
                                        elif friendShipRequest == "request":
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: "request"}})
                                        else:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: "myRequest"}})
                                elif friendShipStatus is False:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                elif friendShipStatus == "request":
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            privacyDict.update({requestUserId: {'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar}})
                            privacyToSet.update({id: privacyDict})
                        else:
                            viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                            viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                            viewAvatar = userPrivacyDict.get('viewAvatar', None)
                            if viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None:
                                if id in friendsIds:
                                    privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                else:
                                    friendShipStatus = friendShipDict.get(id, None)
                                    if friendShipStatus is None:
                                        if friendShipRequests is None:
                                            privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                            friendShipToSet.update({requestUserId: {id: False}})
                                        else:
                                            friendShipRequest = friendShipRequests.get(id, None)
                                            if friendShipRequest is None:
                                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                                friendShipToSet.update({requestUserId: {id: False}})
                                            elif friendShipRequest == "request":
                                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                                friendShipToSet.update({requestUserId: {id: "request"}})
                                            else:
                                                privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                                friendShipToSet.update({requestUserId: {id: "myRequest"}})
                                    elif friendShipStatus is False:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                    elif friendShipStatus == "request":
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = True, isAuthenticated = True)
                                    else:
                                        privacyChecker = checkPrivacy(privacy = privacySettings.get(user__exact = user), isFriend = False, isAuthenticated = True)
                                if viewMainInfo is None:
                                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                    userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                                if viewTHPSInfo is None:
                                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                    userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                                if viewAvatar is None:
                                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                                    userPrivacyDict.update({'viewAvatar': viewAvatar})
                                privacyToSet.update({id: privacyDict})
                if viewMainInfo:
                    if name is None:
                        firstName = user.first_name or None
                        lastName = user.last_name or None
                        if firstName is not None and lastName is not None:
                            name = '{0} {1}'.format(firstName, lastName)
                            smallName = user.username
                        else:
                            if firstName is not None:
                                name = firstName
                                smallName = user.username
                            else:
                                name = user.username
                                smallName = ''
                else:
                    if name is None:
                        name = user.username
                    if smallName is None:
                        smallName = ''
                if viewAvatar:
                    avatar = user.avatarThumbSmaller or None
                    if avatar is not None:
                        if smallName:
                            listThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><p><a href = "{1}">{3}</a></p><small>{4}</small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name, smallName)
                        else:
                            listThumbItem = '<div class = "user-thumb" value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><p><a href = "{1}">{3}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), avatar.url, name)
                    else:
                        if smallName:
                            listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2} [{3}]</p></div></a><p><a href = "{1}">{2}</a></p><small></small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name, smallName)
                        else:
                            listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2}</p></div></a><p><a href = "{1}">{2}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                else:
                    if smallName:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2} [{3}]</p></div></a><p><a href = "{1}">{2}</a></p><small></small></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name, smallName)
                    else:
                        listThumbItem = '<div class = "user-thumb" value = "{0}"><a href = "{1}"><div class = "no-avatar"><p>{2}</p></div></a><p><a href = "{1}">{2}</a></p></div>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                smallThumbsResult.update({id: listThumbItem})
                smallThumbsDict.update({requestUserId: listThumbItem})
                smallThumbItemsToSet.update({id: smallThumbsDict})
            else:
                smallThumbsResult.update({id: smallThumb})
    if len(thumbsSurplus) > 0:
        if len(mainThumbsSurplus) > 0 or len(namesSurplus) > 0:
            mainDialogThumbsCache.set_many(mainThumbItemsToSet)
        if len(smallThumbsSurplus) > 0:
            smallDialogThumbsCache.set_many(smallThumbItemsToSet)
        if len(privacySurplus) > 0:
            privacyCache.set_many(privacyToSet)
            if len(friendShipSurplus) > 0:
                friendShipCache.set_many(friendShipToSet)

    return mainThumbsResult, smallThumbsResult, namesResult, onlineStatusDict, activeStatusDict