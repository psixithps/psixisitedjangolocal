from django.contrib import admin
from psixiSocial.models import mainUserProfile, THPSProfile, personalMessage, dialogMessage, userProfilePrivacy, groupMessage, groupDialogAttributes

admin.site.register(mainUserProfile)
admin.site.register(userProfilePrivacy)
admin.site.register(THPSProfile)
admin.site.register(dialogMessage)
admin.site.register(personalMessage)
admin.site.register(groupDialogAttributes)
admin.site.register(groupMessage)
