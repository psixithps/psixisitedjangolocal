from django.db import models
import datetime, uuid
from django.utils import timezone
from libraryTHPS.models import clansMain, playersShip
from django.contrib.auth.models import AbstractUser
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill

def getPathAvatar(self, image):
    return 'psixiSocial/%d/avatars/%s/' % (self.id, image)

def getPathBackground(self, image):
    return 'psixiSocial/%d/backgrounds/%s/' % (self.id, image)

class mainUserProfile(AbstractUser):
    ''' Основные поля от родного User тут наследуются '''
    profileUrl = models.SlugField(max_length = 150, blank = False, db_index = True)
    country = models.ForeignKey('cities.Country', related_name = 'cities.Country+', blank = True, on_delete = models.PROTECT, null = True)
    region = models.ForeignKey('cities.Region', related_name = 'cities.region+', blank = True, on_delete = models.PROTECT, null = True)
    subRegion = models.ForeignKey('cities.Subregion', related_name = 'cities.Subregion+', blank = True, on_delete = models.PROTECT, null = True)
    city = models.ForeignKey('cities.City', related_name = 'cities.City+', blank = True, on_delete = models.PROTECT, null = True)
    birthDate = models.DateField(blank = True, null = True)
    avatar = models.ImageField(blank = True, upload_to = getPathAvatar)
    avatarProfileThumb = ImageSpecField(source = 'avatar', processors = [ResizeToFill(250, 300)], format = 'PNG', options = {'quality': 85})
    avatarThumbMiddle = ImageSpecField(source = 'avatar', processors = [ResizeToFill(100, 100)], format = 'PNG', options = {'quality': 60})
    avatarThumbMiddleSmall = ImageSpecField(source = 'avatar', processors = [ResizeToFill(75, 75)], format = 'PNG', options = {'quality': 75})
    avatarThumbSmall = ImageSpecField(source = 'avatar', processors = [ResizeToFill(50, 50)], format = 'PNG', options = {'quality': 40})
    avatarThumbSmaller = ImageSpecField(source = 'avatar', processors = [ResizeToFill(35, 35)], format = 'PNG', options = {'quality': 30})
    background = models.ImageField(blank = True, upload_to = getPathBackground)
    signature = models.CharField(blank = True, max_length = 300, default = "")
    registrationDate = models.DateField(auto_now_add = True)
    friends = models.ManyToManyField("self", related_name = 'mainUserProfile.id+', blank = True)
    class Meta:
        swappable = 'AUTH_USER_MODEL' # Это чтобы для не родного (абстрактного) объекта User не создавалась своя миграция
        permissions = (
            ("can_seeBlocked", "Может видеть заблокированных пользователей"),
        )

    def __str__(self):
        return self.username

class userProfilePrivacy(models.Model):
    user = models.OneToOneField(mainUserProfile, related_name = "mainUserProfile.id+", on_delete = models.CASCADE, primary_key = True)
    viewProfile = models.PositiveSmallIntegerField(default = 1) # Видимость профиля в списках
    viewProfileMainInfo = models.PositiveSmallIntegerField(default = 2) # Данные профиля, имя и фамилия
    viewProfileTHPSInfo = models.PositiveSmallIntegerField(default = 0) # Показ thpsраздела в профиле
    viewTHPSUsersFriends = models.PositiveSmallIntegerField(default = 0)
    viewTHPSUsersFriendsOnline = models.PositiveSmallIntegerField(default = 0)
    viewProfileFriends = models.PositiveSmallIntegerField(default = 0)
    viewFriendsOnline = models.PositiveSmallIntegerField(default = 0)
    viewProfileAvatar = models.PositiveSmallIntegerField(default = 1)
    viewProfileBackground = models.PositiveSmallIntegerField(default = 1)
    viewProfileOnlineStatus = models.PositiveSmallIntegerField(default = 0)
    viewProfileActiveStatus = models.PositiveSmallIntegerField(default = 0)
    viewProfileWall = models.PositiveSmallIntegerField(default = 2)
    sendProfileWall = models.PositiveSmallIntegerField(default = 2)
    viewProfileWallExceptional = models.ManyToManyField("self", blank = True) # Только если поле viewProfileWall == 3
    sendProfileWallExceptional = models.ManyToManyField("self", blank = True) # Только если поле sendProfileWall == 3
    sendPersonalMessages = models.PositiveSmallIntegerField(default = 1)
    sendPersonalMessagesExceptional = models.ManyToManyField("self", blank = True) # Только если поле выше = 3
    sendFriendRequests = models.PositiveSmallIntegerField(default = 1)

    def __str__(self):
        return self.user.username

def getPathTHPSAvatar(self, image):
    return 'psixiSocial/%d/thps_avatars/%s/' % (self.id, image)

class THPSProfile(models.Model):
    toUser = models.OneToOneField(mainUserProfile, on_delete = models.CASCADE, primary_key = True)
    avatar = models.ImageField(blank = True, upload_to = getPathTHPSAvatar)
    avatarThumbMiddle = ImageSpecField(source = 'avatar', processors = [ResizeToFill(100, 100)], format = 'PNG', options = {'quality': 60})
    clan = models.OneToOneField(clansMain, blank = True, on_delete = models.SET_NULL, null = True)
    player = models.OneToOneField(playersShip, blank = True, on_delete = models.SET_NULL, null = True)
    dateJoinToTHPS = models.DateField(blank = True, default = timezone.now)
    dateJoinToClan = models.DateField(blank = True, default = timezone.now)
    nickName = models.CharField(max_length = 25, blank = False)

    def __str__(self):
        return self.nickName

class THPSProfileExNickNames(models.Model):
    toUserTHPSProfile = models.ForeignKey(THPSProfile, on_delete = models.CASCADE)
    dateToGetNick = models.DateField(blank = True, default = timezone.now)
    dateToDropNick = models.DateField(blank = False, default = timezone.now)

class THPSProfileExClans(models.Model):
    toUserTHPSProfile = models.ForeignKey(THPSProfile, on_delete = models.CASCADE)
    clanName = models.CharField(max_length = 20, blank = True)
    dateJoinToClan = models.DateField(blank = True, default = timezone.now)
    dateLeaveFromClan = models.DateField(blank = False, default = timezone.now)

class groupDialogAttributes(models.Model):
    dialog = models.OneToOneField("dialogMessage", related_name = "dialogMessage.id+", primary_key = True, on_delete = models.CASCADE)
    name = models.CharField(max_length = 20)
    slug = models.SlugField(max_length = 20, db_index = True)
    image = models.ImageField(blank = True)
    dialogsListThumb = ImageSpecField(source = "image", processors = [ResizeToFill(75, 75)], format = "PNG", options = {'quality': 75})
    dialogsMinimizedThumb = ImageSpecField(source = "image", processors = [ResizeToFill(50, 50)], format = "PNG", options = {'quality': 40})
    owners = models.ForeignKey(mainUserProfile, related_name = "mainUserProfile.id+", null = True, on_delete = models.SET_NULL)
    membersCanInvite = models.BooleanField(default = False) # Могут ли участники приглашать людей
    lastUpdate = models.DateTimeField()

class dialogMessage(models.Model):
    id = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False)
    users = models.ManyToManyField(mainUserProfile, db_index = True)
    type = models.BooleanField(blank = False, default = True) # True - personal, False - Group
    attrs = models.OneToOneField(groupDialogAttributes, related_name = "groupDialogAttributes.id+", blank = True, null = True, default = None, on_delete = models.CASCADE)

class personalMessage(models.Model):
    id = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False)
    by = models.ForeignKey(mainUserProfile, related_name = 'mainUserProfile.id+', blank = False, on_delete = models.CASCADE)
    to = models.ForeignKey(mainUserProfile, related_name = 'mainUserProfile.id+', blank = False, on_delete = models.CASCADE)
    body = models.CharField(max_length = 2000, blank = True)
    time = models.DateTimeField()
    messageHead = models.BooleanField(blank = False, default = False) # Отображение информации об отправителе, если предыдущее сообщение не от этого же отправителя
    inDialog = models.ForeignKey(dialogMessage, blank = False, on_delete = models.CASCADE, db_index = True)
    imagesAttachment = models.OneToOneField('imagesAttachmentMessagePersonal', on_delete = models.CASCADE, null = True, default = None)

    def __str__(self):
        return str(self.id)

class groupMessage(models.Model):
    id = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False)
    by = models.ForeignKey(mainUserProfile, related_name = 'mainUserProfile.id+', blank = False, on_delete = models.CASCADE)
    members = models.ManyToManyField(mainUserProfile, related_name = "mainUserProfile.id+", db_index = True)
    body = models.CharField(max_length = 2000, blank = True)
    time = models.DateTimeField()
    messageHead = models.BooleanField(blank = False, default = False) # Отображение информации об отправителе, если предыдущее сообщение не от этого же отправителя
    inDialog = models.ForeignKey(dialogMessage, blank = False, on_delete = models.CASCADE, db_index = True)
    imagesAttachment = models.OneToOneField('imagesAttachmentMessageGroup', on_delete = models.CASCADE, blank = True, null = True, default = None)

    def __str__(self):
        return str(self.id)

def createLinkImages(instance, filename):
    time = instance.message.time
    return 'psixiSocial/personalDialog/images/%d/%d/%d/%d/%s' % (instance.by_id, time.year, time.month, time.day, filename)

class imagesAttachmentMessagePersonal(models.Model):
    message = models.OneToOneField(personalMessage, primary_key = True, editable = False, on_delete = models.CASCADE)
    by = models.ForeignKey(mainUserProfile, related_name = 'mainUserProfile.id+', blank = False, on_delete = models.CASCADE, db_index = True)
    image1 = models.FileField(blank = True, upload_to = createLinkImages)
    image2 = models.FileField(blank = True, upload_to = createLinkImages)
    image3 = models.FileField(blank = True, upload_to = createLinkImages)
    image4 = models.FileField(blank = True, upload_to = createLinkImages)
    image5 = models.FileField(blank = True, upload_to = createLinkImages)
    image6 = models.FileField(blank = True, upload_to = createLinkImages)
    itemsLength = models.SmallIntegerField(blank = False)

class imagesAttachmentMessageGroup(models.Model):
    message = models.OneToOneField(groupMessage, primary_key = True, editable = False, on_delete = models.CASCADE)
    by = models.ForeignKey(mainUserProfile, related_name = 'mainUserProfile.id+', blank = False, on_delete = models.CASCADE, db_index = True)
    image1 = models.FileField(blank = True, upload_to = createLinkImages)
    image2 = models.FileField(blank = True, upload_to = createLinkImages)
    image3 = models.FileField(blank = True, upload_to = createLinkImages)
    image4 = models.FileField(blank = True, upload_to = createLinkImages)
    image5 = models.FileField(blank = True, upload_to = createLinkImages)
    image6 = models.FileField(blank = True, upload_to = createLinkImages)
    itemsLength = models.SmallIntegerField(blank = False)

class userUpdatesHistory(models.Model):
    id = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False)
    owner = models.ForeignKey(mainUserProfile, related_name = 'mainUserProfile.id+', on_delete = models.CASCADE)
    destination = models.ForeignKey(mainUserProfile, related_name = 'mainUserProfile.id+', on_delete = models.CASCADE)
    descriptionText = models.CharField(max_length = 300, blank = True)
    name = models.CharField(max_length = 30, blank = False, default = "")
    state = models.CharField(max_length = 30, blank = False, default = "")
    time = models.DateTimeField(auto_now_add = True)