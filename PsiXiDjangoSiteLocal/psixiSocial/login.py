import json
from functools import lru_cache
from django.contrib.auth import authenticate, login as login_method, logout
from psixiSocial.forms import loginForm
from psixiSocial.models import mainUserProfile
from django.shortcuts import redirect, render
from django.urls import reverse
from django.http import JsonResponse
from django.template.loader import render_to_string as rts
from collections import OrderedDict
from base.helpers import checkCSRF

class cachedMenu():
    @property
    @lru_cache(maxsize = 16)
    def menu(self):
        menu = OrderedDict({
            0: {
                "textname": "Гость",
                0: {
                    "textname": "Вход",
                    },
                1: {
                    "textname": "Регистрация",
                    "url": reverse("registration"),
                    }
                }
            })
        return menu

    @property
    @lru_cache(maxsize = 16)
    def menuSerialized(self):
        menu = self.menu
        if isinstance(menu, OrderedDict):
            return json.dumps(menu)
        return json.dumps(menu())

    def menuGetter(self, methodName):
        methodOrCached = getattr(self, methodName)
        if isinstance(methodOrCached, OrderedDict):
            return methodOrCached
        if isinstance(methodOrCached, str):
            return methodOrCached
        return methodOrCached()

def login(request):
    if request.method == "GET":
        if request.user.is_authenticated is False:
            menu = cachedMenu()
            if request.is_ajax():
                if "global" in request.GET:
                    return JsonResponse({"newPage": json.dumps({
                        "newTemplate": rts("psixiSocial/psixiSocial.html", {
                            'parentTemplate': 'base/classicPage/emptyParent.html',
                            'content': ('psixiSocial', 'login'),
                            'menu': menu.menuGetter('menuSerialized')
                            }),
                        'url': request.path
                        })})
                if "lightbox" in request.GET:
                    return JsonResponse({})
                return JsonResponse({"newPage": json.dumps({
                    'templateHead': rts("psixiSocial/head/edit.html"),
                    'templateBody': rts("psixiSocial/body/login.html", {
                        'content': ('psixiSocial', 'login'),
                        'form': loginForm(),
                        'csrf': ''
                        }),
                    'templateScripts': rts("psixiSocial/scripts/edit.html"),
                    'content': ('psixiSocial', 'login'),
                    'menu': menu.menuGetter('menu'),
                    'url' : request.path,
                    'titl': 'Вход'
                    })})
            return render(request, "psixiSocial/psixiSocial.html", {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'login'),
                'menu': menu.menuGetter('menuSerialized'),
                'form': loginForm(),
                'csrf': checkCSRF(request, request.COOKIES),
                'titl': ''
                })
    elif request.method == "POST":
        if request.user.is_authenticated is False:
            userName = request.POST["username"]
            passWord = request.POST["password"]
            try:
                findUser = mainUserProfile.objects.get(username__iexact = userName)
            except mainUserProfile.DoesNotExist:
                findUser = None
            if findUser is not None:
                if mainUserProfile.check_password(findUser, passWord) == True:
                    auth = authenticate(username = userName, password = passWord)
                else:
                    auth = False
            else:
                auth = None
            form = loginForm(request.POST, auth = auth)
            if form.is_valid():
                user = login_method(request, auth)
                if "next" in request.GET:
                    next = request.GET["next"]
                else:
                    next = reverse("profile", kwargs = {'currentUser': 'im'})
                if request.is_ajax():
                    return JsonResponse({"fullRedirect": json.dumps(next)})
                return redirect(next)
            if request.is_ajax():
                return JsonResponse({"newPage": json.dumps({
                    'templateBody': rts("psixiSocial/body/login.html", {
                        'content': ('psixiSocial', 'login'),
                        'form': form,
                        'csrf': ''
                        }),
                    'content': ('psixiSocial', 'login')
                    })})
            return render(request, "psixiSocial/psixiSocial.html", {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'login'),
                'form': form,
                'csrf': checkCSRF(request, request.COOKIES),
                'titl': '',
                })

def login_decorator(func):
    def decor(request, *args, **kwargs):
        if request.user.is_authenticated is False:
            menu = cachedMenu()
            if request.is_ajax():
                if "global" in request.GET:
                    return JsonResponse({"newPage": json.dumps({
                    "newTemplate": rts("psixiSocial/psixiSocial.html", {
                        'parentTemplate': 'base/classicPage/emptyParent.html',
                        'content': ('psixiSocial', 'login'),
                        'menu': menu.menuGetter('menuSerialized'),
                        'form': loginForm(),
                        'csrf': ''
                        }),
                    'url': "%s?next=%s" % (reverse("login"), request.path)
                    })})
                return JsonResponse({"newPage": json.dumps({
                    'templateHead': rts("psixiSocial/head/edit.html"),
                    'templateBody': rts("psixiSocial/body/login.html", {
                        'content': ('psixiSocial', 'login'),
                        'form': loginForm(),
                        'csrf': ''
                        }),
                    'content': ('psixiSocial', 'login'),
                    'menu': menu.menuGetter('menu'),
                    'url' : '%s?next=%s' % (reverse("login"), request.path),
                    'titl': 'Вход'
                    })})
            return redirect("%s?next=%s" % (reverse("login"), request.path))
        return func(request, *args, **kwargs)
    decor.__doc__ = func.__doc__
    decor.__name__ = func.__name__
    return decor

def logOut(request):
    user = request.user
    if user.is_authenticated:
        logout(request)
        if request.is_ajax():
            return JsonResponse({"newPage": json.dumps({
                'templateHead': rts("psixiSocial/head/edit.html"),
                'templateBody': rts("psixiSocial/body/login.html", {
                    'content': ('psixiSocial', 'login'),
                    'form': loginForm(),
                    'csrf': ''
                    }),
                'templateScripts': rts("psixiSocial/scripts/edit.html"),
                'content': ('psixiSocial', 'login'),
                'menu': cachedMenu().menuGetter('menu'),
                'url' : request.path,
                'titl': 'Вход'
                })})
        return render(request, "psixiSocial/psixiSocial.html", {
            'parentTemplate': 'base/classicPage/base.html',
            'content': ('psixiSocial', 'login'),
            'menu': cachedMenu().menuGetter('menuSerialized'),
            'form': loginForm(),
            'csrf': checkCSRF(request, request.COOKIES),
            'titl': ''
            })