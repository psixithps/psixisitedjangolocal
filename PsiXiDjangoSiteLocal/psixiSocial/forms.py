from django import forms
from psixiSocial.models import mainUserProfile, THPSProfile, personalMessage, userProfilePrivacy
from cities.models import Country, Region, Subregion, City
from django.contrib.auth import authenticate
from django.utils.text import slugify
from django.core.validators import FileExtensionValidator, validate_image_file_extension


privacyLevels = (
    (8, 'Только я'),
    (7, 'Все, кроме следующих пользователей'),
    (6, 'Только следующие пользователи'),
    (5, 'Соклановцы'),
    (4, 'Члены следующих кланов'),
    (3, 'Друзья и друзья друзей'),
    (2, 'Друзья'),
    (1, 'Пользователи'),
    (0, 'Гости')
    )

sendFriendRequestsPrivacyLevels = (
    (1, 'Все пользователи'),
    (3, 'Только следующие пользователи'),
    (4, 'Все пользователи, кроме следующих'),
    (7, 'Никто')
    )

class loginForm(forms.Form):
    username = forms.CharField(required = True, max_length = 150, label = "Логин", widget = forms.TextInput)
    password = forms.CharField(required = True, max_length = 30, label = "Пароль", widget = forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        self.auth = kwargs.pop("auth", None)
        super(loginForm, self).__init__(*args, **kwargs)

    def clean_username(self):
        if self.auth is None:
            raise forms.ValidationError("Пользователь под логином %s не был зарегистрирован" % self.data["username"], code = "invalid")
        return self.cleaned_data

    def clean_password(self):
        if self.auth == False:
            raise forms.ValidationError("Недействительный пароль", code = "invalid")
        return self.cleaned_data

class registrationForm(forms.Form):
    username = forms.CharField(required = True, max_length = 150, label = "Логин", widget = forms.TextInput)
    email = forms.CharField(required = True, max_length = 30, label = "Адрес электронной почты", widget = forms.EmailInput)
    password = forms.CharField(required = True, label = "Повторение пароля", widget = forms.PasswordInput)
    passwordRepeat = forms.CharField(required = True, label = "Повторение пароля", widget = forms.PasswordInput)

    def clean_username(self):
        self.username = self.cleaned_data.get("username")
        userOnUsername = mainUserProfile.objects.filter(username__in = self.username)
        if userOnUsername.count():
            raise forms.ValidationError("Пользователь с таким логином уже существует", code = "invalid")
        return self.username

    def clean_email(self):
        self.email = self.cleaned_data.get("email")
        userOnEmail = mainUserProfile.objects.filter(email = self.email)

        if userOnEmail.count():
            raise forms.ValidationError("Указанная электронная почта занята другим пользователем", code = "invalid")
        return self.email

    def clean(self):
        if self.cleaned_data.get("password") != self.cleaned_data.get("passwordRepeat"):
            raise forms.ValidationError("Пароли должны совпадать", code = "invalid")
        return self.cleaned_data

class registrationCheckNickAndClanForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.choises = kwargs.pop('choices')
        self.initData = kwargs.get('initial', {})
        self.mail = self.initData.get('email', None)
        self.pas = self.initData.get('password', None)
        super(registrationCheckNickAndClanForm, self).__init__(*args, **kwargs)
        if len(self.choises) < 3:
            self.fields['nick'].choices = self.choises
            del self.fields['clan']
        else:
            self.fields['nick'].choices = self.choises
            self.fields['clan'].choices = self.choises
        if self.mail:
            self.fields['email'].widget = forms.HiddenInput(attrs = {})
        if self.pas:
            self.fields['password'].widget = forms.HiddenInput(attrs = {})
            self.fields['passwordRepeat'].widget = forms.HiddenInput(attrs = {})
        if self.data:
            self.itsName = self.data.get("itsUsername", None)
            if self.itsName is None or self.itsName is False:
                self.data["username"] = self.choises[int(self.data["nick"])][1]

    username = forms.CharField(required = True, max_length = 30, label = "Логин", widget = forms.HiddenInput)
    email = forms.CharField(required = True, max_length = 30, label = "Адрес электронной почты", widget = forms.EmailInput)
    password = forms.CharField(required = True, label = "Повторение пароля", widget = forms.PasswordInput)
    passwordRepeat = forms.CharField(required = True, label = "Повторение пароля", widget = forms.PasswordInput)
    itsUsername = forms.BooleanField(required = False, label = 'Всё нормально', help_text = 'Всё это только логин', widget = forms.CheckboxInput)
    nick = forms.ChoiceField(required = True, label = 'Укажите ник', widget = forms.RadioSelect)
    clan = forms.ChoiceField(required = True, label = 'Укажите клан', widget = forms.RadioSelect)

    def clean_username(self):
        self.username = self.cleaned_data["username"]
        userOnUsername = mainUserProfile.objects.filter(username__in = self.username)
        if userOnUsername.count():
            raise forms.ValidationError("Пользователь с таким логином уже существует", code = "invalid")
        return self.username

    def clean_email(self):
        if self.mail is None:
            self.email = self.cleaned_data["email"]
            userOnEmail = mainUserProfile.objects.filter(email = self.email)
            if userOnEmail.count():
                raise forms.ValidationError("Указанная электронная почта занята другим пользователем", code = "invalid")
        return self.email

    def clean(self):
        if self.pas is None:
            if self.cleaned_data["password"] != self.cleaned_data["passwordRepeat"]:
                raise forms.ValidationError("Пароли должны совпадать", code = "invalid")

        return self.cleaned_data

class userEditDetalForm(forms.ModelForm):
    class Meta:
        model = mainUserProfile
        fields = ('first_name', 'last_name', 'country', 'region', 'subRegion', 'city', 'birthDate', 'avatar',)

    first_name = forms.CharField(required = False, max_length = 30, label = 'Имя')
    last_name = forms.CharField(required = False, max_length = 150, label = 'Фамилия')
    country = forms.CharField(required = False)
    region = forms.CharField(required = False)
    subRegion = forms.CharField(required = False)
    city = forms.CharField(required = False)
    birthDate = forms.DateField(required = False, label = 'Дата рождения')
    avatar = forms.ImageField(required = False, label = 'Аватар')
    confirmPassword = forms.CharField(required = True, max_length = 30, label = 'Пароль - подтверждение', widget = forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        self.thisUser = kwargs.pop("user", None)
        super(userEditDetalForm, self).__init__(*args, **kwargs)

    def clean_country(self):
        self.country = self.cleaned_data.get("country")
        if self.country:
            try:
                self.country = Country.objects.get(name = self.country)
            except Country.DoesNotExist:
                raise forms.ValidationError("Укажите корректное название государства", code = "invalid")
        else:
            self.country = None
        return self.country

    def clean_region(self):
        self.region = self.cleaned_data.get("region")
        if self.region:
            try:
                self.region = Region.objects.get(name = self.region)
            except Region.DoesNotExist:
                raise forms.ValidationError("Укажите корректное название округа", code = "invalid")
        else:
            self.region = None
        return self.region

    def clean_subRegion(self):
        self.subRegion = self.cleaned_data.get("subRegion")
        if self.subRegion:
            try:
                self.subRegion = Subregion.objects.get(name = self.subRegion)
            except Subregion.DoesNotExist:
                raise forms.ValidationError("Укажите корректное название области", code = "invalid")
        else:
            self.subRegion = None
        return self.subRegion

    def clean_city(self):
        self.city = self.cleaned_data.get("city")
        if self.city:
            try:
                self.city = City.objects.get(name = self.city)
            except City.DoesNotExist:
                raise forms.ValidationError("Укажите корректное название города", code = "invalid")
        else:
            self.city = None
        return self.city

    def clean_confirmPassword(self):
        self.confirmPassword = self.cleaned_data.get("confirmPassword", None)
        if self.confirmPassword is not None:
            if self.thisUser.check_password(self.confirmPassword) is False:
                raise forms.ValidationError("Неправильный пароль", code = "invalid")
        return self.confirmPassword

class userEditMainForm(forms.ModelForm):
    class Meta:
        model = mainUserProfile
        fields = ('profileUrl', 'username')

    confirmPassword = forms.CharField(required = True, max_length = 30, label = "Пароль - подтверждение", widget = forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None) 
        self.initData = kwargs.get('initial', {})
        super(userEditMainForm, self).__init__(*args, **kwargs)
        if 'username' in self.changed_data:
            self.changedUsername = True
            if 'profileUrl' in self.changed_data:
                self.url = self.data.get("profileUrl", "")
            else:
                self.url = self.data.get("username", "")
        else:
            self.changedUsername = False
            if 'profileUrl' in self.changed_data:
                self.url = self.data.get("profileUrl")
            else:
                self.url = self.initData["profileUrl"]

    def clean_username(self):
        self.username = self.cleaned_data.get("username")
        if self.changedUsername == True:
            userOnUsername = mainUserProfile.objects.filter(username = self.username)
            if userOnUsername.count():
                raise forms.ValidationError("Пользователь с таким логином уже существует", code = "invalid")
        return self.username

    def clean_profileUrl(self):
        self.profileUrl =  slugify(self.url)
        return self.profileUrl

    def clean_confirmPassword(self):
        self.confirmPassword = self.cleaned_data.get("confirmPassword", None)
        if self.confirmPassword is not None:
            if self.user.check_password(self.confirmPassword) == False:
                raise forms.ValidationError("Неправильный пароль", code = "invalid")
        return self.confirmPassword

class userEditContactsForm(forms.ModelForm):
    class Meta:
        model = mainUserProfile
        fields = ('email',)

    confirmPassword = forms.CharField(required = True, max_length = 30, label = "Пароль - подтверждение", widget = forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user", None)
        super(userEditMainForm, self).__init__(*args, **kwargs)

    def clean_email(self):
        self.email = self.cleaned_data.get("email", None)
        if self.email:
            userOnEmail = mainUserProfile.objects.filter(email = self.email).exclude(username = self.user.username)
            if userOnEmail.count():
                raise forms.ValidationError("Указанная электронная почта занята другим пользователем", code = "invalid")
        else:
            raise forms.ValidationError("Обязательное поле", code = "required")
        return self.email

    def clean_confirmPassword(self):
        self.confirmPassword = self.cleaned_data.get("confirmPassword", None)
        if self.confirmPassword is not None:
            if self.user.check_password(self.confirmPassword) == False:
                raise forms.ValidationError("Неправильный пароль", code = "invalid")
        return self.confirmPassword

class userEditPassword(forms.ModelForm):
    class Meta:
        model = mainUserProfile
        fields = ('password',)
        labels = {
            'password': "Текущий пароль",
        }
        widgets = {
            'password': forms.PasswordInput,
        }
    newPassword = forms.CharField(required = True, max_length = 30, label = 'Новый пароль', widget = forms.PasswordInput)
    newPasswordRepeat = forms.CharField(required = True, max_length = 30, label = 'Повторение нового пароля', widget = forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user", None)
        super(userEditPassword, self).__init__(*args, **kwargs)

    def clean_password(self):
        self.password = self.cleaned_data.get("password", None)
        if self.password is not None:
            if self.user.check_password(self.password) == False:
                raise forms.ValidationError("Неправильный пароль", code = "invalid")
        return self.password

    def clean(self):
        if self.cleaned_data.get("newPassword") != self.cleaned_data.get("newPasswordRepeat"):
            raise forms.ValidationError('Новые пароли должны совпадать', code = "invalid")
        if self.cleaned_data.get("password") == self.cleaned_data.get("newPassword"):
            raise forms.ValidationError("Текущий и новый пароли совпадают", code = "invalid")
        return self.cleaned_data

class userEditTHPSForm(forms.ModelForm):
    class Meta:
        model = THPSProfile
        fields = ('toUser', 'nickName', 'dateJoinToTHPS', 'clan', 'dateJoinToClan')
        widgets = {
            'toUser': forms.HiddenInput,
            }
    confirmPassword = forms.CharField(max_length = 30, required = True, widget = forms.PasswordInput)

class editPrivacyProfileForm(forms.ModelForm):
    class Meta:
        model = userProfilePrivacy
        fields = ('viewProfile', 'viewProfileMainInfo', 'viewProfileTHPSInfo', 'viewProfileFriends', 'viewProfileAvatar',
                  'viewProfileBackground', 'viewProfileOnlineStatus', 'viewProfileActiveStatus', 'viewProfileWall',
                  'sendProfileWall', 'sendFriendRequests', 'sendPersonalMessages')
        labels = {
            'viewProfile': 'Профиль виден',
            'viewProfileMainInfo': 'Личные данные профиля',
            'viewProfileTHPSInfo': 'THPS данные профиля',
            'viewProfileFriends': 'Список друзей',
            'viewProfileAvatar': 'Аватар',
            'viewProfileBackground': 'Фон',
            'viewProfileOnlineStatus': 'Онлайн статус',
            'viewProfileActiveStatus': 'Статус активности',
            'viewProfileWall': 'Стена',
            'sendProfileWall': 'Делать записи на стене',
            'sendFriendRequests': 'Запросы на добавление в друзья',
            'sendPersonalMessages': 'Личные сообщения'
        }

    viewProfile = forms.ChoiceField(required = True, choices = privacyLevels)
    viewProfileMainInfo = forms.ChoiceField(required = True, choices = privacyLevels)
    viewProfileTHPSInfo = forms.ChoiceField(required = True, choices = privacyLevels)
    viewProfileFriends = forms.ChoiceField(required = True, choices = privacyLevels)
    viewProfileAvatar = forms.ChoiceField(required = True, choices = privacyLevels)
    viewProfileBackground = forms.ChoiceField(required = True, choices = privacyLevels)
    viewProfileOnlineStatus = forms.ChoiceField(required = True, choices = privacyLevels)
    viewProfileActiveStatus = forms.ChoiceField(required = True, choices = privacyLevels)
    viewProfileWall = forms.ChoiceField(required = True, choices = privacyLevels)
    sendProfileWall = forms.ChoiceField(required = True, choices = privacyLevels)
    sendFriendRequests = forms.ChoiceField(required = True, choices = sendFriendRequestsPrivacyLevels)
    sendPersonalMessages = forms.ChoiceField(required = True, choices = privacyLevels)

class sendMessageForm(forms.Form):
    body = forms.CharField(max_length = 1000, required = False, label = False, widget = forms.Textarea())
    insertion = forms.FileField(required = False, label = ' ', widget = forms.FileInput(attrs = {'rows': 3, 'cols': 10, 'class': 'insertion', 'multiple': True}), validators = [FileExtensionValidator, validate_image_file_extension])

    def clean_body(self):
        text = self.cleaned_data.get("body", None)
        if text is not None:
            if len(text) >= 1000:
                raise forms.ValidationError("Ограничение длины тела сообщения - 1000 символов", code = "invalid")
        return text

    def clean_insertion(self):
        imagesData = self.cleaned_data.get("insertion", None)
        #if imagesData is not None:
            #if len(imagesData) > 6:
                #raise forms.ValidationError("Нельзя загружать больше 6 файлов за раз", code = "invalid")
        return imagesData

    def clean(self):
        data = self.cleaned_data
        body = data.get("body", None)
        if (body is None or len(body) == 0) and data.get("insertion", None) is None:
            raise forms.ValidationError("В сообщении должен быть текст или должны быть выбраны файлы", code = "invalid")
        return data