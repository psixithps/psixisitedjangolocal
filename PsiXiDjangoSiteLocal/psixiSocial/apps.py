from django.apps import AppConfig


class psixiSocialConfig(AppConfig):
    name = 'psixiSocial'
