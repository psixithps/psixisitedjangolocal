import json, re, copy
from functools import lru_cache
from django.db import transaction
from ipware.ip import get_ip
from django.core.cache import caches
from django.utils.text import slugify
from psixiSocial.models import mainUserProfile, THPSProfile, userProfilePrivacy
from libraryTHPS.models import clansMain, playersShip
from django.contrib.auth import authenticate, login
from psixiSocial.forms import registrationForm, registrationCheckNickAndClanForm
from django.shortcuts import render, redirect
from django.template.loader import render_to_string as rts
from django.http import JsonResponse
from collections import OrderedDict
from django.urls import reverse
from base.helpers import checkCSRF

class cachedFragments():

    @property
    @lru_cache(maxsize = 16)
    def menuType1(self):
        menu = OrderedDict({
            0: {
                "textname": "Гость",
                0: {
                    "textname": "Регистрация"
                    },
                1: {
                    "textname": "Вход",
                    "url": reverse("login")
                    }
                }
        })
        return menu

    @property
    @lru_cache(maxsize = 16)
    def menuType1Serialized(self):
        menu = self.menuType1
        if isinstance(menu, OrderedDict):
            return json.dumps(menu)
        return json.dumps(menu())

    @property
    @lru_cache(maxsize = 16)
    def menuType2(self):
        menu = OrderedDict({
            0: {
                "textname": "Гость",
                0: {
                    "textname": "Регистрация",
                    0: {
                        "textname": "Обнаружены дополнительные параметры"
                        }
                    },
                1: {
                    "textname": "Вход",
                    "url": reverse("login")
                    }
                }
            })
        return menu

    @property
    @lru_cache(maxsize = 16)
    def menuType2Serialized(self):
        menu = self.menuType2
        if isinstance(menu, OrderedDict):
            return json.dumps(menu)
        return json.dumps(menu())

    def menuGetter(self, methodName):
        methodOrCached = getattr(self, methodName)
        if isinstance(methodOrCached, OrderedDict):
            return methodOrCached
        if isinstance(methodOrCached, str):
            return methodOrCached
        return methodOrCached()


def profileRegistration(request):
    if request.method == "GET":
        if request.user.is_authenticated is False:
            fragments = cachedFragments()
            if request.is_ajax():
                if "global" in request.GET:
                    return JsonResponse({"newPage": json.dumps({
                        'newTemplate': rts("psixiSocial/psixiSocial.html", {
                            'parentTemplate': 'base/classicPage/emptyParent.html',
                            'content': ('psixiSocial', 'registration'),
                            'menu': fragments.menuGetter('menuType1Serialized'),
                            'form': registrationForm(),
                            'csrf': '',
                            'helpTextDict': OrderedDict([
                                ('h1', ('Регистрация',))
                                ])
                            }),
                        'url': request.path,
                        'titl': ''
                        })})
                return JsonResponse({"newPage": json.dumps({
                    'templateHead': rts("psixiSocial/head/registration.html"),
                    'templateBody': rts("psixiSocial/body/registration.html", {
                        'form': registrationForm(),
                        'csrf': '',
                        'helpTextDict': OrderedDict([
                            ('h1', ('Регистрация',))
                            ])
                        }),
                    'templateScripts': rts("psixiSocial/scripts/edit.html"),
                    'content': ('psixiSocial', 'registration'),
                    'menu': fragments.menuGetter('menuType1'),
                    'url': request.path,
                    'titl': 'Регистрация'
                    })})
            return render(request, "psixiSocial/psixiSocial.html", {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'registration'),
                'menu': fragments.menuGetter('menuType1Serialized'),
                'form': registrationForm(),
                'csrf': checkCSRF(request, request.COOKIES),
                'helpTextDict': OrderedDict([
                    ('h1', ('Регистрация',))
                    ])
                })
        if request.is_ajax():
            return JsonResponse({
                "redirectTo": json.dumps(reverse("profile", args = {'currentUser': 'im'}))
                })
        return redirect("profile", "im")
    if request.method == "POST":
        if request.user.is_authenticated is False:
            form = registrationForm(request.POST)
            if form.is_valid():
                data = form.cleaned_data
                username = data["username"]
                password = data["password"]
                email = data["email"]
                rPattern = re.compile('\.|\*|//|\\\|\\|\$|&|/|@|%|!|_|__|\^| ')
                finded = rPattern.findall(username)
                if finded:
                    dataCache = caches["smallDataCache"]
                    troubleUserName = slugify(username)
                    splited = re.split(rPattern, username)
                    dataOwnerIp = get_ip(request)
                    if dataOwnerIp is not None:
                        dataCache.set(troubleUserName, {
                            'ip': get_ip(request),
                            'data': splited,
                            'usernameSafed': username,
                            'usernameSlugify': troubleUserName,
                            'email': email,
                            'password': password
                            }, 60)
                    if request.is_ajax():
                        return JsonResponse({"redirectTo": 
                                             json.dumps(reverse("registrationStrong", kwargs = {'troubleUsername': troubleUserName}))
                                             })
                    return redirect("registrationStrong", troubleUserName)
                slug = slugify(username)
                email = form.cleaned_data["email"]
                newUser = mainUserProfile.objects.create_user(username, email, password)
                newUser.profileUrl = slug
                with transaction.atomic():
                    newUser.save()
                    userProfilePrivacy.objects.create(user = newUser)
                user = authenticate(username = username, password = password)
                login(request, user)
                if request.is_ajax():
                    return JsonResponse({"fullRedirect": json.dumps(reverse("profile", kwargs = {'currentUser': 'im'}))})
                return redirect("profile", "im")
            if request.is_ajax():
                return JsonResponse({"newPage": json.dumps({
                    'templateBody': rts("psixiSocial/body/registration.html", {
                        'form': form,
                        'csrf': '',
                        'helpTextDict': OrderedDict([
                            ('h1', ('Регистрация',))
                            ])
                        }),
                    })})
            fragments = cachedFragments()
            return render("psixiSocial/psixiSocial.html", {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'registration'),
                'form': form,
                'csrf': checkCSRF(request, request.COOKIES),
                'helpTextDict': OrderedDict([
                    ('h1', ('Регистрация',))
                    ]),
                'menu': fragments.menuGetter('menuType1Serialized')
                })
        if request.is_ajax():
            return JsonResponse({
                "redirectTo": json.dumps(reverse("profile", args = {'currentUser': 'im'}))
                })
        return redirect("profile", "im")

def profileRegistrationStrong(request, troubleUsername):
    if request.method == "GET":
        if request.user.is_authenticated is False:
            fragments = cachedFragments()
            dataCache = caches["smallDataCache"]
            currentDataCache = dataCache.get(troubleUsername, None)
            if currentDataCache is not None:
                currentIp, lastDataIp = get_ip(request), currentDataCache.get("ip", None)
                if currentIp and lastDataIp and currentIp == lastDataIp:
                    data = currentDataCache.get('data', None)
                    if data:
                        username = currentDataCache.get('usernameSafed', '')
                        email = currentDataCache.get('email', '')
                        password = currentDataCache.get('password', '')
                        choice = [(data.index(v), v) for v in data]
                        if request.is_ajax():
                            return JsonResponse({"newPage": json.dumps({
                                'templateHead': rts("psixiSocial/head/registration.html"),
                                'templateBody': rts("psixiSocial/body/registration.html", {
                                    'form': registrationCheckNickAndClanForm(choices = choice, initial = {
                                        'username': username,
                                        'email': email,
                                        'password': password,
                                        'passwordRepeat': password
                                        }),
                                    'helpTextDict': OrderedDict((
                                        ('h1', ('Регистрация',)),
                                        ('h3', ('Похоже, указанный вами логин значит нечто большее, чем просто логин.',)),
                                        ('p', ('Чтобы завершить регистрацию нужно прояснить этот момент:',
                                               'В поле формы укажите какая часть является вашим ником в THPS, а какая наименованием клана, в котором вы состоите.'))
                                        )),
                                    'csrf': ''
                                    }),
                                'templateScripts': rts("psixiSocial/scripts/edit.html"),
                                'content': ('psixiSocial', 'registration', 'extended'),
                                'menu': fragments.menuGetter('menuType2'),
                                'url': request.path,
                                'titl': 'Регистрация'
                                })})
                        return render(request, "psixiSocial/psixiSocial.html", {
                            'parentTemplate': 'base/classicPage/emptyParent.html',
                            'content': ('psixiSocial', 'registration', 'extended'),
                            'form': registrationCheckNickAndClanForm(initial = {
                                'username': username,
                                'email': email,
                                'password': password,
                                'passwordRepeat': password
                                }, choices = choice),
                            'csrf': checkCSRF(request, request.COOKIES),
                            'helpTextDict': OrderedDict((
                                ('h1', ('Регистрация',)),
                                ('h3', ('Похоже, указанный вами логин значит нечто большее, чем просто логин.',)),
                                ('p', ('Чтобы завершить регистрацию нужно прояснить этот момент:',
                                      'В поле формы укажите какая часть является вашим ником в THPS, а какая наименованием клана, в котором вы состоите.'))
                                )),
                            'menu': fragments.menuGetter('menuType2Serialized'),
                            'titl': 'Регистрация'
                            })
            if request.is_ajax():
                return JsonResponse({"newPage": json.dumps({
                    'templateHead': rts("psixiSocial/head/registration.html"),
                    'templateBody': rts("psixiSocial/body/registration.html", {
                        'form': registrationForm(),
                        'csrf': '',
                        'helpTextDict': OrderedDict([
                            ('h1', ('Регистрация',))
                            ])
                        }),
                    'templateScripts': rts("psixiSocial/scripts/edit.html"),
                    'content': ('psixiSocial', 'registration'),
                    'menu': fragments.menuGetter('menuType1'),
                    'url': reverse("registration"),
                    'titl': 'Регистрация'
                    })})
            return redirect("registration")
        if request.is_ajax():
            return JsonResponse({
                "redirectTo": json.dumps(reverse("profile", args = {'currentUser': 'im'}))
                })
        return redirect("profile", "im")
    elif request.method == "POST":
        if request.user.is_authenticated is False:
            dataCache = caches["smallDataCache"]
            currentDataCache = dataCache.get(troubleUsername, None)
            if currentDataCache is not None:
                currentIp, lastDataIp = get_ip(request), currentDataCache.get("ip", None)
                if currentIp and lastDataIp and currentIp == lastDataIp:
                    data = currentDataCache.get('data', None)
                    if data:
                        choice = [(data.index(v), v) for v in data]
                        form = registrationCheckNickAndClanForm(request.POST, choices = choice)
                        if form.is_valid():
                            formData = form.cleaned_data
                            onlyUsername = formData["itsUsername"]
                            username = formData["username"]
                            password = formData["password"]
                            if onlyUsername == True:
                                newProfile = mainUserProfile.objects.create_user(username = username,
                                                                                 email = formData["email"],
                                                                                 password = password,
                                                                                 profileUrl = troubleUsername)
                            else:
                                choiceToFind = copy.copy(choice)
                                del choiceToFind[int(formData["nick"])]
                                clanName = choiceToFind[0][1]
                                clanSlugName = slugify(clanName)
                                slugUsername = slugify(username)
                                newProfile = mainUserProfile.objects.create_user(username = username,
                                                                                 email = formData["email"],
                                                                                 password = password,
                                                                                 profileUrl = slugUsername)
                                try:
                                    findInPlayers = playersShip.objects.get(slugName = slugUsername)
                                except playersShip.DoesNotExist:
                                    findInPlayers = None
                                try:
                                    findInClans = clansMain.objects.get(slugName = clanSlugName)
                                except clansMain.DoesNotExist:
                                    findInClans = None
                                if findInPlayers is None and findInClans is None:
                                    newPlayer = playersShip.objects.create(slugName = slugUsername, name = username, createdBy = newProfile, status = 'w')
                                    newClan = clansMain.objects.create(slugName = clanSlugName, name = clanName, addedBy = newProfile, status = 'w')
                                    newTHPSProfile = THPSProfile.objects.create(toUser = newProfile, inClan = newClan, nickName = newPlayer)
                                    newClan.members.add(newPlayer)
                                    with transaction.atomic():
                                        newProfile.save()
                                        userProfilePrivacy.objects.create(user = newProfile)
                                        newPlayer.save()
                                        newClan.save()
                                        newTHPSProfile.save()
                                else:
                                    if findInPlayers is None:
                                        newPlayer = playersShip.objects.create(slugName = slugUsername, name = username, createdBy = newProfile, status = 'w')
                                        findInClans.members.add(newPlayer)
                                        newTHPSProfile = THPSProfile.objects.create(toUser = newProfile, inClan = findInClans, nickName = newPlayer)
                                        with transaction.atomic():
                                            newProfile.save()
                                            userProfilePrivacy.objects.create(user = newProfile)
                                            newPlayer.save()
                                            findInClans.save()
                                            newTHPSProfile.save()
                                    elif findInClans is None:
                                        newClan = clansMain.objects.create(slugName = clanSlugName, name = clanName, addedBy = newProfile, status = 'w')
                                        newClan.members.add(findInPlayers)
                                        newTHPSProfile = THPSProfile.objects.create(toUser = newProfile, inClan = newClan, nickName = findInPlayers)
                                        with transaction.atomic():
                                            newProfile.save()
                                            userProfilePrivacy.objects.create(user = newProfile)
                                            newClan.save()
                                            newTHPSProfile.save()
                                    else:
                                        newTHPSProfile = THPSProfile.objects.create(toUser = newProfile, inClan = findInClans, nickName = findInPlayers)
                                        with transaction.atomic():
                                            newProfile.save()
                                            userProfilePrivacy.objects.create(user = newProfile)
                                            newTHPSProfile.save()
                            user = authenticate(username = username, password = password)
                            login(request, user)
                            if request.is_ajax():
                                return JsonResponse({"redirectTo": json.dumps(reverse("profile", kwargs = {'currentUser': 'im'}))})
                            return redirect("profile", "im")
                        if request.is_ajax():
                            pass
                        pass
                    fragments = cachedFragments()
                    if request.is_ajax():
                        return JsonResponse({"newPage": json.dumps({
                            'templateHead': rts("psixiSocial/head/registration.html"),
                            'templateBody': rts("psixiSocial/body/registration.html", {
                                'form': registrationForm(),
                                'csrf': '',
                                'helpTextDict': OrderedDict([
                                    ('h1', ('Регистрация',))
                                    ])
                                }),
                            'templateScripts': rts("psixiSocial/scripts/edit.html"),
                            'content': ('psixiSocial', 'registration'),
                            'menu': fragments.menuGetter('menuType1'),
                            'url': reverse("registration"),
                            'titl': 'Регистрация'
                            })})
                    return redirect("registration")
                if request.is_ajax():
                    return JsonResponse({"newPage": json.dumps({
                        'templateHead': rts("psixiSocial/head/registration.html"),
                        'templateBody': rts("psixiSocial/body/registration.html", {
                            'form': registrationForm(),
                            'csrf': '',
                            'helpTextDict': OrderedDict([
                                ('h1', ('Регистрация',))
                                ])
                            }),
                        'templateScripts': rts("psixiSocial/scripts/edit.html"),
                        'content': ('psixiSocial', 'registration'),
                        'menu': fragments.menuGetter('menuType1'),
                        'url': reverse("registration"),
                        'titl': 'Регистрация'
                        })})
                return redirect("registration")
            if request.is_ajax():
                return JsonResponse({"newPage": json.dumps({
                    'templateHead': rts("psixiSocial/head/registration.html"),
                    'templateBody': rts("psixiSocial/body/registration.html", {
                        'form': registrationForm(),
                        'csrf': '',
                        'helpTextDict': OrderedDict([
                            ('h1', ('Регистрация',))
                            ])
                        }),
                    'templateScripts': rts("psixiSocial/scripts/edit.html"),
                    'content': ('psixiSocial', 'registration'),
                    'menu': fragments.menuGetter('menuType1'),
                    'url': reverse("registration"),
                    'titl': 'Регистрация'
                    })})
            return redirect("registration")
        if request.is_ajax():
            return JsonResponse({
                "redirectTo": json.dumps(reverse("profile", args = {'currentUser': 'im'}))
                })
        return redirect("profile", "im")