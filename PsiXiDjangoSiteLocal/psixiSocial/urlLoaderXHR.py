import requests
from django.http import JsonResponse

def loadWebPage(request):
    page = requests.get('http://psixi-thps.pro')
    documentStr = page.text
    return JsonResponse({"page": documentStr})
