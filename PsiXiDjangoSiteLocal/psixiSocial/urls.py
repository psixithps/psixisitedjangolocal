from django.conf.urls import url
from psixiSocial.profileEdit import profileEdit, profileEditHabitation, editPrivacy, editPrivacyExclusiveRights
from psixiSocial.profile.profile import emptyProfileUrl, profileView, renderFriendsList
from psixiSocial.profile.item import getSmallThumbXHR
from psixiSocial.activities import activities
from psixiSocial.users.users import usersListPage
from psixiSocial.friends.friends import friendsListPage
from psixiSocial.dialogs.personalMessages import dialogPersonal, filesLoaderPersonal
from psixiSocial.dialogs.groupMessages import dialogGroup, filesLoaderGroup
from psixiSocial.dialogs.dialogs import dialogPage, getNewDialogXHR
from psixiSocial.friend.cancel import cancelAddFriendRequest
from psixiSocial.friend.confirm import addFriend
from psixiSocial.friend.reject import rejectFriend
from psixiSocial.friend.remove import removeFriend
from psixiSocial.friend.request import addFriendRequest
from psixiSocial.urlLoaderXHR import loadWebPage

urlpatterns = [
    url(r'^$', emptyProfileUrl),
    url(r'^get-small-thumb/$', getSmallThumbXHR, name = "loadSmallThumb"),
    url(r'^loadwebresource/$', loadWebPage, name = "openLink"),
    url(r'^users/(?P<category>all|online)(?P<sep>/|)(?P<page>\d+|)/$', usersListPage, name = "users"),
    url(r'^(?P<user>[-\w]+|my)/friends/(?P<category>all|online)(?P<sep>/|)(?P<page>\d+|)/$', friendsListPage, name = "friends"),
    url(r'^edit/(?P<editType>main|detail|contacts|thps|password)/$', profileEdit, name = "profileEditUrl"),
    url(r'^edit/privacy/(?P<editPlace>profile)/$', editPrivacy, name = "editPrivacyUrl"),
    url(r'^edit/privacy/(?P<editPlace>profile)/exclusiverights/$', editPrivacyExclusiveRights, name = "editPrivacyExclusiveUrl"),
    url(r'^activity/$', activities, name = "activity"),
    url(r'^dialogs(?P<sep>/|)(?P<page>([0-9]+|))/$', dialogPage, name = "dialogs"),
    url(r'^dialog/(?P<toUser>[-\w]+)/$', dialogPersonal, name = "personalMessage"),
    url(r'^group-dialog/(?P<dialogSlug>[-\w]+)/$', dialogGroup, name = "groupMessage"),
    url(r'^dialog/(?P<toUserId>[0-9]+)/(?P<type>image|file|video)/xhr-send-files/$', filesLoaderPersonal, name = "dialogMessagesFileLoaderPersonal"),
    url(r'^dialog/(?P<dialogSlugName>[-\w]+)/(?P<type>image|file|video)/xhr-send-files/$', filesLoaderGroup, name = "dialogMessagesFileLoaderGroup"),
    url(r'^dialogs/load-new-item/$', getNewDialogXHR, name = "loadDialogListItem"),
    url(r'service/(?P<userId>\d+)/reloadfriendslist/', renderFriendsList, name = "reloadFriendsList"),
    url(r'^ajax/loadLocationParametrs$', profileEditHabitation, name = "loadLocations"),
    url(r'^(?P<userId>\d+)/addfriend/$', addFriendRequest, name = "addFriendRequest"),
    url(r'^(?P<userId>\d+)/confirmfriend/$', addFriend, name = "addFriend"),
    url(r'^(?P<userId>\d+)/rejectfriend/$', rejectFriend, name = "rejectFriend"),
    url(r'^(?P<userId>\d+)/canceladdfriend/$', cancelAddFriendRequest, name = "cancelAddFriendRequest"),
    url(r'^(?P<userId>\d+)/removefriend/$', removeFriend, name = "removeFriend"),
    url(r'^(?P<currentUser>[-\w]+|im)/$', profileView, name = "profile"),
]