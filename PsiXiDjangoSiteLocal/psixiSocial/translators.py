from functools import lru_cache

class profileTimeStatusText():
    def __init__(self, *args, **kwargs):
        self.langudge = kwargs.pop('lang')

    @property
    @lru_cache(maxsize = None)
    def ru(self):
        textDict = {
            'sec': ['секунду', 'секунды', 'секунд'],
            'min': ['минуту', 'минуты', 'минут'],
            'hour': ['час', 'часа', 'часов'],
            'day': ['день', 'дня', 'дней'],
            'uknownStatus': 'Статус определится через',
            'uknownStatusNoJavascript': 'Статус определится в течение 20 секунд',
            'offlineStatus': 'Заходил',
            'onlineStatus': 'Сейчас на сайте',
            'today': 'сегодня',
            'yesterday': 'вчера',
            'timeEndString': 'назад'
            }
        return textDict

    @property
    @lru_cache(maxsize = None)
    def en(self):
        pass

    def getDict(self):
        defineMethod = getattr(self, self.langudge)
        if isinstance(defineMethod, dict):
            return defineMethod
        pack = defineMethod()
        return pack

class usersListText():
    ''' Выводимый текст для страниц: users/users online/friends/friends online '''
    def __init__(self, *args, **kwargs):
        self.langudge = kwargs.pop('lang')

    @property
    @lru_cache(maxsize = None)
    def ru(self):
        stringsDict = {
            'personalmessage': 'Перейти к диалогу',
            'viewFriends': 'Посмотреть друзей',
            'addToFriends': 'Добавить в друзья',
            'confirm': 'Принять',
            'reject': 'Отклонить',
            'cancel': 'Отменить заявку',
            'removeFromFriends': 'Удалить из друзей',
            'setBlock': 'Заблокировать',
            'uknownStatus': 'Статус определится в течение 20 секунд',
            'onlineStatus': 'Сейчас на сайте',
            'activeStatus': 'Доступен',
            'middleStatus': 'Отошёл',
            'leaveStatus': 'Недоступен'
            }
        return stringsDict

    def getDict(self):
        defineMethod = getattr(self, self.langudge)
        if isinstance(defineMethod, dict):
            return defineMethod
        pack = defineMethod()
        return pack

class singleElementGetter():
    ''' Используется для вывода одиночной строки на фронтенд, если её там нету и не было. '''
    def __init__(self, *args, **kwargs):
        self.langudge = kwargs.pop('lang')

    #@property
    #@lru_cache(maxsize = None)
    def ru(self, stringName):
        stringsDict = {'friendsOnline': 'Друзья онлайн', 'friendsAll': 'Все', 'userProfileIsHidden': 'скрыл свой профиль настройками приватности', 'userFriendsHidden': 'cкрыл список своих друзей настройками приватности'}
        findedString = stringsDict[stringName]
        return findedString

    def getString(self, stringName):
        defineMethod = getattr(self, self.langudge)
        if isinstance(defineMethod, str):
            return defineMethod
        string = defineMethod(stringName)
        return string