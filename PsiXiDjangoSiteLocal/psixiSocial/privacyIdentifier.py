from psixiSocial.models import userProfilePrivacy, mainUserProfile

class checkPrivacy():
    def __init__(self, **kwargs):
        self.userId = kwargs.get('id', None)
        self.privacyModelInstance = kwargs['privacy']
        self.isFriend = kwargs.get('isFriend', None)
        self.isAuthenticated = kwargs['isAuthenticated']

    def checkViewProfilePrivacy(self):
        viewProfile = self.privacyModelInstance.viewProfile
        if viewProfile == 0:
            return True
        elif viewProfile == 1:
            if self.isAuthenticated:
                return True
            return False
        elif viewProfile == 2:
            if self.isAuthenticated:
                if self.isFriend:
                    return True
                return False
            return False
        elif viewProfile == 3:
            if self.isAuthenticated:
                if self.isFriend:
                    return True
                return False
            return False
        elif viewProfile == 8:
            return False
        else: # Временная заглушка
            return False

    def checkMainInfoPrivacy(self):
        mainInfoPrivacy = self.privacyModelInstance.viewProfileMainInfo
        if mainInfoPrivacy == 0:
            return True
        elif mainInfoPrivacy == 1:
            if self.isAuthenticated:
                return True
            return False
        elif mainInfoPrivacy == 2:
            if self.isAuthenticated:
                if self.isFriend:
                    return False
                return False
            return False
        elif mainInfoPrivacy == 7:
            return False
        else: # Временная заглушка
            return False

    def checkTHPSInfoPrivacy(self):
        thpsInfoPrivacy = self.privacyModelInstance.viewProfileTHPSInfo
        if thpsInfoPrivacy == 0:
            return True
        elif thpsInfoPrivacy == 1:
            if self.isAuthenticated:
                return True
            return False
        elif thpsInfoPrivacy == 2:
            if self.isAuthenticated:
                if self.isFriend:
                    return True
                return False
            return False
        elif thpsInfoPrivacy == 7:
            return False
        else: # Временная заглушка
            return False

    def checkOnlineStatusPrivacy(self):
        viewOnlineStatus = self.privacyModelInstance.viewProfileOnlineStatus
        if viewOnlineStatus == 0:
            return True
        elif viewOnlineStatus == 1:
            if self.isAuthenticated:
                return True
            return False
        elif viewOnlineStatus == 2:
            if self.isAuthenticated:
                if self.isFriend:
                    return True
                return False
            return False
        elif viewOnlineStatus == 7:
            return False
        else: # Временная заглушка
            return False

    def checkActiveStatusPrivacy(self):
        viewActiveStatus = self.privacyModelInstance.viewProfileActiveStatus
        if viewActiveStatus == 0:
            return True
        elif viewActiveStatus == 1:
            if self.isAuthenticated:
                return True
            return False
        elif viewActiveStatus == 2:
            if self.isAuthenticated:
                if self.isFriend:
                    return True
                return False
            return False
        elif viewActiveStatus == 7:
            return False
        else: # Временная заглушка
            return False

    def checkFriendsListPrivacy(self):
        friendsPrivacy = self.privacyModelInstance.viewProfileFriends
        if friendsPrivacy == 0:
            return True
        elif friendsPrivacy == 1:
            if self.isAuthenticated:
                return True
            return False
        elif friendsPrivacy == 2:
            if self.isAuthenticated:
                if self.isFriend:
                    return True
                return False
            return False
        elif friendsPrivacy == 7:
            return False
        else: # Временная заглушка
            return False

    def checkFriendsOnlineListPrivacy(self):
        friendsOnlinePrivacy = self.privacyModelInstance.viewFriendsOnline
        if friendsOnlinePrivacy == 0:
            return True
        elif friendsOnlinePrivacy == 1:
            if self.isAuthenticated:
                return True
            return False
        elif friendsOnlinePrivacy == 2:
            if self.isAuthenticated:
                if self.isFriend:
                    return True
                return False
            return False
        elif friendsOnlinePrivacy == 7:
            return False
        else: # Временная заглушка
            return False

    def checkTHPSUsersFriends(self):
        thpsUsersFriends = self.privacyModelInstance.viewTHPSUsersFriends
        if thpsUsersFriends == 0:
            return True
        elif thpsUsersFriends == 1:
            if self.isAuthenticated:
                return True
            return False
        elif thpsUsersFriends == 2:
            if self.isAuthenticated:
                if self.isFriend:
                    return True
                return False
            return False
        elif thpsUsersFriends == 7:
            return False
        else: # Временная заглушка
            return False

    def checkTHPSUsersFriendsOnline(self):
        thpsUsersFriendsOnline = self.privacyModelInstance.viewTHPSUsersFriendsOnline
        if thpsUsersFriendsOnline == 0:
            return True
        elif thpsUsersFriendsOnline == 1:
            if self.isAuthenticated:
                return True
            return False
        elif thpsUsersFriendsOnline == 2:
            if self.isAuthenticated:
                if self.isFriend:
                    return True
                return False
            return False
        else: # Временная заглушка
            return False

    def checkAvatarPrivacy(self):
        avatarPrivacy = self.privacyModelInstance.viewProfileAvatar
        if avatarPrivacy == 0:
            return True
        elif avatarPrivacy == 1:
            if self.isAuthenticated:
                return True
            return False
        elif avatarPrivacy == 2:
            if self.isAuthenticated:
                if self.isFriend:
                    return True
                return False
            return False
        elif avatarPrivacy == 7:
            return False
        else: # Временная заглушка
            return False

    def sendPersonalMessages(self):
        personalMessagesPrivacy = self.privacyModelInstance.sendPersonalMessages
        if personalMessagesPrivacy == 0:
            return True
        elif personalMessagesPrivacy == 1:
            if self.isAuthenticated:
                return True
            return False
        elif personalMessagesPrivacy == 2:
            if self.isAuthenticated:
                if self.isFriend:
                    return True
                return False
            return False
        elif personalMessagesPrivacy == 7:
            return False
        else: # Временная заглушка
            return False

    def sendFriendRequests(self):
        friendRequests = self.privacyModelInstance.sendFriendRequests
        if self.isAuthenticated:
            if self.isFriend:
                return False
            if friendRequests == 1:
                return True
            elif friendRequests == 3:
                return True
            elif friendRequests == 4:
                return True
            elif friendRequests == 7:
                return False
        return False