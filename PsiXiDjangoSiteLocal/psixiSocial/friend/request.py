import json
from datetime import datetime
from django.contrib.auth import logout
from django.views.decorators.http import require_POST
from psixiSocial.login import login_decorator
from psixiSocial.models import mainUserProfile, userProfilePrivacy
from psixiSocial.privacyIdentifier import checkPrivacy
from psixiSocial.translators import profileTimeStatusText, singleElementGetter
from django.http import JsonResponse
from django.shortcuts import redirect
from django.core.cache import caches
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.urls import reverse

@login_decorator
@require_POST
def addFriendRequest(request, userId):
    senderInstance = request.user
    senderId = str(senderInstance.id)
    try:
        myUserInstance = mainUserProfile.objects.get(id__exact = senderId)
    except:
        logout(request)
        return redirect(reverse('login'))
    try:
        friendUserInstance = mainUserProfile.objects.get(id__exact = userId)
    except mainUserProfile.DoesNotExist:
        pass # return
    privacyCache = caches['privacyCache']
    privacyMany = privacyCache.get_many({senderId, userId})
    privacyToSet = {}
    friendShipRequestsCache = None
    friendShipRequestsMy = None
    friendShipStatusDictMy = None
    friendShipRequestsFriend = None
    friendShipCache = None
    friendsCacheId = None
    friendsCache = None
    privacyDictFriend = privacyMany.get(userId, None)
    if privacyDictFriend is None:
        privacy = userProfilePrivacy.objects.get(user_id = userId)
        friendShipCache = caches['friendShipStatusCache']
        friendShipStatusDictMy = friendShipCache.get(senderId, None)
        if friendShipStatusDictMy is None:
            friendsCacheId = caches['friendsIdCache']
            friendsIds = friendsCacheId.get(senderId, None)
            if friendsIds is None:
                friendsCache = caches['friendsCache']
                friends = friendsCache.get(senderId, None)
                if friends is None:
                    friends = senderInstance.friends.all()
                    if friends.count():
                        friendsCache.set(senderId, friends)
                        friendsIdsQuerySet = userFriends.values("id")
                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                        friendsCacheId.set(senderId, friendsIds)
                    else:
                        friendsCache.set(senderId, False)
                        friendsIds = set()
                        friendsCacheId.set(senderId, friendsIds)
                else:
                    if friends is False:
                        friendsIds = set()
                    else:
                        friendsIdsQuerySet = userFriends.values("id")
                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                    friendsCacheId.set(senderId, friendsIds)
            if userId in friendsIds:
                friendShipStatus = True
            else:
                friendShipStatus = False
                friendShipRequestsCache = caches['friendshipRequests']
                friendShipRequestsMy = friendShipRequestsCache.get(senderId, tuple())
                for friendRequest in friendShipRequestsMy:
                    if friendRequest[0] == userId:
                        friendShipStatus = "request"
                if friendShipStatus is False:
                    friendShipRequestsFriend = friendShipRequestsCache.get(userId, [])
                    for friendRequest in friendShipRequestsFriend:
                        if friendRequest[0] == senderId:
                            friendShipStatus = "myRequest"
        else:
            friendShipStatus = friendShipStatusDictMy.get(userId, None)
            if friendShipStatus is None:
                friendsCacheId = caches['friendsIdCache']
                friendsIds = friendsCacheId.get(senderId, None)
                if friendsIds is None:
                    friendsCache = caches['friendsCache']
                    friends = friendsCache.get(senderId, None)
                    if friends is None:
                        friends = senderInstance.friends.all()
                        if friends.count():
                            friendsCache.set(senderId, friends)
                            friendsIdsQuerySet = userFriends.values("id")
                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                            friendsCacheId.set(senderId, friendsIds)
                        else:
                            friendsCache.set(senderId, False)
                            friendsIds = set()
                            friendsCacheId.set(senderId, friendsIds)
                    else:
                        if friends is False:
                            friendsIds = set()
                        else:
                            friendsIdsQuerySet = userFriends.values("id")
                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                        friendsCacheId.set(senderId, friendsIds)
                if userId in friendsIds:
                    friendShipStatus = True
                else:
                    friendShipStatus = False
                    friendShipRequestsCache = caches['friendshipRequests']
                    friendShipRequestsMy = friendShipRequestsCache.get(senderId, tuple())
                    for friendRequest in friendShipRequestsMy:
                        if friendRequest[0] == userId:
                            friendShipStatus = "request"
                    if friendShipStatus is False:
                        friendShipRequestsFriend = friendShipRequestsCache.get(userId, [])
                        for friendRequest in friendShipRequestsFriend:
                            if friendRequest[0] == senderId:
                                friendShipStatus = "myRequest"
                friendShipStatusDictMy.update({userId: friendShipStatus})
                friendShipCache.set(senderId, friendShipStatusDictMy)
        privacyChecker = checkPrivacy(user = senderInstance, privacy = privacy, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
        sendFriendRequests = privacyChecker.sendFriendRequests()
        privacyToSet.update({userId: {senderId: {'sendFriendRequests': sendFriendRequests}}})
    else:
        userPrivacyDict = privacyDictFriend.get(senderId, None)
        if userPrivacyDict is None:
            privacy = userProfilePrivacy.objects.get(user_id = userId)
            friendShipCache = caches['friendShipStatusCache']
            friendShipStatusDictMy = friendShipCache.get(senderId, None)
            if friendShipStatusDictMy is None:
                friendsCacheId = caches['friendsIdCache']
                friendsIds = friendsCacheId.get(senderId, None)
                if friendsIds is None:
                    friendsCache = caches['friendsCache']
                    friends = friendsCache.get(senderId, None)
                    if friends is None:
                        friends = senderInstance.friends.all()
                        if friends.count():
                            friendsCache.set(senderId, friends)
                            friendsIdsQuerySet = userFriends.values("id")
                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                            friendsCacheId.set(senderId, friendsIds)
                        else:
                            friendsCache.set(senderId, False)
                            friendsIds = set()
                            friendsCacheId.set(senderId, friendsIds)
                    else:
                        if friends is False:
                            friendsIds = set()
                        else:
                            friendsIdsQuerySet = userFriends.values("id")
                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                        friendsCacheId.set(senderId, friendsIds)
                if userId in friendsIds:
                    friendShipStatus = True
                else:
                    friendShipStatus = False
                    friendShipRequestsCache = caches['friendshipRequests']
                    friendShipRequestsMy = friendShipRequestsCache.get(senderId, tuple())
                    for friendRequest in friendShipRequestsMy:
                        if friendRequest[0] == userId:
                            friendShipStatus = "request"
                    if friendShipStatus is False:
                        friendShipRequestsFriend = friendShipRequestsCache.get(userId, [])
                        for friendRequest in friendShipRequestsFriend:
                            if friendRequest[0] == senderId:
                                friendShipStatus = "myRequest"
            else:
                friendShipStatus = friendShipStatusDictMy.get(userId, None)
                if friendShipStatus is None:
                    friendsCacheId = caches['friendsIdCache']
                    friendsIds = friendsCacheId.get(senderId, None)
                    if friendsIds is None:
                        friendsCache = caches['friendsCache']
                        friends = friendsCache.get(senderId, None)
                        if friends is None:
                            friends = senderInstance.friends.all()
                            if friends.count():
                                friendsCache.set(senderId, friends)
                                friendsIdsQuerySet = userFriends.values("id")
                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                friendsCacheId.set(senderId, friendsIds)
                            else:
                                friendsCache.set(senderId, False)
                                friendsIds = set()
                                friendsCacheId.set(senderId, friendsIds)
                        else:
                            if friends is False:
                                friendsIds = set()
                            else:
                                friendsIdsQuerySet = userFriends.values("id")
                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                            friendsCacheId.set(senderId, friendsIds)
                    if userId in friendsIds:
                        friendShipStatus = True
                    else:
                        friendShipStatus = False
                        friendShipRequestsCache = caches['friendshipRequests']
                        friendShipRequestsMy = friendShipRequestsCache.get(senderId, tuple())
                        for friendRequest in friendShipRequestsMy:
                            if friendRequest[0] == userId:
                                friendShipStatus = "request"
                        if friendShipStatus is False:
                            friendShipRequestsFriend = friendShipRequestsCache.get(userId, [])
                            for friendRequest in friendShipRequestsFriend:
                                if friendRequest[0] == senderId:
                                    friendShipStatus = "myRequest"
            privacyChecker = checkPrivacy(user = senderInstance, privacy = privacy, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
            sendFriendRequests = privacyChecker.sendFriendRequests()
            privacyDictFriend.update({senderId: {'sendFriendRequests': sendFriendRequests}})
            privacyToSet.update({userId: privacyDictFriend})
        else:
            sendFriendRequests = userPrivacyDict.get('sendFriendRequests', None)
            if sendFriendRequests is None:
                privacy = userProfilePrivacy.objects.get(user_id = userId)
                friendShipCache = caches['friendShipStatusCache']
                friendShipStatusDictMy = friendShipCache.get(senderId, None)
                if friendShipStatusDictMy is None:
                    friendsCacheId = caches['friendsIdCache']
                    friendsIds = friendsCacheId.get(senderId, None)
                    if friendsIds is None:
                        friendsCache = caches['friendsCache']
                        friends = friendsCache.get(senderId, None)
                        if friends is None:
                            friends = senderInstance.friends.all()
                            if friends.count():
                                friendsCache.set(senderId, friends)
                                friendsIdsQuerySet = userFriends.values("id")
                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                friendsCacheId.set(senderId, friendsIds)
                            else:
                                friendsCache.set(senderId, False)
                                friendsIds = set()
                                friendsCacheId.set(senderId, friendsIds)
                        else:
                            if friends is False:
                                friendsIds = set()
                            else:
                                friendsIdsQuerySet = userFriends.values("id")
                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                            friendsCacheId.set(senderId, friendsIds)
                    if userId in friendsIds:
                        friendShipStatus = True
                    else:
                        friendShipStatus = False
                        friendShipRequestsCache = caches['friendshipRequests']
                        friendShipRequestsMy = friendShipRequestsCache.get(senderId, tuple())
                        for friendRequest in friendShipRequestsMy:
                            if friendRequest[0] == userId:
                                friendShipStatus = "request"
                        if friendShipStatus is False:
                            friendShipRequestsFriend = friendShipRequestsCache.get(userId, [])
                            for friendRequest in friendShipRequestsFriend:
                                if friendRequest[0] == senderId:
                                    friendShipStatus = "myRequest"
                else:
                    friendShipStatus = friendShipStatusDictMy.get(userId, None)
                    if friendShipStatus is None:
                        friendsCacheId = caches['friendsIdCache']
                        friendsIds = friendsCacheId.get(senderId, None)
                        if friendsIds is None:
                            friendsCache = caches['friendsCache']
                            friends = friendsCache.get(senderId, None)
                            if friends is None:
                                friends = senderInstance.friends.all()
                                if friends.count():
                                    friendsCache.set(senderId, friends)
                                    friendsIdsQuerySet = userFriends.values("id")
                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    friendsCacheId.set(senderId, friendsIds)
                                else:
                                    friendsCache.set(senderId, False)
                                    friendsIds = set()
                                    friendsCacheId.set(senderId, friendsIds)
                            else:
                                if friends is False:
                                    friendsIds = set()
                                else:
                                    friendsIdsQuerySet = userFriends.values("id")
                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                friendsCacheId.set(senderId, friendsIds)
                        if userId in friendsIds:
                            friendShipStatus = True
                        else:
                            friendShipStatus = False
                            friendShipRequestsCache = caches['friendshipRequests']
                            friendShipRequestsMy = friendShipRequestsCache.get(senderId, tuple())
                            for friendRequest in friendShipRequestsMy:
                                if friendRequest[0] == userId:
                                    friendShipStatus = "request"
                            if friendShipStatus is False:
                                friendShipRequestsFriend = friendShipRequestsCache.get(userId, [])
                                for friendRequest in friendShipRequestsFriend:
                                    if friendRequest[0] == senderId:
                                        friendShipStatus = "myRequest"
                privacyChecker = checkPrivacy(user = senderInstance, privacy = privacy, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                sendFriendRequests = privacyChecker.sendFriendRequests()
                userPrivacyDict.update({'sendFriendRequests': sendFriendRequests})
                privacyToSet.update({userId: privacyDictFriend})
    if sendFriendRequests:
        if friendShipCache is None:
            friendShipCache = caches['friendShipStatusCache']
        if friendShipRequestsCache is None:
            friendShipRequestsCache = caches["friendshipRequests"]
        if friendsCacheId is None:
            friendsCacheId = caches['friendsIdCache']
            friendsIds = friendsCacheId.get(senderId, None)
            if friendsIds is None:
                if friendsCache is None:
                    friendsCache = caches['friendsCache']
                    friends = senderInstance.friends.all()
                    if len(friends) > 0:
                        friendsCache.set(senderId, friends)
                        friendsIdsQuerySet = userFriends.values("id")
                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                    else:
                        friendsCache.set(senderId, False)
                        friendsIds = set()
                    friendsCacheId.set(senderId, friendsIds)
                else:
                    if friends is False:
                        friendsIds = set()
                    else:
                        friendsIdsQuerySet = userFriends.values("id")
                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                    friendsCacheId.set(senderId, friendsIds)
        if userId in friendsIds:
            if request.is_ajax():
                return JsonResponse({'data': None})
            return redirect("profile", friendUserInstance.profileUrl)
        if friendShipRequestsFriend is None:
            friendShipRequestsFriend = friendShipRequestsCache.get(userId, [])
        for friendShipRequest in friendShipRequestsFriend:
            if friendShipRequest[0] == senderId:
                if request.is_ajax():
                    return JsonResponse({'data': None})
                return redirect("profile", friendUserInstance.profileUrl)
        if friendShipRequestsMy is None:
            friendShipRequestsMy = friendShipRequestsCache.get(senderId, set())
        for friendShipRequest in friendShipRequestsMy:
            if friendRequest[0] == userId:
                if request.is_ajax():
                    return JsonResponse({'data': None})
                return redirect("profile", friendUserInstance.profileUrl)
        if friendShipStatusDictMy is None:
            friendShipStatusMany = friendShipCache.get_many({senderId, userId})
            friendShipStatusDictMy = friendShipStatusMany.get(senderId, None)
            friendShipStatusFriendDict = friendShipStatusMany.get(userId, None)
        else:
            friendShipStatusFriendDict = friendShipCache.get(userId, None)
        if friendShipStatusDictMy is not None and friendShipStatusFriendDict is not None:
            friendShipStatusDictMy[userId] = "request"
            friendShipStatusFriendDict[senderId] = "myRequest"
            friendShipCache.set_many({senderId: friendShipStatusDictMy, userId: friendShipStatusFriendDict})
        else:
            if friendShipStatusDictMy is not None:
                friendShipStatusDictMy[userId] = "request"
                friendShipCache.set_many({senderId: friendShipStatusDictMy, userId: {senderId: "myRequest"}})
            elif friendShipStatusFriendDict is not None:
                friendShipStatusFriendDict[senderId] = "myRequest"
                friendShipCache.set_many({senderId: {userId: "request"}, userId: friendShipStatusFriendDict})
        privacyDictMy = privacyMany.get(senderId, None)
        if privacyDictMy is not None:
            privacyDictMyKeys = privacyDictMy.keys()
            if userId in privacyDictMyKeys:
                del privacyDictMy[userId]
                if len(privacyDictMyKeys) > 0:
                    privacyToSet.update({senderId: privacyDictMy})
                else:
                    privacyCache.delete(senderId)
        if len(privacyToSet.keys()) > 0:
            privacyCache.set_many(privacyToSet)
        smallThumbsCache = caches['smallThumbs']
        smallThumbsDict = smallThumbsCache.get(senderId, None)
        if smallThumbsDict is not None:
            smallThumbsDictKeys = smallThumbsDict.keys()
            if userId in smallThumbsDictKeys:
                del smallThumbsDict[userId]
                if len(smallThumbsDictKeys) > 0:
                    smallThumbsCache.set(senderId, thumbsDict)
                else:
                    smallThumbsCache.delete(senderId)
        bigThumbsCache = caches['dialogThumbs']
        bigThumbsDict = bigThumbsCache.get(senderId, None)
        if bigThumbsDict is not None:
            bigThumbsDictKeys = bigThumbsDict.keys()
            if userId in bigThumbsDictKeys:
                del bigThumbsDict[userId]
                if len(bigThumbsDictKeys) > 0:
                    bigThumbsCache.set(senderId, bigThumbsDict)
                else:
                    bigThumbsCache.delete(senderId)
        dialogListThumbsCache = caches['dialogListThumbs']
        dialogListThumbsDict = dialogListThumbsCache.get(senderId, None)
        if dialogListThumbsDict is not None:
            dialogListThumbsDictKeys = dialogListThumbsDict.keys()
            if userId in dialogListThumbsDictKeys:
                del dialogListThumbsDict[userId]
                if len(dialogListThumbsDictKeys) > 0:
                    dialogListThumbsCache.set(senderId, dialogListThumbsDict)
                else:
                    dialogListThumbsCache.delete(senderId)
        messagesListThumbsCache = caches['messagesListThumbs']
        messagesListThumbsDict = messagesListThumbsCache.get(senderId, None)
        if messagesListThumbsDict is not None:
            messagesListThumbsDictKeys = messagesListThumbsDict.keys()
            if userId in messagesListThumbsDictKeys:
                del messagesListThumbsDict[userId]
                if len(messagesListThumbsDictKeys) > 0:
                    messagesListThumbsCache.set(senderId, messagesListThumbsDict)
                else:
                    messagesListThumbsCache.delete(senderId)
        usersListThumbsCache = caches['usersListThumbs']
        usersListThumbsDict = usersListThumbsCache.get(senderId, None)
        if usersListThumbsDict is not None:
            usersListThumbsDictKeys = usersListThumbsDict.keys()
            if userId in usersListThumbsDictKeys:
                del usersListThumbsDict[userId]
                if len(usersListThumbsDictKeys) > 0:
                    usersListThumbsCache.set(senderId, usersListThumbsDict)
                else:
                    usersListThumbsCache.delete(senderId)
        timeStr = str(datetime.now())
        friendShipRequestsFriend.append([senderId, timeStr])
        friendShipRequestsCache.set(userId, friendShipRequestsFriend)
        updatesCache = caches["newUpdates"]
        updates = updatesCache.get(userId, [])
        updates.append(['friend', 'new', senderId, timeStr])
        updatesCache.set(userId, updates)
        senderSlug = senderInstance.profileUrl
        layer = get_channel_layer().group_send
        async_to_sync(layer)(userId, {"type": "sendToClient", "data": {"item": json.dumps((
            {
                'method': 'doReloadIfCurrentLink',
                'link': reverse("profile", kwargs = {'currentUser': senderSlug})
            }, {
                'method': 'friendRequestToMeNotify',
                'value': 'friend-new-%s' % senderId,
                'eventType': 'notify',
                'profileLink': reverse("profile", kwargs = {'currentUser': senderSlug}),
                'username': senderInstance.username,
                'confirmLink': reverse("addFriend", kwargs = {'userId': senderId}),
                'rejectLink': reverse("rejectFriend", kwargs = {'userId': senderId}),
                'forgetLink': reverse("deleteNotifyEvent", kwargs = {'type': 'friend', 'state': 'new', 'senderId': senderId}),
                'notifyCounterChange': {
                    'method': 'incrementNotifyValue',
                    'baseValue': 1,
                    'multiValue': 1
                }
            },
            ))}})
        friendSlug = friendUserInstance.profileUrl
        data = json.dumps({
            'method': 'friendRequest',
            'eventType': 'fragment',
            'class': 'profile-actions',
            'path': reverse("profile", kwargs = {'currentUser': friendSlug}),
            'cancelFriendLink': reverse("cancelAddFriendRequest", kwargs = {'userId': userId})
            })
        async_to_sync(layer)(senderId, {"type": "sendToClient", "data": {"item": data}})
        if request.is_ajax():
            return JsonResponse({'data': data})
        return redirect("profile", friendSlug)
    if request.is_ajax():
        return JsonResponse({'data': None})
    return redirect("profile", friendUserInstance.profileUrl)