import json
from datetime import datetime
from django.contrib.auth import logout
from django.views.decorators.http import require_POST
from psixiSocial.login import login_decorator
from django.db import transaction, Error
from psixiSocial.models import mainUserProfile, userProfilePrivacy, userUpdatesHistory
from psixiSocial.privacyIdentifier import checkPrivacy
from psixiSocial.translators import profileTimeStatusText, singleElementGetter
from django.http import JsonResponse
from django.shortcuts import redirect
from django.core.cache import caches
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.urls import reverse

@login_decorator
@require_POST
def removeFriend(request, userId):
    userInstance = request.user
    id = str(userInstance.id)
    try:
        mainUserProfile.objects.get(id__exact = id)
    except:
        logout(request)
        return redirect(reverse('login'))
    try:
        friendUserInstance = mainUserProfile.objects.get(id__exact = userId)
    except mainUserProfile.DoesNotExist:
        pass # return
    friends = None
    friendsCache = None
    friendsIdsCache = caches['friendsIdCache']
    myFriendsIdsSet = friendsIdsCache.get(id, None)
    if myFriendsIdsSet is None:
        friendsCache = caches['friendsCache']
        friends = friendsCache.get(id, None)
        if friends is None:
            friends = userInstance.friends.all()
            if friends.count():
                friendsIdsQuerySet = userFriends.values("id")
                myFriendsIdsSet = {user.get("id") for user in friendsIdsQuerySet}
            else:
                myFriendsIdsSet = set()
        else:
            if friends is not False:
                friendsIdsQuerySet = userFriends.values("id")
                myFriendsIdsSet = {user.get("id") for user in friendsIdsQuerySet}
            else:
                myFriendsIdsSet = set()
        friendsIdsCache.set(id, myFriendsIdsSet)
    if userId in myFriendsIdsSet:
        newUpdateInHistoryMy = userUpdatesHistory.objects.create(owner = userInstance, destination = friendUserInstance)
        newUpdateInHistoryMy.name = 'friend'
        newUpdateInHistoryMy.state = 'removedByMe'
        newUpdateInHistoryFriend = userUpdatesHistory.objects.create(owner = friendUserInstance, destination = userInstance)
        newUpdateInHistoryFriend.name = 'friend'
        newUpdateInHistoryFriend.state = 'removedByFriend'
        userInstance.friends.remove(friendUserInstance)
        friendUserInstance.friends.remove(userInstance)
        try:
            with transaction.atomic():
                userInstance.save()
                friendUserInstance.save()
                newUpdateInHistoryMy.save()
                newUpdateInHistoryFriend.save()
        except Error:
            if request.is_ajax():
                return JsonResponse({'data': None})
            return redirect("profile", userInstance.profileUrl)
        users = {id, userId}
        privacyCache = caches['privacyCache']
        privacyDict = privacyCache.get_many(users)
        privacyDictMy = privacyDict.get(id, None)
        privacyDictFriend = privacyDict.get(userId, None)
        if privacyDictMy is not None and privacyDictFriend is not None:
            existsMy, existsFriend = userId in privacyDictMy.keys(), id in privacyDictFriend.keys()
            if existsMy and existsFriend:
                del privacyDictMy[userId], privacyDictFriend[id]
                privacyCache.set_many({id: privacyDictMy, userId: privacyDictFriend})
            else:
                if existsMy:
                    del privacyDictMy[userId]
                    privacyCache.set(id, privacyDictMy)
                elif existsFriend:
                    del privacyDictFriend[id]
                    privacyCache.set(userId, privacyDictFriend)
        smallThumbsCache = caches['smallThumbs']
        smallThumbsDictMany = smallThumbsCache.get_many(users)
        smallThumbsDictMy, smallThumbsDictFriend = smallThumbsDictMany.get(id, None), smallThumbsDictMany.get(userId, None)
        if smallThumbsDictMy is not None and smallThumbsDictFriend is not None:
            smallThumbsDictMyKeys, smallThumbsDictFriendKeys = smallThumbsDictMy.keys(), smallThumbsDictFriend.keys()
            smallThumbsDictMyIn, smallThumbsDictFriendIn = userId in smallThumbsDictMyKeys, id in smallThumbsDictFriendKeys
            if smallThumbsDictMyIn and smallThumbsDictFriendIn:
                del smallThumbsDictMy[userId], smallThumbsDictFriend[id]
                smallThumbsDictMyExact, smallThumbsDictFriendExact = len(smallThumbsDictMyKeys) > 0, len(smallThumbsDictFriendKeys) > 0
                if smallThumbsDictMyExact and smallThumbsDictFriendExact:
                    smallThumbsCache.set_many({id: smallThumbsDictMy, userId: smallThumbsDictFriend})
                else:
                    if smallThumbsDictMyExact:
                        smallThumbsCache.set(id, smallThumbsDictMy)
                        smallThumbsCache.delete(userId)
                    elif smallThumbsDictFriendExact:
                        smallThumbsCache.set(userId, smallThumbsDictFriend)
                        smallThumbsCache.delete(id)
                    else:
                        smallThumbsCache.delete_many(users)
            else:
                if smallThumbsDictMyIn:
                    del smallThumbsDictMy[userId]
                    if len(smallThumbsDictMyKeys) > 0:
                        smallThumbsCache.set(id, smallThumbsDictMy)
                    else:
                        smallThumbsCache.delete(id)
                elif smallThumbsDictFriendIn:
                    del smallThumbsDictFriend[id]
                    if len(smallThumbsDictFriendKeys) > 0:
                        smallThumbsCache.set(userId, smallThumbsDictFriend)
                    else:
                        smallThumbsCache.delete(userId)
        else:
            if smallThumbsDictMy is not None:
                smallThumbsDictMyKeys = smallThumbsDictMy.keys()
                if userId in smallThumbsDictMyKeys:
                    del smallThumbsDictMy[userId]
                    if len(smallThumbsDictMyKeys) > 0:
                        smallThumbsCache.set(id, smallThumbsDictMy)
                    else:
                        smallThumbsCache.delete(id)
            elif smallThumbsDictFriend is not None:
                smallThumbsDictFriendKeys = smallThumbsDictFriend.keys()
                if id in smallThumbsDictFriendKeys:
                    del smallThumbsDictFriend[id]
                    if len(smallThumbsDictFriendKeys) > 0:
                        smallThumbsCache.set(userId, smallThumbsDictFriend)
                    else:
                        smallThumbsCache.delete(userId)
        bigThumbsCache = caches['dialogThumbs']
        bigThumbsDictMany = bigThumbsCache.get_many(users)
        bigThumbsDictMy, bigThumbsDictFriend = bigThumbsDictMany.get(id, None), bigThumbsDictMany.get(userId, None)
        if bigThumbsDictMy is not None and bigThumbsDictFriend is not None:
            bigThumbsDictMyKeys, bigThumbsDictFriendKeys = bigThumbsDictMy.keys(), bigThumbsDictFriend.keys()
            bigThumbsDictMyIn, bigThumbsDictFriendIn = userId in bigThumbsDictMyKeys, id in bigThumbsDictFriendKeys
            if bigThumbsDictMyIn and bigThumbsDictFriendIn:
                del bigThumbsDictMy[userId], bigThumbsDictFriend[id]
                bigThumbsDictMyExact, bigThumbsDictFriendExact = len(bigThumbsDictMyKeys) > 0, len(bigThumbsDictFriendKeys) > 0
                if bigThumbsDictMyExact and bigThumbsDictFriendExact:
                    bigThumbsCache.set_many({id: bigThumbsDictMy, userId: bigThumbsDictFriend})
                else:
                    if bigThumbsDictMyExact:
                        bigThumbsCache.set(id, bigThumbsDictMy)
                        bigThumbsCache.delete(userId)
                    elif bigThumbsDictFriendExact:
                        bigThumbsCache.set(userId, bigThumbsDictFriend)
                        bigThumbsCache.delete(id)
                    else:
                        bigThumbsCache.delete_many(users)
            else:
                if bigThumbsDictMyIn:
                    del bigThumbsDictMy[userId]
                    if len(bigThumbsDictMyKeys) > 0:
                        bigThumbsCache.set(id, bigThumbsDictMy)
                    else:
                        bigThumbsCache.delete(id)
                elif bigThumbsDictFriendIn:
                    del bigThumbsDictFriend[id]
                    if len(bigThumbsDictFriendKeys) > 0:
                        bigThumbsCache.set(userId, bigThumbsDictFriend)
                    else:
                        bigThumbsCache.delete(userId)
        else:
            if bigThumbsDictMy is not None:
                bigThumbsDictMyKeys = bigThumbsDictMany.keys()
                if userId in bigThumbsDictMyKeys:
                    del bigThumbsDictMy[userId]
                    if len(bigThumbsDictMyKeys) > 0:
                        bigThumbsCache.set(id, bigThumbsDictMy)
                    else:
                        bigThumbsCache.delete(id)
            elif bigThumbsDictFriend is not None:
                bigThumbsDictFriendKeys = bigThumbsDictFriend.keys()
                if id in bigThumbsDictFriendKeys:
                    del bigThumbsDictFriend[id]
                    if len(bigThumbsDictFriendKeys) > 0:
                        bigThumbsCache.set(userId, bigThumbsDictFriend)
                    else:
                        bigThumbsCache.delete(userId)
        dialogListThumbsCache = caches['dialogListThumbs']
        dialogListThumbsDictMany = dialogListThumbsCache.get_many(users)
        dialogListThumbsDictMy, dialogListThumbsDictFriend = dialogListThumbsDictMany.get(id, None), dialogListThumbsDictMany.get(userId, None)
        if dialogListThumbsDictMy is not None and dialogListThumbsDictFriend is not None:
            dialogListThumbsDictMyKeys, dialogListThumbsDictFriendKeys = dialogListThumbsDictMy.keys(), dialogListThumbsDictFriend.keys()
            dialogListThumbsDictMyIn, dialogListThumbsDictFriendIn = userId in dialogListThumbsDictMyKeys, id in dialogListThumbsDictFriendKeys
            if dialogListThumbsDictMyIn and dialogListThumbsDictFriendIn:
                del dialogListThumbsDictMy[userId], dialogListThumbsDictFriend[id]
                dialogListThumbsDictMyExact, dialogListThumbsDictFriendExact = len(dialogListThumbsDictMyKeys) > 0, len(dialogListThumbsDictFriendKeys) > 0
                if dialogListThumbsDictMyExact and dialogListThumbsDictFriendExact:
                    dialogListThumbsCache.set_many({id: dialogListThumbsDictMy, userId: dialogListThumbsDictFriend})
                else:
                    if dialogListThumbsDictMyExact:
                        dialogListThumbsCache.set(id, dialogListThumbsDictMy)
                        dialogListThumbsCache.delete(userId)
                    elif dialogListThumbsDictFriendExact:
                        dialogListThumbsCache.set(userId, dialogListThumbsDictFriend)
                        dialogListThumbsCache.delete(id)
                    else:
                        dialogListThumbsCache.delete_many(users)
            else:
                if dialogListThumbsDictMyIn:
                    del dialogListThumbsDictMy[userId]
                    if len(dialogListThumbsDictMyKeys) > 0:
                        dialogListThumbsCache.set(id, dialogListThumbsDictMy)
                    else:
                        dialogListThumbsCache.delete(id)
                elif dialogListThumbsDictFriendIn:
                    del dialogListThumbsDictFriend[id]
                    if len(dialogListThumbsDictFriendKeys) > 0:
                        dialogListThumbsCache.set(userId, dialogListThumbsDictFriend)
                    else:
                        dialogListThumbsCache.delete(userId)
        else:
            if dialogListThumbsDictMy is not None:
                dialogListThumbsDictMyKeys = dialogListThumbsDictMy.keys()
                if userId in dialogListThumbsDictMyKeys:
                    del dialogListThumbsDictMy[userId]
                    if len(dialogListThumbsDictMyKeys) > 0:
                        dialogListThumbsCache.set(id, dialogListThumbsDictMy)
                    else:
                        dialogListThumbsCache.delete(id)
            elif dialogListThumbsDictFriend is not None:
                dialogListThumbsDictFriendKeys = dialogListThumbsDictFriend.keys()
                if id in dialogListThumbsDictFriendKeys:
                    del dialogListThumbsDictFriend[id]
                    if len(dialogListThumbsDictFriendKeys) > 0:
                        dialogListThumbsCache.set(userId, dialogListThumbsDictFriend)
                    else:
                        dialogListThumbsCache.delete(userId)
        messagesListThumbsCache = caches['messagesListThumbs']
        messagesListThumbsDictMany = messagesListThumbsCache.get_many(users)
        messagesListThumbsDictMy, messagesListThumbsDictFriend = messagesListThumbsDictMany.get(id, None), messagesListThumbsDictMany.get(userId, None)
        if messagesListThumbsDictMy is not None and messagesListThumbsDictFriend is not None:
            messagesListThumbsDictMyKeys, messagesListThumbsDictFriendKeys = messagesListThumbsDictMy.keys(), messagesListThumbsDictFriend.keys()
            messagesListThumbsDictMyIn, messagesListThumbsDictFriendIn = userId in messagesListThumbsDictMyKeys, id in messagesListThumbsDictFriendKeys
            if messagesListThumbsDictMyIn and messagesListThumbsDictFriendIn:
                del messagesListThumbsDictMy[userId], messagesListThumbsDictFriend[id]
                messagesListThumbsDictMyExact, messagesListThumbsDictFriendExact = len(messagesListThumbsDictMyKeys) > 0, len(messagesListThumbsDictFriendKeys) > 0
                if messagesListThumbsDictMyExact and messagesListThumbsDictFriendExact:
                    messagesListThumbsCache.set_many({id: messagesListThumbsDictMy, userId: messagesListThumbsDictFriend})
                else:
                    if messagesListThumbsDictMyExact:
                        messagesListThumbsCache.set(id, messagesListThumbsDictMy)
                        messagesListThumbsCache.delete(userId)
                    elif messagesListThumbsDictFriendExact:
                        messagesListThumbsCache.set(userId, messagesListThumbsDictFriend)
                        messagesListThumbsCache.delete(id)
                    else:
                        messagesListThumbsCache.delete_many(users)
            else:
                if messagesListThumbsDictMyIn:
                    del messagesListThumbsDictMy[userId]
                    if len(messagesListThumbsDictMyKeys) > 0:
                        messagesListThumbsCache.set(id, messagesListThumbsDictMy)
                    else:
                        messagesListThumbsCache.delete(id)
                elif messagesListThumbsDictFriendIn:
                    del messagesListThumbsDictFriend[id]
                    if len(messagesListThumbsDictFriendKeys) > 0:
                        messagesListThumbsCache.set(userId, messagesListThumbsDictFriend)
                    else:
                        messagesListThumbsCache.delete(userId)
        else:
            if messagesListThumbsDictMy is not None:
                messagesListThumbsDictMyKeys = messagesListThumbsDictMany.keys()
                if userId in messagesListThumbsDictMyKeys:
                    del messagesListThumbsDictMy[userId]
                    if len(messagesListThumbsDictMyKeys) > 0:
                        messagesListThumbsCache.set(id, messagesListThumbsDictMy)
                    else:
                        messagesListThumbsCache.delete(id)
            elif messagesListThumbsDictFriend is not None:
                messagesListThumbsDictFriendKeys = messagesListThumbsDictFriend.keys()
                if id in messagesListThumbsDictFriendKeys:
                    del messagesListThumbsDictFriend[id]
                    if len(messagesListThumbsDictFriendKeys) > 0:
                        messagesListThumbsCache.set(userId, messagesListThumbsDictFriend)
                    else:
                        messagesListThumbsCache.delete(userId)
        usersListThumbsCache = caches['usersListThumbs']
        usersListThumbsDictMany = usersListThumbsCache.get_many(users)
        usersListThumbsDictMy, usersListThumbsDictFriend = usersListThumbsDictMany.get(id, None), usersListThumbsDictMany.get(userId, None)
        if usersListThumbsDictMy is not None and usersListThumbsDictFriend is not None:
            usersListThumbsDictMyKeys, usersListThumbsDictFriendKeys = usersListThumbsDictMy.keys(), usersListThumbsDictFriend.keys()
            usersListThumbsDictMyIn, usersListThumbsDictFriendIn = userId in usersListThumbsDictMyKeys, id in usersListThumbsDictFriendKeys
            if usersListThumbsDictMyIn and usersListThumbsDictFriendIn:
                del usersListThumbsDictMy[userId], usersListThumbsDictFriend[id]
                usersListThumbsDictMyExact, usersListThumbsDictFriendExact = len(usersListThumbsDictMyKeys) > 0, len(usersListThumbsDictFriendKeys) > 0
                if usersListThumbsDictMyExact and usersListThumbsDictFriendExact:
                    usersListThumbsCache.set_many({id: usersListThumbsDictMy, userId: usersListThumbsDictFriend})
                else:
                    if usersListThumbsDictMyExact:
                        usersListThumbsCache.set(id, usersListThumbsDictMy)
                        usersListThumbsCache.delete(userId)
                    elif usersListThumbsDictFriendExact:
                        usersListThumbsCache.set({userId, usersListThumbsDictFriend})
                        usersListThumbsCache.delete(id)
                    else:
                        usersListThumbsCache.delete_many(users)
            else:
                if usersListThumbsDictMyIn:
                    del usersListThumbsDictMy[userId]
                    if len(usersListThumbsDictMyKeys) > 0:
                        usersListThumbsCache.set(id, usersListThumbsDictMy)
                    else:
                        usersListThumbsCache.delete(id)
                elif usersListThumbsDictFriendIn:
                    del usersListThumbsDictFriend[id]
                    if len(usersListThumbsDictFriendKeys) > 0:
                        usersListThumbsCache.set(userId, usersListThumbsDictFriend)
                    else:
                        usersListThumbsCache.delete(userId)
        else:
            if usersListThumbsDictMy is not None:
                usersListThumbsDictMyKeys = usersListThumbsDictMy.keys()
                if userId in usersListThumbsDictMyKeys:
                    del usersListThumbsDictMy[userId]
                    if len(usersListThumbsDictMyKeys) > 0:
                        usersListThumbsCache.set(id, usersListThumbsDictMy)
                    else:
                        usersListThumbsCache.delete(id)
            elif usersListThumbsDictFriend is not None:
                usersListThumbsDictFriendKeys = usersListThumbsDictFriend.keys()
                if id in usersListThumbsDictFriendKeys:
                    del usersListThumbsDictFriend[id]
                    if len(usersListThumbsDictFriendKeys) > 0:
                        usersListThumbsCache.set(userId, usersListThumbsDictFriend)
                    else:
                        usersListThumbsCache.delete(userId)
        if friendsCache is None:
            friendsCache = caches['friendsCache']
        friendFriends = friendsCache.get(userId, None)
        friendsCache.delete_many(users)
        myFriendsIdsSet.remove(userId)
        friendsIdsCache.set(id, myFriendsIdsSet)
        friendsIdsCache.delete(userId)
        friendShipStatusCache = caches['friendShipStatusCache']
        friendShipStatusDict = friendShipStatusCache.get_many(users)
        friendShipStatusMyDict = friendShipStatusDict.get(id, None)
        friendShipStatusFriendDict = friendShipStatusDict.get(userId, None)
        if friendShipStatusMyDict is not None and friendShipStatusFriendDict is not None:
            friendShipStatusMyDict[userId], friendShipStatusFriendDict[id] = False, False
            friendShipStatusCache.set_many({id: friendShipStatusMyDict, userId: friendShipStatusFriendDict})
        else:
            if friendShipStatusMyDict is not None:
                friendShipStatusMyDict[userId] = False
                friendShipStatusCache.set_many({id: friendShipStatusMyDict, userId: {id: False}})
            elif friendShipStatusFriendDict is not None:
                friendShipStatusFriendDict[id] = False
                friendShipStatusCache.set_many({id: {userId: False}, userId: friendShipStatusFriendDict})
        updatesCache = caches["newUpdates"]
        friendUpdates = updatesCache.get(userId, [])
        isExistsMyNotify = False
        isExistsFriendNotify = False
        myUpdates = updatesCache.get(id, tuple())
        for update in myUpdates:
            if update[0] == 'friend' and update[2] == userId:
                isExistsMyNotify = 'friend-%s-%s' % (update[1], userId)
                del myUpdates[myUpdates.index(update)]
                break
        for update in friendUpdates:
            if update[0] == 'friend' and update[2] == id:
                isExistsFriendNotify = 'friend-%s-%s' % (update[1], id)
                del friendUpdates[friendUpdates.index(update)]
                break
        friendUpdates.append(['friend', 'removed', id, datetime.now()])
        if isExistsMyNotify is not False:
            updatesCache.set_many({id: myUpdates, userId: friendUpdates})
            data = json.dumps((
                {
                    'method': 'doReloadIfCurrentLink',
                    'link': reverse("profile", kwargs = {'currentUser': friendUserInstance.profileUrl})
                }, {
                    'method': 'removeNotifyEvent',
                    'reduceBaseValueBy': 1,
                    'value': isExistsMyNotify
                }
            ))
        else:
            updatesCache.set(userId, friendUpdates)
            data = json.dumps({
                'method': 'doReloadIfCurrentLink',
                'link': reverse("profile", kwargs = {'currentUser': friendUserInstance.profileUrl})
                })
        if isExistsFriendNotify is not False:
            async_to_sync(layer)(userId, {"type": "sendToClient", "data": {"item": json.dumps((
                {
                    'method': 'doReloadIfCurrentLink',
                    'link': reverse("profile", kwargs = {'currentUser': userInstance.profileUrl})
                }, {
                    'method': 'removeNotifyEvent',
                    'value': isExistsFriendNotify
                    }
            ))}})
        else:
            async_to_sync(layer)(userId, {"type": "sendToClient", "data": {"item": json.dumps((
                {
                    'method': 'doReloadIfCurrentLink',
                    'link': reverse("profile", kwargs = {'currentUser': userInstance.profileUrl})
                }, {
                    'method': 'simpleNotify',
                    'textKeyName': 'friendRemoved',
                    'eventType': 'notify',
                    'profileLink': reverse("profile", kwargs = {'currentUser': userInstance.profileUrl}),
                    'username': userInstance.username,
                    'notifyCounterChange': {
                        'method': 'incrementNotifyValue',
                        'baseValue': 1,
                        'multiValue': 1
                    }
                }
            ))}})
        async_to_sync(layer)(userId, {"type": "sendToClient", "data": {"item": data}})
        if request.is_ajax():
            return JsonResponse({'data': data})
        return redirect("profile", friendUserInstance.profileUrl)
    else:
        if myFriendsIdsSet is None:
            friendsIdsCache.set(userId, set(user.id for user in friends))
    if request.is_ajax():
        return JsonResponse({'data': None})
    return redirect("profile", friendUserInstance.profileUrl)