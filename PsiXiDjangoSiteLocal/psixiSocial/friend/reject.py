import json
from datetime import datetime
from django.contrib.auth import logout
from django.views.decorators.http import require_POST
from psixiSocial.login import login_decorator
from psixiSocial.models import mainUserProfile, userProfilePrivacy
from psixiSocial.privacyIdentifier import checkPrivacy
from psixiSocial.translators import profileTimeStatusText, singleElementGetter
from django.http import JsonResponse
from django.shortcuts import redirect
from django.core.cache import caches
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.urls import reverse

@login_decorator
@require_POST
def rejectFriend(request, userId):
    requestInstance = request.user
    requestUserId = str(requestInstance.id)
    try:
        myUserInstance = mainUserProfile.objects.get(id__exact = requestUserId)
    except:
        logout(request)
        return redirect(reverse('login'))
    try:
        friendUserInstance = mainUserProfile.objects.get(id__exact = userId)
    except mainUserProfile.DoesNotExist:
        pass # return
    notifyItemIsExists = False
    isExists = False
    friendshipCache = caches["friendshipRequests"]
    friendRequests = friendshipCache.get(requestUserId, [])
    for friend in friendRequests:
        if friend[0] == userId:
            isExists = True
            friendRequestIndex = friendRequests.index(friend)
            break
    if isExists:
        updatesCache = caches["newUpdates"]
        userUpdates = updatesCache.get(userId, [])
        userUpdates.append(['friend', 'rejected', requestUserId, datetime.now()])
        myUpdates = updatesCache.get(requestUserId, [])
        for update in myUpdates:
            if update[2] == userId:
                if update[0] == "friend" and update[2] == userId:
                    state = update[1]
                    if state == "new":
                        notifyItemIsExists = True
                        del myUpdates[myUpdates.index(update)]
                        break
                    elif state == "hidden":
                        del myUpdates[myUpdates.index(update)]
                        break
        if bool(myUpdates):
            updatesCache.set_many({requestUserId: myUpdates, userId: userUpdates})
        else:
            updatesCache.delete(requestUserId)
            updatesCache.set(userId, userUpdates)
        friendShipStatusCache = caches['friendShipStatusCache']
        friendShipStatusMany = friendShipStatusCache.get_many({requestUserId, userId})
        friendShipStatusMyDict = friendShipStatusMany.get(requestUserId, None)
        friendShipStatusFriendDict = friendShipStatusMany.get(userId, None)
        if friendShipStatusMyDict is not None and friendShipStatusFriendDict is not None:
            friendShipStatusMyDict[userId] = False
            friendShipStatusFriendDict[requestUserId] = False
            friendShipStatusCache.set_many({requestUserId: friendShipStatusMyDict, userId: friendShipStatusFriendDict})
        else:
            if friendShipStatusMyDict is not None:
                friendShipStatusMyDict[userId] = False
                friendShipStatusCache.set(requestUserId, friendShipStatusMyDict)
            elif friendShipStatusFriendDict is not None:
                friendShipStatusFriendDict[requestUserId] = False
                friendShipStatusCache.set(userId, friendShipStatusFriendDict)
        privacyCache = caches['privacyCache']
        privacyDict = privacyCache.get(userId, None)
        if privacyDict is not None:
            privacyDictKeys = privacyDict.keys()
            if requestUserId in privacyDictKeys:
                del privacyDict[requestUserId]
                if len(privacyDictKeys) > 0:
                    privacyCache.set(userId, privacyDict)
                else:
                    privacyCache.delete(userId)
        del friendRequests[friendRequestIndex]
        if len(friendRequests) > 0:
            friendshipCache.set(requestUserId, friendRequests)
        else:
            friendshipCache.delete(requestUserId)
        smallThumbsCache = caches['smallThumbs']
        smallThumbsDict = smallThumbsCache.get(userId, None)
        if smallThumbsDict is not None:
            smallThumbsDictKeys = smallThumbsDict.keys()
            if requestUserId in smallThumbsDictKeys:
                del smallThumbsDict[requestUserId]
                if len(smallThumbsDictKeys) > 0:
                    smallThumbsCache.set(userId, thumbsDict)
                else:
                    smallThumbsCache.delete(userId)
        bigThumbsCache = caches['dialogThumbs']
        bigThumbsDict = bigThumbsCache.get(userId, None)
        if bigThumbsDict is not None:
            bigThumbsDictKeys = bigThumbsDict.keys()
            if requestUserId in bigThumbsDictKeys:
                del bigThumbsDict[requestUserId]
                if len(bigThumbsDictKeys) > 0:
                    bigThumbsCache.set(userId, bigThumbsDict)
                else:
                    bigThumbsCache.delete(userId)
        dialogListThumbsCache = caches['dialogListThumbs']
        dialogListThumbsDict = dialogListThumbsCache.get(userId, None)
        if dialogListThumbsDict is not None:
            dialogListThumbsDictKeys = dialogListThumbsDict.keys()
            if requestUserId in dialogListThumbsDictKeys:
                del dialogListThumbsDict[requestUserId]
                if len(dialogListThumbsDictKeys) > 0:
                    dialogListThumbsCache.set(userId, dialogListThumbsDict)
                else:
                    dialogListThumbsCache.delete(userId)
        messagesListThumbsCache = caches['messagesListThumbs']
        messagesListThumbsDict = messagesListThumbsCache.get(userId, None)
        if messagesListThumbsDict is not None:
            messagesListThumbsDictKeys = messagesListThumbsDict.keys()
            if requestUserId in messagesListThumbsDictKeys:
                del messagesListThumbsDict[requestUserId]
                if len(messagesListThumbsDictKeys) > 0:
                    messagesListThumbsCache.set(userId, messagesListThumbsDict)
                else:
                    messagesListThumbsCache.delete(userId)
        usersListThumbsCache = caches['usersListThumbs']
        usersListThumbsDict = usersListThumbsCache.get(userId, None)
        if usersListThumbsDict is not None:
            usersListThumbsDictKeys = usersListThumbsDict.keys()
            if requestUserId in usersListThumbsDictKeys:
                del usersListThumbsDict[requestUserId]
                if len(usersListThumbsDictKeys) > 0:
                    usersListThumbsCache.set(userId, usersListThumbsDict)
                else:
                    usersListThumbsCache.delete(userId)
        requestInstanceSlug = requestInstance.profileUrl
        layer = get_channel_layer().group_send
        async_to_sync(layer)(userId, {"type": "sendToClient", "data": {"item": json.dumps((
            {
                'method': 'addFriend',
                'eventType': 'fragment',
                'class': 'profile-actions',
                'path': reverse("profile", kwargs = {'currentUser': requestInstanceSlug}),
                'addFriendLink': reverse("addFriendRequest", kwargs = {'userId': requestUserId})
            }, {
                'method': 'simpleNotify',
                'textKeyName': 'friendRejected',
                'eventType': 'notify',
                'profileLink': reverse("profile", kwargs = {'currentUser': requestInstanceSlug}),
                'value': 'friend-rejected-%s' % requestUserId,
                'username': requestInstance.username,
                'forgetLink': reverse("deleteNotifyEvent", kwargs = {'type': 'friend', 'state': 'rejected', 'senderId': requestUserId}),
                'notifyCounterChange': {
                    'method': 'incrementNotifyValue',
                    'baseValue': 1,
                    'multiValue': 1
                    }
            }
        ))}})
        toUserSlug = friendUserInstance.profileUrl
        if notifyItemIsExists:
            data = json.dumps((
                {
                    'method': 'doReloadIfCurrentLink',
                    'link': reverse("profile", kwargs = {'currentUser': toUserSlug}),
                }, {
                    'method': 'removeNotifyEvent',
                    'reduceBaseValueBy': 1,
                    'value': 'friend-new-%s' % userId
                    }
            ))
        else:
            data = json.dumps((
                {
                    'method': 'doReloadIfCurrentLink',
                    'link': reverse("profile", kwargs = {'currentUser': toUserSlug})
                }
            ))
        async_to_sync(layer)(requestUserId, {"type": "sendToClient", "data": {"item": data}})
        if request.is_ajax():
            return JsonResponse({'data': data})
        return redirect("profile", friendUserInstance.profileUrl)
    if request.is_ajax():
        return JsonResponse({'data': None})
    return redirect("profile", friendUserInstance.profileUrl)