import json, copy
from datetime import datetime
from django.contrib.auth import logout
from django.views.decorators.http import require_POST
from psixiSocial.login import login_decorator
from django.db import transaction, Error
from psixiSocial.models import mainUserProfile, userProfilePrivacy, userUpdatesHistory
from psixiSocial.profile.items import getUserFriendsIm
from psixiSocial.privacyIdentifier import checkPrivacy
from django.http import JsonResponse
from django.shortcuts import redirect
from django.core.cache import caches
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.urls import reverse

@login_decorator
@require_POST
def addFriend(request, userId):
    userInstance = request.user
    try:
        friendUserInstance = mainUserProfile.objects.get(id__exact = userId)
    except mainUserProfile.DoesNotExist:
        if request.is_ajax():
            return JsonResponse({'data': None})
        return redirect("profile", userInstance.profileUrl)
    id = str(userInstance.id)
    try:
        myUserInstance = mainUserProfile.objects.get(id__exact = id)
    except mainUserProfile.DoesNotExist:
        logout(request)
        return redirect(reverse('login'))
    isExists = False
    friendShipRequestsCache = caches["friendshipRequests"]
    friendRequests = friendShipRequestsCache.get(id, [])
    for friend in friendRequests:
        if friend[0] == userId:
            isExists = True
            friendRequestIndex = friendRequests.index(friend)
            break
    if isExists:
        isExistsNotifyItemCacheShowed = False
        myUserFriends = myUserInstance.friends
        friendUserFriends = friendUserInstance.friends
        newUpdateInHistoryMy = userUpdatesHistory.objects.create(owner = myUserInstance, destination = friendUserInstance)
        newUpdateInHistoryMy.name = 'friend'
        newUpdateInHistoryMy.state = 'confirmedByMe'
        newUpdateInHistoryFriend = userUpdatesHistory.objects.create(owner = friendUserInstance, destination = myUserInstance)
        newUpdateInHistoryFriend.name = 'friend'
        newUpdateInHistoryFriend.state = 'confirmedByFriend'
        friendFriendsAll, myFriendsAll = friendUserFriends.all(), myUserFriends.all()
        friendFriendsOnlineIdSet = set(str(user.id) for user in friendFriendsAll)
        myFriendsOnlineIdSet = set(str(user.id) for user in myFriendsAll)
        myUserFriends.add(friendUserInstance)
        friendUserFriends.add(myUserInstance)
        try:
            with transaction.atomic():
                myUserInstance.save()
                friendUserInstance.save()
                newUpdateInHistoryMy.save()
                newUpdateInHistoryFriend.save()
        except Error:
            if request.is_ajax():
                return JsonResponse({'data': None})
            return redirect("profile", friendUserInstance.profileUrl)
        updatesCache = caches["newUpdates"]
        newFriendUpdates = updatesCache.get(userId, [])
        newFriendUpdates.append(['friend', 'confirmed', id, datetime.now()])
        myUpdates = updatesCache.get(id, [])
        for update in myUpdates:
            if update[0] == 'friend' and update[2] == userId:
                state = update[1]
                if state == 'new':
                    del myUpdates[myUpdates.index(update)]
                    isExistsNotifyItemCacheShowed = True
                    break
                elif state == 'hidden':
                    del myUpdates[myUpdates.index(update)]
                    break
        if len(myUpdates) > 0:
            updatesCache.set_many({id: myUpdates, userId: newFriendUpdates})
        else:
            updatesCache.delete(id)
            updatesCache.set(userId, newFriendUpdates)
        del friendRequests[friendRequestIndex]
        if len(friendRequests) > 0:
            friendShipRequestsCache.set(id, friendRequests)
        else:
            friendShipRequestsCache.delete(id)
        privacyCache = caches['privacyCache']
        privacyDict = privacyCache.get(userId, None)
        if privacyDict is not None:
            privacyDictKeys = privacyDict.keys()
            if id in privacyDictKeys:
                del privacyDict[id]
                if len(privacyDictKeys) > 0:
                    privacyCache.set(userId, privacyDict)
                else:
                    privacyCache.delete(userId)
        friendShipCache = caches['friendShipStatusCache']
        friendShipDict = friendShipCache.get_many({id, userId})
        friendShipMyDict = friendShipDict.get(id, None)
        friendShipFriendDict = friendShipDict.get(userId, None)
        if friendShipMyDict is not None and friendShipFriendDict is not None:
            friendShipMyDict[userId] = True
            friendShipFriendDict[id] = True
            friendShipCache.set_many({id: friendShipMyDict, userId: friendShipFriendDict})
        else:
            if friendShipMyDict is not None:
                friendShipMyDict[userId] = True
                friendShipCache.set(id, friendShipMyDict)
            elif friendShipFriendDict is not None:
                friendShipFriendDict[id] = True
                friendShipCache.set(userId, friendShipFriendDict)
        caches['friendsCache'].delete_many({id, userId})
        friendsIdCache = caches['friendsIdCache']
        friendIdsMy = friendsIdCache.get(id, None)
        friendIdsFriend = friendsIdCache.get(userId, None)
        if friendIdsMy is None:
            friendIdsMy = copy.copy(myFriendsOnlineIdSet)
        friendIdsMy.add(userId)
        if friendIdsFriend is None:
            friendIdsFriend = copy.copy(friendFriendsOnlineIdSet)
        friendIdsFriend.add(id)
        friendsIdCache.set_many({id: friendIdsMy, userId: friendIdsFriend})
        ###
        smallThumbsCache = caches['smallThumbs']
        smallThumbsDict = smallThumbsCache.get(userId, None)
        if smallThumbsDict is not None:
            smallThumbsDictKeys = smallThumbsDict.keys()
            if id in smallThumbsDictKeys:
                del smallThumbsDict[id]
                if len(smallThumbsDictKeys) > 0:
                    smallThumbsCache.set(userId, thumbsDict)
                else:
                    smallThumbsCache.delete(userId)
        bigThumbsCache = caches['dialogThumbs']
        bigThumbsDict = bigThumbsCache.get(userId, None)
        if bigThumbsDict is not None:
            bigThumbsDictKeys = bigThumbsDict.keys()
            if id in bigThumbsDictKeys:
                del bigThumbsDict[id]
                if len(bigThumbsDictKeys) > 0:
                    bigThumbsCache.set(userId, bigThumbsDict)
                else:
                    bigThumbsCache.delete(userId)
        dialogListThumbsCache = caches['dialogListThumbs']
        dialogListThumbsDict = dialogListThumbsCache.get(userId, None)
        if dialogListThumbsDict is not None:
            dialogListThumbsDictKeys = dialogListThumbsDict.keys()
            if id in dialogListThumbsDictKeys:
                del dialogListThumbsDict[id]
                if len(dialogListThumbsDictKeys) > 0:
                    dialogListThumbsCache.set(userId, dialogListThumbsDict)
                else:
                    dialogListThumbsCache.delete(userId)
        messagesListThumbsCache = caches['messagesListThumbs']
        messagesListThumbsDict = messagesListThumbsCache.get(userId, None)
        if messagesListThumbsDict is not None:
            messagesListThumbsDictKeys = messagesListThumbsDict.keys()
            if id in messagesListThumbsDictKeys:
                del messagesListThumbsDict[id]
                if len(messagesListThumbsDictKeys) > 0:
                    messagesListThumbsCache.set(userId, messagesListThumbsDict)
                else:
                    messagesListThumbsCache.delete(userId)
        usersListThumbsCache = caches['usersListThumbs']
        usersListThumbsDict = usersListThumbsCache.get(userId, None)
        if usersListThumbsDict is not None:
            usersListThumbsDictKeys = usersListThumbsDict.keys()
            if id in usersListThumbsDictKeys:
                del usersListThumbsDict[id]
                if len(usersListThumbsDictKeys) > 0:
                    usersListThumbsCache.set(userId, usersListThumbsDict)
                else:
                    usersListThumbsCache.delete(userId)
        myPushFriendMethodDict = {
            'method': 'getAddedFriend',
            'userId': userId,
            'id': id
            }
        friendPushFriendMethodDict = {
            'method': 'getAddedFriend',
            'userId': id,
            'id': userId
            }
        if isExistsNotifyItemCacheShowed:
            data = json.dumps(({
                'method': 'removeFriend',
                'eventType': 'fragment',
                'class': 'profile-actions',
                'path': reverse("profile", kwargs = {'currentUser': friendUserInstance.profileUrl}),
                'removeFriendLink': reverse("removeFriend", kwargs = {'userId': userId})
            }, {
                'method': 'removeNotifyEvent',
                'reduceBaseValueBy': 1,
                'value': 'friend-new-%s' % userId
            }, myPushFriendMethodDict, friendPushFriendMethodDict))
        else:
            data = json.dumps(({
                'method': 'removeFriend',
                'eventType': 'fragment',
                'class': 'profile-actions',
                'path': reverse("profile", kwargs = {'currentUser': friendUserInstance.profileUrl}),
                'removeFriendLink': reverse("removeFriend", kwargs = {'userId': userId})
            }, myPushFriendMethodDict, friendPushFriendMethodDict))
        userInstanceProfileUrl = reverse("profile", kwargs = {'currentUser': userInstance.profileUrl})
        async_to_sync(layer)(userId, {"type": "sendToClient", "data": {"item": json.dumps((
            {
                'method': 'simpleNotify',
                'textKeyName': 'friendConfirmed',
                'eventType': 'notify',
                'username': userInstance.username,
                'profileLink': userInstanceProfileUrl,
                'notifyCounterChange': {
                    'method': 'incrementNotifyValue',
                    'baseValue': 1,
                    'multiValue': 1
                    }
                }, {
                    'method': 'doReloadIfCurrentLink',
                    'link': userInstanceProfileUrl
                    }, myPushFriendMethodDict, friendPushFriendMethodDict
                ))}})
        async_to_sync(layer)(id, {"type": "sendToClient", "data": {"item": data}})
        # Send to other users
        '''privacys = userProfilePrivacy.objects.filter(user__in = {userInstance, friendUserInstance})
        myUserPrivacy, friendUserPrivacy = privacys.get(user__exact = userInstance), privacys.get(user__exact = friendUserInstance)
        myPrivacyViewProfile, friendPrivacyViewProfile = myUserPrivacy.viewProfile < 7, friendUserPrivacy.viewProfile < 7
        if myPrivacyViewProfile and friendPrivacyViewProfile:
            myPrivacyViewFriends, friendPrivacyViewFriends = myUserPrivacy.viewProfileFriends, friendUserPrivacy.viewProfileFriends
            if myPrivacyViewFriends == friendPrivacyViewFriends and myPrivacyViewFriends < 2:
                if myPrivacyViewFriends == 0:
                    manyFriendsData = {'text': json.dumps((myPushFriendMethodDict, friendPushFriendMethodDict,))}
                    Group('guests').send(manyFriendsData)
                    Group('users').send(manyFriendsData)
                elif myPrivacyViewFriends == 1:
                    Group('users').send({'text': json.dumps((myPushFriendMethodDict, friendPushFriendMethodDict,))})
            else:
                if myPrivacyViewFriends == 0:
                    myFriendData = {'text': json.dumps(myPushFriendMethodDict)}
                    Group('guests').send(myFriendData)
                    Group('users').send(myFriendData)
                elif myPrivacyViewFriends == 1:
                    Group('users').send({'text': json.dumps(myPushFriendMethodDict)})
                elif myPrivacyViewFriends == 2:
                    myFriendData = {'text': json.dumps(myPushFriendMethodDict)}
                    for user_id in myFriendsOnlineIdSet:
                        Group('service_%s' % user_id).send(myFriendData)
                if friendPrivacyViewFriends == 0:
                    friendFriendsData = {'text': json.dumps(friendPushFriendMethodDict)}
                    Group('guests').send(friendFriendsData)
                    Group('users').send(friendFriendsData)
                elif friendPrivacyViewFriends == 1:
                    Group('users').send({'text': json.dumps(friendPushFriendMethodDict)})
                elif friendPrivacyViewFriends == 2:
                    friendFriendsData = {'text': json.dumps(friendPushFriendMethodDict)}
                    for user_id in friendFriendsOnlineIdSet:
                        Group('service_%s' % user_id).send(friendFriendsData)
        else:
            if myPrivacyViewProfile:
                myPrivacyViewFriends = friendUserPrivacy.viewProfileFriends
                if myPrivacyViewFriends == 0:
                    myFriendData = {'text': json.dumps(myPushFriendMethodDict)}
                    Group('guests').send(myFriendData)
                    Group('users').send(myFriendData)
                elif myPrivacyViewFriends == 1:
                    Group('users').send({'text': json.dumps(myPushFriendMethodDict)})
                elif myPrivacyViewFriends == 2:
                    myFriendData = {'text': json.dumps(myPushFriendMethodDict)}
                    for user_id in myFriendsOnlineIdSet:
                        Group('service_%s' % user_id).send(myFriendData)
            elif friendPrivacyViewProfile:
                friendPrivacyViewFriends = friendUserPrivacy.viewProfileFriends
                if friendPrivacyViewFriends == 0:
                    friendFriendsData = {'text': json.dumps(friendPushFriendMethodDict)}
                    Group('guests').send(friendFriendsData)
                    Group('users').send(friendFriendsData)
                elif friendPrivacyViewFriends == 1:
                    Group('users').send({'text': json.dumps(friendPushFriendMethodDict)})
                elif friendPrivacyViewFriends == 2:
                    friendFriendsData = {'text': json.dumps(friendPushFriendMethodDict)}
                    for user_id in friendFriendsOnlineIdSet:
                        Group('service_%s' % user_id).send(friendFriendsData)'''
        if request.is_ajax():
            return JsonResponse({'data': data})
        return redirect("profile", friendUserInstance.profileUrl)
    # Не найден элемент запроса на добавление в друзья в кеше!
    if request.is_ajax():
        return JsonResponse({'data': None})
    return redirect("profile", friendUserInstance.profileUrl)