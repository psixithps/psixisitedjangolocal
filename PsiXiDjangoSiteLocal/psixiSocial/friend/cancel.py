import json
from datetime import datetime
from django.contrib.auth import logout
from django.views.decorators.http import require_POST
from psixiSocial.login import login_decorator
from psixiSocial.models import mainUserProfile, userProfilePrivacy
from psixiSocial.privacyIdentifier import checkPrivacy
from psixiSocial.translators import profileTimeStatusText, singleElementGetter
from django.http import JsonResponse
from django.shortcuts import redirect
from django.core.cache import caches
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.urls import reverse

@login_decorator
@require_POST
def cancelAddFriendRequest(request, userId):
    requestUser = request.user
    requestUserId = str(requestUser.id)
    try:
        mainUserProfile.objects.get(id__exact = requestUserId)
    except:
        logout(request)
        return redirect(reverse('login'))
    try:
        friendUserInstance = mainUserProfile.objects.get(id__exact = userId)
    except:
        pass # return
    isExists = False
    friendshipCache = caches["friendshipRequests"]
    friendRequests = friendshipCache.get(userId, tuple())
    for friend in friendRequests:
        if friend[0] == requestUserId:
            isExists = True
            friendRequestIndex = friendRequests.index(friend)
            break
    if isExists:
        friendShipShortCache = caches['friendShipStatusCache']
        friendShipShortDict = friendShipShortCache.get_many({requestUserId, userId})
        friendShipShort = friendShipShortDict.get(requestUserId, None)
        friendShipShortFriend = friendShipShortDict.get(userId, None)
        if friendShipShort is not None and friendShipShortFriend is not None:
            friendShipShort[userId] = False
            friendShipShortFriend[requestUserId] = False
            friendShipShortCache.set_many({requestUserId: friendShipShort, userId: friendShipShortFriend})
        else:
            if friendShipShort is not None:
                friendShipShort[userId] = False
                friendShipShortCache.set(requestUserId, friendShipShort)
            elif friendShipShortFriend is not None:
                friendShipShortFriend[requestUserId] = False
                friendShipShortCache.set(userId, friendShipShortFriend)
        updatesCache = caches["newUpdates"]
        updates = updatesCache.get(userId, [])
        for update in updates:
            if update[0] == 'friend' and update[2] == requestUserId:
                isExists2 = True
                del updates[updates.index(update)]
                break
        if isExists2:
            if bool(updates):
                updatesCache.set(userId, updates)
            else:
                updatesCache.delete(userId)
        del friendRequests[friendRequestIndex]
        if bool(friendRequests):
            friendshipCache.set(userId, friendRequests)
        else:
            friendshipCache.delete(userId)
        privacyCache = caches['privacyCache']
        privacyDict = privacyCache.get(requestUserId, None)
        if privacyDict is not None:
            if userId in privacyDict.keys():
                del privacyDict[userId]
                privacyCache.set(requestUserId, privacyDict)
        smallThumbsCache = caches['smallThumbs']
        smallThumbsDict = smallThumbsCache.get(requestUserId, None)
        if smallThumbsDict is not None:
            smallThumbsDictKeys = smallThumbsDict.keys()
            if userId in smallThumbsDictKeys:
                del smallThumbsDict[userId]
                if len(smallThumbsDictKeys) > 0:
                    smallThumbsCache.set(requestUserId, thumbsDict)
                else:
                    smallThumbsCache.delete(requestUserId)
        bigThumbsCache = caches['dialogThumbs']
        bigThumbsDict = bigThumbsCache.get(requestUserId, None)
        if bigThumbsDict is not None:
            bigThumbsDictKeys = bigThumbsDict.keys()
            if userId in bigThumbsDictKeys:
                del bigThumbsDict[userId]
                if len(bigThumbsDictKeys) > 0:
                    bigThumbsCache.set(requestUserId, bigThumbsDict)
                else:
                    bigThumbsCache.delete(requestUserId)
        dialogListThumbsCache = caches['dialogListThumbs']
        dialogListThumbsDict = dialogListThumbsCache.get(requestUserId, None)
        if dialogListThumbsDict is not None:
            dialogListThumbsDictKeys = dialogListThumbsDict.keys()
            if userId in dialogListThumbsDictKeys:
                del dialogListThumbsDict[userId]
                if len(dialogListThumbsDictKeys) > 0:
                    dialogListThumbsCache.set(requestUserId, dialogListThumbsDict)
                else:
                    dialogListThumbsCache.delete(requestUserId)
        messagesListThumbsCache = caches['messagesListThumbs']
        messagesListThumbsDict = messagesListThumbsCache.get(requestUserId, None)
        if messagesListThumbsDict is not None:
            messagesListThumbsDictKeys = messagesListThumbsDict.keys()
            if userId in messagesListThumbsDictKeys:
                del messagesListThumbsDict[userId]
                if len(messagesListThumbsDictKeys) > 0:
                    messagesListThumbsCache.set(requestUserId, messagesListThumbsDict)
                else:
                    messagesListThumbsCache.delete(requestUserId)
        usersListThumbsCache = caches['usersListThumbs']
        usersListThumbsDict = usersListThumbsCache.get(requestUserId, None)
        if usersListThumbsDict is not None:
            usersListThumbsDictKeys = usersListThumbsDict.keys()
            if userId in usersListThumbsDictKeys:
                del usersListThumbsDict[userId]
                if len(usersListThumbsDictKeys) > 0:
                    usersListThumbsCache.set(requestUserId, usersListThumbsDict)
                else:
                    usersListThumbsCache.delete(requestUserId)
        async_to_sync(layer)(userId, {"type": "sendToClient", "data": {"item": json.dumps((
            {
                'method': 'doReloadIfCurrentLink',
                'link': reverse("profile", kwargs = {'currentUser': requestUser.profileUrl})
            }, {
                'method': 'removeNotifyEvent',
                'value': 'friend-new-%s' % requestUserId,
                'reduceBaseValueBy': 1
            }
        ))}})
        data = json.dumps((
            {
                'method': 'addFriend',
                'eventType': 'fragment',
                'class': 'profile-actions',
                'path': reverse("profile", kwargs = {'currentUser': friendUserInstance.profileUrl}),
                'addFriendLink': reverse("addFriendRequest", kwargs = {'userId': userId})
            },
        ))
        async_to_sync(layer)(userId, {"type": "sendToClient", "data": {"item": data}})
        if request.is_ajax():
            return JsonResponse({'data': data})
        return redirect("profile", friendUserInstance.profileUrl)
    if request.is_ajax():
        return JsonResponse({'data': None})
    return redirect("profile", friendUserInstance.profileUrl)