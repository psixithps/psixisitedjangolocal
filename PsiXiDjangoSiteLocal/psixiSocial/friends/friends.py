import json, copy
from datetime import datetime
from django.conf import settings
from django.core.cache import caches
from psixiSocial.models import mainUserProfile, userProfilePrivacy
from psixiSocial.translators import usersListText, singleElementGetter, profileTimeStatusText
from psixiSocial.privacyIdentifier import checkPrivacy
from functools import lru_cache
from django.shortcuts import render, redirect
from django.template.loader import render_to_string as rts
from collections import OrderedDict
from django.urls import reverse
from django.http import JsonResponse
from django.core.paginator import Paginator
from psixiSocial.friends import items, itemsXHR

class cachedFragments():
    def __init__(self, *args, **kwargs):
        self.username = kwargs.pop('username', None)
        self.slug = kwargs.pop('slug', None)
        self.currentPath = kwargs.pop('path', None)

    @property
    @lru_cache(maxsize = 16)
    def menuType1(self):
        menu = OrderedDict({
            0: {
                'textname': 'Мои друзья',
                0: {
                    'textname': 'Все',
                    'url': reverse('friends', kwargs = {'user': 'my', 'category': 'all', 'sep': '',  'page': ''})
                    },
                1: {
                    'textname': 'Сейчас в сети',
                    'url': reverse('friends', kwargs = {'user': 'my', 'category': 'online', 'sep': '',  'page': ''})
                    }
                },
            1: {
                'textname': 'Мой профиль',
                'url': reverse('profile', kwargs = {'currentUser': 'im'}),
                0: {
                    'textname': 'Записи в блоге'
                    },
                1: {
                    'textname': 'Записи в libraryTHPS'
                    },
                2: {
                    'textname': 'Редактировать',
                    0: {
                        'textname': 'Основные',
                        'url': reverse('profileEditUrl', kwargs = {'editType': 'main'})
                        },
                    1: {
                        'textname': 'Дополнительно',
                        'url': reverse('profileEditUrl', kwargs = {'editType': 'detail'})
                        },
                    2: {
                        'textname': 'Обратная связь',
                        'url': reverse('profileEditUrl', kwargs = {'editType': 'contacts'})
                        },
                    3: {
                        'textname': 'Настройки приватности',
                        'url': reverse('editPrivacyUrl', kwargs = {'editPlace': 'profile'}),
                        0: {
                            'textname': 'Права для исключительных пользователей',
                            'url': reverse('editPrivacyExclusiveUrl', kwargs = {'editPlace': 'profile'})
                            }
                        },
                    4: {
                        'textname': 'THPS профиль',
                        'url': reverse('profileEditUrl', kwargs = {'editType': 'thps'})
                        }
                    }
                },
            2: {
                'textname': 'Пользователи',
                0: {
                    'textname': 'Все',
                    'url': reverse('users', kwargs = {'category': 'all', 'sep': '', 'page': ''})
                    },
                1: {
                    'textname': 'Сейчас в сети',
                    'url': reverse('users', kwargs = {'category': 'online', 'sep': '', 'page': ''})
                    }
                },
            3: {
                'textname': 'Выход',
                'url': reverse("logOut")
                }
            })
        return menu

    @property
    @lru_cache(maxsize = 16)
    def menuType1Serialized(self):
        menuDumps = self.menuType1
        if isinstance(menuDumps, OrderedDict):
            return json.dumps(menuDumps)
        return json.dumps(menuDumps())

    @property
    @lru_cache(maxsize = 16)
    def menuType2(self):
        menu = OrderedDict({
            0: {
                'textname': 'Друзья %s' % self.username,
                0: {
                    'textname': 'Все',
                    'url': reverse('friends', kwargs = {'user': self.slug, 'category': 'all', 'sep': '', 'page': ''})
                    },
                1: {
                    'textname': 'Сейчас в сети',
                    'url': reverse('friends', kwargs = {'user': self.slug, 'category': 'online', 'sep': '', 'page': ''})
                    }
                },
            1: {
                'textname': 'Профиль %s' % self.username,
                'url': reverse('profile', kwargs = {'currentUser': self.slug}),
                0: {
                    'textname': 'Записи в блоге'
                    },
                1: {
                    'textname': 'Записи в libraryTHPS'
                    }
                },
            2: {
                'textname': 'Пользователи',
                0: {
                    'textname': 'Все',
                    'url': reverse('users', kwargs = {'category': 'all', 'sep': '', 'page': ''})
                    },
                1: {
                    'textname': 'Сейчас в сети',
                    'url': reverse('users', kwargs = {'category': 'online', 'sep': '', 'page': ''})
                    }
                },
            3: {
                'textname': 'Выход',
                'url': reverse("logOut")
                }
            })
        return menu

    @property
    @lru_cache(maxsize = 16)
    def menuType2Serialized(self):
        menuDumps = self.menuType2
        if isinstance(menuDumps, OrderedDict):
            return json.dumps(menuDumps)
        return json.dumps(menuDumps())

    @property
    @lru_cache(maxsize = 16)
    def menuGuest(self):
        menu = OrderedDict({
            0: {
                "textname": "Гость",
                0: {
                    "textname": "Вход",
                    "url": "%s?next=%s" % (reverse("login"), self.currentPath)
                    },
                1: {
                    "textname": "Регистрация",
                    "url": reverse("registration"),
                    }
                },
            1: {
                'textname': 'Друзья %s' % self.username,
                0: {
                    'textname': 'Все',
                    'url': reverse('friends', kwargs = {'user': self.slug, 'category': 'all', 'sep': '', 'page': ''})
                    },
                1: {
                    'textname': 'Сейчас в сети',
                    'url': reverse('friends', kwargs = {'user': self.slug, 'category': 'online', 'sep': '', 'page': ''})
                    }
                },
            2: {
                'textname': 'Профиль %s' % self.username,
                'url': reverse('profile', kwargs = {'currentUser': self.slug}),
                0: {
                    'textname': 'Записи в блоге'
                    },
                1: {
                    'textname': 'Записи в libraryTHPS'
                    }
                },
            3: {
                'textname': 'Пользователи',
                0: {
                    'textname': 'Все',
                    'url': reverse('users', kwargs = {'category': 'all', 'sep': '', 'page': ''})
                    },
                1: {
                    'textname': 'Сейчас в сети',
                    'url': reverse('users', kwargs = {'category': 'online', 'sep': '', 'page': ''})
                    }
                }
            })
        return menu

    @property
    @lru_cache(maxsize = 16)
    def menuGuestSerialized(self):
        menuDumps = self.menuGuest
        if isinstance(menuDumps, OrderedDict):
            return json.dumps(menuDumps)
        return json.dumps(menuDumps())

    @property
    @lru_cache(maxsize = 16)
    def menuGuestHiddenProfile(self):
        menu = OrderedDict({
            0: {
                "textname": "Гость",
                0: {
                    "textname": "Вход",
                    "url": "%s?next=%s" % (reverse("login"), self.currentPath)
                    },
                1: {
                    "textname": "Регистрация",
                    "url": reverse("registration"),
                    }
                },
            1: {
                'textname': 'Друзья %s' % self.username,
                0: {
                    'textname': 'Все',
                    'url': reverse('friends', kwargs = {'user': self.slug, 'category': 'all', 'sep': '', 'page': ''})
                    },
                1: {
                    'textname': 'Сейчас в сети',
                    'url': reverse('friends', kwargs = {'user': self.slug, 'category': 'online', 'sep': '', 'page': ''})
                    }
                },
            2: {
                'textname': 'Профиль %s' % self.username,
                'url': reverse('profile', kwargs = {'currentUser': self.slug}),
                0: {
                    'textname': 'Записи в блоге'
                    },
                1: {
                    'textname': 'Записи в libraryTHPS'
                    }
                },
            3: {
                'textname': 'Пользователи',
                0: {
                    'textname': 'Все',
                    'url': reverse('users', kwargs = {'category': 'all', 'sep': '', 'page': ''})
                    },
                1: {
                    'textname': 'Сейчас в сети',
                    'url': reverse('users', kwargs = {'category': 'online', 'sep': '', 'page': ''})
                    }
                }
            })
        return menu

    @property
    @lru_cache(maxsize = 16)
    def menuGuestHiddenProfileSerialized(self):
        menuDumps = self.menuGuest
        if isinstance(menuDumps, OrderedDict):
            return json.dumps(menuDumps)
        return json.dumps(menuDumps())

    def menuGetter(self, methodName):
        menu = getattr(self, methodName)
        if isinstance(menu, OrderedDict):
            return menu
        if isinstance(menu, str):
            return menu
        return menu()

def friendsListPage(request, user, category, sep, page):
    if request.method == "GET":
        requestUser = request.user
        if requestUser.is_authenticated:
            requestUserId = str(requestUser.id)
            if user == "my" or requestUser.profileUrl == user:
                friendsIdsCache = caches['friendsIdCache']
                friendsIds = friendsIdsCache.get(requestUserId, None)
                if friendsIds is None:
                    friendsCache = caches['friendsCache']
                    userFriends = friendsCache.get(requestUserId, None)
                    if userFriends is None:
                        userFriends = requestUser.friends.all()
                        if userFriends.count() > 0:
                            friendsCache.set(requestUserId, userFriends)
                            userFriendsIds = userFriends.values("id")
                            friendsIds = {str(user.get("id")) for user in userFriendsIds}
                        else:
                            friendsCache.set(requestUserId, False)
                            friendsIds = set()
                    else:
                        if userFriends is False:
                            friendsIds = set()
                        else:
                            userFriendsIds = userFriends.values("id")
                            friendsIds = {str(user.get("id")) for user in userFriendsIds}
                        friendsIdsCache.set(requestUserId, friendsIds)
                else:
                    userFriends = None
                loadItemsPage = request.GET.get('page', None)
                if loadItemsPage is not None:
                    loadItemsPage = int(loadItemsPage)
                    if category == "online":
                        onlineUsers = set(caches["onlineUsers"].get_many(friendsIds).keys())
                        iffyTimeDict = caches['iffyUsersOnlineStatus'].get_many((friendsIds - onlineUsers))
                        friendsPaginator = Paginator(tuple((onlineUsers | set(iffyTimeDict.keys()))), 8)
                        if friendsPaginator.num_pages >= loadItemsPage:
                            statusDict, itemsList = itemsXHR.createFriendsListOnlineIm(userFriends, friendsPaginator.page(page).object_list, requestUser, requestUserId, iffyTimeDict, onlineUsers)
                            return JsonResponse({
                                'usersListTuple': itemsList, 
                                'statusDict': statusDict
                                })
                        return JsonResponse({"friends": None})
                    friendsPaginator = Paginator(tuple(friendsIds), 8)
                    if friendsPaginator.num_pages >= loadItemsPage:
                        statusDict, itemsList = itemsXHR.createFriendsListIm(userFriends, friendsPaginator.page(page).object_list, requestUser, requestUserId, privacyCache, onlineUsers)
                        return JsonResponse({
                            'usersListTuple': itemsList, 
                            'statusDict': statusDict
                            })
                    return JsonResponse({"friends": None})
                if page != "":
                    page = int(page)
                else:
                    page = 1
                if category == "online":
                    onlineUsers = set(caches["onlineUsers"].get_many(friendsIds).keys())
                    iffyTimeDict = caches['iffyUsersOnlineStatus'].get_many((friendsIds - onlineUsers))
                    friendsPaginator = Paginator(tuple((onlineUsers | set(iffyTimeDict.keys()))), 8)
                    if friendsPaginator.num_pages >= page:
                        if request.is_ajax():
                            statusDict, itemsList = itemsXHR.createFriendsListOnlineIm(userFriends, friendsPaginator.page(page).object_list, requestUser, requestUserId, iffyTimeDict, onlineUsers)
                            if "global" in request.GET:
                                return JsonResponse({"newPage": json.dumps({
                                    'newTemplate': rts("psixiSocial/psixiSocial.html", {
                                        'parentTemplate': 'base/classicPage/emptyParent.html',
                                        'content': ('psixiSocial', 'friends', 'online'),
                                        'menu': cachedFragments().menuGetter('menuType1Serialized'),
                                        'usersListTuple': itemsList, 
                                        'statusDict': statusDict,
                                        'targetUser': None,
                                        'requestUserId': requestUserId,
                                        'textDict': usersListText(lang = 'ru').getDict(),
                                        'currentDateTime': str(datetime.now())
                                        }),
                                    'url': request.path,
                                    'titl': ''
                                    })})
                            return JsonResponse({"newPage": json.dumps({
                                'templateHead': rts("psixiSocial/head/users.html"),
                                'templateBody': rts("psixiSocial/body/friends/friendsOnline.html", {
                                    'usersListTuple': itemsList,
                                    }),
                                'menu': cachedFragments().menuGetter('menuType1'),
                                'templateScripts': rts("psixiSocial/scripts/users/users.html", {
                                    'requestUserId': requestUserId,
                                    'statusDict': statusDict,
                                    'textDict': usersListText(lang = 'ru').getDict(),
                                    'currentDateTime': str(datetime.now())
                                    }),
                                'content': ('psixiSocial', 'friends', 'online'),
                                'url': request.path,
                                'titl': ''
                                })})
                        statusDict, itemsList = items.createFriendsListOnlineIm(userFriends, friendsPaginator.page(page).object_list, requestUser, requestUserId, iffyTimeDict, onlineUsers)
                        return render(request, "psixiSocial/psixiSocial.html", {
                            'parentTemplate': 'base/classicPage/base.html',
                            'content': ('psixiSocial', 'friends', 'online'),
                            'menu': cachedFragments().menuGetter('menuType1Serialized'),
                            'usersListTuple': itemsList,
                            'statusDict': statusDict,
                            'targetUser': None,
                            'requestUserId': requestUserId,
                            'textDict': usersListText(lang = 'ru').getDict(),
                            'currentDateTime': str(datetime.now())
                            })
                    # Out of range pages
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'newTemplate': rts("psixiSocial/psixiSocial.html", {
                                    'parentTemplate': 'base/classicPage/emptyParent.html',
                                    'content': ('psixiSocial', 'friends', 'online'),
                                    'menu': cachedFragments().menuGetter('menuType1Serialized'),
                                    'usersDict': createFriendsListIm(userFriends, friendsPaginator.page(page).object_list, requestUser, requestUserId, privacyCache),
                                    'targetUser': None,
                                    'requestUserId': requestUserId,
                                    'textDict': usersListText(lang = 'ru').getDict()
                                    }),
                                'url': request.path,
                                'titl': ''
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            'templateHead': rts("psixiSocial/head/users.html"),
                            'templateBody': rts("psixiSocial/body/friends/friendsOnline.html", {
                                'usersDict': createFriendsListIm(userFriends, friendsPaginator.page(page).object_list, requestUser, requestUserId, privacyCache),
                                'targetUser': None
                                }),
                            'menu': cachedFragments().menuGetter('menuType1'),
                            'templateScripts': rts("psixiSocial/scripts/users/users.html", {
                                'requestUserId': requestUserId,
                                'textDict': usersListText(lang = 'ru').getDict()
                                }),
                            'content': ('psixiSocial', 'friends', 'online'),
                            'url': request.path,
                            'titl': ''
                            })})
                    return render(request, "psixiSocial/psixiSocial.html", {
                        'parentTemplate': 'base/classicPage/base.html',
                        'content': ('psixiSocial', 'friends', 'online'),
                        'menu': cachedFragments().menuGetter('menuType1Serialized'),
                        'usersDict': createFriendsListIm(userFriends, friendsPaginator.page(page).object_list, requestUser, requestUserId, privacyCache),
                        'targetUser': None,
                        'requestUserId': requestUserId,
                        'textDict': usersListText(lang = 'ru').getDict()
                        })
                friendsPaginator = Paginator(tuple(friendsIds), 8)
                pages = friendsPaginator.num_pages
                if pages >= page:
                    if request.is_ajax():
                        statusDict, itemsList = itemsXHR.createFriendsListIm(userFriends, friendsPaginator.page(page).object_list, requestUser, requestUserId)
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'newTemplate': rts("psixiSocial/psixiSocial.html", {
                                    'parentTemplate': 'base/classicPage/emptyParent.html',
                                    'content': ('psixiSocial', 'friends', 'all'),
                                    'menu': cachedFragments().menuGetter('menuType1Serialized'),
                                    'usersListTuple': itemsList,
                                    'statusDict': statusDict,
                                    'targetUser': None,
                                    'requestUserId': requestUserId,
                                    'textDict': usersListText(lang = 'ru').getDict(),
                                    'currentDateTime': str(datetime.now())
                                    }),
                                'url': request.path,
                                'titl': ''
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            'templateHead': rts("psixiSocial/head/users.html"),
                            'templateBody': rts("psixiSocial/body/friends/friends.html", {
                                'usersListTuple': itemsList,
                                }),
                            'menu': cachedFragments().menuGetter('menuType1'),
                            'templateScripts': rts("psixiSocial/scripts/users/users.html", {
                                'requestUserId': requestUserId,
                                'statusDict': statusDict,
                                'textDict': usersListText(lang = 'ru').getDict(),
                                'currentDateTime': str(datetime.now())
                                }),
                            'content': ('psixiSocial', 'friends', 'all'),
                            'url': request.path,
                            'titl': ''
                            })})
                    statusDict, itemsList = items.createFriendsListIm(userFriends, friendsPaginator.page(page).object_list, requestUser, requestUserId)
                    return render(request, "psixiSocial/psixiSocial.html", {
                        'parentTemplate': 'base/classicPage/base.html',
                        'content': ('psixiSocial', 'friends', 'all'),
                        'menu': cachedFragments().menuGetter('menuType1Serialized'),
                        'usersListTuple': itemsList,
                        'statusDict': statusDict,
                        'targetUser': None,
                        'requestUserId': requestUserId,
                        'textDict': usersListText(lang = 'ru').getDict(),
                        'currentDateTime': str(datetime.now())
                        })
                # Out of range pages
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'newTemplate': rts("psixiSocial/psixiSocial.html", {
                                'parentTemplate': 'base/classicPage/emptyParent.html',
                                'content': ('psixiSocial', 'friends', 'all'),
                                'menu': cachedFragments().menuGetter('menuType1Serialized'),
                                'targetUser': None,
                                'requestUserId': requestUserId,
                                'textDict': usersListText(lang = 'ru').getDict()
                                }),
                            'url': request.path,
                            'titl': ''
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        'templateHead': rts("psixiSocial/head/users.html"),
                        'templateBody': rts("psixiSocial/body/friends/friendsOnline.html", {
                            'targetUser': None
                            }),
                        'menu': cachedFragments().menuGetter('menuType1'),
                        'templateScripts': rts("psixiSocial/scripts/users/users.html", {
                            'requestUserId': requestUserId,
                            'textDict': usersListText(lang = 'ru').getDict()
                            }),
                        'content': ('psixiSocial', 'friends', 'all'),
                        'url': request.path,
                        'titl': ''
                        })})
                return render(request, "psixiSocial/psixiSocial.html", {
                    'parentTemplate': 'base/classicPage/base.html',
                    'content': ('psixiSocial', 'friends', 'all'),
                    'menu': cachedFragments().menuGetter('menuType1Serialized'),
                    'targetUser': None,
                    'requestUserId': requestUserId,
                    'textDict': usersListText(lang = 'ru').getDict()
                    })
            try:
                userProfile = mainUserProfile.objects.get(profileUrl = user)
            except mainUserProfile.DoesNotExist:
                pass
            userProfileId = userProfile.id
            privacyCache = caches['privacyCache']
            privacyDict = privacyCache.get(userProfileId, None)
            friendsIdsCache = None
            friendShipCache = None
            friendRequestsCache = None
            if privacyDict is None:
                userPrivacy = userProfilePrivacy.objects.get(user__exact = userProfile)
                friendShipCache = caches['friendShipStatusCache']
                friendShipDict = friendShipCache.get(userProfileId, None)
                if friendShipDict is None:
                    friendsIdsCache = caches['friendsIdCache']
                    friendsIds = friendsIdsCache.get(userProfileId, None)
                    if friendsIds is None:
                        friendsCache = caches['friendsCache']
                        userFriends = friendsCache.get(userProfileId, None)
                        if userFriends is None:
                            userFriends = userProfile.friends.all()
                            if userFriends.count() == 0:
                                userFriends = False
                                friendsIds = set()
                            else:
                                userFriendsIds = userFriends.values("id")
                                friendsIds = set(user.get("id") for user in userFriendsIds)
                            friendsCache.set(userProfileId, userFriends)
                        elif userFriends is False:
                            friendsIds = set()
                        else:
                            userFriendsIds = userFriends.values("id")
                            friendsIds = set(user.get("id") for user in userFriendsIds)
                        friendsIdsCache.set(userProfileId, friendsIds)
                    if requestUserId in friendsIds:
                        privacyGetter = checkPrivacy(user = requestUser, privacy = userPrivacy, isFriend = True, isAuthenticated = True)
                        friendShipCache.set(userProfileId, {requestUserId: True})
                    else:
                        privacyGetter = checkPrivacy(user = requestUser, privacy = userPrivacy, isFriend = False, isAuthenticated = True)
                        friendRequestsCache = caches['friendshipRequests']
                        friendRequests = friendRequestsCache.get(requestUserId, tuple())
                        find = False
                        for friendRequest in friendRequests:
                            if friendRequest[0] == userProfileId:
                                find = True
                                privacyGetter.isFriend = True
                                friendShipCache.set(userProfileId, {requestUserId: "request"})
                                break
                        if find is False:
                            friendShipCache.set(userProfileId, {requestUserId: False})
                else:
                    friendShipStatus = friendShipDict.get(requestUserId, None)
                    if friendShipStatus is None:
                        friendsIdsCache = caches['friendsIdCache']
                        friendsIds = friendsIdsCache.get(userProfileId, None)
                        if friendsIds is None:
                            friendsCache = caches['friendsCache']
                            userFriends = friendsCache.get(userProfileId, None)
                            if userFriends is None:
                                userFriends = userProfile.friends.all()
                                if userFriends.count() == 0:
                                    userFriends = False
                                    friendsIds = set()
                                else:
                                    friendsIds = set(user.id for user in userFriends)
                                friendsCache.set(userProfileId, userFriends)
                            elif userFriends is False:
                                friendsIds = set()
                            else:
                                friendsIds = set(user.id for user in userFriends)
                            friendsIdsCache.set(userProfileId, friendsIds)
                        if requestUserId in friendsIds:
                            privacyGetter = checkPrivacy(user = requestUser, privacy = userPrivacy, isFriend = True, isAuthenticated = True)
                            friendShipDict.update({requestUserId: True})
                            friendShipCache.set(userProfileId, friendShipDict)
                        else:
                            privacyGetter = checkPrivacy(user = requestUser, privacy = userPrivacy, isFriend = False, isAuthenticated = True)
                            friendRequestsCache = caches['friendshipRequests']
                            friendRequests = friendRequestsCache.get(requestUserId, tuple())
                            find = False
                            for friendRequest in friendRequests:
                                if friendRequest[0] == userProfileId:
                                    find = True
                                    privacyGetter.isFriend = True
                                    friendShipDict.update({requestUserId: "request"})
                                    friendShipCache.set(userProfileId, friendShipDict)
                                    break
                            if find is False:
                                friendShipDict.update({requestUserId: False})
                                friendShipCache.set(userProfileId, friendShipDict)
                    else:
                        privacyGetter = checkPrivacy(user = requestUser, privacy = userPrivacy, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                viewFriends = privacyGetter.checkFriendsListPrivacy()
                privacyCache.set(userProfileId,  {requestUserId: {'viewFriends': viewFriends}})
            else:
                userPrivacyDict = privacyDict.get(requestUserId, None)
                if userPrivacyDict is None:
                    userPrivacy = userProfilePrivacy.objects.get(user__exact = userProfile)
                    friendShipCache = caches['friendShipStatusCache']
                    friendShipDict = friendShipCache.get(userProfileId, None)
                    if friendShipDict is None:
                        friendsIdsCache = caches['friendsIdCache']
                        friendsIds = friendsIdsCache.get(userProfileId, None)
                        if friendsIds is None:
                            friendsCache = caches['friendsCache']
                            userFriends = friendsCache.get(userProfileId, None)
                            if userFriends is None:
                                userFriends = userProfile.friends.all()
                                if userFriends.count() == 0:
                                    userFriends = False
                                    friendsIds = set()
                                else:
                                    friendsIds = set(user.id for user in userFriends)
                                friendsCache.set(userProfileId, userFriends)
                            elif userFriends is False:
                                friendsIds = set()
                            else:
                                friendsIds = set(user.id for user in userFriends)
                            friendsIdsCache.set(userProfileId, friendsIds)
                        if requestUserId in friendsIds:
                            privacyGetter = checkPrivacy(user = requestUser, privacy = userPrivacy, isFriend = True, isAuthenticated = True)
                            friendShipCache.set(userProfileId, {requestUserId: "request"})
                        else:
                            privacyGetter = checkPrivacy(user = requestUser, privacy = userPrivacy, isFriend = False, isAuthenticated = True)
                            friendRequestsCache = caches['friendshipRequests']
                            friendRequests = friendRequestsCache.get(requestUserId, tuple())
                            for friendRequest in friendRequests:
                                if friendRequest[0] == userProfileId:
                                    privacyGetter.isFriend = True
                                    friendShipCache.set(userProfileId, {requestUserId: "request"})
                                    break
                    else:
                        friendShipStatus = friendShipDict.get(requestUserId, None)
                        if friendShipStatus is None:
                            friendsIdsCache = caches['friendsIdCache']
                            friendsIds = friendsIdsCache.get(userProfileId, None)
                            if friendsIds is None:
                                friendsCache = caches['friendsCache']
                                userFriends = friendsCache.get(userProfileId, None)
                                if userFriends is None:
                                    userFriends = userProfile.friends.all()
                                    if userFriends.count() == 0:
                                        userFriends = False
                                        friendsIds = set()
                                    else:
                                        friendsIds = set(user.id for user in userFriends)
                                    friendsCache.set(userProfileId, userFriends)
                                elif userFriends is False:
                                    friendsIds = set()
                                else:
                                    friendsIds = set(user.id for user in userFriends)
                                friendsIdsCache.set(userProfileId, friendsIds)
                            if requestUserId in friendsIds:
                                privacyGetter = checkPrivacy(user = requestUser, privacy = userPrivacy, isFriend = True, isAuthenticated = True)
                                friendShipCache.set(userProfileId, {requestUserId: "request"})
                            else:
                                privacyGetter = checkPrivacy(user = requestUser, privacy = userPrivacy, isFriend = False, isAuthenticated = True)
                                friendRequestsCache = caches['friendshipRequests']
                                friendRequests = friendRequestsCache.get(requestUserId, tuple())
                                for friendRequest in friendRequests:
                                    if friendRequest[0] == userProfileId:
                                        privacyGetter.isFriend = True
                                        friendShipCache.set(userProfileId, {requestUserId: "request"})
                                        break
                        else:
                            privacyGetter = checkPrivacy(user = requestUser, privacy = userPrivacy, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                    viewFriends = privacyGetter.checkFriendsListPrivacy()
                    privacyDict.update({requestUserId: {'viewFriends': viewFriends}})
                    privacyCache.set(userProfileId, privacyDict)
                else:
                    viewFriends = userPrivacyDict.get('viewFriends', None)
                    if viewFriends is None:
                        userPrivacy = userProfilePrivacy.objects.get(user__exact = userProfile)
                        friendShipCache = caches['friendShipStatusCache']
                        friendShipDict = friendShipCache.get(userProfileId, None)
                        if friendShipDict is None:
                            friendsIdsCache = caches['friendsIdCache']
                            friendsIds = friendsIdsCache.get(userProfileId, None)
                            if friendsIds is None:
                                friendsCache = caches['friendsCache']
                                userFriends = friendsCache.get(userProfileId, None)
                                if userFriends is None:
                                    userFriends = userProfile.friends.all()
                                    if userFriends.count() == 0:
                                        userFriends = False
                                        friendsIds = set()
                                    else:
                                        friendsIds = set(user.id for user in userFriends)
                                    friendsCache.set(userProfileId, userFriends)
                                elif userFriends is False:
                                    friendsIds = set()
                                else:
                                    friendsIds = set(user.id for user in userFriends)
                                friendsIdsCache.set(userProfileId, friendsIds)
                            if requestUserId in friendsIds:
                                privacyGetter = checkPrivacy(user = requestUser, privacy = userPrivacy, isFriend = True, isAuthenticated = True)
                                friendShipCache.set(userProfileId, {requestUserId: "request"})
                            else:
                                privacyGetter = checkPrivacy(user = requestUser, privacy = userPrivacy, isFriend = False, isAuthenticated = True)
                                friendRequestsCache = caches['friendshipRequests']
                                friendRequests = friendRequestsCache.get(requestUserId, tuple())
                                for friendRequest in friendRequests:
                                    if friendRequest[0] == userProfileId:
                                        privacyGetter.isFriend = True
                                        friendShipCache.set(userProfileId, {requestUserId: "request"})
                                        break
                        else:
                            friendShipStatus = friendShipDict.get(requestUserId, None)
                            if friendShipStatus is None:
                                friendsIdsCache = caches['friendsIdCache']
                                friendsIds = friendsIdsCache.get(userProfileId, None)
                                if friendsIds is None:
                                    friendsCache = caches['friendsCache']
                                    userFriends = friendsCache.get(userProfileId, None)
                                    if userFriends is None:
                                        userFriends = userProfile.friends.all()
                                        if userFriends.count() == 0:
                                            userFriends = False
                                            friendsIds = set()
                                        else:
                                            friendsIds = set(user.id for user in userFriends)
                                        friendsCache.set(userProfileId, userFriends)
                                    elif userFriends is False:
                                        friendsIds = set()
                                    else:
                                        friendsIds = set(user.id for user in userFriends)
                                    friendsIdsCache.set(userProfileId, friendsIds)
                                if requestUserId in friendsIds:
                                    privacyGetter = checkPrivacy(user = requestUser, privacy = userPrivacy, isFriend = True, isAuthenticated = True)
                                    friendShipCache.set(userProfileId, {requestUserId: "request"})
                                else:
                                    privacyGetter = checkPrivacy(user = requestUser, privacy = userPrivacy, isFriend = False, isAuthenticated = True)
                                    friendRequestsCache = caches['friendshipRequests']
                                    friendRequests = friendRequestsCache.get(requestUserId, tuple())
                                    for friendRequest in friendRequests:
                                        if friendRequest[0] == userProfileId:
                                            privacyGetter.isFriend = True
                                            friendShipCache.set(userProfileId, {requestUserId: "request"})
                                            break
                            else:
                                privacyGetter = checkPrivacy(user = requestUser, privacy = userPrivacy, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                        viewFriends = privacyGetter.checkFriendsListPrivacy()
                        userPrivacyDict.update({'viewFriends': viewFriends})
                        privacyCache.set(userProfileId, privacyDict)
            if viewFriends:
                if friendsIdsCache is None:
                    friendsIdsCache = caches['friendsIdCache']
                    friendsIds = friendsIdsCache.get(userProfileId, None)
                    if friendsIds is None:
                        friendsCache = caches['friendsCache']
                        userFriends = friendsCache.get(userProfileId, None)
                        if userFriends is None:
                            userFriends = userProfile.friends.all()
                            if userFriends.count() == 0:
                                friendsIds = set()
                                friendsCache.set(userProfileId, userFriends)
                            else:
                                userFriendsIds = userFriends.values("id")
                                friendsIds = set(user.get("id") for user in userFriendsIds)
                                friendsCache.set(userProfileId, userFriends)
                        elif userFriends is False:
                            friendsIds = set()
                        else:
                            userFriendsIds = userFriends.values("id")
                            friendsIds = set(user.get("id") for user in userFriendsIds)
                        friendsIdsCache.set(userProfileId, friendsIds)
                    else:
                        userFriends = None
                loadItemsPage = request.GET.get('page', None)
                if loadItemsPage is not None:
                    loadItemsPage = int(loadItemsPage)
                    if category == "online":
                        onlineUsers = set(caches["onlineUsers"].get_many(friendsIds).keys())
                        iffyTimeDict = caches['iffyUsersOnlineStatus'].get_many((friendsIds - onlineUsers))
                        friendsPaginator = Paginator(tuple((onlineUsers | set(iffyTimeDict.keys()))), 8)
                        if friendsPaginator.num_pages >= loadItemsPage:
                            statusDict, itemsList = itemsXHR.createFriendsListOnline(userFriends, friendsPaginator.page(page).object_list, requestUser, requestUserId, friendsIds, privacyCache, friendShipCache, friendRequestsCache, iffyTimeDict, onlineUsers)
                            return JsonResponse({
                                'usersListTuple': itemsList,
                                'statusDict': statusDict
                                })
                        return JsonResponse({"friends": None})
                    friendsPaginator = Paginator(tuple(friendsIds), 8)
                    if friendsPaginator.num_pages >= loadItemsPage:
                        statusDict, itemsList = itemsXHR.createFriendsList(userFriends, friendsPaginator.page(page).object_list, requestUser, requestUserId, friendsIds, privacyCache, friendShipCache, friendRequestsCache)
                        return JsonResponse({
                            'usersListTuple': itemsList,
                            'statusDict': statusDict
                            })
                    return JsonResponse({"friends": None})
                if page != "":
                    page = int(page)
                else:
                    page = 1
                if category == "online":
                    onlineUsers = set(caches["onlineUsers"].get_many(friendsIds).keys())
                    iffyTimeDict = caches['iffyUsersOnlineStatus'].get_many((friendsIds - onlineUsers))
                    friendsPaginator = Paginator(tuple((onlineUsers | set(iffyTimeDict.keys()))), 8)
                    pages = friendsPaginator.num_pages
                    if pages >= page:
                        if request.is_ajax():
                            statusDict, itemsList = itemsXHR.createFriendsListOnline(userFriends, friendsPaginator.page(page).object_list, requestUser, requestUserId, friendsIds, privacyCache, friendShipCache, friendRequestsCache, iffyTimeDict, onlineUsers)
                            if "global" in request.GET:
                                return JsonResponse({"newPage": json.dumps({
                                    'newTemplate': rts("psixiSocial/psixiSocial.html", {
                                    'parentTemplate': 'base/classicPage/emptyParent.html',
                                    'content': ('psixiSocial', 'friends', 'online'),
                                    'menu': cachedFragments(username = userProfile.username, slug = user).menuGetter('menuType2Serialized'),
                                    'usersListTuple': itemsList,
                                    'statusDict': statusDict,
                                    'requestUserId': requestUserId,
                                    'textDict': usersListText(lang = 'ru').getDict(),
                                    'currentDateTime': str(datetime.now())
                                    }),
                                'url': request.path,
                                'titl': ''
                                })})
                            return JsonResponse({"newPage": json.dumps({
                                'templateHead': rts("psixiSocial/head/users.html"),
                                'templateBody': rts("psixiSocial/body/friends/friendsOnline.html", {
                                    'usersListTuple': itemsList,
                                    }),
                                'menu': cachedFragments(username = userProfile.username, slug = user).menuGetter('menuType2'),
                                'templateScripts': rts("psixiSocial/scripts/users/users.html", {
                                    'requestUserId': requestUserId,
                                    'statusDict': statusDict,
                                    'textDict': usersListText(lang = 'ru').getDict(),
                                    'currentDateTime': str(datetime.now())
                                    }),
                                'content': ('psixiSocial', 'friends', 'online'),
                                'url': request.path,
                                'titl': ''
                                })})
                        statusDict, itemsList = items.createFriendsListOnline(userFriends, friendsPaginator.page(page).object_list, requestUser, requestUserId, friendsIds, privacyCache, friendShipCache, friendRequestsCache, iffyTimeDict, onlineUsers)
                        return render(request, "psixiSocial/psixiSocial.html", {
                            'parentTemplate': 'base/classicPage/base.html',
                            'content': ('psixiSocial', 'friends', 'online'),
                            'menu': cachedFragments(username = userProfile.username, slug = user).menuGetter('menuType2Serialized'),
                            'usersListTuple': itemsList,
                            'statusDict': statusDict,
                            'requestUserId': requestUserId,
                            'textDict': usersListText(lang = 'ru').getDict(),
                            'currentDateTime': str(datetime.now())
                            })
                    # Out of range pages
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'newTemplate': rts("psixiSocial/psixiSocial.html", {
                                    'parentTemplate': 'base/classicPage/emptyParent.html',
                                    'content': ('psixiSocial', 'friends', 'online'),
                                    'menu': cachedFragments(username = userProfile.username, slug = user).menuGetter('menuType2Serialized'),
                                    'targetUser': userProfile,
                                    'requestUserId': requestUserId,
                                    'textDict': usersListText(lang = 'ru').getDict()
                                    }),
                                'url': request.path,
                                'titl': ''
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            'templateHead': rts("psixiSocial/head/users.html"),
                            'templateBody': rts("psixiSocial/body/friends/friendsOnline.html", {
                                'targetUser': userProfile
                                }),
                            'menu': cachedFragments(username = userProfile.username, slug = user).menuGetter('menuType2'),
                            'templateScripts': rts("psixiSocial/scripts/users/users.html", {
                                'requestUserId': requestUserId,
                                'textDict': usersListText(lang = 'ru').getDict()
                                }),
                            'content': ('psixiSocial', 'friends', 'online'),
                            'url': request.path,
                            'titl': ''
                            })})
                    return render(request, "psixiSocial/psixiSocial.html", {
                        'parentTemplate': 'base/classicPage/base.html',
                        'content': ('psixiSocial', 'friends', 'online'),
                        'menu': cachedFragments(username = userProfile.username, slug = user).menuGetter('menuType2Serialized'),
                        'targetUser': userProfile,
                        'requestUserId': requestUserId,
                        'textDict': usersListText(lang = 'ru').getDict()
                        })
                friendsPaginator = Paginator(tuple(friendsIds), 8)
                pages = friendsPaginator.num_pages
                if pages >= page:
                    if request.is_ajax():
                        statusDict, itemsList = itemsXHR.createFriendsList(userFriends, friendsPaginator.page(page).object_list, requestUser, requestUserId, friendsIds, privacyCache, friendShipCache, friendRequestsCache)
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'newTemplate': rts("psixiSocial/psixiSocial.html", {
                                    'parentTemplate': 'base/classicPage/emptyParent.html',
                                    'content': ('psixiSocial', 'friends', 'all'),
                                    'menu': cachedFragments(username = userProfile.username, slug = user).menuGetter('menuType2Serialized'),
                                    'usersListTuple': itemsList,
                                    'statusDict': statusDict,
                                    'targetUser': userProfile,
                                    'requestUserId': requestUserId,
                                    'textDict': usersListText(lang = 'ru').getDict(),
                                    'currentDateTime': str(datetime.now())
                                    }),
                                'url': request.path,
                                'titl': ''
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            'templateHead': rts("psixiSocial/head/users.html"),
                            'templateBody': rts("psixiSocial/body/friends/friendsOnline.html", {
                                'usersListTuple': itemsList
                                }),
                            'menu': cachedFragments(username = userProfile.username, slug = user).menuGetter('menuType2'),
                            'templateScripts': rts("psixiSocial/scripts/users/users.html", {
                                'requestUserId': requestUserId,
                                'statusDict': statusDict,
                                'textDict': usersListText(lang = 'ru').getDict(),
                                'currentDateTime': str(datetime.now())
                                }),
                            'content': ('psixiSocial', 'friends', 'all'),
                            'url': request.path,
                            'titl': ''
                            })})
                    statusDict, itemsList = items.createFriendsList(userFriends, friendsPaginator.page(page).object_list, requestUser, requestUserId, friendsIds, privacyCache, friendShipCache, friendRequestsCache)
                    return render(request, "psixiSocial/psixiSocial.html", {
                        'parentTemplate': 'base/classicPage/base.html',
                        'content': ('psixiSocial', 'friends', 'all'),
                        'menu': cachedFragments(username = userProfile.username, slug = user).menuGetter('menuType2Serialized'),
                        'usersListTuple': itemsList,
                        'statusDict': statusDict,
                        'targetUser': userProfile,
                        'requestUserId': requestUserId,
                        'textDict': usersListText(lang = 'ru').getDict(),
                        'currentDateTime': str(datetime.now())
                        })
                # Out of range pages
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'newTemplate': rts("psixiSocial/psixiSocial.html", {
                                'parentTemplate': 'base/classicPage/emptyParent.html',
                                'content': ('psixiSocial', 'friends', 'all'),
                                'menu': cachedFragments(username = userProfile.username, slug = user).menuGetter('menuType2Serialized'),
                                'targetUser': userProfile,
                                'requestUserId': requestUserId,
                                'textDict': usersListText(lang = 'ru').getDict()
                                }),
                            'url': request.path,
                            'titl': ''
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        'templateHead': rts("psixiSocial/head/users.html"),
                        'templateBody': rts("psixiSocial/body/friends/friendsOnline.html", {
                            'targetUser': userProfile
                            }),
                        'menu': cachedFragments(username = userProfile.username, slug = user).menuGetter('menuType2'),
                        'templateScripts': rts("psixiSocial/scripts/users/users.html", {
                            'requestUserId': requestUserId,
                            'textDict': usersListText(lang = 'ru').getDict()
                            }),
                        'content': ('psixiSocial', 'friends', 'all'),
                        'url': request.path,
                        'titl': ''
                        })})
                return render(request, "psixiSocial/psixiSocial.html", {
                    'parentTemplate': 'base/classicPage/base.html',
                    'content': ('psixiSocial', 'friends', 'all'),
                    'menu': cachedFragments(username = userProfile.username, slug = user).menuGetter('menuType2Serialized'),
                    'targetUser': userProfile,
                    'requestUserId': requestUserId,
                    'textDict': usersListText(lang = 'ru').getDict()
                    })
            # viewFriends == False
            if request.is_ajax():
                path = request.path
                if "global" in request.GET:
                    return JsonResponse({"newPage": json.dumps({
                        'newTemplate': rts("psixiSocial/psixiSocial.html", {
                            'parentTemplate': 'base/classicPage/emptyParent.html',
                            'content': ('psixiSocial', 'friends', 'hidden'),
                            'menu': cachedFragments(username = userProfile.username, slug = user, path = path).menuGetter('menuType2Serialized'),
                            'text': singleElementGetter(lang = 'ru').getString('userFriendsHidden'),
                            'userProfile': userProfile
                            }),
                            'url': path,
                            'titl': ''
                        })})
                return JsonResponse({"newPage": json.dumps({
                    'templateHead': rts("psixiSocial/head/users.html"),
                    'templateBody': rts("psixiSocial/body/friends/hidden.html", {
                        'text': singleElementGetter(lang = 'ru').getString('userFriendsHidden'),
                        'userProfile': userProfile
                        }),
                    'menu': cachedFragments(username = userProfile.username, slug = user, path = path).menuGetter('menuType2'),
                    'templateScripts': rts("psixiSocial/scripts/friends/hidden.html"),
                    'content': ('psixiSocial', 'friends', 'hidden'),
                    'url': path,
                    'titl': ''
                    })})
            return render(request, "psixiSocial/psixiSocial.html", {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'friends', 'hidden'),
                'menu': cachedFragments(username = userProfile.username, slug = user, path = request.path).menuGetter('menuType2Serialized'),
                'text': singleElementGetter(lang = 'ru').getString('userFriendsHidden'),
                'userProfile': userProfile
                })
        try:
            userProfile = mainUserProfile.objects.get(profileUrl = user)
        except mainUserProfile.DoesNotExist:
            pass
        userProfileId = userProfile.id
        privacyCache = caches['privacyCache']
        privacyDict = privacyCache.get(userProfileId, None)
        if privacyDict is None:
            userPrivacy = userProfilePrivacy.objects.get(user__exact = userProfile)
            privacyGetter = checkPrivacy(privacy = userPrivacy, isFriend = False, isAuthenticated = False)
            viewFriends = privacyGetter.checkFriendsListPrivacy()
            privacyCache.set(userProfileId,  {0: {'viewFriends': viewFriends}})
        else:
            userPrivacyDict = privacyDict.get(0, None)
            if userPrivacyDict is None:
                userPrivacy = userProfilePrivacy.objects.get(user__exact = userProfile)
                privacyGetter = checkPrivacy(privacy = userPrivacy, isFriend = False, isAuthenticated = False)
                viewFriends = privacyGetter.checkFriendsListPrivacy()
                privacyDict.update({0: {'viewFriends': viewFriends}})
                privacyCache.set(userProfileId, privacyDict)
            else:
                viewFriends = userPrivacyDict.get('viewFriends', None)
                if viewFriends is None:
                    userPrivacy = userProfilePrivacy.objects.get(user__exact = userProfile)
                    privacyGetter = checkPrivacy(privacy = userPrivacy, isFriend = False, isAuthenticated = False)
                    viewFriends = privacyGetter.checkFriendsListPrivacy()
                    userPrivacyDict.update({'viewFriends': viewFriends})
                    privacyCache.set(userProfileId, privacyDict)
        if viewFriends:
            loadItemsPage = request.GET.get('page', None)
            friendsIdsCache = caches['friendsIdCache']
            friendsIds = friendsIdsCache.get(requestUserId, None)
            if friendsIds is None:
                friendsCache = caches['friendsCache']
                userFriends = friendsCache.get(requestUserId, None)
                if userFriends is None:
                    userFriends = requestUser.friends.all()
                    if userFriends.count() > 0:
                        friendsCache.set(requestUserId, userFriends)
                        userFriendsIds = userFriends.values("id")
                        friendsIds = {str(user.get("id")) for user in userFriendsIds}
                    else:
                        friendsCache.set(requestUserId, False)
                        friendsIds = set()
                else:
                    if userFriends is False:
                        friendsIds = set()
                    else:
                        userFriendsIds = userFriends.values("id")
                        friendsIds = {str(user.get("id")) for user in userFriendsIds}
                    friendsIdsCache.set(requestUserId, friendsIds)
            else:
                userFriends = None
            if loadItemsPage is not None:
                loadItemsPage = int(loadItemsPage)
                if category == "online":
                    onlineUsers = set(caches["onlineUsers"].get_many(friendsIds).keys())
                    iffyTimeDict = caches['iffyUsersOnlineStatus'].get_many((friendsIds - onlineUsers))
                    friendsPaginator = Paginator(tuple((onlineUsers | set(iffyTimeDict.keys()))), 8)
                    if friendsPaginator.num_pages >= loadItemsPage:
                        onlineStatusDict, activeStatusDict, itemsDict = itemsXHR.createFriendsListOnlineGuest(friendsPaginator.page(loadItemsPage), userProfileId, privacyCache, iffyTimeDict, onlineUsers)
                        return JsonResponse({
                            'usersListTuple': itemsDict,
                            'statusDict': statusDict
                            })
                    return JsonResponse({"friends": None})
                friendsPaginator = Paginator(tuple(friendsIds), 8)
                if friendsPaginator.num_pages >= loadItemsPage:
                    onlineStatusDict, activeStatusDict, itemsDict = itemsXHR.createFriendsListGuest(friendsPaginator.page(loadItemsPage), userProfileId, privacyCache)
                    return JsonResponse({
                        'usersListTuple': itemsDict,
                        'statusDict': statusDict
                        })
                return JsonResponse({"friends": None})
            if page != "":
                page = int(page)
            else:
                page = 1
            if category == "online":
                onlineUsers = set(caches["onlineUsers"].get_many(friendsIds).keys())
                iffyTimeDict = caches['iffyUsersOnlineStatus'].get_many((friendsIds - onlineUsers))
                friendsPaginator = Paginator(tuple((onlineUsers | set(iffyTimeDict.keys()))), 8)
                pages = friendsPaginator.num_pages
                if pages >= page:
                    if request.is_ajax():
                        statusDict, itemsList = itemsXHR.createFriendsListOnlineGuest(userFriends, friendsPaginator.page(page).object_list, userProfileId, privacyCache, iffyTimeDict, onlineUsers)
                        path = request.path
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'newTemplate': rts("psixiSocial/psixiSocial.html", {
                                'parentTemplate': 'base/classicPage/emptyParent.html',
                                'content': ('psixiSocial', 'friends', 'online'),
                                'menu': cachedFragments(username = userProfile.username, slug = user, path = path).menuGetter('menuGuestSerialized'),
                                'usersListTuple': itemsList,
                                'statusDict': statusDict,
                                'requestUserId': None,
                                'textDict': usersListText(lang = 'ru').getDict(),
                                'currentDateTime': str(datetime.now())
                                }),
                            'url': path,
                            'titl': ''
                            })})
                        return JsonResponse({"newPage": json.dumps({
                            'templateHead': rts("psixiSocial/head/users.html"),
                            'templateBody': rts("psixiSocial/body/friends/friendsOnline.html", {
                                'usersListTuple': itemsList
                                }),
                            'menu': cachedFragments(username = userProfile.username, slug = user).menuGetter('menuGuest'),
                            'templateScripts': rts("psixiSocial/scripts/users/users.html", {
                                'statusDict': statusDict,
                                'requestUserId': None,
                                'textDict': usersListText(lang = 'ru').getDict(),
                                'currentDateTime': str(datetime.now())
                                }),
                            'content': ('psixiSocial', 'friends', 'online'),
                            'url': path,
                            'titl': ''
                            })})
                    statusDict, itemsList = items.createFriendsListOnlineGuest(userFriends, friendsPaginator.page(page).object_list, userProfileId, privacyCache, iffyTimeDict, onlineUsers)
                    return render(request, "psixiSocial/psixiSocial.html", {
                        'parentTemplate': 'base/classicPage/base.html',
                        'content': ('psixiSocial', 'friends', 'online'),
                        'menu': cachedFragments(username = userProfile.username, slug = user, path = request.path).menuGetter('menuGuestSerialized'),
                        'usersListTuple': itemsList,
                        'statusDict': statusDict,
                        'requestUserId': None,
                        'textDict': usersListText(lang = 'ru').getDict(),
                        'currentDateTime': str(datetime.now())
                        })
                # Out of range pages
                if request.is_ajax():
                    path = request.path
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'newTemplate': rts("psixiSocial/psixiSocial.html", {
                                'parentTemplate': 'base/classicPage/emptyParent.html',
                                'content': ('psixiSocial', 'friends', 'online'),
                                'menu': cachedFragments(username = userProfile.username, slug = user, path = path).menuGetter('menuGuestSerialized'),
                                'targetUser': userProfile,
                                'requestUserId': None,
                                'textDict': usersListText(lang = 'ru').getDict()
                                }),
                            'url': path,
                            'titl': ''
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        'templateHead': rts("psixiSocial/head/users.html"),
                        'templateBody': rts("psixiSocial/body/friends/friendsOnline.html", {
                            'targetUser': userProfile
                            }),
                        'menu': cachedFragments(username = userProfile.username, slug = user, path = path).menuGetter('menuGuest'),
                        'templateScripts': rts("psixiSocial/scripts/users/users.html", {
                            'requestUserId': None,
                            'textDict': usersListText(lang = 'ru').getDict()
                            }),
                        'content': ('psixiSocial', 'friends', 'online'),
                        'url': path,
                        'titl': ''
                        })})
                return render(request, "psixiSocial/psixiSocial.html", {
                    'parentTemplate': 'base/classicPage/base.html',
                    'content': ('psixiSocial', 'friends', 'online'),
                    'menu': cachedFragments(username = userProfile.username, slug = user, path = request.path).menuGetter('menuGuestSerialized'),
                    'targetUser': userProfile,
                    'requestUserId': None,
                    'textDict': usersListText(lang = 'ru').getDict()
                    })
            friendsPaginator = Paginator(tuple(friendsIds), 8)
            pages = friendsPaginator.num_pages
            if pages >= page:
                if request.is_ajax():
                    statusDict, itemsList = itemsXHR.createFriendsListGuest(userFriends, friendsPaginator.page(page).object_list, userProfileId, privacyCache)
                    path = request.path
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'newTemplate': rts("psixiSocial/psixiSocial.html", {
                                'parentTemplate': 'base/classicPage/emptyParent.html',
                                'content': ('psixiSocial', 'friends', 'all'),
                                'menu': cachedFragments(username = userProfile.username, slug = user, path = path).menuGetter('menuGuestSerialized'),
                                'usersListTuple': itemsList,
                                'statusDict': statusDict,
                                'requestUserId': None,
                                'textDict': usersListText(lang = 'ru').getDict(),
                                'currentDateTime': str(datetime.now())
                                }),
                            'url': path,
                            'titl': ''
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        'templateHead': rts("psixiSocial/head/users.html"),
                        'templateBody': rts("psixiSocial/body/friends/friendsOnline.html", {
                                'usersListTuple': itemsList,
                            }),
                        'menu': cachedFragments(username = userProfile.username, slug = user, path = path).menuGetter('menuGuest'),
                        'templateScripts': rts("psixiSocial/scripts/users/users.html", {
                            'statusDict': statusDict,
                            'requestUserId': None,
                            'textDict': usersListText(lang = 'ru').getDict(),
                            'currentDateTime': str(datetime.now())
                            }),
                        'content': ('psixiSocial', 'friends', 'all'),
                        'url': path,
                        'titl': ''
                        })})
                statusDict, itemsList = items.createFriendsListGuest(userFriends, friendsPaginator.page(page).object_list, userProfileId, privacyCache)
                return render(request, "psixiSocial/psixiSocial.html", {
                    'parentTemplate': 'base/classicPage/base.html',
                    'content': ('psixiSocial', 'friends', 'all'),
                    'menu': cachedFragments(username = userProfile.username, slug = user, path = request.path).menuGetter('menuGuestSerialized'),
                    'usersListTuple': itemsList,
                    'statusDict': statusDict,
                    'requestUserId': None,
                    'textDict': usersListText(lang = 'ru').getDict(),
                    'currentDateTime': str(datetime.now())
                    })
            # Out of range pages
            if request.is_ajax():
                path = request.path
                if "global" in request.GET:
                    return JsonResponse({"newPage": json.dumps({
                        'newTemplate': rts("psixiSocial/psixiSocial.html", {
                            'parentTemplate': 'base/classicPage/emptyParent.html',
                            'content': ('psixiSocial', 'friends', 'all'),
                            'menu': cachedFragments(username = userProfile.username, slug = user, path = path).menuGetter('menuGuestSerialized'),
                            'targetUser': userProfile,
                            'requestUserId': None,
                            'textDict': usersListText(lang = 'ru').getDict()
                            }),
                        'url': path,
                        'titl': ''
                        })})
                return JsonResponse({"newPage": json.dumps({
                    'templateHead': rts("psixiSocial/head/users.html"),
                    'templateBody': rts("psixiSocial/body/friends/friendsOnline.html", {
                        'targetUser': userProfile
                        }),
                    'menu': cachedFragments(username = userProfile.username, slug = user, path = path).menuGetter('menuGuest'),
                    'templateScripts': rts("psixiSocial/scripts/users/users.html", {
                        'requestUserId': None,
                        'textDict': usersListText(lang = 'ru').getDict()
                        }),
                    'content': ('psixiSocial', 'friends', 'all'),
                    'url': path,
                    'titl': ''
                    })})
            return render(request, "psixiSocial/psixiSocial.html", {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'friends', 'all'),
                'menu': cachedFragments(username = userProfile.username, slug = user, path = request.path).menuGetter('menuGuestSerialized'),
                'targetUser': userProfile,
                'requestUserId': None,
                'textDict': usersListText(lang = 'ru').getDict()
                })
        # viewFriends == Flase
        if request.is_ajax():
            path = request.path
            if "global" in request.GET:
                return JsonResponse({"newPage": json.dumps({
                    'newTemplate': rts("psixiSocial/psixiSocial.html", {
                        'parentTemplate': 'base/classicPage/emptyParent.html',
                        'content': ('psixiSocial', 'friends', 'hidden'),
                        'menu': cachedFragments(username = userProfile.username, slug = user, path = path).menuGetter('menuGuestSerialized'),
                        'text': singleElementGetter(lang = 'ru').getString('userFriendsHidden'),
                        'user': userProfile
                        }),
                        'url': path,
                        'titl': ''
                        })})
            return JsonResponse({"newPage": json.dumps({
                'templateHead': rts("psixiSocial/head/users.html"),
                'templateBody': rts("psixiSocial/body/friends/hidden.html", {
                    'text': singleElementGetter(lang = 'ru').getString('userFriendsHidden'),
                    'user': userProfile
                    }),
                'menu': cachedFragments(username = userProfile.username, slug = user, path = path).menuGetter('menuGuest'),
                'templateScripts': rts("psixiSocial/scripts/friends/hidden.html"),
                'content': ('psixiSocial', 'friends', 'hidden'),
                'url': path,
                'titl': ''
                })})
        return render(request, "psixiSocial/psixiSocial.html", {
            'parentTemplate': 'base/classicPage/base.html',
            'content': ('psixiSocial', 'friends', 'hidden'),
            'menu': cachedFragments(username = userProfile.username, slug = user, path = request.path).menuGetter('menuGuestSerialized'),
            'text': singleElementGetter(lang = 'ru').getString('userFriendsHidden'),
            'user': userProfile
            })