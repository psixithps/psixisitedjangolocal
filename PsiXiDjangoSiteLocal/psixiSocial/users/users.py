from datetime import datetime
import json
from django.core.cache import caches
from functools import lru_cache
from psixiSocial.models import mainUserProfile
from django.shortcuts import render, redirect
from django.template.loader import render_to_string as rts
from collections import OrderedDict
from django.urls import reverse
from django.http import JsonResponse
from django.core.paginator import Paginator
from psixiSocial.translators import usersListText
from psixiSocial.users import items, itemsXHR
'''
 ### Получение всех записей кеша memcache для продакшн, используя модуль - github.com/dlrust/python-memcached-stats.git ###

 from memcached_stats import MemcachedStats
 onlineUsersCacheKeysList = MemcachedStats(localhost, port).keys()
 activeUsersCachekeysList = MemcachedStats(localhost, port).keys()

 ### Получение всех записей кеша LocMem ###
 from django.core.cache.backends import locmem

'''
from django.core.cache.backends import locmem

class cachedFragments():

    @property
    @lru_cache(maxsize = 16)
    def menuType1(self):
        menu = OrderedDict({
            0: {
                'textname': 'Пользователи',
                0: {
                    'textname': 'Все',
                    'url': reverse('users', kwargs = {'category': 'all', 'sep': '', 'page': ''})
                    },
                1: {
                    'textname': 'Сейчас в сети',
                    'url': reverse('users', kwargs = {'category': 'online', 'sep': '', 'page': ''})
                    }
                },
            1: {
                'textname': 'Мой профиль',
                'url': reverse('profile', kwargs = {'currentUser': 'im'}),
                0: {
                    'textname': 'Диалоги',
                    'url': reverse('dialogs', kwargs = {'sep': '', 'page': ''})
                    },
                1: {
                    'textname': 'Друзья',
                    0: {
                        'textname': 'Все',
                        'url': reverse('friends', kwargs = {'user': 'my', 'category': 'all', 'sep': '',  'page': ''})
                        },
                    1: {
                        'textname': 'Сейчас в сети',
                        'url': reverse('friends', kwargs = {'user': 'my', 'category': 'online', 'sep': '',  'page': ''})
                        }
                    },
                2: {
                    'textname': 'Редактировать',
                    0: {
                        'textname': 'Основные',
                        'url': reverse('profileEditUrl', kwargs = {'editType': 'main'})
                        },
                    1: {
                        'textname': 'Дополнительно',
                        'url': reverse('profileEditUrl', kwargs = {'editType': 'detail'})
                        },
                    2: {
                        'textname': 'Обратная связь',
                        'url': reverse('profileEditUrl', kwargs = {'editType': 'contacts'})
                        },
                    3: {
                        'textname': 'Настройки приватности',
                            'url': reverse('editPrivacyUrl', kwargs = {'editPlace': 'profile'}),
                            0: {
                                'textname': 'Права для исключительных пользователей',
                                'url': reverse('editPrivacyExclusiveUrl', kwargs = {'editPlace': 'profile'})
                                }
                        },
                    4: {
                        'textname': 'THPS профиль',
                        'url': reverse('profileEditUrl', kwargs = {'editType': 'thps'})
                        }
                    }
                },
            3: {
                'textname': 'Выход',
                'url': reverse("logOut")
                }
            })
        return menu

    @property
    @lru_cache(maxsize = 16)
    def menuType1Serialized(self):
        menuDumps = self.menuType1
        if isinstance(menuDumps, OrderedDict):
            return json.dumps(menuDumps)
        return json.dumps(menuDumps())

    def menuGetter(self, methodName):
        menu = getattr(self, methodName)
        if isinstance(menu, OrderedDict):
            return menu
        if isinstance(menu, str):
            return menu
        return menu()

def usersListPage(request, category, sep, page):
    if request.method == 'GET':
        requestUser = request.user
        loadItemsPage = request.GET.get('page', None)
        if loadItemsPage is not None:
            if requestUser.is_authenticated:
                requestUserId = str(requestUser.id)
                if category == 'online':
                    locmemCache = locmem._caches
                    usersPaginator = Paginator(tuple(set(tupleItem[-1] for tupleItem in locmemCache['onlineusers'].keys()) | 
                                                     set(tupleItem[-1] for tupleItem in locmemCache['iffyusers'].keys())), 8)
                    #usersPaginator = Paginator(tuple(set(MemcachedStats(localhost, port).keys()) | set(MemcachedStats(localhost, port).keys()))) # Для продакшн - memcached
                    loadItemsPage = int(loadItemsPage)
                    if usersPaginator.num_pages >= loadItemsPage:
                        statusDict, itemsList = itemsXHR.createUsersListOnline(usersPaginator.page(loadItemsPage).object_list, requestUser, requestUserId)
                        return JsonResponse({'usersListTuple': itemsList, 'statusDict': statusDict})
                    return JsonResponse({"users": None})
                usersPaginator = Paginator(mainUserProfile.objects.all().values("id"), 8)
                loadItemsPage = int(loadItemsPage)
                if usersPaginator.num_pages >= loadItemsPage:
                    statusDict, itemsList = itemsXHR.createUsersList(usersPaginator.page(loadItemsPage).object_list, requestUser, requestUserId)
                    return JsonResponse({'usersListTuple': itemsList, 'statusDict': statusDict})
                return JsonResponse({"users": None})
            if category == 'online':
                locmemCache = locmem._caches
                usersPaginator = Paginator(tuple(set(tupleItem[-1] for tupleItem in locmemCache['onlineusers'].keys()) | 
                                                     set(tupleItem[-1] for tupleItem in locmemCache['iffyusers'].keys())), 8)
                #usersPaginator = Paginator(tuple(set(MemcachedStats(localhost, port).keys()) | set(MemcachedStats(localhost, port).keys()))) # Для продакшн - memcached
                loadItemsPage = int(loadItemsPage)
                if usersPaginator.num_pages >= loadItemsPage:
                    statusDict, itemsList = itemsXHR.createUsersListOnlineGuest(usersPaginator.page(loadItemsPage).object_list, iffyUsersCache, iffyUsers, onlineUsers)
                    return JsonResponse({'usersListTuple': itemsList, 'statusDict': statusDict})
                return JsonResponse({"users": None})
            usersPaginator = Paginator(mainUserProfile.objects.all().values("id"), 8)
            loadItemsPage = int(loadItemsPage)
            if usersPaginator.num_pages >= loadItemsPage:
                statusDict, itemsList = itemsXHR.createUsersListGuest(usersPaginator.page(loadItemsPage).object_list)
                return JsonResponse({'usersListTuple': itemsList, 'statusDict': statusDict})
            return JsonResponse({"users": None})
        if requestUser.is_authenticated:
            if page == '':
                page = 1
            else:
                page = int(page)
            requestUserId = str(requestUser.id)
            fragments = cachedFragments()
            if category == 'online':
                locmemCache = locmem._caches
                usersPaginator = Paginator(tuple(set(tupleItem[-1] for tupleItem in locmemCache['onlineusers'].keys()) | 
                                                     set(tupleItem[-1] for tupleItem in locmemCache['iffyusers'].keys())), 8)
                #usersPaginator = Paginator(tuple(set(MemcachedStats(localhost, port).keys()) | set(MemcachedStats(localhost, port).keys()))) # Для продакшн - memcached
                if usersPaginator.num_pages >= page:
                    if request.is_ajax():
                        statusDict, itemsList = itemsXHR.createUsersListOnline(usersPaginator.page(page).object_list, requestUser, requestUserId, iffyUsersCache, iffyUsers, onlineUsers)
                        if 'global' in request.GET:
                            return JsonResponse({'newPage': json.dumps({
                                'newTemplate': rts('psixiSocial/psixiSocial.html', {
                                    'parentTemplate': 'base/classicPage/emptyParent.html',
                                    'content': ('psixiSocial', 'users', 'all'),
                                    'menu': fragments.menuGetter('menuType1Serialized'),
                                    'usersListTuple': itemsList,
                                    'statusDict': statusDict,
                                    'requestUserId': requestUserId,
                                    'textDict': usersListText(lang = 'ru').getDict(),
                                    'currentDateTime': datetime.now()
                                    }),
                                'url': request.path,
                                'titl': ''
                                })})
                        textDict = usersListText(lang = 'ru').getDict()
                        return JsonResponse({'newPage': json.dumps({
                            'templateHead': rts('psixiSocial/head/users.html'),
                            'templateBody': rts('psixiSocial/body/users/users.html', {
                                'usersListTuple': itemsList
                                }),
                            'menu': fragments.menuGetter('menuType1'),
                            'templateScripts': rts('psixiSocial/scripts/users/usersOnline.html', {
                                'statusDict': statusDict,
                                'requestUserId': requestUserId,
                                'textDict': textDict,
                                'currentDateTime': datetime.now()
                                }),
                            'content': ('psixiSocial', 'users', 'all'),
                            'url': request.path,
                            'titl': ''
                            })})
                    if request.COOKIES.get("js", None) is not None:
                        statusDict, itemsList = itemsXHR.createUsersListOnline(usersPaginator.page(page).object_list, requestUser, requestUserId, iffyUsersCache, iffyUsers, onlineUsers)
                    else:
                        statusDict, itemsList = items.createUsersListOnline(usersPaginator.page(page).object_list, requestUser, requestUserId, iffyUsersCache, iffyUsers, onlineUsers)
                    return render(request, 'psixiSocial/psixiSocial.html', {
                        'parentTemplate': 'base/classicPage/base.html',
                        'content': ('psixiSocial', 'users', 'all'),
                        'menu': fragments.menuGetter('menuType1Serialized'),
                        'usersListTuple': itemsList,
                        'statusDict': statusDict,
                        'requestUserId': requestUserId,
                        'textDict': usersListText(lang = 'ru').getDict(),
                        'currentDateTime': datetime.now()
                        })
            
            usersPaginator = Paginator(mainUserProfile.objects.all().values("id"), 8)
            if usersPaginator.num_pages >= page:
                if request.is_ajax():
                    statusDict, itemsList = itemsXHR.createUsersList(usersPaginator.page(page).object_list, requestUser, requestUserId)
                    if 'global' in request.GET:
                        return JsonResponse({'newPage': json.dumps({
                            'newTemplate': rts('psixiSocial/psixiSocial.html', {
                                'parentTemplate': 'base/classicPage/emptyParent.html',
                                'content': ('psixiSocial', 'users', 'all'),
                                'menu': fragments.menuGetter('menuType1Serialized'),
                                'usersListTuple': itemsList,
                                'statusDict': statusDict,
                                'requestUserId': requestUserId,
                                'textDict': usersListText(lang = 'ru').getDict(),
                                'currentDateTime': datetime.now()
                                }),
                            'url': request.path,
                            'titl': ''
                            })})
                    textDict = usersListText(lang = 'ru').getDict()
                    return JsonResponse({'newPage': json.dumps({
                        'templateHead': rts('psixiSocial/head/users.html'),
                        'templateBody': rts('psixiSocial/body/users/users.html', {
                            'usersListTuple': itemsList
                            }),
                        'menu': fragments.menuGetter('menuType1'),
                        'templateScripts': rts('psixiSocial/scripts/users/users.html', {
                            'statusDict': statusDict,
                            'requestUserId': requestUserId,
                            'textDict': textDict,
                            'currentDateTime': datetime.now()
                            }),
                        'content': ('psixiSocial', 'users', 'all'),
                        'url': request.path,
                        'titl': ''
                        })})
                statusDict, itemsList = items.createUsersList(usersPaginator.page(page).object_list, requestUser, requestUserId)
                return render(request, 'psixiSocial/psixiSocial.html', {
                    'parentTemplate': 'base/classicPage/base.html',
                    'content': ('psixiSocial', 'users', 'all'),
                    'menu': fragments.menuGetter('menuType1Serialized'),
                    'usersListTuple': itemsList,
                    'statusDict': statusDict,
                    'requestUserId': requestUserId,
                    'textDict': usersListText(lang = 'ru').getDict(),
                    'currentDateTime': datetime.now()
                    })
        if page == '':
            page = 1
        else:
            page = int(page)
        fragments = cachedFragments()
        if category == 'online':
            locmemCache = locmem._caches
            usersPaginator = Paginator(tuple(set(tupleItem[-1] for tupleItem in locmemCache['onlineusers'].keys()) | 
                                                     set(tupleItem[-1] for tupleItem in locmemCache['iffyusers'].keys())), 8)
            #usersPaginator = Paginator(tuple(set(MemcachedStats(localhost, port).keys()) | set(MemcachedStats(localhost, port).keys()))) # Для продакшн - memcached
            if usersPaginator.num_pages >= page:
                if request.is_ajax():
                    statusDict, itemsList = itemsXHR.createUsersListOnlineGuest(usersPaginator.page(page).object_list, iffyUsersCache, iffyUsers, onlineUsers)
                    if 'global' in request.GET:
                        return JsonResponse({'newPage': json.dumps({
                            'newTemplate': rts('psixiSocial/psixiSocial.html', {
                                'parentTemplate': 'base/classicPage/emptyParent.html',
                                'content': ('psixiSocial', 'users', 'all'),
                                'menu': fragments.menuGetter('menuType1Serialized'),
                                'usersListTuple': itemsList,
                                'statusDict': statusDict,
                                'requestUserId': None,
                                'textDict': usersListText(lang = 'ru').getDict(),
                                'currentDateTime': datetime.now()
                                }),
                            'url': request.path,
                            'titl': ''
                            })})
                    textDict = usersListText(lang = 'ru').getDict()
                    return JsonResponse({'newPage': json.dumps({
                        'templateHead': rts('psixiSocial/head/users.html'),
                        'templateBody': rts('psixiSocial/body/users/users.html', {
                            'usersListTuple': itemsList
                            }),
                        'menu': fragments.menuGetter('menuType1'),
                        'templateScripts': rts('psixiSocial/scripts/users/usersOnline.html', {
                            'statusDict': statusDict,
                            'requestUserId': None,
                            'textDict': textDict,
                            'currentDateTime': datetime.now()
                            }),
                        'content': ('psixiSocial', 'users', 'all'),
                        'url': request.path,
                        'titl': ''
                        })})
                statusDict, itemsList = items.createUsersListOnlineGuest(usersPaginator.page(page).object_list, iffyUsersCache, iffyUsers, onlineUsers)
                return render(request, 'psixiSocial/psixiSocial.html', {
                    'parentTemplate': 'base/classicPage/base.html',
                    'content': ('psixiSocial', 'users', 'all'),
                    'menu': fragments.menuGetter('menuType1Serialized'),
                    'usersListTuple': itemsList,
                    'statusDict': statusDict,
                    'requestUserId': None,
                    'textDict': usersListText(lang = 'ru').getDict(),
                    'currentDateTime': datetime.now()
                    })
        
        usersPaginator = Paginator(mainUserProfile.objects.all().values("id"), 8)
        if usersPaginator.num_pages >= page:
            if request.is_ajax():
                statusDict, itemsList = itemsXHR.createUsersListGuest(usersPaginator.page(page).object_list)
                if 'global' in request.GET:
                    return JsonResponse({'newPage': json.dumps({
                        'newTemplate': rts('psixiSocial/psixiSocial.html', {
                            'parentTemplate': 'base/classicPage/emptyParent.html',
                            'content': ('psixiSocial', 'users', 'all'),
                            'menu': fragments.menuGetter('menuType1Serialized'),
                            'usersListTuple': itemsList,
                            'statusDict': statusDict,
                            'requestUserId': None,
                            'textDict': usersListText(lang = 'ru').getDict(),
                            'currentDateTime': datetime.now()
                            }),
                        'url': request.path,
                        'titl': ''
                        })})
                textDict = usersListText(lang = 'ru').getDict()
                return JsonResponse({'newPage': json.dumps({
                    'templateHead': rts('psixiSocial/head/users.html'),
                    'templateBody': rts('psixiSocial/body/users/users.html', {
                        'usersListTuple': itemsList
                        }),
                    'menu': fragments.menuGetter('menuType1'),
                    'templateScripts': rts('psixiSocial/scripts/users/users.html', {
                        'statusDict': statusDict,
                        'requestUserId': None,
                        'textDict': textDict,
                        'currentDateTime': datetime.now()
                        }),
                    'content': ('psixiSocial', 'users', 'all'),
                    'url': request.path,
                    'titl': ''
                    })})
            statusDict, itemsList = items.createUsersListGuest(usersPaginator.page(page).object_list)
            return render(request, 'psixiSocial/psixiSocial.html', {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'users', 'all'),
                'menu': fragments.menuGetter('menuType1Serialized'),
                'usersListTuple': itemsList,
                'statusDict': statusDict,
                'requestUserId': None,
                'textDict': usersListText(lang = 'ru').getDict(),
                'currentDateTime': datetime.now()
                })