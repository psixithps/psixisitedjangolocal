from datetime import datetime
from django.urls import reverse
from django.core.cache import caches
from psixiSocial.models import mainUserProfile, userProfilePrivacy, THPSProfile
from psixiSocial.privacyIdentifier import checkPrivacy
from psixiSocial.translators import usersListText

def createUsersList(iterableIdsPageInstance, requestUser, requestUserId):
    result = []
    statusDict = {}
    onlineUsersSet = set()
    iffyUsersDict = {}
    activeUsersSet = set()
    hiddenActiveUsersSet = set()
    hiddenOnlineUsersSet = set()
    middleUsersDict = {}
    thumbItemsToSet = None
    iterableIds = {str(user.get("id")) for user in iterableIdsPageInstance}
    thumbsCache = caches['usersListThumbs']
    itemsDictMany = thumbsCache.get_many(iterableIds)
    itemsDictKeys = itemsDictMany.keys()
    idsToGetThumbItem = iterableIds - itemsDictKeys
    isSetThumbItems = False
    textDict = None
    viewFriendsText = None
    for id in itemsDictKeys:
        itemsDict = itemsDictMany[id]
        itemDict = itemsDict.get(requestUserId, None)
        if itemDict is None:
            idsToGetThumbItem.add(id)
        elif itemDict is False:
            iterableIds.remove(id)
        else:
            if id == requestUserId:
                options = itemDict.pop('usersList')
                if options is None:
                    textDict = usersListText(lang = 'ru').getDict()
                    viewFriendsText = textDict['viewFriends']
                    options = '<div class = "actions"><ul><li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li></ul></div></li>'.format(reverse('friends', kwargs = {'user': 'my', 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                    itemDict.update({'usersList': options})
                    thumbItemsToSet = {id: itemsDict}
                result.append((itemDict['thumb'], options))
                iterableIds.remove(id)
            else:
                if 'usersList' not in itemDict.keys():
                    idsToGetThumbItem.add(id)
    if len(idsToGetThumbItem) > 0:
        isSetThumbItems = True
        if thumbItemsToSet is None:
            thumbItemsToSet = {}
        addRequestText = None
        confirmText = None
        cancelText = None
        rejectText = None
        removeText = None
        sendMessageText = None
        userItems = mainUserProfile.objects.filter(id__in = idsToGetThumbItem)
        thpsProfiles = THPSProfile.objects.filter(toUser_id__in = idsToGetThumbItem)
        if textDict is None:
            textDict = usersListText(lang = 'ru').getDict()
        if requestUserId in idsToGetThumbItem:
            if viewFriendsText is None:
                viewFriendsText = textDict['viewFriends']
            user = userItems.get(id__exact = requestUserId)
            profilePath = reverse("profile", kwargs = {'currentUser': user.profileUrl})
            firstName = user.first_name or None
            lastName = user.last_name or None
            if firstName and lastName:
                name = '{0} {1}'.format(firstName, lastName)
                smallName = None
            else:
                if firstName:
                    name = firstName
                    smallName = user.username
                else:
                    name = user.username
                    smallName = None
            avatar = user.avatarThumbMiddle or None
            if avatar is None:
                if smallName:
                    itemString = '<li value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(requestUserId, name, smallName, profilePath)
                else:
                    itemString = '<li value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(requestUserId, name, profilePath)
            else:
                if smallName:
                    itemString = '<li value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(requestUserId, profilePath, avatar.url, name, smallName)
                else:
                    itemString = '<li value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(requestUserId, profilePath, avatar.url, name)
            try:
                thpsProfile = thpsProfiles.get(toUser__exact = user)
            except:
                thpsProfile = None
            if thpsProfile is not None:
                clan = thpsProfile.clan or None
                if clan is not None:
                    thpsAvatar = thpsProfile.avatarThumbMiddle or None
                    if thpsAvatar is not None:
                        itemString += '<div class = "thps-avatar-wrap"><a href = "{0}"><img src = "{1}" /></a></div><p>{2}*{3}</p>'.format(profilePath, thpsAvatar.url, thpsProfile.nickName, clan)
                    else:
                        itemString += '<a href = "{0}"><div class = "thps-no-avatar-wrap"><small>{1}*{2}</small></div></a><p>{1}*{2}</p>'.format(profilePath, thpsProfile.nickName, clan)
                else:
                    thpsAvatar = thpsProfile.avatarThumbMiddle or None
                    if thpsAvatar is not None:
                        itemString += '<div class = "thps-avatar-wrap"><a href = "{0}"><img src = "{1}" /></a></div><p>{2}</p>'.format(profilePath, thpsAvatar.url, thpsProfile.nickName)
                    else:
                        itemString += '<a href = "{0}"><div class = "thps-no-avatar-wrap"><small>{1}</small></div></a><p>{1}</p>'.format(profilePath, thpsProfile.nickName)
            options = '<div class = "actions"><ul><li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li></ul></div></li>'.format(reverse('friends', kwargs = {'user': 'my', 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
            itemsDict = itemsDictMany.get(requestUserId, None)
            if itemsDict is None:
                thumbItemsToSet.update({requestUserId: {requestUserId: {'thumb': itemString, 'usersList': options, 'showInOnlineList': True, 'showInActiveList': True}}})
            else:
                itemsDict.update({requestUserId: {'thumb': itemString, 'usersList': options, 'showInOnlineList': True, 'showInActiveList': True}})
                thumbItemsToSet.update({requestUserId: itemsDict})
            result.append((itemString, options))
            iterableIds.remove(requestUserId)
            idsToGetThumbItem.remove(requestUserId)
        friendsIdsCache = caches['friendsIdCache']
        friendsIds = friendsIdsCache.get(requestUserId, None)
        if friendsIds is None:
            friendsCache = caches['friendsCache']
            userFriends = friendsCache.get(requestUserId, None)
            if userFriends is None:
                userFriends = requestUser.friends.all()
                if userFriends.count() > 0:
                    userFriendsMany = userFriends.values("id")
                    friendsIds = {user.get("id") for user in userFriendsMany}
                    friendsCache.set(requestUserId, userFriends)
                else:
                    friendsIds = set()
                    friendsCache.set(requestUserId, False)
            elif userFriends is False:
                friendsIds = set()
            else:
                if userFriends.count() > 0:
                    userFriendsMany = userFriends.values("id")
                    friendsIds = {user.get("id") for user in userFriendsMany}
                else:
                    friendsIds = set()
            friendsIdsCache.set(requestUserId, friendsIds)
        friendShipToGet = idsToGetThumbItem - friendsIds
        isFriendShipToGet = len(friendShipToGet) > 0
        if isFriendShipToGet is True:
            friendShipCache = caches['friendShipStatusCache']
            friendShipDictMany = friendShipCache.get_many(friendShipToGet)
            friendShipDictKeys = friendShipDictMany.keys()
            friendShipRequestsToGet = friendShipToGet - friendShipDictKeys
            for id in friendShipDictKeys:
                if requestUserId not in friendShipDictMany[id].keys():
                    friendShipRequestsToGet.add(id)
            isFriendShipRequestsToGet = len(friendShipRequestsToGet) > 0
            if isFriendShipRequestsToGet is True:
                friendShipStatusToSet = {}
                friendShipRequestsCache = caches['friendshipRequests']
                friendShipRequestsToGet.add(requestUserId)
                friendShipRequestDictList = friendShipRequestsCache.get_many(friendShipRequestsToGet)
        privacyCache = caches['privacyCache']
        privacyDictMany = privacyCache.get_many(idsToGetThumbItem)
        privacyDictManyKeys = privacyDictMany.keys()
        privacyToGet = idsToGetThumbItem - privacyDictManyKeys
        for id in privacyDictManyKeys:
            privacyDict = privacyDictMany[id].get(requestUserId, None)
            if privacyDict is None:
                privacyToGet.add(id)
            else:
                privacyDictKeys = privacyDict.keys()
                if 'viewProfile' not in privacyDictKeys or 'viewMainInfo' not in privacyDictKeys or 'viewTHPSInfo' not in privacyDictKeys or 'viewAvatar' not in privacyDictKeys or 'sendFriendRequests' not in privacyDictKeys or 'sendPersonalMessages' not in privacyDictKeys or 'viewFriends' not in privacyDictKeys or 'viewOnlineStatus' not in privacyDictKeys or 'viewActiveStatus' not in privacyDictKeys:
                    privacyToGet.add(id)
        isPrivacyToSet = len(privacyToGet) > 0
        if isPrivacyToSet is True:
            privacyInstances = userProfilePrivacy.objects.filter(user_id__in = privacyToGet)
            privacyToSet = {}
    onlineUsers = caches['onlineUsers'].get_many(iterableIds).keys()
    iffyMany = caches['iffyUsersOnlineStatus'].get_many((iterableIds - onlineUsers))
    activeUsers = caches['chatStatusActive'].get_many((iffyMany.keys() | onlineUsers)).keys()
    middleMany = caches['chatStatusMiddle'].get_many((iterableIds - activeUsers))

    for id in iterableIds:
        itemsDict = itemsDictMany.get(id, None)
        if itemsDict is None:
            if id in friendsIds:
                friendShipStatus = True
            else:
                friendShipDict = friendShipDictMany.get(id, None)
                if friendShipDict is None:
                    friendShipStatus = False
                    friendShipRequestList = friendShipRequestDictList.get(id, None)
                    if friendShipRequestList is not None:
                        for request in friendShipRequestList:
                            if request[0] == requestUserId:
                                friendShipStatus = "myRequest"
                                break
                    if friendShipStatus is False:
                        friendShipRequestList = friendShipRequestDictList.get(requestUserId, None)
                        if friendShipRequestList is not None:
                            for request in friendShipRequestList:
                                if request[0] == id:
                                    friendShipStatus = "request"
                                    break
                    friendShipStatusToSet.update({id: {requestUserId: friendShipStatus}})
                else:
                    friendShipStatus = friendShipDict.get(requestUserId, None)
                    if friendShipStatus is None:
                        friendShipStatus = False
                        friendShipRequestList = friendShipRequestDictList.get(id, None)
                        if friendShipRequestList is not None:
                            for request in friendShipRequestList:
                                if request[0] == requestUserId:
                                    friendShipStatus = "myRequest"
                                    break
                        if friendShipStatus is False:
                            friendShipRequestList = friendShipRequestDictList.get(requestUserId, None)
                            if friendShipRequestList is not None:
                                for request in friendShipRequestList:
                                    if request[0] == id:
                                        friendShipStatus = "request"
                                        break
                        friendShipDict.update({requestUserId: friendShipStatus})
                        friendShipStatusToSet.update({id: friendShipDict})
            privacyDict = privacyDictMany.get(id, None)
            if privacyDict is None:
                privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                viewProfile = privacyChecker.checkViewProfilePrivacy()
                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                viewAvatar = privacyChecker.checkAvatarPrivacy()
                sendFriendRequests = privacyChecker.sendFriendRequests()
                sendPersonalMessages = privacyChecker.sendPersonalMessages()
                viewFriends = privacyChecker.checkFriendsListPrivacy()
                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                privacyToSet.update({id: {requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
            else:
                userPrivacyDict = privacyDict.get(requestUserId, None)
                if userPrivacyDict is None:
                    privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    sendFriendRequests = privacyChecker.sendFriendRequests()
                    sendPersonalMessages = privacyChecker.sendPersonalMessages()
                    viewFriends = privacyChecker.checkFriendsListPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                    privacyDict.update({requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
                    privacyToSet.update({id: privacyDict})
                else:
                    viewProfile = userPrivacyDict.get('viewProfile', None)
                    viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                    viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                    viewAvatar = userPrivacyDict.get('viewAvatar', None)
                    sendFriendRequests = userPrivacyDict.get('sendFriendRequests', None)
                    sendPersonalMessages = userPrivacyDict.get('sendPersonalMessages', None)
                    viewFriends = userPrivacyDict.get('viewFriends', None)
                    viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                    viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                    if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or sendFriendRequests is None or sendPersonalMessages is None or viewFriends is None or viewOnlineStatus is None or viewActiveStatus is None:
                        privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                        if viewProfile is None:
                            viewProfile = privacyChecker.checkViewProfilePrivacy()
                            userPrivacyDict.update({'viewProfile': viewProfile})
                        if viewMainInfo is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                        if viewTHPSInfo is None:
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                        if viewAvatar is None:
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            userPrivacyDict.update({'viewAvatar': viewAvatar})
                        if viewOnlineStatus is None:
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                        if viewActiveStatus is None:
                            viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                            userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                        if sendFriendRequests is None:
                            sendFriendRequests = privacyChecker.sendFriendRequests()
                            userPrivacyDict.update({'sendFriendRequests': sendFriendRequests})
                        if sendPersonalMessages is None:
                            sendPersonalMessages = privacyChecker.sendPersonalMessages()
                            userPrivacyDict.update({'sendPersonalMessages': sendPersonalMessages})
                        if viewFriends is None:
                            viewFriends = privacyChecker.checkFriendsListPrivacy()
                            userPrivacyDict.update({'viewFriends': viewFriends})
                        privacyToSet.update({id: privacyDict})
            if viewProfile:
                user = userItems.get(id__exact = id)
                profileSlug = user.profileUrl
                profilePath = reverse("profile", kwargs = {'currentUser': profileSlug})
                if viewMainInfo:
                    firstName = user.first_name or None
                    lastName = user.last_name or None
                    if firstName and lastName:
                        name = '{0} {1}'.format(firstName, lastName)
                        smallName = user.username
                    else:
                        if firstName:
                            name = firstName
                            smallName = user.username
                        else:
                            name = user.username
                            smallName = None
                else:
                    name = user.username
                    smallName = None
                if viewAvatar:
                    avatar = user.avatarThumbMiddle or None
                    if avatar is not None:
                        if smallName:
                            itemString0 = '<li value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(id, profilePath, avatar.url, name, smallName)
                        else:
                            itemString0 = '<li value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(id, profilePath, avatar.url, name)
                    else:
                        if smallName:
                            itemString0 = '<li value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(id, name, smallName, profilePath)
                        else:
                            itemString0 = '<li value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(id, name, profilePath)
                else:
                    if smallName:
                        itemString0 = '<li value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(id, name, smallName, profilePath)
                    else:
                        itemString0 = '<li value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(id, name, profilePath)
                if viewTHPSInfo:
                    try:
                        thpsProfile = thpsProfiles.get(toUser__exact = user)
                    except:
                        thpsProfile = None
                    if thpsProfile is not None:
                        thpsItem = {}
                        nick = thpsProfile.nickName
                        clan = thpsProfile.clan or None
                        if clan is not None:
                            thpsItem.update({'nick': nick, 'clan': clan})
                        else:
                            thpsItem.update({'nick': nick})
                        thpsAvatar = thpsProfile.avatarThumbMiddle or None
                        if thpsAvatar is not None:
                            thpsItem.update({'avatar': thpsProfile})
                        item.update({'thps': thpsItem})
                if friendShipStatus is True:
                    if removeText is None:
                        removeText = textDict['removeFromFriends']
                    itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("removeFriend", kwargs = {'userId': id}), removeText)
                    if viewFriends:
                        if viewFriendsText is None:
                            viewFriendsText = textDict['viewFriends']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                    if sendPersonalMessages:
                        if sendMessageText is None:
                            sendMessageText = textDict['personalmessage']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                    itemString1 += '</ul></div></li>'
                elif friendShipStatus == "request":
                    if confirmText is None:
                        confirmText = textDict['confirm']
                    if rejectText is None:
                        rejectText = textDict['reject']
                    itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li><li><form method = "post" action = "{2}"><input type = "submit" value = "{3}"></form></li>'.format(reverse("addFriend", kwargs = {'userId': id}), confirmText, reverse("rejectFriend", kwargs = {'userId': id}), rejectText)
                    if viewFriends:
                        if viewFriendsText is None:
                            viewFriendsText = textDict['viewFriends']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                    if sendPersonalMessages:
                        if sendMessageText is None:
                            sendMessageText = textDict['personalmessage']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                    itemString1 += '</ul></div></li>'
                elif friendShipStatus == "myRequest":
                    if cancelText is None:
                        cancelText = textDict['cancel']
                    itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("cancelAddFriendRequest", kwargs = {'userId': id}), cancelText)
                    if viewFriends:
                        if viewFriendsText is None:
                            viewFriendsText = textDict['viewFriends']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                    if sendPersonalMessages:
                        if sendMessageText is None:
                            sendMessageText = textDict['personalmessage']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                    itemString1 += '</ul></div></li>'
                else:
                    if sendFriendRequests:
                        if addRequestText is None:
                            addRequestText = textDict['addToFriends']
                        itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("addFriendRequest", kwargs = {'userId': id}), addRequestText)
                        if viewFriends:
                            if viewFriendsText is None:
                                viewFriendsText = textDict['viewFriends']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                        if sendPersonalMessages:
                            if sendMessageText is None:
                                sendMessageText = textDict['personalmessage']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                        itemString1 += '</ul></div></li>'
                    else:
                        itemString1 = '</li>'
                if viewOnlineStatus and viewActiveStatus:
                    if id in onlineUsers:
                        onlineUsersSet.add(id)
                        if id in activeUsers:
                            activeUsersSet.add(id)
                        else:
                            middleTupleStr = middleMany.pop(id, None)
                            if middleTupleStr is not None:
                                middleUsersDict.update({id: middleTupleStr})
                    else:
                        iffyTimeStr = iffyMany.pop(id, None)
                        if iffyTimeStr is not None:
                            iffyUsersDict.update({id: iffyTimeStr})
                            if id in activeUsers:
                                activeUsersSet.add(id)
                            else:
                                middleTupleStr = middleMany.pop(id, None)
                                if middleTupleStr is not None:
                                    middleUsersDict.update({id: middleTupleStr})
                else:
                    if viewOnlineStatus:
                        if id in onlineUsers:
                            onlineUsersSet.add(id)
                        else:
                            iffyTimeStr = iffyMany.pop(id, None)
                            if iffyTimeStr is not None:
                                iffyUsersDict.update({id: iffyTimeStr})
                        hiddenActiveUsersSet.add(id)
                    elif viewActiveStatus:
                        if id in activeUsers:
                            activeUsersSet.add(id)
                        else:
                            middleTupleStr = middleMany.pop(id, None)
                            if middleTupleStr is not None:
                                middleUsersDict.update({id: middleTupleStr})
                        hiddenOnlineUsersSet.add(id)
                    else:
                        hiddenActiveUsersSet.add(id)
                        hiddenOnlineUsersSet.add(id)
                result.append((itemString0, itemString1))
                thumbItemsToSet.update({id: {requestUserId: {'thumb': itemString0, 'usersList': itemString1, 'showInOnlineList': viewOnlineStatus, 'showInActiveList': viewActiveStatus}}})
            else:
                thumbItemsToSet.update({id: {requestUserId: False}})
        else:
            item = itemsDict.get(requestUserId, None)
            if item is None:
                if id in friendsIds:
                    friendShipStatus = True
                else:
                    friendShipDict = friendShipDictMany.get(id, None)
                    if friendShipDict is None:
                        friendShipStatus = False
                        friendShipRequestList = friendShipRequestDictList.get(id, None)
                        if friendShipRequestList is not None:
                            for request in friendShipRequestList:
                                if request[0] == requestUserId:
                                    friendShipStatus = "myRequest"
                                    break
                        if friendShipStatus is False:
                            friendShipRequestList = friendShipRequestDictList.get(requestUserId, None)
                            if friendShipRequestList is not None:
                                for request in friendShipRequestList:
                                    if request[0] == id:
                                        friendShipStatus = "request"
                                        break
                        friendShipStatusToSet.update({id: {requestUserId: friendShipStatus}})
                    else:
                        friendShipStatus = friendShipDict.get(requestUserId, None)
                        if friendShipStatus is None:
                            friendShipStatus = False
                            friendShipRequestList = friendShipRequestDictList.get(id, None)
                            if friendShipRequestList is not None:
                                for request in friendShipRequestList:
                                    if request[0] == requestUserId:
                                        friendShipStatus = "myRequest"
                                        break
                            if friendShipStatus is False:
                                friendShipRequestList = friendShipRequestDictList.get(requestUserId, None)
                                if friendShipRequestList is not None:
                                    for request in friendShipRequestList:
                                        if request[0] == id:
                                            friendShipStatus = "request"
                                            break
                            friendShipDict.update({requestUserId: friendShipStatus})
                            friendShipStatusToSet.update({id: friendShipDict})
                privacyDict = privacyDictMany.get(id, None)
                if privacyDict is None:
                    privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    sendFriendRequests = privacyChecker.sendFriendRequests()
                    sendPersonalMessages = privacyChecker.sendPersonalMessages()
                    viewFriends = privacyChecker.checkFriendsListPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                    privacyToSet.update({id: {requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
                else:
                    userPrivacyDict = privacyDict.get(requestUserId, None)
                    if userPrivacyDict is None:
                        privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                        viewProfile = privacyChecker.checkViewProfilePrivacy()
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        sendFriendRequests = privacyChecker.sendFriendRequests()
                        sendPersonalMessages = privacyChecker.sendPersonalMessages()
                        viewFriends = privacyChecker.checkFriendsListPrivacy()
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                        privacyDict.update({requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
                        privacyToSet.update({id: privacyDict})
                    else:
                        viewProfile = userPrivacyDict.get('viewProfile', None)
                        viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                        viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                        viewAvatar = userPrivacyDict.get('viewAvatar', None)
                        sendFriendRequests = userPrivacyDict.get('sendFriendRequests', None)
                        sendPersonalMessages = userPrivacyDict.get('sendPersonalMessages', None)
                        viewFriends = userPrivacyDict.get('viewFriends', None)
                        viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                        viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                        if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or sendFriendRequests is None or sendPersonalMessages is None or viewFriends is None or viewOnlineStatus is None or viewActiveStatus is None:
                            privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                            if viewProfile is None:
                                viewProfile = privacyChecker.checkViewProfilePrivacy()
                                userPrivacyDict.update({'viewProfile': viewProfile})
                            if viewMainInfo is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                            if viewTHPSInfo is None:
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                            if viewAvatar is None:
                                viewAvatar = privacyChecker.checkAvatarPrivacy()
                                userPrivacyDict.update({'viewAvatar': viewAvatar})
                            if viewOnlineStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                            if viewActiveStatus is None:
                                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                            if sendFriendRequests is None:
                                sendFriendRequests = privacyChecker.sendFriendRequests()
                                userPrivacyDict.update({'sendFriendRequests': sendFriendRequests})
                            if sendPersonalMessages is None:
                                sendPersonalMessages = privacyChecker.sendPersonalMessages()
                                userPrivacyDict.update({'sendPersonalMessages': sendPersonalMessages})
                            if viewFriends is None:
                                viewFriends = privacyChecker.checkFriendsListPrivacy()
                                userPrivacyDict.update({'viewFriends': viewFriends})
                            privacyToSet.update({id: privacyDict})
                if viewProfile:
                    user = userItems.get(id__exact = id)
                    profileSlug = user.profileUrl
                    profilePath = reverse("profile", kwargs = {'currentUser': profileSlug})
                    if viewMainInfo:
                        firstName = user.first_name or None
                        lastName = user.last_name or None
                        if firstName and lastName:
                            name = '{0} {1}'.format(firstName, lastName)
                            smallName = user.username
                        else:
                            if firstName:
                                name = firstName
                                smallName = user.username
                            else:
                                name = user.username
                                smallName = None
                    else:
                        name = user.username
                        smallName = None
                    if viewAvatar:
                        avatar = user.avatarThumbMiddle or None
                        if avatar is not None:
                            if smallName:
                                itemString0 = '<li value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(id, profilePath, avatar.url, name, smallName)
                            else:
                                itemString0 = '<li value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(id, profilePath, avatar.url, name)
                        else:
                            if smallName:
                                itemString0 = '<li value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(id, name, smallName, profilePath)
                            else:
                                itemString0 = '<li value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(id, name, profilePath)
                    else:
                        if smallName:
                            itemString0 = '<li value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(id, name, smallName, profilePath)
                        else:
                            itemString0 = '<li value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(id, name, profilePath)
                    if viewTHPSInfo:
                        try:
                            thpsProfile = thpsProfiles.get(toUser__exact = user)
                        except:
                            thpsProfile = None
                        if thpsProfile is not None:
                            thpsItem = {}
                            nick = thpsProfile.nickName
                            clan = thpsProfile.clan or None
                            if clan is not None:
                                thpsItem.update({'nick': nick, 'clan': clan})
                            else:
                                thpsItem.update({'nick': nick})
                            thpsAvatar = thpsProfile.avatarThumbMiddle or None
                            if thpsAvatar is not None:
                                thpsItem.update({'avatar': thpsProfile})
                            item.update({'thps': thpsItem})
                    if friendShipStatus is True:
                        if removeText is None:
                            removeText = textDict['removeFromFriends']
                        itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("removeFriend", kwargs = {'userId': id}), removeText)
                        if viewFriends:
                            if viewFriendsText is None:
                                viewFriendsText = textDict['viewFriends']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                        if sendPersonalMessages:
                            if sendMessageText is None:
                                sendMessageText = textDict['personalmessage']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                        itemString1 += '</ul></div></li>'
                    elif friendShipStatus == "request":
                        if confirmText is None:
                            confirmText = textDict['confirm']
                        if rejectText is None:
                            rejectText = textDict['reject']
                        itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li><li><form method = "post" action = "{2}"><input type = "submit" value = "{3}"></form></li>'.format(reverse("addFriend", kwargs = {'userId': id}), confirmText, reverse("rejectFriend", kwargs = {'userId': id}), rejectText)
                        if viewFriends:
                            if viewFriendsText is None:
                                viewFriendsText = textDict['viewFriends']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                        if sendPersonalMessages:
                            if sendMessageText is None:
                                sendMessageText = textDict['personalmessage']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                        itemString1 += '</ul></div></li>'
                    elif friendShipStatus == "myRequest":
                        if cancelText is None:
                            cancelText = textDict['cancel']
                        itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("cancelAddFriendRequest", kwargs = {'userId': id}), cancelText)
                        if viewFriends:
                            if viewFriendsText is None:
                                viewFriendsText = textDict['viewFriends']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                        if sendPersonalMessages:
                            if sendMessageText is None:
                                sendMessageText = textDict['personalmessage']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                        itemString1 += '</ul></div></li>'
                    else:
                        if sendFriendRequests:
                            if addRequestText is None:
                                addRequestText = textDict['addToFriends']
                            itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("addFriendRequest", kwargs = {'userId': id}), addRequestText)
                            if viewFriends:
                                if viewFriendsText is None:
                                    viewFriendsText = textDict['viewFriends']
                                itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                            if sendPersonalMessages:
                                if sendMessageText is None:
                                    sendMessageText = textDict['personalmessage']
                                itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                            itemString1 += '</ul></div></li>'
                        else:
                            itemString1 = '</li>'
                    if viewOnlineStatus and viewActiveStatus:
                        if id in onlineUsers:
                            onlineUsersSet.add(id)
                            if id in activeUsers:
                                activeUsersSet.add(id)
                            else:
                                middleTupleStr = middleMany.pop(id, None)
                                if middleTupleStr is not None:
                                    middleUsersDict.update({id: middleTupleStr})
                        else:
                            iffyTimeStr = iffyMany.pop(id, None)
                            if iffyTimeStr is not None:
                                iffyUsersDict.update({id: iffyTimeStr})
                                if id in activeUsers:
                                    activeUsersSet.add(id)
                                else:
                                    middleTupleStr = middleMany.pop(id, None)
                                    if middleTupleStr is not None:
                                        middleUsersDict.update({id: middleTupleStr})
                    else:
                        if viewOnlineStatus:
                            if id in onlineUsers:
                                onlineUsersSet.add(id)
                            else:
                                iffyTimeStr = iffyMany.pop(id, None)
                                if iffyTimeStr is not None:
                                    iffyUsersDict.update({id: iffyTimeStr})
                            hiddenActiveUsersSet.add(id)
                        elif viewActiveStatus:
                            if id in activeUsers:
                                activeUsersSet.add(id)
                            else:
                                middleTupleStr = middleMany.pop(id, None)
                                if middleTupleStr is not None:
                                    middleUsersDict.update({id: middleTupleStr})
                            hiddenOnlineUsersSet.add(id)
                        else:
                            hiddenActiveUsersSet.add(id)
                            hiddenOnlineUsersSet.add(id)
                    result.append((itemString0, itemString1))
                    itemsDict.update({requestUserId: {'thumb': itemString0, 'usersList': itemString1, 'showInOnlineList': viewOnlineStatus, 'showInActiveList': viewActiveStatus}})
                    thumbItemsToSet.update({id: itemsDict})
                else:
                    itemsDict.update({requestUserId: False})
                    thumbItemsToSet.update({id: itemsDict})
            else:
                if item is not False:
                    options = item.pop('usersList', None)
                    showOnlineStatus = item['showInOnlineList']
                    showActiveStatus = item['showInActiveList']
                    if options is None:
                        if id in friendsIds:
                            friendShipStatus = True
                        else:
                            friendShipDict = friendShipDictMany.get(id, None)
                            if friendShipDict is None:
                                friendShipStatus = False
                                friendShipRequestList = friendShipRequestDictList.get(id, None)
                                if friendShipRequestList is not None:
                                    for request in friendShipRequestList:
                                        if request[0] == requestUserId:
                                            friendShipStatus = "myRequest"
                                            break
                                if friendShipStatus is False:
                                    friendShipRequestList = friendShipRequestDictList.get(requestUserId, None)
                                    if friendShipRequestList is not None:
                                        for request in friendShipRequestList:
                                            if request[0] == id:
                                                friendShipStatus = "request"
                                                break
                                friendShipStatusToSet.update({id: {requestUserId: friendShipStatus}})
                            else:
                                friendShipStatus = friendShipDict.get(requestUserId, None)
                                if friendShipStatus is None:
                                    friendShipStatus = False
                                    friendShipRequestList = friendShipRequestDictList.get(id, None)
                                    if friendShipRequestList is not None:
                                        for request in friendShipRequestList:
                                            if request[0] == requestUserId:
                                                friendShipStatus = "myRequest"
                                                break
                                    if friendShipStatus is False:
                                        friendShipRequestList = friendShipRequestDictList.get(requestUserId, None)
                                        if friendShipRequestList is not None:
                                            for request in friendShipRequestList:
                                                if request[0] == id:
                                                    friendShipStatus = "request"
                                                    break
                                    friendShipDict.update({requestUserId: friendShipStatus})
                                    friendShipStatusToSet.update({id: friendShipDict})
                        privacyDict = privacyDictMany.get(id, None)
                        if privacyDict is None:
                            privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                            sendFriendRequests = privacyChecker.sendFriendRequests()
                            sendPersonalMessages = privacyChecker.sendPersonalMessages()
                            viewFriends = privacyChecker.checkFriendsListPrivacy()
                            privacyToSet.update({id: {requestUserId: {'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends}}})
                        else:
                            userPrivacyDict = privacyDict.get(requestUserId, None)
                            if userPrivacyDict is None:
                                privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                                sendFriendRequests = privacyChecker.sendFriendRequests()
                                sendPersonalMessages = privacyChecker.sendPersonalMessages()
                                viewFriends = privacyChecker.checkFriendsListPrivacy()
                                privacyDict.update({requestUserId: {'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends}})
                                privacyToSet.update({id: privacyDict})
                            else:
                                sendFriendRequests = userPrivacyDict.get('sendFriendRequests', None)
                                sendPersonalMessages = userPrivacyDict.get('sendPersonalMessages', None)
                                viewFriends = userPrivacyDict.get('viewFriends', None)
                                if sendFriendRequests is None or sendPersonalMessages is None or viewFriends is None:
                                    privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                                    if sendFriendRequests is None:
                                        sendFriendRequests = privacyChecker.sendFriendRequests()
                                        userPrivacyDict.update({'sendFriendRequests': sendFriendRequests})
                                    if sendPersonalMessages is None:
                                        sendPersonalMessages = privacyChecker.sendPersonalMessages()
                                        userPrivacyDict.update({'sendPersonalMessages': sendPersonalMessages})
                                    if viewFriends is None:
                                        viewFriends = privacyChecker.checkFriendsListPrivacy()
                                        userPrivacyDict.update({'viewFriends': viewFriends})
                                    privacyToSet.update({id: privacyDict})
                        profileSlug = user.profileUrl
                        if friendShipStatus is True:
                            if removeText is None:
                                removeText = textDict['removeFromFriends']
                            options = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("removeFriend", kwargs = {'userId': id}), removeText)
                            if viewFriends:
                                if viewFriendsText is None:
                                    viewFriendsText = textDict['viewFriends']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                            if sendPersonalMessages:
                                if sendMessageText is None:
                                    sendMessageText = textDict['personalmessage']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                            options += '</ul></div></li>'
                        elif friendShipStatus == "request":
                            if confirmText is None:
                                confirmText = textDict['confirm']
                            if rejectText is None:
                                rejectText = textDict['reject']
                            options = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li><li><form method = "post" action = "{2}"><input type = "submit" value = "{3}"></form></li>'.format(reverse("addFriend", kwargs = {'userId': id}), confirmText, reverse("rejectFriend", kwargs = {'userId': id}), rejectText)
                            if viewFriends:
                                if viewFriendsText is None:
                                    viewFriendsText = textDict['viewFriends']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                            if sendPersonalMessages:
                                if sendMessageText is None:
                                    sendMessageText = textDict['personalmessage']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                            options += '</ul></div></li>'
                        elif friendShipStatus == "myRequest":
                            if cancelText is None:
                                cancelText = textDict['cancel']
                            options = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("cancelAddFriendRequest", kwargs = {'userId': id}), cancelText)
                            if viewFriends:
                                if viewFriendsText is None:
                                    viewFriendsText = textDict['viewFriends']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                            if sendPersonalMessages:
                                if sendMessageText is None:
                                    sendMessageText = textDict['personalmessage']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                            options += '</ul></div></li>'
                        else:
                            if sendFriendRequests:
                                if addRequestText is None:
                                    addRequestText = textDict['addToFriends']
                                options = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("addFriendRequest", kwargs = {'userId': id}), addRequestText)
                                if viewFriends:
                                    if viewFriendsText is None:
                                        viewFriendsText = textDict['viewFriends']
                                    options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                                if sendPersonalMessages:
                                    if sendMessageText is None:
                                        sendMessageText = textDict['personalmessage']
                                    options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                                options += '</ul></div></li>'
                            else:
                                options = '</li>'
                        item.update({'usersList': options})
                        thumbItemsToSet.update({id: itemsDict})
                    if showOnlineStatus and showActiveStatus:
                        if id in onlineUsers:
                            onlineUsersSet.add(id)
                            if id in activeUsers:
                                activeUsersSet.add(id)
                            else:
                                middleTupleStr = middleMany.pop(id, None)
                                if middleTupleStr is not None:
                                    middleUsersDict.update({id: middleTupleStr})
                        else:
                            iffyTimeStr = iffyMany.pop(id, None)
                            if iffyTimeStr is not None:
                                iffyUsersDict.update({id: iffyTimeStr})
                                if id in activeUsers:
                                    activeUsersSet.add(id)
                                else:
                                    middleTupleStr = middleMany.pop(id, None)
                                    if middleTupleStr is not None:
                                        middleUsersDict.update({id: middleTupleStr})
                    else:
                        if showOnlineStatus:
                            if id in onlineUsers:
                                onlineUsersSet.add(id)
                            else:
                                iffyTimeStr = iffyMany.pop(id, None)
                                if iffyTimeStr is not None:
                                    iffyUsersDict.update({id: iffyTimeStr})
                            hiddenActiveUsersSet.add(id)
                        elif showActiveStatus:
                            if id in activeUsers:
                                activeUsersSet.add(id)
                            else:
                                middleTupleStr = middleMany.pop(id, None)
                                if middleTupleStr is not None:
                                    middleUsersDict.update({id: middleTupleStr})
                            hiddenOnlineUsersSet.add(id)
                        else:
                            hiddenActiveUsersSet.add(id)
                            hiddenOnlineUsersSet.add(id)
                    result.append((item['thumb'], options))

    if isSetThumbItems is True:
        if isPrivacyToSet is True:
            privacyCache.set_many(privacyToSet)
        if isFriendShipToGet is True and isFriendShipRequestsToGet is True:
            friendShipCache.set_many(friendShipStatusToSet)
        thumbsCache.set_many(thumbItemsToSet)

    if len(onlineUsersSet) > 0:
        statusDict.update({'stateOnline': list(onlineUsersSet)})
    if len(iffyUsersDict.keys()) > 0:
        statusDict.update({'stateTimer': iffyUsersDict})
    if len(activeUsersSet) > 0:
        statusDict.update({'stateActive': list(activeUsersSet)})
    if len(middleUsersDict) > 0:
        statusDict.update({'stateMiddle': middleUsersDict})

    if len(hiddenOnlineUsersSet) > 0:
        statusDict.update({'hiddenOnline': list(hiddenOnlineUsersSet)})
    if len(hiddenActiveUsersSet) > 0:
        statusDict.update({'hiddenActive': list(hiddenActiveUsersSet)})

    return statusDict, result

def createUsersListOnline(iterableIdsPageInstance, requestUser, requestUserId, iffyCache, iffyUsers, onlineUsers):
    result = []
    statusDict = {}
    onlineUsersSet = set()
    iffyUsersDict = {}
    activeUsersSet = set()
    hiddenActiveUsersSet = set()
    middleUsersDict = {}
    thumbItemsToSet = None
    thumbsCache = caches['usersListThumbs']
    iterableIds = set(iterableIdsPageInstance)
    itemsDictMany = thumbsCache.get_many(iterableIds)
    itemsDictKeys = itemsDictMany.keys()
    idsToGetThumbItem = iterableIds - itemsDictKeys
    isSetThumbItems = False
    textDict = None
    viewFriendsText = None
    for id in itemsDictKeys:
        itemsDict = itemsDictMany[id]
        itemDict = itemsDict.get(requestUserId, None)
        if itemDict is None:
            idsToGetThumbItem.add(id)
        elif itemDict is False:
            iterableIds.remove(id)
        else:
            if id == requestUserId:
                options = itemDict.pop('usersList')
                if options is None:
                    textDict = usersListText(lang = 'ru').getDict()
                    viewFriendsText = textDict['viewFriends']
                    options = '<div class = "actions"><ul><li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li></ul></div></li>'.format(reverse('friends', kwargs = {'user': 'my', 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                    itemDict.update({'usersList': options})
                    thumbItemsToSet = {id: itemsDict}
                result.append((itemDict['thumb'], options))
                iterableIds.remove(id)
            else:
                if 'usersList' not in itemDict.keys():
                    idsToGetThumbItem.add(id)
    if len(idsToGetThumbItem) > 0:
        isSetThumbItems = True
        if thumbItemsToSet is None:
            thumbItemsToSet = {}
        addRequestText = None
        confirmText = None
        cancelText = None
        rejectText = None
        removeText = None
        sendMessageText = None
        userItems = mainUserProfile.objects.filter(id__in = idsToGetThumbItem)
        thpsProfiles = THPSProfile.objects.filter(toUser_id__in = idsToGetThumbItem)
        if textDict is None:
            textDict = usersListText(lang = 'ru').getDict()
        if requestUserId in idsToGetThumbItem:
            if viewFriendsText is None:
                viewFriendsText = textDict['viewFriends']
            user = userItems.get(id__exact = requestUserId)
            profilePath = reverse("profile", kwargs = {'currentUser': user.profileUrl})
            firstName = user.first_name or None
            lastName = user.last_name or None
            if firstName and lastName:
                name = '{0} {1}'.format(firstName, lastName)
                smallName = None
            else:
                if firstName:
                    name = firstName
                    smallName = user.username
                else:
                    name = user.username
                    smallName = None
            avatar = user.avatarThumbMiddle or None
            if avatar is None:
                if smallName:
                    itemString = '<li value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(requestUserId, name, smallName, profilePath)
                else:
                    itemString = '<li value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(requestUserId, name, profilePath)
            else:
                if smallName:
                    itemString = '<li value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(requestUserId, profilePath, avatar.url, name, smallName)
                else:
                    itemString = '<li value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(requestUserId, profilePath, avatar.url, name)
            try:
                thpsProfile = thpsProfiles.get(toUser__exact = user)
            except:
                thpsProfile = None
            if thpsProfile is not None:
                clan = thpsProfile.clan or None
                if clan is not None:
                    thpsAvatar = thpsProfile.avatarThumbMiddle or None
                    if thpsAvatar is not None:
                        itemString += '<div class = "thps-avatar-wrap"><a href = "{0}"><img src = "{1}" /></a></div><p>{2}*{3}</p>'.format(profilePath, thpsAvatar.url, thpsProfile.nickName, clan)
                    else:
                        itemString += '<a href = "{0}"><div class = "thps-no-avatar-wrap"><small>{1}*{2}</small></div></a><p>{1}*{2}</p>'.format(profilePath, thpsProfile.nickName, clan)
                else:
                    thpsAvatar = thpsProfile.avatarThumbMiddle or None
                    if thpsAvatar is not None:
                        itemString += '<div class = "thps-avatar-wrap"><a href = "{0}"><img src = "{1}" /></a></div><p>{2}</p>'.format(profilePath, thpsAvatar.url, thpsProfile.nickName)
                    else:
                        itemString += '<a href = "{0}"><div class = "thps-no-avatar-wrap"><small>{1}</small></div></a><p>{1}</p>'.format(profilePath, thpsProfile.nickName)
            options = '<div class = "actions"><ul><li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li></ul></div></li>'.format(reverse('friends', kwargs = {'user': 'my', 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
            itemsDict = itemsDictMany.get(requestUserId, None)
            if itemsDict is None:
                thumbItemsToSet.update({requestUserId: {requestUserId: {'thumb': itemString, 'usersList': options, 'showInOnlineList': True, 'showInActiveList': True}}})
            else:
                itemsDict.update({requestUserId: {'thumb': itemString, 'usersList': options, 'showInOnlineList': True, 'showInActiveList': True}})
                thumbItemsToSet.update({requestUserId: itemsDict})
            result.append((itemString, options))
            iterableIds.remove(requestUserId)
            idsToGetThumbItem.remove(requestUserId)
        friendsIdsCache = caches['friendsIdCache']
        friendsIds = friendsIdsCache.get(requestUserId, None)
        if friendsIds is None:
            friendsCache = caches['friendsCache']
            userFriends = friendsCache.get(requestUserId, None)
            if userFriends is None:
                userFriends = requestUser.friends.all()
                if userFriends.count() > 0:
                    userFriendsMany = userFriends.values("id")
                    friendsIds = {user.get("id") for user in userFriendsMany}
                    friendsCache.set(requestUserId, userFriends)
                else:
                    friendsIds = set()
                    friendsCache.set(requestUserId, False)
            elif userFriends is False:
                friendsIds = set()
            else:
                if userFriends.count() > 0:
                    userFriendsMany = userFriends.values("id")
                    friendsIds = {user.get("id") for user in userFriendsMany}
                else:
                    friendsIds = set()
            friendsIdsCache.set(requestUserId, friendsIds)
        friendShipToGet = idsToGetThumbItem - friendsIds
        isFriendShipToGet = len(friendShipToGet) > 0
        if isFriendShipToGet is True:
            friendShipCache = caches['friendShipStatusCache']
            friendShipDictMany = friendShipCache.get_many(friendShipToGet)
            friendShipDictKeys = friendShipDictMany.keys()
            friendShipRequestsToGet = friendShipToGet - friendShipDictKeys
            for id in friendShipDictKeys:
                if requestUserId not in friendShipDictMany[id].keys():
                    friendShipRequestsToGet.add(id)
            isFriendShipRequestsToGet = len(friendShipRequestsToGet) > 0
            if isFriendShipRequestsToGet is True:
                friendShipStatusToSet = {}
                friendShipRequestsCache = caches['friendshipRequests']
                friendShipRequestsToGet.add(requestUserId)
                friendShipRequestDictList = friendShipRequestsCache.get_many(friendShipRequestsToGet)
        privacyCache = caches['privacyCache']
        privacyDictMany = privacyCache.get_many(idsToGetThumbItem)
        privacyDictManyKeys = privacyDictMany.keys()
        privacyToGet = idsToGetThumbItem - privacyDictManyKeys
        for id in privacyDictManyKeys:
            privacyDict = privacyDictMany[id].get(requestUserId, None)
            if privacyDict is None:
                privacyToGet.add(id)
            else:
                privacyDictKeys = privacyDict.keys()
                if 'viewProfile' not in privacyDictKeys or 'viewMainInfo' not in privacyDictKeys or 'viewTHPSInfo' not in privacyDictKeys or 'viewAvatar' not in privacyDictKeys or 'sendFriendRequests' not in privacyDictKeys or 'sendPersonalMessages' not in privacyDictKeys or 'viewFriends' not in privacyDictKeys or 'viewOnlineStatus' not in privacyDictKeys or 'viewActiveStatus' not in privacyDictKeys:
                    privacyToGet.add(id)
        isPrivacyToSet = len(privacyToGet) > 0
        if isPrivacyToSet is True:
            privacyInstances = userProfilePrivacy.objects.filter(user_id__in = privacyToGet)
            privacyToSet = {}
    iffyMany = iffyCache.get_many((iterableIds & iffyUsers))
    activeUsers = caches['chatStatusActive'].get_many(iterableIds).keys()
    middleMany = caches['chatStatusMiddle'].get_many((iterableIds - activeUsers))

    for id in iterableIds:
        itemsDict = itemsDictMany.get(id, None)
        if itemsDict is None:
            if id in friendsIds:
                friendShipStatus = True
            else:
                friendShipDict = friendShipDictMany.get(id, None)
                if friendShipDict is None:
                    friendShipStatus = False
                    friendShipRequestList = friendShipRequestDictList.get(id, None)
                    if friendShipRequestList is not None:
                        for request in friendShipRequestList:
                            if request[0] == requestUserId:
                                friendShipStatus = "myRequest"
                                break
                    if friendShipStatus is False:
                        friendShipRequestList = friendShipRequestDictList.get(requestUserId, None)
                        if friendShipRequestList is not None:
                            for request in friendShipRequestList:
                                if request[0] == id:
                                    friendShipStatus = "request"
                                    break
                    friendShipStatusToSet.update({id: {requestUserId: friendShipStatus}})
                else:
                    friendShipStatus = friendShipDict.get(requestUserId, None)
                    if friendShipStatus is None:
                        friendShipStatus = False
                        friendShipRequestList = friendShipRequestDictList.get(id, None)
                        if friendShipRequestList is not None:
                            for request in friendShipRequestList:
                                if request[0] == requestUserId:
                                    friendShipStatus = "myRequest"
                                    break
                        if friendShipStatus is False:
                            friendShipRequestList = friendShipRequestDictList.get(requestUserId, None)
                            if friendShipRequestList is not None:
                                for request in friendShipRequestList:
                                    if request[0] == id:
                                        friendShipStatus = "request"
                                        break
                        friendShipDict.update({requestUserId: friendShipStatus})
                        friendShipStatusToSet.update({id: friendShipDict})
            privacyDict = privacyDictMany.get(id, None)
            if privacyDict is None:
                privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                viewProfile = privacyChecker.checkViewProfilePrivacy()
                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                viewAvatar = privacyChecker.checkAvatarPrivacy()
                sendFriendRequests = privacyChecker.sendFriendRequests()
                sendPersonalMessages = privacyChecker.sendPersonalMessages()
                viewFriends = privacyChecker.checkFriendsListPrivacy()
                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                privacyToSet.update({id: {requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
            else:
                userPrivacyDict = privacyDict.get(requestUserId, None)
                if userPrivacyDict is None:
                    privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    sendFriendRequests = privacyChecker.sendFriendRequests()
                    sendPersonalMessages = privacyChecker.sendPersonalMessages()
                    viewFriends = privacyChecker.checkFriendsListPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                    privacyDict.update({requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
                    privacyToSet.update({id: privacyDict})
                else:
                    viewProfile = userPrivacyDict.get('viewProfile', None)
                    viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                    viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                    viewAvatar = userPrivacyDict.get('viewAvatar', None)
                    sendFriendRequests = userPrivacyDict.get('sendFriendRequests', None)
                    sendPersonalMessages = userPrivacyDict.get('sendPersonalMessages', None)
                    viewFriends = userPrivacyDict.get('viewFriends', None)
                    viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                    viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                    if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or sendFriendRequests is None or sendPersonalMessages is None or viewFriends is None or viewOnlineStatus is None or viewActiveStatus is None:
                        privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                        if viewProfile is None:
                            viewProfile = privacyChecker.checkViewProfilePrivacy()
                            userPrivacyDict.update({'viewProfile': viewProfile})
                        if viewMainInfo is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                        if viewTHPSInfo is None:
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                        if viewAvatar is None:
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            userPrivacyDict.update({'viewAvatar': viewAvatar})
                        if viewOnlineStatus is None:
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                        if viewActiveStatus is None:
                            viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                            userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                        if sendFriendRequests is None:
                            sendFriendRequests = privacyChecker.sendFriendRequests()
                            userPrivacyDict.update({'sendFriendRequests': sendFriendRequests})
                        if sendPersonalMessages is None:
                            sendPersonalMessages = privacyChecker.sendPersonalMessages()
                            userPrivacyDict.update({'sendPersonalMessages': sendPersonalMessages})
                        if viewFriends is None:
                            viewFriends = privacyChecker.checkFriendsListPrivacy()
                            userPrivacyDict.update({'viewFriends': viewFriends})
                        privacyToSet.update({id: privacyDict})
            if viewProfile:
                user = userItems.get(id__exact = id)
                profileSlug = user.profileUrl
                profilePath = reverse("profile", kwargs = {'currentUser': profileSlug})
                if viewMainInfo:
                    firstName = user.first_name or None
                    lastName = user.last_name or None
                    if firstName and lastName:
                        name = '{0} {1}'.format(firstName, lastName)
                        smallName = user.username
                    else:
                        if firstName:
                            name = firstName
                            smallName = user.username
                        else:
                            name = user.username
                            smallName = None
                else:
                    name = user.username
                    smallName = None
                if viewAvatar:
                    avatar = user.avatarThumbMiddle or None
                    if avatar is not None:
                        if smallName:
                            itemString0 = '<li value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(id, profilePath, avatar.url, name, smallName)
                        else:
                            itemString0 = '<li value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(id, profilePath, avatar.url, name)
                    else:
                        if smallName:
                            itemString0 = '<li value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(id, name, smallName, profilePath)
                        else:
                            itemString0 = '<li value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(id, name, profilePath)
                else:
                    if smallName:
                        itemString0 = '<li value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(id, name, smallName, profilePath)
                    else:
                        itemString0 = '<li value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(id, name, profilePath)
                if viewTHPSInfo:
                    try:
                        thpsProfile = thpsProfiles.get(toUser__exact = user)
                    except:
                        thpsProfile = None
                    if thpsProfile is not None:
                        thpsItem = {}
                        nick = thpsProfile.nickName
                        clan = thpsProfile.clan or None
                        if clan is not None:
                            thpsItem.update({'nick': nick, 'clan': clan})
                        else:
                            thpsItem.update({'nick': nick})
                        thpsAvatar = thpsProfile.avatarThumbMiddle or None
                        if thpsAvatar is not None:
                            thpsItem.update({'avatar': thpsProfile})
                        item.update({'thps': thpsItem})
                if friendShipStatus is True:
                    if removeText is None:
                        removeText = textDict['removeFromFriends']
                    itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("removeFriend", kwargs = {'userId': id}), removeText)
                    if viewFriends:
                        if viewFriendsText is None:
                            viewFriendsText = textDict['viewFriends']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                    if sendPersonalMessages:
                        if sendMessageText is None:
                            sendMessageText = textDict['personalmessage']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                    itemString1 += '</ul></div></li>'
                elif friendShipStatus == "request":
                    if confirmText is None:
                        confirmText = textDict['confirm']
                    if rejectText is None:
                        rejectText = textDict['reject']
                    itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li><li><form method = "post" action = "{2}"><input type = "submit" value = "{3}"></form></li>'.format(reverse("addFriend", kwargs = {'userId': id}), confirmText, reverse("rejectFriend", kwargs = {'userId': id}), rejectText)
                    if viewFriends:
                        if viewFriendsText is None:
                            viewFriendsText = textDict['viewFriends']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                    if sendPersonalMessages:
                        if sendMessageText is None:
                            sendMessageText = textDict['personalmessage']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                    itemString1 += '</ul></div></li>'
                elif friendShipStatus == "myRequest":
                    if cancelText is None:
                        cancelText = textDict['cancel']
                    itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("cancelAddFriendRequest", kwargs = {'userId': id}), cancelText)
                    if viewFriends:
                        if viewFriendsText is None:
                            viewFriendsText = textDict['viewFriends']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                    if sendPersonalMessages:
                        if sendMessageText is None:
                            sendMessageText = textDict['personalmessage']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                    itemString1 += '</ul></div></li>'
                else:
                    if sendFriendRequests:
                        if addRequestText is None:
                            addRequestText = textDict['addToFriends']
                        itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("addFriendRequest", kwargs = {'userId': id}), addRequestText)
                        if viewFriends:
                            if viewFriendsText is None:
                                viewFriendsText = textDict['viewFriends']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                        if sendPersonalMessages:
                            if sendMessageText is None:
                                sendMessageText = textDict['personalmessage']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                        itemString1 += '</ul></div></li>'
                    else:
                        itemString1 = '</li>'
                if viewOnlineStatus:
                    if id in onlineUsers:
                        onlineUsersSet.add(id)
                        if viewActiveStatus:
                            if id in activeUsers:
                                activeUsersSet.add(id)
                            else:
                                middleTupleStr = middleMany.pop(id, None)
                                if middleTupleStr is not None:
                                    middleUsersDict.update({id: middleTupleStr})
                        else:
                            hiddenActiveUsersSet.add(id)
                    else:
                        iffyTimeStr = iffyMany.pop(id, None)
                        if iffyTimeStr is not None:
                            iffyUsersDict.update({id: iffyTimeStr})
                            if viewActiveStatus:
                                if id in activeUsers:
                                    activeUsersSet.add(id)
                                else:
                                    middleTupleStr = middleMany.pop(id, None)
                                    if middleTupleStr is not None:
                                        middleUsersDict.update({id: middleTupleStr})
                            else:
                                hiddenActiveUsersSet.add(id)
                    result.append((itemString0, itemString1))
                thumbItemsToSet.update({id: {requestUserId: {'thumb': itemString0, 'usersList': itemString1, 'showInOnlineList': viewOnlineStatus, 'showInActiveList': viewActiveStatus}}})
            else:
                thumbItemsToSet.update({id: {requestUserId: False}})
        else:
            item = itemsDict.get(requestUserId, None)
            if item is None:
                if id in friendsIds:
                    friendShipStatus = True
                else:
                    friendShipDict = friendShipDictMany.get(id, None)
                    if friendShipDict is None:
                        friendShipStatus = False
                        friendShipRequestList = friendShipRequestDictList.get(id, None)
                        if friendShipRequestList is not None:
                            for request in friendShipRequestList:
                                if request[0] == requestUserId:
                                    friendShipStatus = "myRequest"
                                    break
                        if friendShipStatus is False:
                            friendShipRequestList = friendShipRequestDictList.get(requestUserId, None)
                            if friendShipRequestList is not None:
                                for request in friendShipRequestList:
                                    if request[0] == id:
                                        friendShipStatus = "request"
                                        break
                        friendShipStatusToSet.update({id: {requestUserId: friendShipStatus}})
                    else:
                        friendShipStatus = friendShipDict.get(requestUserId, None)
                        if friendShipStatus is None:
                            friendShipStatus = False
                            friendShipRequestList = friendShipRequestDictList.get(id, None)
                            if friendShipRequestList is not None:
                                for request in friendShipRequestList:
                                    if request[0] == requestUserId:
                                        friendShipStatus = "myRequest"
                                        break
                            if friendShipStatus is False:
                                friendShipRequestList = friendShipRequestDictList.get(requestUserId, None)
                                if friendShipRequestList is not None:
                                    for request in friendShipRequestList:
                                        if request[0] == id:
                                            friendShipStatus = "request"
                                            break
                            friendShipDict.update({requestUserId: friendShipStatus})
                            friendShipStatusToSet.update({id: friendShipDict})
                privacyDict = privacyDictMany.get(id, None)
                if privacyDict is None:
                    privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    sendFriendRequests = privacyChecker.sendFriendRequests()
                    sendPersonalMessages = privacyChecker.sendPersonalMessages()
                    viewFriends = privacyChecker.checkFriendsListPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                    privacyToSet.update({id: {requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
                else:
                    userPrivacyDict = privacyDict.get(requestUserId, None)
                    if userPrivacyDict is None:
                        privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                        viewProfile = privacyChecker.checkViewProfilePrivacy()
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        sendFriendRequests = privacyChecker.sendFriendRequests()
                        sendPersonalMessages = privacyChecker.sendPersonalMessages()
                        viewFriends = privacyChecker.checkFriendsListPrivacy()
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                        privacyDict.update({requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
                        privacyToSet.update({id: privacyDict})
                    else:
                        viewProfile = userPrivacyDict.get('viewProfile', None)
                        viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                        viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                        viewAvatar = userPrivacyDict.get('viewAvatar', None)
                        sendFriendRequests = userPrivacyDict.get('sendFriendRequests', None)
                        sendPersonalMessages = userPrivacyDict.get('sendPersonalMessages', None)
                        viewFriends = userPrivacyDict.get('viewFriends', None)
                        viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                        viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                        if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or sendFriendRequests is None or sendPersonalMessages is None or viewFriends is None or viewOnlineStatus is None or viewActiveStatus is None:
                            privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                            if viewProfile is None:
                                viewProfile = privacyChecker.checkViewProfilePrivacy()
                                userPrivacyDict.update({'viewProfile': viewProfile})
                            if viewMainInfo is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                            if viewTHPSInfo is None:
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                            if viewAvatar is None:
                                viewAvatar = privacyChecker.checkAvatarPrivacy()
                                userPrivacyDict.update({'viewAvatar': viewAvatar})
                            if viewOnlineStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                            if viewActiveStatus is None:
                                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                            if sendFriendRequests is None:
                                sendFriendRequests = privacyChecker.sendFriendRequests()
                                userPrivacyDict.update({'sendFriendRequests': sendFriendRequests})
                            if sendPersonalMessages is None:
                                sendPersonalMessages = privacyChecker.sendPersonalMessages()
                                userPrivacyDict.update({'sendPersonalMessages': sendPersonalMessages})
                            if viewFriends is None:
                                viewFriends = privacyChecker.checkFriendsListPrivacy()
                                userPrivacyDict.update({'viewFriends': viewFriends})
                            privacyToSet.update({id: privacyDict})
                if viewProfile:
                    user = userItems.get(id__exact = id)
                    profileSlug = user.profileUrl
                    profilePath = reverse("profile", kwargs = {'currentUser': profileSlug})
                    if viewMainInfo:
                        firstName = user.first_name or None
                        lastName = user.last_name or None
                        if firstName and lastName:
                            name = '{0} {1}'.format(firstName, lastName)
                            smallName = user.username
                        else:
                            if firstName:
                                name = firstName
                                smallName = user.username
                            else:
                                name = user.username
                                smallName = None
                    else:
                        name = user.username
                        smallName = None
                    if viewAvatar:
                        avatar = user.avatarThumbMiddle or None
                        if avatar is not None:
                            if smallName:
                                itemString0 = '<li value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(id, profilePath, avatar.url, name, smallName)
                            else:
                                itemString0 = '<li value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(id, profilePath, avatar.url, name)
                        else:
                            if smallName:
                                itemString0 = '<li value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(id, name, smallName, profilePath)
                            else:
                                itemString0 = '<li value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(id, name, profilePath)
                    else:
                        if smallName:
                            itemString0 = '<li value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(id, name, smallName, profilePath)
                        else:
                            itemString0 = '<li value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(id, name, profilePath)
                    if viewTHPSInfo:
                        try:
                            thpsProfile = thpsProfiles.get(toUser__exact = user)
                        except:
                            thpsProfile = None
                        if thpsProfile is not None:
                            thpsItem = {}
                            nick = thpsProfile.nickName
                            clan = thpsProfile.clan or None
                            if clan is not None:
                                thpsItem.update({'nick': nick, 'clan': clan})
                            else:
                                thpsItem.update({'nick': nick})
                            thpsAvatar = thpsProfile.avatarThumbMiddle or None
                            if thpsAvatar is not None:
                                thpsItem.update({'avatar': thpsProfile})
                            item.update({'thps': thpsItem})
                    if friendShipStatus is True:
                        if removeText is None:
                            removeText = textDict['removeFromFriends']
                        itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("removeFriend", kwargs = {'userId': id}), removeText)
                        if viewFriends:
                            if viewFriendsText is None:
                                viewFriendsText = textDict['viewFriends']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                        if sendPersonalMessages:
                            if sendMessageText is None:
                                sendMessageText = textDict['personalmessage']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                        itemString1 += '</ul></div></li>'
                    elif friendShipStatus == "request":
                        if confirmText is None:
                            confirmText = textDict['confirm']
                        if rejectText is None:
                            rejectText = textDict['reject']
                        itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li><li><form method = "post" action = "{2}"><input type = "submit" value = "{3}"></form></li>'.format(reverse("addFriend", kwargs = {'userId': id}), confirmText, reverse("rejectFriend", kwargs = {'userId': id}), rejectText)
                        if viewFriends:
                            if viewFriendsText is None:
                                viewFriendsText = textDict['viewFriends']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                        if sendPersonalMessages:
                            if sendMessageText is None:
                                sendMessageText = textDict['personalmessage']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                        itemString1 += '</ul></div></li>'
                    elif friendShipStatus == "myRequest":
                        if cancelText is None:
                            cancelText = textDict['cancel']
                        itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("cancelAddFriendRequest", kwargs = {'userId': id}), cancelText)
                        if viewFriends:
                            if viewFriendsText is None:
                                viewFriendsText = textDict['viewFriends']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                        if sendPersonalMessages:
                            if sendMessageText is None:
                                sendMessageText = textDict['personalmessage']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                        itemString1 += '</ul></div></li>'
                    else:
                        if sendFriendRequests:
                            if addRequestText is None:
                                addRequestText = textDict['addToFriends']
                            itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("addFriendRequest", kwargs = {'userId': id}), addRequestText)
                            if viewFriends:
                                if viewFriendsText is None:
                                    viewFriendsText = textDict['viewFriends']
                                itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                            if sendPersonalMessages:
                                if sendMessageText is None:
                                    sendMessageText = textDict['personalmessage']
                                itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                            itemString1 += '</ul></div></li>'
                        else:
                            itemString1 = '</li>'
                    if viewOnlineStatus:
                        if id in onlineUsers:
                            onlineUsersSet.add(id)
                            if viewActiveStatus:
                                if id in activeUsers:
                                    activeUsersSet.add(id)
                                else:
                                    middleTupleStr = middleMany.pop(id, None)
                                    if middleTupleStr is not None:
                                        middleUsersDict.update({id: middleTupleStr})
                            else:
                                hiddenActiveUsersSet.add(id)
                        else:
                            iffyTimeStr = iffyMany.pop(id, None)
                            if iffyTimeStr is not None:
                                iffyUsersDict.update({id: iffyTimeStr})
                                if viewActiveStatus:
                                    if id in activeUsers:
                                        activeUsersSet.add(id)
                                    else:
                                        middleTupleStr = middleMany.pop(id, None)
                                        if middleTupleStr is not None:
                                            middleUsersDict.update({id: middleTupleStr})
                                else:
                                    hiddenActiveUsersSet.add(id)
                        result.append((itemString0, itemString1))
                    itemsDict.update({requestUserId: {'thumb': itemString0, 'usersList': itemString1, 'showInOnlineList': viewOnlineStatus, 'showInActiveList': viewActiveStatus}})
                    thumbItemsToSet.update({id: itemsDict})
                else:
                    itemsDict.update({requestUserId: False})
                    thumbItemsToSet.update({id: itemsDict})
            else:
                if item is not False:
                    options = item.pop('usersList', None)
                    showOnlineStatus = item['showInOnlineList']
                    showActiveStatus = item['showInActiveList']
                    if options is None:
                        if id in friendsIds:
                            friendShipStatus = True
                        else:
                            friendShipDict = friendShipDictMany.get(id, None)
                            if friendShipDict is None:
                                friendShipStatus = False
                                friendShipRequestList = friendShipRequestDictList.get(id, None)
                                if friendShipRequestList is not None:
                                    for request in friendShipRequestList:
                                        if request[0] == requestUserId:
                                            friendShipStatus = "myRequest"
                                            break
                                if friendShipStatus is False:
                                    friendShipRequestList = friendShipRequestDictList.get(requestUserId, None)
                                    if friendShipRequestList is not None:
                                        for request in friendShipRequestList:
                                            if request[0] == id:
                                                friendShipStatus = "request"
                                                break
                                friendShipStatusToSet.update({id: {requestUserId: friendShipStatus}})
                            else:
                                friendShipStatus = friendShipDict.get(requestUserId, None)
                                if friendShipStatus is None:
                                    friendShipStatus = False
                                    friendShipRequestList = friendShipRequestDictList.get(id, None)
                                    if friendShipRequestList is not None:
                                        for request in friendShipRequestList:
                                            if request[0] == requestUserId:
                                                friendShipStatus = "myRequest"
                                                break
                                    if friendShipStatus is False:
                                        friendShipRequestList = friendShipRequestDictList.get(requestUserId, None)
                                        if friendShipRequestList is not None:
                                            for request in friendShipRequestList:
                                                if request[0] == id:
                                                    friendShipStatus = "request"
                                                    break
                                    friendShipDict.update({requestUserId: friendShipStatus})
                                    friendShipStatusToSet.update({id: friendShipDict})
                        privacyDict = privacyDictMany.get(id, None)
                        if privacyDict is None:
                            privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                            sendFriendRequests = privacyChecker.sendFriendRequests()
                            sendPersonalMessages = privacyChecker.sendPersonalMessages()
                            viewFriends = privacyChecker.checkFriendsListPrivacy()
                            privacyToSet.update({id: {requestUserId: {'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends}}})
                        else:
                            userPrivacyDict = privacyDict.get(requestUserId, None)
                            if userPrivacyDict is None:
                                privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                                sendFriendRequests = privacyChecker.sendFriendRequests()
                                sendPersonalMessages = privacyChecker.sendPersonalMessages()
                                viewFriends = privacyChecker.checkFriendsListPrivacy()
                                privacyDict.update({requestUserId: {'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends}})
                                privacyToSet.update({id: privacyDict})
                            else:
                                sendFriendRequests = userPrivacyDict.get('sendFriendRequests', None)
                                sendPersonalMessages = userPrivacyDict.get('sendPersonalMessages', None)
                                viewFriends = userPrivacyDict.get('viewFriends', None)
                                if sendFriendRequests is None or sendPersonalMessages is None or viewFriends is None:
                                    privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                                    if sendFriendRequests is None:
                                        sendFriendRequests = privacyChecker.sendFriendRequests()
                                        userPrivacyDict.update({'sendFriendRequests': sendFriendRequests})
                                    if sendPersonalMessages is None:
                                        sendPersonalMessages = privacyChecker.sendPersonalMessages()
                                        userPrivacyDict.update({'sendPersonalMessages': sendPersonalMessages})
                                    if viewFriends is None:
                                        viewFriends = privacyChecker.checkFriendsListPrivacy()
                                        userPrivacyDict.update({'viewFriends': viewFriends})
                                    privacyToSet.update({id: privacyDict})
                        profileSlug = user.profileUrl
                        if friendShipStatus is True:
                            if removeText is None:
                                removeText = textDict['removeFromFriends']
                            options = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("removeFriend", kwargs = {'userId': id}), removeText)
                            if viewFriends:
                                if viewFriendsText is None:
                                    viewFriendsText = textDict['viewFriends']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                            if sendPersonalMessages:
                                if sendMessageText is None:
                                    sendMessageText = textDict['personalmessage']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                            options += '</ul></div></li>'
                        elif friendShipStatus == "request":
                            if confirmText is None:
                                confirmText = textDict['confirm']
                            if rejectText is None:
                                rejectText = textDict['reject']
                            options = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li><li><form method = "post" action = "{2}"><input type = "submit" value = "{3}"></form></li>'.format(reverse("addFriend", kwargs = {'userId': id}), confirmText, reverse("rejectFriend", kwargs = {'userId': id}), rejectText)
                            if viewFriends:
                                if viewFriendsText is None:
                                    viewFriendsText = textDict['viewFriends']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                            if sendPersonalMessages:
                                if sendMessageText is None:
                                    sendMessageText = textDict['personalmessage']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                            options += '</ul></div></li>'
                        elif friendShipStatus == "myRequest":
                            if cancelText is None:
                                cancelText = textDict['cancel']
                            options = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("cancelAddFriendRequest", kwargs = {'userId': id}), cancelText)
                            if viewFriends:
                                if viewFriendsText is None:
                                    viewFriendsText = textDict['viewFriends']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                            if sendPersonalMessages:
                                if sendMessageText is None:
                                    sendMessageText = textDict['personalmessage']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                            options += '</ul></div></li>'
                        else:
                            if sendFriendRequests:
                                if addRequestText is None:
                                    addRequestText = textDict['addToFriends']
                                options = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("addFriendRequest", kwargs = {'userId': id}), addRequestText)
                                if viewFriends:
                                    if viewFriendsText is None:
                                        viewFriendsText = textDict['viewFriends']
                                    options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                                if sendPersonalMessages:
                                    if sendMessageText is None:
                                        sendMessageText = textDict['personalmessage']
                                    options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                                options += '</ul></div></li>'
                            else:
                                options = '</li>'
                        item.update({'usersList': options})
                        thumbItemsToSet.update({id: itemsDict})
                    if showOnlineStatus:
                        if id in onlineUsers:
                            onlineUsersSet.add(id)
                            if showActiveStatus:
                                if id in activeUsers:
                                    activeUsersSet.add(id)
                                else:
                                    middleTupleStr = middleMany.pop(id, None)
                                    if middleTupleStr is not None:
                                        middleUsersDict.update({id: middleTupleStr})
                            else:
                                hiddenActiveUsersSet.add(id)
                        else:
                            iffyTimeStr = iffyMany.pop(id, None)
                            if iffyTimeStr is not None:
                                iffyUsersDict.update({id: iffyTimeStr})
                                if showActiveStatus:
                                    if id in activeUsers:
                                        activeUsersSet.add(id)
                                    else:
                                        middleTupleStr = middleMany.pop(id, None)
                                        if middleTupleStr is not None:
                                            middleUsersDict.update({id: middleTupleStr})
                                else:
                                    hiddenActiveUsersSet.add(id)
                        result.append((item['thumb'], options))

    if isSetThumbItems is True:
        if isPrivacyToSet is True:
            privacyCache.set_many(privacyToSet)
        if isFriendShipToGet is True and isFriendShipRequestsToGet is True:
            friendShipCache.set_many(friendShipStatusToSet)
        thumbsCache.set_many(thumbItemsToSet)

    if len(onlineUsersSet) > 0:
        statusDict.update({'stateOnline': list(onlineUsersSet)})
    if len(iffyUsersDict.keys()) > 0:
        statusDict.update({'stateTimer': iffyUsersDict})
    if len(activeUsersSet) > 0:
        statusDict.update({'stateActive': list(activeUsersSet)})
    if len(middleUsersDict) > 0:
        statusDict.update({'stateMiddle': middleUsersDict})

    if len(hiddenActiveUsersSet) > 0:
        statusDict.update({'hiddenActive': list(hiddenActiveUsersSet)})

    return statusDict, result

def createUsersListGuest(iterableIdsPageInstance):
    result = []
    statusDict = {}
    onlineUsersSet = set()
    iffyUsersDict = {}
    activeUsersSet = set()
    hiddenActiveUsersSet = set()
    hiddenOnlineUsersSet = set()
    middleUsersDict = {}
    iterableIds = {str(user.get("id")) for user in iterableIdsPageInstance}
    thumbsCache = caches['usersListThumbs']
    itemsDictMany = thumbsCache.get_many(iterableIds)
    itemsDictKeys = itemsDictMany.keys()
    idsToGetThumbItem = iterableIds - itemsDictKeys
    for id in itemsDictKeys:
        itemDict = itemsDictMany[id].get(0, None)
        if itemDict is None:
            idsToGetThumbItem.add(id)
        elif itemDict is False:
            iterableIds.remove(id)
        else:
            if 'usersList' not in itemDict.keys():
                idsToGetThumbItem.add(id)

    if len(idsToGetThumbItem) > 0:
        thumbItemsToSet = {}
        privacyToSet = {}
        addRequestText = None
        confirmText = None
        cancelText = None
        rejectText = None
        removeText = None
        viewFriendsText = None
        sendMessageText = None
        userItems = mainUserProfile.objects.filter(id__in = idsToGetThumbItem)
        thpsProfiles = THPSProfile.objects.filter(toUser_id__in = idsToGetThumbItem)
        privacyCache = caches['privacyCache']
        privacyDictMany = privacyCache.get_many(idsToGetThumbItem)
        privacyDictManyKeys = privacyDictMany.keys()
        privacyToGet = idsToGetThumbItem - privacyDictManyKeys
        for id in privacyDictManyKeys:
            privacyDict = privacyDictMany[id].get(0, None)
            if privacyDict is None:
                privacyToGet.add(id)
            else:
                privacyDictKeys = privacyDict.keys()
                if 'viewProfile' not in privacyDictKeys or 'viewMainInfo' not in privacyDictKeys or 'viewTHPSInfo' not in privacyDictKeys or 'viewAvatar' not in privacyDictKeys or 'sendFriendRequests' not in privacyDictKeys or 'sendPersonalMessages' not in privacyDictKeys or 'viewFriends' not in privacyDictKeys or 'viewOnlineStatus' not in privacyDictKeys or 'viewActiveStatus' not in privacyDictKeys:
                    privacyToGet.add(id)
        isPrivacyToSet = len(privacyToGet) > 0
        if isPrivacyToSet is True:
            privacyInstances = userProfilePrivacy.objects.filter(user_id__in = privacyToGet)
            privacyToSet = {}
        textDict = usersListText(lang = 'ru').getDict()
    onlineUsers = caches['onlineUsers'].get_many(iterableIds).keys()
    iffyMany = caches['iffyUsersOnlineStatus'].get_many((iterableIds - onlineUsers))
    activeUsers = caches['chatStatusActive'].get_many((iffyMany.keys() | onlineUsers)).keys()
    middleMany = caches['chatStatusMiddle'].get_many((iterableIds - activeUsers))

    for id in iterableIds:
        itemsDict = itemsDictMany.get(id, None)
        if itemsDict is None:
            privacyDict = privacyDictMany.get(id, None)
            if privacyDict is None:
                privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isAuthenticated = False)
                viewProfile = privacyChecker.checkViewProfilePrivacy()
                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                viewAvatar = privacyChecker.checkAvatarPrivacy()
                sendFriendRequests = privacyChecker.sendFriendRequests()
                sendPersonalMessages = privacyChecker.sendPersonalMessages()
                viewFriends = privacyChecker.checkFriendsListPrivacy()
                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                privacyToSet.update({id: {0: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
            else:
                userPrivacyDict = privacyDict.get(0, None)
                if userPrivacyDict is None:
                    privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isAuthenticated = False)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    sendFriendRequests = privacyChecker.sendFriendRequests()
                    sendPersonalMessages = privacyChecker.sendPersonalMessages()
                    viewFriends = privacyChecker.checkFriendsListPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                    privacyDict.update({0: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
                    privacyToSet.update({id: privacyDict})
                else:
                    viewProfile = userPrivacyDict.get('viewProfile', None)
                    viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                    viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                    viewAvatar = userPrivacyDict.get('viewAvatar', None)
                    sendFriendRequests = userPrivacyDict.get('sendFriendRequests', None)
                    sendPersonalMessages = userPrivacyDict.get('sendPersonalMessages', None)
                    viewFriends = userPrivacyDict.get('viewFriends', None)
                    viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                    viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                    if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or sendFriendRequests is None or sendPersonalMessages is None or viewFriends is None or viewOnlineStatus is None or viewActiveStatus is None:
                        privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isAuthenticated = False)
                        if viewProfile is None:
                            viewProfile = privacyChecker.checkViewProfilePrivacy()
                            userPrivacyDict.update({'viewProfile': viewProfile})
                        if viewMainInfo is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                        if viewTHPSInfo is None:
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                        if viewAvatar is None:
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            userPrivacyDict.update({'viewAvatar': viewAvatar})
                        if viewOnlineStatus is None:
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                        if viewActiveStatus is None:
                            viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                            userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                        if sendFriendRequests is None:
                            sendFriendRequests = privacyChecker.sendFriendRequests()
                            userPrivacyDict.update({'sendFriendRequests': sendFriendRequests})
                        if sendPersonalMessages is None:
                            sendPersonalMessages = privacyChecker.sendPersonalMessages()
                            userPrivacyDict.update({'sendPersonalMessages': sendPersonalMessages})
                        if viewFriends is None:
                            viewFriends = privacyChecker.checkFriendsListPrivacy()
                            userPrivacyDict.update({'viewFriends': viewFriends})
                        privacyToSet.update({id: privacyDict})
            if viewProfile:
                user = userItems.get(id__exact = id)
                profileSlug = user.profileUrl
                profilePath = reverse("profile", kwargs = {'currentUser': profileSlug})
                if viewMainInfo:
                    firstName = user.first_name or None
                    lastName = user.last_name or None
                    if firstName and lastName:
                        name = '{0} {1}'.format(firstName, lastName)
                        smallName = user.username
                    else:
                        if firstName:
                            name = firstName
                            smallName = user.username
                        else:
                            name = user.username
                            smallName = None
                else:
                    name = user.username
                    smallName = None
                if viewAvatar:
                    avatar = user.avatarThumbMiddle or None
                    if avatar is not None:
                        if smallName:
                            itemString0 = '<li value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(id, profilePath, avatar.url, name, smallName)
                        else:
                            itemString0 = '<li value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(id, profilePath, avatar.url, name)
                    else:
                        if smallName:
                            itemString0 = '<li value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(id, name, smallName, profilePath)
                        else:
                            itemString0 = '<li value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(id, name, profilePath)
                else:
                    if smallName:
                        itemString0 = '<li value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(id, name, smallName, profilePath)
                    else:
                        itemString0 = '<li value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(id, name, profilePath)
                if viewTHPSInfo:
                    try:
                        thpsProfile = thpsProfiles.get(toUser__exact = user)
                    except:
                        thpsProfile = None
                    if thpsProfile is not None:
                        thpsItem = {}
                        nick = thpsProfile.nickName
                        clan = thpsProfile.clan or None
                        if clan is not None:
                            thpsItem.update({'nick': nick, 'clan': clan})
                        else:
                            thpsItem.update({'nick': nick})
                        thpsAvatar = thpsProfile.avatarThumbMiddle or None
                        if thpsAvatar is not None:
                            thpsItem.update({'avatar': thpsProfile})
                        item.update({'thps': thpsItem})
                if friendShipStatus is True:
                    if removeText is None:
                        removeText = textDict['removeFromFriends']
                    itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("removeFriend", kwargs = {'userId': id}), removeText)
                    if viewFriends:
                        if viewFriendsText is None:
                            viewFriendsText = textDict['viewFriends']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                    if sendPersonalMessages:
                        if sendMessageText is None:
                            sendMessageText = textDict['personalmessage']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                    itemString1 += '</ul></div></li>'
                elif friendShipStatus == "request":
                    if confirmText is None:
                        confirmText = textDict['confirm']
                    if rejectText is None:
                        rejectText = textDict['reject']
                    itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li><li><form method = "post" action = "{2}"><input type = "submit" value = "{3}"></form></li>'.format(reverse("addFriend", kwargs = {'userId': id}), confirmText, reverse("rejectFriend", kwargs = {'userId': id}), rejectText)
                    if viewFriends:
                        if viewFriendsText is None:
                            viewFriendsText = textDict['viewFriends']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                    if sendPersonalMessages:
                        if sendMessageText is None:
                            sendMessageText = textDict['personalmessage']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                    itemString1 += '</ul></div></li>'
                elif friendShipStatus == "myRequest":
                    if cancelText is None:
                        cancelText = textDict['cancel']
                    itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("cancelAddFriendRequest", kwargs = {'userId': id}), cancelText)
                    if viewFriends:
                        if viewFriendsText is None:
                            viewFriendsText = textDict['viewFriends']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                    if sendPersonalMessages:
                        if sendMessageText is None:
                            sendMessageText = textDict['personalmessage']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                    itemString1 += '</ul></div></li>'
                else:
                    if sendFriendRequests:
                        if addRequestText is None:
                            addRequestText = textDict['addToFriends']
                        itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("addFriendRequest", kwargs = {'userId': id}), addRequestText)
                        if viewFriends:
                            if viewFriendsText is None:
                                viewFriendsText = textDict['viewFriends']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                        if sendPersonalMessages:
                            if sendMessageText is None:
                                sendMessageText = textDict['personalmessage']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                        itemString1 += '</ul></div></li>'
                    else:
                        itemString1 = '</li>'
                if viewOnlineStatus and viewActiveStatus:
                    if id in onlineUsers:
                        onlineUsersSet.add(id)
                        if id in activeUsers:
                            activeUsersSet.add(id)
                        else:
                            middleTupleStr = middleMany.pop(id, None)
                            if middleTupleStr is not None:
                                middleUsersDict.update({id: middleTupleStr})
                    else:
                        iffyTimeStr = iffyMany.pop(id, None)
                        if iffyTimeStr is not None:
                            iffyUsersDict.update({id: iffyTimeStr})
                            if id in activeUsers:
                                activeUsersSet.add(id)
                            else:
                                middleTupleStr = middleMany.pop(id, None)
                                if middleTupleStr is not None:
                                    middleUsersDict.update({id: middleTupleStr})
                else:
                    if viewOnlineStatus:
                        if id in onlineUsers:
                            onlineUsersSet.add(id)
                        else:
                            iffyTimeStr = iffyMany.pop(id, None)
                            if iffyTimeStr is not None:
                                iffyUsersDict.update({id: iffyTimeStr})
                        hiddenActiveUsersSet.add(id)
                    elif viewActiveStatus:
                        if id in activeUsers:
                            activeUsersSet.add(id)
                        else:
                            middleTupleStr = middleMany.pop(id, None)
                            if middleTupleStr is not None:
                                middleUsersDict.update({id: middleTupleStr})
                        hiddenOnlineUsersSet.add(id)
                    else:
                        hiddenActiveUsersSet.add(id)
                        hiddenOnlineUsersSet.add(id)
                result.append((itemString0, itemString1))
                thumbItemsToSet.update({id: {0: {'thumb': itemString0, 'usersList': itemString1, 'showInOnlineList': viewOnlineStatus, 'showInActiveList': viewActiveStatus}}})
            else:
                thumbItemsToSet.update({id: {0: False}})
        else:
            item = itemsDict.get(0, None)
            if item is None:
                privacyDict = privacyDictMany.get(id, None)
                if privacyDict is None:
                    privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isAuthenticated = False)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    sendFriendRequests = privacyChecker.sendFriendRequests()
                    sendPersonalMessages = privacyChecker.sendPersonalMessages()
                    viewFriends = privacyChecker.checkFriendsListPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                    privacyToSet.update({id: {0: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
                else:
                    userPrivacyDict = privacyDict.get(0, None)
                    if userPrivacyDict is None:
                        privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isAuthenticated = False)
                        viewProfile = privacyChecker.checkViewProfilePrivacy()
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        sendFriendRequests = privacyChecker.sendFriendRequests()
                        sendPersonalMessages = privacyChecker.sendPersonalMessages()
                        viewFriends = privacyChecker.checkFriendsListPrivacy()
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                        privacyDict.update({0: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
                        privacyToSet.update({id: privacyDict})
                    else:
                        viewProfile = userPrivacyDict.get('viewProfile', None)
                        viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                        viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                        viewAvatar = userPrivacyDict.get('viewAvatar', None)
                        sendFriendRequests = userPrivacyDict.get('sendFriendRequests', None)
                        sendPersonalMessages = userPrivacyDict.get('sendPersonalMessages', None)
                        viewFriends = userPrivacyDict.get('viewFriends', None)
                        viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                        viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                        if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or sendFriendRequests is None or sendPersonalMessages is None or viewFriends is None or viewOnlineStatus is None or viewActiveStatus is None:
                            privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isAuthenticated = False)
                            if viewProfile is None:
                                viewProfile = privacyChecker.checkViewProfilePrivacy()
                                userPrivacyDict.update({'viewProfile': viewProfile})
                            if viewMainInfo is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                            if viewTHPSInfo is None:
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                            if viewAvatar is None:
                                viewAvatar = privacyChecker.checkAvatarPrivacy()
                                userPrivacyDict.update({'viewAvatar': viewAvatar})
                            if viewOnlineStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                            if viewActiveStatus is None:
                                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                            if sendFriendRequests is None:
                                sendFriendRequests = privacyChecker.sendFriendRequests()
                                userPrivacyDict.update({'sendFriendRequests': sendFriendRequests})
                            if sendPersonalMessages is None:
                                sendPersonalMessages = privacyChecker.sendPersonalMessages()
                                userPrivacyDict.update({'sendPersonalMessages': sendPersonalMessages})
                            if viewFriends is None:
                                viewFriends = privacyChecker.checkFriendsListPrivacy()
                                userPrivacyDict.update({'viewFriends': viewFriends})
                            privacyToSet.update({id: privacyDict})
                if viewProfile:
                    user = userItems.get(id__exact = id)
                    profileSlug = user.profileUrl
                    profilePath = reverse("profile", kwargs = {'currentUser': profileSlug})
                    if viewMainInfo:
                        firstName = user.first_name or None
                        lastName = user.last_name or None
                        if firstName and lastName:
                            name = '{0} {1}'.format(firstName, lastName)
                            smallName = user.username
                        else:
                            if firstName:
                                name = firstName
                                smallName = user.username
                            else:
                                name = user.username
                                smallName = None
                    else:
                        name = user.username
                        smallName = None
                    if viewAvatar:
                        avatar = user.avatarThumbMiddle or None
                        if avatar is not None:
                            if smallName:
                                itemString0 = '<li value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(id, profilePath, avatar.url, name, smallName)
                            else:
                                itemString0 = '<li value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(id, profilePath, avatar.url, name)
                        else:
                            if smallName:
                                itemString0 = '<li value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(id, name, smallName, profilePath)
                            else:
                                itemString0 = '<li value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(id, name, profilePath)
                    else:
                        if smallName:
                            itemString0 = '<li value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(id, name, smallName, profilePath)
                        else:
                            itemString0 = '<li value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(id, name, profilePath)
                    if viewTHPSInfo:
                        try:
                            thpsProfile = thpsProfiles.get(toUser__exact = user)
                        except:
                            thpsProfile = None
                        if thpsProfile is not None:
                            thpsItem = {}
                            nick = thpsProfile.nickName
                            clan = thpsProfile.clan or None
                            if clan is not None:
                                thpsItem.update({'nick': nick, 'clan': clan})
                            else:
                                thpsItem.update({'nick': nick})
                            thpsAvatar = thpsProfile.avatarThumbMiddle or None
                            if thpsAvatar is not None:
                                thpsItem.update({'avatar': thpsProfile})
                            item.update({'thps': thpsItem})
                    if friendShipStatus is True:
                        if removeText is None:
                            removeText = textDict['removeFromFriends']
                        itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("removeFriend", kwargs = {'userId': id}), removeText)
                        if viewFriends:
                            if viewFriendsText is None:
                                viewFriendsText = textDict['viewFriends']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                        if sendPersonalMessages:
                            if sendMessageText is None:
                                sendMessageText = textDict['personalmessage']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                        itemString1 += '</ul></div></li>'
                    elif friendShipStatus == "request":
                        if confirmText is None:
                            confirmText = textDict['confirm']
                        if rejectText is None:
                            rejectText = textDict['reject']
                        itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li><li><form method = "post" action = "{2}"><input type = "submit" value = "{3}"></form></li>'.format(reverse("addFriend", kwargs = {'userId': id}), confirmText, reverse("rejectFriend", kwargs = {'userId': id}), rejectText)
                        if viewFriends:
                            if viewFriendsText is None:
                                viewFriendsText = textDict['viewFriends']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                        if sendPersonalMessages:
                            if sendMessageText is None:
                                sendMessageText = textDict['personalmessage']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                        itemString1 += '</ul></div></li>'
                    elif friendShipStatus == "myRequest":
                        if cancelText is None:
                            cancelText = textDict['cancel']
                        itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("cancelAddFriendRequest", kwargs = {'userId': id}), cancelText)
                        if viewFriends:
                            if viewFriendsText is None:
                                viewFriendsText = textDict['viewFriends']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                        if sendPersonalMessages:
                            if sendMessageText is None:
                                sendMessageText = textDict['personalmessage']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                        itemString1 += '</ul></div></li>'
                    else:
                        if sendFriendRequests:
                            if addRequestText is None:
                                addRequestText = textDict['addToFriends']
                            itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("addFriendRequest", kwargs = {'userId': id}), addRequestText)
                            if viewFriends:
                                if viewFriendsText is None:
                                    viewFriendsText = textDict['viewFriends']
                                itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                            if sendPersonalMessages:
                                if sendMessageText is None:
                                    sendMessageText = textDict['personalmessage']
                                itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                            itemString1 += '</ul></div></li>'
                        else:
                            itemString1 = '</li>'
                    if viewOnlineStatus and viewActiveStatus:
                        if id in onlineUsers:
                            onlineUsersSet.add(id)
                            if id in activeUsers:
                                activeUsersSet.add(id)
                            else:
                                middleTupleStr = middleMany.pop(id, None)
                                if middleTupleStr is not None:
                                    middleUsersDict.update({id: middleTupleStr})
                        else:
                            iffyTimeStr = iffyMany.pop(id, None)
                            if iffyTimeStr is not None:
                                iffyUsersDict.update({id: iffyTimeStr})
                                if id in activeUsers:
                                    activeUsersSet.add(id)
                                else:
                                    middleTupleStr = middleMany.pop(id, None)
                                    if middleTupleStr is not None:
                                        middleUsersDict.update({id: middleTupleStr})
                    else:
                        if viewOnlineStatus:
                            if id in onlineUsers:
                                onlineUsersSet.add(id)
                            else:
                                iffyTimeStr = iffyMany.pop(id, None)
                                if iffyTimeStr is not None:
                                    iffyUsersDict.update({id: iffyTimeStr})
                            hiddenActiveUsersSet.add(id)
                        elif viewActiveStatus:
                            if id in activeUsers:
                                activeUsersSet.add(id)
                            else:
                                middleTupleStr = middleMany.pop(id, None)
                                if middleTupleStr is not None:
                                    middleUsersDict.update({id: middleTupleStr})
                            hiddenOnlineUsersSet.add(id)
                        else:
                            hiddenActiveUsersSet.add(id)
                            hiddenOnlineUsersSet.add(id)
                    result.append((itemString0, itemString1))
                    itemsDict.update({requestUserId: {'thumb': itemString0, 'usersList': itemString1, 'showInOnlineList': viewOnlineStatus, 'showInActiveList': viewActiveStatus}})
                    thumbItemsToSet.update({id: itemsDict})
                else:
                    itemsDict.update({0: False})
                    thumbItemsToSet.update({id: itemsDict})
            else:
                if item is not False:
                    options = item.pop('usersList', None)
                    showOnlineStatus = item['showInOnlineList']
                    showActiveStatus = item['showInActiveList']
                    if options is None:
                        if privacyDict is None:
                            privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isAuthenticated = False)
                            sendFriendRequests = privacyChecker.sendFriendRequests()
                            sendPersonalMessages = privacyChecker.sendPersonalMessages()
                            viewFriends = privacyChecker.checkFriendsListPrivacy()
                            privacyToSet.update({id: {0: {'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends}}})
                        else:
                            userPrivacyDict = privacyDict.get(0, None)
                            if userPrivacyDict is None:
                                privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isAuthenticated = False)
                                sendFriendRequests = privacyChecker.sendFriendRequests()
                                sendPersonalMessages = privacyChecker.sendPersonalMessages()
                                viewFriends = privacyChecker.checkFriendsListPrivacy()
                                privacyDict.update({0: {'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends}})
                                privacyToSet.update({id: privacyDict})
                            else:
                                sendFriendRequests = userPrivacyDict.get('sendFriendRequests', None)
                                sendPersonalMessages = userPrivacyDict.get('sendPersonalMessages', None)
                                viewFriends = userPrivacyDict.get('viewFriends', None)
                                if sendFriendRequests is None or sendPersonalMessages is None or viewFriends is None:
                                    privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isAuthenticated = False)
                                    if sendFriendRequests is None:
                                        sendFriendRequests = privacyChecker.sendFriendRequests()
                                        userPrivacyDict.update({'sendFriendRequests': sendFriendRequests})
                                    if sendPersonalMessages is None:
                                        sendPersonalMessages = privacyChecker.sendPersonalMessages()
                                        userPrivacyDict.update({'sendPersonalMessages': sendPersonalMessages})
                                    if viewFriends is None:
                                        viewFriends = privacyChecker.checkFriendsListPrivacy()
                                        userPrivacyDict.update({'viewFriends': viewFriends})
                                    privacyToSet.update({id: privacyDict})
                        profileSlug = user.profileUrl
                        if friendShipStatus is True:
                            if removeText is None:
                                removeText = textDict['removeFromFriends']
                            options = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("removeFriend", kwargs = {'userId': id}), removeText)
                            if viewFriends:
                                if viewFriendsText is None:
                                    viewFriendsText = textDict['viewFriends']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                            if sendPersonalMessages:
                                if sendMessageText is None:
                                    sendMessageText = textDict['personalmessage']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                            options += '</ul></div></li>'
                        elif friendShipStatus == "request":
                            if confirmText is None:
                                confirmText = textDict['confirm']
                            if rejectText is None:
                                rejectText = textDict['reject']
                            options = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li><li><form method = "post" action = "{2}"><input type = "submit" value = "{3}"></form></li>'.format(reverse("addFriend", kwargs = {'userId': id}), confirmText, reverse("rejectFriend", kwargs = {'userId': id}), rejectText)
                            if viewFriends:
                                if viewFriendsText is None:
                                    viewFriendsText = textDict['viewFriends']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                            if sendPersonalMessages:
                                if sendMessageText is None:
                                    sendMessageText = textDict['personalmessage']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                            options += '</ul></div></li>'
                        elif friendShipStatus == "myRequest":
                            if cancelText is None:
                                cancelText = textDict['cancel']
                            options = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("cancelAddFriendRequest", kwargs = {'userId': id}), cancelText)
                            if viewFriends:
                                if viewFriendsText is None:
                                    viewFriendsText = textDict['viewFriends']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                            if sendPersonalMessages:
                                if sendMessageText is None:
                                    sendMessageText = textDict['personalmessage']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                            options += '</ul></div></li>'
                        else:
                            if sendFriendRequests:
                                if addRequestText is None:
                                    addRequestText = textDict['addToFriends']
                                options = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("addFriendRequest", kwargs = {'userId': id}), addRequestText)
                                if viewFriends:
                                    if viewFriendsText is None:
                                        viewFriendsText = textDict['viewFriends']
                                    options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                                if sendPersonalMessages:
                                    if sendMessageText is None:
                                        sendMessageText = textDict['personalmessage']
                                    options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                                options += '</ul></div></li>'
                            else:
                                options = '</li>'
                        item.update({'usersList': options})
                        thumbItemsToSet.update({id: itemsDict})
                    if showOnlineStatus and showActiveStatus:
                        if id in onlineUsers:
                            onlineUsersSet.add(id)
                            if id in activeUsers:
                                activeUsersSet.add(id)
                            else:
                                middleTupleStr = middleMany.pop(id, None)
                                if middleTupleStr is not None:
                                    middleUsersDict.update({id: middleTupleStr})
                        else:
                            iffyTimeStr = iffyMany.pop(id, None)
                            if iffyTimeStr is not None:
                                iffyUsersDict.update({id: iffyTimeStr})
                                if id in activeUsers:
                                    activeUsersSet.add(id)
                                else:
                                    middleTupleStr = middleMany.pop(id, None)
                                    if middleTupleStr is not None:
                                        middleUsersDict.update({id: middleTupleStr})
                    else:
                        if showOnlineStatus:
                            if id in onlineUsers:
                                onlineUsersSet.add(id)
                            else:
                                iffyTimeStr = iffyMany.pop(id, None)
                                if iffyTimeStr is not None:
                                    iffyUsersDict.update({id: iffyTimeStr})
                            hiddenActiveUsersSet.add(id)
                        elif showActiveStatus:
                            if id in activeUsers:
                                activeUsersSet.add(id)
                            else:
                                middleTupleStr = middleMany.pop(id, None)
                                if middleTupleStr is not None:
                                    middleUsersDict.update({id: middleTupleStr})
                            hiddenOnlineUsersSet.add(id)
                        else:
                            hiddenActiveUsersSet.add(id)
                            hiddenOnlineUsersSet.add(id)
                    result.append((item['thumb'], options))

    if len(idsToGetThumbItem) > 0:
        if isPrivacyToSet is True:
            privacyCache.set_many(privacyToSet)
        thumbsCache.set_many(thumbItemsToSet)

    if len(onlineUsersSet) > 0:
        statusDict.update({'stateOnline': list(onlineUsersSet)})
    if len(iffyUsersDict.keys()) > 0:
        statusDict.update({'stateTimer': iffyUsersDict})
    if len(activeUsersSet) > 0:
        statusDict.update({'stateActive': list(activeUsersSet)})
    if len(middleUsersDict) > 0:
        statusDict.update({'stateMiddle': middleUsersDict})

    if len(hiddenOnlineUsersSet) > 0:
        statusDict.update({'hiddenOnline': list(hiddenOnlineUsersSet)})
    if len(hiddenActiveUsersSet) > 0:
        statusDict.update({'hiddenActive': list(hiddenActiveUsersSet)})

    return statusDict, result

def createUsersListOnlineGuest(iterableIdsPageInstance, iffyCache, iffyUsers, onlineUsers):
    result = []
    statusDict = {}
    onlineUsersSet = set()
    iffyUsersDict = {}
    activeUsersSet = set()
    hiddenActiveUsersSet = set()
    middleUsersDict = {}
    thumbsCache = caches['usersListThumbs']
    iterableIds = set(iterableIdsPageInstance)
    itemsDictMany = thumbsCache.get_many(iterableIds)
    itemsDictKeys = itemsDictMany.keys()
    idsToGetThumbItem = iterableIds - itemsDictKeys
    for id in itemsDictKeys:
        itemDict = itemsDictMany[id].get(0, None)
        if itemDict is None:
            idsToGetThumbItem.add(id)
        elif itemDict is False:
            iterableIds.remove(id)
        else:
            if 'usersList' not in itemDict.keys():
                idsToGetThumbItem.add(id)

    if len(idsToGetThumbItem) > 0:
        thumbItemsToSet = {}
        privacyToSet = {}
        addRequestText = None
        confirmText = None
        cancelText = None
        rejectText = None
        removeText = None
        viewFriendsText = None
        sendMessageText = None
        userItems = mainUserProfile.objects.filter(id__in = idsToGetThumbItem)
        thpsProfiles = THPSProfile.objects.filter(toUser_id__in = idsToGetThumbItem)
        privacyCache = caches['privacyCache']
        privacyDictMany = privacyCache.get_many(idsToGetThumbItem)
        privacyDictManyKeys = privacyDictMany.keys()
        privacyToGet = idsToGetThumbItem - privacyDictManyKeys
        for id in privacyDictManyKeys:
            privacyDict = privacyDictMany[id].get(0, None)
            if privacyDict is None:
                privacyToGet.add(id)
            else:
                privacyDictKeys = privacyDict.keys()
                if 'viewProfile' not in privacyDictKeys or 'viewMainInfo' not in privacyDictKeys or 'viewTHPSInfo' not in privacyDictKeys or 'viewAvatar' not in privacyDictKeys or 'sendFriendRequests' not in privacyDictKeys or 'sendPersonalMessages' not in privacyDictKeys or 'viewFriends' not in privacyDictKeys or 'viewOnlineStatus' not in privacyDictKeys or 'viewActiveStatus' not in privacyDictKeys:
                    privacyToGet.add(id)
        isPrivacyToSet = len(privacyToGet) > 0
        if isPrivacyToSet is True:
            privacyInstances = userProfilePrivacy.objects.filter(user_id__in = privacyToGet)
            privacyToSet = {}
        textDict = usersListText(lang = 'ru').getDict()
    iffyMany = iffyCache.get_many((iterableIds & iffyUsers))
    activeUsers = caches['chatStatusActive'].get_many(iterableIds).keys()
    middleMany = caches['chatStatusMiddle'].get_many((iterableIds - activeUsers))

    for id in iterableIds:
        itemsDict = itemsDictMany.get(id, None)
        if itemsDict is None:
            privacyDict = privacyDictMany.get(id, None)
            if privacyDict is None:
                privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isAuthenticated = False)
                viewProfile = privacyChecker.checkViewProfilePrivacy()
                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                viewAvatar = privacyChecker.checkAvatarPrivacy()
                sendFriendRequests = privacyChecker.sendFriendRequests()
                sendPersonalMessages = privacyChecker.sendPersonalMessages()
                viewFriends = privacyChecker.checkFriendsListPrivacy()
                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                privacyToSet.update({id: {0: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
            else:
                userPrivacyDict = privacyDict.get(0, None)
                if userPrivacyDict is None:
                    privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isAuthenticated = False)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    sendFriendRequests = privacyChecker.sendFriendRequests()
                    sendPersonalMessages = privacyChecker.sendPersonalMessages()
                    viewFriends = privacyChecker.checkFriendsListPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                    privacyDict.update({0: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
                    privacyToSet.update({id: privacyDict})
                else:
                    viewProfile = userPrivacyDict.get('viewProfile', None)
                    viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                    viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                    viewAvatar = userPrivacyDict.get('viewAvatar', None)
                    sendFriendRequests = userPrivacyDict.get('sendFriendRequests', None)
                    sendPersonalMessages = userPrivacyDict.get('sendPersonalMessages', None)
                    viewFriends = userPrivacyDict.get('viewFriends', None)
                    viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                    viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                    if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or sendFriendRequests is None or sendPersonalMessages is None or viewFriends is None or viewOnlineStatus is None or viewActiveStatus is None:
                        privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isAuthenticated = False)
                        if viewProfile is None:
                            viewProfile = privacyChecker.checkViewProfilePrivacy()
                            userPrivacyDict.update({'viewProfile': viewProfile})
                        if viewMainInfo is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                        if viewTHPSInfo is None:
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                        if viewAvatar is None:
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            userPrivacyDict.update({'viewAvatar': viewAvatar})
                        if viewOnlineStatus is None:
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                        if viewActiveStatus is None:
                            viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                            userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                        if sendFriendRequests is None:
                            sendFriendRequests = privacyChecker.sendFriendRequests()
                            userPrivacyDict.update({'sendFriendRequests': sendFriendRequests})
                        if sendPersonalMessages is None:
                            sendPersonalMessages = privacyChecker.sendPersonalMessages()
                            userPrivacyDict.update({'sendPersonalMessages': sendPersonalMessages})
                        if viewFriends is None:
                            viewFriends = privacyChecker.checkFriendsListPrivacy()
                            userPrivacyDict.update({'viewFriends': viewFriends})
                        privacyToSet.update({id: privacyDict})
            if viewProfile:
                user = userItems.get(id__exact = id)
                profileSlug = user.profileUrl
                profilePath = reverse("profile", kwargs = {'currentUser': profileSlug})
                if viewMainInfo:
                    firstName = user.first_name or None
                    lastName = user.last_name or None
                    if firstName and lastName:
                        name = '{0} {1}'.format(firstName, lastName)
                        smallName = user.username
                    else:
                        if firstName:
                            name = firstName
                            smallName = user.username
                        else:
                            name = user.username
                            smallName = None
                else:
                    name = user.username
                    smallName = None
                if viewAvatar:
                    avatar = user.avatarThumbMiddle or None
                    if avatar is not None:
                        if smallName:
                            itemString0 = '<li value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(id, profilePath, avatar.url, name, smallName)
                        else:
                            itemString0 = '<li value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(id, profilePath, avatar.url, name)
                    else:
                        if smallName:
                            itemString0 = '<li value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(id, name, smallName, profilePath)
                        else:
                            itemString0 = '<li value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(id, name, profilePath)
                else:
                    if smallName:
                        itemString0 = '<li value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(id, name, smallName, profilePath)
                    else:
                        itemString0 = '<li value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(id, name, profilePath)
                if viewTHPSInfo:
                    try:
                        thpsProfile = thpsProfiles.get(toUser__exact = user)
                    except:
                        thpsProfile = None
                    if thpsProfile is not None:
                        thpsItem = {}
                        nick = thpsProfile.nickName
                        clan = thpsProfile.clan or None
                        if clan is not None:
                            thpsItem.update({'nick': nick, 'clan': clan})
                        else:
                            thpsItem.update({'nick': nick})
                        thpsAvatar = thpsProfile.avatarThumbMiddle or None
                        if thpsAvatar is not None:
                            thpsItem.update({'avatar': thpsProfile})
                        item.update({'thps': thpsItem})
                if friendShipStatus is True:
                    if removeText is None:
                        removeText = textDict['removeFromFriends']
                    itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("removeFriend", kwargs = {'userId': id}), removeText)
                    if viewFriends:
                        if viewFriendsText is None:
                            viewFriendsText = textDict['viewFriends']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                    if sendPersonalMessages:
                        if sendMessageText is None:
                            sendMessageText = textDict['personalmessage']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                    itemString1 += '</ul></div></li>'
                elif friendShipStatus == "request":
                    if confirmText is None:
                        confirmText = textDict['confirm']
                    if rejectText is None:
                        rejectText = textDict['reject']
                    itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li><li><form method = "post" action = "{2}"><input type = "submit" value = "{3}"></form></li>'.format(reverse("addFriend", kwargs = {'userId': id}), confirmText, reverse("rejectFriend", kwargs = {'userId': id}), rejectText)
                    if viewFriends:
                        if viewFriendsText is None:
                            viewFriendsText = textDict['viewFriends']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                    if sendPersonalMessages:
                        if sendMessageText is None:
                            sendMessageText = textDict['personalmessage']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                    itemString1 += '</ul></div></li>'
                elif friendShipStatus == "myRequest":
                    if cancelText is None:
                        cancelText = textDict['cancel']
                    itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("cancelAddFriendRequest", kwargs = {'userId': id}), cancelText)
                    if viewFriends:
                        if viewFriendsText is None:
                            viewFriendsText = textDict['viewFriends']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                    if sendPersonalMessages:
                        if sendMessageText is None:
                            sendMessageText = textDict['personalmessage']
                        itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                    itemString1 += '</ul></div></li>'
                else:
                    if sendFriendRequests:
                        if addRequestText is None:
                            addRequestText = textDict['addToFriends']
                        itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("addFriendRequest", kwargs = {'userId': id}), addRequestText)
                        if viewFriends:
                            if viewFriendsText is None:
                                viewFriendsText = textDict['viewFriends']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                        if sendPersonalMessages:
                            if sendMessageText is None:
                                sendMessageText = textDict['personalmessage']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                        itemString1 += '</ul></div></li>'
                    else:
                        itemString1 = '</li>'
                if viewOnlineStatus:
                    if id in onlineUsers:
                        onlineUsersSet.add(id)
                        if viewActiveStatus:
                            if id in activeUsers:
                                activeUsersSet.add(id)
                            else:
                                middleTupleStr = middleMany.pop(id, None)
                                if middleTupleStr is not None:
                                    middleUsersDict.update({id: middleTupleStr})
                        else:
                            hiddenActiveUsersSet.add(id)
                    else:
                        iffyTimeStr = iffyMany.pop(id, None)
                        if iffyTimeStr is not None:
                            iffyUsersDict.update({id: iffyTimeStr})
                            if viewActiveStatus:
                                if id in activeUsers:
                                    activeUsersSet.add(id)
                                else:
                                    middleTupleStr = middleMany.pop(id, None)
                                    if middleTupleStr is not None:
                                        middleUsersDict.update({id: middleTupleStr})
                            else:
                                hiddenActiveUsersSet.add(id)
                    result.append((itemString0, itemString1))
                thumbItemsToSet.update({id: {0: {'thumb': itemString0, 'usersList': itemString1, 'showInOnlineList': viewOnlineStatus, 'showInActiveList': viewActiveStatus}}})
            else:
                thumbItemsToSet.update({id: {0: False}})
        else:
            item = itemsDict.get(0, None)
            if item is None:
                privacyDict = privacyDictMany.get(id, None)
                if privacyDict is None:
                    privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isAuthenticated = False)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    sendFriendRequests = privacyChecker.sendFriendRequests()
                    sendPersonalMessages = privacyChecker.sendPersonalMessages()
                    viewFriends = privacyChecker.checkFriendsListPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                    privacyToSet.update({id: {0: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}}})
                else:
                    userPrivacyDict = privacyDict.get(0, None)
                    if userPrivacyDict is None:
                        privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isAuthenticated = False)
                        viewProfile = privacyChecker.checkViewProfilePrivacy()
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        sendFriendRequests = privacyChecker.sendFriendRequests()
                        sendPersonalMessages = privacyChecker.sendPersonalMessages()
                        viewFriends = privacyChecker.checkFriendsListPrivacy()
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                        privacyDict.update({0: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends, 'viewOnlineStatus': viewOnlineStatus, 'viewActiveStatus': viewActiveStatus}})
                        privacyToSet.update({id: privacyDict})
                    else:
                        viewProfile = userPrivacyDict.get('viewProfile', None)
                        viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                        viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                        viewAvatar = userPrivacyDict.get('viewAvatar', None)
                        sendFriendRequests = userPrivacyDict.get('sendFriendRequests', None)
                        sendPersonalMessages = userPrivacyDict.get('sendPersonalMessages', None)
                        viewFriends = userPrivacyDict.get('viewFriends', None)
                        viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                        viewActiveStatus = userPrivacyDict.get('viewActiveStatus', None)
                        if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or sendFriendRequests is None or sendPersonalMessages is None or viewFriends is None or viewOnlineStatus is None or viewActiveStatus is None:
                            privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isAuthenticated = False)
                            if viewProfile is None:
                                viewProfile = privacyChecker.checkViewProfilePrivacy()
                                userPrivacyDict.update({'viewProfile': viewProfile})
                            if viewMainInfo is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                            if viewTHPSInfo is None:
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                            if viewAvatar is None:
                                viewAvatar = privacyChecker.checkAvatarPrivacy()
                                userPrivacyDict.update({'viewAvatar': viewAvatar})
                            if viewOnlineStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                            if viewActiveStatus is None:
                                viewActiveStatus = privacyChecker.checkActiveStatusPrivacy()
                                userPrivacyDict.update({'viewActiveStatus': viewActiveStatus})
                            if sendFriendRequests is None:
                                sendFriendRequests = privacyChecker.sendFriendRequests()
                                userPrivacyDict.update({'sendFriendRequests': sendFriendRequests})
                            if sendPersonalMessages is None:
                                sendPersonalMessages = privacyChecker.sendPersonalMessages()
                                userPrivacyDict.update({'sendPersonalMessages': sendPersonalMessages})
                            if viewFriends is None:
                                viewFriends = privacyChecker.checkFriendsListPrivacy()
                                userPrivacyDict.update({'viewFriends': viewFriends})
                            privacyToSet.update({id: privacyDict})
                if viewProfile:
                    user = userItems.get(id__exact = id)
                    profileSlug = user.profileUrl
                    profilePath = reverse("profile", kwargs = {'currentUser': profileSlug})
                    if viewMainInfo:
                        firstName = user.first_name or None
                        lastName = user.last_name or None
                        if firstName and lastName:
                            name = '{0} {1}'.format(firstName, lastName)
                            smallName = user.username
                        else:
                            if firstName:
                                name = firstName
                                smallName = user.username
                            else:
                                name = user.username
                                smallName = None
                    else:
                        name = user.username
                        smallName = None
                    if viewAvatar:
                        avatar = user.avatarThumbMiddle or None
                        if avatar is not None:
                            if smallName:
                                itemString0 = '<li value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3} [{4}]" /></a></div><h3><a href = "{1}">{3}</a></h3><small><a href = "{1}">{4}</a></small>'.format(id, profilePath, avatar.url, name, smallName)
                            else:
                                itemString0 = '<li value = "{0}"><div class = "avatar-wrap"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></div><h3><a href = "{1}">{3}</a></h3>'.format(id, profilePath, avatar.url, name)
                        else:
                            if smallName:
                                itemString0 = '<li value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(id, name, smallName, profilePath)
                            else:
                                itemString0 = '<li value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(id, name, profilePath)
                    else:
                        if smallName:
                            itemString0 = '<li value = "{0}"><a href = "{3}"><div class = "no-avatar"><p>{1} [{2}]</p></div></a><h3><a href = "{3}">{1}</a></h3><small><a href = "{3}">{2}</a></small>'.format(id, name, smallName, profilePath)
                        else:
                            itemString0 = '<li value = "{0}"><a href = "{2}"><div class = "no-avatar"><p>{1}</p></div></a><h3><a href = "{2}">{1}</a></h3>'.format(id, name, profilePath)
                    if viewTHPSInfo:
                        try:
                            thpsProfile = thpsProfiles.get(toUser__exact = user)
                        except:
                            thpsProfile = None
                        if thpsProfile is not None:
                            thpsItem = {}
                            nick = thpsProfile.nickName
                            clan = thpsProfile.clan or None
                            if clan is not None:
                                thpsItem.update({'nick': nick, 'clan': clan})
                            else:
                                thpsItem.update({'nick': nick})
                            thpsAvatar = thpsProfile.avatarThumbMiddle or None
                            if thpsAvatar is not None:
                                thpsItem.update({'avatar': thpsProfile})
                            item.update({'thps': thpsItem})
                    if friendShipStatus is True:
                        if removeText is None:
                            removeText = textDict['removeFromFriends']
                        itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("removeFriend", kwargs = {'userId': id}), removeText)
                        if viewFriends:
                            if viewFriendsText is None:
                                viewFriendsText = textDict['viewFriends']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                        if sendPersonalMessages:
                            if sendMessageText is None:
                                sendMessageText = textDict['personalmessage']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                        itemString1 += '</ul></div></li>'
                    elif friendShipStatus == "request":
                        if confirmText is None:
                            confirmText = textDict['confirm']
                        if rejectText is None:
                            rejectText = textDict['reject']
                        itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li><li><form method = "post" action = "{2}"><input type = "submit" value = "{3}"></form></li>'.format(reverse("addFriend", kwargs = {'userId': id}), confirmText, reverse("rejectFriend", kwargs = {'userId': id}), rejectText)
                        if viewFriends:
                            if viewFriendsText is None:
                                viewFriendsText = textDict['viewFriends']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                        if sendPersonalMessages:
                            if sendMessageText is None:
                                sendMessageText = textDict['personalmessage']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                        itemString1 += '</ul></div></li>'
                    elif friendShipStatus == "myRequest":
                        if cancelText is None:
                            cancelText = textDict['cancel']
                        itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("cancelAddFriendRequest", kwargs = {'userId': id}), cancelText)
                        if viewFriends:
                            if viewFriendsText is None:
                                viewFriendsText = textDict['viewFriends']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                        if sendPersonalMessages:
                            if sendMessageText is None:
                                sendMessageText = textDict['personalmessage']
                            itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                        itemString1 += '</ul></div></li>'
                    else:
                        if sendFriendRequests:
                            if addRequestText is None:
                                addRequestText = textDict['addToFriends']
                            itemString1 = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("addFriendRequest", kwargs = {'userId': id}), addRequestText)
                            if viewFriends:
                                if viewFriendsText is None:
                                    viewFriendsText = textDict['viewFriends']
                                itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                            if sendPersonalMessages:
                                if sendMessageText is None:
                                    sendMessageText = textDict['personalmessage']
                                itemString1 += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                            itemString1 += '</ul></div></li>'
                        else:
                            itemString1 = '</li>'
                    if viewOnlineStatus:
                        if id in onlineUsers:
                            onlineUsersSet.add(id)
                            if viewActiveStatus:
                                if id in activeUsers:
                                    activeUsersSet.add(id)
                                else:
                                    middleTupleStr = middleMany.pop(id, None)
                                    if middleTupleStr is not None:
                                        middleUsersDict.update({id: middleTupleStr})
                            else:
                                hiddenActiveUsersSet.add(id)
                        else:
                            iffyTimeStr = iffyMany.pop(id, None)
                            if iffyTimeStr is not None:
                                iffyUsersDict.update({id: iffyTimeStr})
                                if viewActiveStatus:
                                    if id in activeUsers:
                                        activeUsersSet.add(id)
                                    else:
                                        middleTupleStr = middleMany.pop(id, None)
                                        if middleTupleStr is not None:
                                            middleUsersDict.update({id: middleTupleStr})
                                else:
                                    hiddenActiveUsersSet.add(id)
                        result.append((itemString0, itemString1))
                    itemsDict.update({0: {'thumb': itemString0, 'usersList': itemString1, 'showInOnlineList': viewOnlineStatus, 'showInActiveList': viewActiveStatus}})
                    thumbItemsToSet.update({id: itemsDict})
                else:
                    itemsDict.update({0: False})
                    thumbItemsToSet.update({id: itemsDict})
            else:
                if item is not False:
                    options = item.pop('usersList', None)
                    showOnlineStatus = item['showInOnlineList']
                    showActiveStatus = item['showInActiveList']
                    if options is None:
                        if privacyDict is None:
                            privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isAuthenticated = False)
                            sendFriendRequests = privacyChecker.sendFriendRequests()
                            sendPersonalMessages = privacyChecker.sendPersonalMessages()
                            viewFriends = privacyChecker.checkFriendsListPrivacy()
                            privacyToSet.update({id: {0: {'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends}}})
                        else:
                            userPrivacyDict = privacyDict.get(0, None)
                            if userPrivacyDict is None:
                                privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isAuthenticated = False)
                                sendFriendRequests = privacyChecker.sendFriendRequests()
                                sendPersonalMessages = privacyChecker.sendPersonalMessages()
                                viewFriends = privacyChecker.checkFriendsListPrivacy()
                                privacyDict.update({0: {'sendFriendRequests': sendFriendRequests, 'sendPersonalMessages': sendPersonalMessages, 'viewFriends': viewFriends}})
                                privacyToSet.update({id: privacyDict})
                            else:
                                sendFriendRequests = userPrivacyDict.get('sendFriendRequests', None)
                                sendPersonalMessages = userPrivacyDict.get('sendPersonalMessages', None)
                                viewFriends = userPrivacyDict.get('viewFriends', None)
                                if sendFriendRequests is None or sendPersonalMessages is None or viewFriends is None:
                                    privacyChecker = checkPrivacy(privacy = privacyInstances.get(user_id__exact = id), isAuthenticated = False)
                                    if sendFriendRequests is None:
                                        sendFriendRequests = privacyChecker.sendFriendRequests()
                                        userPrivacyDict.update({'sendFriendRequests': sendFriendRequests})
                                    if sendPersonalMessages is None:
                                        sendPersonalMessages = privacyChecker.sendPersonalMessages()
                                        userPrivacyDict.update({'sendPersonalMessages': sendPersonalMessages})
                                    if viewFriends is None:
                                        viewFriends = privacyChecker.checkFriendsListPrivacy()
                                        userPrivacyDict.update({'viewFriends': viewFriends})
                                    privacyToSet.update({id: privacyDict})
                        profileSlug = user.profileUrl
                        if friendShipStatus is True:
                            if removeText is None:
                                removeText = textDict['removeFromFriends']
                            options = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("removeFriend", kwargs = {'userId': id}), removeText)
                            if viewFriends:
                                if viewFriendsText is None:
                                    viewFriendsText = textDict['viewFriends']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                            if sendPersonalMessages:
                                if sendMessageText is None:
                                    sendMessageText = textDict['personalmessage']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                            options += '</ul></div></li>'
                        elif friendShipStatus == "request":
                            if confirmText is None:
                                confirmText = textDict['confirm']
                            if rejectText is None:
                                rejectText = textDict['reject']
                            options = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li><li><form method = "post" action = "{2}"><input type = "submit" value = "{3}"></form></li>'.format(reverse("addFriend", kwargs = {'userId': id}), confirmText, reverse("rejectFriend", kwargs = {'userId': id}), rejectText)
                            if viewFriends:
                                if viewFriendsText is None:
                                    viewFriendsText = textDict['viewFriends']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                            if sendPersonalMessages:
                                if sendMessageText is None:
                                    sendMessageText = textDict['personalmessage']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                            options += '</ul></div></li>'
                        elif friendShipStatus == "myRequest":
                            if cancelText is None:
                                cancelText = textDict['cancel']
                            options = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("cancelAddFriendRequest", kwargs = {'userId': id}), cancelText)
                            if viewFriends:
                                if viewFriendsText is None:
                                    viewFriendsText = textDict['viewFriends']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                            if sendPersonalMessages:
                                if sendMessageText is None:
                                    sendMessageText = textDict['personalmessage']
                                options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                            options += '</ul></div></li>'
                        else:
                            if sendFriendRequests:
                                if addRequestText is None:
                                    addRequestText = textDict['addToFriends']
                                options = '<div class = "actions"><ul><li><form method = "post" action = "{0}"><input type = "submit" value = "{1}" /></form></li>'.format(reverse("addFriendRequest", kwargs = {'userId': id}), addRequestText)
                                if viewFriends:
                                    if viewFriendsText is None:
                                        viewFriendsText = textDict['viewFriends']
                                    options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("friends", kwargs = {'user': profileSlug, 'category': 'all', 'sep': '', 'page': ''}), viewFriendsText)
                                if sendPersonalMessages:
                                    if sendMessageText is None:
                                        sendMessageText = textDict['personalmessage']
                                    options += '<li><form method = "get" action = "{0}"><input type = "submit" value = "{1}"></form></li>'.format(reverse("personalMessage", kwargs = {'toUser': profileSlug}), sendMessageText)
                                options += '</ul></div></li>'
                            else:
                                options = '</li>'
                        item.update({'usersList': options})
                        thumbItemsToSet.update({id: itemsDict})
                    if showOnlineStatus:
                        if id in onlineUsers:
                            onlineUsersSet.add(id)
                            if showActiveStatus:
                                if id in activeUsers:
                                    activeUsersSet.add(id)
                                else:
                                    middleTupleStr = middleMany.pop(id, None)
                                    if middleTupleStr is not None:
                                        middleUsersDict.update({id: middleTupleStr})
                            else:
                                hiddenActiveUsersSet.add(id)
                        else:
                            iffyTimeStr = iffyMany.pop(id, None)
                            if iffyTimeStr is not None:
                                iffyUsersDict.update({id: iffyTimeStr})
                                if showActiveStatus:
                                    if id in activeUsers:
                                        activeUsersSet.add(id)
                                    else:
                                        middleTupleStr = middleMany.pop(id, None)
                                        if middleTupleStr is not None:
                                            middleUsersDict.update({id: middleTupleStr})
                                else:
                                    hiddenActiveUsersSet.add(id)
                        result.append((item['thumb'], options))

    if len(idsToGetThumbItem) > 0:
        if isPrivacyToSet is True:
            privacyCache.set_many(privacyToSet)
        thumbsCache.set_many(thumbItemsToSet)

    if len(onlineUsersSet) > 0:
        statusDict.update({'stateOnline': list(onlineUsersSet)})
    if len(iffyUsersDict.keys()) > 0:
        statusDict.update({'stateTimer': iffyUsersDict})
    if len(activeUsersSet) > 0:
        statusDict.update({'stateActive': list(activeUsersSet)})
    if len(middleUsersDict) > 0:
        statusDict.update({'stateMiddle': middleUsersDict})

    if len(hiddenActiveUsersSet) > 0:
        statusDict.update({'hiddenActive': list(hiddenActiveUsersSet)})

    return statusDict, result