from os import path
from datetime import datetime
from django.urls import reverse
from django.conf import settings
from django.http import JsonResponse
from django.core.cache import caches
from psixiSocial.models import mainUserProfile, THPSProfile, userProfilePrivacy
from psixiSocial.privacyIdentifier import checkPrivacy
from psixiSocial.translators import singleElementGetter

def getSmallThumbXHR(request):
    secondUserId = request.GET.get('id', None)
    if secondUserId:
        secondUser = None
        secondUserId = int(secondUserId)
        user = request.user
        if user.is_authenticated:
            id = user.id
            thumbsCache = caches['smallThumbs']
            thumbsDict = thumbsCache.get(secondUserId, None)
            if thumbsDict is None:
                secondUser = mainUserProfile.objects.get(id__exact = secondUserId)
                privacyCache = caches['privacyCache']
                privacyDict = privacyCache.get(secondUserId, None)
                if privacyDict is None:
                    friendShipCache = caches['friendShipStatusCache']
                    friendShipDict = friendShipCache.get(secondUserId, None)
                    if friendShipDict is None:
                        friendsIdsCache = caches['friendsIdCache']
                        friendsIds = friendsIdsCache.get(id, None)
                        if friendsIds is None:
                            friendsCache = caches['friendsCache']
                            friends = friendsCache.get(id, None)
                            if friends is None:
                                friends = user.friends.all()
                                if friends.count():
                                    friendsCache.set(id, friends)
                                    friendsIdsQuerySet = userFriends.values("id")
                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                else:
                                    friendsCache.set(id, False)
                                    friendsIds = set()
                            else:
                                if friends is False:
                                    friendsIds = set()
                                else:
                                    friendsIdsQuerySet = userFriends.values("id")
                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                            friendsIdsCache.set(id, friendsIds)
                        if secondUserId in friendsIds:
                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                            friendShipCache.set(secondUserId, {secondUserId: {id: True}})
                        else:
                            friendShipRequestsCache = caches["friendshipRequests"]
                            friendRequests = friendShipRequestsCache.get(id, set())
                            friendShipStatus = False
                            for item in friendRequests:
                                if item[0] == secondUserId:
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                    friendShipStatus = "request"
                                    break
                            if friendShipStatus is False:
                                friendRequests = friendShipRequestsCache.get(secondUserId, set())
                                for item in friendRequests:
                                    if item[0] == id:
                                        friendShipStatus = "myRequest"
                                        break
                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = False, isAuthenticated = True)
                            friendShipCache.set(secondUserId, {secondUserId: {id: friendShipStatus}})
                    else:
                        friendShipStatus = friendShipDict.get(id, None)
                        if friendShipStatus is None:
                            friendsIdsCache = caches['friendsIdCache']
                            friendsIds = friendsIdsCache.get(id, None)
                            if friendsIds is None:
                                friendsCache = caches['friendsCache']
                                friends = friendsCache.get(id, None)
                                if friends is None:
                                    friends = user.friends.all()
                                    if friends.count():
                                        friendsCache.set(id, friends)
                                        friendsIdsQuerySet = userFriends.values("id")
                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    else:
                                        friendsCache.set(id, False)
                                        friendsIds = set()
                                else:
                                    if friends is False:
                                        friendsIds = set()
                                    else:
                                        friendsIdsQuerySet = userFriends.values("id")
                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                friendsIdsCache.set(id, friendsIds)
                            if secondUserId in friendsIds:
                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                friendShipDict.update({id: True})
                                friendShipCache.set(secondUserId, friendShipDict)
                            else:
                                friendShipRequestsCache = caches["friendshipRequests"]
                                friendRequests = friendShipRequestsCache.get(id, set())
                                friendShipStatus = False
                                for item in friendRequests:
                                    if item[0] == secondUserId:
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                        friendShipStatus = "request"
                                        break
                                if friendShipStatus is False:
                                    friendRequests = friendShipRequestsCache.get(secondUserId, set())
                                    for item in friendRequests:
                                        if item[0] == id:
                                            friendShipStatus = "myRequest"
                                            break
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = False, isAuthenticated = True)
                                friendShipDict.update({id: friendShipStatus})
                                friendShipCache.set(secondUserId, friendShipDict)
                        else:
                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    privacyCache.set(secondUserId, {id: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                else:
                    userPrivacyDict = privacyDict.get(id, None)
                    if userPrivacyDict is None:
                        friendShipCache = caches['friendShipStatusCache']
                        friendShipDict = friendShipCache.get(secondUserId, None)
                        if friendShipDict is None:
                            friendsIdsCache = caches['friendsIdCache']
                            friendsIds = friendsIdsCache.get(id, None)
                            if friendsIds is None:
                                friendsCache = caches['friendsCache']
                                friends = friendsCache.get(id, None)
                                if friends is None:
                                    friends = user.friends.all()
                                    if friends.count():
                                        friendsCache.set(id, friends)
                                        friendsIdsQuerySet = userFriends.values("id")
                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    else:
                                        friendsCache.set(id, False)
                                        friendsIds = set()
                                else:
                                    if friends is False:
                                        friendsIds = set()
                                    else:
                                        friendsIdsQuerySet = userFriends.values("id")
                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                friendsIdsCache.set(id, friendsIds)
                            if secondUserId in friendsIds:
                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                friendShipCache.set(secondUserId, {secondUserId: {id: True}})
                            else:
                                friendShipRequestsCache = caches["friendshipRequests"]
                                friendRequests = friendShipRequestsCache.get(id, set())
                                friendShipStatus = False
                                for item in friendRequests:
                                    if item[0] == secondUserId:
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                        friendShipStatus = "request"
                                        break
                                if friendShipStatus is False:
                                    friendRequests = friendShipRequestsCache.get(secondUserId, set())
                                    for item in friendRequests:
                                        if item[0] == id:
                                            friendShipStatus = "myRequest"
                                            break
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = False, isAuthenticated = True)
                                friendShipCache.set(secondUserId, {secondUserId: {id: friendShipStatus}})
                        else:
                            friendShipStatus = friendShipDict.get(id, None)
                            if friendShipStatus is None:
                                friendsIdsCache = caches['friendsIdCache']
                                friendsIds = friendsIdsCache.get(id, None)
                                if friendsIds is None:
                                    friendsCache = caches['friendsCache']
                                    friends = friendsCache.get(id, None)
                                    if friends is None:
                                        friends = user.friends.all()
                                        if friends.count():
                                            friendsCache.set(id, friends)
                                            friendsIdsQuerySet = userFriends.values("id")
                                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                        else:
                                            friendsCache.set(id, False)
                                            friendsIds = set()
                                    else:
                                        if friends is False:
                                            friendsIds = set()
                                        else:
                                            friendsIdsQuerySet = userFriends.values("id")
                                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    friendsIdsCache.set(id, friendsIds)
                                if secondUserId in friendsIds:
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                    friendShipDict.update({id: True})
                                    friendShipCache.set(secondUserId, friendShipDict)
                                else:
                                    friendShipRequestsCache = caches["friendshipRequests"]
                                    friendRequests = friendShipRequestsCache.get(id, set())
                                    friendShipStatus = False
                                    for item in friendRequests:
                                        if item[0] == secondUserId:
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                            friendShipStatus = "request"
                                            break
                                    if friendShipStatus is False:
                                        friendRequests = friendShipRequestsCache.get(secondUserId, set())
                                        for item in friendRequests:
                                            if item[0] == id:
                                                friendShipStatus = "myRequest"
                                                break
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = False, isAuthenticated = True)
                                    friendShipDict.update({id: friendShipStatus})
                                    friendShipCache.set(secondUserId, friendShipDict)
                            else:
                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                        viewProfile = privacyChecker.checkViewProfilePrivacy()
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        privacyDict.update({id: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                        privacyCache.set(secondUserId, privacyDict)
                    else:
                        viewProfile = userPrivacyDict.get('viewProfile', None)
                        viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                        viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                        viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                        viewAvatar = userPrivacyDict.get('viewAvatar', None)
                        if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewOnlineStatus is None or viewAvatar is None:
                            friendShipCache = caches['friendShipStatusCache']
                            friendShipDict = friendShipCache.get(secondUserId, None)
                            if friendShipDict is None:
                                friendsIdsCache = caches['friendsIdCache']
                                friendsIds = friendsIdsCache.get(id, None)
                                if friendsIds is None:
                                    friendsCache = caches['friendsCache']
                                    friends = friendsCache.get(id, None)
                                    if friends is None:
                                        friends = user.friends.all()
                                        if friends.count():
                                            friendsCache.set(id, friends)
                                            friendsIdsQuerySet = userFriends.values("id")
                                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                        else:
                                            friendsCache.set(id, False)
                                            friendsIds = set()
                                    else:
                                        if friends is False:
                                            friendsIds = set()
                                        else:
                                            friendsIdsQuerySet = userFriends.values("id")
                                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    friendsIdsCache.set(id, friendsIds)
                                if secondUserId in friendsIds:
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                    friendShipCache.set(secondUserId, {secondUserId: {id: True}})
                                else:
                                    friendShipRequestsCache = caches["friendshipRequests"]
                                    friendRequests = friendShipRequestsCache.get(id, set())
                                    friendShipStatus = False
                                    for item in friendRequests:
                                        if item[0] == secondUserId:
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                            friendShipStatus = "request"
                                            break
                                    if friendShipStatus is False:
                                        friendRequests = friendShipRequestsCache.get(secondUserId, set())
                                        for item in friendRequests:
                                            if item[0] == id:
                                                friendShipStatus = "myRequest"
                                                break
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = False, isAuthenticated = True)
                                    friendShipCache.set(secondUserId, {secondUserId: {id: friendShipStatus}})
                            else:
                                friendShipStatus = friendShipDict.get(id, None)
                                if friendShipStatus is None:
                                    friendsIdsCache = caches['friendsIdCache']
                                    friendsIds = friendsIdsCache.get(id, None)
                                    if friendsIds is None:
                                        friendsCache = caches['friendsCache']
                                        friends = friendsCache.get(id, None)
                                        if friends is None:
                                            friends = user.friends.all()
                                            if friends.count():
                                                friendsCache.set(id, friends)
                                                friendsIdsQuerySet = userFriends.values("id")
                                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                            else:
                                                friendsCache.set(id, False)
                                                friendsIds = set()
                                        else:
                                            if friends is False:
                                                friendsIds = set()
                                            else:
                                                friendsIdsQuerySet = userFriends.values("id")
                                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                        friendsIdsCache.set(id, friendsIds)
                                    if secondUserId in friendsIds:
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                        friendShipDict.update({id: True})
                                        friendShipCache.set(secondUserId, friendShipDict)
                                    else:
                                        friendShipRequestsCache = caches["friendshipRequests"]
                                        friendRequests = friendShipRequestsCache.get(id, set())
                                        friendShipStatus = False
                                        for item in friendRequests:
                                            if item[0] == secondUserId:
                                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                                friendShipStatus = "request"
                                                break
                                        if friendShipStatus is False:
                                            friendRequests = friendShipRequestsCache.get(secondUserId, set())
                                            for item in friendRequests:
                                                if item[0] == id:
                                                    friendShipStatus = "myRequest"
                                                    break
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = False, isAuthenticated = True)
                                        friendShipDict.update({id: friendShipStatus})
                                        friendShipCache.set(secondUserId, friendShipDict)
                                else:
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                            if viewProfile is None:
                                viewProfile = privacyChecker.checkViewProfilePrivacy()
                                userPrivacyDict.update({'viewProfile': viewProfile})
                            if viewOnlineStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                userPrivacyDict.update({'viewProfile': viewProfile})
                            if viewMainInfo is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                            if viewTHPSInfo is None:
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                            if viewAvatar is None:
                                viewAvatar = privacyChecker.checkAvatarPrivacy()
                                userPrivacyDict.update({'viewAvatar': viewAvatar})
                            privacyCache.set(secondUserId, privacyDict)
                if viewProfile:
                    if viewMainInfo:
                        firstName = secondUser.first_name or None
                        lastName = secondUser.last_name or None
                        if firstName and lastName:
                            name = '{0} {1}'.format(firstName, lastName)
                        else:
                            if firstName:
                                name = firstName
                            else:
                                name = secondUser.username
                    else:
                        name = secondUser.username
                    if viewTHPSInfo:
                        try:
                            thpsProfile = thpsProfiles.get(toUser__exact = secondUser)
                        except:
                            thpsProfile = None
                        if thpsProfile is not None:
                            nick = thpsProfile.nickName 
                            clan = thpsProfile.clan or None
                            if clan is not None:
                                name = '{0} [{1}*{2}]'.format(name, nick, clan)
                            else:
                                name = '{0} [{1}]'.format(name, nick)
                    if viewAvatar:
                        image = secondUser.avatarThumbSmall or None
                        if image is None:
                            if viewTHPSInfo:
                                if thpsProfile is not None:
                                    if clan is not None:
                                        background = clan.background
                                        logo = clan.logo
                                        if background and logo:
                                            image = generateSmallThumbs(logo)
                                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(secondUserId, reverse("profile", kwargs = {'currentUser': secondUser.profileUrl}), image, name)
                                        else:
                                            if logo:
                                                image = generateSmallThumbs(logo)
                                                itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(secondUserId, reverse("profile", kwargs = {'currentUser': secondUser.profileUrl}), image, name)
                                            else:
                                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(secondUserId, reverse("profile", kwargs = {'currentUser': secondUser.profileUrl}), name)
                                    else:
                                        itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(secondUserId, reverse("profile", kwargs = {'currentUser': secondUser.profileUrl}), name)
                                else:
                                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(secondUserId, reverse("profile", kwargs = {'currentUser': secondUser.profileUrl}), image, name)
                            else:
                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(secondUserId, reverse("profile", kwargs = {'currentUser': secondUser.profileUrl}), name)
                        else:
                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(secondUserId, reverse("profile", kwargs = {'currentUser': secondUser.profileUrl}), image.url, name)
                    else:
                        itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(secondUserId, reverse("profile", kwargs = {'currentUser': secondUser.profileUrl}), name)
                    thumb = {'item': itemString, 'showInOnlineList': viewOnlineStatus}
                    if viewOnlineStatus:
                        iffyCache = caches['iffyUsersOnlineStatus']
                        iffyUsers = iffyCache.get('iffy', set())
                        if id in iffyUsers:
                            iffyTime = iffyCache.get(secondUserId, None)
                            if isinstance(iffyTime, datetime):
                                status = {'iffy': str(iffyTime)}
                            else:
                                status = 'online'
                        else:
                            onlineUsers = caches['onlineUsers'].get('online', set())
                            if id in onlineUsers:
                                status = 'online'
                            else:
                                status = False
                    thumbBody = itemString
                else:
                    thumb = False
                    thumbBody = False
                thumbsCache.set(secondUserId, {id: thumb})
            else:
                thumb = thumbsDict.get(id, None)
                if thumb is None:
                    secondUser = mainUserProfile.objects.get(id__exact = secondUserId)
                    privacyCache = caches['privacyCache']
                    privacyDict = privacyCache.get(secondUserId, None)
                    if privacyDict is None:
                        friendShipCache = caches['friendShipStatusCache']
                        friendShipDict = friendShipCache.get(secondUserId, None)
                        if friendShipDict is None:
                            friendsIdsCache = caches['friendsIdCache']
                            friendsIds = friendsIdsCache.get(id, None)
                            if friendsIds is None:
                                friendsCache = caches['friendsCache']
                                friends = friendsCache.get(id, None)
                                if friends is None:
                                    friends = user.friends.all()
                                    if friends.count():
                                        friendsCache.set(id, friends)
                                        friendsIdsQuerySet = userFriends.values("id")
                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    else:
                                        friendsCache.set(id, False)
                                        friendsIds = set()
                                else:
                                    if friends is False:
                                        friendsIds = set()
                                    else:
                                        friendsIdsQuerySet = userFriends.values("id")
                                        friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                friendsIdsCache.set(id, friendsIds)
                            if secondUserId in friendsIds:
                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                friendShipCache.set(secondUserId, {secondUserId: {id: True}})
                            else:
                                friendShipRequestsCache = caches["friendshipRequests"]
                                friendRequests = friendShipRequestsCache.get(id, set())
                                friendShipStatus = False
                                for item in friendRequests:
                                    if item[0] == secondUserId:
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                        friendShipStatus = "request"
                                        break
                                if friendShipStatus is False:
                                    friendRequests = friendShipRequestsCache.get(secondUserId, set())
                                    for item in friendRequests:
                                        if item[0] == id:
                                            friendShipStatus = "myRequest"
                                            break
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = False, isAuthenticated = True)
                                friendShipCache.set(secondUserId, {secondUserId: {id: friendShipStatus}})
                        else:
                            friendShipStatus = friendShipDict.get(id, None)
                            if friendShipStatus is None:
                                friendsIdsCache = caches['friendsIdCache']
                                friendsIds = friendsIdsCache.get(id, None)
                                if friendsIds is None:
                                    friendsCache = caches['friendsCache']
                                    friends = friendsCache.get(id, None)
                                    if friends is None:
                                        friends = user.friends.all()
                                        if friends.count():
                                            friendsCache.set(id, friends)
                                            friendsIdsQuerySet = userFriends.values("id")
                                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                        else:
                                            friendsCache.set(id, False)
                                            friendsIds = set()
                                    else:
                                        if friends is False:
                                            friendsIds = set()
                                        else:
                                            friendsIdsQuerySet = userFriends.values("id")
                                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    friendsIdsCache.set(id, friendsIds)
                                if secondUserId in friendsIds:
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                    friendShipDict.update({id: True})
                                    friendShipCache.set(secondUserId, friendShipDict)
                                else:
                                    friendShipRequestsCache = caches["friendshipRequests"]
                                    friendRequests = friendShipRequestsCache.get(id, set())
                                    friendShipStatus = False
                                    for item in friendRequests:
                                        if item[0] == secondUserId:
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                            friendShipStatus = "request"
                                            break
                                    if friendShipStatus is False:
                                        friendRequests = friendShipRequestsCache.get(secondUserId, set())
                                        for item in friendRequests:
                                            if item[0] == id:
                                                friendShipStatus = "myRequest"
                                                break
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = False, isAuthenticated = True)
                                    friendShipDict.update({id: friendShipStatus})
                                    friendShipCache.set(secondUserId, friendShipDict)
                            else:
                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                        viewProfile = privacyChecker.checkViewProfilePrivacy()
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        privacyCache.set(secondUserId, {id: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                    else:
                        userPrivacyDict = privacyDict.get(id, None)
                        if userPrivacyDict is None:
                            friendShipCache = caches['friendShipStatusCache']
                            friendShipDict = friendShipCache.get(secondUserId, None)
                            if friendShipDict is None:
                                friendsIdsCache = caches['friendsIdCache']
                                friendsIds = friendsIdsCache.get(id, None)
                                if friendsIds is None:
                                    friendsCache = caches['friendsCache']
                                    friends = friendsCache.get(id, None)
                                    if friends is None:
                                        friends = user.friends.all()
                                        if friends.count():
                                            friendsCache.set(id, friends)
                                            friendsIdsQuerySet = userFriends.values("id")
                                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                        else:
                                            friendsCache.set(id, False)
                                            friendsIds = set()
                                    else:
                                        if friends is False:
                                            friendsIds = set()
                                        else:
                                            friendsIdsQuerySet = userFriends.values("id")
                                            friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                    friendsIdsCache.set(id, friendsIds)
                                if secondUserId in friendsIds:
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                    friendShipCache.set(secondUserId, {secondUserId: {id: True}})
                                else:
                                    friendShipRequestsCache = caches["friendshipRequests"]
                                    friendRequests = friendShipRequestsCache.get(id, set())
                                    friendShipStatus = False
                                    for item in friendRequests:
                                        if item[0] == secondUserId:
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                            friendShipStatus = "request"
                                            break
                                    if friendShipStatus is False:
                                        friendRequests = friendShipRequestsCache.get(secondUserId, set())
                                        for item in friendRequests:
                                            if item[0] == id:
                                                friendShipStatus = "myRequest"
                                                break
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = False, isAuthenticated = True)
                                    friendShipCache.set(secondUserId, {secondUserId: {id: friendShipStatus}})
                            else:
                                friendShipStatus = friendShipDict.get(id, None)
                                if friendShipStatus is None:
                                    friendsIdsCache = caches['friendsIdCache']
                                    friendsIds = friendsIdsCache.get(id, None)
                                    if friendsIds is None:
                                        friendsCache = caches['friendsCache']
                                        friends = friendsCache.get(id, None)
                                        if friends is None:
                                            friends = user.friends.all()
                                            if friends.count():
                                                friendsCache.set(id, friends)
                                                friendsIdsQuerySet = userFriends.values("id")
                                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                            else:
                                                friendsCache.set(id, False)
                                                friendsIds = set()
                                        else:
                                            if friends is False:
                                                friendsIds = set()
                                            else:
                                                friendsIdsQuerySet = userFriends.values("id")
                                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                        friendsIdsCache.set(id, friendsIds)
                                    if secondUserId in friendsIds:
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                        friendShipDict.update({id: True})
                                        friendShipCache.set(secondUserId, friendShipDict)
                                    else:
                                        friendShipRequestsCache = caches["friendshipRequests"]
                                        friendRequests = friendShipRequestsCache.get(id, set())
                                        friendShipStatus = False
                                        for item in friendRequests:
                                            if item[0] == secondUserId:
                                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                                friendShipStatus = "request"
                                                break
                                        if friendShipStatus is False:
                                            friendRequests = friendShipRequestsCache.get(secondUserId, set())
                                            for item in friendRequests:
                                                if item[0] == id:
                                                    friendShipStatus = "myRequest"
                                                    break
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = False, isAuthenticated = True)
                                        friendShipDict.update({id: friendShipStatus})
                                        friendShipCache.set(secondUserId, friendShipDict)
                                else:
                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                            viewProfile = privacyChecker.checkViewProfilePrivacy()
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            privacyDict.update({id: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                            privacyCache.set(secondUserId, privacyDict)
                        else:
                            viewProfile = userPrivacyDict.get('viewProfile', None)
                            viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                            viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                            viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                            viewAvatar = userPrivacyDict.get('viewAvatar', None)
                            if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewOnlineStatus is None or viewAvatar is None:
                                friendShipCache = caches['friendShipStatusCache']
                                friendShipDict = friendShipCache.get(secondUserId, None)
                                if friendShipDict is None:
                                    friendsIdsCache = caches['friendsIdCache']
                                    friendsIds = friendsIdsCache.get(id, None)
                                    if friendsIds is None:
                                        friendsCache = caches['friendsCache']
                                        friends = friendsCache.get(id, None)
                                        if friends is None:
                                            friends = user.friends.all()
                                            if friends.count():
                                                friendsCache.set(id, friends)
                                                friendsIdsQuerySet = userFriends.values("id")
                                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                            else:
                                                friendsCache.set(id, False)
                                                friendsIds = set()
                                        else:
                                            if friends is False:
                                                friendsIds = set()
                                            else:
                                                friendsIdsQuerySet = userFriends.values("id")
                                                friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                        friendsIdsCache.set(id, friendsIds)
                                    if secondUserId in friendsIds:
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                        friendShipCache.set(secondUserId, {secondUserId: {id: True}})
                                    else:
                                        friendShipRequestsCache = caches["friendshipRequests"]
                                        friendRequests = friendShipRequestsCache.get(id, set())
                                        friendShipStatus = False
                                        for item in friendRequests:
                                            if item[0] == secondUserId:
                                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                                friendShipStatus = "request"
                                                break
                                        if friendShipStatus is False:
                                            friendRequests = friendShipRequestsCache.get(secondUserId, set())
                                            for item in friendRequests:
                                                if item[0] == id:
                                                    friendShipStatus = "myRequest"
                                                    break
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = False, isAuthenticated = True)
                                        friendShipCache.set(secondUserId, {secondUserId: {id: friendShipStatus}})
                                else:
                                    friendShipStatus = friendShipDict.get(id, None)
                                    if friendShipStatus is None:
                                        friendsIdsCache = caches['friendsIdCache']
                                        friendsIds = friendsIdsCache.get(id, None)
                                        if friendsIds is None:
                                            friendsCache = caches['friendsCache']
                                            friends = friendsCache.get(id, None)
                                            if friends is None:
                                                friends = user.friends.all()
                                                if friends.count():
                                                    friendsCache.set(id, friends)
                                                    friendsIdsQuerySet = userFriends.values("id")
                                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                                else:
                                                    friendsCache.set(id, False)
                                                    friendsIds = set()
                                            else:
                                                if friends is False:
                                                    friendsIds = set()
                                                else:
                                                    friendsIdsQuerySet = userFriends.values("id")
                                                    friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
                                            friendsIdsCache.set(id, friendsIds)
                                        if secondUserId in friendsIds:
                                            privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                            friendShipDict.update({id: True})
                                            friendShipCache.set(secondUserId, friendShipDict)
                                        else:
                                            friendShipRequestsCache = caches["friendshipRequests"]
                                            friendRequests = friendShipRequestsCache.get(id, set())
                                            friendShipStatus = False
                                            for item in friendRequests:
                                                if item[0] == secondUserId:
                                                    privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = True, isAuthenticated = True)
                                                    friendShipStatus = "request"
                                                    break
                                            if friendShipStatus is False:
                                                friendRequests = friendShipRequestsCache.get(secondUserId, set())
                                                for item in friendRequests:
                                                    if item[0] == id:
                                                        friendShipStatus = "myRequest"
                                                        break
                                                privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = False, isAuthenticated = True)
                                            friendShipDict.update({id: friendShipStatus})
                                            friendShipCache.set(secondUserId, friendShipDict)
                                    else:
                                        privacyChecker = checkPrivacy(privacy = userProfilePrivacy.objects.get(user__exact = secondUser), isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                                if viewProfile is None:
                                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                                    userPrivacyDict.update({'viewProfile': viewProfile})
                                if viewOnlineStatus is None:
                                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                    userPrivacyDict.update({'viewProfile': viewProfile})
                                if viewMainInfo is None:
                                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                    userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                                if viewTHPSInfo is None:
                                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                    userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                                if viewAvatar is None:
                                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                                    userPrivacyDict.update({'viewAvatar': viewAvatar})
                                privacyCache.set(secondUserId, privacyDict)
                    if viewProfile:
                        if viewMainInfo:
                            firstName = secondUser.first_name or None
                            lastName = secondUser.last_name or None
                            if firstName and lastName:
                                name = '{0} {1}'.format(firstName, lastName)
                            else:
                                if firstName:
                                    name = firstName
                                else:
                                    name = secondUser.username
                        else:
                            name = secondUser.username
                        if viewTHPSInfo:
                            try:
                                thpsProfile = thpsProfiles.get(toUser__exact = secondUser)
                            except:
                                thpsProfile = None
                            if thpsProfile is not None:
                                nick = thpsProfile.nickName 
                                clan = thpsProfile.clan or None
                                if clan is not None:
                                    name = '{0} [{1}*{2}]'.format(name, nick, clan)
                                else:
                                    name = '{0} [{1}]'.format(name, nick)
                        if viewAvatar:
                            image = secondUser.avatarThumbSmall or None
                            if image is None:
                                if viewTHPSInfo:
                                    if thpsProfile is not None:
                                        if clan is not None:
                                            background = clan.background
                                            logo = clan.logo
                                            if background and logo:
                                                image = generateSmallThumbs(logo)
                                                itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(secondUserId, reverse("profile", kwargs = {'currentUser': secondUser.profileUrl}), image, name)
                                            else:
                                                if logo:
                                                    image = generateSmallThumbs(logo)
                                                    itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(secondUserId, reverse("profile", kwargs = {'currentUser': secondUser.profileUrl}), image, name)
                                                else:
                                                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(secondUserId, reverse("profile", kwargs = {'currentUser': secondUser.profileUrl}), name)
                                        else:
                                            itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(secondUserId, reverse("profile", kwargs = {'currentUser': secondUser.profileUrl}), name)
                                    else:
                                        itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(secondUserId, reverse("profile", kwargs = {'currentUser': secondUser.profileUrl}), image, name)
                                else:
                                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(secondUserId, reverse("profile", kwargs = {'currentUser': secondUser.profileUrl}), name)
                            else:
                                itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(secondUserId, reverse("profile", kwargs = {'currentUser': secondUser.profileUrl}), image.url, name)
                        else:
                            itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(secondUserId, reverse("profile", kwargs = {'currentUser': secondUser.profileUrl}), name)
                        thumb = {'item': itemString, 'showInOnlineList': viewOnlineStatus}
                        if viewOnlineStatus:
                            iffyCache = caches['iffyUsersOnlineStatus']
                            iffyUsers = iffyCache.get('iffy', set())
                            if id in iffyUsers:
                                iffyTime = iffyCache.get(secondUserId, None)
                                if isinstance(iffyTime, datetime):
                                    status = {'iffy': str(iffyTime)}
                                else:
                                    status = 'online'
                            else:
                                onlineUsers = caches['onlineUsers'].get('online', set())
                                if id in onlineUsers:
                                    status = 'online'
                                else:
                                    status = False
                        thumbBody = itemString
                    else:
                        thumb = False
                        thumbBody = False
                    thumbsDict.update({id: thumb})
                    thumbsCache.set(secondUserId, thumbsDict)
                else:
                    if thumb is not False:
                        if thumb['showInOnlineList']:
                            iffyCache = caches['iffyUsersOnlineStatus']
                            iffyUsers = iffyCache.get('iffy', set())
                            if id in iffyUsers:
                                iffyTime = iffyCache.get(secondUserId, None)
                                if isinstance(iffyTime, datetime):
                                    status = {'iffy': str(iffyTime)}
                                else:
                                    status = 'online'
                            else:
                                onlineUsers = caches['onlineUsers'].get('online', set())
                                if id in onlineUsers:
                                    status = 'online'
                                else:
                                    status = False
                        else:
                            status = False
                        thumbBody = thumb['item']
                    else:
                        thumbBody = False
                        status = False
            getData = request.GET.get('optns', None)
            if getData is not None:
                text = singleElementGetter(lang = 'ru')
                linksDict = {}
                textDict = {}
                allText = None
                slug = None
                if 'friendsulall' in getData:
                    if secondUser is None:
                        slug = mainUserProfile.objects.get(id__exact = secondUserId).profileUrl
                    else:
                        slug = secondUser.profileUrl
                    allText = text.getString('friendsAll')
                    linksDict.update({'friendsAll': reverse("friends", kwargs = {'user': slug, 'category': 'all', 'sep': '', 'page': ''})})
                    textDict.update({'all': allText})
                if 'friendsulonlinecreate' in getData:
                    if secondUser is None:
                        if slug is None:
                            slug = mainUserProfile.objects.get(id__exact = secondUserId).profileUrl
                    else:
                        if slug is None:
                            slug = secondUser.profileUrl
                    textDict.update({'friendsOnlineHead': text.getString('friendsOnline')})
                elif 'friendsulonlineall' in getData:
                    if secondUser is None:
                        if slug is None:
                            slug = mainUserProfile.objects.get(id__exact = secondUserId).profileUrl
                    else:
                        if slug is None:
                            slug = secondUser.profileUrl
                    if allText is None:
                        allText = text.getString('friendsAll')
                        textDict.update({'all': allText})
                    linksDict.update({'friendsOnline'})
                return JsonResponse({'thumb': thumbBody, 'status': status, 'dataDicts': {'links': linksDict, 'text': textDict}})
            return JsonResponse({'thumb': thumbBody, 'status': status})
        thumbsCache = caches['smallThumbs']
        thumbsDict = thumbsCache.get(id, None)
