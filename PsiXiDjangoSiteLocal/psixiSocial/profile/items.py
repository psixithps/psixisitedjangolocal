from os import path
from datetime import datetime
from django.urls import reverse
from django.conf import settings
from django.core.cache import caches
from psixiSocial.models import mainUserProfile, THPSProfile, userProfilePrivacy
from psixiSocial.privacyIdentifier import checkPrivacy

def generateSmallThumbs(image):
    imageFullName = image.name
    newImagePath = '%spsixiSocial/CACHE/users/50x50/%s' % (settings.MEDIA_URL, imageFullName)
    fullNewImagePath = '%s/%s' % (settings.BASE_DIR, newImagePath)
    if path.exists(fullNewImagePath):
        return newImagePath
    dotIndex = imageFullName.rfind(".") + 1
    name = imageFullName[0 : dotIndex]
    frmat = imageFullName[dotIndex : len(imageFullName)]
    if frmat == "jpg":
        frmat = "jpeg"
    class thumbResize(ImageSpec):
        processors = [ResizeTiFill(50, 50)]
        format = frmat
        options = {'quality': 60}
    with open(image.url, 'r') as originalImage:
        newImg = thumbResize(source = originalImage)
        newImgGenerated = newImg.generate()
    with open(newImagePath, 'wb') as thumb:
        thumb.write(newImgGenerated.read())
    return newImagePath

def getUserFriendsIm(requestUserId, friends, friendsIds, thumbsCache, onlineUsers, iffyCache, statusData):
    resultBody = []
    counter = 0
    iterableIds = set()
    for id in friendsIds:
        if counter == 20:
            break
        iterableIds.add(id)
        counter += 1
    thumbItemsToAdd = {}
    itemsDictMany = thumbsCache.get_many(iterableIds)
    itemsDictKeys = itemsDictMany.keys()
    itemsSurplusSet = iterableIds - itemsDictKeys
    itemsSurplusSet1 = set(id for id in itemsDictKeys if requestUserId not in itemsDictMany[id])
    idsToGetTHPSProfiles = itemsSurplusSet | itemsSurplusSet1
    if len(idsToGetTHPSProfiles) > 0:
        users = friends.filter(id__in = idsToGetTHPSProfiles)
        thpsProfiles = THPSProfile.objects.filter(toUser__in = users)
        if requestUserId in idsToGetTHPSProfiles:
            user = users.get(id__exact = requestUserId)
            firstName = user.first_name or None
            lastName = user.last_name or None
            if firstName and lastName:
                name = '{0} {1}'.format(firstName, lastName)
            else:
                if firstName:
                    name = firstName
                else:
                    name = user.username
            try:
                thpsProfile = thpsProfiles.get(toUser__exact = user)
            except:
                thpsProfile = None
            if thpsProfile is not None:
                nick = thpsProfile.nickName 
                clan = thpsProfile.clan or None
                if clan is not None:
                    name = '{0} [{1}*{2}]'.format(name, nick, clan)
                else:
                     name = '{0} [{1}]'.format(name, nick)
            thumb = user.avatarThumbSmall or None
            if thumb is None:
                if thpsProfile is not None:
                    if clan is not None:
                        background = clan.background
                        logo = clan.logo
                        if background and logo:
                            thumb = generateSmallThumbs(logo)
                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" /></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb)
                        else:
                            if logo:
                                thumb = generateSmallThumbs(logo)
                                itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                            else:
                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                    else:
                        itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                else:
                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
            else:
                itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb.url, name)
            item = {'item':itemString, 'showInOnlineList': True}
            thumbItemsToAdd.update({requestUserId: {requestUserId: item}})
            resultBody.append(itemString)
            statusData.update({requestUserId: 'online'})
            iterableIds.remove(requestUserId)
        privacyCache = caches['privacyCache']
        privacyMany = privacyCache.get_many(idsToGetTHPSProfiles)
        privacyManyKeys = privacyMany.keys()
        privacySurplusSet = idsToGetTHPSProfiles - privacyManyKeys
        privacySurplusSet1 = set()
        for id in privacyManyKeys:
            privacyItem = privacyMany[id]
            privacyItemMy = privacyItem.get(requestUserId, None)
            if privacyItemMy is None or 'viewProfile' or 'viewMainInfo' or 'viewTHPSInfo' or 'viewAvatar' or 'viewOnlineStatus' not in privacyItemMy:
                privacySurplusSet1.add(id)
        idsToGetPrivacyModel = privacySurplusSet | privacySurplusSet1
        if len(idsToGetPrivacyModel) > 0:
            users = friends.filter(id__in = idsToGetPrivacyModel)
            privacyInstances = userProfilePrivacy.objects.filter(user__in = set(users)) # ?
        privacyToAdd = {}
    iffyMany = iffyCache.get_many(iterableIds)
    for id in iterableIds:
        itemsDict = itemsDictMany.get(id, None)
        if itemsDict is None:
            user = None
            thumb = None
            privacyDict = privacyMany.get(id, None)
            if privacyDict is None:
                user = users.get(id = id)
                privacySettings = privacyInstances.get(user__exact = user)
                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                viewProfile = privacyChecker.checkViewProfilePrivacy()
                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                viewAvatar = privacyChecker.checkAvatarPrivacy()
                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                privacyToAdd.update({id: {requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}}})
            else:
                userPrivacyDict = privacyDict.get(requestUserId, None)
                if userPrivacyDict is None:
                    user = users.get(id = id)
                    privacySettings = privacyInstances.get(user__exact = user)
                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    privacyDict.update({requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                    privacyToAdd.update({id: privacyDict})
                else:
                    viewProfile = userPrivacyDict.get('viewProfile', None)
                    viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                    viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                    viewAvatar = userPrivacyDict.get('viewAvatar', None)
                    viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                    if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None:
                        user = users.get(id = id)
                        privacySettings = privacyInstances.get(user__exact = user)
                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                        if viewProfile is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewProfile': viewProfile})
                        if viewMainInfo is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                        if viewTHPSInfo is None:
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                        if viewAvatar is None:
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            userPrivacyDict.update({'viewAvatar': viewAvatar})
                        if viewOnlineStatus is None:
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                        privacyToAdd.update({id: privacyDict})
            if viewProfile:
                if user is None:
                    user = users.get(id__exact = id)
                if viewMainInfo:
                    firstName = user.first_name or None
                    lastName = user.last_name or None
                    if firstName and lastName:
                        name = '{0} {1}'.format(firstName, lastName)
                    else:
                        if firstName:
                            name = firstName
                        else:
                            name = user.username
                else:
                    name = user.username
                if viewTHPSInfo:
                    try:
                        thpsProfile = thpsProfiles.get(toUser__exact = user)
                    except:
                        thpsProfile = None
                    if thpsProfile is not None:
                        nick = thpsProfile.nickName 
                        clan = thpsProfile.clan or None
                        if clan is not None:
                            name = '{0} [{1}*{2}]'.format(name, nick, clan)
                        else:
                            name = '{0} [{1}]'.format(name, nick)
                if viewAvatar:
                    thumb = user.avatarThumbSmall or None
                    if thumb is None:
                        if viewTHPSInfo:
                            if thpsProfile is not None:
                                if clan is not None:
                                    background = clan.background
                                    logo = clan.logo
                                    if background and logo:
                                        thumb = generateSmallThumbs(logo)
                                        itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                    else:
                                        if logo:
                                            thumb = generateSmallThumbs(logo)
                                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                        else:
                                            itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                                else:
                                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                            else:
                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                        else:
                            itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                    else:
                        itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb.url, name)
                else:
                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                item = {'item': itemString, 'showInOnlineList': viewOnlineStatus}
                if viewOnlineStatus:
                    iffyTuple = iffyMany.get(id, None)
                    if iffyTuple is not None:
                        statusData.update({id: {'iffy': iffyTuple}})
                    elif id in onlineUsers:
                        statusData.update({id: 'online'})
            else:
                item = False
            thumbItemsToAdd.update({id: {requestUserId: item}})
        else:
            item = itemsDict.get(requestUserId, None)
            if item is None:
                user = None
                thumb = None
                privacyDict = privacyMany.get(id, None)
                if privacyDict is None:
                    user = users.get(id = id)
                    privacySettings = privacyInstances.get(user__exact = user)
                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    privacyToAdd.update({id: {requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}}})
                else:
                    userPrivacyDict = privacyDict.get(requestUserId, None)
                    if userPrivacyDict is None:
                        user = users.get(id = id)
                        privacySettings = privacyInstances.get(user__exact = user)
                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                        viewProfile = privacyChecker.checkViewProfilePrivacy()
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        privacyDict.update({requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                        privacyToAdd.update({id: privacyDict})
                    else:
                        viewProfile = userPrivacyDict.get('viewProfile', None)
                        viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                        viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                        viewAvatar = userPrivacyDict.get('viewAvatar', None)
                        viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                        if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None:
                            user = users.get(id = id)
                            privacySettings = privacyInstances.get(user__exact = user)
                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                            if viewProfile is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewProfile': viewProfile})
                            if viewMainInfo is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                            if viewTHPSInfo is None:
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                            if viewAvatar is None:
                                viewAvatar = privacyChecker.checkAvatarPrivacy()
                                userPrivacyDict.update({'viewAvatar': viewAvatar})
                            if viewOnlineStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                            privacyToAdd.update({id: privacyDict})
                if viewProfile:
                    if user is None:
                        user = users.get(id__exact = id)
                    if viewMainInfo:
                        firstName = user.first_name or None
                        lastName = user.last_name or None
                        if firstName and lastName:
                            name = '{0} {1}'.format(firstName, lastName)
                        else:
                            if firstName:
                                name = firstName
                            else:
                                name = user.username
                    else:
                        name = user.username
                    if viewTHPSInfo:
                        try:
                            thpsProfile = thpsProfiles.get(toUser__exact = user)
                        except:
                            thpsProfile = None
                        if thpsProfile is not None:
                            nick = thpsProfile.nickName 
                            clan = thpsProfile.clan or None
                            if clan is not None:
                                name = '{0} [{1}*{2}]'.format(name, nick, clan)
                            else:
                                name = '{0} [{1}]'.format(name, nick)
                    if viewAvatar:
                        thumb = user.avatarThumbSmall or None
                        if thumb is None:
                            if viewTHPSInfo:
                                if thpsProfile is not None:
                                    if clan is not None:
                                        background = clan.background
                                        logo = clan.logo
                                        if background and logo:
                                            thumb = generateSmallThumbs(logo)
                                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                        else:
                                            if logo:
                                                thumb = generateSmallThumbs(logo)
                                                itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                            else:
                                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                                    else:
                                        itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                                else:
                                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                            else:
                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                        else:
                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb.url, name)
                    else:
                        itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                    item = {'item': itemString, 'showInOnlineList': viewOnlineStatus}
                    if viewOnlineStatus:
                        iffyTuple = iffyMany.get(id, None)
                        if iffyTuple is not None:
                            statusData.update({id: {'iffy': iffyTuple}})
                        elif id in onlineUsers:
                            statusData.update({id: 'online'})
                else:
                    item = False
                itemsDict.update({requestUserId: item})
                thumbItemsToAdd.update({id: itemsDict})
            else:
                if item is not False:
                    if item['showInOnlineList']:
                        iffyTuple = iffyMany.get(id, None)
                        if iffyTuple is not None:
                            statusData.update({id: {'iffy': iffyTuple}})
                        elif id in onlineUsers:
                            statusData.update({id: 'online'})
                    resultBody.append(item['item'])
    if len(thumbItemsToAdd) > 0:
        thumbsCache.set_many(thumbItemsToAdd)
        if len(privacyToAdd) > 0:
            privacyCache.set_many(privacyToAdd)
    return resultBody, statusData

def getUserFriendsOnlineIm(requestUserId, friends, friendsIds, thumbsCache, onlineUsers, iffyCache, statusData):
    resultBody = []
    counter = 0
    iterableIdsDict = {}
    iffyMany = iffyCache.get_many(friendsIds)
    iffyUsers = set(iffyMany.keys())
    for id in friendsIds:
        if id in onlineUsers:
            if counter == 20:
                break
            iterableIdsDict.update({id: True})
            counter += 1
        elif id in iffyUsers:
            if counter == 20:
                break
            iterableIdsDict.update({id: False})
            counter += 1
    iterableIds = set(iterableIdsDict.keys())
    itemsDictMany = thumbsCache.get_many(iterableIds)
    itemsDictKeys = itemsDictMany.keys()
    itemsSurplusSet = iterableIds - itemsDictKeys
    itemsSurplusSet1 = set()
    for id in itemsDictKeys:
        myItem = itemsDictMany[id].get(requestUserId, None)
        if myItem is False:
            del iterableIdsDict[id]
            iterableIds.remove(id)
        else:
            if myItem is None:
                itemsSurplusSet1.add(id)
    idsToGetTHPSProfiles = itemsSurplusSet | itemsSurplusSet1
    if len(idsToGetTHPSProfiles) > 0:
        users = friends.filter(id__in = idsToGetTHPSProfiles)
        thpsProfiles = THPSProfile.objects.filter(toUser__in = users)
        if requestUserId in idsToGetTHPSProfiles:
            user = users.get(id__exact = requestUserId)
            firstName = user.first_name or None
            lastName = user.last_name or None
            if firstName and lastName:
                name = '{0} {1}'.format(firstName, lastName)
            else:
                if firstName:
                    name = firstName
                else:
                    name = user.username
            try:
                thpsProfile = thpsProfiles.get(toUser__exact = user)
            except:
                thpsProfile = None
            if thpsProfile is not None:
                nick = thpsProfile.nickName 
                clan = thpsProfile.clan or None
                if clan is not None:
                    name = '{0} [{1}*{2}]'.format(name, nick, clan)
                else:
                     name = '{0} [{1}]'.format(name, nick)
            if thumb is None:
                if thpsProfile is not None:
                    if clan is not None:
                        background = clan.background
                        logo = clan.logo
                        if background and logo:
                            thumb = generateSmallThumbs(logo)
                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" /></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb)
                        else:
                            if logo:
                                thumb = generateSmallThumbs(logo)
                                itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                            else:
                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                    else:
                        itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                else:
                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
            else:
                itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb.url, name)
            item = {'item':itemString, 'showInOnlineList': True}
            thumbItemsToAdd.update({requestUserId: {requestUserId: item}})
            resultBody.append(itemString)
            statusData.update({requestUserId: 'online'})
            idsToGetTHPSProfiles.remove(requestUserId)
            del iterableIdsDict[requestUserId]
        privacyCache = caches['privacyCache']
        privacyMany = privacyCache.get_many(idsToGetTHPSProfiles)
        privacyManyKeys = privacyMany.keys()
        privacySurplusSet = idsToGetTHPSProfiles - privacyManyKeys
        privacySurplusSet1 = set()
        for id in privacyManyKeys:
            privacyItem = privacyMany[id]
            privacyItemMy = privacyItem.get(requestUserId, None)
            if privacyItemMy is None or 'viewProfile' or 'viewMainInfo' or 'viewTHPSInfo' or 'viewAvatar' or 'viewOnlineStatus' not in privacyItemMy:
                privacySurplusSet1.add(id)
        idsToGetPrivacyModel = privacySurplusSet | privacySurplusSet1
        if len(idsToGetPrivacyModel) > 0:
            users = friends.filter(id__in = idsToGetPrivacyModel)
            privacyInstances = userProfilePrivacy.objects.filter(user__in = users)
        privacyToAdd = {}
    thumbItemsToAdd = {}
    for id, onlineStatus in iterableIdsDict.items():
        itemsDict = itemsDictMany.get(id, None)
        if itemsDict is None:
            user = None
            thumb = None
            privacyDict = privacyMany.get(id, None)
            if privacyDict is None:
                user = users.get(id = id)
                privacySettings = privacyInstances.get(user__exact = user)
                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                viewProfile = privacyChecker.checkViewProfilePrivacy()
                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                viewAvatar = privacyChecker.checkAvatarPrivacy()
                privacyToAdd.update({id: {requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}}})
            else:
                userPrivacyDict = privacyDict.get(requestUserId, None)
                if userPrivacyDict is None:
                    user = users.get(id = id)
                    privacySettings = privacyInstances.get(user__exact = user)
                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    privacyDict.update({requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                    privacyToAdd.update({id: privacyDict})
                else:
                    viewProfile = userPrivacyDict.get('viewProfile', None)
                    viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                    viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                    viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                    viewAvatar = userPrivacyDict.get('viewAvatar', None)
                    if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewOnlineStatus is None or viewAvatar is None:
                        user = users.get(id = id)
                        privacySettings = privacyInstances.get(user__exact = user)
                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                        if viewProfile is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewProfile': viewProfile})
                        if viewMainInfo is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                        if viewTHPSInfo is None:
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                        if viewOnlineStatus is None:
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                        if viewAvatar is None:
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            userPrivacyDict.update({'viewAvatar': viewAvatar})
                        privacyToAdd.update({id: privacyDict})
            if viewProfile and viewOnlineStatus:
                if user is None:
                    user = users.get(id__exact = id)
                if viewMainInfo:
                    firstName = user.first_name or None
                    lastName = user.last_name or None
                    if firstName and lastName:
                        name = '{0} {1}'.format(firstName, lastName)
                    else:
                        if firstName:
                            name = firstName
                        else:
                            name = user.username
                else:
                    name = user.username
                if viewTHPSInfo:
                    try:
                        thpsProfile = thpsProfiles.get(toUser__exact = user)
                    except:
                        thpsProfile = None
                    if thpsProfile is not None:
                        nick = thpsProfile.nickName 
                        clan = thpsProfile.clan or None
                        if clan is not None:
                            name = '{0} [{1}*{2}]'.format(name, nick, clan)
                        else:
                            name = '{0} [{1}]'.format(name, nick)
                if viewAvatar:
                    thumb = user.avatarThumbSmall or None
                    if thumb is None:
                        if viewTHPSInfo:
                            if thpsProfile is not None:
                                if clan is not None:
                                    background = clan.background
                                    logo = clan.logo
                                    if background and logo:
                                        thumb = generateSmallThumbs(logo)
                                        itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                    else:
                                        if logo:
                                            thumb = generateSmallThumbs(logo)
                                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                        else:
                                            itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                                else:
                                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                            else:
                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                        else:
                            itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                    else:
                        itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb.url, name)
                else:
                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                item = {'item': itemString, 'showInOnlineList': viewOnlineStatus}
                iffyTuple = iffyMany.get(id, None)
                if iffyTuple is not None:
                    statusData.update({id: {'iffy': iffyTuple}})
                elif id in onlineUsers:
                    statusData.update({id: 'online'})
            else:
                item = False
            thumbItemsToAdd.update({id: {requestUserId: item}})
        else:
            item = itemsDict.get(requestUserId, None)
            if item is None:
                user = None
                thumb = None
                privacyDict = privacyMany.get(id, None)
                if privacyDict is None:
                    user = users.get(id = id)
                    privacySettings = privacyInstances.get(user__exact = user)
                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    privacyToAdd.update({id: {requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}}})
                else:
                    userPrivacyDict = privacyDict.get(requestUserId, None)
                    if userPrivacyDict is None:
                        user = users.get(id = id)
                        privacySettings = privacyInstances.get(user__exact = user)
                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                        viewProfile = privacyChecker.checkViewProfilePrivacy()
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        privacyDict.update({requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                        privacyToAdd.update({id: privacyDict})
                    else:
                        viewProfile = userPrivacyDict.get('viewProfile', None)
                        viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                        viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                        viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                        viewAvatar = userPrivacyDict.get('viewAvatar', None)
                        if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewOnlineStatus is None or viewAvatar is None:
                            user = users.get(id = id)
                            privacySettings = privacyInstances.get(user__exact = user)
                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                            if viewProfile is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewProfile': viewProfile})
                            if viewMainInfo is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                            if viewTHPSInfo is None:
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                            if viewOnlineStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                            if viewAvatar is None:
                                viewAvatar = privacyChecker.checkAvatarPrivacy()
                                userPrivacyDict.update({'viewAvatar': viewAvatar})
                            privacyToAdd.update({id: privacyDict})
                if viewProfile and viewOnlineStatus:
                    if user is None:
                        user = users.get(id__exact = id)
                    if viewMainInfo:
                        firstName = user.first_name or None
                        lastName = user.last_name or None
                        if firstName and lastName:
                            name = '{0} {1}'.format(firstName, lastName)
                        else:
                            if firstName:
                                name = firstName
                            else:
                                name = user.username
                    else:
                        name = user.username
                    if viewTHPSInfo:
                        try:
                            thpsProfile = thpsProfiles.get(toUser__exact = user)
                        except:
                            thpsProfile = None
                        if thpsProfile is not None:
                            nick = thpsProfile.nickName 
                            clan = thpsProfile.clan or None
                            if clan is not None:
                                name = '{0} [{1}*{2}]'.format(name, nick, clan)
                            else:
                                name = '{0} [{1}]'.format(name, nick)
                    if viewAvatar:
                        thumb = user.avatarThumbSmall or None
                        if thumb is None:
                            if viewTHPSInfo:
                                if thpsProfile is not None:
                                    if clan is not None:
                                        background = clan.background
                                        logo = clan.logo
                                        if background and logo:
                                            thumb = generateSmallThumbs(logo)
                                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                        else:
                                            if logo:
                                                thumb = generateSmallThumbs(logo)
                                                itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                            else:
                                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                                    else:
                                        itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                                else:
                                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                            else:
                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                        else:
                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb.url, name)
                    else:
                        itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                    item = {'item': itemString, 'showInOnlineList': viewOnlineStatus}
                    if viewOnlineStatus:
                        iffyTuple = iffyMany.get(id, None)
                        if iffyTuple is not None:
                            statusData.update({id: {'iffy': iffyTuple}})
                        elif id in onlineUsers:
                            statusData.update({id: 'online'})
                else:
                    item = False
                itemsDict.update({requestUserId: item})
                thumbItemsToAdd.update({id: itemsDict})
            else:
                if item is not False:
                    if item['showInOnlineList']:
                        iffyTuple = iffyMany.get(id, None)
                        if iffyTuple is not None:
                            statusData.update({id: {'iffy': iffyTuple}})
                        elif id in onlineUsers:
                            statusData.update({id: 'online'})
                    resultBody.append(item['item'])
    if len(thumbItemsToAdd) > 0:
        thumbsCache.set_many(thumbItemsToAdd)
        if len(privacyToAdd) > 0:
            privacyCache.set_many(privacyToAdd)
    return resultBody, statusData

def getUserFriends(requestUserId, friends, friendsIds, friendsCache, friendsIdsCache, friendShipStatusCache, friendRequestsCache, thumbsCache, privacyCache, onlineUsers, iffyCache, statusData):
    resultBody = []
    counter = 0
    iterableIds = set()
    for id in friendsIds:
        if counter == 20:
            break
        iterableIds.add(id)
        counter += 1
    thumbItemsToAdd = {}
    itemsDictMany = thumbsCache.get_many(iterableIds)
    itemsDictKeys = itemsDictMany.keys()
    itemsSurplusSet = iterableIds - itemsDictKeys
    itemsSurplusSet1 = set()
    for id in itemsDictKeys:
        myItem = itemsDictMany[id].get(requestUserId, None)
        if myItem is None:
            itemsSurplusSet1.add(id)
        elif myItem is False:
            iterableIds.remove(id)
        else:
            if id == requestUserId:
                statusData.update({requestUserId: 'online'})
                resultBody.append(itemsDictMany[id][id]['item'])
                iterableIds.remove(id)
    idsToGetTHPSProfiles = itemsSurplusSet | itemsSurplusSet1
    if len(idsToGetTHPSProfiles) > 0:
        users = friends.filter(id__in = idsToGetTHPSProfiles)
        thpsProfiles = THPSProfile.objects.filter(toUser__in = users)
        if requestUserId in idsToGetTHPSProfiles:
            user = users.get(id__exact = requestUserId)
            firstName = user.first_name or None
            lastName = user.last_name or None
            if firstName and lastName:
                name = '{0} {1}'.format(firstName, lastName)
            else:
                if firstName:
                    name = firstName
                else:
                    name = user.username
            try:
                thpsProfile = thpsProfiles.get(toUser__exact = user)
            except:
                thpsProfile = None
            if thpsProfile is not None:
                nick = thpsProfile.nickName 
                clan = thpsProfile.clan or None
                if clan is not None:
                    name = '{0} [{1}*{2}]'.format(name, nick, clan)
                else:
                     name = '{0} [{1}]'.format(name, nick)
            thumb = user.avatarThumbSmall or None
            if thumb is None:
                if thpsProfile is not None:
                    if clan is not None:
                        background = clan.background
                        logo = clan.logo
                        if background and logo:
                            thumb = generateSmallThumbs(logo)
                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" /></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb)
                        else:
                            if logo:
                                thumb = generateSmallThumbs(logo)
                                itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                            else:
                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                    else:
                        itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                else:
                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
            else:
                itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb.url, name)
            item = {'item':itemString, 'showInOnlineList': True}
            thumbItemsToAdd.update({requestUserId: {requestUserId: item}})
            resultBody.append(itemString)
            statusData.update({requestUserId: 'online'})
            iterableIds.remove(requestUserId)
        privacyMany = privacyCache.get_many(idsToGetTHPSProfiles)
        privacyManyKeys = privacyMany.keys()
        privacySurplusSet = idsToGetTHPSProfiles - privacyManyKeys
        privacySurplusSet1 = set()
        for id in privacyManyKeys:
            privacyItem = privacyMany[id]
            privacyItemMy = privacyItem.get(requestUserId, None)
            if privacyItemMy is None or 'viewProfile' or 'viewMainInfo' or 'viewTHPSInfo' or 'viewAvatar' or 'viewOnlineStatus' not in privacyItemMy:
                privacySurplusSet1.add(id)
        idsToGetPrivacyModel = privacySurplusSet | privacySurplusSet1
        if len(idsToGetPrivacyModel) > 0:
            users = friends.filter(id__in = idsToGetPrivacyModel)
            privacyInstances = userProfilePrivacy.objects.filter(user__in = users)
            friendShipSurplus = idsToGetPrivacyModel - friendsIds
            friendShipStatusMany = friendShipStatusCache.get_many(friendShipSurplus)
            friendShipRequestsMany = friendRequestsCache.get_many(friendShipSurplus)
            friendShipStatusToAdd = {}
        privacyToAdd = {}
    iffyMany = iffyCache.get_many(iterableIds)
    for id in iterableIds:
        itemsDict = itemsDictMany.get(id, None)
        if itemsDict is None:
            user = None
            thumb = None
            privacyDict = privacyMany.get(id, None)
            if privacyDict is None:
                user = users.get(id__exact = id)
                privacySettings = privacyInstances.get(user__exact = user)
                friendShipDict = friendShipStatusMany.get(id, None)
                if friendShipDict is None:
                    if id in friendsIds:
                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                        friendShipStatusToAdd.update({id: {requestUserId: True}})
                    else:
                        requests = friendShipRequestsMany.get(requestUserId, set())
                        find = False
                        for request in requests:
                            if request[0] == id:
                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                friendShipStatusToAdd.update({id: {requestUserId: "request"}})
                                find = True
                                break
                        if find is False:
                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                            friendShipStatusToAdd.update({id: {requestUserId: False}})
                else:
                    friendShipStatus = friendShipDict.get(requestUserId, None)
                    if friendShipStatus is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                            friendShipDict.update({requestUserId: True})
                        else:
                            requests = friendShipRequestsMany.get(requestUserId, set())
                            find = False
                            for request in requests:
                                if request[0] == id:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                    friendShipDict.update({requestUserId: "request"})
                                    find = True
                                    break
                            if find is False:
                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                friendShipDict.update({requestUserId: False})
                        friendShipStatusToAdd.update({id: friendShipDict})
                    else:
                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                viewProfile = privacyChecker.checkViewProfilePrivacy()
                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                viewAvatar = privacyChecker.checkAvatarPrivacy()
                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                privacyToAdd.update({id: {requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}}})
            else:
                userPrivacyDict = privacyDict.get(requestUserId, None)
                if userPrivacyDict is None:
                    user = users.get(id__exact = id)
                    privacySettings = privacyInstances.get(user__exact = user)
                    friendShipDict = friendShipStatusMany.get(id, None)
                    if friendShipDict is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                            friendShipStatusToAdd.update({id: {requestUserId: True}})
                        else:
                            requests = friendShipRequestsMany.get(requestUserId, set())
                            find = False
                            for request in requests:
                                if request[0] == id:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                    friendShipStatusToAdd.update({id: {requestUserId: "request"}})
                                    find = True
                                    break
                            if find is False:
                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                friendShipStatusToAdd.update({id: {requestUserId: False}})
                    else:
                        friendShipStatus = friendShipDict.get(requestUserId, None)
                        if friendShipStatus is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                friendShipDict.update({requestUserId: True})
                            else:
                                requests = friendShipRequestsMany.get(requestUserId, set())
                                find = False
                                for request in requests:
                                    if request[0] == id:
                                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                        friendShipDict.update({requestUserId: "request"})
                                        find = True
                                        break
                                if find is False:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                    friendShipDict.update({requestUserId: False})
                            friendShipStatusToAdd.update({id: friendShipDict})
                        else:
                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    privacyDict.update({requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                    privacyToAdd.update({id: privacyDict})
                else:
                    viewProfile = userPrivacyDict.get('viewProfile', None)
                    viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                    viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                    viewAvatar = userPrivacyDict.get('viewAvatar', None)
                    viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                    if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None:
                        user = users.get(id__exact = id)
                        privacySettings = privacyInstances.get(user__exact = user)
                        friendShipDict = friendShipStatusMany.get(id, None)
                        if friendShipDict is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                friendShipStatusToAdd.update({id: {requestUserId: True}})
                            else:
                                requests = friendShipRequestsMany.get(requestUserId, set())
                                find = False
                                for request in requests:
                                    if request[0] == id:
                                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                        friendShipStatusToAdd.update({id: {requestUserId: "request"}})
                                        find = True
                                        break
                                if find is False:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                    friendShipStatusToAdd.update({id: {requestUserId: False}})
                        else:
                            friendShipStatus = friendShipDict.get(requestUserId, None)
                            if friendShipStatus is None:
                                if id in friendsIds:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                    friendShipDict.update({requestUserId: True})
                                else:
                                    requests = friendShipRequestsMany.get(requestUserId, set())
                                    find = False
                                    for request in requests:
                                        if request[0] == id:
                                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                            friendShipDict.update({requestUserId: "request"})
                                            find = True
                                            break
                                    if find is False:
                                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                        friendShipDict.update({requestUserId: False})
                                friendShipStatusToAdd.update({id: friendShipDict})
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                        if viewProfile is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewProfile': viewProfile})
                        if viewMainInfo is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                        if viewTHPSInfo is None:
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                        if viewAvatar is None:
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            userPrivacyDict.update({'viewAvatar': viewAvatar})
                        if viewOnlineStatus is None:
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                        privacyToAdd.update({id: privacyDict})
            if viewProfile:
                if user is None:
                    user = users.get(id__exact = id)
                if viewMainInfo:
                    firstName = user.first_name or None
                    lastName = user.last_name or None
                    if firstName and lastName:
                        name = '{0} {1}'.format(firstName, lastName)
                    else:
                        if firstName:
                            name = firstName
                        else:
                            name = user.username
                else:
                    name = user.username
                if viewTHPSInfo:
                    try:
                        thpsProfile = thpsProfiles.get(toUser__exact = user)
                    except:
                        thpsProfile = None
                    if thpsProfile is not None:
                        nick = thpsProfile.nickName 
                        clan = thpsProfile.clan or None
                        if clan is not None:
                            name = '{0} [{1}*{2}]'.format(name, nick, clan)
                        else:
                            name = '{0} [{1}]'.format(name, nick)
                if viewAvatar:
                    thumb = user.avatarThumbSmall or None
                    if thumb is None:
                        if viewTHPSInfo:
                            if thpsProfile is not None:
                                if clan is not None:
                                    background = clan.background
                                    logo = clan.logo
                                    if background and logo:
                                        thumb = generateSmallThumbs(logo)
                                        itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                    else:
                                        if logo:
                                            thumb = generateSmallThumbs(logo)
                                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                        else:
                                            itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                                else:
                                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                            else:
                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                        else:
                            itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                    else:
                        itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb.url, name)
                else:
                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                item = {'item': itemString, 'showInOnlineList': viewOnlineStatus}
                if viewOnlineStatus:
                    iffyTuple = iffyMany.get(id, None)
                    if iffyTuple is not None:
                        statusData.update({id: {'iffy': iffyTuple}})
                    elif id in onlineUsers:
                        statusData.update({id: 'online'})
            else:
                item = False
            thumbItemsToAdd.update({id: {requestUserId: item}})
        else:
            item = itemsDict.get(requestUserId, None)
            if item is None:
                user = None
                thumb = None
                privacyDict = privacyMany.get(id, None)
                if privacyDict is None:
                    user = users.get(id__exact = id)
                    privacySettings = privacyInstances.get(user__exact = user)
                    friendShipDict = friendShipStatusMany.get(id, None)
                    if friendShipDict is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                            friendShipStatusToAdd.update({id: {requestUserId: True}})
                        else:
                            requests = friendShipRequestsMany.get(requestUserId, set())
                            find = False
                            for request in requests:
                                if request[0] == id:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                    friendShipStatusToAdd.update({id: {requestUserId: "request"}})
                                    find = True
                                    break
                            if find is False:
                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                friendShipStatusToAdd.update({id: {requestUserId: False}})
                    else:
                        friendShipStatus = friendShipDict.get(requestUserId, None)
                        if friendShipStatus is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                friendShipDict.update({requestUserId: True})
                            else:
                                requests = friendShipRequestsMany.get(requestUserId, set())
                                find = False
                                for request in requests:
                                    if request[0] == id:
                                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                        friendShipDict.update({requestUserId: "request"})
                                        find = True
                                        break
                                if find is False:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                    friendShipDict.update({requestUserId: False})
                            friendShipStatusToAdd.update({id: friendShipDict})
                        else:
                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    privacyToAdd.update({id: {requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}}})
                else:
                    userPrivacyDict = privacyDict.get(requestUserId, None)
                    if userPrivacyDict is None:
                        user = users.get(id__exact = id)
                        privacySettings = privacyInstances.get(user__exact = user)
                        friendShipDict = friendShipStatusMany.get(id, None)
                        if friendShipDict is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                friendShipStatusToAdd.update({id: {requestUserId: True}})
                            else:
                                requests = friendShipRequestsMany.get(requestUserId, set())
                                find = False
                                for request in requests:
                                    if request[0] == id:
                                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                        friendShipStatusToAdd.update({id: {requestUserId: "request"}})
                                        find = True
                                        break
                                if find is False:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                    friendShipStatusToAdd.update({id: {requestUserId: False}})
                        else:
                            friendShipStatus = friendShipDict.get(requestUserId, None)
                            if friendShipStatus is None:
                                if id in friendsIds:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                    friendShipDict.update({requestUserId: True})
                                else:
                                    requests = friendShipRequestsMany.get(requestUserId, set())
                                    find = False
                                    for request in requests:
                                        if request[0] == id:
                                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                            friendShipDict.update({requestUserId: "request"})
                                            find = True
                                            break
                                    if find is False:
                                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                        friendShipDict.update({requestUserId: False})
                                friendShipStatusToAdd.update({id: friendShipDict})
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                        viewProfile = privacyChecker.checkViewProfilePrivacy()
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        privacyDict.update({requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                        privacyToAdd.update({id: privacyDict})
                    else:
                        viewProfile = userPrivacyDict.get('viewProfile', None)
                        viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                        viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                        viewAvatar = userPrivacyDict.get('viewAvatar', None)
                        viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                        if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None:
                            user = users.get(id__exact = id)
                            privacySettings = privacyInstances.get(user__exact = user)
                            friendShipDict = friendShipStatusMany.get(id, None)
                            if friendShipDict is None:
                                if id in friendsIds:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                    friendShipStatusToAdd.update({id: {requestUserId: True}})
                                else:
                                    requests = friendShipRequestsMany.get(requestUserId, set())
                                    find = False
                                    for request in requests:
                                        if request[0] == id:
                                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                            friendShipStatusToAdd.update({id: {requestUserId: "request"}})
                                            find = True
                                            break
                                    if find is False:
                                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                        friendShipStatusToAdd.update({id: {requestUserId: False}})
                            else:
                                friendShipStatus = friendShipDict.get(requestUserId, None)
                                if friendShipStatus is None:
                                    if id in friendsIds:
                                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                        friendShipDict.update({requestUserId: True})
                                    else:
                                        requests = friendShipRequestsMany.get(requestUserId, set())
                                        find = False
                                        for request in requests:
                                            if request[0] == id:
                                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                                friendShipDict.update({requestUserId: "request"})
                                                find = True
                                                break
                                        if find is False:
                                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                            friendShipDict.update({requestUserId: False})
                                    friendShipStatusToAdd.update({id: friendShipDict})
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                            if viewProfile is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewProfile': viewProfile})
                            if viewMainInfo is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                            if viewTHPSInfo is None:
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                            if viewAvatar is None:
                                viewAvatar = privacyChecker.checkAvatarPrivacy()
                                userPrivacyDict.update({'viewAvatar': viewAvatar})
                            if viewOnlineStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                            privacyToAdd.update({id: privacyDict})
                if viewProfile:
                    if user is None:
                        user = users.get(id__exact = id)
                    if viewMainInfo:
                        firstName = user.first_name or None
                        lastName = user.last_name or None
                        if firstName and lastName:
                            name = '{0} {1}'.format(firstName, lastName)
                        else:
                            if firstName:
                                name = firstName
                            else:
                                name = user.username
                    else:
                        name = user.username
                    if viewTHPSInfo:
                        try:
                            thpsProfile = thpsProfiles.get(toUser__exact = user)
                        except:
                            thpsProfile = None
                        if thpsProfile is not None:
                            nick = thpsProfile.nickName 
                            clan = thpsProfile.clan or None
                            if clan is not None:
                                name = '{0} [{1}*{2}]'.format(name, nick, clan)
                            else:
                                name = '{0} [{1}]'.format(name, nick)
                    if viewAvatar:
                        thumb = user.avatarThumbSmall or None
                        if thumb is None:
                            if viewTHPSInfo:
                                if thpsProfile is not None:
                                    if clan is not None:
                                        background = clan.background
                                        logo = clan.logo
                                        if background and logo:
                                            thumb = generateSmallThumbs(logo)
                                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                        else:
                                            if logo:
                                                thumb = generateSmallThumbs(logo)
                                                itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                            else:
                                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                                    else:
                                        itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                                else:
                                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                            else:
                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                        else:
                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb.url, name)
                    else:
                        itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                    item = {'item': itemString, 'showInOnlineList': viewOnlineStatus}
                    if viewOnlineStatus:
                        iffyTuple = iffyMany.get(id, None)
                        if iffyTuple is not None:
                            statusData.update({id: {'iffy': iffyTuple}})
                        elif id in onlineUsers:
                            statusData.update({id: 'online'})
                else:
                    item = False
                itemsDict.update({requestUserId: item})
                thumbItemsToAdd.update({id: itemsDict})
            else:
                if item is not False:
                    if item['showInOnlineList']:
                        iffyTuple = iffyMany.get(id, None)
                        if iffyTuple is not None:
                            statusData.update({id: {'iffy': iffyTuple}})
                        elif id in onlineUsers:
                            statusData.update({id: 'online'})
                    resultBody.append(item['item'])
    if len(thumbItemsToAdd) > 0:
        thumbsCache.set_many(thumbItemsToAdd)
        if len(friendShipStatusToAdd) > 0:
            friendShipStatusCache.set_many(friendShipStatusToAdd)
        if len(privacyToAdd) > 0:
            privacyCache.set_many(privacyToAdd)
    return resultBody, statusData

def getUserFriendsGuest(friends, friendsIds, thumbsCache, privacyCache, onlineUsers, iffyCache, statusData):
    resultBody = []
    counter = 0
    iterableIds = set()
    for id in friendsIds:
        if counter == 20:
            break
        iterableIds.add(id)
        counter += 1
    itemsDictMany = thumbsCache.get_many(iterableIds)
    itemsDictKeys = itemsDictMany.keys()
    itemsSurplusSet = iterableIds - itemsDictKeys
    itemsSurplusSet1 = set(id for id in itemsDictKeys if 0 not in itemsDictMany[id])
    idsToGetTHPSProfiles = itemsSurplusSet | itemsSurplusSet1
    if len(idsToGetTHPSProfiles) > 0:
        users = friends.filter(id__in = idsToGetTHPSProfiles)
        thpsProfiles = THPSProfile.objects.filter(toUser__in = users)
        privacyMany = privacyCache.get_many(iterableIds)
        privacyManyKeys = privacyMany.keys()
        privacySurplusSet = iterableIds - privacyManyKeys
        privacySurplusSet1 = set()
        for id in privacyManyKeys:
            privacyItem = privacyMany[id]
            privacyItemMy = privacyItem.get(0, None)
            if privacyItemMy is None or 'viewProfile' or 'viewMainInfo' or 'viewTHPSInfo' or 'viewAvatar' or 'viewOnlineStatus' not in privacyItemMy:
                privacySurplusSet1.add(id)
        idsToGetPrivacyModel = privacySurplusSet | privacySurplusSet1
        if len(idsToGetPrivacyModel) > 0:
            users = friends.filter(id__in = idsToGetPrivacyModel)
            privacyInstances = userProfilePrivacy.objects.filter(user__in = users)
        privacyToAdd = {}
    thumbItemsToAdd = {}
    iffyMany = iffyCache.get_many(iterableIds)
    for id in iterableIds:
        itemsDict = itemsDictMany.get(id, None)
        if itemsDict is None:
            user = None
            thumb = None
            privacyDict = privacyMany.get(id, None)
            if privacyDict is None:
                user = users.get(id = id)
                privacySettings = privacyInstances.get(user__exact = user)
                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = False)
                viewProfile = privacyChecker.checkViewProfilePrivacy()
                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                viewAvatar = privacyChecker.checkAvatarPrivacy()
                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                privacyToAdd.update({id: {0: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}}})
            else:
                userPrivacyDict = privacyDict.get(0, None)
                if userPrivacyDict is None:
                    user = users.get(id = id)
                    privacySettings = privacyInstances.get(user__exact = user)
                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = False)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    privacyDict.update({0: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                    privacyToAdd.update({id: privacyDict})
                else:
                    viewProfile = userPrivacyDict.get('viewProfile', None)
                    viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                    viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                    viewAvatar = userPrivacyDict.get('viewAvatar', None)
                    viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                    if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None:
                        user = users.get(id = id)
                        privacySettings = privacyInstances.get(user__exact = user)
                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = False)
                        if viewProfile is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewProfile': viewProfile})
                        if viewMainInfo is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                        if viewTHPSInfo is None:
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                        if viewAvatar is None:
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            userPrivacyDict.update({'viewAvatar': viewAvatar})
                        if viewOnlineStatus is None:
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                        privacyToAdd.update({id: privacyDict})
            if viewProfile:
                if user is None:
                    user = users.get(id__exact = id)
                if viewMainInfo:
                    firstName = user.first_name or None
                    lastName = user.last_name or None
                    if firstName and lastName:
                        name = '{0} {1}'.format(firstName, lastName)
                    else:
                        if firstName:
                            name = firstName
                        else:
                            name = user.username
                else:
                    name = user.username
                if viewTHPSInfo:
                    try:
                        thpsProfile = thpsProfiles.get(toUser__exact = user)
                    except:
                        thpsProfile = None
                    if thpsProfile is not None:
                        nick = thpsProfile.nickName 
                        clan = thpsProfile.clan or None
                        if clan is not None:
                            name = '{0} [{1}*{2}]'.format(name, nick, clan)
                        else:
                            name = '{0} [{1}]'.format(name, nick)
                if viewAvatar:
                    thumb = user.avatarThumbSmall or None
                    if thumb is None:
                        if viewTHPSInfo:
                            if thpsProfile is not None:
                                if clan is not None:
                                    background = clan.background
                                    logo = clan.logo
                                    if background and logo:
                                        thumb = generateSmallThumbs(logo)
                                        itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                    else:
                                        if logo:
                                            thumb = generateSmallThumbs(logo)
                                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                        else:
                                            itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                                else:
                                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                            else:
                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                        else:
                            itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                    else:
                        itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb.url, name)
                else:
                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                item = {'item': itemString, 'showInOnlineList': viewOnlineStatus}
                if viewOnlineStatus:
                    iffyTuple = iffyMany.get(id, None)
                    if iffyTuple is not None:
                        statusData.update({id: {'iffy': iffyTuple}})
                    elif id in onlineUsers:
                        statusData.update({id: 'online'})
            else:
                item = False
            thumbItemsToAdd.update({id: {0: item}})
        else:
            item = itemsDict.get(0, None)
            if item is None:
                user = None
                thumb = None
                privacyDict = privacyMany.get(id, None)
                if privacyDict is None:
                    user = users.get(id = id)
                    privacySettings = privacyInstances.get(user__exact = user)
                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = False)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    privacyToAdd.update({id: {0: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}}})
                else:
                    userPrivacyDict = privacyDict.get(0, None)
                    if userPrivacyDict is None:
                        user = users.get(id = id)
                        privacySettings = privacyInstances.get(user__exact = user)
                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = False)
                        viewProfile = privacyChecker.checkViewProfilePrivacy()
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        privacyDict.update({0: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                        privacyToAdd.update({id: privacyDict})
                    else:
                        viewProfile = userPrivacyDict.get('viewProfile', None)
                        viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                        viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                        viewAvatar = userPrivacyDict.get('viewAvatar', None)
                        viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                        if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None:
                            user = users.get(id = id)
                            privacySettings = privacyInstances.get(user__exact = user)
                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = False)
                            if viewProfile is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewProfile': viewProfile})
                            if viewMainInfo is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                            if viewTHPSInfo is None:
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                            if viewAvatar is None:
                                viewAvatar = privacyChecker.checkAvatarPrivacy()
                                userPrivacyDict.update({'viewAvatar': viewAvatar})
                            if viewOnlineStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                            privacyToAdd.update({id: privacyDict})
                if viewProfile:
                    if user is None:
                        user = users.get(id__exact = id)
                    if viewMainInfo:
                        firstName = user.first_name or None
                        lastName = user.last_name or None
                        if firstName and lastName:
                            name = '{0} {1}'.format(firstName, lastName)
                        else:
                            if firstName:
                                name = firstName
                            else:
                                name = user.username
                    else:
                        name = user.username
                    if viewTHPSInfo:
                        try:
                            thpsProfile = thpsProfiles.get(toUser__exact = user)
                        except:
                            thpsProfile = None
                        if thpsProfile is not None:
                            nick = thpsProfile.nickName 
                            clan = thpsProfile.clan or None
                            if clan is not None:
                                name = '{0} [{1}*{2}]'.format(name, nick, clan)
                            else:
                                name = '{0} [{1}]'.format(name, nick)
                    if viewAvatar:
                        thumb = user.avatarThumbSmall or None
                        if thumb is None:
                            if viewTHPSInfo:
                                if thpsProfile is not None:
                                    if clan is not None:
                                        background = clan.background
                                        logo = clan.logo
                                        if background and logo:
                                            thumb = generateSmallThumbs(logo)
                                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                        else:
                                            if logo:
                                                thumb = generateSmallThumbs(logo)
                                                itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                            else:
                                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                                    else:
                                        itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                                else:
                                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                            else:
                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                        else:
                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb.url, name)
                    else:
                        itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                    item = {'item': itemString, 'showInOnlineList': viewOnlineStatus}
                    if viewOnlineStatus:
                        iffyTuple = iffyMany.get(id, None)
                        if iffyTuple is not None:
                            statusData.update({id: {'iffy': iffyTuple}})
                        elif id in onlineUsers:
                            statusData.update({id: 'online'})
                else:
                    item = False
                itemsDict.update({0: item})
                thumbItemsToAdd.update({id: itemsDict})
            else:
                if item is not False:
                    if item['showInOnlineList']:
                        iffyTuple = iffyMany.get(id, None)
                        if iffyTuple is not None:
                            statusData.update({id: {'iffy': iffyTuple}})
                        elif id in onlineUsers:
                            statusData.update({id: 'online'})
                    resultBody.append(item['item'])
    if len(thumbItemsToAdd) > 0:
        thumbsCache.set_many(thumbItemsToAdd)
        if len(privacyToAdd) > 0:
            privacyCache.set_many(privacyToAdd)
    return resultBody, statusData

def getUserFriendsOnline(requestUserId, friends, friendsIds, friendsCache, friendsIdsCache, friendShipStatusCache, friendRequestsCache, thumbsCache, privacyCache, onlineUsers, iffyCache, statusData):
    resultBody = []
    counter = 0
    iterableIdsDict = {}
    iffyMany = iffyCache.get_many(friendsIds)
    iffyUsers = set(iffyMany.keys())
    for id in friendsIds:
        if id in onlineUsers:
            if counter == 20:
                break
            iterableIdsDict.update({id: True})
            counter += 1
        elif id in iffyUsers:
            if counter == 20:
                break
            iterableIdsDict.update({id: False})
            counter += 1
    thumbItemsToAdd = {}
    iterableIds = set(iterableIdsDict.keys())
    itemsDictMany = thumbsCache.get_many(iterableIds)
    itemsDictKeys = itemsDictMany.keys()
    itemsSurplusSet = iterableIds - itemsDictKeys
    itemsSurplusSet1 = set()
    itemsSurplusSet1 = set()
    for id in itemsDictKeys:
        myItem = itemsDictMany[id].get(requestUserId, None)
        if myItem is None:
            itemsSurplusSet1.add(id)
        elif myItem is False:
            del iterableIdsDict[id]
            iterableIds.remove(id)
        else:
            if id == requestUserId:
                statusData.update({requestUserId: 'online'})
                resultBody.append(itemsDictMany[id][id]['item'])
                del iterableIdsDict[id]
                iterableIds.remove(id)
    idsToGetTHPSProfiles = itemsSurplusSet | itemsSurplusSet1
    if len(idsToGetTHPSProfiles) > 0:
        users = friends.filter(id__in = idsToGetTHPSProfiles)
        thpsProfiles = THPSProfile.objects.filter(toUser__in = users)
        if requestUserId in idsToGetTHPSProfiles:
            user = users.get(id__exact = requestUserId)
            firstName = user.first_name or None
            lastName = user.last_name or None
            if firstName and lastName:
                name = '{0} {1}'.format(firstName, lastName)
            else:
                if firstName:
                    name = firstName
                else:
                    name = user.username
            try:
                thpsProfile = thpsProfiles.get(toUser__exact = user)
            except:
                thpsProfile = None
            if thpsProfile is not None:
                nick = thpsProfile.nickName 
                clan = thpsProfile.clan or None
                if clan is not None:
                    name = '{0} [{1}*{2}]'.format(name, nick, clan)
                else:
                     name = '{0} [{1}]'.format(name, nick)
            thumb = user.avatarThumbSmall or None
            if thumb is None:
                if thpsProfile is not None:
                    if clan is not None:
                        background = clan.background
                        logo = clan.logo
                        if background and logo:
                            thumb = generateSmallThumbs(logo)
                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" /></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb)
                        else:
                            if logo:
                                thumb = generateSmallThumbs(logo)
                                itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                            else:
                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                    else:
                        itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                else:
                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
            else:
                itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(requestUserId, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb.url, name)
            item = {'item':itemString, 'showInOnlineList': True}
            thumbItemsToAdd.update({requestUserId: {requestUserId: item}})
            resultBody.append(itemString)
            statusData.update({requestUserId: 'online'})
            idsToGetTHPSProfiles.remove(requestUserId)
            del iterableIdsDict[requestUserId]
        privacyMany = privacyCache.get_many(idsToGetTHPSProfiles)
        privacyManyKeys = privacyMany.keys()
        privacySurplusSet = idsToGetTHPSProfiles - privacyManyKeys
        privacySurplusSet1 = set()
        for id in privacyManyKeys:
            privacyItem = privacyMany[id]
            privacyItemMy = privacyItem.get(requestUserId, None)
            if privacyItemMy is None or 'viewProfile' or 'viewMainInfo' or 'viewTHPSInfo' or 'viewAvatar' or 'viewOnlineStatus' not in privacyItemMy:
                privacySurplusSet1.add(id)
        idsToGetPrivacyModel = privacySurplusSet | privacySurplusSet1
        if len(idsToGetPrivacyModel) > 0:
            users = friends.filter(id__in = idsToGetPrivacyModel)
            privacyInstances = userProfilePrivacy.objects.filter(user__in = users)
            friendShipSurplus = idsToGetPrivacyModel - friendsIds
            friendShipStatusMany = friendShipStatusCache.get_many(friendShipSurplus)
            friendShipRequestsMany = friendRequestsCache.get_many(friendShipSurplus)
        friendShipStatusToAdd = {}
        privacyToAdd = {}
    for id, onlineStatus in iterableIdsDict.items():
        itemsDict = itemsDictMany.get(id, None)
        if itemsDict is None:
            user = None
            thumb = None
            privacyDict = privacyMany.get(id, None)
            if privacyDict is None:
                user = users.get(id__exact = id)
                privacySettings = privacyInstances.get(user__exact = user)
                friendShipDict = friendShipStatusMany.get(id, None)
                if friendShipDict is None:
                    if id in friendsIds:
                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                        friendShipStatusToAdd.update({id: {requestUserId: True}})
                    else:
                        requests = friendShipRequestsMany.get(requestUserId, set())
                        find = False
                        for request in requests:
                            if request[0] == id:
                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                friendShipStatusToAdd.update({id: {requestUserId: "request"}})
                                find = True
                                break
                        if find is False:
                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                            friendShipStatusToAdd.update({id: {requestUserId: False}})
                else:
                    friendShipStatus = friendShipDict.get(requestUserId, None)
                    if friendShipStatus is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                            friendShipDict.update({requestUserId: True})
                        else:
                            requests = friendShipRequestsMany.get(requestUserId, set())
                            find = False
                            for request in requests:
                                if request[0] == id:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                    friendShipDict.update({requestUserId: "request"})
                                    find = True
                                    break
                            if find is False:
                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                friendShipDict.update({requestUserId: False})
                        friendShipStatusToAdd.update({id: friendShipDict})
                    else:
                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                viewProfile = privacyChecker.checkViewProfilePrivacy()
                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                viewAvatar = privacyChecker.checkAvatarPrivacy()
                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                privacyToAdd.update({id: {requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}}})
            else:
                userPrivacyDict = privacyDict.get(requestUserId, None)
                if userPrivacyDict is None:
                    user = users.get(id__exact = id)
                    privacySettings = privacyInstances.get(user__exact = user)
                    friendShipDict = friendShipStatusMany.get(id, None)
                    if friendShipDict is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                            friendShipStatusToAdd.update({id: {requestUserId: True}})
                        else:
                            requests = friendShipRequestsMany.get(requestUserId, set())
                            find = False
                            for request in requests:
                                if request[0] == id:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                    friendShipStatusToAdd.update({id: {requestUserId: "request"}})
                                    find = True
                                    break
                            if find is False:
                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                friendShipStatusToAdd.update({id: {requestUserId: False}})
                    else:
                        friendShipStatus = friendShipDict.get(requestUserId, None)
                        if friendShipStatus is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                friendShipDict.update({requestUserId: True})
                            else:
                                requests = friendShipRequestsMany.get(requestUserId, set())
                                find = False
                                for request in requests:
                                    if request[0] == id:
                                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                        friendShipDict.update({requestUserId: "request"})
                                        find = True
                                        break
                                if find is False:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                    friendShipDict.update({requestUserId: False})
                            friendShipStatusToAdd.update({id: friendShipDict})
                        else:
                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    privacyDict.update({requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                    privacyToAdd.update({id: privacyDict})
                else:
                    viewProfile = userPrivacyDict.get('viewProfile', None)
                    viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                    viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                    viewAvatar = userPrivacyDict.get('viewAvatar', None)
                    viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                    if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None:
                        user = users.get(id__exact = id)
                        privacySettings = privacyInstances.get(user__exact = user)
                        friendShipDict = friendShipStatusMany.get(id, None)
                        if friendShipDict is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                friendShipStatusToAdd.update({id: {requestUserId: True}})
                            else:
                                requests = friendShipRequestsMany.get(requestUserId, set())
                                find = False
                                for request in requests:
                                    if request[0] == id:
                                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                        friendShipStatusToAdd.update({id: {requestUserId: "request"}})
                                        find = True
                                        break
                                if find is False:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                    friendShipStatusToAdd.update({id: {requestUserId: False}})
                        else:
                            friendShipStatus = friendShipDict.get(requestUserId, None)
                            if friendShipStatus is None:
                                if id in friendsIds:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                    friendShipDict.update({requestUserId: True})
                                else:
                                    requests = friendShipRequestsMany.get(requestUserId, set())
                                    find = False
                                    for request in requests:
                                        if request[0] == id:
                                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                            friendShipDict.update({requestUserId: "request"})
                                            find = True
                                            break
                                    if find is False:
                                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                        friendShipDict.update({requestUserId: False})
                                friendShipStatusToAdd.update({id: friendShipDict})
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                        if viewProfile is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewProfile': viewProfile})
                        if viewMainInfo is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                        if viewTHPSInfo is None:
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                        if viewAvatar is None:
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            userPrivacyDict.update({'viewAvatar': viewAvatar})
                        if viewOnlineStatus is None:
                            userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                        privacyToAdd.update({id: privacyDict})
            if viewProfile and viewOnlineStatus:
                if user is None:
                    user = users.get(id__exact = id)
                if viewMainInfo:
                    firstName = user.first_name or None
                    lastName = user.last_name or None
                    if firstName and lastName:
                        name = '{0} {1}'.format(firstName, lastName)
                    else:
                        if firstName:
                            name = firstName
                        else:
                            name = user.username
                else:
                    name = user.username
                if viewTHPSInfo:
                    try:
                        thpsProfile = thpsProfiles.get(toUser__exact = user)
                    except:
                        thpsProfile = None
                    if thpsProfile is not None:
                        nick = thpsProfile.nickName 
                        clan = thpsProfile.clan or None
                        if clan is not None:
                            name = '{0} [{1}*{2}]'.format(name, nick, clan)
                        else:
                            name = '{0} [{1}]'.format(name, nick)
                if viewAvatar:
                    thumb = user.avatarThumbSmall or None
                    if thumb is None:
                        if viewTHPSInfo:
                            if thpsProfile is not None:
                                if clan is not None:
                                    background = clan.background
                                    logo = clan.logo
                                    if background and logo:
                                        thumb = generateSmallThumbs(logo)
                                        itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                    else:
                                        if logo:
                                            thumb = generateSmallThumbs(logo)
                                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                        else:
                                            itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                                else:
                                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                            else:
                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                        else:
                            itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                    else:
                        itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb.url, name)
                else:
                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                item = {'item': itemString, 'showInOnlineList': viewOnlineStatus}
                if onlineStatus is True:
                    statusData.update({id: 'online'})
                else:
                    iffyTuple = iffyMany.get(id, None)
                    if iffyTuple is not None:
                        statusData.update({id: {'iffy': iffyTuple}})
            else:
                item = False
            thumbItemsToAdd.update({id: {requestUserId: item}})
        else:
            item = itemsDict.get(requestUserId, None)
            if item is None:
                user = None
                thumb = None
                privacyDict = privacyMany.get(id, None)
                if privacyDict is None:
                    user = users.get(id__exact = id)
                    privacySettings = privacyInstances.get(user__exact = user)
                    friendShipDict = friendShipStatusMany.get(id, None)
                    if friendShipDict is None:
                        if id in friendsIds:
                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                            friendShipStatusToAdd.update({id: {requestUserId: True}})
                        else:
                            requests = friendShipRequestsMany.get(requestUserId, set())
                            find = False
                            for request in requests:
                                if request[0] == id:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                    friendShipStatusToAdd.update({id: {requestUserId: "request"}})
                                    find = True
                                    break
                            if find is False:
                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                friendShipStatusToAdd.update({id: {requestUserId: False}})
                    else:
                        friendShipStatus = friendShipDict.get(requestUserId, None)
                        if friendShipStatus is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                friendShipDict.update({requestUserId: True})
                            else:
                                requests = friendShipRequestsMany.get(requestUserId, set())
                                find = False
                                for request in requests:
                                    if request[0] == id:
                                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                        friendShipDict.update({requestUserId: "request"})
                                        find = True
                                        break
                                if find is False:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                    friendShipDict.update({requestUserId: False})
                            friendShipStatusToAdd.update({id: friendShipDict})
                        else:
                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    privacyToAdd.update({id: {requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}}})
                else:
                    userPrivacyDict = privacyDict.get(requestUserId, None)
                    if userPrivacyDict is None:
                        user = users.get(id__exact = id)
                        privacySettings = privacyInstances.get(user__exact = user)
                        friendShipDict = friendShipStatusMany.get(id, None)
                        if friendShipDict is None:
                            if id in friendsIds:
                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                friendShipStatusToAdd.update({id: {requestUserId: True}})
                            else:
                                requests = friendShipRequestsMany.get(requestUserId, set())
                                find = False
                                for request in requests:
                                    if request[0] == id:
                                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                        friendShipStatusToAdd.update({id: {requestUserId: "request"}})
                                        find = True
                                        break
                                if find is False:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                    friendShipStatusToAdd.update({id: {requestUserId: False}})
                        else:
                            friendShipStatus = friendShipDict.get(requestUserId, None)
                            if friendShipStatus is None:
                                if id in friendsIds:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                    friendShipDict.update({requestUserId: True})
                                else:
                                    requests = friendShipRequestsMany.get(requestUserId, set())
                                    find = False
                                    for request in requests:
                                        if request[0] == id:
                                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                            friendShipDict.update({requestUserId: "request"})
                                            find = True
                                            break
                                    if find is False:
                                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                        friendShipDict.update({requestUserId: False})
                                friendShipStatusToAdd.update({id: friendShipDict})
                            else:
                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                        viewProfile = privacyChecker.checkViewProfilePrivacy()
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        privacyDict.update({requestUserId: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                        privacyToAdd.update({id: privacyDict})
                    else:
                        viewProfile = userPrivacyDict.get('viewProfile', None)
                        viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                        viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                        viewAvatar = userPrivacyDict.get('viewAvatar', None)
                        viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                        if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewAvatar is None or viewOnlineStatus is None:
                            user = users.get(id__exact = id)
                            privacySettings = privacyInstances.get(user__exact = user)
                            friendShipDict = friendShipStatusMany.get(id, None)
                            if friendShipDict is None:
                                if id in friendsIds:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                    friendShipStatusToAdd.update({id: {requestUserId: True}})
                                else:
                                    requests = friendShipRequestsMany.get(requestUserId, set())
                                    find = False
                                    for request in requests:
                                        if request[0] == id:
                                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                            friendShipStatusToAdd.update({id: {requestUserId: "request"}})
                                            find = True
                                            break
                                    if find is False:
                                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                        friendShipStatusToAdd.update({id: {requestUserId: False}})
                            else:
                                friendShipStatus = friendShipDict.get(requestUserId, None)
                                if friendShipStatus is None:
                                    if id in friendsIds:
                                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                        friendShipDict.update({requestUserId: True})
                                    else:
                                        requests = friendShipRequestsMany.get(requestUserId, set())
                                        find = False
                                        for request in requests:
                                            if request[0] == id:
                                                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = True, isAuthenticated = True)
                                                friendShipDict.update({requestUserId: "request"})
                                                find = True
                                                break
                                        if find is False:
                                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = True)
                                            friendShipDict.update({requestUserId: False})
                                    friendShipStatusToAdd.update({id: friendShipDict})
                                else:
                                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True)
                            if viewProfile is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewProfile': viewProfile})
                            if viewMainInfo is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                            if viewTHPSInfo is None:
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                            if viewAvatar is None:
                                viewAvatar = privacyChecker.checkAvatarPrivacy()
                                userPrivacyDict.update({'viewAvatar': viewAvatar})
                            if viewOnlineStatus is None:
                                userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                            privacyToAdd.update({id: privacyDict})
                if viewProfile and viewOnlineStatus:
                    if user is None:
                        user = users.get(id__exact = id)
                    if viewMainInfo:
                        firstName = user.first_name or None
                        lastName = user.last_name or None
                        if firstName and lastName:
                            name = '{0} {1}'.format(firstName, lastName)
                        else:
                            if firstName:
                                name = firstName
                            else:
                                name = user.username
                    else:
                        name = user.username
                    if viewTHPSInfo:
                        try:
                            thpsProfile = thpsProfiles.get(toUser__exact = user)
                        except:
                            thpsProfile = None
                        if thpsProfile is not None:
                            nick = thpsProfile.nickName 
                            clan = thpsProfile.clan or None
                            if clan is not None:
                                name = '{0} [{1}*{2}]'.format(name, nick, clan)
                            else:
                                name = '{0} [{1}]'.format(name, nick)
                    if viewAvatar:
                        thumb = user.avatarThumbSmall or None
                        if thumb is None:
                            if viewTHPSInfo:
                                if thpsProfile is not None:
                                    if clan is not None:
                                        background = clan.background
                                        logo = clan.logo
                                        if background and logo:
                                            thumb = generateSmallThumbs(logo)
                                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                        else:
                                            if logo:
                                                thumb = generateSmallThumbs(logo)
                                                itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                            else:
                                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                                    else:
                                        itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                                else:
                                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                            else:
                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                        else:
                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb.url, name)
                    else:
                        itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                    item = {'item': itemString, 'showInOnlineList': viewOnlineStatus}
                    if onlineStatus is True:
                        statusData.update({id: 'online'})
                    else:
                        iffyTuple = iffyMany.get(id, None)
                        if iffyTuple is not None:
                            statusData.update({id: {'iffy': iffyTuple}})
                else:
                    item = False
                itemsDict.update({requestUserId: item})
                thumbItemsToAdd.update({id: itemsDict})
            else:
                if item is not False:
                    if item['showInOnlineList']:
                        if onlineStatus is True:
                            statusData.update({id: 'online'})
                        else:
                            iffyTuple = iffyMany.get(id, None)
                            if iffyTuple is not None:
                                statusData.update({id: {'iffy': iffyTuple}})
                    resultBody.append(item['item'])
    if len(thumbItemsToAdd) > 0:
        thumbsCache.set_many(thumbItemsToAdd)
        if len(friendShipStatusToAdd) > 0:
            friendShipStatusCache.set_many(friendShipStatusToAdd)
        if len(privacyToAdd) > 0:
            privacyCache.set_many(privacyToAdd)
    return resultBody, statusData

def getUserFriendsOnlineGuest(friends, friendsIds, thumbsCache, privacyCache, onlineUsers, iffyCache, iffyUsers, statusData):
    resultBody = []
    counter = 0
    iterableIdsDict = {}
    for id in friendsIds:
        if id in onlineUsers:
            if counter == 20:
                break
            iterableIdsDict.update({id: True})
            counter += 1
        elif id in iffyUsers:
            if counter == 20:
                break
            iterableIdsDict.update({id: False})
            counter += 1
    thumbItemsToAdd = {}
    iterableIds = set(iterableIdsDict.keys())
    itemsDictMany = thumbsCache.get_many(iterableIds)
    itemsDictKeys = itemsDictMany.keys()
    itemsSurplusSet = iterableIds - itemsDictKeys
    itemsSurplusSet1 = set()
    for id in itemsDictKeys:
        myItem = itemsDictMany[id].get(0, None)
        if myItem is None:
            itemsSurplusSet1.add(id)
        elif myItem is False:
            del iterableIdsDict[id]
            iterableIds.remove(id)
    iffyTimeMany = iffyCache.get_many(iterableIds)
    idsToGetTHPSProfiles = itemsSurplusSet | itemsSurplusSet1
    if len(idsToGetTHPSProfiles) > 0:
        users = friends.filter(id__in = idsToGetTHPSProfiles)
        thpsProfiles = THPSProfile.objects.filter(toUser__in = users)
        privacyMany = privacyCache.get_many(idsToGetTHPSProfiles)
        privacyManyKeys = privacyMany.keys()
        privacySurplusSet = idsToGetTHPSProfiles - privacyManyKeys
        privacySurplusSet1 = set()
        for id in privacyManyKeys:
            privacyItem = privacyMany[id]
            privacyItemMy = privacyItem.get(0, None)
            if privacyItemMy is None or 'viewProfile' or 'viewMainInfo' or 'viewTHPSInfo' or 'viewAvatar' or 'viewOnlineStatus' not in privacyItemMy:
                privacySurplusSet1.add(id)
        idsToGetPrivacyModel = privacySurplusSet | privacySurplusSet1
        if len(idsToGetPrivacyModel) > 0:
            users = friends.filter(id__in = idsToGetPrivacyModel)
            privacyInstances = userProfilePrivacy.objects.filter(user__in = users)
        privacyToAdd = {}
    iffyMany = iffyCache.get_many(iterableIds)
    for id, onlineStatus in iterableIdsDict.items():
        itemsDict = itemsDictMany.get(id, None)
        if itemsDict is None:
            user = None
            thumb = None
            privacyDict = privacyMany.get(id, None)
            if privacyDict is None:
                user = users.get(id = id)
                privacySettings = privacyInstances.get(user__exact = user)
                privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = False)
                viewProfile = privacyChecker.checkViewProfilePrivacy()
                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                viewAvatar = privacyChecker.checkAvatarPrivacy()
                privacyToAdd.update({id: {0: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}}})
            else:
                userPrivacyDict = privacyDict.get(0, None)
                if userPrivacyDict is None:
                    user = users.get(id = id)
                    privacySettings = privacyInstances.get(user__exact = user)
                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = False)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    privacyDict.update({0: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                    privacyToAdd.update({id: privacyDict})
                else:
                    viewProfile = userPrivacyDict.get('viewProfile', None)
                    viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                    viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                    viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                    viewAvatar = userPrivacyDict.get('viewAvatar', None)
                    if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewOnlineStatus is None or viewAvatar is None:
                        user = users.get(id = id)
                        privacySettings = privacyInstances.get(user__exact = user)
                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = False)
                        if viewProfile is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewProfile': viewProfile})
                        if viewMainInfo is None:
                            viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                            userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                        if viewTHPSInfo is None:
                            viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                            userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                        if viewOnlineStatus is None:
                            viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                            userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                        if viewAvatar is None:
                            viewAvatar = privacyChecker.checkAvatarPrivacy()
                            userPrivacyDict.update({'viewAvatar': viewAvatar})
                        privacyToAdd.update({id: privacyDict})
            if viewProfile and viewOnlineStatus:
                if user is None:
                    user = users.get(id__exact = id)
                if viewMainInfo:
                    firstName = user.first_name or None
                    lastName = user.last_name or None
                    if firstName and lastName:
                        name = '{0} {1}'.format(firstName, lastName)
                    else:
                        if firstName:
                            name = firstName
                        else:
                            name = user.username
                else:
                    name = user.username
                if viewTHPSInfo:
                    try:
                        thpsProfile = thpsProfiles.get(toUser__exact = user)
                    except:
                        thpsProfile = None
                    if thpsProfile is not None:
                        nick = thpsProfile.nickName 
                        clan = thpsProfile.clan or None
                        if clan is not None:
                            name = '{0} [{1}*{2}]'.format(name, nick, clan)
                        else:
                            name = '{0} [{1}]'.format(name, nick)
                if viewAvatar:
                    thumb = user.avatarThumbSmall or None
                    if thumb is None:
                        if viewTHPSInfo:
                            if thpsProfile is not None:
                                if clan is not None:
                                    background = clan.background
                                    logo = clan.logo
                                    if background and logo:
                                        thumb = generateSmallThumbs(logo)
                                        itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                    else:
                                        if logo:
                                            thumb = generateSmallThumbs(logo)
                                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                        else:
                                            itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                                else:
                                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                            else:
                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                        else:
                            itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                    else:
                        itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb.url, name)
                else:
                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                item = {'item': item, 'showInOnlineList': viewOnlineStatus}
                if onlineStatus is True:
                    statusData.update({id: 'online'})
                else:
                    iffyTuple = iffyMany.get(id, None)
                    if iffyTuple is not None:
                        statusData.update({id: {'iffy': iffyTuple}})
            else:
                item = False
            thumbItemsToAdd.update({id: {0: item}})
        else:
            item = itemsDict.get(0, None)
            if item is None:
                user = None
                thumb = None
                privacyDict = privacyMany.get(id, None)
                if privacyDict is None:
                    user = users.get(id = id)
                    privacySettings = privacyInstances.get(user__exact = user)
                    privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = False)
                    viewProfile = privacyChecker.checkViewProfilePrivacy()
                    viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                    viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                    viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                    viewAvatar = privacyChecker.checkAvatarPrivacy()
                    privacyToAdd.update({id: {0: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}}})
                else:
                    userPrivacyDict = privacyDict.get(0, None)
                    if userPrivacyDict is None:
                        user = users.get(id = id)
                        privacySettings = privacyInstances.get(user__exact = user)
                        privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = False)
                        viewProfile = privacyChecker.checkViewProfilePrivacy()
                        viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                        viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                        viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                        viewAvatar = privacyChecker.checkAvatarPrivacy()
                        privacyDict.update({0: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus}})
                        privacyToAdd.update({id: privacyDict})
                    else:
                        viewProfile = userPrivacyDict.get('viewProfile', None)
                        viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
                        viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
                        viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
                        viewAvatar = userPrivacyDict.get('viewAvatar', None)
                        if viewProfile is None or viewMainInfo is None or viewTHPSInfo is None or viewOnlineStatus is None or viewAvatar is None:
                            user = users.get(id = id)
                            privacySettings = privacyInstances.get(user__exact = user)
                            privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = False)
                            if viewProfile is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewProfile': viewProfile})
                            if viewMainInfo is None:
                                viewMainInfo = privacyChecker.checkMainInfoPrivacy()
                                userPrivacyDict.update({'viewMainInfo': viewMainInfo})
                            if viewTHPSInfo is None:
                                viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
                                userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
                            if viewOnlineStatus is None:
                                viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
                                userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
                            if viewAvatar is None:
                                viewAvatar = privacyChecker.checkAvatarPrivacy()
                                userPrivacyDict.update({'viewAvatar': viewAvatar})
                            privacyToAdd.update({id: privacyDict})
                if viewProfile and viewOnlineStatus:
                    if user is None:
                        user = users.get(id__exact = id)
                    if viewMainInfo:
                        firstName = user.first_name or None
                        lastName = user.last_name or None
                        if firstName and lastName:
                            name = '{0} {1}'.format(firstName, lastName)
                        else:
                            if firstName:
                                name = firstName
                            else:
                                name = user.username
                    else:
                        name = user.username
                    if viewTHPSInfo:
                        try:
                            thpsProfile = thpsProfiles.get(toUser__exact = user)
                        except:
                            thpsProfile = None
                        if thpsProfile is not None:
                            nick = thpsProfile.nickName 
                            clan = thpsProfile.clan or None
                            if clan is not None:
                                name = '{0} [{1}*{2}]'.format(name, nick, clan)
                            else:
                                name = '{0} [{1}]'.format(name, nick)
                    if viewAvatar:
                        thumb = user.avatarThumbSmall or None
                        if thumb is None:
                            if viewTHPSInfo:
                                if thpsProfile is not None:
                                    if clan is not None:
                                        background = clan.background
                                        logo = clan.logo
                                        if background and logo:
                                            thumb = generateSmallThumbs(logo)
                                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                        else:
                                            if logo:
                                                thumb = generateSmallThumbs(logo)
                                                itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                                            else:
                                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                                    else:
                                        itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                                else:
                                    itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{3}"><p>{3}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb, name)
                            else:
                                itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                        else:
                            itemString = '<li value = "{0}"><a href = "{1}"><img src = "{2}" title = "{3}" /></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), thumb.url, name)
                    else:
                        itemString = '<li value = "{0}"><a href = "{1}"><div class = "no-avatar" title = "{2}"><p>{2}</p></div></a></li>'.format(id, reverse("profile", kwargs = {'currentUser': user.profileUrl}), name)
                    item = {'item': itemString, 'showInOnlineList': viewOnlineStatus}
                    if onlineStatus is True:
                        statusData.update({id: 'online'})
                    else:
                        iffyTuple = iffyMany.get(id, None)
                        if iffyTuple is not None:
                            statusData.update({id: {'iffy': iffyTuple}})
                else:
                    item = False
                itemsDict.update({0: item})
                thumbItemsToAdd.update({id: itemsDict})
            else:
                if item is not False:
                    if item['showInOnlineList']:
                        if onlineStatus is True:
                            statusData.update({id: 'online'})
                        else:
                            iffyTuple = iffyMany.get(id, None)
                            if iffyTuple is not None:
                                statusData.update({id: {'iffy': iffyTuple}})
                    resultBody.append(item['item'])
    if len(thumbItemsToAdd) > 0:
        thumbsCache.set_many(thumbItemsToAdd)
        if len(privacyToAdd) > 0:
            privacyCache.set_many(privacyToAdd)
    return resultBody, statusData