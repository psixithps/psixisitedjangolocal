import json
from datetime import datetime
from functools import lru_cache
from collections import OrderedDict
from django.core.cache import caches
from django.urls import reverse
from django.shortcuts import render, redirect
from django.template.loader import render_to_string as rts
from django.http import JsonResponse
from django.core.paginator import Paginator
from cities.models import Country, Region, Subregion, City
from psixiSocial.models import mainUserProfile, THPSProfile, userProfilePrivacy
from psixiSocial.forms import registrationForm, userEditDetalForm, userEditMainForm, userEditPassword
from psixiSocial.translators import singleElementGetter, profileTimeStatusText
from psixiSocial.privacyIdentifier import checkPrivacy
from base.helpers import checkCSRF
from imagekit import ImageSpec
from imagekit.processors import ResizeToFill
from psixiSocial.profile.items import getUserFriends, getUserFriendsGuest, getUserFriendsIm, getUserFriendsOnline, getUserFriendsOnlineGuest, getUserFriendsOnlineIm

class cachedFragments():
	def __init__(self, **kwargs):
		self.username = kwargs.pop('username', None)
		self.profileUrl = kwargs.pop('profileUrl', None)

	@property
	@lru_cache(maxsize = 16)
	def type1(self):
		menu = OrderedDict({
			0: {
				'textname': 'Мой профиль',
				'url': reverse('profile', kwargs = {'currentUser': 'im'}),
				0: {
					'textname': 'Диалоги',
					'url': reverse('dialogs', kwargs = {'sep': '', 'page': ''})
					},
				1: {
					'textname': 'Друзья',
					0: {
						'textname': 'Все',
						'url': reverse('friends', kwargs = {'user': 'my', 'category': 'all', 'sep': '',  'page': ''})
						},
					1: {
						'textname': 'Сейчас в сети',
						'url': reverse('friends', kwargs = {'user': 'my', 'category': 'online', 'sep': '',  'page': ''})
						}
					},
				2: {
					'textname': 'Записи в блоге'
					},
				3: {
					'textname': 'Записи в libraryTHPS'
					},
				4: {
					'textname': 'Редактировать',
					0: {
						'textname': 'Основные',
						'url': reverse('profileEditUrl', kwargs = {'editType': 'main'})
						},
					1: {
						'textname': 'Дополнительно',
						'url': reverse('profileEditUrl', kwargs = {'editType': 'detail'})
						},
					2: {
						'textname': 'Обратная связь',
						'url': reverse('profileEditUrl', kwargs = {'editType': 'contacts'})
						},
					3: {
						'textname': 'Настройки приватности',
						'url': reverse('editPrivacyUrl', kwargs = {'editPlace': 'profile'}),
						0: {
							'textname': 'Права для исключительных пользователей',
							'url': reverse('editPrivacyExclusiveUrl', kwargs = {'editPlace': 'profile'})
							}
						},
					4: {
						'textname': 'THPS профиль',
						'url': reverse('profileEditUrl', kwargs = {'editType': 'thps'})
						}
					},
				},
			1: {
				'textname': 'Пользователи',
				0: {
					'textname': 'Все',
					'url': reverse('users', kwargs = {'category': 'all', 'sep': '', 'page': ''})
					},
				1: {
					'textname': 'Сейчас в сети',
					'url': reverse('users', kwargs = {'category': 'online', 'sep': '', 'page': ''})
					}
				},
			2: {
				'textname': 'Выход',
				'url': reverse("logOut")
				}
			})
		return menu

	@property
	@lru_cache(maxsize = 16)
	def type1Serialized(self):
		menu = self.type1
		if isinstance(menu, OrderedDict):
			return json.dumps(menu)
		return json.dumps(menu())

	@property
	@lru_cache(maxsize = 16)
	def type2(self):
		menu = OrderedDict({
			0: {
				'textname': 'Профиль %s' % self.username,
				'url': reverse('profile', kwargs = {'currentUser': self.profileUrl}),
				0: {
					'textname': 'Перейти к диалогу с %s' % self.username,
					'url': reverse('personalMessage', kwargs = {'toUser': self.profileUrl})
					},
				1: {
					'textname': 'Друзья',
					0: {
						'textname': 'Все',
						'url': reverse('friends', kwargs = {'user': self.profileUrl, 'category': 'all', 'sep': '', 'page': ''})
						},
					1: {
						'textname': 'Сейчас в сети',
						'url': reverse('friends', kwargs = {'user': self.profileUrl,'category': 'online', 'sep': '', 'page': ''})
						}
					},
				2: {
					'textname': 'Записи в блоге'
					},
				3: {
					'textname': 'Записи в libraryTHPS'
					},
				},
			1: {
				'textname': 'Мой профиль',
				'url': reverse('profile', kwargs = {'currentUser': 'im'}),
				0: {
					'textname': 'Диалоги',
					'url': reverse('dialogs', kwargs = {'sep': '', 'page': ''})
					},
				1: {
					'textname': 'Друзья',
					0: {
						'textname': 'Все',
						'url': reverse('friends', kwargs = {'user': 'my', 'category': 'all', 'sep': '',  'page': ''})
						},
					1: {
						'textname': 'Сейчас в сети',
						'url': reverse('friends', kwargs = {'user': 'my', 'category': 'online', 'sep': '',  'page': ''})
						}
					},
				2: {
					'textname': 'Редактировать',
					0: {
						'textname': 'Основные',
						'url': reverse('profileEditUrl', kwargs = {'editType': 'main'})
						},
					1: {
						'textname': 'Дополнительно',
						'url': reverse('profileEditUrl', kwargs = {'editType': 'detail'})
						},
					2: {
						'textname': 'Обратная связь',
						'url': reverse('profileEditUrl', kwargs = {'editType': 'contacts'})
						},
					3: {
						'textname': 'Настройки приватности',
						'url': reverse('editPrivacyUrl', kwargs = {'editPlace': 'profile'}),
						0: {
							'textname': 'Права для исключительных пользователей',
							'url': reverse('editPrivacyExclusiveUrl', kwargs = {'editPlace': 'profile'})
							}
						},
					4: {
						'textname': 'THPS профиль',
						'url': reverse('profileEditUrl', kwargs = {'editType': 'thps'})
						}
					},
				},
			2: {
				'textname': 'Пользователи',
				0: {
					'textname': 'Все',
					'url': reverse('users', kwargs = {'category': 'all', 'sep': '', 'page': ''})
					},
				1: {
					'textname': 'Сейчас в сети',
					'url': reverse('users', kwargs = {'category': 'online', 'sep': '', 'page': ''})
					}
				},
			3: {
				'textname': 'Выход',
				'url': reverse("logOut")
				}
			})
		return menu

	@property
	@lru_cache(maxsize = 16)
	def type2Serialized(self):
		menu = self.type2
		if isinstance(menu, OrderedDict):
			return json.dumps(menu)
		return json.dumps(menu())

	def type3(self):
		menu = OrderedDict({
			0: {
				'textname': 'Профиль %s' % self.username,
				'url': reverse('profile', kwargs = {'currentUser': self.profileUrl}),
				0: {
					'textname': 'Друзья',
					0: {
						'textname': 'Все',
						'url': reverse('friends', kwargs = {'user': self.profileUrl, 'category': 'all', 'sep': '', 'page': ''})
						},
					1: {
						'textname': 'Сейчас в сети',
						'url': reverse('friends', kwargs = {'user': self.profileUrl,'category': 'online', 'sep': '', 'page': ''})
						}
					},
				1: {
					'textname': 'Записи в блоге'
					},
				2: {
					'textname': 'Записи в libraryTHPS'
					},
				},
			1: {
				'textname': 'Гость',
				0: {
					'textname': 'Регистрация'
					},
				1: {
					'textname': 'Вход',
					'url': reverse('login')
					}
				},
			2: {
				'textname': 'Пользователи',
				0: {
					'textname': 'Все',
					'url': reverse('users', kwargs = {'category': 'all', 'sep': '', 'page': ''})
					},
				1: {
					'textname': 'Сейчас в сети',
					'url': reverse('users', kwargs = {'category': 'online', 'sep': '', 'page': ''})
					}
				}
			})
		return menu

	def type3Serialized(self):
		menu = self.type3
		if isinstance(menu, OrderedDict):
			return json.dumps(menu)
		return json.dumps(menu())

	def menuGetter(self, menuType):
		menuOrMethod = getattr(self, menuType)
		if isinstance(menuOrMethod, OrderedDict):
			return menuOrMethod
		if isinstance(menuOrMethod, str):
			return menuOrMethod
		return menuOrMethod()

def emptyProfileUrl(request):
	if request.is_ajax():
		return JsonResponse({
			'redirectTo': json.dumps({reverse('profile', kwargs = {'currentUser': 'im'})})
			})
	return redirect('profile', 'im')

def renderFriendsList(request, userId):
	requestUser = request.user
	try:
		userProfile = mainUserProfile.objects.get(id = userId)
	except:
		pass
	privacyModelInstance = userProfilePrivacy.objects.get(user = userProfile)
	friendsListPrivacy = privacyModelInstance.viewProfileFriends
	if friendsListPrivacy == 0:
		pass
	if friendsListPrivacy == 1:
		if requestUser.is_authenticated:
			return JsonResponse({
				"friendsLists": json.dumps(rts("psixiSocial/body/profile/profileFriends.html", {
					
					}))
				})
	if friendsListPrivacy == 2:
		if requestUser.is_authenticated:
			pass
	if friendsListPrivacy == 7:
		pass

def profileView(request, currentUser):
	if request.method == 'GET':
		user = request.user
		if user.is_authenticated:
			requestUserSlug = user.profileUrl
			if currentUser == 'im' or currentUser == requestUserSlug:
				id = str(user.id)
				try:
					mainUserProfile.objects.get(id__exact = id)
				except mainUserProfile.DoesNotExist:
					return render(request, 'base/404.html', {'msg': 'Пользователь %s не найден' % user.username})
				fragments = cachedFragments()
				timeTextDict = profileTimeStatusText(lang = 'ru').getDict()
				friendsCache = caches['friendsCache']
				friends = friendsCache.get(id, None)
				timeStr = str(datetime.now())
				if friends is None:
					friends = user.friends.all()
					if friends.count() > 0:
						thumbsCache = caches['smallThumbs']
						friendsCacheId = caches['friendsIdCache']
						friendsIds = friendsCacheId.get(id, None)
						if friendsIds is None:
							friendsIdsQuerySet = friends.values("id")
							friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
							friendsCacheId.set(id, friendsIds)
						onlineStatusCache = caches['onlineUsers']
						iffyStatusCache = caches['iffyUsersOnlineStatus']
						onlineUsers = onlineStatusCache.get('online', set())
						statusDict = {}
						friendsList, statusDict = getUserFriendsIm(id, friends, friendsIds, thumbsCache, onlineUsers, iffyStatusCache, statusDict)
						friendsOnlineList, statusDict = getUserFriendsOnlineIm(id, friends, friendsIds, thumbsCache, onlineUsers, iffyStatusCache, statusDict)
						friendsCache.set(id, friends)
					else:
						friendsCache.set(id, False)
						friendsList, friendsOnlineList, statusDict = None, None, None
				else:
					if friends is not False:
						thumbsCache = caches['smallThumbs']
						friendsCacheId = caches['friendsIdCache']
						friendsIds = friendsCacheId.get(id, None)
						if friendsIds is None:
							friendsIdsQuerySet = friends.values("id")
							friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
							friendsCacheId.set(id, friendsIds)
						onlineStatusCache = caches['onlineUsers']
						iffyStatusCache = caches['iffyUsersOnlineStatus']
						onlineUsers = onlineStatusCache.get('online', set())
						statusDict = {}
						friendsList, statusDict = getUserFriendsIm(id, friends, friendsIds, thumbsCache, onlineUsers, iffyStatusCache, statusDict)
						friendsOnlineList, statusDict = getUserFriendsOnlineIm(id, friends, friendsIds, thumbsCache, onlineUsers, iffyStatusCache, statusDict)
					else:
						friendsList, friendsOnlineList, statusDict = None, None, None
				try:
					thpsProfile = THPSProfile.objects.get(toUser = user)
				except:
					thpsProfile = None
				if thpsProfile is None:
					if request.is_ajax():
						if 'global' in request.GET:
							return JsonResponse({'newPage': json.dumps({
								'newTemplate': rts('psixiSocial/psixiSocial.html', {
									'parentTemplate': 'base/classicPage/emptyParent.html',
									'content': ('psixiSocial', 'profile', 'show', 'my', ''),
									'menu': fragments.menuGetter('type1Serialized'),
									'profileMainInfo': {
										'id': id,
										'url': requestUserSlug,
										'username': user.username,
										'firstname': user.first_name,
										'lastname': user.last_name,
										'country': user.country,
										'region': user.subRegion,
										'city': user.city,
										'birthdate': user.birthDate
										},
									'profileContacts': {
										'email': user.email
										},
									'profileTHPSInfo': None,
									'profileImages': {
										'background': user.background,
										'avatar': user.avatarProfileThumb
										},
									'csrf': '',
									'bodyStatus': timeTextDict['onlineStatus'],
									'scriptsStatusDict': None,
									'onlineStatusTextDict': None,
									'currentDateTime': timeStr,
									'requestUserId': id,
									'targetUserId': None,
									'friends': friendsList,
									'friendsStatusDict': statusDict,
									'onlineFriends': friendsOnlineList
									}),
								'url': reverse('profile', kwargs = {'currentUser': 'im'}),
								'titl': 'Профиль %s' % user.username
								})})
						return JsonResponse({'newPage': json.dumps({
							'content': ('psixiSocial', 'profile', 'show', 'my', ''),
							'menu': fragments.menuGetter('type1'),
							'templateHead': rts('psixiSocial/head/profile.html'),
							'templateBody': rts('psixiSocial/body/profile/profile.html', {
								'profileMainInfo': {
									'id': id,
									'url': requestUserSlug,
									'username': user.username,
									'firstname': user.first_name,
									'lastname': user.last_name,
									'country': user.country,
									'region': user.subRegion,
									'city': user.city,
									'birthdate': user.birthDate
									},
								'profileContacts': {
									'email': user.email
									},
								'profileTHPSInfo': None,
								'profileImages': {
									'background': user.background,
									'avatar': user.avatarProfileThumb
									},
								'bodyStatus': timeTextDict['onlineStatus'],
								'csrf': '',
								'friends': friendsList,
								'onlineFriends': friendsOnlineList
								}),
							'templateScripts': rts('psixiSocial/scripts/profile.html', {
								'onlineStatusTextDict': None,
								'scriptsStatusDict': None,
								'currentDateTime': timeStr,
								'requestUserId': id,
								'targetUserId': None,
								'friendsStatusDict': statusDict
								}),
							'url': reverse('profile', kwargs = {'currentUser': 'im'})
							})})
					return render(request, 'psixiSocial/psixiSocial.html', {
						'parentTemplate': 'base/classicPage/base.html',
						'content': ('psixiSocial', 'profile', 'show', 'my', ''),
						'menu': fragments.menuGetter('type1Serialized'),
						'profileMainInfo': {
							'id': id,
							'url': requestUserSlug,
							'username': user.username,
							'firstname': user.first_name,
							'lastname': user.last_name,
							'country': user.country,
							'region': user.subRegion,
							'city': user.city,
							'birthdate': user.birthDate
							},
						'profileContacts': {
							'email': user.email
							},
						'profileTHPSInfo': None,
						'profileImages': {
							'background': user.background,
							'avatar': user.avatarProfileThumb
							},
						'csrf': checkCSRF(request, request.COOKIES),
						'bodyStatus': timeTextDict['onlineStatus'],
						'scriptsStatusDict': None,
						'onlineStatusTextDict': None,
						'currentDateTime': timeStr,
						'requestUserId': id,
						'targetUserId': None,
						'friends': friendsList,
						'friendsStatusDict': statusDict,
						'onlineFriends': friendsOnlineList
						})
				if request.is_ajax():
					if 'global' in request.GET:
						return JsonResponse({'newPage': json.dumps({
							'newTemplate': rts('psixiSocial/psixiSocial.html', {
								'parentTemplate': 'base/classicPage/emptyParent.html',
								'content': ('psixiSocial', 'profile', 'show', 'my', 'thps'),
								'menu': fragments.menuGetter('type1Serialized'),
								'profileMainInfo': {
									'id': id,
									'url': requestUserSlug,
									'username': user.username,
									'firstname': user.first_name,
									'lastname': user.last_name,
									'country': user.country,
									'region': user.subRegion,
									'city': user.city,
									'birthdate': user.birthDate
									},
								'profileContacts': {
									'email': user.email
									},
								'profileTHPSInfo': {
									'nickname': thpsProfile.nickName,
									'clan': thpsProfile.clanName
									},
								'profileImages': {
									'background': user.background,
									'avatar': user.avatarProfileThumb
									},
								'csrf': '',
								'bodyStatus': timeTextDict['onlineStatus'],
								'scriptsStatusDict': None,
								'onlineStatusTextDict': None,
								'currentDateTime': timeStr,
								'requestUserId': id,
								'targetUserId': None,
								'friends': friendsList,
								'friendsStatusDict': statusDict,
								'onlineFriends': friendsOnlineList
								}),
							'url': reverse('profile', kwargs = {'currentUser': 'im'}),
							'titl': 'Профиль %s' % user.username
							})})
					return JsonResponse({'newPage': json.dumps({
						'content': ('psixiSocial', 'profile', 'show', 'my', 'thps'),
						'menu': fragments.menuGetter('type1'),
						'templateHead': rts('psixiSocial/head/profile.html'),
						'templateBody': rts('psixiSocial/body/profile/profileThpsInfo.html', {
							'profileMainInfo': {
								'id': id,
								'url': requestUserSlug,
								'username': user.username,
								'firstname': user.first_name,
								'lastname': user.last_name,
								'country': user.country,
								'region': user.subRegion,
								'city': user.city,
								'birthdate': user.birthDate
								},
							'profileContacts': {
								'email': user.email
								},
							'profileTHPSInfo': {
								'nickname': thpsProfile.nickName,
								'clan': thpsProfile.clanName
								},
							'profileImages': {
								'background': user.background,
								'avatar': user.avatarProfileThumb
								},
							'bodyStatus': timeTextDict['onlineStatus'],
							'csrf': '',
							'friends': friendsList,
							'onlineFriends': friendsOnlineList
							}),
						'templateScripts': rts('psixiSocial/scripts/profile.html', {
							'scriptsStatusDict': None,
							'onlineStatusTextDict': None,
							'currentDateTime': timeStr,
							'requestUserId': id,
							'targetUserId': None,
							'friendsStatusDict': statusDict
							}),
						'url': reverse('profile', kwargs = {'currentUser': 'im'})
						})})
				return render(request, 'psixiSocial/psixiSocial.html', {
					'parentTemplate': 'base/classicPage/base.html',
					'content': ('psixiSocial', 'profile', 'show', 'my', 'thps'),
					'menu': fragments.menuGetter('type1Serialized'),
					'profileMainInfo': {
						'id': id,
						'url': requestUserSlug,
						'username': user.username,
						'firstname': user.first_name,
						'lastname': user.last_name,
						'country': user.country,
						'region': user.subRegion,
						'city': user.city,
						'birthdate': user.birthDate
						},
					'profileContacts': {
						'email': user.email
						},
					'profileTHPSInfo': {
						'nickname': thpsProfile.nickName,
						'clan': thpsProfile.clanName
						},
					'profileImages': {
						'background': user.background,
						'avatar': user.avatarProfileThumb
						},
					'csrf': checkCSRF(request, request.COOKIES),
					'bodyStatus': timeTextDict['onlineStatus'],
					'scriptsStatusDict': None,
					'onlineStatusTextDict': None,
					'currentDateTime': timeStr,
					'requestUserId': id,
					'targetUserId': None,
					'friends': friendsList,
					'friendsStatusDict': statusDict,
					'onlineFriends': friendsOnlineList
					})
			try:
				userProfile = mainUserProfile.objects.get(profileUrl = currentUser)
			except mainUserProfile.DoesNotExist:
				return render(request, 'base/404.html', {'msg': 'Пользователь %s не найден' % currentUser})
			id = str(user.id)
			userProfileId = str(userProfile.id)
			fragments = cachedFragments(username = userProfile.username, profileUrl = currentUser)
			friendsCache = None
			userFriends = None
			friendsIds = None
			friendRequestsCache = None
			friendShipCache = caches['friendShipStatusCache']
			friendShipStatusDict = friendShipCache.get(userProfileId, None)
			if friendShipStatusDict is None:
				friendsIdsCache = caches['friendsIdCache']
				friendsIds = friendsIdsCache.get(userProfileId, None)
				if friendsIds is None:
					friendsCache = caches['friendsCache']
					userFriends = friendsCache.get(userProfileId, None)
					if userFriends is None:
						userFriends = userProfile.friends.all()
						if userFriends.count() == 0:
							userFriends = False
							friendsIds = set()
						else:
							friendsIdsQuerySet = userFriends.values("id")
							friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
						friendsCache.set(userProfileId, userFriends)
					elif userFriends is False:
						friendsIds = set()
					else:
						friendsIdsQuerySet = userFriends.values("id")
						friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
					friendsIdsCache.set(userProfileId, friendsIds)
				if id in friendsIds:
					friendShipStatus = True
				else:
					friendShipStatus = False
					friendRequestsCache = caches['friendshipRequests']
					friendRequests = friendRequestsCache.get(userProfileId, tuple())
					for friendsRequest in friendRequests:
						if friendsRequest[0] == id:
							friendShipStatus = "myRequest"
							break
					if friendShipStatus is False:
						friendRequests = friendRequestsCache.get(id, tuple())
						for friendsRequest in friendRequests:
							if friendsRequest[0] == userProfileId:
								friendShipStatus = "request"
								break
				friendShipCache.set(userProfileId, {id: friendShipStatus})
			else:
				friendShipStatus = friendShipStatusDict.get(id, None)
				if friendShipStatus is None:
					friendsIdsCache = caches['friendsIdCache']
					friendsIds = friendsIdsCache.get(userProfileId, None)
					if friendsIds is None:
						friendsCache = caches['friendsCache']
						userFriends = friendsCache.get(userProfileId, None)
						if userFriends is None:
							userFriends = userProfile.friends.all()
							if userFriends.count() == 0:
								userFriends = False
								friendsIds = set()
							else:
								friendsIdsQuerySet = userFriends.values("id")
								friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
							friendsCache.set(userProfileId, userFriends)
						elif userFriends is False:
							friendsIds = set()
						else:
							friendsIdsQuerySet = userFriends.values("id")
							friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
						friendsIdsCache.set(userProfileId, friendsIds)
					if id in friendsIds:
						friendShipStatus = True
					else:
						friendShipStatus = False
						friendRequestsCache = caches['friendshipRequests']
						friendRequests = friendRequestsCache.get(userProfileId, tuple())
						for friendsRequest in friendRequests:
							if friendsRequest[0] == id:
								friendShipStatus = "myRequest"
								break
						if friendShipStatus is False:
							friendRequests = friendRequestsCache.get(id, tuple())
							for friendsRequest in friendRequests:
								if friendsRequest[0] == userProfileId:
									friendShipStatus = "request"
									break
					friendShipStatusDict.update({id: friendShipStatus})
					friendShipCache.set(userProfileId, friendShipStatusDict)
			privacyCache = caches['privacyCache']
			privacyDict = privacyCache.get(userProfileId, None)
			if privacyDict is None:
				if friendsIds is None:
					if friendsCache is None:
						friendsCache = caches['friendsCache']
					friendsIdsCache = caches['friendsIdCache']
					userFriends = friendsCache.get(userProfileId, None)
					if userFriends is None:
						userFriends = userProfile.friends.all()
						if userFriends.count() == 0:
							userFriends = False
							friendsIds = set()
						else:
							friendsIdsQuerySet = userFriends.values("id")
							friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
						friendsCache.set(userProfileId, userFriends)
					elif userFriends is False:
						friendsIds = set()
					else:
						friendsIdsQuerySet = userFriends.values("id")
						friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
					friendsIdsCache.set(userProfileId, friendsIds)
				privacySettings = userProfilePrivacy.objects.get(user = userProfile)
				privacyChecker = checkPrivacy(id = id, privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True, friendsIdsCache = friendsIdsCache, friendsCache = friendsCache, friendsIds = friendsIds)
				viewProfile = privacyChecker.checkViewProfilePrivacy()
				viewMainInfo = privacyChecker.checkMainInfoPrivacy()
				viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
				viewFriends = privacyChecker.checkFriendsListPrivacy()
				viewFriendsOnline = privacyChecker.checkFriendsOnlineListPrivacy()
				viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
				viewTHPSFriends = privacyChecker.checkTHPSUsersFriends()
				viewTHPSFriendsOnline = privacyChecker.checkTHPSUsersFriendsOnline()
				viewAvatar = privacyChecker.checkAvatarPrivacy()
				sendFriendRequest = privacyChecker.sendFriendRequests()
				sendPersonalMessage = privacyChecker.sendPersonalMessages()
				privacyCache.set(userProfileId, {id: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewFriends': viewFriends, 'viewFriendsOnline': viewFriendsOnline, 'viewTHPSInfo': viewTHPSInfo, 'viewTHPSFriends': viewTHPSFriends, 'viewTHPSFriendsOnline': viewTHPSFriendsOnline, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'sendPersonalMessages': sendPersonalMessage, 'sendFriendRequests': sendFriendRequest}})
			else:
				userPrivacyDict = privacyDict.get(id, None)
				if userPrivacyDict is None:
					if friendsIds is None:
						if friendsCache is None:
							friendsCache = caches['friendsCache']
						friendsIdsCache = caches['friendsIdCache']
						userFriends = friendsCache.get(userProfileId, None)
						if userFriends is None:
							userFriends = userProfile.friends.all()
							if userFriends.count() == 0:
								userFriends = False
								friendsIds = set()
							else:
								friendsIdsQuerySet = userFriends.values("id")
								friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
							friendsCache.set(userProfileId, userFriends)
						elif userFriends is False:
							friendsIds = set()
						else:
							friendsIdsQuerySet = userFriends.values("id")
							friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
						friendsIdsCache.set(userProfileId, friendsIds)
					if friendsCache is None:
						friendsCache = caches['friendsCache']
					privacySettings = userProfilePrivacy.objects.get(user = userProfile)
					privacyChecker = checkPrivacy(id = id, privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True, friendsIdsCache = friendsIdsCache, friendsCache = friendsCache, friendsIds = friendsIds)
					viewProfile = privacyChecker.checkViewProfilePrivacy()
					viewMainInfo = privacyChecker.checkMainInfoPrivacy()
					viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
					viewFriends = privacyChecker.checkFriendsListPrivacy()
					viewFriendsOnline = privacyChecker.checkFriendsOnlineListPrivacy()
					viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
					viewTHPSFriends = privacyChecker.checkTHPSUsersFriends()
					viewTHPSFriendsOnline = privacyChecker.checkTHPSUsersFriendsOnline()
					viewAvatar = privacyChecker.checkAvatarPrivacy()
					sendFriendRequest = privacyChecker.sendFriendRequests()
					sendPersonalMessage = privacyChecker.sendPersonalMessages()
					privacyDict.update({id: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewFriends': viewFriends, 'viewFriendsOnline': viewFriendsOnline, 'viewTHPSInfo': viewTHPSInfo, 'viewTHPSFriends': viewTHPSFriends, 'viewTHPSFriendsOnline': viewTHPSFriendsOnline, 'viewAvatar': viewAvatar, 'viewOnlineStatus': viewOnlineStatus, 'sendPersonalMessages': sendPersonalMessage, 'sendFriendRequests': sendFriendRequest}})
					privacyCache.set(userProfileId, privacyDict)
				else:
					viewProfile = userPrivacyDict.get('viewProfile', None)
					viewMainInfo = userPrivacyDict.get('viewMainInfo', None)
					viewTHPSInfo = userPrivacyDict.get('viewTHPSInfo', None)
					viewTHPSFriends = userPrivacyDict.get('viewTHPSFriends', None)
					viewTHPSFriendsOnline = userPrivacyDict.get('viewTHPSFriendsOnline', None)
					viewOnlineStatus = userPrivacyDict.get('viewOnlineStatus', None)
					viewAvatar = userPrivacyDict.get('viewAvatar', None)
					viewFriends = userPrivacyDict.get('viewFriends', None)
					viewFriendsOnline = userPrivacyDict.get('viewFriendsOnline', None)
					sendFriendRequest = userPrivacyDict.get('sendFriendRequests', None)
					sendPersonalMessage = userPrivacyDict.get('sendPersonalMessages', None)
					if viewProfile is None or viewMainInfo is None or viewFriends is None or viewFriendsOnline is None or viewTHPSInfo is None or viewTHPSFriends is None or viewTHPSFriendsOnline is None or viewOnlineStatus is None or viewAvatar is None or sendFriendRequest is None or sendPersonalMessage is None:
						if friendsIds is None:
							if friendsCache is None:
								friendsCache = caches['friendsCache']
							friendsIdsCache = caches['friendsIdCache']
							userFriends = friendsCache.get(userProfileId, None)
							if userFriends is None:
								userFriends = userProfile.friends.all()
								if userFriends.count() == 0:
									userFriends = False
									friendsIds = set()
								else:
									friendsIdsQuerySet = userFriends.values("id")
									friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
								friendsCache.set(userProfileId, userFriends)
							elif userFriends is False:
								friendsIds = set()
							else:
								friendsIdsQuerySet = userFriends.values("id")
								friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
							friendsIdsCache.set(userProfileId, friendsIds)
						if friendsCache is None:
							friendsCache = caches['friendsCache']
						privacySettings = userProfilePrivacy.objects.get(user = userProfile)
						privacyChecker = checkPrivacy(id = id, privacy = privacySettings, isFriend = friendShipStatus is True or friendShipStatus == "request", isAuthenticated = True, friendsIdsCache = friendsIdsCache, friendsCache = friendsCache, friendsIds = friendsIds)
						if viewProfile is None:
							viewMainInfo = privacyChecker.checkMainInfoPrivacy()
							userPrivacyDict.update({'viewProfile': viewProfile})
						if viewMainInfo is None:
							viewMainInfo = privacyChecker.checkMainInfoPrivacy()
							userPrivacyDict.update({'viewMainInfo': viewMainInfo})
						if viewFriends is None:
							viewFriends = privacyChecker.checkFriendsListPrivacy()
							userPrivacyDict.update({'viewFriends': viewFriends})
						if viewFriendsOnline is None:
							viewFriendsOnline = privacyChecker.checkFriendsListPrivacy()
							userPrivacyDict.update({'viewFriendsOnline': viewFriendsOnline})
						if viewTHPSInfo is None:
							viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
							userPrivacyDict.update({'viewTHPSInfo': viewTHPSInfo})
						if viewTHPSFriends is None:
							viewTHPSFriends = privacyChecker.checkTHPSUsersFriends()
							userPrivacyDict.update({'viewTHPSFriends': viewTHPSFriends})
						if viewTHPSFriendsOnline is None:
							viewTHPSFriendsOnline = privacyChecker.checkTHPSUsersFriendsOnline()
							userPrivacyDict.update({'viewTHPSFriendsOnline': viewTHPSFriendsOnline})
						if viewOnlineStatus is None:
							viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
							userPrivacyDict.update({'viewOnlineStatus': viewOnlineStatus})
						if viewAvatar is None:
							viewAvatar = privacyChecker.checkAvatarPrivacy()
							userPrivacyDict.update({'viewAvatar': viewAvatar})
						if sendFriendRequest is None:
							sendFriendRequest = privacyChecker.sendFriendRequests()
							userPrivacyDict.update({'sendFriendRequests': sendFriendRequest})
						if sendPersonalMessage is None:
							sendPersonalMessage = privacyChecker.sendPersonalMessages()
							userPrivacyDict.update({'sendPersonalMessages': sendPersonalMessage})
						privacyCache.set(userProfileId, privacyDict)
			if viewProfile is False:
				if request.is_ajax():
					if 'global' in request.GET:
						return JsonResponse({"newPage": json.dumps({
							'newTemplate': rts('psixiSocial/psixiSocial.html', {
								'parentTemplate': 'base/classicPage/emptyParent.html',
								'content': ('psixiSocial', 'profile', 'hidden'),
								'menu': fragments.menuGetter('type2Serialized'),
								'text': singleElementGetter(lang = 'ru').getString('userProfileIsHidden'),
								'slug': currentUser,
								'username': userProfile.username,
								'sendFriendRequestPrivacy': sendFriendRequest
								}),
							'url': request.path,
							'titl': 'Профиль %s' % userProfile.username
							})})
					return JsonResponse({"newPage": json.dumps({
						'content': ('psixiSocial', 'profile', 'hidden'),
						'menu': fragments.menuGetter('type2'),
						'templateBody': rts('psixiSocial/body/profile/hidden.html', {
							'text': singleElementGetter(lang = 'ru').getString('userProfileIsHidden'),
							'slug': currentUser,
							'username': userProfile.username,
							'sendFriendRequestPrivacy': sendFriendRequest
							}),
						'url': request.path,
						'titl': 'Профиль %s' % userProfile.username
						})})
				return render(request, 'psixiSocial/psixiSocial.html', {
					'parentTemplate': 'base/classicPage/base.html',
					'content': ('psixiSocial', 'profile', 'hidden'),
					'menu': fragments.menuGetter('type2Serialized'),
					'text': singleElementGetter(lang = 'ru').getString('userProfileIsHidden'),
					'slug': currentUser,
					'username': userProfile.username,
					'sendFriendRequestPrivacy': sendFriendRequest,
					'titl': 'Профиль %s' % userProfile.username
					})
			onlineStatusCache = None
			iffyStatusCache = None
			if viewOnlineStatus or viewFriendsOnline or viewTHPSFriends or viewTHPSFriendsOnline:
				timeStr = str(datetime.now())
				if viewFriends:
					if userFriends is None:
						if friendsCache is None:
							friendsCache = caches['friendsCache']
						userFriends = friendsCache.get(userProfileId, None)
						if userFriends is None:
							userFriends = userProfile.friends.all()
							if userFriends.count() > 0:
								if friendsIds is None:
									friendsIdsCache = caches['friendsIdCache']
									friendsIds = friendsIdsCache.get(userProfileId, None)
									if friendsIds is None:
										friendsIdsQuerySet = userFriends.values("id")
										friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
										friendsIdsCache.set(userProfileId, friendsIds)
								thumbsCache = caches['smallThumbs']
								if friendRequestsCache is None:
									friendRequestsCache = caches['friendshipRequests']
								onlineStatusCache = caches['onlineUsers']
								iffyStatusCache = caches['iffyUsersOnlineStatus']
								onlineUsers = onlineStatusCache.get('online', set())
								statusDict = {}
								friendsList, statusDict = getUserFriends(id, userFriends, friendsIds, friendsCache, friendsIdsCache, friendShipCache, friendRequestsCache, thumbsCache, privacyCache, onlineUsers, iffyStatusCache, statusDict)
								if viewFriendsOnline or viewTHPSFriendsOnline:
									if viewFriendsOnline:
										friendsOnlineList, statusDict = getUserFriendsOnline(id, userFriends, friendsIds, friendsCache, friendsIdsCache, friendShipCache, friendRequestsCache, thumbsCache, privacyCache, onlineUsers, iffyStatusCache, statusDict)
									else:
										friendsOnlineList, statusDict = None, None
								else:
									friendsOnlineList, statusDict = None, None
								friendsCache.set(userProfileId, userFriends)
							else:
								friendsCache.set(userProfileId, False)
								friendsList, friendsOnlineList, statusDict = None, None, None
						elif userFriends is False:
							friendsList, friendsOnlineList, statusDict = None, None, None
						else:
							if friendsIds is None:
								friendsIdsCache = caches['friendsIdCache']
								friendsIds = friendsIdsCache.get(userProfileId, None)
								if friendsIds is None:
									friendsIdsQuerySet = userFriends.values("id")
									friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
									friendsIdsCache.set(userProfileId, friendsIds)
							thumbsCache = caches['smallThumbs']
							if friendRequestsCache is None:
								friendRequestsCache = caches['friendshipRequests']
							onlineStatusCache = caches['onlineUsers']
							iffyStatusCache = caches['iffyUsersOnlineStatus']
							onlineUsers = onlineStatusCache.get('online', set())
							statusDict = {}
							friendsList, statusDict = getUserFriends(id, userFriends, friendsIds, friendsCache, friendsIdsCache, friendShipCache, friendRequestsCache, thumbsCache, privacyCache, onlineUsers, iffyStatusCache, statusDict)
							if viewFriendsOnline or viewTHPSFriendsOnline:
								if viewFriendsOnline:
									friendsOnlineList, statusDict = getUserFriendsOnline(id, userFriends, friendsIds, friendsCache, friendsIdsCache, friendShipCache, friendRequestsCache, thumbsCache, privacyCache, onlineUsers, iffyStatusCache, statusDict)
								else:
									friendsOnlineList, statusDict = None, None
							else:
								friendsOnlineList, statusDict = None, None
					elif userFriends is False:
						friendsList, friendsOnlineList, statusDict = None, None, None
					else:
						if friendsIds is None:
							friendsIdsCache = caches['friendsIdCache']
							friendsIds = friendsIdsCache.get(userProfileId, None)
							if friendsIds is None:
								friendsIdsQuerySet = userFriends.values("id")
								friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
								friendsIdsCache.set(userProfileId, friendsIds)
						thumbsCache = caches['smallThumbs']
						if friendRequestsCache is None:
							friendRequestsCache = caches['friendshipRequests']
						onlineStatusCache = caches['onlineUsers']
						iffyStatusCache = caches['iffyUsersOnlineStatus']
						onlineUsers = onlineStatusCache.get('online', set())
						statusDict = {}
						friendsList, statusDict = getUserFriends(id, userFriends, friendsIds, friendsCache, friendsIdsCache, friendShipCache, friendRequestsCache, thumbsCache, privacyCache, onlineUsers, iffyStatusCache, statusDict)
						if viewFriendsOnline or viewTHPSFriendsOnline:
							if viewFriendsOnline:
								friendsOnlineList, statusDict = getUserFriendsOnline(id, userFriends, friendsIds, friendsCache, friendsIdsCache, friendShipCache, friendRequestsCache, thumbsCache, privacyCache, onlineUsers, iffyStatusCache, statusDict)
							else:
								friendsOnlineList, statusDict = None, None
						else:
							friendsOnlineList, statusDict = None, None
				else:
					friendsList, friendsOnlineList, statusDict = "hidden", None, None
				if viewOnlineStatus:
					timeTextDict = profileTimeStatusText(lang = 'ru').getDict()
					if iffyStatusCache is None:
						iffyStatusCache = caches['iffyUsersOnlineStatus']
					userIsIffy = iffyStatusCache.get(userProfileId, False)
					if userIsIffy is not False:
						statusBody = timeTextDict['uknownStatusNoJavascript']
						statusScripts = {'iffy': userIsIffy[0]}
					else:
						if onlineStatusCache is None:
							onlineStatusCache = caches['onlineUsers']
						if onlineStatusCache.get(userProfileId, False):
							statusScripts = '"online"'
							statusBody = timeTextDict['onlineStatus']
						else:
							lastOnlineStatusCache = caches['lastVisit']
							lastVisitTime = lastOnlineStatusCache.get(userProfileId, '')
							statusScripts = {'lastVisitTime': str(lastVisitTime)}
							statusBody = {'lastVisitTime': lastVisitTime, 'text': timeTextDict['offlineStatus']}
				else:
					timeTextDict, statusBody, statusScripts, lastVisitTime, timeStr = None, None, None, None, None
			else:
				timeTextDict, friendsList, friendsOnlineList, statusDict, statusBody, statusScripts, lastVisitTime, timeStr = None, "hidden", None, None, None, None, None, None
			try:
				thpsProfile = THPSProfile.objects.get(pk = userProfile)
			except:
				thpsProfile = None
			if thpsProfile is None:
				if request.is_ajax():
					if 'global' in request.GET:
						return JsonResponse({'newPage': json.dumps({
							'newTemplate': rts('psixiSocial/psixiSocial.html', {
								'parentTemplate': 'base/classicPage/emptyParent.html',
								'content': ('psixiSocial', 'profile', 'show', '', ''),
								'menu': fragments.menuGetter('type2Serialized'),
								'profileIsMine': False,
								'isFriend': friendShipStatus,
								'profileMainInfo': {
									'id': userProfileId,
									'slug': currentUser,
									'username': userProfile.username,
									'firstname': userProfile.first_name,
									'lastname': userProfile.last_name,
									'country': userProfile.country,
									'region': userProfile.subRegion,
									'city': userProfile.city,
									'birthdate': userProfile.birthDate
									},
								'profileContacts': {
									'email': userProfile.email
									},
								'profileTHPSInfo': None,
								'profileImages': {
									'background': userProfile.background,
									'avatar': userProfile.avatarProfileThumb
									},
								'bodyStatus': statusBody,
								'scriptsStatusDict': statusScripts,
								'csrf': '',
								'onlineStatusTextDict': timeTextDict,
								'requestUserId': id,
								'targetUserId': userProfileId,
								'currentDateTime': timeStr,
								'friends': friendsList,
								'friendsStatusDict': statusDict,
								'onlineFriends': friendsOnlineList,
								'sendFriendRequestPrivacy': sendFriendRequest
								}),
							'url': reverse('profile', kwargs = {'currentUser': currentUser}),
							'titl': 'Профиль %s' % userProfile.username
							})})
					return JsonResponse({'newPage': json.dumps({
						'content': ('psixiSocial', 'profile', 'show', '', ''),
						'menu': fragments.menuGetter('type2'),
						'templateHead': rts('psixiSocial/head/profile.html'),
						'templateBody': rts('psixiSocial/body/profile/profile.html', {
						'profileIsMine': False,
						'isFriend': friendShipStatus,
						'profileMainInfo': {
							'id': userProfileId,
							'slug': currentUser,
							'username': userProfile.username,
							'firstname': userProfile.first_name,
							'lastname': userProfile.last_name,
							'country': userProfile.country,
							'region': userProfile.subRegion,
							'city': userProfile.city,
							'birthdate': userProfile.birthDate
							},
						'profileContacts': {
							'email': userProfile.email
							},
						'profileTHPSInfo': None,
						'profileImages': {
							'background': userProfile.background,
							'avatar': userProfile.avatarProfileThumb
							},
						'bodyStatus': statusBody,
						'csrf': '',
						'friends': friendsList,
						'onlineFriends': friendsOnlineList,
						'sendFriendRequestPrivacy': sendFriendRequest
						}),
						'templateScripts': rts('psixiSocial/scripts/profile.html', {
							'onlineStatusTextDict': timeTextDict,
							'requestUserId': id,
							'targetUserId': userProfileId,
							'scriptsStatusDict': statusScripts,
							'currentDateTime': timeStr,
							'friendsStatusDict': statusDict
							}),
						'url': reverse('profile', kwargs = {'currentUser': currentUser})
						})})
				return render(request, 'psixiSocial/psixiSocial.html', {
					'parentTemplate': 'base/classicPage/base.html',
					'content': ('psixiSocial', 'profile', 'show', '', ''),
					'menu': fragments.menuGetter('type2Serialized'),
					'profileIsMine': False,
					'isFriend': friendShipStatus,
					'profileMainInfo': {
						'id': userProfileId,
						'slug': currentUser,
						'username': userProfile.username,
						'firstname': userProfile.first_name,
						'lastname': userProfile.last_name,
						'country': userProfile.country,
						'region': userProfile.subRegion,
						'city': userProfile.city,
						'birthdate': userProfile.birthDate
						},
					'profileContacts': {
						'email': userProfile.email
						},
					'profileTHPSInfo': None,
					'profileImages': {
						'background': userProfile.background,
						'avatar': userProfile.avatarProfileThumb
						},
					'bodyStatus': statusBody,
					'scriptsStatusDict': statusScripts,
					'csrf': checkCSRF(request, request.COOKIES),
					'onlineStatusTextDict': timeTextDict,
					'requestUserId': id,
					'targetUserId': userProfileId,
					'currentDateTime': timeStr,
					'friends': friendsList,
					'friendsStatusDict': statusDict,
					'onlineFriends': friendsOnlineList,
					'sendFriendRequestPrivacy': sendFriendRequest
					})
			if request.is_ajax():
				if 'global' in request.GET:
					return JsonResponse({'newPage': json.dumps({
						'newTemplate': rts('psixiSocial/psixiSocial.html', {
							'parentTemplate': 'base/classicPage/emptyParent.html',
							'content': ('psixiSocial', 'profile', 'show', '', 'thps'),
							'menu': fragments.menuGetter('type2Serialized'),
							'profileIsMine': False,
							'isFriend': friendShipStatus,
							'profileMainInfo': {
								'id': userProfileId,
								'slug': currentUser,
								'username': userProfile.username,
								'firstname': userProfile.first_name,
								'lastname': userProfile.last_name,
								'country': userProfile.country,
								'region': userProfile.subRegion,
								'city': userProfile.city,
								'birthdate': userProfile.birthDate
								},
							'profileContacts': {
								'email': userProfile.email
								},
							'profileTHPSInfo': {
								'nickname': thpsProfile.nickName,
								'clan': thpsProfile.clanName
								},
							'profileImages': {
								'background': userProfile.background,
								'avatar': userProfile.avatarProfileThumb
								},
							'bodyStatus': statusBody,
							'scriptsStatusDict': statusScripts,
							'csrf': '',
							'onlineStatusTextDict': timeTextDict,
							'requestUserId': id,
							'targetUserId': userProfileId,
							'currentDateTime': timeStr,
							'friends': friendsList,
							'friendsStatusDict': statusDict,
							'onlineFriends': friendsOnlineList,
							'sendFriendRequestPrivacy': sendFriendRequest
						}),
						'url': reverse('profile', kwargs = {'currentUser': currentUser}),
						'titl': 'Профиль %s' % userProfile.username
						})})
				return JsonResponse({'newPage': json.dumps({
					'content': ('psixiSocial', 'profile', 'show', '', 'thps'),
					'menu': fragments.menuGetter('type2'),
					'templateHead': rts('psixiSocial/head/profile.html'),
					'templateBody': rts('psixiSocial/body/profile/profileThpsInfo.html', {
						'profileIsMine': False,
						'isFriend': friendShipStatus,
						'profileMainInfo': {
							'id': userProfileId,
							'slug': currentUser,
							'username': userProfile.username,
							'firstname': userProfile.first_name,
							'lastname': userProfile.last_name,
							'country': userProfile.country,
							'region': userProfile.subRegion,
							'city': userProfile.city,
							'birthdate': userProfile.birthDate
							},
						'profileContacts': {
							'email': userProfile.email
							},
						'profileTHPSInfo': {
							'nickname': thpsProfile.nickName,
							'clan': thpsProfile.clanName
							},
						'profileImages': {
							'background': userProfile.background,
							'avatar': userProfile.avatarProfileThumb
							},
						'bodyStatus': statusBody,
						'csrf': '',
						'friends': friendsList,
						'onlineFriends': friendsOnlineList,
						'sendFriendRequestPrivacy': sendFriendRequest
						}),
					'templateScripts': rts('psixiSocial/scripts/profile.html', {
						'onlineStatusTextDict': timeTextDict,
						'requestUserId': id,
						'targetUserId': userProfileId,
						'scriptsStatusDict': statusScripts,
						'currentDateTime': timeStr,
						'friendsStatusDict': statusDict,
						}),
					'url': reverse('profile', kwargs = {'currentUser': currentUser})
					})})
			return render(request, 'psixiSocial/psixiSocial.html', {
				'parentTemplate': 'base/classicPage/base.html',
				'content': ('psixiSocial', 'profile', 'show', '', 'thps'),
				'menu': fragments.menuGetter('type2Serialized'),
				'profileIsMine': False,
				'isFriend': friendShipStatus,
				'profileMainInfo': {
					'id': userProfileId,
					'slug': currentUser,
					'username': userProfile.username,
					'firstname': userProfile.first_name,
					'lastname': userProfile.last_name,
					'country': userProfile.country,
					'region': userProfile.subRegion,
					'city': userProfile.city,
					'birthdate': userProfile.birthDate
					},
				'profileContacts': {
					'email': userProfile.email
					},
				'profileTHPSInfo': {
					'nickname': thpsProfile.nickName,
					'clan': thpsProfile.clanName
					},
				'profileImages': {
					'background': userProfile.background,
					'avatar': userProfile.avatarProfileThumb
					},
				'bodyStatus': statusBody,
				'scriptsStatusDict': statusScripts,
				'csrf': checkCSRF(request, request.COOKIES),
				'onlineStatusTextDict': timeTextDict,
				'requestUserId': id,
				'targetUserId': userProfileId,
				'currentDateTime': timeStr,
				'friends': friendsList,
				'friendsStatusDict': statusDict,
				'onlineFriends': friendsOnlineList,
				'sendFriendRequestPrivacy': sendFriendRequest
				})
		try:
			userProfile = mainUserProfile.objects.get(profileUrl = currentUser)
		except mainUserProfile.DoesNotExist:
			return render(request, 'base/404.html', {'msg': 'Пользователь %s не найден' % currentUser})
		userProfileId = str(userProfile.id)
		userProfileSlug = userProfile.profileUrl
		privacyCache = caches['privacyCache']
		privacyDictGuest = privacyCache.get(userProfileId, None)
		if privacyDictGuest is None:
			privacySettings = userProfilePrivacy.objects.get(user = userProfile)
			privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = False)
			viewProfile = privacyChecker.checkViewProfilePrivacy()
			viewMainInfo = privacyChecker.checkMainInfoPrivacy()
			viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
			viewFriends = privacyChecker.checkFriendsListPrivacy()
			viewFriendsOnline = privacyChecker.checkFriendsOnlineListPrivacy()
			viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
			viewAvatar = privacyChecker.checkAvatarPrivacy()
			viewTHPSFriends = privacyChecker.checkTHPSUsersFriends()
			viewTHPSFriendsOnline = privacyChecker.checkTHPSUsersFriendsOnline()
			privacyCache.set(userProfileId, {0: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewAvatar': viewAvatar, 'viewFriends': viewFriends, 'viewFriendsOnline': viewFriendsOnline, 'viewTHPSFriends': viewTHPSFriends, 'viewTHPSFriendsOnline': viewTHPSFriendsOnline, 'viewOnlineStatus': viewOnlineStatus}})
		else:
			privacyDict = privacyDictGuest.get(0, None)
			if privacyDict is None:
				privacySettings = userProfilePrivacy.objects.get(user = userProfile)
				privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = False)
				viewProfile = privacyChecker.checkViewProfilePrivacy()
				viewMainInfo = privacyChecker.checkMainInfoPrivacy()
				viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
				viewFriends = privacyChecker.checkFriendsListPrivacy()
				viewFriendsOnline = privacyChecker.checkFriendsOnlineListPrivacy()
				viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
				viewAvatar = privacyChecker.checkAvatarPrivacy()
				viewTHPSFriends = privacyChecker.checkTHPSUsersFriends()
				viewTHPSFriendsOnline = privacyChecker.checkTHPSUsersFriendsOnline()
				privacyDictGuest.update({0: {'viewProfile': viewProfile, 'viewMainInfo': viewMainInfo, 'viewTHPSInfo': viewTHPSInfo, 'viewFriends': viewFriends, 'viewAvatar': viewAvatar, 'viewFriendsOnline': viewFriendsOnline, 'viewTHPSFriends': viewTHPSFriends, 'viewTHPSFriendsOnline': viewTHPSFriendsOnline, 'viewOnlineStatus': viewOnlineStatus}})
				privacyCache.set(userProfileId, privacyDictGuest)
			else:
				viewProfile = privacyDict.get('viewProfile', None)
				viewMainInfo = privacyDict.get('viewMainInfo', None)
				viewFriends = privacyDict.get('viewFriends', None)
				viewFriendsOnline = privacyDict.get('viewFriendsOnline', None)
				viewOnlineStatus = privacyDict.get('viewOnlineStatus', None)
				viewTHPSInfo = privacyDict.get('viewTHPSInfo', None)
				viewAvatar = privacyDict.get('viewAvatar', None)
				viewTHPSFriends = privacyDict.get('viewTHPSFriends', None)
				viewTHPSFriendsOnline = privacyDict.get('viewTHPSFriendsOnline', None)
				if viewProfile is None or viewMainInfo is None or viewFriends is None or viewFriendsOnline is None or viewTHPSInfo is None or viewAvatar is None or viewTHPSFriends is None or viewTHPSFriendsOnline is None or viewOnlineStatus is None:
					privacySettings = userProfilePrivacy.objects.get(user = userProfile)
					privacyChecker = checkPrivacy(privacy = privacySettings, isFriend = False, isAuthenticated = False)
					if viewProfile is None:
						viewProfile = privacyChecker.checkViewProfilePrivacy()
						privacyDict.update({'viewProfile': viewProfile})
					if viewMainInfo is None:
						viewMainInfo = privacyChecker.checkMainInfoPrivacy()
						privacyDict.update({'viewMainInfo': viewMainInfo})
					if viewFriends is None:
						viewFriends = privacyChecker.checkFriendsListPrivacy()
						privacyDict.update({'viewFriends': viewFriends})
					if viewFriendsOnline is None:
						viewFriendsOnline = privacyChecker.checkFriendsOnlineListPrivacy()
						privacyDict.update({'viewFriendsOnline': viewFriendsOnline})
					if viewTHPSInfo is None:
						viewTHPSInfo = privacyChecker.checkTHPSInfoPrivacy()
						privacyDict.update({'viewTHPSInfo': viewTHPSInfo})
					if viewAvatar is None:
						viewAvatar = privacyChecker.checkAvatarPrivacy()
						privacyDict.update({'viewAvatar': viewAvatar})
					if viewTHPSFriends is None:
						viewTHPSFriends = privacyChecker.checkTHPSUsersFriends()
						privacyDict.update({'viewTHPSFriends': viewTHPSFriends})
					if viewTHPSFriendsOnline is None:
						viewTHPSFriendsOnline = privacyChecker.checkTHPSUsersFriendsOnline()
						privacyDict.update({'viewTHPSFriendsOnline': viewTHPSFriendsOnline})
					if viewOnlineStatus is None:
						viewOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
						privacyDict.update({'viewOnlineStatus': viewOnlineStatus})
					privacyCache.set(userProfileId, privacyDictGuest)
		fragments = cachedFragments(username = userProfile.username, profileUrl = userProfileSlug)
		timeTextDict = profileTimeStatusText(lang = 'ru').getDict()
		if viewProfile is False:
			if request.is_ajax():
				if 'global' in request.GET:
					return JsonResponse({"newPage": json.dumps({
						'newTemplate': rts('psixiSocial/psixiSocial.html', {
							'parentTemplate': 'base/classicPage/emptyParent.html',
							'content': ('psixiSocial', 'profile', 'hidden'),
							'menu': fragments.menuGetter('type3Serialized'),
							'text': singleElementGetter(lang = 'ru').getString('userProfileIsHidden'),
							'slug': currentUser,
							'username': userProfile.username
							}),
						'url': request.path,
						'titl': 'Профиль %s' % userProfile.username
						})})
				return JsonResponse({"newPage": json.dumps({
					'content': ('psixiSocial', 'profile', 'hidden'),
					'menu': fragments.menuGetter('type3'),
					'templateBody': rts('psixiSocial/body/profile/hidden.html', {
						'text': singleElementGetter(lang = 'ru').getString('userProfileIsHidden'),
						'slug': currentUser,
						'username': userProfile.username
						}),
					'url': request.path,
					'titl': 'Профиль %s' % userProfile.username
					})})
			return render(request, 'psixiSocial/psixiSocial.html', {
				'parentTemplate': 'base/classicPage/base.html',
				'content': ('psixiSocial', 'profile', 'hidden'),
				'menu': fragments.menuGetter('type3Serialized'),
				'text': singleElementGetter(lang = 'ru').getString('userProfileIsHidden'),
				'slug': currentUser,
				'username': userProfile.username,
				'titl': 'Профиль %s' % userProfile.username
				})
		if viewFriends or viewOnlineStatus:
			timeStr = str(datetime.now())
			if viewFriends:
				friendsCache = caches['friendsCache']
				friends = friendsCache.get(userProfileId, None)
				if friends is None:
					friends = userProfile.friends.all()
					if friends.count() > 0:
						thumbsCache = caches['smallThumbs']
						friendsCacheId = caches['friendsIdCache']
						friendsIds = friendsCacheId.get(userProfileId, None)
						if friendsIds is None:
							friendsIdsQuerySet = friends.values("id")
							friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
							friendsCacheId.set(userProfileId, friendsIds)
						onlineStatusCache = caches['onlineUsers']
						iffyStatusCache = caches['iffyUsersOnlineStatus']
						onlineUsers = onlineStatusCache.get('online', set())
						statusDict = {}
						friendsList, statusDict = getUserFriendsGuest(friends, friendsIds, thumbsCache, privacyCache, onlineUsers, iffyStatusCache, statusDict)
						friendsOnlineList, statusDict = getUserFriendsOnlineGuest(friends, friendsIds, thumbsCache, privacyCache, onlineUsers, iffyCache, statusDict)
						friendsCache.set(userProfileId, friends)
					else:
						friendsList, friendsOnlineList, statusDict = None, None, None
				else:
					if friends is not False:
						thumbsCache = caches['smallThumbs']
						friendsCacheId = caches['friendsIdCache']
						friendsIds = friendsCacheId.get(userProfileId, None)
						if friendsIds is None:
							friendsIdsQuerySet = friends.values("id")
							friendsIds = {str(user.get("id")) for user in friendsIdsQuerySet}
							friendsCacheId.set(userProfileId, friendsIds)
						onlineStatusCache = caches['onlineUsers']
						iffyStatusCache = caches['iffyUsersOnlineStatus']
						onlineUsers = onlineStatusCache.get('online', set())
						statusDict = {}
						friendsList, statusDict = getUserFriendsGuest(friends, friendsIds, thumbsCache, privacyCache, onlineUsers, iffyStatusCache, statusDict)
						friendsOnlineList, statusDict = getUserFriendsOnlineGuest(friends, friendsIds, thumbsCache, privacyCache, onlineUsers, iffyStatusCache, statusDict)
					else:
						friendsList, friendsOnlineList, statusDict = None, None, None
			else:
				friendsList, friendsOnlineList, statusDict = "hidden", None, None
			userIsIffy = iffyStatusCache.get(userProfileId, False)
			if viewOnlineStatus:
				if userIsIffy is not False:
					statusBody = timeTextDict['uknownStatusNoJavascript']
					statusScripts = {'iffy': userIsIffy[0]}
				else:
					if onlineStatusCache.get(userProfileId, False):
						statusScripts = '"online"'
						statusBody = timeTextDict['onlineStatus']
					else:
						lastOnlineStatusCache = caches['lastVisit']
						lastVisitTime = lastOnlineStatusCache.get(userProfileId, '')
						statusScripts = {'lastVisitTime': str(lastVisitTime)}
						statusBody = {'lastVisitTime': lastVisitTime, 'text': timeTextDict['offlineStatus']}
			else:
				statusBody, statusScripts, lastVisitTime = None, None, None
			timeStr = str(datetime.now())
		else:
			friendsOnlineList, statusDict, statusBody, statusScripts, lastVisitTime, friendsList, timeStr = None, None, None, None, None, "hidden", None
		try:
			thpsProfile = THPSProfile.objects.get(pk = userProfile)
		except:
			thpsProfile = None
		if thpsProfile is None:
			if request.is_ajax():
				if 'global' in request.GET:
					return JsonResponse({'newPage': json.dumps({
						'newTemplate': rts('psixiSocial/psixiSocial.html', {
							'parentTemplate': 'base/classicPage/emptyParent.html',
							'content': ('psixiSocial', 'profile', 'show', '', ''),
							'menu': fragments.menuGetter('type3Serialized'),
							'profileIsMine': False,
							'isFriend': 'Guest',
							'profileMainInfo': {
								'id': userProfileId,
								'url': userProfileSlug,
								'username': userProfile.username,
								'firstname': userProfile.first_name,
								'lastname': userProfile.last_name,
								'country': userProfile.country,
								'region': userProfile.subRegion,
								'city': userProfile.city,
								'birthdate': userProfile.birthDate
								},
							'profileContacts': {
								'email': userProfile.email
								},
							'profileTHPSInfo': None,
							'profileImages': {
								'background': userProfile.background,
								'avatar': userProfile.avatarProfileThumb
								},
							'bodyStatus': statusBody,
							'scriptsStatusDict': statusScripts,
							'csrf': '',
							'onlineStatusTextDict': timeTextDict,
							'requestUserId': None,
							'targetUserId': userProfileId,
							'currentDateTime': timeStr,
							'friends': friendsList,
							'friendsStatusDict': statusDict,
							'onlineFriends': friendsOnlineList
							}),
						'url': reverse('profile', kwargs = {'currentUser': currentUser}),
						'titl': 'Профиль %s' % userProfile.username
						})})
				return JsonResponse({'newPage': json.dumps({
					'content': ('psixiSocial', 'profile', 'show', '', ''),
					'menu': fragments.menuGetter('type3'),
					'templateHead': rts('psixiSocial/head/profile.html'),
					'templateBody': rts('psixiSocial/body/profile/profile.html', {
					'profileIsMine': False,
					'isFriend': 'Guest',
					'profileMainInfo': {
						'id': userProfileId,
						'url': userProfileSlug,
						'username': userProfile.username,
						'firstname': userProfile.first_name,
						'lastname': userProfile.last_name,
						'country': userProfile.country,
						'region': userProfile.subRegion,
						'city': userProfile.city,
						'birthdate': userProfile.birthDate
						},
					'profileContacts': {
						'email': userProfile.email
						},
					'profileTHPSInfo': None,
					'profileImages': {
						'background': userProfile.background,
						'avatar': userProfile.avatarProfileThumb
						},
					'bodyStatus': statusBody,
					'csrf': '',
					'friends': friendsList,
					'onlineFriends': friendsOnlineList
					}),
					'templateScripts': rts('psixiSocial/scripts/profile.html', {
						'onlineStatusTextDict': timeTextDict,
						'requestUserId': None,
						'targetUserId': userProfileId,
						'scriptsStatusDict': statusScripts,
						'currentDateTime': timeStr,
						'friendsStatusDict': statusDict
						}),
					'url': reverse('profile', kwargs = {'currentUser': currentUser})
					})})
			return render(request, 'psixiSocial/psixiSocial.html', {
				'parentTemplate': 'base/classicPage/base.html',
				'content': ('psixiSocial', 'profile', 'show', '', ''),
				'menu': fragments.menuGetter('type3Serialized'),
				'profileIsMine': False,
				'isFriend': 'Guest',
				'profileMainInfo': {
					'id': userProfileId,
					'url': userProfileSlug,
					'username': userProfile.username,
					'firstname': userProfile.first_name,
					'lastname': userProfile.last_name,
					'country': userProfile.country,
					'region': userProfile.subRegion,
					'city': userProfile.city,
					'birthdate': userProfile.birthDate
					},
				'profileContacts': {
					'email': userProfile.email
					},
				'profileTHPSInfo': None,
				'profileImages': {
					'background': userProfile.background,
					'avatar': userProfile.avatarProfileThumb
					},
				'bodyStatus': statusBody,
				'scriptsStatusDict': statusScripts,
				'csrf': checkCSRF(request, request.COOKIES),
				'onlineStatusTextDict': timeTextDict,
				'requestUserId': None,
				'targetUserId': userProfileId,
				'currentDateTime': timeStr,
				'friends': friendsList,
				'friendsStatusDict': statusDict,
				'onlineFriends': friendsOnlineList
				})
		if request.is_ajax():
			if 'global' in request.GET:
				return JsonResponse({'newPage': json.dumps({
					'newTemplate': rts('psixiSocial/psixiSocial.html', {
						'parentTemplate': 'base/classicPage/emptyParent.html',
						'content': ('psixiSocial', 'profile', 'show', '', 'thps'),
						'menu': fragments.menuGetter('type3Serialized'),
						'profileIsMine': False,
						'isFriend': 'Guest',
						'profileMainInfo': {
							'id': userProfileId,
							'url': userProfileSlug,
							'username': userProfile.username,
							'firstname': userProfile.first_name,
							'lastname': userProfile.last_name,
							'country': userProfile.country,
							'region': userProfile.subRegion,
							'city': userProfile.city,
							'birthdate': userProfile.birthDate
							},
						'profileContacts': {
							'email': userProfile.email
							},
						'profileTHPSInfo': {
							'nickname': thpsProfile.nickName,
							'clan': thpsProfile.clanName
							},
						'profileImages': {
							'background': userProfile.background,
							'avatar': userProfile.avatarProfileThumb
							},
						'bodyStatus': statusBody,
						'scriptsStatusDict': statusScripts,
						'csrf': '',
						'onlineStatusTextDict': timeTextDict,
						'requestUserId': None,
						'targetUserId': userProfileId,
						'currentDateTime': timeStr,
						'friends': friendsList,
						'friendsStatusDict': statusDict,
						'onlineFriends': friendsOnlineList
					}),
					'url': reverse('profile', kwargs = {'currentUser': currentUser}),
					'titl': 'Профиль %s' % userProfile.username
					})})
			return JsonResponse({'newPage': json.dumps({
				'content': ('psixiSocial', 'profile', 'show', '', 'thps'),
				'menu': fragments.menuGetter('type3'),
				'templateHead': rts('psixiSocial/head/profile.html'),
				'templateBody': rts('psixiSocial/body/profile/profileThpsInfo.html', {
					'profileIsMine': False,
					'isFriend': 'Guest',
					'profileMainInfo': {
						'id': userProfileId,
						'url': userProfileSlug,
						'username': userProfile.username,
						'firstname': userProfile.first_name,
						'lastname': userProfile.last_name,
						'country': userProfile.country,
						'region': userProfile.subRegion,
						'city': userProfile.city,
						'birthdate': userProfile.birthDate
						},
					'profileContacts': {
						'email': userProfile.email
						},
					'profileTHPSInfo': {
						'nickname': thpsProfile.nickName,
						'clan': thpsProfile.clanName
						},
					'profileImages': {
						'background': userProfile.background,
						'avatar': userProfile.avatarProfileThumb
						},
					'bodyStatus': statusBody,
					'csrf': '',
					'friends': friendsList,
					'onlineFriends': friendsOnlineList
					}),
				'templateScripts': rts('psixiSocial/scripts/profile.html', {
					'onlineStatusTextDict': timeTextDict,
					'requestUserId': None,
					'targetUserId': userProfileId,
					'scriptsStatusDict': statusScripts,
					'currentDateTime': timeStr,
					'friendsStatusDict': statusDict
					}),
				'url': reverse('profile', kwargs = {'currentUser': currentUser})
				})})
		return render(request, 'psixiSocial/psixiSocial.html', {
			'parentTemplate': 'base/classicPage/base.html',
			'content': ('psixiSocial', 'profile', 'show', '', 'thps'),
			'menu': fragments.menuGetter('type3Serialized'),
			'profileIsMine': False,
			'isFriend': 'Guest',
			'profileMainInfo': {
				'id': userProfileId,
				'url': userProfileSlug,
				'username': userProfile.username,
				'firstname': userProfile.first_name,
				'lastname': userProfile.last_name,
				'country': userProfile.country,
				'region': userProfile.subRegion,
				'city': userProfile.city,
				'birthdate': userProfile.birthDate
				},
			'profileContacts': {
				'email': userProfile.email
				},
			'profileTHPSInfo': {
				'nickname': thpsProfile.nickName,
				'clan': thpsProfile.clanName
				},
			'profileImages': {
				'background': userProfile.background,
				'avatar': userProfile.avatarProfileThumb
				},
			'bodyStatus': statusBody,
			'scriptsStatusDict': statusScripts,
			'csrf': checkCSRF(request, request.COOKIES),
			'onlineStatusTextDict': timeTextDict,
			'requestUserId': None,
			'targetUserId': userProfileId,
			'currentDateTime': timeStr,
			'friends': friendsList,
			'friendsStatusDict': statusDict,
			'onlineFriends': friendsOnlineList
			})