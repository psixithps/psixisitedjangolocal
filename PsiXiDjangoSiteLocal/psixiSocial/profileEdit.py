import json
from functools import lru_cache
from django.utils.text import slugify
from psixiSocial.models import mainUserProfile, THPSProfile, userProfilePrivacy
from libraryTHPS.models import playersShip
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from psixiSocial.login import login_decorator
from psixiSocial.forms import registrationForm, userEditContactsForm, userEditDetalForm, userEditMainForm, userEditPassword, userEditTHPSForm, editPrivacyProfileForm
from psixiSocial.privacyIdentifier import checkPrivacy
from django.shortcuts import render, redirect
from django.template.loader import render_to_string as rts
from django.http import JsonResponse
from cities.models import Country, Region, Subregion, City
from django.core import serializers
from collections import OrderedDict
from django.db import transaction
from django.urls import reverse
from base.helpers import checkCSRF
from django.core.cache import caches

class cachedFragments():
    def __init__(self, *args, **kwargs):
        self.username = kwargs.pop('username')
        self.profileUrl = kwargs.pop('profileUrl')

    @property
    @lru_cache(maxsize = 16)
    def menu(self):
        menu = OrderedDict({
            0: {
                'textname': 'Редактирование',
                'fixed': '',
                0: {
                    'textname': 'Профиль %s' % self.username,
                    'fixed': '',
                    'url': reverse('profile', kwargs = {'currentUser': self.profileUrl}),
                    0: {
                        'textname': 'Профиль',
                        'fixed': '',
                        0: {
                            'textname': 'Основные',
                            'url': reverse('profileEditUrl', kwargs = {'editType': 'main'}),
                            'fixed': ''
                            },
                        1: {
                            'textname': 'Контакты',
                            'url': reverse('profileEditUrl', kwargs = {'editType': 'contacts'}),
                            'fixed': ''
                            },
                        2: {
                            'textname': 'Подробно',
                            'url': reverse('profileEditUrl', kwargs = {'editType': 'detail'}),
                            'fixed': ''
                            },
                        3: {
                            'textname': 'Настройки приватности',
                            'url': reverse('editPrivacyUrl', kwargs = {'editPlace': 'profile'}),
                            0: {
                                'textname': 'Права для исключительных пользователей',
                                'url': reverse('editPrivacyExclusiveUrl', kwargs = {'editPlace': 'profile'})
                                }
                            },
                        4: {
                            'textname': 'Аутентификация',
                            'url': reverse('profileEditUrl', kwargs = {'editType': 'password'}),
                            'fixed': ''
                            }
                        },
                    1: {
                        'textname': 'THPS профиль',
                        'url': reverse('profileEditUrl', kwargs = {'editType': 'thps'})
                        }
                    }
            },
            1: {
                'textname': 'Пользователи',
                0: {
                    'textname': 'Все',
                    'url': reverse('users', kwargs = {'category': 'all', 'sep': '', 'page': ''})
                    },
                1: {
                    'textname': 'Сейчас на сайте',
                    'url': reverse('users', kwargs = {'category': 'online', 'sep': '', 'page': ''})
                    }
                },
            2: {
                'textname': 'Друзья',
                0: {
                    'textname': 'Все',
                    'url': reverse('friends', kwargs = {'user': self.profileUrl, 'category': 'all', 'sep': '', 'page': ''})
                    },
                1: {
                    'textname': 'Сейчас на сайте',
                    'url': reverse('friends', kwargs = {'user': self.profileUrl, 'category': 'online', 'sep': '', 'page': ''})
                    }
                },
            3: {
                'textname': 'Выход',
                'url': reverse("logOut")
                }
            })
        return menu

    @property
    @lru_cache(maxsize = 16)
    def menuSerialized(self):
        menuDumps = self.menu
        if isinstance(menuDumps, OrderedDict):
            return menuDumps
        return json.dumps(menuDumps())

    def menuGetter(self, methodName):
        menu = getattr(self, methodName)
        if isinstance(menu, OrderedDict):
            return menu
        if isinstance(menu, str):
            return menu
        return menu()

@login_decorator
def profileEdit(request, editType):
    try:
        userProfile = mainUserProfile.objects.get(id = request.user.id)
    except:
        pass
    type = request.method
    if type == 'GET':
        if editType == 'main':
            username = userProfile.username
            profileUrl = userProfile.profileUrl
            fragments = cachedFragments(username = username, profileUrl = profileUrl)
            if request.is_ajax():
                if 'global' in request.GET:
                    return JsonResponse({'newPage': json.dumps({
                        'newTemplate': rts('psixiSocial/psixiSocial.html', {
                            'parentTemplate': 'base/classicPage/emptyParent.html',
                            'content': ('psixiSocial', 'profile', 'edit', 'main'),
                            'csrf': '',
                            'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Основные данные')]),
                            'form': userEditMainForm(initial = {
                                'username': username,
                                'profileUrl': profileUrl
                                }),
                            'formTextDict': None,
                            'menu': fragments.menuGetter('menuSerialized'),
                        'titl': '',
                        'url': request.path
                        })
                        })})
                return JsonResponse({'newPage': json.dumps({
                    'url': request.path,
                    'menu': fragments.menuGetter('menu'),
                    'templateHead': rts('psixiSocial/head/edit.html'),
                    'templateBody': rts('psixiSocial/body/edit.html', {
                        'content': ('psixiSocial', 'profile', 'edit', 'main'),
                        'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Основные данные')]),
                        'csrf': '',
                        'form': userEditMainForm(initial = {
                            'username': username,
                            'profileUrl': profileUrl
                            }),
                        'formTextDict': None,
                    }),
                    'templateScripts': rts('psixiSocial/scripts/edit.html')
                    })})
            return render(request, 'psixiSocial/psixiSocial.html', {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'profile', 'edit', 'main'),
                'menu': fragments.menuGetter('menuSerialized'),
                'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Основные данные')]),
                'csrf': checkCSRF(request, request.COOKIES),
                'form': userEditMainForm(initial = {
                    'username': username,
                    'profileUrl': profileUrl
                    }),
                'formTextDict': None,
                })
        elif editType == 'contacts':
            fragments = cachedFragments(username = userProfile.username, profileUrl = userProfile.profileUrl)
            if request.is_ajax():
                if 'global' in request.GET:
                    return JsonResponse({'newPage': json.dumps({
                        'newTemplate': rts('psixiSocial/psixiSocial.html', {
                            'parentTemplate': 'base/classicPage/emptyParent.html',
                            'content': ('psixiSocial', 'profile', 'edit', 'contacts'),
                            'menu': fragments.menuGetter('menuSerialized'),
                            'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Контактные данные')]),
                            'csrf': '',
                            'form': userEditContactsForm(initial = {
                                'email': userProfile.email
                                }),
                            'formTextDict': None,
                            }),
                        'titl': '',
                        'url': request.path
                        })})
                return JsonResponse({'newPage': json.dumps({
                    'url': request.path,
                    'menu': fragments.menuGetter('menu'),
                    'templateHead': rts('psixiSocial/head/edit.html'),
                    'templateBody': rts('psixiSocial/body/edit.html', {
                        'content': ('psixiSocial', 'profile', 'edit', 'contacts'),
                        'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Контактные данные')]),
                        'csrf': '',
                        'form': userEditContactsForm(initial = {
                            'email': userProfile.email
                            }),
                        'formTextDict': None,
                        }),
                    'templateScripts': rts('psixiSocial/scripts/edit.html')
                    })})
            return render(request, 'psixiSocial/psixiSocial.html', {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'profile', 'edit', 'contacts'),
                'menu': fragments.menuGetter('menuSerialized'),
                'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Контактные данные')]),
                'csrf': checkCSRF(request, request.COOKIES),
                'form': userEditContactsForm(initial = {
                    'email': userProfile.email
                    }),
                'formTextDict': None,
                })
        elif editType == 'detail':
            fragments = cachedFragments(username = userProfile.username, profileUrl = userProfile.profileUrl)
            if request.is_ajax():
                if 'global' in request.GET:
                    return JsonResponse({'newPage': json.dumps({
                        'newTemplate': rts('psixiSocial/psixiSocial.html', {
                            'parentTemplate': 'base/classicPage/emptyParent.html',
                            'content': ('psixiSocial', 'profile', 'edit', 'detail'),
                            'menu': fragments.menuGetter('menuSerialized'),
                            'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Подробнее о себе')]),
                            'csrf': '',
                            'form': userEditDetalForm(initial = {
                                'first_name': userProfile.first_name,
                                'last_name': userProfile.last_name,
                                'avatar': userProfile.avatar
                                }),
                            'formTextDict': None,
                            }),
                        'titl': '',
                        'url': request.path
                        })})
                return JsonResponse({'newPage': json.dumps({
                    'url': request.path,
                    'menu': fragments.menuGetter('menu'),
                    'templateHead': rts('psixiSocial/head/edit.html'),
                    'templateBody': rts('psixiSocial/body/edit.html', {
                        'content': ('psixiSocial', 'profile', 'edit', 'detail'),
                        'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Подробнее о себе')]),
                        'csrf': '',
                        'form': userEditDetalForm(initial = {
                            'first_name': userProfile.first_name,
                            'last_name': userProfile.last_name,
                            'avatar': userProfile.avatar
                            }),
                        'formTextDict': None,
                        }),
                    'templateScripts': rts('psixiSocial/scripts/edit.html')
                    })})
            return render(request, 'psixiSocial/psixiSocial.html', {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'profile', 'edit', 'detail'),
                'menu': fragments.menuGetter('menuSerialized'),
                'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Подробнее о себе')]),
                'csrf': checkCSRF(request, request.COOKIES),
                'form': userEditDetalForm(initial = {
                    'first_name': userProfile.first_name,
                    'last_name': userProfile.last_name,
                    'avatar': userProfile.avatar
                    }),
                'formTextDict': None,
                })
        elif editType == 'thps':
            try:
                THPSProfile = THPSProfile.objects.get(pk = userProfile)
            except:
                THPSProfile = None
            fragments = cachedFragments(username = userProfile.username, profileUrl = userProfile.profileUrl)
            if THPSProfile is None:
                if request.is_ajax():
                    if 'global' in request.GET:
                        return JsonResponse({'newPage': json.dumps({
                            'newTemplate': rts('psixiSocial/psixiSocial.html', {
                                'parentTemplate': 'base/classicPage/emptyParent.html',
                                'content': ('psixiSocial', 'profile', 'edit', 'confirmAction'),
                                'menu': fragments.menuGetter('menuSerialized'),
                                'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Подпрофиль THPS')]),
                                'form': None,
                                'formTextDict': OrderedDict([('h2', 'THPS вас знает?!'), ('p', 'Если вы знаете о чём здесь речь: подтвердите создание подпрофиля, посвящённого вашей игровой карьере'), ('br', ''), ('input', 'type = "submit" value = "Создать THPS подпрофиль"'), ('br', ''), ('small', 'Я не играю, <a href = "%s">Вернуться к профилю.</a>' % reverse('profile', kwargs = {'currentUser': 'im'}))
                                                             ]),
                                }),
                            'titl': '',
                            'url': request.path
                            })})
                    return JsonResponse({'newPage': json.dumps({
                        'templateHead': rts('psixiSocial/head/edit.html'),
                        'templateBody': rts('psixiSocial/body/edit.html', {
                            'content': ('psixiSocial', 'profile', 'edit', 'confirmAction'),
                            'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Подпрофиль THPS')]),
                            'csrf': '',
                            'form': None,
                            'formTextDict': OrderedDict([('h2', 'THPS вас знает?!'), ('p', 'Если вы знаете о чём здесь речь: подтвердите создание подпрофиля, посвящённого вашей игровой карьере'), ('br', ''), ('input', 'type = "submit" value = "Создать THPS подпрофиль"'), ('br', ''), ('small', 'Я не играю, <a href = "%s">Вернуться к профилю.</a>' % reverse('profile', kwargs = {'currentUser': 'im'}))
                                                         ]),
                                }),
                        'url': request.path,
                        'menu': fragments.menuGetter('menu'),
                        })})
                return render(request, 'psixiSocial/psixiSocial.html', {
                    'parentTemplate': 'base/classicPage/base.html',
                    'content': ('psixiSocial', 'profile', 'edit', 'confirmAction'),
                    'menu': fragments.menuGetter('menuSerialized'),
                    'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Подпрофиль THPS')]),
                    'csrf': checkCSRF(request, request.COOKIES),
                    'form': None,
                    'formTextDict': OrderedDict([('h2', 'THPS вас знает?!'), ('p', 'Если вы знаете о чём здесь речь: подтвердите создание подпрофиля, посвящённого вашей игровой карьере'), ('br', ''), ('input', 'type = "submit" value = "Создать THPS подпрофиль"'), ('br', ''), ('small', 'Я не играю, <a href = "%s">Вернуться к профилю.</a>' % reverse('profile', kwargs = {'currentUser': 'im'}))
                                                     ]),
                    })
            if request.is_ajax():
                if 'global' in request.GET:
                    return JsonResponse({'newPage': json.dumps({
                        'newTemplate': rts('psixiSocial/psixiSocial.html', {
                            'parentTemplate': 'base/classicPage/emptyParent.html',
                            'content': ('psixiSocial', 'profile', 'edit', 'thps'),
                            'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Подпрофиль THPS')]),
                            'csrf': '',
                            'form': userEditTHPSForm(initial = {
                                'toUser': userProfile,
                                'nickName': THPSProfile.nickName,
                                'dateJoinToTHPS': THPSProfile.dateJoinToTHPS,
                                'clanName': THPSProfile.clanName,
                                'dateJoinToClan': THPSProfile.dateJoinToClan,
                                'withoutClan': THPSProfile.withoutClan
                                })
                            }),
                        'titl': '',
                        'url': request.path
                        })})
                return JsonResponse({'newPage': json.dumps({
                    'url': request.path,
                    'menu': fragments.menuGetter('menu'),
                    'templateHead': rts('psixiSocial/head/edit.html'),
                    'templateBody': rts('psixiSocial/body/edit.html', {
                        'content': ('psixiSocial', 'profile', 'edit', 'thps'),
                        'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Подпрофиль THPS')]),
                        'csrf': '',
                        'form': userEditTHPSForm(initial = {
                            'toUser': userProfile,
                            'nickName': THPSProfile.nickName,
                            'dateJoinToTHPS': THPSProfile.dateJoinToTHPS,
                            'clanName': THPSProfile.clanName,
                            'dateJoinToClan': THPSProfile.dateJoinToClan,
                            'withoutClan': THPSProfile.withoutClan
                            })
                        }),
                    'templateScripts': rts('psixiSocial/scripts/edit.html')
                    })})
            return render(request, 'psixiSocial/psixiSocial.html', {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'profile', 'edit', 'thps'),
                'menu': fragments.menuGetter('menuSerialized'),
                'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Подпрофиль THPS')]),
                'csrf': checkCSRF(request, request.COOKIES),
                'form': userEditTHPSForm(initial = {
                    'toUser': userProfile,
                    'nickName': THPSProfile.nickName,
                    'dateJoinToTHPS': THPSProfile.dateJoinToTHPS,
                    'clanName': THPSProfile.clanName,
                    'dateJoinToClan': THPSProfile.dateJoinToClan,
                    'withoutClan': THPSProfile.withoutClan
                    })
                })
        elif editType == 'password':
            fragments = cachedFragments(username = userProfile.username, profileUrl = userProfile.profileUrl)
            if request.is_ajax():
                if 'global' in request.GET:
                    return JsonResponse({'newPage': json.dumps({
                        'newTemplate': rts('psixiSocial/psixiSocial.html', {
                            'parentTemplate': 'base/classicPage/emptyParent.html',
                            'content': ('psixiSocial', 'profile', 'edit', 'password'),
                            'menu': fragments.menuGetter('menuSerialized'),
                            'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Настройки аутентификации')]),
                            'csrf': '',
                            'form': userEditPassword(),
                            'formTextDict': None,
                            }),
                        'titl': '',
                        'url': request.path
                        })})
                return JsonResponse({'newPage': json.dumps({
                    'url': request.path,
                    'menu': fragments.menuGetter('menu'),
                    'templateHead': rts('psixiSocial/head/edit.html'),
                    'templateBody': rts('psixiSocial/body/edit.html', {
                        'content': ('psixiSocial', 'profile', 'edit', 'password'),
                        'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Настройки аутентификации')]),
                        'csrf': '',
                        'form': userEditPassword(),
                        'formTextDict': None,
                        }),
                    'templateScripts': rts('psixiSocial/scripts/edit.html')
                    })})
            return render(request, 'psixiSocial/psixiSocial.html', {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'profile', 'edit', 'password'),
                'menu': fragments.menuGetter('menuSerialized'),
                'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Настройки аутентификации')]),
                'csrf': checkCSRF(request, request.COOKIES),
                'form': userEditPassword(),
                'formTextDict': None,
                })
    elif type == 'POST':
        id = str(request.user.id)
        try:
            userProfile = mainUserProfile.objects.get(id__exact = id)
        except mainUserProfile.DoesNotExist:
            pass
        if editType == 'main':
            form = userEditMainForm(request.POST, instance = userProfile, user = userProfile,
                                    initial = {
                                            'username': userProfile.username,
                                            'profileUrl': userProfile.profileUrl
                                            })
            if form.is_valid():
                form.save()
                changedData = form.changed_data
                if 'profileUrl' in changedData or 'username' in changedData:
                    caches['smallThumbs'].delete(id)
                    caches['usersListThumbs'].delete(id)
                    caches['dialogListThumbs'].delete(id)
                    caches['messagesListThumbs'].delete(id)
                    caches['dialogThumbs'].delete(id)
                if request.is_ajax():
                    return JsonResponse({'redirectTo': None})
                return redirect(request.path)
            if request.is_ajax():
                return JsonResponse({'newPage': json.dumps({
                    'templateBody': rts('psixiSocial/body/edit.html', {
                        'content': ('psixiSocial', 'profile', 'edit', 'main'),
                        'csrf': '',
                        'form': form,
                        'formTextDict': None,
                        })
                    })})
            fragments = cachedFragments(username = userProfile.username, profileUrl = userProfile.profileUrl)
            return render(request, 'psixiSocial/psixiSocial.html', {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'profile', 'edit', 'main'),
                'menu': fragments.menuGetter('menuSerialized'),
                'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Основные данные')]),
                'csrf': checkCSRF(request, request.COOKIES),
                'form': form,
                'formTextDict': None,
                })
        elif editType == 'detail':
            form = userEditDetalForm(request.POST, request.FILES, instance = userProfile, user = userProfile, initial = {
                'first_name': userProfile.first_name,
                'last_name': userProfile.last_name,
                'avatar': userProfile.avatar
                })
            if form.is_valid():
                form.save()
                changedData = form.changed_data
                if 'first_name' in changedData or 'last_name' in changedData or 'avatar' in changedData:
                    caches['smallThumbs'].delete(id)
                    caches['usersListThumbs'].delete(id)
                    caches['dialogListThumbs'].delete(id)
                    caches['messagesListThumbs'].delete(id)
                    caches['dialogThumbs'].delete(id)
                if request.is_ajax():
                    return JsonResponse({'redirectTo': None})
                return redirect(request.path)
            if request.is_ajax():
                return JsonResponse({'newPage': json.dumps({
                    'templateBody': rts('psixiSocial/body/edit.html', {
                        'content': ('psixiSocial', 'profile', 'edit', 'detail'),
                        'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Подробнее о себе')]),
                        'csrf': '',
                        'form': form,
                        'formTextDict': None,
                        })
                    })})
            fragments = cachedFragments(username = userProfile.username, profileUrl = userProfile.profileUrl)
            return render(request, 'psixiSocial/psixiSocial.html', {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'profile', 'edit', 'detail'),
                'menu': fragments.menuGetter('menuSerialized'),
                'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Подробнее о себе')]),
                'csrf': checkCSRF(request, request.COOKIES),
                'form': form,
                'formTextDict': None,
                })
        elif editType == 'thps':
            try:
                THPSProfile = THPSProfile.objects.get(pk = userProfile)
            except:
                THPSProfile = None
            if THPSProfile is None:
                try:
                    playerItem = playersShip.objects.get(slugName = slugify(userProfile.profileUrl))
                except playersShip.DoesNotExist:
                    playerItem = None
                if playerItem is None:
                    newTHPSProfile = THPSProfile.objects.create(toUser = userProfile, nickName = userProfile.username)
                    newTHPSProfile.save()
                    if request.is_ajax():
                        return JsonResponse({'redirectTo': json.dumps(reverse("profileEditUrl", kwargs = {'editType': 'thps'}))})
                    return redirect('profileEditUrl', 'thps')
            form = userEditTHPSForm(request.POST, instance = THPSProfile)
            if form.is_valid():
                form.save()
                if request.is_ajax():
                    return JsonResponse({'redirectTo': None})
                return redirect(request.path)
            if request.is_ajax():
                return JsonResponse({'newPage': json.dumps({
                    'templateBody': rts('psixiSocial/body/edit.html', {
                        'content': ('psixiSocial', 'profile', 'edit', 'thps'),
                        'csrf': '',
                        'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Подпрофиль THPS')]),
                        'form': form
                        })
                    })})
            fragments = cachedFragments(username = userProfile.username, profileUrl = userProfile.profileUrl)
            return render(request, 'psixiSocial/psixiSocial.html', {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'profile', 'edit', 'thps'),
                'menu': fragments.menuGetter('menuSerialized'),
                'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Подпрофиль THPS')]),
                'csrf': checkCSRF(request, request.COOKIES),
                'form': form
                })
        elif editType == 'password':
            form = userEditPassword(request.POST, instance = userProfile, user = userProfile)
            if form.is_valid():
                form.save()
                if request.is_ajax():
                    return JsonResponse({'redirectTo': None})
                return redirect(request.path)
            if request.is_ajax():
                return JsonResponse({'newPage': json.dumps({
                    'templateBody': rts('psixiSocial/body/edit.html', {
                        'content': ('psixiSocial', 'profile', 'edit', 'password'),
                        'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Настройки аутентификации')]),
                        'csrf': '',
                        'form': form,
                        'formTextDict': None
                        })
                    })})
            fragments = cachedFragments(username = userProfile.username, profileUrl = userProfile.profileUrl)
            return render(request, 'psixiSocial/psixiSocial.html', {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'profile', 'edit', 'password'),
                'menu': fragments.menuGetter('menuSerialized'),
                'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Настройки аутентификации')]),
                'csrf': checkCSRF(request, request.COOKIES),
                'form': form,
                'formTextDict': None
                })

@login_decorator
def editPrivacy(request, editPlace):
    reqMethod = request.method
    if reqMethod == "GET":
        user = request.user
        id = str(user.id)
        try:
            userProfile = mainUserProfile.objects.get(id__exact = id)
        except:
            pass
        fragments = cachedFragments(username = user.username, profileUrl = user.profileUrl)
        if editPlace == "profile":
            privacy = userProfilePrivacy.objects.get(user_id__exact = id)
            if request.is_ajax():
                if 'global' in request.GET:
                    return JsonResponse({"newPage": rts('psixiSocial/psixiSocial.html', {
                            'parentTemplate': 'base/classicPage/base.html',
                            'content': ('psixiSocial', 'profile', 'edit', 'privacy'),
                            'menu': fragments.menuGetter('menuSerialized'),
                            'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Настройки аутентификации')]),
                            'csrf': '',
                            'form': editPrivacyProfileForm(initial = {
                                'viewProfile': privacy.viewProfile,
                                'viewProfileMainInfo': privacy.viewProfileMainInfo,
                                'viewProfileFriends': privacy.viewProfileFriends,
                                'viewProfileTHPSInfo': privacy.viewProfileTHPSInfo,
                                'viewProfileFriends': privacy.viewProfileFriends,
                                'viewProfileAvatar': privacy.viewProfileAvatar,
                                'viewProfileBackground': privacy.viewProfileBackground,
                                'viewProfileOnlineStatus': privacy.viewProfileOnlineStatus,
                                'viewProfileActiveStatus': privacy.viewProfileActiveStatus,
                                'viewProfileWall': privacy.viewProfileWall,
                                'sendProfileWall': privacy.sendProfileWall,
                                'sendFriendRequests': privacy.sendFriendRequests,
                                'sendPersonalMessages': privacy.sendPersonalMessages
                                }),
                            'titl': '',
                            'url': request.path
                            })
                        })
                return JsonResponse({"newPage": json.dumps({
                    'url': request.path,
                    'menu': fragments.menuGetter('menu'),
                    'templateHead': rts('psixiSocial/head/edit.html'),
                    'templateBody': rts('psixiSocial/body/edit.html', {
                        'content': ('psixiSocial', 'profile', 'edit', 'privacy'),
                        'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Настройки приватности')]),
                        'csrf': '',
                        'form': editPrivacyProfileForm(initial = {
                            'viewProfile': privacy.viewProfile,
                            'viewProfileMainInfo': privacy.viewProfileMainInfo,
                            'viewProfileFriends': privacy.viewProfileFriends,
                            'viewProfileAvatar': privacy.viewProfileAvatar,
                            'viewProfileBackground': privacy.viewProfileBackground,
                            'viewProfileOnlineStatus': privacy.viewProfileOnlineStatus,
                            'viewProfileActiveStatus': privacy.viewProfileActiveStatus,
                            'viewProfileWall': privacy.viewProfileWall,
                            'sendProfileWall': privacy.sendProfileWall,
                            'sendFriendRequests': privacy.sendFriendRequests,
                            'sendPersonalMessages': privacy.sendPersonalMessages
                            }),
                        'formTextDict': None,
                    }),
                    'templateScripts': rts('psixiSocial/scripts/edit.html')
                    })})
            return render(request, 'psixiSocial/psixiSocial.html', {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'profile', 'edit', 'privacy'),
                'menu': fragments.menuGetter('menuSerialized'),
                'helpTextDict': OrderedDict([('h1', 'Редактирование профиля: '), ('h2', 'Настройки приватности')]),
                'csrf': checkCSRF(request, request.COOKIES),
                'form': editPrivacyProfileForm(initial = {
                    'viewProfile': privacy.viewProfile,
                    'viewProfileMainInfo': privacy.viewProfileMainInfo,
                    'viewProfileFriends': privacy.viewProfileFriends,
                    'viewProfileAvatar': privacy.viewProfileAvatar,
                    'viewProfileBackground': privacy.viewProfileBackground,
                    'viewProfileOnlineStatus': privacy.viewProfileOnlineStatus,
                    'viewProfileActiveStatus': privacy.viewProfileActiveStatus,
                    'viewProfileWall': privacy.viewProfileWall,
                    'sendProfileWall': privacy.sendProfileWall,
                    'sendFriendRequests': privacy.sendFriendRequests,
                    'sendPersonalMessages': privacy.sendPersonalMessages
                    }),
                'formTextDict': None,
                })
    elif reqMethod == "POST":
        user = request.user
        id = str(user.id)
        try:
            userProfile = mainUserProfile.objects.get(id__exact = id)
        except:
            pass
        if editPlace == "profile":
            privacyForm = editPrivacyProfileForm(request.POST, instance = userProfilePrivacy.objects.get(user_id__exact = id))
            if privacyForm.is_valid():
                privacyForm.save()
                caches['privacyCache'].delete(id)
                caches['smallThumbs'].delete(id)
                caches['usersListThumbs'].delete(id)
                caches['dialogListThumbs'].delete(id)
                caches['messagesListThumbs'].delete(id)
                caches['dialogThumbs'].delete(id)
                if request.is_ajax():
                    return JsonResponse({"newPage": json.dumps({
                        'templateBody': rts('psixiSocial/body/edit.html', {
                        'content': ('psixiSocial', 'profile', 'edit', 'privacy'),
                        'form': privacyForm
                        })
                        })})
                return render('psixiSocial/psixiSocial.html', {
                    'parentTemplate': 'base/classicPage/base.html',
                    'content': ('psixiSocial', 'profile', 'edit', 'privacy'),
                    'menu': cachedFragments(username = user.username, profileUrl = user.profileUrl).menuSerialized(),
                    'form': privacyForm,
                    'csrf': checkCSRF(request, request.COOKIES)
                    })
            if request.is_ajax():
                return JsonResponse({"newPage": json.dumps({
                    'templateBody': rts('psixiSocial/body/edit.html', {
                        'content': ('psixiSocial', 'profile', 'edit', 'privacy'),
                        'form': privacyForm
                    })
                })})
            return render('psixiSocial/psixiSocial.html', {
                'parentTemplate': 'base/classicPage/base.html',
                'content': ('psixiSocial', 'profile', 'edit', 'privacy'),
                'menu': cachedFragments(username = user.username, profileUrl = user.profileUrl).menuSerialized(),
                'form': privacyForm,
                'csrf': checkCSRF(request, request.COOKIES)
                })

@login_decorator
def editPrivacyExclusiveRights(requst, editPlace):
    if request.method == "GET":
        pass
    if request.method == "POST":
        pass

@login_decorator
def profileEditHabitation(request):
    curLocation = request.GET.get('searchString', None)
    curLocation = json.loads(curLocation)
    currentCountry = curLocation.get('country', None)
    currentRegion = curLocation.get('region', None)
    currentSubRegion = curLocation.get('subRegion', None)
    currentCity = curLocation.get('city', None)
    
    if currentCountry:
        finded = Country.objects.filter(name__istartswith = currentCountry)
    if currentRegion:
        if currentCountry and finded.count():
            finded = Region.objects.filter(country = finded).filter(name__istartswith = currentRegion)
        else:
            finded = Region.objects.filter(name__istartswith = currentRegion)
    if currentSubRegion:
        if currentRegion and finded.count():
            finded = Subregion.objects.filter(region = finded).filter(name__istartswith = currentSubRegion)
        else:
            finded = Subregion.objects.filter(name__istartswith = currentSubRegion)
    if currentCity:
        if currentSubRegion and finded.count():
            finded = City.objects.filter(subregion = finded).filter(name__istartswith = currentCity)
        elif currentCountry and finded.count():
            finded = City.objects.filter(country = finded).filter(name__istartswith = currentCity)
        elif currentRegion and finded.count():
            finded = City.objects.filter(region = finded).filter(name__istartswith = currentCity)
        else:
            finded = City.objects.filter(name__istartswith = currentCity)

               
    placeDict = {
        'placesObject': serializers.serialize('json', list(finded), fields = ('name',)),
    }

    return JsonResponse(placeDict)
