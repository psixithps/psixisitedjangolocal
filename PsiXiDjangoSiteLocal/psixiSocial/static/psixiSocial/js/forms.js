function successHelper(context) {
    context.removeAttribute("class");
    var form = context.getElementsByTagName("form")[0] || null;
    if (form) {
        var button = formSearchButton(form);
        !button.onclick ? setButtonhandler(context.id, form, button) : 0;
    }
};
function onSubmit(bodyId, formId) {
    var form = document.forms[formId];
    var button = formSearchButton(form);
    setButtonhandler(bodyId, form, button);
};
function formSearchButton(form) {
    var button = Array.prototype.slice.call(form.getElementsByTagName("input")).filter(function(i) {
        return i.hasAttribute("type") && i.type == "submit" ? i : null;
    })[0];
    return button;
};
function setButtonhandler(bodyId, form, button) {
    var loc = document.location;
    var i = loc.href.indexOf("?");
    var path = i == -1 ? loc.pathname : "" + loc.pathname + loc.href.slice(i, loc.href.length);
    button.onclick = function(event) {
        doPostForm(bodyId, form, path);
        return event.preventDefault();
    };
};