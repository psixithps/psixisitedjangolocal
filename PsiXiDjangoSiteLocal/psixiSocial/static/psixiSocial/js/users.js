var statusInitial; // ������� �������� ��������� ��������� ������������� �� statusObject, ����������� ���, ��� �� ��������������
function usersList(requestUserId, textObj, onlineUsersPage, statusObject, timeStr) {
    var currentTime = new Date(timeStr);
    var doc = document;
    var sock = userSock;
    var contextLinksObject = {};
    var onlineStatusActiveAnimations = {};
    var activeStatusActiveAnimations = {};
    var rootListElement = doc.getElementById("users-list");
    /* �������� ������� */
    function updateContextItems(contextLinksObject, elements) {
        /* ������� canvas-������, ��������� � ��������� ������, ������ �������� � contextLinksObject[userIdStr] */
        var el = elements.length;
        if (el > 0) {
            var contextElement = doc.createElement("CANVAS");
            contextElement.className = "status-animation";
            contextElement.setAttribute("width", 119.6);
            contextElement.setAttribute("height", 119.6);
            function createCanvasContexts(element) {
                var content = element.getContext("2d");
                return content;
            };
            for (var i = el; i--;) {
                var element = elements[i];
                var id = element.value + "";
                var newContextElement = contextElement.cloneNode(false);
                element.insertBefore(newContextElement, element.firstElementChild);
                contextLinksObject[id] = createCanvasContexts(newContextElement);
            }
           
            return contextLinksObject;
        } else {
            return {};
        }
    };
    function draw(context, r, w, time, position, color, callback) {
        var diffTime = time;
        var iterDiff = 0;
        var dn = Date.now;
        var m = Math;
        var p = m.PI;
        var cl = m.round;
        var fl = m.ceil;
        var timeControl = dn();
        var action = setInterval(function () {
            function draw(ctx, startPosition, endPosition) {
                ctx.lineWidth = w;
                ctx.strokeStyle = color;
                ctx.beginPath();
                ctx.beginPath();
                ctx.arc(59.5, 59.5, r, (startPosition + 1) * p / 180, endPosition * p / 180, true); // ����� ���� �� ��� �� ��� (������ � �������  * p / 180)
                ctx.stroke();
                ctx.closePath();
            };
            if (position > 0) {
                diffTime += (cl(dn() - timeControl) - time); // ���������� (��) �������������
                iterDiff = fl(diffTime / time); // ����������, ������� �� ����������� ����� (������� �� ������� ��������, - ���� �� ���������� ��������)
                var pos = position;
                if (iterDiff >= 1) { // �� ������� ���� �� 1 ��������, ���� ����� ������, ����� �������������� ����������
                    var endPos = pos - iterDiff;
                    draw(context, pos, endPos);
                    diffTime = 0; // �������� ������� ����������
                } else { // �� ��, ������ �� 1 �������
                    var endPos = position - 1;
                    draw(context, position, endPos);
                }
                position = endPos;
                timeControl = dn();
            } else { // ����������, �������� ������ ������� � ������� �������
                clearInterval(action);
                action = null;
                callback ? callback() : 0;
            }
        }, time);
        return action;
    };
    function drawFast(context, r, w, angle, color) {
        var context = context;
        context
        context.beginPath();
        context.strokeStyle = color;
        context.lineWidth = w;
        context.arc(59.5, 59.5, r, 0, angle * Math.PI / 180, false);
        context.stroke();
        context.closePath();
    };
    function drawStatusError(context, radius) {
        function repaint(c, rad, pi) {
            c.strokeStyle = "transparent";
            var r1 = rad + 4;
            for (var i = 0; i < 360; i++) {
                var step = pi * (i * 20) / 180;
                var step1 = pi * ((i + 1) * 20) / 180;
                c.fillStyle = i % 2 == 0 ? "#ffd699" : "#8f8f8f";
                c.beginPath();
                c.arc(59.5, 59.5, r1, step, step1, false);
                c.arc(59.5, 59.5, rad, step1, step, true);
                c.closePath();
                c.stroke();
                c.fill();
            }
        };
        var i = 0;
        var pi = Math.PI;
        var animation = setInterval(function () {
            var cntxt = context;
            cntxt.translate(59.5, 59.5);
            cntxt.rotate((i += 15) * Math.PI / 180);
            cntxt.translate(-59.5, -59.5);
            repaint(cntxt, radius, pi);
            i = i == 360 ? 0 : i;
        }, 540);
        return animation;
    };
    function createStatus(contextLinksObj, statusObj, currentTime) {
        function dropAnimation(obj, key) {
            var animationItem = obj[key] || null;
            if (animationItem) {
                clearInterval(animationItem);
                var animationItem = null;
            }
        };
        var hiddenActiveArr = statusObj.hiddenActive || [];
        var onlineStatusTimers = {};
        var activeStatusTimers = {};
        var onlineStatusActiveAnims = onlineStatusActiveAnimations;
        var activeStatusActiveAnims = activeStatusActiveAnimations;
        var keys = Object.keys(statusObj);
        var statusArr = []; // ������ ��� ���������� "stateLeave", ���������� �� ������������ � ������� �������
        var arr1 = [];
        for (var i = keys.length; i--;) {
            var statusType = keys[i];
            if (statusType == "onlineError") {
                statusObj[statusType].reduce(function (accum, arrItem) {
                    dropAnimation(onlineStatusActiveAnims, arrItem);
                    onlineStatusTimers[arrItem] = drawStatusError(contextLinksObj[arrItem], 
                        hiddenActiveArr.indexOf(arrItem) == -1 ? 55.8 : 51.8);
                }, 0);
            } else if (statusType == "activeError") {
                statusObj[statusType].reduce(function (accum, arrItem) {
                    dropAnimation(activeStatusActiveAnims, arrItem);
                    activeStatusTimers[arrItem] = drawStatusError(contextLinksObj[arrItem],
                        51.8);
                }, 0);
            } else {
                if (statusType == "stateMiddle" || statusType == "stateTimer") {
                    var statusObject = statusObj[statusType];
                    if (statusType == "stateTimer") {
                        Object.keys(statusObject).reduce(function (accum, arrItem) {
                            dropAnimation(onlineStatusActiveAnims, arrItem);
                            statusArr.unshift(arrItem);
                            var context = contextLinksObj[arrItem];
                            var startPosition = 360 - (360 / (20000 / (currentTime - Date.parse(statusObject[arrItem]))));
                            drawFast(context, hiddenActiveArr.indexOf(arrItem) == -1 ? 55.8 : 51.8, 4, startPosition, "#47b647");
                            onlineStatusTimers[arrItem] = draw(context, 53, 12, 20000 / 360, startPosition, "#FFF");
                        }, 0);
                    } else {
                        Object.keys(statusObject).reduce(function (accum, arrItem) {
                            dropAnimation(activeStatusActiveAnims, arrItem);
                            var context = contextLinksObj[arrItem];
                            var timeStrArr = statusObject[arrItem].split("|");
                            var totalTime = +timeStrArr[1].slice(3, -1) * 1000;
                            var startPosition = 360 / (totalTime / (currentTime - Date.parse(timeStrArr[0].slice(2, -4))));
                            drawFast(context, 51.8, 4, 360, "#D74620");
                            drawFast(context, 51.8, 4, 360 - startPosition, "#FF7f00");
                            activeStatusTimers[arrItem] = draw(context, 51.8, 4, totalTime / 360, 360 - startPosition, "#D74620");
                            statusArr.indexOf(arrItem) > -1 ? delete statusArr[arrItem] : 0;
                            arr1.unshift(arrItem);
                        }, 0);
                    }
                } else {
                    if (statusType == "stateActive") {
                        if (keys.indexOf("stateTimer") > -1) {
                            var onlineTimersObj = statusObj.stateTimer;
                            statusObj[statusType].reduce(function (accum, arrItem) {
                                onlineTimersObj.hasOwnProperty(arrItem) ?
                                    drawFast(contextLinksObj[arrItem], 51.8, 4, 360 - (360 / (20000 / (currentTime - Date.parse(onlineTimersObj[arrItem])))), "#00AFFF") :
                                    drawFast(contextLinksObj[arrItem], 51.8, 4, 360, "#00AFFF");
                                statusArr.indexOf(arrItem) > -1 ? delete statusArr[arrItem] : 0;
                                arr1.unshift(arrItem);
                            }, 0);
                        } else {
                            statusObj[statusType].reduce(function (accum, arrItem) {
                                drawFast(contextLinksObj[arrItem], 51.8, 4, 360, "#00AFFF");
                                statusArr.indexOf(arrItem) > -1 ? delete statusArr[arrItem] : 0;
                                arr1.unshift(arrItem);
                            }, 0);
                        }
                    } else if (statusType == "stateOnline") {
                        statusObj[statusType].reduce(function (accum, arrItem) {
                            statusArr.unshift(arrItem);
                            drawFast(contextLinksObj[arrItem], hiddenActiveArr.indexOf(arrItem) == -1 ? 55.8 : 51.8, 4, 360, "#47b647");
                        }, 0);
                    }
                }
            }
        }
        var statusArr = statusArr.difference(arr1).difference(hiddenActiveArr);
        var statusArr = statusArr.difference(hiddenActiveArr);
        for (var t = statusArr.length; t--;) {
            drawFast(contextLinksObj[statusArr[t]], 51.8, 4, 360, "#D74620");
        }
        Object.keys(onlineStatusTimers).length > 0 ? Object.assign(onlineStatusActiveAnimations, onlineStatusTimers) : 0;
        Object.keys(activeStatusTimers).length > 0 ? Object.assign(activeStatusActiveAnimations, activeStatusTimers) : 0;
    };
    /* end �������� ������� */
    function clickHandler(event) {
        var target = event.target;
        var targetName = target.nodeName;
        var parent = target.parentNode;
        if (targetName == "A") {
            localNavigation(null, target.href, event);
            event.preventDefault();
        } else if (targetName == "P") {
            parent.className == "no-avatar" ? localNavigation(null, parent.parentNode.href, event) : 0;
            event.preventDefault();
        } else if (targetName == "INPUT") {
            if (parent.method == "post") {
                var s = sock;
                doPostFormSimplifield(doc, parent.action, parent, null, s ? s.readyState == 1 ? true : false : false, event);
            } else {
                localNavigation(null, parent.action, event);
                event.preventDefault();
            }
        } else {
            var parentName = parent.nodeName;
            if (parentName == "H3") {
                localNavigation(null, target.href, event);
            } else if (parentName == "A") {
                localNavigation(null, parent.href, event);
            }
            event.preventDefault();
        }
        event.stopPropagation();
    };
    function searchParentByClassName(element, className) {
        var parent = null;
        do {
            if (element) {
                if (element.className != className) {
                    var element = element.parentNode;
                } else {
                    var parent = element;
                    break;
                }
            } else {
                break;
            }
        } while (element)
        return parent;
    };
    function openOptionsHandler(event) {
        var search = searchParentByClassName(event.target, "actions");
        if (search) {
            var itemToShow = search.firstElementChild;
            itemToShow.getAttribute("class") != "open" ? itemToShow.setAttribute("class", "open") : 0;
        }
    };
    function hideOptionsHandler(event) {
        var search = searchParentByClassName(event.target, "actions");
        if (search) {
            var itemToHide = search.firstElementChild;
            itemToHide.getAttribute("class") == "open" ? itemToHide.removeAttribute("class") : 0;
        }
    };
    var pageCounter = null, loadUsersLink = null;
    function createLoadUrl(doc) {
        var pattern = new RegExp(/[0-9]+/);
        var path = doc.location.pathname;
        var matchesArray = path.match(pattern);
        if (matchesArray) {
            var page = matchesArray[0];
            loadUsersLink = path.slice(0, path.lastIndexOf(page));
            pageCounter = 1 + parseInt(page);
        } else {
            loadUsersLink = path;
            pageCounter = 2;
        }
    };
    createLoadUrl(doc);
    var contentBlockHeight = 0;
    function getContentBlockHeight(rootListElement) {
        contentBlockHeight = rootListElement.parentNode.clientHeight;
    };
    getContentBlockHeight(rootListElement);
    function loadMoreUsers(event) {
        var data = this || event.target;
        var element = data.scrollingElement;
        if ((element.scrollHeight - element.scrollTop) == contentBlockHeight) {
            var request = new XMLHttpRequest();
            request.open("GET", loadUsersLink + '?page=' + pageCounter, true);
            request.onloadend = function(data) {
                var data = this || data.target;
                var data = JSON.parse(data.response);
                var data = data.users;
                if (data) {
                    var usersId = listConstructor(doc, data, rootListElement, onlineUsersPage);
                    pageCounter++;
                    sockErrorClose(null, usersId);
                }
            };
            request.send();
        }
    };
    if (doc.addEventListener) {
        rootListElement.addEventListener("mouseover", openOptionsHandler, false);
        rootListElement.addEventListener("mouseout", hideOptionsHandler, false);
        rootListElement.addEventListener("click", clickHandler, false);
        doc.addEventListener("scroll", loadMoreUsers, false);
    } else {
        rootListElement.attachEvent("mouseover", openOptionsHandler, false);
        rootListElement.attachEvent("mouseout", hideOptionsHandler, false);
        rootListElement.attachEvent("click", clickHandler, false);
        doc.attachEvent("scroll", loadMoreUsers, false);
    }
    function listConstructor(doc, data, rootListElement, isOnlinePage) {
        var container = doc.createElement("UL");
        var length = data.length;
        var elem = doc.createElement("LI");
        var image = doc.createElement("IMG");
        var h3 = doc.createElement("H3");
        var small = doc.createElement("SMALL");
        var link = doc.createElement("A");
        var isOnline = doc.createElement("SPAN");
        var cnv = doc.createElement("CANVAS");
        var contextLinks = {};
        for (var i = 0; i < length; i++) {
            var item = data[i];
            var id = item.id;
            var el = elem.cloneNode(false);
            var actions = container.cloneNode(false);
            var cnv1 = cnv.cloneNode(false);
            el.appendChild(cnv1);
            actions.className = "actions";
            el.setAttribute("value", id);
            var l = link.cloneNode(false);
            var l1 = null;
            var h = h3.cloneNode(false);
            var sm = null;
            var avatarWr = null;
            var isOnl = null;
            if (isOnlinePage || item.hasOwnProperty('online')) {
                var isOnl = isOnline.cloneNode(false);
                isOnl.innerText = 'Online';
            }
            var avatarWrap = doc.createElement("DIV");
            avatarWrap.className = "avatar-wrap";
            var avatar = item.avatar;
            l.setAttribute("href", item.profileUrl);
            if (avatar) {
                var l1 = l.cloneNode(false);
                var avatarWr = avatarWrap.cloneNode(false);
                avatarWr.appendChild(l1);
                var img = image.cloneNode(false);
                img.setAttribute("src", avatar);
                l1.appendChild(img);
            }
            if (item.hasOwnProperty('firstname') && item.hasOwnProperty('lastname')) {
                l.innerText = item.firstname + " " + item.lastname;
            } else {
                if (item.hasOwnProperty('firstname')) {
                    var sm = small.cloneNode(false);
                    l.innerText = item.firstname;
                    sm.innerText = "[" + item.username + "]";
                } else {
                    l.innerText = item.username;
                }
            }
            avatarWr ? el.appendChild(avatarWr) : 0;
            h.appendChild(l);
            el.appendChild(h);
            sm ? el.appendChild(sm) : 0;
            el.appendChild(actions);
            isOnl ? el.appendChild(isOnl) : 0;
            container.appendChild(el);
            contextLinks[id] = cnv1.getContext("2d");
        }
        Object.assign(contextLinksObject, contextLinks);
        rootListElement.innerHTML += container.innerHTML;
        return Object.keys(contextLinks);
    };
    var mainSockListener = function (event) {
        var data = event.target || false ? event.target.response || false ? JSON.parse(this.response.data || event.target.response.data) : JSON.parse(event.data) : event;
        data.constructor === Array ? 0 : data = [data];
        for (var i = data.length; i--;) {
            var obj = data[i];
            var method = obj.method;
            if (method == 'stateOnline' || method == 'stateTimer' || method == 'stateLeave') {
                var userId = obj.id;
                var context = contextLinksObject[userId] || null;
                if (context !== null) {
                    var statusObj = statusObject;
                    var liveOnlineStatusTimer = onlineStatusActiveAnimations[userId] || null;
                    if (method == 'stateOnline') {
                        if (liveOnlineStatusTimer !== null) {
                            clearInterval(liveOnlineStatusTimer);
                            var liveOnlineStatusTimer = null;
                            delete onlineStatusActiveAnimations[userId];
                        }
                        if ((statusObj.hiddenActive || []).indexOf(userId) != -1) {
                            drawFast(context, 51.8, 4, 360, "#47b647");
                        } else {
                            if (statusObj.hasOwnProperty('stateActive')) {
                                drawFast(context, 51.8, 4, 360, "#00AFFF");
                            } else if (statusObj.hasOwnProperty('stateMiddle')) {
                                drawFast(context, 51.8, 4, 360, "#FF7f00");
                                activeStatusActiveAnimations[userId] = draw(context, 51.8, 4, 210000 / 360, 360, "#D74620");
                            } else {
                                drawFast(context, 51.8, 4, 360, "#D74620");
                            }
                            drawFast(context, 55.8, 4, 360, "#47b647");
                        }
                        var statusData = statusObj.method || null;
                        statusData ? statusData.push(userId) : statusObj[method] = [userId];
                    } else if (method == 'stateTimer') {
                        if (liveOnlineStatusTimer === null) {
                            onlineStatusActiveAnimations[userId] = draw(context, 53, 12, 20000 / 360, 360, "#FFF", function () {
                                delete onlineStatusActiveAnimations[userId];
                            });
                            var liveActiveStatusTimer = activeStatusActiveAnimations[userId] || null;
                            if (liveActiveStatusTimer) {
                                clearInterval(liveActiveStatusTimer);
                                var liveActiveStatusTimer = null;
                                delete activeStatusActiveAnimations[userId];
                            }
                        }
                        var statusData = statusObj.method || null;
                        if (statusData) {
                            statusData[userId] = currentTime.getTime();
                        } else {
                            var statusData = {};
                            statusData[userId] = currentTime.getTime();
                            statusObj[method] = statusData;
                        }
                    } else {
                        if (liveOnlineStatusTimer !== null) {
                            delete onlineStatusActiveAnimations[userId];
                        }
                        drawFast(context, 51.8, 8, 360, "#fff");
                        var statusData = statusObj.stateOnline || null;
                        if (statusData) {
                            var p = statusData.indexOf(userId);
                            if (p != -1) {
                                statusData.splice(p, 1);
                            } else {
                                var statusData = statusObj.stateTimer || null;
                                statusData ? statusData.hasOwnProperty(userId) ? delete statusData[userId] : null : 0;
                            }
                        } else {
                            var statusData = statusObj.stateTimer || null;
                            statusData ? statusData.hasOwnProperty(userId) ? delete statusData[userId] : null : 0;
                        }
                    }
                    statusObject = statusObj;
                    data.splice(i, 1);
                }
            } else {
                if (method == 'stateActive' || method == 'stateMiddle' || method == 'stateLeave') {
                    var userId = obj.id;
                    var context = contextLinksObject[userId] || null;
                    if (context !== null) {
                        var statusObj = statusObject;
                        var liveActiveStatusTimer = activeStatusActiveAnimations[userId] || null;
                        if (liveActiveStatusTimer !== null) {
                            clearInterval(liveActiveStatusTimer);
                            var liveActiveStatusTimer = null;
                            delete activeStatusActiveAnimations[userId];
                        }
                        if (method == 'stateActive') {
                            drawFast(context, 51.8, 4, 360, "#00AFFF");
                            var statusData = statusObject.method || null;
                            statusData ? statusData.push(userId) : statusObject[method] = [userId];
                        } else if (method == 'stateMiddle') {
                            if ((activeStatusActiveAnimations[userId] || null) === null) {
                                drawFast(context, 51.8, 4, 360, "#FF7f00");
                                activeStatusActiveAnimations[userId] = draw(context, 51.8, 4, 210000 / 360, 360, "#D74620");
                            } else {

                            }
                            var statusData = statusObject.method || null;
                            if (statusData) {
                                statusData[userId] = currentTime.getTime();
                            } else {
                                var statusObj = {};
                                statusObj[userId] = currentTime.getTime();
                                statusObject[method] = statusObj;
                            }
                        } else {
                            drawFast(context, 51.8, 4, 360, "#D74620");
                            var statusObj = statusObject;
                            var statusData = statusObj.hasOwnProperty('stateActive') ? statusObject.stateActive : null;
                            if (statusData) {
                                var p = statusData.indexOf(userId);
                                if (p != -1) {
                                    statusData.splice(p, 1);
                                } else {
                                    var statusData = statusObj.hasOwnProperty('stateTimer') ? statusObject.stateTimer : null;
                                    statusData.hasOwnProperty(userId) ? delete statusData[userId] : null;
                                }
                            } else {
                                var statusData = statusObj.hasOwnProperty('stateTimer') ? statusObject.stateTimer : null;
                                statusData.hasOwnProperty(userId) ? delete statusData[userId] : null;
                            }
                        }
                        data.splice(i, 1);
                    }
                }
            }
        }
        data.length > 0 ? defaultListener(data) : 0;
    };
    statusInitial = function(onlineUsers, activeUsers, onlineUsersErrors, activeUsersErrors) {
        /* ������ ������ ������� � ������������� ����������� � ��������� ������������ ������������� */
        var statusObj = statusObject;
        var keys = Object.keys(statusObj);
        var onlineErrorsArr = [];
        var activeErrorsArr = [];
        for (var t = keys.length; t--;) {
            var statusType = keys[t];
            var statusData = statusObj[statusType];
            if (statusData.constructor == Array) {
                if (statusType == "stateOnline") {
                    for (var i = statusData.length; i--;) {
                        var item = statusData[i];
                        var index = onlineUsers.indexOf(item);
                        if (index == -1) {
                            statusData.splice(index, 1);
                        } else {
                            onlineUsersErrors.indexOf(item) != -1 ? onlineErrorsArr.push(item) : 0;
                        }
                    }
                } else if (statusType == "stateActive") {
                    for (var i = statusData.length; i--;) {
                        var item = statusData[i];
                        var index = activeUsers.indexOf(item);
                        if (index == -1) {
                            statusData.splice(index, 1);
                        } else {
                            activeUsersErrors.indexOf(item) != -1 ? activeErrorsArr.push(item) : 0;
                        }
                    }
                }
            } else {
                var users = Object.keys(statusData);
                if (statusType == "stateTimer") {
                    for (var i = users.length; i--;) {
                        var item = users[i];
                        if (onlineUsers.indexOf(item) == -1) {
                            delete statusData[item];
                        } else {
                            onlineUsersErrors.indexOf(item) != -1 ? onlineErrorsArr.push(item) : 0;
                        }
                    }
                } else if (statusType == "stateMiddle") {
                    for (var i = users.length; i--;) {
                        var item = users[i];
                        if (activeUsers.indexOf(item) == -1) {
                            delete statusData[item];
                            activeErrorsArr.push(item);
                        } else {
                            activeUsersErrors.indexOf(item) != -1 ? activeErrorsArr.push(item) : 0;
                        }
                    }
                }
            }
        }
        onlineErrorsArr.length ? statusObj.onlineError = onlineErrorsArr : 0;
        activeErrorsArr.length ? statusObj.activeError = activeErrorsArr : 0;
        createStatus(contextLinksObject, statusObj, currentTime);
    }
    var contextLinksObject = updateContextItems(contextLinksObject, rootListElement.children);
    sockErrorInit = function () { // ��������� ������ ����������� �� WebSocket
        var usersObject = usersOnListern;
        statusInitial(usersObject.online, usersObject.active); // �������
    };
    sockErrorClose = function(event, usersArr) { // cyclicRequests.js sock onopen() --> 
        // sockErrorClose -> manageStatus --> server(startListernStatus), � ����� ������ �� �������
        // cyclicRequests.js(acceptListernUserStatus) --> statusInitial() --> createStatus()
        // usersArr ����� �������������� ������ ���� ������� ���������� �� ��������� ������������� ��������
        var newUsers = usersArr || Object.keys(contextLinksObject);
        var newUsers = newUsers.removeItem(requestUserId);
        var listenUsers = usersOnListern;
        var curOnline = listenUsers.online;
        var curActive = listenUsers.active;
        var connect = listenUsers.connect;
        var sendObjArr = [];
        var connectedUsers = curOnline.difference(curActive);
        var connectedUsers = newUsers.difference(connectedUsers);
        var usersToConnect = connectedUsers.union(connect);
        usersToConnect.length ? sendObjArr.push({
            "type": "startListernStatus", "users": usersToConnect, "callback": function () {
                var usersToConnect = usersOnListern.connect; if (usersToConnect.length) {
                    var usersToConnect = usersToConnect.difference(newUsers);
                    usersOnListern.connect = usersToConnect;
                }
            }
        }) : 0;
        var disconnect = listenUsers.disconnect;
        var disconnect1 = connectedUsers.difference(newUsers);
        if (!event && !usersArr) {
            /* "���������" �������������, � �������� ������� �� ���������� � ����������� ���� �� ��������, ��� ���� �� ������� �������� */
            statusInitial(newUsers.intersection(curOnline), newUsers.intersection(curActive), [], []);
            disconnect.union(disconnect1);
        }
        disconnect.length ? sendObjArr.push({ "type": "endListernStatus", "users": disconnect }) : 0;
        if (sendObjArr.length > 0) {
            sendData(event ? event.target : sock, sendObjArr);
            console.log(sendObjArr);
        }
    };
    sock.readyState == 1 ? sockErrorClose() : 0; // ���� ��� ������� ���������� �� XHR � �������� �����, �� ���; 
    //����� ��� ������� � ���������� ������� sockErrorClose
    return mainSockListener;
};
var mainSockListener = initUsersList();
if (typeof (userSock) != "undefined") {
        userSock.onmessage = mainSockListener;
    }