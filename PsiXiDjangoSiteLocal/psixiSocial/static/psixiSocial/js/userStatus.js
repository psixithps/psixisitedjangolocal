var sockErrorInit; // ������� ������ ������ ����������� �� WebSocket
var sockErrorClose; // ������� ���������� ������ ����������� �� WebSocket
if (!usersOnListern) {
    var usersOnListern = { // � ������ ������ ������ ���� ������������� �������������
        "online": [],
        "active": [],
        "connect": [], // �� ������� � �����������
        "disconnect": [] // �� ������� � ����������
    };
}
var sendData = (function () {
    var sendDataBroker = []; // �������� ������� ��������� ��������� �� ��������
    function send(s, messageData) {
        function filterEquals(arr) {
            for (var i = arr.length; i--;) {
                var first = true;
                var arr1 = arr.map(function(item, index, arr) {
                    var resultItem = !first ? JSON.stringify(arr[i]) != JSON.stringify(item) ? item : null : item;
                    first = !first ? false : first;
                    return resultItem;
                });
            }
            return arr1;
        };
        if (s.readyState == 1) {
            if (messageData !== true) {
                if (messageData.constructor === Array) {
                    var data = messageData.concat();
                    var callbackArr = [];
                    for (var r = data.length; r--;) {
                        var callback = data[r].pop("callback");
                        callback ? callbackArr.push(callback) : 0;
                    }
                    try {
                        s.send(JSON.stringify(data));
                    } catch (error) {
                        arguments.callee();
                        return;
                    }
                } else {
                    var callback = messageData.pop("callback");
                    try {
                        s.send(JSON.stringify(messageData));
                    } catch (error) {
                        messageData.callback = callback;
                        arguments.callee();
                        return;
                    }
                    callback ? callback() : 0;
                }
            } else {
                var arr = filterEquals(sendDataBroker);
                if (arr) {
                    var arr1 = arr.filter(function (item) {
                        var callback = item.pop("callback");
                        try {
                            s.send(JSON.stringify(item));
                        } catch (error) {
                            item.callback = callback;
                            return item;
                        }
                        callback ? callback() : 0;
                    });
                    sendDataBroker = arr1;
                }
            }
        } else {
            messageData !== true ? messageData.constructor == Object ?
                sendDataBroker.unshift(messageData) : sendDataBroker.update(messageData) : 0;
            setTimeout(function () { sendData(userSock, true); }, 1500);
        }
    };
    return send;
})();