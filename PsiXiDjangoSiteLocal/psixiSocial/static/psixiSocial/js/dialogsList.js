

function dialogs(requestId, timeStr, smallThumbsObject, onlineStatusObject, activeStatusObject) {
    var doc = document;
    var sock = userSock;
    var dialogsList = doc.getElementById("dialogs");
    var time = (function (timeStr) {
        var timeDate = new Date(Date.parse(timeStr));
        return timeDate;
    })(timeStr);
    /* ��������� ������������� �������� �� �������� ��� �������� ������ */
    var messagesObject = {};
    var statusContextObject = {};
    var statusContextObjectSimple = {};
    function updateDialogsIds(d, listRoot, canvasSizeX, canvasSizeY) {
        /* dialogsObject = {dialogId: [
         * ������_��_li_��������, 
         * ������_��_ul_���������,
         * id_����������_�����������_���������
         * ],}
         * messagesObject = {messageId: ������_��_li_���������,}
         * statusContextObject = {userId: {dialogId: ������_��_CANVAS_��������},}
         * statusContextObjectSimple = {userId: ������_��_CANVAS_��������,}
          */
        var dialogsLinks = {};
        var messagesLinks = {};
        var statusLinks = {};
        var statusLinksSimple = {};
        var dialogs = listRoot.children;
        var dialogsLength = dialogs.length;
        if (dialogsLength > 0) {
            var canvasStatusItem = d.createElement("CANVAS");
            canvasStatusItem.setAttribute("width", canvasSizeX);
            canvasStatusItem.setAttribute("height", canvasSizeY);
            var joinObj = Object.assign;
        }
        for (var i = dialogsLength - 1; i--;) {
            var dialog = dialogs[i];
            var dialogId = dialog.getAttribute("value");
            var dialogHeadPlace = dialog.firstElementChild.firstElementChild;
            var dialogMembers = dialogHeadPlace.firstElementChild.children;
            var statusL = {};
            for (var u = dialogMembers.length - 1; u--;) {
                var dialogMember = dialogMembers[u].firstElementChild;
                var canvasItem = canvasStatusItem.cloneNode(false);
                dialogMember.appendChild(canvasItem);
                statusL[parseInt(dialogMember.getAttribute("value"))] = canvasItem.getContext("2d");
            }
            statusLinks[dialogId] = statusL;
            var statusLinksSimple = joinObj(statusLinksSimple, statusL);
            var messagesPlace = dialogHeadPlace.lastElementChild;
            var messagesPlaceChilds = messagesPlace.children;
            var childsLength = messagesPlaceChilds.length - 1;
            for (var y = 0; y <= childsLength; y++) {
                var message = messagesPlaceChilds[y];
                if (y == 0) {
                    var startRow = message.firstElementChild;
                }
                messagesLinks[message.getAttribute("value")] = message;
            }
            var lastSenderId = parseInt(startRow.firstElementChild.firstElementChild.getAttribute("value"));
            dialogsLinks[dialogId] = [dialog, messagesPlace, lastSenderId];
            if (i == 0) {
                messagesObject = messagesLinks;
                statusContextObject = statusLinks;
                statusContextObjectSimple = statusLinksSimple;
            }
        }
        return dialogsLinks;
    };
    var dialogsObject = updateDialogsIds(doc, dialogsList, 91, 91);
    /* end ��������� ������������� �������� �� �������� ��� �������� ������ */
    /* �������� ������� */
    function prepareActiveStatus(mainObj, activeObj, myId) {
        var o = Object;
        /* ���������� activeObj - ������ ������� �������, ���� ���� �������� 'stateMiddle': (datetimeStr, ���������_�����_���������_middle_�������) */
        var activeKeys = o.keys(activeObj);
        for (var i = activeKeys.length - 1; i >= 0; i--) {
            var key = activeKeys[i];
            var instance = activeObj[key];
            if (typeof (instance) === "object" && instance.constructor === o && instance.hasOwnProperty("stateMiddle")) {
                var valuesArr = instance["stateMiddle"].split(",");
                var middleTimeString = valuesArr[0];
                var middleTimeString = middleTimeString.slice(2, middleTimeString.length - 1);
                var middleTimeTotalString = valuesArr[1];
                var middleTimeTotalNumber = parseInt(middleTimeTotalString.slice(1, middleTimeTotalString.length - 1));
                activeObj[key]["stateMiddle"] = [middleTimeString, middleTimeTotalNumber];
            }
        }
        if (!mainObj.hasOwnProperty(myId)) {
            var online = "online"
            mainObj[myId] = online;
            onlineStatusObject[myId] = online;
        }
        /* ��� ������� ������� � activeStatus ������ ��������, ���� ��� ���� ���� �� � onlineStatus */
        var mainKeys = o.keys(mainObj);
        var newActiveObj = {};
        for (var i = mainKeys.length - 1; i >= 0; i--) {
            var key = mainKeys[i];
            !activeObj.hasOwnProperty(key) ? newActiveObj[key] = key == myId ? "stateActive" : "stateLeave" : 0;
        }
        var newActiveObj = o.assign(activeObj, newActiveObj);
        return newActiveObj;
    };
    function draw(context, xy, r, w, time, position, color, callback) {
        var counter = position;
        var p = Math.PI;
        context.beginPath();
        context.lineWidth = w;
        context.strokeStyle = color;
        var action = setInterval(function () {
            if (counter > 0) {
                var start = counter * p / 180;
                counter--;
                var end = counter * p / 180;
                context.arc(xy, xy, r, start, end, true);
                context.stroke();
            } else {
                context.closePath();
                callback ? callback() : 0;
                clearInterval(action);
            }
        }, time);
        return action;
    };
    function drawFast(context, xy, r, w, angle, color) {
        var context = context;
        context.beginPath();
        context.strokeStyle = color;
        context.lineWidth = w;
        context.arc(xy, xy, r, 0, angle * Math.PI / 180, false);
        context.stroke();
        context.closePath();
    };
    var onlineStatusActiveAnimations = {};
    var activeStatusActiveAnimations = {};
    function createStatus(contextLinksObject, statusObj, currentTime, center, radius, width) {
        var keys = Object.keys(statusObj);
        var Round = Math.round;
        var colorsObj = {
            "online": "#47b647",
            "stateActive": "#00AFFF",
            "stateLeave": "#D74620"
        };
        for (var i = keys.length - 1; i >= 0; i--) {
            var key = keys[i];
            var item = statusObj[key];
            var color = colorsObj[item] || null;
            if (color != null) {
                drawFast(contextLinksObject[key], center, radius, width, 360, color);
            } else {
                if (item.hasOwnProperty('iffy')) {
                    var seconds = Round((currentTime.getTime() - Date.parse(item.iffy)) / 1000);
                    var color = "#47b647";
                    var t = (20 / 360) - (20 - seconds);
                    var position = ((360 / 20) * t) * -1;
                    var context = contextLinksObject[key];
                    drawFast(context, center, radius, width, position, color);
                    setTimeout(function () {
                        onlineStatusActiveAnimations[key] = draw(context, center, radius, width, (20000 / 360), position, "#FFF5F5",
                            function () {
                                delete onlineStatusActiveAnimations[key];
                                drawFast(context, center, 38.5, 8, 360, "#FFF5F5");
                            });
                    }, 25);
                } else {
                    var middleData = item.stateMiddle;
                    var middleTimeTotal = middleData[1];
                    var seconds = Round((currentTime.getTime() - Date.parse(middleData[0])) / 1000);
                    var t = (middleTimeTotal / 360) - (middleTimeTotal - seconds);
                    var position = ((360 / middleTimeTotal) * t) * -1;
                    var context = contextLinksObj[key];
                    drawFast(context, center, radius, width, 360, "#D74620");
                    drawFast(context, center, radius, width, position, "#FF7f00");
                    setTimeout(function () {
                        activeStatusActiveAnimations[key] = draw(context, center, radius, width, ((middleTimeTotal * 1000) / 360), position, "#D74620",
                            function () {
                                delete activeStatusActiveAnimations[key];
                            });
                    }, 25);
                }
            }
        }
    };
    var activeStatusObject = prepareActiveStatus(onlineStatusObject, activeStatusObject, requestId);
    //createStatus(statusContextObjectSimple, onlineStatusObject, time, 46.5, 42.5, 4);
    //createStatus(statusContextObjectSimple, activeStatusObject, time, 46.5, 38.5, 4);
    /* end �������� ������� */
    function clickHandler(event) {
        var target = event.target;
        if (target.nodeName != "A") {
            var link = searchP(target, "A");
        } else {
            var link = target;
        }
        if (link) {
            localNavigation(event, link.href);
        }
        event.stopPropagation();
    };
    if (doc.addEventListener) {
        dialogsList.addEventListener("click", clickHandler, false);
    } else {
        dialogsList.attachEvent("click", clickHandler, false);
    }
    function createMessage(doc, messageDataObj, byId, lastSenderId) {
        var myId = requestId;
        var messageId = messageDataObj.id;
        var messageWrap = doc.createElement("LI");
        messageWrap.setAttribute("value", messageId);
        var messageProps = doc.createElement("DIV");
        var messageBody = messageProps.cloneNode(false);
        messageProps.className = "message-properties";
        messageBody.className = "message-textbody";
        var timeSmall = doc.createElement("SMALL");
        var messageP = doc.createElement("P");
        messageP.innerText = messageDataObj.body;
        timeSmall.innerText = time.getTime();
        if (lastSenderId != byId) {
            messageWrap.innerHTML = smallThumbsObject[byId];
            byId == myId ? messageWrap.className = "my-message start-group unread" : messageWrap.className = "not-my-message start-group unread";
        } else {
            byId == myId ? messageWrap.className = "my-message unread" : messageWrap.className = "not-my-message unread";
        }
        messageBody.appendChild(messageP);
        messageProps.appendChild(timeSmall);
        messageWrap.appendChild(messageBody);
        messageWrap.appendChild(messageProps);
        messagesObject[messageId] = messageWrap;
        return messageWrap;
    };
    var mainSockListener = function (event) {
        var sockData = event.data || null;
        if (sockData) {
            var data = JSON.parse(sockData);
        } else {
            return;
        }
        var data = data.constructor === Array ? data : [data];
        var obj = null;
        for (var i = data.length - 1; i >= 0; i--) {
            var obj = data[i];
            if (obj.hasOwnProperty("personalMessageRead")) {
                var allMessagesLinks = messagesObject;
                var messagesRead = obj.personalMessageRead;
                for (var i = messagesRead.length - 1; i >= 0; i--) {
                    var element = allMessagesLinks[messagesRead[i]] || null;
                    if (element) {
                        element.className = element.className.replace(" unread", "");
                    }
                }
                data.splice(i, 1);
            } else {
                var method = obj.method;
                if (method == 'messagePersonal') {
                    var dialogId = obj.dialogId;
                    var dialogsObj = dialogsObject;
                    if (dialogsObj.hasOwnProperty(dialogId)) {
                        var dialogItem = dialogsObj[dialogId];
                        var byId = obj.byId;
                        var dialogElement = dialogItem[0];
                        var messagesRaw = dialogItem[1];
                        var lastSenderId = dialogItem[2];
                        var newMessage = createMessage(doc, obj, byId, lastSenderId);
                        var lastMessage = messagesRaw.lastElementChild || null;
                        if (lastMessage) {
                            var messagesRawChilds = messagesRaw.children;
                            if (messagesRawChilds.length < 6) {
                                messagesRaw.appendChild(newMessage);
                            } else {
                                var secondMessage = messagesRawChilds[1];
                                if (secondMessage.firstElementChild.className != "user-thumb") {
                                    var findFirstThumb = messagesRaw.getElementsByClassName("user-thumb")[0];
                                    secondMessage.className += " start-group";
                                    secondMessage.insertBefore(findFirstThumb, secondMessage.firstElementChild);
                                }
                                messagesRaw.removeChild(messagesRawChilds[0]);
                                messagesRaw.appendChild(newMessage);
                            }
                        } else {
                            messagesRaw.appendChild(newMessage);
                        }
                        dialogsObj[dialogId][2] = byId;
                        var dialogsPlace = dialogsList;
                        var firstDialog = dialogsPlace.firstElementChild;
                        firstDialog != dialogElement ? dialogsPlace.insertBefore(dialogElement, firstDialog) : 0;
                    } else {
                        //
                    }
                    data.splice(i, 1);
                } else if (method == 'stateOnline' || method == 'stateTimer' || method == 'stateLeave') {
                    var userId = obj.userId;
                    var requestUserId = requestId;
                    var contextObj = statusContextObjectSimple;
                    if (!requestUserId || contextObj.hasOwnProperty(userId)) {
                        var onlineStatusAnimationsObj = onlineStatusActiveAnimations;
                        if (method == 'stateOnline') {
                            var mainContext = contextObj[userId];
                            if (onlineStatusAnimationsObj && onlineStatusAnimationsObj.hasOwnProperty(userId)) {
                                clearInterval(onlineStatusAnimationsObj[userId]);
                                delete onlineStatusAnimationsObj[userId];
                            } else {
                                drawFast(mainContext, 46.5, 38.5, 4, 360, "#00AFFF");
                            }
                            drawFast(mainContext, 46.5, 42.5, 4, 360, "#47b647");
                        } else if (method == 'stateTimer') {
                            if (onlineStatusAnimationsObj && !onlineStatusAnimationsObj.hasOwnProperty(userId)) {
                                var time = 20000 / 360;
                                var mainContext = contextObj[userId];
                                onlineStatusAnimationsObj[userId] = draw(mainContext, 46.5, 42.5, 4, time, 360, "#FFF5F5",
                                    function () {
                                        delete onlineStatusAnimationsObj[userId];
                                        drawFast(mainContext, 46.5, 38.5, 8, 360, "#FFF5F5");
                                    });
                            }
                        } else if (method == 'stateLeave') {
                            if (onlineStatusAnimationsObj && onlineStatusAnimationsObj.hasOwnProperty(userId)) {
                                clearInterval(onlineStatusAnimationsObj[userId]);
                                delete onlineStatusAnimationsObj[userId];
                            }
                            drawFast(contextObj[userId], 46.5, 38.5, 8, 360, "#FFF5F5");
                        }
                    }
                    data.splice(i, 1);
                } else if (method == "stateActive" || method == "stateMiddle" || method == "stateLeave") {
                    var userId = obj.id;
                    var requestUserId = requestId;
                    var contextObj = statusContextObjectSimple;
                    if (!requestUserId || contextObj.hasOwnProperty(userId)) {
                        var activeStatusAnimationsObj = activeStatusActiveAnimations;
                        if (method == "stateActive") {
                            if (activeStatusAnimationsObj && activeStatusAnimationsObj.hasOwnProperty(userId)) {
                                clearInterval(activeStatusAnimationsObj[userId]);
                                delete activeStatusAnimationsObj[userId];
                            }
                            drawFast(contextObj[userId], 46.5, 38.5, 4, 360, "#00AFFF");
                        } else if (method == "stateMiddle") {
                            if (activeStatusAnimationsObj && !activeStatusAnimationsObj.hasOwnProperty(userId)) {
                                var context = contextObj[userId];
                                drawFast(context, 46.5, 38.5, 4, 360, "#FF7f00");
                                activeStatusAnimationsObj[userId] = draw(context, 46.5, 38.5, 4, 210000 / 360, 360, "#D74620");
                            }
                        } else {
                            if (activeStatusAnimationsObj && activeStatusAnimationsObj.hasOwnProperty(userId)) {
                                clearInterval(activeStatusAnimationsObj[userId]);
                                delete activeStatusAnimationsObj[userId];
                            }
                            drawFast(contextObj[userId], 46.5, 38.5, 4, 360, "#D74620");
                        }
                    }
                    data.splice(i, 1);
                }
            }
        }
        data.length > 0 ? defaultListener(data) : 0;
    };
    if (sock !== null) {
        sock.onmessage = mainSockListener;
    }
    return mainSockListener;
};
var mainSockListener = initDialogsList();