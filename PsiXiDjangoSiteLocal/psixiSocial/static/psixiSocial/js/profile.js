

function profile(myUserId, profilePageId, statusTextObject, onlineStatusObject, currentTimeStr, friendsStatusObj, loadThumbUrl) {
    var sock = userSock;
    var currentTime = new Date(currentTimeStr);
    var doc = document;
    var profileElement = doc.getElementById("profile");
    var friendsUl = profileElement.getElementsByClassName("friends-list")[0];
    var friendsOnlineUl = profileElement.getElementsByClassName("friends-online-list")[0] || null;
    var friendsLength = friendsUl.childElementCount;
    var friendsEmptyLi = null;
    var friendsOnlineEmptyLi = null; // Хранение <li> - пустой элемент с текстом
    var friendsAllLi = null; // Хранение <div> - кликабельный блок со ссылкой на список
    var friendsOnlineAllLi = null; // Хранение <div> - кликабельный блок со ссылкой на список
    if (friendsLength == 1) {
        var friendsUlEmpty = friendsUl.firstElementChild.className;
        var friendsUlEmpty = friendsUlEmpty == "empty" ? true : friendsUlEmpty == "hidden" ? null : false; // Если null - значит список скрыт приватностью
    } else {
        var friendsUlEmpty = false;
    }
    if (friendsOnlineUl) {
        var friendsOnlineLength = friendsOnlineUl.childElementCount;
        var friendsOnlineUlEmpty = friendsOnlineLength == 1 ? friendsOnlineUl.firstElementChild.className == "empty" ? true : false : false;
        var friendsOnlineLength = friendsOnlineUlEmpty ? 0 : friendsOnlineLength;
        var friendsOnlineHead = friendsOnlineUl.previousElementSibling.firstElementChild; // Хранение <h3> со счётчиком в оглавлении списка
    } else {
        var friendsOnlineLength = null; // Если null - значит список скрыт приватностью
        var friendsOnlineUlEmpty = true;
    }
    var userThumbsCache = {};
    function removeUser() {
    };
    function startIffyAnimationUser() {

    };
    function checkUserInList(id, idArr) {
        return idArr.some(function(item) {
            return item == id;
        });
    };
    function setInitialStatus(doc, statusPlace, statusPlaceText, onlineStatusObject) {
        if ('lastVisitTime' in onlineStatusObject || 'iffy' in onlineStatusObject) {
            if ('lastVisitTime' in onlineStatusObject) {
                var lastVisitDate = new Date(onlineStatusObject.lastVisitTime);
                if ((currentTime.getMonth() - lastVisitDate.getMonth()) == 0) {
                    var secondCounter = Math.round((currentTime.getTime() - lastVisitDate.getTime()) / 1000);
                    timerWrapper(doc, statusPlace, statusPlaceText, statusTextObject, false, secondCounter, true);
                }
            } else {
                var iffyTimeStr = onlineStatusObject.iffy.split(",")[0];
                var iffyTimeStr = iffyTimeStr.slice(2, iffyTimeStr.length - 1);
                var iffyDate = new Date(onlineStatusObject.iffy);
                var secondCounter = Math.round((currentTime.getTime() - iffyDate.getTime()) / 1000);
                timerWrapper(doc, statusPlace, statusPlaceText, statusTextObject, true, secondCounter, false);
            }
        }
    };
    function setClickHandlers(d, profile) {
        function handler(event) {
            var target = event.target;
            var form = searchP(target, "FORM");
            if (form) {
                if (form.method == "get") {
                    localNavigation(event, target.getAttribute('formaction'));
                } else {
                    doPostFormSimplifield(d, target.getAttribute('formaction'), form, sock || false);
                    event.preventDefault();
                }
            }
            event.stopPropagation();
        };
        if (d.addEventListener) {
            profile.addEventListener("click", handler, false);
        } else {
            profile.attachEvent("click", handler, false);
        }
    };
    setClickHandlers(doc, profileElement);
    if (profilePageId) {
        var statusPlace = profileElement.getElementsByClassName("online-status");
        var statusPlace = statusPlace.length != 0 ? statusPlace[0] : null;
        if (statusPlace) {
            var statusPlaceText = statusPlace.firstElementChild;
            typeof (onlineStatusObject) != "string" ? setInitialStatus(doc, statusPlace, statusPlaceText, onlineStatusObject) : 0;
        }
    }
    /*
    isShort - true = обратный таймер при неопределённом статусе пользователя(считает от 20 до 0), counter - текущее начальное значение в секундах,
    initial - запустить таймер и сразу же установить значение в элементе документа (начальный пуск)
    */
    function timerWrapper(doc, statusPlace, statusPlaceText, statusTextObject, isShort, counter, initial) {
        function setTimer(statusPlace, statusPlaceText, statusTextObject, countrebleArr, counter, step) {
            profile.timer = setInterval(function() {
                if (isShort) {
                    setShortTimerTime(statusTextObject, statusPlace, statusPlaceText, counter);
                    if (counter > 1) {
                        counter--;
                    } else {
                        profile.stop();
                        timerWrapper(doc, statusPlace, statusPlaceText, statusTextObject, false, 20, false);
                    }
                } else {
                    var data = checkTimer(counter, statusTextObject);
                    var newStep = data[0];
                    countrebleArr = data[1];
                    counterText = data[2];
                    if (step != newStep) {
                        step = newStep;
                        profile.stop();
                        setTimer(statusPlace, statusPlaceText, statusTextObject, countrebleArr, counter, step);
                    }
                    setTime(statusPlace, statusPlaceText, statusTextObject, countrebleArr, counterText);
                    counter += step;
                }
            }, step * 1000);
        };
        profile.stop = function () {
            clearInterval(profile.timer);
            profile.timer = null;
        };
        var data = checkTimer(counter, statusTextObject);
        var step = data[0];
        var countrebleArr = data[1];
        var counterText = data[2];
        initial ? setTime(statusPlace, statusPlaceText, statusTextObject, countrebleArr, counterText) : 0;
        setTimer(statusPlace, statusPlaceText, statusTextObject, countrebleArr, counter, step);
        function setShortTimerTime(statusTextObject, statusPlace, statusPlaceText, counter) {
            statusPlaceText.innerText = statusTextObject.uknownStatus + " " + counter;
        };
        function setTime(statusPlace, statusPlaceText, textObject, countrebleArr, counterText) {
            var counterStr = "" + counterText;
            var counterStrLength = counterStr.length;
            var counterLast = parseInt(counterStr.slice(counterStrLength - 1, counterStrLength));
            var position = counterText >= 10 && counterText <= 20 ? 2 : counterLast != 0 ? counterLast != 1 ? counterLast < 5 ? 1 : 2 : 0 : 2;
            statusPlaceText.innerText = textObject.offlineStatus + " " + counterText + " " +
                countrebleArr[position] + " " + textObject.timeEndString;
        };
        function checkTimer(counter, textObject) {
            var day = 0;
            var hour = 0;
            var minute = 0;
            var second = 0;
            if (counter < 2678400) { // 1-31 день
                if (counter < 86400) {
                    if (counter < 3600) {
                        if (counter < 60) {
                            var countrebleArr = textObject.sec;
                            var second = counter;
                            var step = 1;
                        } else {
                            var countrebleArr = textObject.min;
                            var minute = Math.round(counter / 60);
                            var step = 60;
                        }
                    } else {
                        var countrebleArr = textObject.hour;
                        var hour = Math.round(counter / 60 / 60);
                        var step = 3600;
                    }
                } else {
                    var countrebleArr = textObject.day;
                    var day = Math.round(counter / 60 / 60 / 24);
                    var step = 86400;
                }
            }
            return [step, countrebleArr, day + hour + minute + second];
        };
    };
    function createSmallThumb(d, thumbStr) {
        var wrap = d.createElement("DIV");
        wrap.innerHTML = thumbStr;
        var thumb = wrap.firstElementChild;
        return thumb;
    };
    function insertNewFriend(thumb, ul, length, isEmpty) {
        if (length > 19) {
            if (isEmpty) {
                var first = ul.firstElementChild;
                friendsEmptyLi = first;
                ul.replaceChild(thumb, first);
            } else {
                ul.replaceChild(thumb, ul.firstElementChild);
            }
        } else {
            if (isEmpty) {
                var first = ul.firstElementChild;
                friendsEmptyLi = first;
                ul.replaceChild(thumb, first);
            } else {
                ul.insertBefore(thumb, ul.firstElementChild);
            }
        }
        return thumb;
    };
    function insertNewFriendOnline(d, thumb, friendsUl, ulOnline, data) {
        var first = ulOnline.firstElementChild || null;
        ulOnline.insertBefore(thumb, first);
        if (friendsOnlineUlEmpty) {
            friendsOnlineEmptyLi = first.cloneNode(true);
            ulOnline.removeChild(first);
        }
        var onlineUsersCount = friendsOnlineLength;
        if (onlineUsersCount) {
            var headText = friendsOnlineHead.innerText;
            var dottsIndex = headText.indexOf(":");
            friendsOnlineHead.innerText = dottsIndex != -1 ? headText.slice(0, dottsIndex + 2) + (onlineUsersCount + 1) : headText + ": 1";
        }
        return thumb;
    };
    function removeFriend(d, id, profile) {
        var friendsUl = profile.getElementsByClassName("friends-list")[0];

    };
    function removeFriendOnline(id, ul, itemToRemove) {
        userThumbsCache[id] = itemToRemove.cloneNode(true);
        ul.removeChild(itemToRemove);
        var onlineUsersCount = friendsOnlineLength;
        if (onlineUsersCount) {
            var headText = friendsOnlineHead.innerText;
            if (onlineUsersCount > 1) {
                friendsOnlineHead.innerText = headText.slice(0, headText.indexOf(":") + 2) + (onlineUsersCount - 1);
            } else {

            }
        }
        delete contextLinksFriendsOnlineObject[id];
    };
    var contextLinksFriendsObject = {};
    var contextLinksFriendsOnlineObject = {};
    var activeAnimationsFriends = {};
    var activeAnimationsFriendsOnline = {};
    /* pushUser */
    function createUsersList() {

    };
    /*function loadThumbsXHR(d, toUserId, ul, ulOnline, isEmpty, isEmptyOnline) {
        var friendsL = friendsLength;
        var friendsOnlineL = friendsOnlineLength;
        if (isEmptyOnline && ulOnline) {
            friendsOnlineEmptyLi = ulOnline.firstElementChild;
        }
        function checkpageElementsXHR(friendsLen, friendsOnlineLen) {
            var urlParams = null;
            if (friendsLen == 19) {
                var urlParams = "&optns=[friendsulall";
            }
            if (!friendsOnlineLen) {
                var urlParams = urlParams ? urlParams + ",friendsulonlinecreate]" : "&optns=[friendsulonlinecreate]";
            } else {
                if (friendsOnlineLen == 19) {
                    var urlParams = urlParams ? urlParams + ",friendsulonlineall" : "&optns=[friendsulonlineall";
                } else {
                    var urlParams = urlParams ? urlParams + "]" : urlParams;
                }
            }
            return urlParams;
        };
        var params = checkpageElementsXHR(friendsL, friendsOnlineL);
        var req = new XMLHttpRequest();
        var path = loadThumbUrl;
        req.open("GET", path + "?id=" + toUserId + (params ? params : ""), false);
        req.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        req.onloadend = function (event) {
            var data = JSON.parse((this || event.target).response);
            var thumb = createSmallThumb(d, data.thumb);
            var thumbOnline = thumb.cloneNode(true);
            if (thumb) {
                var cTime = currentTime;
                var status = data.status;
                var thumb = insertNewFriend(thumb, ul, friendsL, isEmpty); // friends
                var contextLinksFriends = contextLinksFriendsObject;
                var contextLinksFriendsNew = updateContextItems(contextLinksFriends, thumb);
                var newStatusObj = {};
                newStatusObj[toUserId] = data.status;
                createStatus(contextLinksFriendsNew, newStatusObj, cTime);
                contextLinksFriendsObject = Object.assign(contextLinksFriends, contextLinksFriendsNew);
                if (status) {
                    var templateData = data.dataDicts || null;
                    var thumbOnline = insertNewFriendOnline(d, thumbOnline, ul, ulOnline, templateData) // friends-online
                    var contextLinksFriendsOnline = contextLinksFriendsOnlineObject;
                    var contextLinksFriendsOnlineNew = updateContextItems(contextLinksFriendsOnline, [thumbLi]);
                    createStatus(contextLinksFriendsOnlineNew, newStatusObj, cTime);
                    contextLinksFriendsOnlineObject = Object.assign(contextLinksFriendsOnline, contextLinksFriendsOnlineNew);
                }
            }
        };
        req.send(null);
    };*/
    function mainSockListener(event) {
        function checkpageElements(friendsLen, friendsOnlineLen) {
            var urlParams = [];
            if (friendsLen == 19) {
                urlParams.push("friendsulall");
            }
            if (!friendsOnlineLen) {
                urlParams.push("friendsulonlinecreate");
            } else {
                if (friendsOnlineLen == 19) {
                    urlParams.push("friendsulonlineall");
                }
            }
            return urlParams;
        };
        var data = event.data || false;
        if (!data) {
            var data = event.target || false;
            var data = data.response ? JSON.parse(data.data) : data;
        } else {
            var data = JSON.parse(data);
        }
        if (data.nodeName && data.className != "wallPostForm") {
            return;
        }
        var d = doc;
        data.constructor === Array ? 0 : data = [data];
        for (var i = 0; i < data.length; i++) {
            var obj = data[i];
            var method = obj.method;
            if (method == 'stateOnline' || method == 'stateTimer' || method == 'stateLeave') {
                var userId = obj.userId;
                var myId = myUserId;
                var profileId = profilePageId;
                if (myId ? myId != userId : true && profileId != userId) {
                    var context = contextLinksFriendsObject[userId] || null;
                    var contextOnline = contextLinksFriendsOnlineObject[userId] || null;
                    if (method == 'stateOnline') {
                        if (context) {
                            if (activeAnimationsFriends.hasOwnProperty(userId)) {
                                var animation = activeAnimationsFriends[userId];
                                clearInterval(animation);
                                delete animation;
                            }
                            drawFast(context, 360, "#47b647");
                        }
                        if (contextOnline) {
                            if (activeAnimationsFriendsOnline.hasOwnProperty(userId)) {
                                var animation = activeAnimationsFriendsOnline[userId];
                                clearInterval(animation);
                                delete animation;
                            }
                            drawFast(contextOnline, 360, "#47b647");
                        } else {
                            var cachedThumb = userThumbsCache[userId] || null;
                            if (cachedThumb) {
                                var cTime = currentTime;
                                var templateData = null;
                                var cachedThumb = insertNewFriendOnline(d, cachedThumb, friendsUl, friendsOnlineUl, templateData);
                                var contextLinksFriendsOnline = contextLinksFriendsOnlineObject;
                                var newStatusObj = {};
                                newStatusObj[userId] = 'online';
                                var contextLinksFriendsOnlineNew = updateContextItems(contextLinksFriendsOnline, [cachedThumb]);
                                createStatus(contextLinksFriendsOnlineNew, newStatusObj, cTime);
                                contextLinksFriendsOnlineObject = Object.assign(contextLinksFriendsOnline, contextLinksFriendsOnlineNew);
                            } else {
                                sock.send(JSON.stringify({ 'type': 'getSmallProfileThumb', 'target': userId, 'profile': profileId || myId }));
                            }
                        }

                    } else if (method == 'stateTimer') {
                        if (context) {
                            if (!activeAnimationsFriends.hasOwnProperty(userId)) {
                                draw(context, 20000 / 360, userId, 360);
                            }
                        }
                        if (contextOnline) {
                            if (!activeAnimationsFriendsOnline.hasOwnProperty(userId)) {
                                var item = contextOnline.canvas.parentNode;
                                draw(contextOnline, 20000 / 360, userId, 360, friendsOnlineUl, item);
                            }
                        }
                    } else {
                        if (context) {
                            if (activeAnimationsFriends.hasOwnProperty(userId)) {
                                var animation = activeAnimationsFriends[userId];
                                clearInterval(animation);
                                delete animation;
                            }
                            drawFast(context, 360, "#FAFAFA");
                        }
                        if (contextOnline) {
                            if (activeAnimationsFriendsOnline.hasOwnProperty(userId)) {
                                var animation = activeAnimationsFriendsOnline[userId];
                                clearInterval(animation);
                                delete animation;
                            }
                            delete contextLinksFriendsOnlineObject[userId];
                            friendsOnlineUl.removeChild(contextOnline.canvas.parentNode);
                        }
                    }
                }
                if (profileId == userId || (!profileId && myUserId == userId)) {
                    var timer = profile.timer || null;
                    if (method == 'stateOnline') {
                        timer ? profile.stop() : 0;
                        statusPlace.removeAttribute("title");
                        statusPlaceText.innerText = statusTextObject.onlineStatus;
                    } else if (method == 'stateTimer') {
                        timer ? profile.stop() : 0;
                        timerWrapper(d, statusPlace, statusPlaceText, statusTextObject, true, 20, false);
                    } else {
                        var outTime = new Date(Date.parse(obj.time));
                        statusPlace.setAttribute("title", outTime.toLocaleDateString() + " " + outTime.toLocaleTimeString());
                        if (!timer) {
                            timerWrapper(d, statusPlace, statusPlaceText, statusTextObject, false, outTime.getTime() / 1000, true);
                        }
                    }
                }
                data.splice(i, 1);
                i -= 1;
            } else if (method == "returnSmallProfileThumb") {
                var thumbStr = obj.thumb;
                if (thumbStr) {
                    var cTime = currentTime;
                    var thumb = createSmallThumb(d, thumbStr);
                    var templateData = obj.dataDicts || null;
                    var thumb = insertNewFriendOnline(d, thumb, friendsUl, friendsOnlineUl, templateData);
                    var contextLinksFriendsOnline = contextLinksFriendsOnlineObject;
                    var newStatusObj = {};
                    newStatusObj[obj.userId] = 'online';
                    var contextLinksFriendsOnlineNew = updateContextItems(contextLinksFriendsOnline, [thumb]);
                    createStatus(contextLinksFriendsOnlineNew, newStatusObj, cTime);
                    contextLinksFriendsOnlineObject = Object.assign(contextLinksFriendsOnline, contextLinksFriendsOnlineNew);
                }
                data.splice(i, 1);
                i -= 1;
            } else if (method == "excludeFriend") {
                var toUserId = obj.userId;
                var profileId = profilePageId;
                if (profileId == toUserId || (!profileId && myUserId == toUserId)) {
                    removeFriend(d, toUserId, profileElement);
                }
            } else if (method == "getAddedFriend") { // Отправка данных для получения thumb
                var toUserId = obj.userId;
                var profileId = profilePageId;
                if (profileId == toUserId || (!profileId && myUserId == toUserId)) {
                    var optionsArr = checkpageElements(friendsLength, friendsOnlineLength);
                    if (optionsArr.length > 0) {
                        sock.send(JSON.stringify({ 'type': 'smallUserThumbAddedFriend', 'id': obj.id, 'optns': optionsArr }));
                    } else {
                        sock.send(JSON.stringify({ 'type': 'smallUserThumbAddedFriend', 'id': obj.id }));
                    }
                }
                data.splice(i, 1);
                i -= 1;
            } else if (method == "returnAddedFriend") {
                var thumbStr = obj.thumb;
                if (thumbStr) {
                    var thumb = createSmallThumb(d, thumbStr);
                    var userId = obj.userId;
                    var cTime = currentTime;
                    var ul = friendsUl;
                    var status = obj.status;
                    var thumb = insertNewFriend(thumb, ul, friendsLength, friendsUlEmpty); // friends
                    var thumbOnline = thumb.cloneNode(true);
                    var contextLinksFriends = contextLinksFriendsObject;
                    var contextLinksFriendsNew = updateContextItems(contextLinksFriends, thumb);
                    var newStatusObj = {};
                    newStatusObj[userId] = status;
                    createStatus(contextLinksFriendsNew, newStatusObj, cTime);
                    contextLinksFriendsObject = Object.assign(contextLinksFriends, contextLinksFriendsNew);
                    if (status) {
                        var templateData = obj.dataDicts || null;
                        var thumbOnline = insertNewFriendOnline(d, thumbOnline, ul, friendsOnlineUl, templateData) // friends-online
                        var contextLinksFriendsOnline = contextLinksFriendsOnlineObject;
                        var contextLinksFriendsOnlineNew = updateContextItems(contextLinksFriendsOnline, [thumbOnline]);
                        createStatus(contextLinksFriendsOnlineNew, newStatusObj, cTime);
                        contextLinksFriendsOnlineObject = Object.assign(contextLinksFriendsOnline, contextLinksFriendsOnlineNew);
                    }
                }
                data.splice(i, 1);
                i -= 1;
            }
        }
        data.length > 0 ? defaultListener(data) : 0;
    };
    function draw(context, time, id, position, ul, itemToRemove) {
        var counter = position;
        context.beginPath();
        context.strokeStyle = "#FAFAFA";
        var math = Math;
        var p = math.PI;
        var cos = math.cos;
        var sin = math.sin;
        var action = setInterval(function() {
            if (counter > 0) {
                counter--;
                var radians = counter * p / 180;
                var x = 27 + (cos(radians) * 27);
                var y = 27 + (sin(radians) * 27);
                context.moveTo(27, 27);
                context.lineTo(x, y);
                context.stroke();
            } else {
                context.closePath();
                itemToRemove ? removeFriendOnline(id, ul, itemToRemove) : 0;
                delete activeAnimationsFriendsOnline[id];
                delete activeAnimationsFriends[id];
                clearInterval(action);
            }
        }, time);
        activeAnimationsFriends[id] = action;
    };
    function drawFast(context, angle, color) {
        context.beginPath();
        context.strokeStyle = color;
        context.lineWidth = 2;
        context.arc(27, 27, 25, 0, angle * Math.PI / 180, false)
        context.stroke();
        context.closePath();
    };
    function createStatus(contextLinksObject, statusObj, time, ul) {
        var time = time;
        var keys = Object.keys(statusObj);
        var round = Math.round;
        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            var item = statusObj[key];
            if (item == "online") {
                drawFast(contextLinksObject[key], 360, "#47b647");
            } else {
                var seconds = round((time.getTime() - Date.parse(item['iffy'])) / 1000);
                var t = (20 / 360) - (20 - seconds);
                var position = ((360 / 20) * t) * -1;
                var context = contextLinksObject[key];
                drawFast(context, position, "#47b647");
                ul ? draw(context, 20000 / 360, key, position, ul, context.canvas.parentNode) :
                    draw(context, 20000 / 360, key, position);
            }
        }
    };
    function updateContextItems(contextLinksObject, elements) {
        var contextElement = doc.createElement("CANVAS");
        contextElement.className = "status-animation";
        contextElement.setAttribute("width", 54);
        contextElement.setAttribute("height", 54);
        function createCanvasContexts(element) {
            var content = element.getContext("2d");
            return content;
        };
        var elementsLength = elements.length;
        for (var i = 0; i < elementsLength; i++) {
            var element = elements[i];
            var context = contextElement.cloneNode(false);
            contextLinksObject[element.value] = createCanvasContexts(context);
            element.insertBefore(context, element.firstElementChild);
        }
        return contextLinksObject;
    };
    if (friendsUlEmpty == false) {
        var contextLinksFriendsObject = updateContextItems(contextLinksFriendsObject, friendsUl.children);
        createStatus(contextLinksFriendsObject, friendsStatusObj, currentTime);
    }
    if (friendsOnlineUlEmpty == false) {
        var contextLinksFriendsOnlineObject = updateContextItems(contextLinksFriendsOnlineObject, friendsOnlineUl.children);
        createStatus(contextLinksFriendsOnlineObject, friendsStatusObj, currentTime, friendsOnlineUl);
    }
    return mainSockListener;
};
var mainSockListener = initProfile();
if (typeof (userSock) != "undefined") {
    userSock.onmessage = mainSockListener;
}