function dialog(type, myId, sendersIdsArr, dialogId, thumbsObj, timeString, textObj, sendImagesLink, loadDialogLink,
    urlLoaderLink, statusObj, activeObj, usersIdsUnreadMessagesObject, usersNamesObject, initialMessagesObject) {
    /* type = true - personal; false = group
     * sendersIdsArr - Array участники диалога,
     * При type == false:
     * usersIdsUnreadMessagesObject - Объект списков, сообщения и те кто их пока не прочитал - {ID_сообщения: [id_пользователя, id_пользователя]}.
     * Иначе:
     * usersIdsUnreadMessagesObject - Список с iD сообщений
     * Существует только при type = false.
     * initialMessagesObject - Сообщения, которые нужно встроить (Экономия тактов на сервере)
     * --------------
     * xhrLoaderMessages - false или ссылка на текущий экземпляр xmlHttpRequest
     * 
     * 
     * */
    var sock = userSock;
    var doc = document;
    var dialogBlock = doc.getElementById("dialog");
    var dialogMembersBlock = doc.getElementById("dialog-members");
    var sendFilesBlock = doc.getElementById("send-files"); // Наличие или отсутствие элемента DIV-обёртки для загрузок внутри формы
    var fileInProcess = {}; // filename: xhrInstance
    var sendedMessagesActive = []; // DOMLink Сообщение было отправлено МНОЙ, но сервер пока не ответил
    var time = function (timeString) {
        var time = new Date(parseInt(timeString));
        return time;
    }(timeString);
    /* Status animations */
    var dialogsContextLinksObject = {}; //  {group: {userId: {'angleStart': Number, 'angleEnd': Number, 'context': context}, 
    // {userId: {'angleStart': Number, 'angleEnd': Number, 'context': context}, userId: {'angleStart': Number, 'angleEnd': Number, 'context': context},
// или { userId: context, userId: context1, userId: context2 }
    var mainContextLinks = {}; // {group: {userId: {'angleStart': Number, 'angleEnd': Number, 'context': context}, 
    // {userId: {'angleStart': Number, 'angleEnd': Number, 'context': context}, userId: {'angleStart': Number, 'angleEnd': Number, 'context': context},
// или { userId: context, userId: context1, userId: context2 }
    var onlineStatusActiveAnimations = {}; // Таймеры с редактированием онлайн статуса
    var activeStatusActiveAnimations = {}; // Таймеры с редактированием статуса активности
    /* Анимации статуса */
    function draw(context, xy, r, w, time, position, color, callback) {
        var counter = position;
        var p = Math.PI;
        context.beginPath();
        context.lineWidth = w;
        context.strokeStyle = color;
        var action = setInterval(function () {
            if (counter > 0) {
                var start = counter * p / 180;
                counter--;
                var end = counter * p / 180;
                context.arc(xy, xy, r, start, end, true);
                context.stroke();
            } else {
                context.closePath();
                callback ? callback() : 0;
                clearInterval(action);
            }
        }, time);
        return action;
    };
    function drawFast(context, xy, r, w, angleStart, angleEnd, color) {
        var mPi = Math.PI;
        context.beginPath();
        context.strokeStyle = color;
        context.lineWidth = w;
        context.arc(xy, xy, r, angleStart * mPi / 180, angleEnd * mPi / 180, false);
        context.stroke();
        context.closePath();
    };
    function createStatus(contextLinksObject, statusObj, currentTime, center, radius, width, id) {
        var keys = Object.keys(statusObj);
        var round = Math.round;
        var colorsObj = {
            "stateOnline": "#47b647",
            "stateActive": "#00AFFF",
            "stateLeave": "#D74620"
        };
        for (var i = keys.length; i--;) {
            var key = keys[i];
            var item = statusObj[key];
            var color = colorsObj[item] || null;
            if (color != null) {
                drawFast(contextLinksObject[key], center, radius, width, 0, 360, color);
            } else {
                if (item.hasOwnProperty('iffy')) {
                    var seconds = round((currentTime.getTime() - Date.parse(item.iffy)) / 1000);
                    var color = "#47b647";
                    var t = (20 / 360) - (20 - seconds);
                    var position = ((360 / 20) * t) * -1;
                    var context = contextLinksObject[key];
                    drawFast(context, center, radius, width, 0, position, color);
                    setTimeout(function () {
                        onlineStatusActiveAnimations[key] = draw(context, center, radius, width, (20000 / 360), position, "#FFF5F5",
                            function () {
                                delete onlineStatusActiveAnimations[key];
                                drawFast(context, center, 38.5, 8, 0, 360, "#FFF5F5");
                            });
                    }, 25);
                } else {
                    var middleData = item.stateMiddle;
                    var middleTimeTotal = middleData[1];
                    var seconds = round((currentTime.getTime() - Date.parse(middleData[0])) / 1000);
                    var t = (middleTimeTotal / 360) - (middleTimeTotal - seconds);
                    var position = ((360 / middleTimeTotal) * t) * -1;
                    var context = contextLinksObject[key];
                    drawFast(context, center, radius, width, 0, 360, "#D74620");
                    drawFast(context, center, radius, width, 0, position, "#FF7f00");
                    setTimeout(function () {
                        activeStatusActiveAnimations[key] = draw(context, center, radius, width, ((middleTimeTotal * 1000) / 360), position, "#D74620",
                            function () {
                                delete activeStatusActiveAnimations[key];
                            });
                    }, 25);
                }
            }
        }
        if (contextLinksObject.hasOwnProperty("group")) {
            var index = keys.indexOf(String(id));
            if (index > -1) {
                keys.splice(index, 1);
            }
            var keysLength = keys.length;
            var groupStatusItem = contextLinksObject.group;
            for (var i = keysLength; i--;) {
                var key = keys[i];
                var itemObj = groupStatusItem[key];
                var currentContext = itemObj.context;
                var angleStart = itemObj.angleStart;
                drawFast(currentContext, center, radius, width, angleStart, itemObj.angleEnd, colorsObj[statusObj[key]]);
                keysLength > 1 ? drawFast(currentContext, center, radius, width, angleStart, (angleStart + 3), "#333") : 0;
            }
        }
    };
    function createContextItems(elements, x, y, groupThumbElement, usersIdsArr, id) {
        // Если hasGroup нода, то в объект newContextLinksObj будет добавлено свойство "group", 
        // со значением в качестве объекта с позицией начала, позицией конца и контекста.
       // Canvas - контекст для группового диалога, состоящий из "долек" для каждого из пользователей
        // usersIdsArr - только если hasGroup нода.
        var contextElement = doc.createElement("CANVAS");
        contextElement.className = "status-animation";
        contextElement.setAttribute("width", x);
        contextElement.setAttribute("height", y);
        var newContextLinksObj = {};
        for (var i = elements.length; i--;) {
            var element = elements[i];
            var newContextElement = contextElement.cloneNode(false);
            element.insertBefore(newContextElement, element.firstElementChild);
            newContextLinksObj[element.getAttribute("value")] = newContextElement.getContext("2d");
        }
        if (groupThumbElement) {
            var newContextElement = contextElement.cloneNode(false);
            groupThumbElement.insertBefore(newContextElement, groupThumbElement.firstElementChild);
            var newContextElement = newContextElement.getContext("2d");
            var index = usersIdsArr.indexOf(id);
            if (index > -1) {
                usersIdsArr.splice(index, 1);
            }
            var groupContextObj = {};
            var userArrLength = usersIdsArr.length;
            if (userArrLength == 1) {
                groupContextObj[usersIdsArr[0]] = { 'angleEnd': 360, 'angleStart': 0, 'context': newContextElement };
            } else {
                var unglePerUser = (360 / userArrLength) - 3; // разделители между долями 2 градуса
                for (var i = userArrLength; i--;) {
                    var angleEnd = (i == (userArrLength - 1) ? (360 - 3) : angleStart);
                    var angleStart = angleEnd - unglePerUser;
                    groupContextObj[usersIdsArr[i]] = { 'angleEnd': angleEnd, 'angleStart': angleStart, 'context': newContextElement };
                }
            }
            newContextLinksObj['group'] = groupContextObj;
        }
        return newContextLinksObj;
    };
    function collectFirstChilds(ul, myMainThumbItem) {
        // Извлечение элементов для групповых диалогов
        var list = [];
        var childs = ul.children;
        for (var i = ul.childElementCount; i--;) {
            list.push(childs[i].firstElementChild);
        }
        list.push(myMainThumbItem);
        return list;
    };
    if (type) {
        var messagesList = dialogBlock.getElementsByClassName("messages-list-personal")[0];
        var mainContextLinks = createContextItems(dialogMembersBlock.children, 91, 91, null);
    } else {
        var messagesList = dialogBlock.getElementsByClassName("messages-list-group")[0];
        var mainContextLinks = createContextItems(collectFirstChilds(dialogMembersBlock.lastElementChild,
            dialogMembersBlock.firstElementChild), 91, 91, doc.getElementsByClassName("group-dialog-thumb")[0], sendersIdsArr.concat(), myId);
    }
    function prepareActiveStatus(mainObj, activeObj, myId) {
        var o = Object;
        /* Подготовка activeObj - ручной парсинг кортежа, если есть свойство 'stateMiddle': (datetimeStr, суммарное_время_состояния_middle_статуса) */
        var activeKeys = o.keys(activeObj);
        for (var i = activeKeys.length; i--;) {
            var key = activeKeys[i];
            var instance = activeObj[key];
            if (typeof(instance) === "object" && instance.constructor === o && instance.hasOwnProperty("stateMiddle")) {
                var valuesArr = instance["stateMiddle"].split(",");
                var middleTimeString = valuesArr[0];
                var middleTimeString = middleTimeString.slice(2, middleTimeString.length - 1);
                var middleTimeTotalString = valuesArr[1];
                var middleTimeTotalNumber = parseInt(middleTimeTotalString.slice(1, middleTimeTotalString.length - 1));
                activeObj[key]["stateMiddle"] = [middleTimeString, middleTimeTotalNumber];
            }
        }
        if (!mainObj.hasOwnProperty(myId)) {
            var stateOnline = "stateOnline"
            mainObj[myId] = stateOnline;
            statusObj[myId] = stateOnline;
        }
        /* Эта функция вставит в activeStatus объект значение, если оно есть хотя бы в onlineStatus */
        var mainKeys = o.keys(mainObj);
        var newActiveObj = {};
        for (var i = mainKeys.length - 1; i >= 0; i--) {
            var key = mainKeys[i];
            !activeObj.hasOwnProperty(key) ? newActiveObj[key] = key == myId ? "stateActive" : "stateLeave" : 0;
        }
        var newActiveObj = o.assign(activeObj, newActiveObj);
        return newActiveObj;
    };
    var activeObj = prepareActiveStatus(statusObj, activeObj, myId);
    createStatus(mainContextLinks, statusObj, time, 46.5, 42.5, 4, myId);
    createStatus(mainContextLinks, activeObj, time, 46.5, 38.5, 4, myId);
    /* end status animations */
    // Нужно прочитать localStorage и вставить сообщения, которые не были отправлены из-за ошибки.
    var formWrap = dialogBlock.getElementsByClassName("dialog-form-wrap");
    if (formWrap.length > 0) {
        var formWrap = formWrap[0];
        var form = formWrap.firstElementChild;
        var sendButton = form.lastElementChild;
        var messageArea = dialogBlock.getElementsByTagName("textarea")[0];
    } else {
        var formWrap = null;
        var form = null;
        var sendButton = null;
        var messageArea = null;
    }
    var sendFileBlocks = (function (sendFilesBlock) { // Приготовить объект sendFileBlocks {filename: DOMLink}, если на станице существует div id = "send-files"
        if (sendFilesBlock == null) {
            return {};
        } else {
            var childs = sendFilesBlock.children;
            var childsLen = sendFilesBlock.childElementCount;
            var filesToSend = {};
            for (var i = 0; i < childsLen; i++) {
                var fileNode = childs[i];
                filesToSend[fileNode.className] = fileNode;
            }
            return filesToSend;
        }
    })(sendFilesBlock);
    function makeThumbs(doc, thumbsObj) {
        var keys = Object.keys(thumbsObj);
        var thumbs = {};
        var elem = doc.createElement("DIV");
        for (var i = keys.length; i--;) {
            var thumb = elem.cloneNode(false);
            var key = keys[i];
            thumb.innerHTML = thumbsObj[key];
            thumbs[key] = thumb.firstElementChild;
        }
        return thumbs;
    };
    var thumbsObj = makeThumbs(doc, thumbsObj);
    function namesFromThumbsExtraction(allUsersArray, thumbs) {
        /**
         * ТОЛЬКО ДЛЯ ГРУППОВОЙ ПЕРЕПИСКИ (type === false)
         * Если при инициализации модуля-функции dialog() значение переменной "usersNamesObject" было пустым, 
         * то нужно наполнить этот объект именами пользователей, чтобы потом вставлять их в всплывающий div, 
         * с указателем на не прочитавших сообщение участников.
         * **/
        var namesObject = {};
        for (var i = allUsersArray.length; i--;) {
            var userId = allUsersArray[i];
            namesObject[userId] = thumbs[userId].lastElementChild.innerText;
        }
        return namesObject;
    };
    if (!type) {
        var usersNamesObject = namesFromThumbsExtraction(sendersIdsArr, thumbsObj);
    } else {
        var usersNamesObject = {};
    }
    function checkLastSenderId(messagesList) { // id отправителя последнего сообщения
        var elements = messagesList.getElementsByClassName("start-group");
        var elementsLength = elements.length;
        var value = elementsLength > 0 ? elements[elementsLength - 1].firstElementChild.getAttribute("value") : null;
        return value;
    };
    /* dialogs */
    var dialogsLoadPlace = doc.getElementById("show-dialogs");
    var dialogsItemsLinks = {}; // {dialogId: <p.dialog-text>} Ссылка на элемент с кратким текстом сообщения внутри элемента списка диалогов
    function openDialogs(event) {
        var link = this || event.target;
        var dialogsPlace = link.parentNode;
        var loadDialogs = new XMLHttpRequest();
        loadDialogs.open("GET", link.href, true);
        loadDialogs.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        loadDialogs.onloadend = function (event) {
            var target = this || event.target;
            if (target.status == 200) {
                var data = JSON.parse(target.response);
                var dialogs = data.dialogsMinimized || null;
                if (dialogs) {
                    var dialogsLinks = {}
                    var currentDialogId = dialogId;
                    //var currentDialogType = type; // Возможно убрать
                    var id = myId;
                    var thumbs = dialogs.pop('thumbsDict');
                    var onlineStatusObject = dialogs.pop('onlineStatusDict');
                    var activeStatusObject = dialogs.pop('activeStatusDict');
                    var onlineStatusIdsArr = onlineStatusObject !== null ? Object.keys(onlineStatusObject).map(function (l) {
                        return parseInt(l);
                    }) : null;
                    var activeStatusIdsArr = activeStatusObject !== null ? Object.keys(activeStatusObject).map(function (r) {
                        return parseInt(r);
                    }) : null;
                    var unreadMessagesArray = dialogs.pop('unreadMessages');
                    var myMessage = dialogs.pop('myMessageText');
                    textObj.myMessageText = myMessage;
                    var d = doc;
                    var dialogsWrap = d.createElement("DIV");
                    var dialogsList = d.createElement("UL");
                    dialogsList.className = "dialogs-list";
                    var dialogListItem = d.createElement("LI");
                    var link = d.createElement("A");
                    var dialogsArr = dialogs.pop('dialogsListDict');
                    var text = d.createElement("P");
                    var timeText = d.createElement("SMALL");
                    timeText.className = "dialog-time";
                    text.className = "dialog-text";
                    var contextElement = d.createElement("CANVAS");
                    contextElement.className = "status-animation";
                    contextElement.setAttribute("width", 39);
                    contextElement.setAttribute("height", 39);
                    var curTimeMS = time.getTime();
                    var date = Date;
                    var dateParse = date.parse;
                    var allSteps = 20 / 360;
                    var stepFill = 360 / 20;
                    var allStepsMS = 20000 / 360;
                    var Round = Math.round;
                    var contextLinks = {};
                    /* group dialog elements */
                    var groupDialogName = text.cloneNode(false);
                    groupDialogName.className = "";
                    var groupDialogThumbWrap = dialogsWrap.cloneNode(false);
                    groupDialogThumbWrap.className = "user-thumb";
                    var img = d.createElement("IMG");
                    var groupDialogThumbNoAvatar = dialogsWrap.cloneNode(false);
                    groupDialogThumbNoAvatar.className = "no-avatar";
                    var dialogsLength = dialogsArr.length;
                    for (var i = 0; i < dialogsLength; i++) {
                        /* Элементы */
                        var item = dialogListItem.cloneNode(false);
                        var l = link.cloneNode(false);
                        var dialogText = text.cloneNode(false);
                        var timeTextElement = timeText.cloneNode(false);
                        var thumbElement = dialogsWrap.cloneNode(false);
                        var contextItem = contextElement.cloneNode(false);
                        /* Значения */
                        var dialog = dialogsArr[i];
                        var otherDialogId = dialog.pop('dialog');
                        var messageId = dialog.pop('id');
                        var dialogType = dialog.pop('type');
                        var body = dialog.pop('body');
                        var byId = dialog.pop('by');
                        /*  */
                        if (dialogType) {
                            item.className = currentDialogId != otherDialogId ? "personal" : "current";
                            var opponent = dialog.pop('opponent');
                            if (byId == id) {
                                thumbElement.innerHTML = thumbs[opponent];
                                var thumbElement = thumbElement.firstElementChild;
                                thumbElement.insertBefore(contextItem, thumbElement.firstElementChild);
                                var context = contextItem.getContext("2d");
                                contextLinks[opponent] = context;
                                if (onlineStatusIdsArr !== null && onlineStatusIdsArr.indexOf(opponent) > -1) {
                                    var status = onlineStatusObject[opponent];
                                    if (status.constructor == Object) {
                                        var seconds = Round((curTimeMS - new date(dateParse(status['iffy'])).getTime()) / 1000);
                                        var t = allSteps - (20 - seconds);
                                        var position = (stepFill * t) * -1
                                        drawFast(context, 19.5, 18.5, 2, 0, position, "#47b647");
                                        draw(context, 19.5, allStepsMS, opponent, position);
                                    } else {
                                        drawFast(context, 19.5, 18.5, 2, 0, 360, "#47b647");
                                    }
                                }
                                l.appendChild(thumbElement);
                                var myMessagePrefixElement = text.cloneNode(false);
                                myMessagePrefixElement.innerText = myMessage;
                                myMessagePrefixElement.className = "my-message";
                                l.appendChild(myMessagePrefixElement);
                                if (unreadMessagesArray.indexOf(messageId) > -1) {
                                    dialogText.className = "dialog-text my unread";
                                } else {
                                    dialogText.className = "dialog-text my";
                                }
                            } else {
                                thumbElement.innerHTML = thumbs[byId];
                                var thumbElement = thumbElement.firstElementChild;
                                thumbElement.insertBefore(contextItem, thumbElement.firstElementChild);
                                var context = contextItem.getContext("2d");
                                contextLinks[byId] = context;
                                if (onlineStatusIdsArr !== null && onlineStatusIdsArr.indexOf(byId) > -1) {
                                    var status = onlineStatusObject[byId];
                                    if (status.constructor == Object) {
                                        var seconds = Round((curTimeMS - new date(dateParse(status['iffy'])).getTime()) / 1000);
                                        var t = allSteps - (20 - seconds);
                                        var position = (stepFill * t) * -1
                                        drawFast(context, 19.5, 18.5, 2, 0, position, "#47b647");
                                        draw(context, 19.5, allStepsMS, byId, position);
                                    } else {
                                        drawFast(context, 19.5, 18.5, 2, 0, 360, "#47b647");
                                    }
                                }
                                l.appendChild(thumbElement);
                                if (unreadMessagesArray.indexOf(messageId) > -1) {
                                    dialogText.className = "dialog-text unread";
                                }
                            }
                        } else { // Диалог ГРУППОВОЙ
                            item.className = otherDialogId != currentDialogId ? "group" : "current";
                            var dialogAvatar = dialog.pop('avatar');
                            var gDN = groupDialogName.cloneNode(false);
                            var name = dialog.pop('name');
                            gDN.innerText = name;
                            var gDTW = groupDialogThumbWrap.cloneNode(false);
                            if (dialogAvatar !== null) {
                                var image = img.cloneNode(false);
                                image.src = dialogAvatar;
                            } else {
                                var gDTNA = groupDialogThumbNoAvatar.cloneNode(false);
                                var gDTNAI = text.cloneNode(false);
                                gDTNAI.className = "";
                                gDTNAI.innerText = name;
                                gDTNA.appendChild(gDTNAI);
                                gDTW.appendChild(gDTNA);
                            }
                            gDTW.appendChild(gDN);
                            l.appendChild(gDTW);
                        }
                        /*  */
                        dialogText.innerText = body.length > 15 ? body.slice(0, 15) + "..." : body;
                        dialogText.setAttribute("value", messageId);
                        //timeTextElement.innerText = dialog.time;
                        l.href = dialog.pop('link');
                        item.setAttribute("value", otherDialogId);
                        item.appendChild(l);
                        dialogsLinks[dialogId] = dialogText;
                        l.appendChild(dialogText);
                        l.appendChild(timeTextElement);
                        dialogsList.appendChild(item);
                    }
                    dialogsItemsLinks = dialogsLinks;
                    dialogsContextLinksObject = contextLinks;
                    var dP = dialogsPlace;
                    dP.appendChild(dialogsList);
                    dP.className += "opened";
                    var mL = messagesList;
                    var dMB = dialogMembersBlock;
                    mL.className += " short";
                    dMB.className = "short";
                    formWrap.className += " short";
                    if (d.addEventListener) {
                        dialogsList.addEventListener("click", refToDialog, false);
                    } else {
                        dialogsList.attachEvent("click", refToDialog, false);
                    }
                }
            }
        };
        loadDialogs.send();
        event.preventDefault();
    };
    /* ref to dialog */
    function refToDialog(event) {
        console.time("t");
        var listItem = searchP(event.target, "LI");
        if (listItem) {
            var liItemClass = listItem.className;
            liItemClass != "current" ? loadDialog((this || event.currentTarget), listItem, listItem.firstElementChild.href, liItemClass) : 0;
        }
        event.stopPropagation();
        event.preventDefault();
    };
    function loadDialog(dialogsList, item, link, dialogType) {
        var mesStorage = messagesStorage[item.getAttribute("value")] || {};
        var ref = new XMLHttpRequest();
        ref.open("GET", (link + (mesStorage.hasOwnProperty(0) ? "?from-dialog" : "?from-dialog&get-messages")), true);
        ref.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        ref.onload = function(event) {
            function handleError() {

            };
            function insertDialog(data, messagesPageObj) {
                var itm = item;
                var newDialogPath = link;
                function refreshMembersBlockDialogPersonal(d, membersPlace, toUserMainThumbArr, lastType) {
                    membersPlace.removeChild(membersPlace.lastElementChild);
                    lastType ? 0 : membersPlace.removeChild(membersPlace.lastElementChild);
                    var wrap = d.createElement("DIV");
                    wrap.innerHTML = Array.prototype.join.call(toUserMainThumbArr).replace(",", "");
                    var newItem = wrap.firstElementChild;
                    membersPlace.appendChild(newItem);
                    return newItem;
                };
                function refreshMembersBlockDialogGroup(d, membersPlace, mainThumbsArr, lastType, groupDialogAvatar, path, name) {
                    if (lastType) {
                        var groupDialogHead = d.createElement("H3");
                        var groupDialogThumb = d.createElement("DIV");
                        groupDialogThumb.className = "group-dialog-thumb";
                        var link = d.createElement("A");
                        link.setAttribute("href", path);
                        if (groupDialogAvatar) {
                            var image = d.createElement("IMG");
                            image.setAttribute("src", groupDialogAvatar);
                            link.appendChild(image);
                        } else {
                            var wrap = d.createElement("DIV");
                            wrap.className = "no-avatar";
                            var text = d.createElement("P");
                            text.innerText = (name ? name : slug);
                            wrap.appendChild(text);
                            link.appendChild(wrap);
                        }
                        groupDialogThumb.appendChild(link);
                        var link1 = link.cloneNode(false);
                        link1.innerText = name;
                        groupDialogHead.appendChild(link1);
                        groupDialogThumb.appendChild(groupDialogHead);
                        var thumbsList = d.createElement("UL");
                        membersPlace.removeChild(membersPlace.lastElementChild);
                        membersPlace.appendChild(groupDialogThumb);
                    } else {
                        var thumbsList = membersPlace.lastElementChild;
                        var groupDialogThumb = membersPlace.firstElementChild.nextElementSibling;
                        var link = groupDialogThumb.firstElementChild.nextElementSibling;
                        groupDialogThumb.lastElementChild.innerText = name;
                        link.href = path;
                        link.innerHTML = "";
                        if (groupDialogAvatar) {
                            var image = d.createElement("IMG");
                            image.setAttribute("src", groupDialogAvatar);
                            link.appendChild(image);
                        } else {
                            var wrap = d.createElement("DIV");
                            wrap.className = "no-avatar";
                            var text = d.createElement("P");
                            text.innerText = (name ? name : slug);
                            wrap.appendChild(text);
                            link.appendChild(wrap);
                        }
                        thumbsList.innerHTML = "";
                    }
                    var thumbs = makeThumbs(d, mainThumbsArr);
                    for (var keys = Object.keys(mainThumbsArr), i = keys.length; i--;) {
                        thumbsList.appendChild(thumbs[keys[i]]);
                    }
                    membersPlace.appendChild(thumbsList);
                    return { 'all': thumbsList.children, 'group': groupDialogThumb};
                };
                function refreshMessagesList(d, ul, messages, unread, id, thumbs, dialogType, usernamesObj, allSenders, dialogId, storage) {
                    var newUl = insertMessages(d, messages, unread, id, thumbs, null, dialogType, usernamesObj, allSenders);
                    var messagesInner = newUl.innerHTML;
                    ul.innerHTML = messagesInner;
                    if (!storage.hasOwnProperty(0)) {
                        storage[0] = messagesInner;
                        messagesStorage[dialogId] = storage;
                    }
                };
                function replaceThumbsInStorageString(doc, messagesStringsObj, thumbsObjStr) {
                    // Если запрос был без параметра '&get-messages', 
                    // то перед вставкой строки сообщений нужно проверить не изменилась ли миниатюра пользователя.
                    var ul = doc.createElement("UL");
                    var inner = "";
                    for (var messagesObjKeys = Object.keys(messagesStringsObj), i = messagesObjKeys.length; i--;) {
                        inner += messagesStringsObj[messagesObjKeys[i]];
                    }
                    ul.innerHTML = inner;
                    var childs = ul.getElementsByClassName("start-group");
                    for (var i = childs.length; i--;) {
                        var item = childs[i];
                        var userThumb = item.firstElementChild;
                        item.replaceChild(thumbsObjStr[userThumb.getAttribute("value")], userThumb);
                    }
                    return ul.innerHTML;
                };
                function refreshInsertions(d, imagesData) {
                    var filesBlock = sendFilesBlock;
                    function pushItems(d, imgs, data, block, path) {
                        var sfB = {};
                        var len = imgs.length;
                        for (var i = 0; i < len; i++) {
                            var name = imgs[i];
                            var image = data[name];
                            var elems = createInner(d, name, image[0], path);
                            var wrap = elems[0], img = elems[1], remove = elems[2];
                            wrap.appendChild(img);
                            wrap.appendChild(remove);
                            sfB[name] = wrap;
                            block.appendChild(wrap);
                        }
                        sendFileBls = sfB;
                        return block;
                    };
                    if (imagesData) {
                        var sendFileBls = {};
                        var imagesLoadPath = sendImagesLink;
                        var images = Object.keys(imagesData);
                        if (filesBlock) {
                            var filesBlock = pushItems(d, images, imagesData, filesBlock, imagesLoadPath);
                            filesBlock.innerHTML = filesBlock.innerHTML;
                        } else {
                            var filesBlock = d.createElement("DIV");
                            filesBlock.id = "send-files";
                            var filesBlock = pushItems(d, images, imagesData, filesBlock, imagesLoadPath);
                            formWrap.appendChild(filesBlock);
                        }
                        sendFilesBlock = filesBlock;
                        sendFileBlocks = sendFileBls;
                    } else {
                        if (filesBlock) {
                            filesBlock.parentNode.removeChild(filesBlock);
                            sendFileBlocks = {};
                            sendFilesBlock = null;
                        }
                    }
                };
                var newDialogType = dialogType == "personal";
                var d = doc;
                var my = myId;
                var lastDialogType = type;
                var currentDialogId = data.pop('dialogId');
                var onlineStatusObj = data.pop('onlineStatusDict');
                var chatStatusObj = data.pop('activeStatusDict');
                if (newDialogType) {
                    var toUserId = data.pop('opponentUserId');
                    var smallThumbs = {};
                    smallThumbs[my] = data.pop('myListThumb');
                    smallThumbs[toUserId] = data.pop('toUserListThumb');
                    var currentDialogUsersIds = [data.requestUserId, toUserId];
                    var mainContextLinksLocal = createContextItems([refreshMembersBlockDialogPersonal(d, dialogMembersBlock, data.pop('toUserMainThumb'), lastDialogType)], 91, 91);
                    if (onlineStatusObj !== null && onlineStatusObj.hasOwnProperty(toUserId)) {
                        if (chatStatusObj === null) {
                            var chatStatusObj = {};
                            chatStatusObj[toUserId] = 'stateLeave';
                        } else {
                            if (!chatStatusObj.hasOwnProperty(toUserId)) {
                                chatStatusObj[toUserId] = 'stateLeave';
                            }
                        }
                    } else {
                        if (chatStatusObj !== null && chatStatusObj.hasOwnProperty(toUserId)) {
                            delete chatStatusObj[toUserId];
                        }
                    }
                    var sendersIds = [my, toUserId];
                } else {
                    var currentDialogUsersIds = data.pop('dialogUsersIds');
                    var mainThumbsObjectString = data.pop('mainThumbsDict');
                    delete mainThumbsObjectString[my];
                    var mainThumbsObj = refreshMembersBlockDialogGroup(d, dialogMembersBlock,
                        mainThumbsObjectString, lastDialogType, data.pop('avatar'), newDialogPath,
                        itm.firstElementChild.firstElementChild.lastElementChild.innerText);
                    var mainContextLinksLocal = createContextItems(mainThumbsObj.all, 91, 91, mainThumbsObj.group, currentDialogUsersIds, my);
                    for (var i = currentDialogUsersIds.length; i--;) {
                        var id = currentDialogUsersIds[i];
                        if (onlineStatusObj.hasOwnProperty(id)) {
                            if (!chatStatusObj.hasOwnProperty(id)) {
                                chatStatusObj[id] = 'stateLeave';
                            }
                        }
                    }
                    var smallThumbs = data.pop('smallThumbsDict');
                    var sendersIds = currentDialogUsersIds;
                }
                mainContextLinksLocal[my] = mainContextLinks[my];
                var newListThumbs = makeThumbs(d, smallThumbs);
                var t = time;
                onlineStatusObj !== null ? createStatus(mainContextLinksLocal, onlineStatusObj, t, 46.5, 42.5, 4, my) : 0;
                chatStatusObj !== null ? createStatus(mainContextLinksLocal, chatStatusObj, t, 46.5, 38.5, 4, my) : 0;
                var messagesUl = messagesList;
                var messages = data.pop('messages');
                if (messages !== null) { // null будет означать, что не было 'get-messages' в параметрах запроса
                    if (messages.length > 0) {
                        var unreadMessagesIdsArr = data.unreadMessagesDictList || null;
                        if (unreadMessagesIdsArr !== null) {
                            if (newDialogType) {
                                refreshMessagesList(d, messagesUl, messages, unreadMessagesIdsArr, my, newListThumbs, newDialogType, null, null, currentDialogId, messagesPageObj);
                                sendReadMessagesPersonal(unreadMessagesIdsArr, sock, currentDialogId, currentDialogUsersIds[1] + "")
                            } else {
                                var usersNamesObj = namesFromThumbsExtraction(sendersIds, newListThumbs);
                                refreshMessagesList(d, messagesUl, messages, unreadMessagesIdsArr, my, newListThumbs, newDialogType, usersNamesObj, sendersIds, currentDialogId, messagesPageObj);
                                sendReadMessagesGroup(unreadMessagesIdsArr, sock, currentDialogId, my);
                                usersNamesObject = usersNamesObj;
                            }
                        } else {
                            refreshMessagesList(d, messagesUl, messages, [], my, newListThumbs, newDialogType, null, null, currentDialogId, messagesPageObj);
                        }
                        usersIdsUnreadMessagesObject = unreadMessagesIdsArr;
                        //var defaultActions = messagesData.actionData || null;
                        //defaultActions ? defaultListener(defaultActions) : 0;
                    } else {
                        messagesUl.innerHTML = "";
                    }
                } else {
                    messagesUl.innerHTML = replaceThumbsInStorageString(d, messagesPageObj, newListThumbs);
                }
                mainContextLinks = mainContextLinksLocal;
                statusObj = onlineStatusObj;
                path = newDialogPath;
                lastSenderId = checkLastSenderId(messagesUl);
                changeUrl(newDialogPath); // changeUrl - base/static/navigation.js
                refreshInsertions(d, data.pop('imagesToSend'));
                messagesUl.scrollTop = messagesUl.scrollTopMax || messagesUl.scrollHeight;
                messageLoaderPage = 1;
                var dList = dialogsList;
                (dList.querySelector ?
                    dList.querySelector(".current").className = (lastDialogType ? "personal" : "group") :
                    dList.getElementsByClassName("current")).className = (lastDialogType ? "personal" : "group");
                type = newDialogType;
                dialogId = currentDialogId;
                thumbsObj = newListThumbs;
                sendersIdsArr = sendersIds;
                lastDialogType = itm.className;
                item.className = "current";
                console.timeEnd("t");
            };
            var event = this || event.target;
            if (event.readyState == 4 && event.status == 200) {
                var data = JSON.parse(event.response).newDialog || null;
                data ? insertDialog(data, mesStorage) : 0;
            }
        };
        ref.send(null);
    };
    /* end dialogs */
    /* send Files */
    function formAndMessagesListTransform(formWrap, messagesUl) {
        var gCS = window.getComputedStyle;
        var round = Math.round;
        var formHeight = formWrap ? round(gCS(formWrap).height.slice(0, -2)) : 0;
        if (formAndMessagesListTransform.hasOwnProperty('ulHeight')) {
            messagesUl.setAttribute("style", ("height: " + (formAndMessagesListTransform.ulHeight - formHeight) + "px"));
        } else {
            var ulHeight = round(gCS(messagesUl).height.slice(0, -2));
            formAndMessagesListTransform.ulHeight = ulHeight;
            messagesUl.setAttribute("style", ("height: " + (ulHeight - formHeight) + "px"));
        }
    };
    formAndMessagesListTransform(formWrap, messagesList);
    function createInner(doc, name, file, sendPath) {
        var format = name.slice(0, name.indexOf(".") - 1);
        var wrap = doc.createElement("DIV");
        var img = doc.createElement("IMG");
        var remove = doc.createElement("SPAN");
        remove.className = "remove";
        var link = doc.createElement("A");
        link.href = sendPath + "?remove=" + name;
        wrap.setAttribute("class", name);
        remove.appendChild(link);
        img.setAttribute("src", "data:image/" + format + ";base64, " + file);
        return [wrap, img, remove];
    };
    function addFilesToUpload(doc, name, file, sendPath) { // Работает, если страница открыта на нескольких устройствах одновременно
        var sFb = sendFilesBlock;
        if (sFb == null) {
            var sFb = doc.createElement("DIV");
            sFb.id = "send-files";
            var elems = createInner(doc, name, file, sendPath);
            var wrap = elems[0], img = elems[1], remove = elems[2];
            wrap.appendChild(img);
            wrap.appendChild(remove);
            sFb.appendChild(wrap);
            sendFileBlocks[name] = wrap;
            formWrap.appendChild(sFb);
        } else {
            var names = Object.keys(sendFileBlocks);
            var find = false;
            for (var i = 0; i < names.length; i++) {
                if (names[i] == name) {
                    var find = true;
                }
            }
            if (find == false) {
                var elems = createInner(doc, name, file, sendPath);
                var wrap = elems[0], img = elems[1], remove = elems[2];
                wrap.appendChild(img);
                wrap.appendChild(remove);
                sendFileBlocks[name] = wrap;
                sFb.appendChild(wrap);
            }
        }
    };
    function dragenterHandler(event) {
        var target = this || event.target;
        target.className = 'drag';
    };
    function dropHandler(event) {
        var target = this || event.target;
        target.className = '';
    };
    function sendFilesWrap(event) {
        var s = sock;
        var d = doc;
        var sFb = sendFilesBlock;
        var files = Array.prototype.slice.call((this || event.target).files);
        var filesLength = files.length;
        var sendFilesBlockLocal = false;
        if (sFb == null) {
            var sendFilesBlockLocal = d.createElement("DIV");
            sendFilesBlockLocal.id = "send-files";
        } else {
            var sendFilesBlockLocal = sFb;
        }
        var attachmentBlock = d.createElement("DIV");
        var progressBar = d.createElement("PROGRESS");
        for (var i = 0; i < filesLength; i++) {
            var progressBar = progressBar.cloneNode(false);
            var attachmentBlock = attachmentBlock.cloneNode(false);
            var fileName = files[i].name;
            attachmentBlock.className = fileName;
            progressBar.setAttribute("max", 100);
            progressBar.setAttribute("value", 0);
            attachmentBlock.appendChild(progressBar);
            sendFileBlocks[fileName] = attachmentBlock;
            sendFilesBlockLocal.appendChild(attachmentBlock);
        }
        var sFbs = sendFileBlocks;
        var fWrap = formWrap;
        if (sFb == null) {
            fWrap.appendChild(sendFilesBlockLocal);
            sendFilesBlock = sendFilesBlockLocal;
        }
        var sendCounter = files.length - 1;
        function sendFile(files, sendCounter, sFbs) {
            var file = files[sendCounter];
            var fileName = file.name;
            var block = sFbs[fileName];
            var response = new XMLHttpRequest();
            var sendPath = sendImagesLink;
            response.open("POST", sendPath, true);
            response.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            response.setRequestHeader('X-CSRFToken', csrf);
            response.upload.onloadstart = function (event) {
                fileInProcess[fileName] = response;
            };
            response.upload.onloadend = function (event) {
                var b = block;
                var p = b.firstElementChild;
                p.remove();
                var process = d.createElement("SPAN");
                process.className = "after-load-processing";
                b.appendChild(process);
            };
            response.onloadend = function (event) {
                delete fileInProcess[fileName];
                var b = block;
                var img = new Image;
                img.src = URL.createObjectURL(file);
                b.innerHTML = "";
                var remove = d.createElement("SPAN");
                remove.className = "remove";
                var link = d.createElement("A");
                link.href = sendPath + "?remove=" + fileName;
                remove.appendChild(link);
                b.appendChild(img);
                b.appendChild(remove);
                delete file;
                sendCounter--;
                if (sendCounter >= 0) {
                    sendFile(files, sendCounter, sFbs);
                }
                formAndMessagesListTransform(fWrap, messagesList);
            };
            response.upload.onprogress = function (event) {
                var p = block.firstElementChild;
                p.setAttribute("value", event.loaded / event.total * 100);
            };
            var data = new FormData();
            data.append('insertion', file);
            response.send(data);
        };
        sendFile(files, sendCounter, sFbs);
        event.preventDefault();
    };
    /* end send Files */
    /* send Messages */
    var path = doc.location.pathname;
    if (form) {
        var csrf = form.getElementsByClassName("csrf")[0].value;
        csrf == "" ? csrf = window.csrf : 0;
    } else {
        var csrf = window.csrf;
    }
    function initSendMessage(event) {
        function sendMessage(data) {
            var response = new XMLHttpRequest();
            response.open("POST", path, true);
            response.responseType = "text";
            response.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            response.setRequestHeader('X-CSRFToken', csrf);
            response.onloadstart = function () {
                returnMessage(doc, form, myId, sendFileBlocks, sendedMessagesActive, lastSenderId, messagesList, type);
            };
            response.onloadend = function (event) {
                if (!sock) {
                    messagesSendedSuccess(messagesList, JSON.parse(event.target.response.data), dialogsItemsLinks[myId], dialogsLoadPlace, messageArea, myId, lastSenderId);
                }
            };
            response.onerror = function (event) {
                // Назначание сообщения в localStorage браузера
                var unsendedMessages = localStorage.getItem('unsendedMessages');
                if (!unsendedMessages) {
                    localStorage.setItem('unsendedPersonalMessages', {
                        'dialog': recipientsIds.opponent
                    });
                }
            };
            
            response.send(data);
        };
        var data = new FormData();
        var body = form.id_body;
        var text = body.value;
        if (text == "") {
            if (sendFilesBlock === null) {
                "preventDefault" in event ? event.preventDefault() : 0;
                return;
            } else {
                data.append(body.name, text);
            }
        } else {
            data.append(body.name, text);
        }
        sendMessage(data);
        "preventDefault" in event ? event.preventDefault() : 0;
    };
    /** Чтение сообщений **/
    var sendReadMessagesActive = false;
    function clearSendReadMessagesTimer() {
        setTimeout(function () {
            sendReadMessagesActive = false;
        }, 5000);
    };
    function readMessage() {
        if (sendReadMessagesActive == false) {
            var usersIdsUnreadMessagesObj = usersIdsUnreadMessagesObject;
            if (usersIdsUnreadMessagesObj !== null) {
                if (type) {
                    if (Object.keys(usersIdsUnreadMessagesObj).length > 0) {
                        var sendersArr = sendersIdsArr.concat();
                        sendersArr.splice(sendersArr.indexOf(myId), 1);
                        var sender = "" + sendersArr[0];
                        var messagesToReadIdsArr = usersIdsUnreadMessagesObj[sender] || null;
                        if (messagesToReadIdsArr) {
                            var s = sock;
                            if (s) {
                                if (s.readyState == 1) {
                                    s.send(JSON.stringify({
                                        "type": "readPersonalMessage",
                                        "dialogId": dialogId,
                                        "sender": sender,
                                        "readedMessagesIds": messagesToReadIdsArr
                                    }));
                                    sendReadMessagesActive = true;
                                    clearSendReadMessagesTimer();
                                } else {
                                    s.onopen = function () {
                                        s.send(JSON.stringify({
                                            "type": "readPersonalMessage",
                                            "dialogId": dialogId,
                                            "sender": sender,
                                            "readedMessagesIds": messagesToReadIdsArr
                                        }));
                                        sendReadMessagesActive = true;
                                        clearSendReadMessagesTimer();
                                    };
                                }
                            }
                        }
                    }
                } else {
                    var usersIdsUnreadMessagesObj = usersIdsUnreadMessagesObject;
                    var messageIds = Object.keys(usersIdsUnreadMessagesObj);
                    var messageIdsLength = messageIds.length;
                    if (messageIdsLength > 0) {
                        var messageIdsToSend = [];
                        for (var i = messageIdsLength; i--;) {
                            var messageId = messageIds[i];
                            usersIdsUnreadMessagesObj[messageId].indexOf(myId) > -1 ? messageIdsToSend.push(messageId) : 0;
                        }
                        if (messageIdsToSend.length > 0) {
                            var s = sock;
                            if (s.readyState == 1) {
                                s.send(JSON.stringify({
                                    "type": "readGroupMessage",
                                    "dialogId": dialogId,
                                    "readedMessagesIds": messageIds
                                }));
                                sendReadMessagesActive = true;
                                clearSendReadMessagesTimer();
                            } else {
                                s.onopen = function () {
                                    s.send(JSON.stringify({
                                        "type": "readGroupMessage",
                                        "dialogId": dialogId,
                                        "readedMessagesIds": messageIds
                                    }));
                                    sendReadMessagesActive = true;
                                    clearSendReadMessagesTimer();
                                };
                            }
                        }
                    }
                }
            }
        }
    };
    /* check links in new Message */
    /* initial check links */
    function initialReplaceLinks(d, messages) {
        var splitter = null;
        var re = null;
        var linkElement = null;
        var urlLoaderUrl = null;
        var token = null;
        function wrapMessageBodyLinks(textBody, reg, spl, linkElem, type) {
            /* type == true - анализ при наборе текста вставка фрейма видео или изображения под форму
             type == false - анализ и вставка при получении/подгрузке сообщений.
             */
            if (textBody.indexOf(".") > -1) {
                if (spl == null) {
                    var spl = splitter;
                    if (spl == null) {
                        var spl = new RegExp(/[ ,\|;]/i);
                        splitter = spl;
                    }
                }
                var wordsRow = textBody.split(spl);
                for (var i = wordsRow.length; i--;) {
                    var word = wordsRow[i];
                    if (word.indexOf(".") > -1) {
                        if (reg == null) {
                            var reg = re;
                            if (reg == null) {
                                var reg = new RegExp(/^(https?:\/\/)?(www)?[a-z0-9\-\_\/]+\.[a-z]+\/?([\S]*)?/i);
                                re = reg;
                            }
                        }
                        if (type == false) {
                            if (linkElem == null) {
                                var linkElem = linkElement;
                                if (linkElem == null) {
                                    var linkElem = d.createElement("A");
                                    linkElem.setAttribute("target", "_blank");
                                    linkElement = linkElem;
                                }
                            }
                        } else {
                            //
                        }
                        if (reg.test(word)) {
                            if (type == false) {
                                var link = linkElem.cloneNode(false);
                                if (word.indexOf("http") != -1) {
                                    link.setAttribute("href", word);
                                } else {
                                    link.setAttribute("href", "//" + word);
                                }
                                link.innerText = word;
                                wordsRow[i] = link.outerHTML;
                            } else {

                                if (word.indexOf("youtube")) {
                                    var video = d.createElement("IFRAME");
                                    video.setAttribute("width", "250");
                                    video.setAttribute("height", "250");
                                    video.setAttribute("src", word);
                                }
                            }
                        }
                    }
                }
            }
            if (reg) {
                return wordsRow.join(" ");
            } else {
                return false;
            }
        };
        for (var i = messages.length; i--;) {
            var messageTextBlock = messages[i].lastElementChild.firstElementChild;
            var text = messageTextBlock.innerText;
            if (text.length > 0) {
                var result = wrapMessageBodyLinks(text, re, splitter, linkElement, false);
                if (result) {
                    messageTextBlock.innerHTML = result;
                }
            }
        }
        function loadDocumentByLink(loadPath, link, csrf) {
            var loadWebResource = new XMLHttpRequest();
            loadWebResource.open("POST", loadPath + "?link=" + link, true);
            loadWebResource.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            loadWebResource.setRequestHeader('X-CSRFToken', csrf);
            loadWebResource.onloadend = function (event) {
                var target = (this || event.target);
                if (target.status == 200) {
                    var response = target.response;
                    var documentWrap = new Document();
                    documentWrap.innerHTML = response;

                }
            };
            loadWebResource.send(null);
        };
        return wrapMessageBodyLinks;
    };
    var wrapMessageBodyLinks = initialReplaceLinks(doc, messagesList.children);
    /* end initial check links */
    /*  */
    var holdButton = false;
    function sendMessageFirst(event) {
        if (event.keyCode == 17) {
            holdButton = true;
        };
    };
    function sendMessageSecond(event) {
        var key = event.keyCode;
        if (key == 17) {
            holdButton = false;
        } else if (key == 13) {
            if (holdButton == true) {
                initSendMessage(event);
                holdButton = false;
            }
        }
    };
    /* end send Messages */
    function writingMessageWrap(sendPlace) {
        function startTimer() {
            var dropPressCounterTimer = setTimeout(function() {
                // Отправка сигнала "пользователь набирает текст"
            }, 4000);
        };
        var writingMessage = function (event) {
            var key = event.keyCode;
            if (key == 13) {
                if (holdButton == false) {
                    var htmlContentData = wrapMessageBodyLinks(messageArea.value, null, null, null, true);
                    console.log(htmlContentData);
                }
            } else {
                if (key == 9 || key == 32 || key <= 90 || 96 <= key <= 111 || 186 <= key <= 222) {
                    var htmlContentData = wrapMessageBodyLinks(messageArea.value, null, null, null, true);
                }
            }
        };
        var nothing = function() {};
        return sendPlace ? writingMessage : nothing;
    };
    var writingMessage = writingMessageWrap(messageArea);
    /* end check links in new Message */
    /* view-hide "unread users" box, group dialog */
    function searchMessageElementShow(element) {
        var name = element.className;
        if (name != "" && (name == "my-message unread" || name == "not-my-message unread" ||
            name == "my-message unread start-group" || name == "not-my-message unread start-group")) {
            return element;
        }
        do {
            var element = element.parentNode || null;
            if (element) {
                var name = element.className;
            }
        } while (element && (name != "my-message unread" && name != "not-my-message unread" &&
            name != "my-message unread start-group" && name != "not-my-message unread start-group"));
        return element;
    };
    function searchMessageElementHide(element) {
        var name = element.className;
        if (name != "" && (name == "my-message unread hightlited" || name == "not-my-message unread hightlited" ||
            name == "my-message unread start-group hightlited" || name == "not-my-message unread start-group hightlited")) {
            return element;
        }
        do {
            var element = element.parentNode || null;
            if (element) {
                var name = element.className;
            }
        } while (element && (name != "my-message unread hightlited" && name != "not-my-message unread hightlited" &&
            name != "my-message unread start-group hightlited" && name != "not-my-message unread start-group hightlited"));
        return element;
    };
    function showUnreadMessageUsersBox(event) {
        if (!type) {
            var parentElement = searchMessageElementShow(event.target);
            if (parentElement) {
                var item = parentElement.lastElementChild;
                item.className = "unread-users-box";
                var clickPositionX = event.layerX;
                item.style = "left: " + (clickPositionX <= (parentElement.clientWidth / 2) ?
                    clickPositionX : clickPositionX - item.clientWidth - 5) +
                    "px; top: " + (parentElement == messagesList.lastElementChild ?
                        event.layerY - item.clientHeight : event.layerY) + "px;";
                parentElement.className = parentElement.className + " hightlited";
            }
        }
        event.stopPropagation();
    };
    function hideUnreadMessageUsersBox(event) {
        if (!type) {
            var parentElement = searchMessageElementHide(event.target);
            if (parentElement) {
                parentElement.lastElementChild.className = "unread-users-box hidden";
                var currentClassName = parentElement.className;
                parentElement.className = currentClassName.slice(0, currentClassName.indexOf(" hightlited"));
            }
        }
        event.stopPropagation();
    };
    if (doc.addEventListener) {
        messagesList.addEventListener("scroll", checkScroll, false);
        sendButton ? sendButton.addEventListener("click", initSendMessage, false) : 0;
        doc.addEventListener("mousemove", readMessage, false);
        doc.addEventListener("keydown", sendMessageFirst, false);
        doc.addEventListener("keyup", sendMessageSecond, false);
        form ? form.getElementsByClassName('insertion')[0].addEventListener("change", sendFilesWrap, false) : 0;
        dialogsLoadPlace.firstElementChild.addEventListener("click", openDialogs, false);
        messagesList.addEventListener('click', showUnreadMessageUsersBox, false);
        messagesList.addEventListener('mouseout', hideUnreadMessageUsersBox, false);
        messageArea.addEventListener("keyup", writingMessage, false);
        if (messageArea) {
            if ('draggable' in messageArea) {
                messageArea.addEventListener('dragenter', dragenterHandler, false);
                messageArea.addEventListener('drop', dropHandler, false);
            }
        }
    } else {
        messagesList.attachEvent("scroll", checkScroll, false);
        sendButton ? sendButton.attachEvent("click", initSendMessage, false) : 0;
        doc.attachEvent("mousemove", readMessage, false);
        doc.attachEvent("keydown", sendMessageFirst, false);
        doc.attachEvent("keyup", sendMessageSecond, false);
        form ? form.getElementsByClassName('insertion')[0].attachEvent("change", sendFilesWrap, false) : 0;
        dialogsLoadPlace.firstElementChild.attachEvent("click", openDialogs, false);
        messagesList.attachEvent('mouseover', showUnreadMessageUsersBox, false);
        messagesList.attachEvent('mouseout', hideUnreadMessageUsersBox, false);
        messageArea.attachEvent("keyup", writingMessage, false);
        if (messageArea) {
            if ('draggable' in messageArea) {
                messageArea.attachEvent('dragenter', dragenterHandler, false);
                messageArea.attachEvent('drop', dropHandler, false);
            }
        }
    }
    function removeShortNotifyItems(doc, messagesList) {
        var shortNotify = doc.getElementById('notiList') || null;
        if (shortNotify) {
            var messageItems = messagesList.childNodes;
            var messageItemsLength = messageItems.length;
            var shortNotifyItems = shortNotify.childNodes;
            for (var i = 0; i < shortNotifyItems.length; i++) {
                for (var t = 0; t < messageItemsLength; t++) {
                    checkItem(shortNotifyItems[i], messageItems[t]);
                }
                shortNotifyItems.length == 0 ? shortNotify.parentNode.removeChild(shortNotify) : 0;
            }
        }
        function checkItem(notifyItem, messageItem) {
            if (messageItem.nodeType != 1) {
                return;
            }
            notifyItem.nodeType != 1 || notifyItem.getAttribute('value') == messageItem.getAttribute('value') ? shortNotify.removeChild(notifyItem) : 0;
        };
    };
    removeShortNotifyItems(doc, messagesList);
    function mainSockListener(event) {
        var sockData = event.data || null;
        if (sockData) {
            var data = JSON.parse(sockData);
        } else {
            return;
        }
        var data = data.constructor === Array ? data : [data];
        var obj = null;
        for (var t = data.length; t--;) {
            var obj = data[t];
            var personalMessagesRead = obj.personalMessageRead || null;
            var groupMessagesRead = obj.groupMessageRead || null;
            if (personalMessagesRead || groupMessagesRead) {
                var messagesReadObject = personalMessagesRead || groupMessagesRead;
                if (messagesReadObject.dialogId == dialogId) {
                    var messages = messagesList.getElementsByClassName("unread");
                    var readedMessagesArray = messagesReadObject.messages;
                    var usersIdsUnreadMessagesObj = usersIdsUnreadMessagesObject;
                    if (personalMessagesRead) {
                        var unreadMessagesArr = Object.keys(usersIdsUnreadMessagesObj).length > 0 ? Array.prototype.join.call(Object.values(usersIdsUnreadMessagesObj)) : null;
                        if (unreadMessagesArr !== null) {
                            for (var i = readedMessagesArray.length; i--;) {
                                var messageId = readedMessagesArray[i];
                                if (unreadMessagesArr.indexOf(messageId) > -1) {
                                    for (var r = messages.length; r--;) {
                                        var element = messages[r];
                                        if (element.hasAttribute("value") && element.getAttribute("value") == messageId) {
                                            element.className = element.className.replace(" unread", "");
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        var readedUserId = messagesReadObject.reader;
                        for (var i = readedMessagesArray.length; i--;) {
                            var messageId = readedMessagesArray[i];
                            if (usersIdsUnreadMessagesObj.hasOwnProperty(messageId)) {
                                var unreadUsersArr = usersIdsUnreadMessagesObj[messageId];
                                var readedUserIndex = unreadUsersArr.indexOf(readedUserId);
                                if (readedUserIndex > -1) {
                                    for (var r = messages.length; r--;) {
                                        var element = messages[r];
                                        if (element.hasAttribute("value") && element.getAttribute("value") == messageId) {
                                            var unreadUsersBox = element.lastElementChild;
                                            var unreadUsersBoxChildren = unreadUsersBox.children;
                                            var unreadUsersBoxChildrenLen = unreadUsersBoxChildren.length;
                                            if (unreadUsersBoxChildrenLen > 1) {
                                                for (var v = unreadUsersBoxChildrenLen; v--;) {
                                                    var listItem = unreadUsersBoxChildren[v];
                                                    if (listItem.value == readedUserId) {
                                                        unreadUsersBox.removeChild(listItem);
                                                        break;
                                                    }
                                                }
                                                var slashedUsersCounter = unreadUsersBox.firstElementChild.firstElementChild;
                                                var text = slashedUsersCounter.innerText;
                                                var slashIndex = text.indexOf("/");
                                                slashedUsersCounter.innerText = (parseInt(text.slice(0, slashIndex)) - 1) + text.slice(slashIndex, text.length);
                                            } else {
                                                element.removeChild(unreadUsersBox);
                                                element.className = element.className.replace(" unread", "");
                                            }
                                        }
                                    }
                                    unreadUsersArr.splice(readedUserIndex, 1);
                                    unreadUsersArr.length == 0 ? delete usersIdsUnreadMessagesObj[messageId] : 0;
                                }
                            }
                        }
                    }
                }
                var dialogsPlace = dialogsLoadPlace;
                if (dialogsPlace.className == "opened") {
                    var dialogsUl = dialogsPlace.lastElementChild;
                    var lastMessages = dialogsUl.getElementsByClassName("unread");
                    for (var i = lastMessages.length; i--;) {
                        var lastMessage = lastMessages[i];
                        if (readedMessagesArray.indexOf(lastMessage.getAttribute("value")) != -1) {
                            lastMessage.className = lastMessage.className.replace(" unread", "");
                        }
                    }
                }
                data.splice(t, 1);
            } else {
                var method = obj.method;
                if (method == 'messagePersonal' || method == "messageGroup") {
                    var sender = obj.byId;
                    var dId = obj.dialogId;
                    var currentDialogId = dialogId;
                    var dialogsPlace = dialogsLoadPlace;
                    if (currentDialogId == dId) {
                        var im = myId;
                        var messageId = obj.id;
                        var sendedActiveLength = sendedMessagesActive.length;
                        var body = obj.body;
                        var dialogItem = dialogsItemsLinks[dId] || null;
                        var usersIdsUnreadMessagesObj = usersIdsUnreadMessagesObject || {};
                        var messagesData = messagesStorage[dId] || {};
                        if (sendedActiveLength == 0) { // Получаем сообщения с других компьютеров
                            var images = obj.images || null;
                            var files = obj.files || null;
                            var lastSender = lastSenderId;
                            var d = doc;
                            if (lastSender !== null) { // Первое и пока единственное сообщение или нет
                                if (sender != im) {
                                    var isMine = false;
                                    if (lastSender == im) {
                                        var thumb = thumbsObj[sender];
                                        lastSenderId = sender;
                                        if (dialogItem) {
                                            dialogItem.innerText = body;
                                            dialogItem.setAttribute("value", messageId);
                                            var myMessageText = dialogItem.previousElementSibling;
                                            myMessageText.parentNode.removeChild(myMessageText);
                                        } else {
                                            //
                                        }
                                    } else {
                                        var thumb = null;
                                        if (dialogItem) {
                                            dialogItem.innerText = body.length > 15 ? body.slice(0, 15) + "..." : body;
                                            dialogItem.setAttribute("value", messageId);
                                        } else {
                                            //
                                        }
                                    }
                                } else {
                                    var isMine = true;
                                    var sF = sendFileBlocks;
                                    var imagesNamesInPlaceToSend = Object.keys(sF);
                                    if (imagesNamesInPlaceToSend.length > 0) {
                                        var imageWrap = sF[imagesNamesInPlaceToSend[0]];
                                        imageWrap.parentNode.removeChild(imageWrap);
                                        sendFileBlocks = {};
                                    }
                                    if (lastSender != im) {
                                        var thumb = thumbsObj[sender];
                                        lastSenderId = sender;
                                        if (dialogItem) {
                                            dialogItem.innerText = body.length > 15 ? body.slice(0, 15) + "..." : body;
                                            dialogItem.setAttribute("value", messageId);
                                            dialogItem.className = "dialog-text my unread";
                                            var myMessageText = d.createElement("P");
                                            myMessageText.className = "my-message";
                                            myMessageText.innerText = textObj["myMessageText"];
                                            dialogItem.parentNode.insertBefore(myMessageText, dialogItem);
                                        } else {
                                            //
                                        }
                                    } else {
                                        var thumb = null;
                                        if (dialogItem) {
                                            dialogItem.innerText = body.length > 15 ? body.slice(0, 15) + "..." : body;
                                            dialogItem.setAttribute("value", messageId);
                                        }
                                    }
                                }
                            } else {
                                var thumb = thumbsObj[sender];
                                lastSenderId = sender;
                            }
                            var messagesListLink = messagesList;
                            if (method == "messagePersonal") {
                                var newMessageBody = createMessageBodyPersonal(d, d.createElement("LI"), thumb, body, time, isMine, true, messageId, images, files, false, false);
                                if (usersIdsUnreadMessagesObj.hasOwnProperty(sender)) {
                                    var arr = usersIdsUnreadMessagesObj[sender];
                                } else {
                                    var arr = [];
                                    usersIdsUnreadMessagesObj[sender] = arr;
                                }
                                arr.push(messageId);
                            } else {
                                var senders = sendersIdsArr;
                                var newMessageBody = createMessageBodyGroup(d, d.createElement("LI"), thumb, usersNamesObject, body, time,
                                    isMine, senders, true, messageId, images, files, false, false, false, sender);
                                var allSenders = senders.concat();
                                allSenders.splice(allSenders.indexOf(sender), 1);
                                usersIdsUnreadMessagesObj[messageId] = allSenders;
                            }
                            newMessageBody.setAttribute("value", messageId);
                            messagesListLink.appendChild(newMessageBody);
                            messagesListLink.scrollTop = messagesListLink.scrollTopMax || messagesListLink.scrollHeight;
                            if (Object.keys(messagesData).length > 0) {
                                messagesData[0] += newMessageBody.outerHTML;
                            } else {
                                messagesStorage[dId] = { 0: newMessageBody.outerHTML };
                            }
                        } else {
                            // Возврат с сервера моего отправленного сообщения (с этого компьютера)
                            messagesSendedSuccess(messagesList, messageId, dialogItem, dialogsPlace, messageArea, im, lastSenderId, messagesData);
                            if (method == "messagePersonal") {
                                if (usersIdsUnreadMessagesObj.hasOwnProperty(im)) {
                                    var arr = usersIdsUnreadMessagesObj[im];
                                } else {
                                    var arr = [];
                                    usersIdsUnreadMessagesObj[im] = arr;
                                }
                                arr.push(messageId);
                            } else {
                                var allSenders = sendersIdsArr.concat();
                                allSenders.splice(allSenders.indexOf(im), 1);
                                usersIdsUnreadMessagesObj[messageId] = sendersIdsArr;
                            }

                        }
                        usersIdsUnreadMessagesObject === null ? usersIdsUnreadMessagesObject = usersIdsUnreadMessagesObj : 0;
                        data.splice(t, 1);
                    } else {
                        if (dialogsPlace.className == "opened") {
                            // Создать новый элемент списка диалогов, если он не найден
                            var dialogItems = dialogsItemsLinks;
                            var dialogTextItem = dialogItems[dId] || null;
                            if (dialogTextItem) {
                                dialogTextItem.innerText = obj.body;
                                dialogTextItem.setAttribute("value", obj.id);
                                dialogTextItem.className = "dialog-text unread";
                                var dialogsList = dialogsLoadPlace.lastElementChild;
                                var first = dialogsList.firstElementChild;
                                if (first.getAttribute("value") != sender) {
                                    var dialogItem = dialogTextItem.parentNode.parentNode;
                                    dialogsList.removeChild(dialogItem);
                                    dialogsList.insertBefore(dialogItem, first);
                                }
                            } else {
                                var thumbLoad = new XMLHttpRequest();
                                thumbLoad.open("GET", loadDialogLink + "?id=" + sender, true);
                                thumbLoad.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                                thumbLoad.onloadend = function (event) {
                                    var item = JSON.parse((this || event.target).response).newDialog;
                                    var body = item.body;
                                    var status = item.status;
                                    var path = item.link;
                                    var d = doc;
                                    var thumbWrap = d.createElement("DIV");
                                    thumbWrap.innerHTML = item.thumb;
                                    var thumb = thumbWrap.firstElementChild;
                                    var dialogListItem = d.createElement("LI");
                                    var link = d.createElement("A");
                                    var text = d.createElement("P");
                                    var timeText = d.createElement("SMALL");
                                    timeText.className = "dialog-time";
                                    text.className = "dialog-text unread";
                                    text.innerText = body;
                                    link.setAttribute("href", path);
                                    dialogListItem.setAttribute("value", sender);
                                    link.appendChild(thumb);
                                    link.appendChild(text);
                                    link.appendChild(timeText);
                                    dialogListItem.appendChild(link);
                                    var dialogsList = dialogsLoadPlace.lastElementChild;
                                    dialogsList.insertBefore(dialogListItem, dialogsList.firstElementChild);
                                    dialogsItemsLinks[sender] = text;
                                };
                                thumbLoad.send(null);
                            }
                        }
                    }
                } else if (method == 'dialogAttachment') {
                    if (sendersIdsArr.indexOf(obj.toId) > -1) {
                        var item = obj.image;
                        var name = Object.keys(item)[0];
                        var file = item[name];
                        addFilesToUpload(doc, name, file, sendImagesLink);
                    }
                    data.splice(t, 1);
                } else if (method == 'stateOnline' || method == 'stateTimer' || method == 'stateLeave') {
                    var userId = obj.userId;
                    var requestUserId = myId;
                    if (requestUserId != userId && sendersIdsArr.indexOf(userId) > -1) {
                        if (method == 'stateOnline') {
                            var mainContext = mainContextLinks[userId];
                            if (onlineStatusActiveAnimations && onlineStatusActiveAnimations.hasOwnProperty(userId)) {
                                clearInterval(onlineStatusActiveAnimations[userId]);
                                delete onlineStatusActiveAnimations[userId];
                            } else {
                                drawFast(mainContext, 46.5, 38.5, 4, 0, 360, "#00AFFF");
                            }
                            drawFast(mainContext, 46.5, 42.5, 4, 0, 360, "#47b647");
                        } else if (method == 'stateTimer') {
                            if (onlineStatusActiveAnimations && !onlineStatusActiveAnimations.hasOwnProperty(userId)) {
                                var time = 20000 / 360;
                                var mainContext = mainContextLinks[userId];
                                onlineStatusActiveAnimations[userId] = draw(mainContext, 46.5, 42.5, 4, time, 360, "#FFF5F5",
                                    function () {
                                        delete onlineStatusActiveAnimations[userId];
                                        drawFast(mainContext, 46.5, 38.5, 8, 0, 360, "#FFF5F5");
                                    });
                            }
                        } else if (method == 'stateLeave') {
                            if (onlineStatusActiveAnimations && onlineStatusActiveAnimations.hasOwnProperty(userId)) {
                                clearInterval(onlineStatusActiveAnimations[userId]);
                                delete onlineStatusActiveAnimations[userId];
                            }
                            drawFast(mainContextLinks[userId], 46.5, 38.5, 8, 0, 360, "#FFF5F5");
                        }
                    }
                    data.splice(t, 1);
                } else if (method == "stateActive" || method == "stateMiddle" || method == "stateLeave") {
                    var userId = obj.id;
                    var requestUserId = myId;
                    if (requestUserId != userId && sendersIdsArr.indexOf(userId) > -1) {
                        if (method == "stateActive") {
                            if (activeStatusActiveAnimations && activeStatusActiveAnimations.hasOwnProperty(userId)) {
                                clearInterval(activeStatusActiveAnimations[userId]);
                                delete activeStatusActiveAnimations[userId];
                            }
                            drawFast(mainContextLinks[userId], 46.5, 38.5, 4, 0, 360, "#00AFFF");
                        } else if (method == "stateMiddle") {
                            if (activeStatusActiveAnimations && !activeStatusActiveAnimations.hasOwnProperty(userId)) {
                                var context = mainContextLinks[userId];
                                drawFast(context, 46.5, 38.5, 4, 0, 360, "#FF7f00");
                                activeStatusActiveAnimations[userId] = draw(context, 46.5, 38.5, 4, 210000 / 360, 360, "#D74620");
                            }
                        } else {
                            if (activeStatusActiveAnimations && activeStatusActiveAnimations.hasOwnProperty(userId)) {
                                clearInterval(activeStatusActiveAnimations[userId]);
                                delete activeStatusActiveAnimations[userId];
                            }
                            drawFast(mainContextLinks[userId], 46.5, 38.5, 4, 0, 360, "#D74620");
                        }
                    }
                    data.splice(t, 1);
                }
            }
        }
        data.length > 0 ? defaultListener(data) : 0;
    };
    function createMessageBodyPersonal(doc, li, thumb, body, time, isMine, unreadArr, id, images, files, returnSendedFromXHR, isOwn, groupLoad) {
        /* isOwn == true - сообщение было набрано isOwn == false - сообщение пришло иным путём (отправлено собеседником/подгружено)
         * groupLoad - сообщение получено через подгрузку скроллом или при начальной подгрузке.
         * */
        if (thumb === null) {
            if (unreadArr !== null) {
                if (unreadArr === true) {
                    li.className = isMine ? "my-message unread" : "not-my-message unread";
                } else if (unreadArr.indexOf(id) != -1) {
                    li.setAttribute("value", id);
                    li.className = isMine ? "my-message unread" : "not-my-message unread";
                } else {
                    li.className = isMine ? "my-message" : "not-my-message";
                }
                var time = new Date(Date.parse(time));
            } else {
                var time = new Date(Date.now(time));
                id ? li.setAttribute("value", id) : 0;
                li.className = isMine ? groupLoad ? "my-message" : returnSendedFromXHR ?
                    "my-message unsend" : "my-message unread" : "not-my-message";
            }
        } else {
            if (unreadArr !== null) {
                if (unreadArr === true) {
                    li.className = isMine ? "my-message unread" : "not-my-message unread";
                } else if (unreadArr.indexOf(id) != -1) {
                    li.setAttribute("value", id);
                    li.className = isMine ? "my-message unread start-group" : "not-my-message unread start-group";
                } else {
                    li.className = isMine ? "my-message start-group" : "not-my-message start-group";
                }
                var time = new Date(Date.parse(time));
            } else {
                var time = new Date(Date.now(time));
                id ? li.setAttribute("value", id) : 0;
                li.className = isMine ? groupLoad ? "my-message start-group" : returnSendedFromXHR ?
                    "my-message unsend start-group" : "my-message unread start-group" : "not-my-message start-group";
            }
            li.appendChild(thumb.cloneNode(true))
        }
        var div = doc.createElement("DIV");
        var properties = div.cloneNode(false);
        properties.className = "message-properties";
        var small = doc.createElement("SMALL");
        small.innerText = time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();
        properties.appendChild(small);
        li.appendChild(properties);
        if (body) {
            var htmlContentWithLinks = wrapMessageBodyLinks(body, null, null, null, false);
            var textBody = div.cloneNode(false);
            textBody.className = "message-textbody";
            var p = doc.createElement("P");
            htmlContentWithLinks ? p.innerHTML = htmlContentWithLinks : p.innerText = body;
            textBody.appendChild(p);
            li.appendChild(textBody);
        }
        if (images) {
            var imagesAttachment = div.cloneNode(false);
            imagesAttachment.className = "images-attachment";
            if (isMine && returnSendedFromXHR) {
                for (var i = 0; i < images.length; i++) {
                    var imageNodeWrap = images[i];
                    imageNodeWrap.removeChild(imageNodeWrap.lastElementChild);
                    imagesAttachment.appendChild(images[i]);
                }
            } else {
                var image = doc.createElement("IMG");
                image.setAttribute("lazyload", "")
                for (var i = 0; i < images.length; i++) {
                    var img = image.cloneNode(false);
                    img.src = images[i];
                    imagesAttachment.appendChild(img);
                }
            }
            li.appendChild(imagesAttachment);
        }
        return li;
    };
    function createMessageBodyGroup(doc, li, thumb, usersNamesObject, body, time, isMine,
        allSenders, unreadObjArr, id, images, files, returnSendedFromXHR, isOwn, groupLoad, senderId) {
        /* isOwn == true - сообщение было набрано isOwn == false - сообщение пришло иным путём (отправлено собеседником/подгружено) 
        groupLoad - сообщение получено через подгрузку скроллом или при начальной подгрузке.
        senderId - передаётся не всегда */
        function createUnreadLightbox(d, li, allSenders, unreadUsersArr, namesObj, isReturn, isMine, sender) {
            /**
             * isReturn - это сообщение пришло с вебсокета или 
             * Вызов был из стека с функцией "returnMessage" (вставка набранного сообщения, после xhr отправки, с событием onloadstart)
             * **/
            li.removeAttribute("class");
            li.removeAttribute("value");
            var ul = d.createElement("UL");
            ul.className = "unread-users-box hidden";
            var listHead = d.createElement("P");
            var sm = d.createElement("SMALL");
            if (isReturn == true) {
                if (isMine) {
                    var userIndex = unreadUsersArr.indexOf(myId);
                } else {
                    var userIndex = unreadUsersArr.indexOf(sender);
                }
                userIndex > -1 ? unreadUsersArr.splice(userIndex, 1) : 0;
            }
            var unreadUsersArrLen = unreadUsersArr.length;
            listHead.innerText = unreadUsersArrLen + "/" + allSenders.length;
            var lic = li.cloneNode(false);
            lic.className = "unread-users-box-head";
            lic.appendChild(listHead);
            ul.appendChild(lic);
            for (var t = unreadUsersArrLen; t--;) {
                var id = unreadUsersArr[t];
                var smc = sm.cloneNode(false);
                smc.innerText = namesObj[id];
                var lic = li.cloneNode(false);
                lic.setAttribute("value", id);
                lic.appendChild(smc);
                ul.appendChild(lic);
            }
            return ul;
        };
        if (thumb === null) {
            if (unreadObjArr !== null) {
                if (typeof(unreadObjArr) == "object" && unreadObjArr.hasOwnProperty(id)) {
                    li.setAttribute("value", id);
                    li.className = isMine ? "my-message unread" : "not-my-message unread";
                } else {
                    li.className = unreadObjArr == true ? isMine ? "my-message unsend" : "not-my-message unread" : isMine ? "my-message" : "not-my-message";
                }
                var time = new Date(Date.parse(time));
            } else {
                var time = new Date(Date.now(time));
                id ? li.setAttribute("value", id) : 0;
                li.className =  isMine ? groupLoad ? "my-message" : returnSendedFromXHR ?
                    "my-message unsend" : "my-message" : "not-my-message";
            }
        } else {
            li.appendChild(thumb.cloneNode(true));
            if (unreadObjArr !== null) {
                if (typeof(unreadObjArr) == "object" && unreadObjArr.hasOwnProperty(id)) {
                    li.setAttribute("value", id);
                    li.className = isMine ? "my-message unread start-group" : "not-my-message unread start-group";
                } else {
                    li.className = unreadObjArr == true ? isMine ? "my-message unsend" : "not-my-message unread" : isMine ? "my-message" : "not-my-message";
                }
                var time = new Date(Date.parse(time));
            } else {
                var time = new Date(Date.now(time));
                id ? li.setAttribute("value", id) : 0;
                li.className = isMine ? groupLoad ? "my-message start-group" : returnSendedFromXHR ?
                    "my-message unsend start-group" : "my-message start-group" : "not-my-message start-group";
            }
        }
        var div = doc.createElement("DIV");
        var properties = div.cloneNode(false);
        properties.className = "message-properties";
        var small = doc.createElement("SMALL");
        small.innerText = time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();
        properties.appendChild(small);
        li.appendChild(properties);
        if (body) {
            var htmlContentWithLinks = wrapMessageBodyLinks(body, null, null, null, false);
            var textBody = div.cloneNode(false);
            textBody.className = "message-textbody";
            var p = doc.createElement("P");
            htmlContentWithLinks ? p.innerHTML = htmlContentWithLinks : p.innerText = body;
            textBody.appendChild(p);
            li.appendChild(textBody);
        }
        if (images) {
            var imagesAttachment = div.cloneNode(false);
            imagesAttachment.className = "images-attachment";
            if (isMine && returnSendedFromXHR) {
                for (var i = 0; i < images.length; i++) {
                    var imageNodeWrap = images[i];
                    imageNodeWrap.removeChild(imageNodeWrap.lastElementChild);
                    imagesAttachment.appendChild(images[i]);
                }
            } else {
                var image = doc.createElement("IMG");
                image.setAttribute("lazyload", "")
                for (var i = 0; i < images.length; i++) {
                    var img = image.cloneNode(false);
                    img.src = images[i];
                    imagesAttachment.appendChild(img);
                }
            }
            li.appendChild(imagesAttachment);
        }
        /* unreadObjArr == true, если сообщение пришло с вебсокета; unreadObjArr == true в сочетании с isMine окажет влияние
         * на результат функции "createUnreadLightbox" */
        unreadObjArr !== null ? unreadObjArr === true ?
            li.appendChild(createUnreadLightbox(doc, li.cloneNode(false), allSenders, allSenders.concat(), usersNamesObject, true, isMine, senderId)) :
            unreadObjArr.hasOwnProperty(id) ?
                li.appendChild(createUnreadLightbox(doc, li.cloneNode(false), allSenders, unreadObjArr[id], usersNamesObject, false, false)) : 0 : 0;
        return li;
    };
    function insertMessages(d, messages, unread, myId, thumbs, previousListItem, dialogType, usersNamesObj, allUsers) {
        /* Последних 2 аргументов нету при dialogType == true */
        var messageElements = d.createElement("UL");
        var mesLen = messages.length;
        if (previousListItem) {
            var previousSenderThumb = previousListItem.firstElementChild;
            var previousSenderFirst = previousSenderThumb.getAttribute("value");
        } else {
            var previousSenderFirst = null;
        }
        var lSId = lastSenderId;
        var previousSender = lSId;
        var liElem = d.createElement("LI");
        if (dialogType) {
            unread !== null ? unread = Array.prototype.join.call(Object.values(unread)).split(",") : 0;
            for (var i = 0; i < mesLen; i++) {
                var message = messages[i];
                var id = message.id;
                var by = message.by;
                var thumb = i > 0 ? previousSender == by ? null : thumbs[by] : thumbs[by];
                var previousSender = by;
                var body = message.body;
                var time = message.time;
                var images = message.images || null;
                var files = message.files || null;
                var isMine = by == myId;
                if (i == (mesLen - 1)) {
                    if (by == previousSenderFirst) {
                        var initialListItemClass = previousListItem.className;
                        previousListItem.className = initialListItemClass.slice(0, initialListItemClass.indexOf(" start-group"));
                        previousListItem.removeChild(previousSenderThumb);
                    }
                }
                var messageElement = createMessageBodyPersonal(d, liElem.cloneNode(false), thumb, body, time, isMine, unread, id, images, files, false, false, true);
                messageElements.appendChild(messageElement);
            }
            mesLen > 0 ? lSId != previousSender ? lastSenderId = by : 0 : 0;
        } else {
            for (var i = 0; i < mesLen; i++) {
                var message = messages[i];
                var id = message.id;
                var by = message.by;
                var thumb = i > 0 ? previousSender == by ? null : thumbs[by] : thumbs[by];
                var previousSender = by;
                var body = message.body;
                var time = message.time;
                var images = message.images || null;
                var files = message.files || null;
                var isMine = by == myId;
                if (i == (mesLen - 1)) {
                    if (by == previousSenderFirst) {
                        var initialListItemClass = previousListItem.className;
                        previousListItem.className = initialListItemClass.slice(0, initialListItemClass.indexOf(" start-group"));
                        previousListItem.removeChild(previousSenderThumb);
                    }
                }
                var messageElement = createMessageBodyGroup(d, liElem.cloneNode(false), thumb, usersNamesObj, body,
                    time, isMine, allUsers, unread, id, images, files, false, false, true);
                messageElements.appendChild(messageElement);
            }
            mesLen > 0 ? lSId != previousSender ? lastSenderId = by : 0 : 0;
        }
        return messageElements;
    };
    /* xhr load Messages */
    var xhrLoaderMessages = false;
    var messageLoaderPage = 1;
    function endLoadMessages(d, dialogType, messages, unread, ul, id, thumbs, page, s, dId) {
        if (messages) {
            if (dialogType) {
                if (unread !== null) {
                    var messageElements = insertMessages(d, messages, unread, id, thumbs, ul.firstElementChild, true);
                    unread.hasOwnProperty(id) ? sendReadMessagesPersonal(unread, s, dId, sendersIdsArr[1] + "") : 0;
                } else {
                    var messageElements = insertMessages(d, messages, null, id, thumbs, ul.firstElementChild, true);
                }
            } else {
                var messageElements = insertMessages(d, messages, unread, id, thumbs, ul.firstElementChild, false, usersNamesObject, sendersIdsArr);
                unread !== null ? sendReadMessagesGroup(unread, s, dId, id) : 0;
            }
            var messagesInner = messageElements.innerHTML;
            ul.innerHTML = messagesInner + ul.innerHTML;
            ul.scrollTop = 1;
            removeShortNotifyItems(d, ul);
            var pagesObj = messagesStorage[dId] || {};
            pagesObj[page] = messagesInner;
            if (page == 0) {
                messagesStorage[dId] = pagesObj;
            }
        }
    };
    function loadMessages(messagesUL, page, id, thumbs, dialogType, dialogId) {
        var messagesLoad = new XMLHttpRequest();
        var d = doc;
        messagesLoad.open("GET", d.location.pathname + "?list=" + page, true);
        messagesLoad.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        messagesLoad.responseType = "text";
        messagesLoad.onloadstart = function(event) {
            xhrLoaderMessages = messagesLoad;
        };
        messagesLoad.onerror = function(event) {
            xhrLoaderMessages = false;
        };
        messagesLoad.onloadend = function(event) {
            var target = this || event.target;
            if (target.readyState == 4 && target.status == 200) {
                var messagesData = JSON.parse(target.response);
                var items = messagesData.messagesItems;
                if (items) {
                    var messages = items.messages || null;
                    if (messages && messages.length > 0) {
                        var unread = items.unread || null;
                        var p = page;
                        endLoadMessages(d, dialogType, messages, (unread !== null ? unread.length > 0 ? unread : null : null), messagesUL, id, thumbs, p, sock, dialogId);
                        messageLoaderPage = p + 1;
                    } else {
                        var dId = dialogId;
                        var pagesObj = messagesStorage[dId] || {};
                        pagesObj[page] = null;
                        if (Object.keys(pagesObj).length == 0) {
                            messagesStorage[dId] = pagesObj;
                        }
                        messageLoaderPage = null;
                    }
                } else {
                    var dId = dialogId;
                    var pagesObj = messagesStorage[dId] || {};
                    pagesObj[page] = null;
                    if (Object.keys(pagesObj).length == 0) {
                        messagesStorage[dId] = pagesObj;
                    }
                    messageLoaderPage = null;
                }
                xhrLoaderMessages = false;
            } 
        };
        messagesLoad.send(null);
    };
    var messagesStorage = {}; // {dialogId: {page: messagesInnerHTMLStr, page: messagesInnerHTMLStr}}
    var dataToSendStorage = {}; // {dialogId: "images": []}
    function checkScroll(event) {
        if (xhrLoaderMessages == false) {
            var element = this || event.target;
            if (element.scrollTop == 0) {
                var page = messageLoaderPage;
                if (page !== null) {
                    var mesStorage = messagesStorage;
                    var dId = dialogId;
                    if (mesStorage.hasOwnProperty(dId)) {
                        var mesStorageDialog = mesStorage[dId];
                        if (mesStorageDialog.hasOwnProperty(page)) {
                            var itemsString = mesStorageDialog[page];
                            if (itemsString !== null) { // Последняя страница
                                if (itemsString) {
                                    var ul = messagesList;
                                    ul.innerHTML = mesStorageDialog[page] + ul.innerHTML;
                                    messageLoaderPage = page + 1;
                                } else {
                                    loadMessages(element, page, myId, thumbsObj, type, dId);
                                }
                            }
                        } else {
                            loadMessages(element, page, myId, thumbsObj, type, dId);
                        }
                    } else {
                        loadMessages(element, page, myId, thumbsObj, type, dId);
                    }
                }
            }
        }
    };
    /* end xhr load Messages */
    function returnMessage(d, form, myId, sendFileBlocks, sendedMessagesActive, lastSender, messagesUl, dialogType) {
        var messagePlace = messageArea;
        var imagesObj = sendFileBlocks;
        var imagesObjKeys = Object.keys(imagesObj);
        var imagesObjKeysLength = imagesObjKeys.length;
        if (imagesObjKeysLength > 0) {
            var imagesNodesArr = [];
            for (var i = 0; i < imagesObjKeysLength; i++) {
                imagesNodesArr.push(imagesObj[imagesObjKeys[i]]);
            }
        } else {
            var imagesNodesArr = null;
        }
        function insertMyMessage(doc, dialogType, lastSender, messagesActive, text, time, images, id) {
            var fElemChild = messagePlace.firstElementChild || null;
            var thumb = lastSender != id && messagesActive.length == 0 ? thumbsObj[id] : null;
            var messageElement = dialogType ? createMessageBodyPersonal(doc, doc.createElement("LI"), thumb, text, time, true, null, null, images, null, true) :
                createMessageBodyGroup(doc, doc.createElement("LI"), thumb, usersNamesObject, text, time,
                    true, sendersIdsArr, true, null, images, null, true, false);
            if (fElemChild) {
                fElemChild.insertBefore(messageElement, fElemChild);
            } else {
                messagesUl.appendChild(messageElement);
            }
            sendedMessagesActive.push(messageElement);
        };
        insertMyMessage(d, dialogType, lastSender, sendedMessagesActive, messagePlace.value, time, imagesNodesArr, myId);
        // Заблокировать поля на редактирование, кнопку
        messagesUl.scrollTop = messagesUl.scrollTopMax || messagesUl.scrollHeight;
    };
    function messagesSendedSuccess(ul, messageId, dialogItem, dialogsPlace, messagePlace, id, lastSender, messagesDataObj) {
        var unsendedMessage = ul.getElementsByClassName("unsend")[0] || null;
        if (unsendedMessage) {
            unsendedMessage.setAttribute('value', messageId);
            unsendedMessage.className = unsendedMessage.className.replace("unsend", "unread");
            if (Object.keys(messagesDataObj).length > 0) {
                messagesDataObj[0] += unsendedMessage.outerHTML;
            } else {
                messagesStorage[dId] = {0: unsendedMessage.outerHTML};
            }
        }
        sendFileBlocks.length > 0 ? formAndMessagesListTransform(formWrap, ul) : 0;
        sendFileBlocks = {};
        if (dialogItem !== null) {
            dialogItem.setAttribute("value", messageId);
            dialogItem.className = "dialog-text my unread";
            var messageText = messagePlace.value;
            dialogItem.innerText = messageText.length > 15 ? messageText.slice(0, 15) + "..." : messageText;
            sendedMessagesActive.length -= 1;
            if (lastSender != id) {
                var myMessageText = doc.createElement("P");
                myMessageText.className = "my-message";
                myMessageText.innerText = textObj["myMessageText"];
                var link = dialogItem.parentNode;
                link.insertBefore(myMessageText, dialogItem);
                lastSenderId = id;
            } else {
                var link = dialogItem.parentNode;
            }
            var place = dialogsPlace.lastElementChild;
            var first = place.firstElementChild;
            if (first.getAttribute("value") != dialogId) {
                var dialog = link.parentNode;
                place.insertBefore(dialog, place.firstElementChild);
            }
        } else {
            lastSender != id ? lastSenderId = id : 0;
            sendedMessagesActive.length -= 1;
        }
        messagePlace.value = "";
        messagePlace.focus();
    };
    function sendReadMessagesPersonal(messagesIdsArray, sock, dialogId, messageSenderId) {
        if (sock.readyState == 1) {
            sock.send(JSON.stringify({
                "type": "readPersonalMessage",
                "readedMessagesIds": messagesIdsArray,
                "dialogId": dialogId,
                "sender": messageSenderId
            }));
        } else {
            sock.onopen = function() {
                sock.send(JSON.stringify({
                    "type": "readPersonalMessage",
                    "readedMessagesIds": messagesIdsArray,
                    "dialogId": dialogId,
                    "sender": messageSenderId
                }));
            };
        }
    };
    function sendReadMessagesGroup(messagesIdsObjectArray, sock, dialogId, userId) {
        var messageIds = Object.keys(messagesIdsObjectArray);
        var messageIdsLength = messageIds.length;
        if (messageIdsLength > 0) {
            var messageIdsToSend = [];
            for (var i = messageIdsLength; i--;) {
                var messageId = messageIds[i];
                messagesIdsObjectArray[messageId].indexOf(userId) > -1 ? messageIdsToSend.push(messageId) : 0;
            }
            if (messageIdsToSend.length > 0) {
                if (sock.readyState == 1) {
                    sock.send(JSON.stringify({
                        "type": "readGroupMessage",
                        "readedMessagesIds": messageIdsToSend,
                        "dialogId": dialogId
                    }));
                } else {
                    sock.onopen = function () {
                        sock.send(JSON.stringify({
                            "type": "readGroupMessage",
                            "readedMessagesIds": messageIdsToSend,
                            "dialogId": dialogId
                        }));
                    };
                }
            }
        }
    };
    if (sock !== null) {
        sock.onmessage = mainSockListener;
    }

    /* Начальная вставка сообщений (экономия тактов на сервере) */
    function hideAllUnreadUsersBoxes(d) {
        /* Функция для скрытия элементов с классом "unread-users-box",
        * отображающих список пользователей не прочитавших сообщение.
        * Этот список получит подкласс "nojs", 
        * если список сообщений был отрендерен на сервере. В таком случае, если JavaScript всё же работает, 
        * класс "nojs" необходимо заменить на "hidden". */
        var boxesCollection = d.getElementsByClassName("unread-users-box nojs");
        for (var i = boxesCollection.length; i--;) {
            boxesCollection[i].className = "unread-users-box hidden";
        }
    };
    if (initialMessagesObject) {

        endLoadMessages(doc, type, JSON.parse(initialMessagesObject), usersIdsUnreadMessagesObject, messagesList, myId, thumbsObj, 0, sock, dialogId)
    } else {
        if (type == false) {
            hideAllUnreadUsersBoxes(doc);
        }
        messagesStorage[dialogId] = {0: messagesList.innerHTML};
    }
    messagesList.scrollTop = messagesList.scrollTopMax || messagesList.scrollHeight;
    /****/
    var lastSenderId = checkLastSenderId(messagesList);
    return mainSockListener;
};
var mainSockListener = initMessages();