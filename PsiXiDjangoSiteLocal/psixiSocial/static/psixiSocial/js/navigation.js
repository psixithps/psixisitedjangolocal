responseExtendedProperties = null;
function loadReadyCallBack(body) {
    body.removeAttribute("class");
    window.onSubmitSimplifield ? onSubmitSimplifield(body) : 0;
};
function localNavigation(event, refUrl) {
    loadContent("socialContent", refUrl);
    var w = window;
    w.mainSockListener ? mainSockListener = null : 0;
    w.userSock ? userSock.onmessage = defaultListener : 0;
    w.statusInitial ? statusInitial = null : 0;
    if (event) {
        return event.preventDefault();
    }
};