from django.conf import settings
from imagekit import ImageSpec

def generateUsersListThumb(image):
    imageFullName = image.name
    newImagePath = '%spsixiSocial/CACHE/users/100x100/%s' % (settings.MEDIA_URL, imageFullName)
    fullNewImagePath = '%s/%s' % (settings.BASE_DIR, newImagePath)
    if path.exists(fullNewImagePath):
        return newImagePath
    dotIndex = imageFullName.rfind(".") + 1
    name = imageFullName[0 : dotIndex]
    frmat = imageFullName[dotIndex : len(imageFullName)]
    if frmat == "jpg":
        frmat = "jpeg"
    class thumbResize(ImageSpec):
        processors = [ResizeTiFill(100, 100)]
        format = frmat
        options = {'quality': 60}
    with open(image.url, 'r') as originalImage:
        newImg = thumbResize(source = originalImage)
        newImgGenerated = newImg.generate()
    with open(newImagePath, 'wb') as thumb:
        thumb.write(newImgGenerated.read())
    return newImagePath