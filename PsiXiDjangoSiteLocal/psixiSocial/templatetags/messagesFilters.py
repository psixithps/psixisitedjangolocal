from django import template
from psixiSocial.models import imagesAttachmentMessagePersonal, imagesAttachmentMessageGroup

register = template.Library()

@register.filter
def getAttachmentInstancePersonal(instance, id):
    try:
        find = instance.get(message_id__exact = id)
    except imagesAttachmentMessagePersonal.DoesNotExist:
        return None
    return find

@register.filter
def getAttachmentInstanceGroup(instance, id):
    try:
        find = instance.get(message_id__exact = id)
    except imagesAttachmentMessageGroup.DoesNotExist:
        return None
    return find

