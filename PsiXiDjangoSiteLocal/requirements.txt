channels==2.3.0
channels-redis==2.4.1
Django==2.1
django-appconf==1.0.2
django-cities==0.5.0.6
django-cprofile-middleware==1.0.4
django-imagekit==3.3
django-ipware==2.0.1
moviepy==0.2.2.13
Pillow==6.2.1
python-dateutil==2.8.0
requests==2.22.0
imageio-ffmpeg==0.3.0
django-adv-cache-tag==1.1.2
-e git+https://github.com/dlrust/python-memcached-stats.git#egg=memcached_stats