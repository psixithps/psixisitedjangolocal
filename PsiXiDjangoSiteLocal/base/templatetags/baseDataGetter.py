import json
from django import template
from base.helpers import checkCSRF
from functools import lru_cache

@lru_cache(maxsize = None)
class notificationText():
    def __init__(self, *args, **kwargs):
        self.langudge = kwargs.pop('lang')

    def ru(self):
        textDict = {
            # notify
            'notifyItemProperties': {
                'hide': 'скрыть',
                'hideAll': 'скрыть всё',
                'getList': 'Отобразить список обновлений',
                'closeList': 'Скрыть список обновлений'
                },
            # Messages
            'messageMany': {
                0: 'написал вам',
                'dialogText': 'Перейти к диалогу',
                },
            'messagePersonalCountText': {
                1: 'личное сообщение',
                5: 'личных сообщения',
                'more': 'личных сообщений'
                },
            # Friends
            'addFriend': {
                'dialogText': 'Перейти к диалогу',
                'addFriendText': 'Добавить в друзья'
                },
            'friendRequest': {
                'text': 'Вы ожидаете решения по вашему запросу о дружбе',
                'dialogText': 'Перейти к диалогу',
                'cancelFriend': 'Я передумал, отменить запрос',
                },
            'removeFriend': {
                'text': 'Ваш друг',
                'dialogText': 'Перейти к диалогу',
                'removeText': 'Удалить из друзей'
                },
            'friendRequestToMeNotify': {
                'text': 'отправил вам запрос о дружбе',
                'confirm': 'Подтвердить',
                'reject': 'Отклонить',
                },
            'friendRejected': {
                'text': 'отклонил ваше предложение о дружбе'
                },
            'friendConfirmed': {
                'text': 'принял вашу заявку и теперь он ваш друг'
                },
            'friendRemoved': {
                'text': 'Удалил вас из друзей'
                }
            }
        return textDict

    def getDict(self):
        defineMethod = getattr(self, self.langudge)
        pack = defineMethod()
        pack = json.dumps(pack)
        return pack

register = template.Library()

@register.simple_tag()
def getCSRF(request):
    return checkCSRF(request, request.COOKIES)

@register.simple_tag()
def getNotifyTextDict(request):
    textDict = notificationText(lang = 'ru').getDict()
    return textDict