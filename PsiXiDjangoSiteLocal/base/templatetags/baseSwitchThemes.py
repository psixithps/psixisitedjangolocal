from base import models
from django import template

register = template.Library()

@register.inclusion_tag('base/switchThemesConfig.html')

def initOptionsSwitchThemes():
    result = 15
    return {
        "result": result
        }