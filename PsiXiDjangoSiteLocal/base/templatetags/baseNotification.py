from django import template
from random import randint
from django.core.cache import caches
from psixiSocial.models import dialogMessage, personalMessage

register = template.Library()

@register.inclusion_tag('base/smartNotification.html')

def countUpdates(request):
    id = str(request.user.id)
    updatesCache = caches["newUpdates"]
    '''
    groupMessagesCache = caches['unreadGroupMessages']
    groupMessagesCache.clear()
    dialogsCache = caches['dialogsCache']
    smallThumbsCache = caches['smallThumbs']
    privacyCache = caches['privacyCache']
    friendShipStatusCache = caches['friendShipStatusCache']
    friendsCache = caches['friendsCache']
    friendsIdsCache = caches['friendsIdCache']
    friendshipCache = caches["friendshipRequests"]
    caches["unreadPersonalMessages"].clear()
    friendshipCache.clear()
    updatesCache.clear()
    friendsCache.clear()
    friendsIdsCache.clear()
    privacyCache.clear()
    friendShipStatusCache.clear()
    smallThumbsCache.clear()
    dialogsCache.clear()
    '''
    updates = updatesCache.get(id, None)
    if updates is not None:
        personalMessagesToGetSet = set()
        groupMessagesToGetSet = set()
        updatesLength = 0
        itemsCounter = 0
        for update in updates:
            if update[1] != "hidden":
                type = update[0]
                if type == "personalMessage":
                    personalMessagesToGetSet.add(update[3])
                elif type == "groupMessage":
                    groupMessagesToGetSet.add(update[3])
                else:
                    updatesLength += 1
                    itemsCounter += 1
        if len(personalMessagesToGetSet) > 0 or len(groupMessagesToGetSet):
            if len(personalMessagesToGetSet) > 0:
                personalMessagesDictMany = caches["unreadPersonalMessages"].get_many(personalMessagesToGetSet)
                for personalMessagesDictSet in personalMessagesDictMany.values():
                    messagesSet = personalMessagesDictSet.get(id, None)
                    if messagesSet is not None:
                        itemsCounter += len(messagesSet)
                        updatesLength += 1
            if len(groupMessagesToGetSet) > 0:
                groupMessagesDictMany = caches['unreadGroupMessages'].get_many(groupMessagesToGetSet)
                groupMessagesDictManyValues = groupMessagesDictMany.values()
                if len(groupMessagesDictManyValues) > 0:
                    for groupMessagesDict in groupMessagesDictManyValues:
                        itemsCounter += len(groupMessagesDict.keys())
                    updatesLength += 1
        if itemsCounter > 0:
            return {'updatesCount': updatesLength, 'itemsCount': itemsCounter, 'baseend': randint(100, 999)}
    return None