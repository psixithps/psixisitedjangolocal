from django import template
import datetime

register = template.Library()

@register.simple_tag

def currentYear():
    return datetime.datetime.now().year