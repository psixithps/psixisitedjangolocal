from django import template

register = template.Library()

@register.filter
def isInstanceItemInSetOnce(items, item):
    if item in items:
        items.remove(item)
        return True
    else:
        return False

@register.filter
def isInstanceUUIDItemInSetOnce(items, itemUUID):
    item = str(itemUUID)
    if item in items:
        items.remove(item)
        return True
    else:
        return False

@register.filter
def getValueFromDict(d, k):
    return d[str(k)]

@register.filter
def popValueFromDict(d, k):
    return d.pop(str(k), None)

@register.filter
def getValueFromList(l, i):
    return l[str(k)]

@register.filter
def getDictKeysToSet(d):
    return set(d.keys())

@register.filter
def convertUUIDToStr(uuid):
    return str(uuid)

@register.filter
def getLengthSpec(iterable):
    return len(iterable) - 1