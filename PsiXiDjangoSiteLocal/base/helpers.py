from django.middleware.csrf import get_token

def checkCSRF(request, cookies):
    csrfValue = cookies.get('csrftoken', None)
    if csrfValue is None:
        csrfValue = get_token(request)
    return csrfValue