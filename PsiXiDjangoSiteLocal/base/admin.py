from django.contrib import admin
from cities.models import AlternativeName, City, Country, District, Region, Subregion, PostalCode

admin.site.unregister(AlternativeName)
admin.site.unregister(City)
admin.site.unregister(Country)
admin.site.unregister(District)
admin.site.unregister(Region)
admin.site.unregister(Subregion)
admin.site.unregister(PostalCode)