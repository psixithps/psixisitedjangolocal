from psixiSocial.forms import loginForm
from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect

def loginView(request):
    if not request.user.is_authenticated:
        if request.method == "POST":
            authForm = loginForm(request.POST)
            if authForm.is_valid():
                name = authForm.cleaned_data["username"]
                passw = authForm.cleaned_data["password"] 
                logIn = authenticate(username = name, password = passw)
                login(request, logIn)
                return redirect("profile", "my")
        else:
            authForm = loginForm()
        return render(request, "base/baseLoginPage.html", {'form': authForm})
    else:
        return redirect("profile", "my")