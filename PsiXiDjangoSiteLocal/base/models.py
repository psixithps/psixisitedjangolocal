from django.contrib.postgres.fields import JSONField
from django.db import models

STATUS = (
    ('d', 'Снято с публикации'),
    ('p', 'Опубликовано'),
    ('w', 'Ожидает модерации'),
    ('n', 'Не дописано полностью'),    
)

class levels(models.Model):
    name = models.CharField(max_length = 15)

class themes(models.Model):
    name = models.CharField(max_length = 15)
