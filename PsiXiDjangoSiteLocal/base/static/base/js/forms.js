function doPostForm(bodyID, formValuesObj, path) {
    /* Надо делать валидацию форм на фронтенде, медот checkValiditi() */
    if (window.hasOwnProperty('FormData')) {
        formValuesObj.csrfmiddlewaretoken.value == "" ? formValuesObj.csrfmiddlewaretoken.value = window.csrf : 0;
        var fData = new FormData(formValuesObj);
    } else {
        var fData = {};
        var formInner = formValuesObj.getElementsByTagName("input");
        var formInner1 = formValuesObj.getElementsByTagName("textarea");
        var fDataLength = formInner.length + formInner1.length - 1;
        for (var i = 0; i < fDataLength; i++) {
            var item = formInner1[i] || formInner[i];
            var itemName = item.name;
            var itemValue = item.value;
            if (itemValue == "" && itemName == "csrfmiddlewaretoken") {
                fData[itemName] = window.csrf;
            } else {
                fData[itemName] = itemValue;
            }
        }
    }
    var request = new XMLHttpRequest();
    request.open("POST", path, true);
    request.responseType = "json";
    request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    if (document.addEventListener) {
        request.addEventListener("load", successForm, false);
    } else {
        request.attachEvent("load", successForm, false);
    }
    function successForm(event) {
        var thisEvent = this || event.target;
        if (thisEvent.status == 200) {
            var responseType = Object.keys(thisEvent.response)[0];
            var newPageObj = JSON.parse(thisEvent.response[responseType]);
            if (responseType == "newPage") {
                var urlStr = newPageObj.url ? newPageObj.url : null;
                var titleStr = newPageObj.title ? newPageObj.title : null;
                var headStr = newPageObj.templateHead ? newPageObj.templateHead : null;
                var bodyStr = newPageObj.templateBody ? newPageObj.templateBody : null;
                var headReady = headStr ? false : true;
                if (headReady == false) {
                    var headReady = insertingNewHead(headStr, 1);
                }
                if (headReady == true) {
                    var bodyReady = insertingNewBody(bodyID, bodyStr);
                    if (bodyReady[0] == true) {
                        titleStr ? changeTitle(titleStr) : null;
                        urlStr ? changeUrl(urlStr) : null;
                        var body = bodyReady[1];
                        if (successHelper) {
                            successHelper(body);
                        }
                        setEventsOnLinks(body);
                    }
                }
            } else if (responseType == "redirectTo") {
                loadContent(bodyID, newPageObj ? newPageObj : window.location);
            } else if (responseType == "fullRedirect") {
                var currentLocation = newPageObj ? newPageObj : window.location;
                window.location = "";
                window.location = currentLocation;
            }
        }
    };
    request.send(fData);
};