var menu = null;
function copyMenu() {
    menu = document.getElementById("menu").cloneNode(true);
};
function reBuildMenu(item) {
    if (item.target) {
        var li = item.target;
        var li = searchP(li, "LI") ? searchP(li, "LI") : li;
        var li = searchP(li, "LI") ? searchP(li, "LI") : li;
    } else {
        var li = item;
    }
    li.className.indexOf(" active") == -1 ? li.className += " active" : 0;
    if (document.addEventListener) {
        li.addEventListener("mouseover", showActiveLists);
        li.addEventListener("mouseout", hideActiveLists);
    } else {
        li.attachEvent("mouseover", showActiveLists);
        li.attachEvent("mouseout", hideActiveLists);
    }
    var ul = li.lastElementChild;
    ul.className = "activePageMenu";
    ul.innerHTML = "";
    return ul;
};
function reInactiveMenu() {
    var lastLi = document.getElementById("menu").getElementsByClassName("active")[0];
    var liClass = lastLi.className;
    lastLi.className = liClass.slice(0, liClass.indexOf(" active"));
    if (window.removeEventListener) {
        lastLi.removeEventListener("mouseover", showActiveLists);
        lastLi.removeEventListener("mouseout", hideActiveLists);
    } else {
        lastLi.detachEvent("mouseover", showActiveLists);
        lastLi.detachEvent("mouseout", hideActiveLists);
    }
    var lastUL = lastLi.lastElementChild;
    lastUL.removeAttribute("class");
    lastUL.innerHTML = menu.getElementsByClassName(place)[0].lastElementChild.innerHTML;
};
function toggleMenu() {
    var menu = document.getElementById("menu");
    var menuUL = menu.firstElementChild.firstElementChild;
    var button = menu.lastElementChild;
    menuUL.className = menuUL.className == "mainMenu" ? "mainMenu show" : "mainMenu";
};
function searchP(target, type) {
    do {
        var target = target ? target.parentNode || null : null;
    } while (target && target.nodeName != type);
    return target
};
function searchCh(target, type) {
    var i = 0;
    do {
        var t = target ? target.childNodes[i++] || null : null;
    } while (t && t.nodeName != type)
    return t
};
function showActiveLists(event) {
    var to = event.target;
    var tar = to.nodeName == "LI" ? to : searchP(to, "LI");
    var childUl = tar ? searchCh(tar, "UL") : null;
    if (childUl && !childUl.className.match(/.*fixed|activePageMenu/i)) {
        childUl.setAttribute("class", "showActiveMenu");
    }
    event.stopPropagation();
};
function hideActiveLists(event) {
    var from = event.target;
    var tar = from.nodeName != "UL" ? from : searchP(from, "LI");
    var childUl = tar ? searchCh(tar, "UL"): null;
    if (childUl && !childUl.className.match(/.*fixed|activePageMenu/i)) {
        var to = event.relatedTarget;
        var rootLi = searchP(to, "LI");
        if (rootLi != tar && !to.nodeName.match(/SPAN|STRONG/i)) {
            childUl.removeAttribute("class");
        }
    }
    event.stopPropagation();
};
function showList(event) {
    var to = event.target;
    var to = to.nodeName != "LI" ? searchP(to, "LI") : to;
    var ul = searchCh(to, "UL");
    if (to && ul && !to.className.match(/.*active/i)) {
        ul.setAttribute("class", "show");
    }
    event.stopPropagation();
};
function hideList(event) {
    var from = event.target;
    var to = event.relatedTarget;
    var t1 = searchP(from, "LI");
    var t1 = searchP(t1, "LI") || t1;
    var t2 = searchP(to, "LI");
    var t2 = searchP(t2, "LI") || t2;
    if (t1 && t1 != t2 && !t1.className.match(/.*active/i)) {
        var ul = searchCh(t1, "UL");
        ul ? ul.removeAttribute("class") : 0;
    }
    event.stopPropagation();
};
function generateMenu(menu, place) {
    var docu = document;
    var li = docu.getElementById("menu").getElementsByClassName(place)[0];
    var ulElement0 = reBuildMenu(li);
    function doCreate(docu, ulElement, obj, curKey, prevent) {
        if (!prevent || prevent[0].className != curKey) {
            var li = docu.createElement("LI");
            obj.hasOwnProperty("blocked") ? li.className = "blocked" : 0;
            var span = docu.createElement("SPAN");
            if (obj.hasOwnProperty("url")) {
                var a = docu.createElement("A");
                a.setAttribute("href", obj["url"]);
                obj.hasOwnProperty('handler') ? a.className = obj["handler"] : 0;
                a.innerText = obj["textname"];
                span.appendChild(a);
            } else {
                span.innerText = obj["textname"];
            }
            li.appendChild(span)
            ulElement.appendChild(li);
            if (obj.hasOwnProperty('url') && docu.location.pathname == obj["url"]) {
                li.className += " current";
                ulElement.className = "fixed";
                var isActiveUl = true;
            }
            return [li, isActiveUl || false];
        } else {
            return prevent;
        }
    };
    function readKeys(keys) {
        var isObj = false;
        for (var i = 0; i < keys.length; i++) {
            var isObj = isObj == false ? !keys[i].match(/handler|url|textname|current|fixed|blocked/i) ? true : isObj : isObj;
        }
        return isObj
    };
    var keys0 = Object.keys(menu);
    var prevent = null;
    var searchArr = ["handler", "url", "textname", "current", "fixed", "blocked"];
    for (var i = 0; i < keys0.length; i++) {
        var k0 = keys0[i];
        var prevent = doCreate(docu, ulElement0, menu[k0], k0, prevent);
        if (searchArr.indexOf(k0) == -1) {
            var keys1 = Object.keys(menu[k0]);
            if (readKeys(keys1)) {
                var ul = docu.createElement("UL");
                var ulElement1 = prevent[0].appendChild(ul);
                prevent[1] == true ? ul.className = "fixed" : 0;
                for (var v = 0; v < keys1.length; v++) {
                    var k1 = keys1[v];
                    if (searchArr.indexOf(k1) == -1) {
                        var prevent = doCreate(docu, ulElement1, menu[k0][k1], k1, prevent);
                        prevent[1] == true ? ulElement0.className = "fixed" : 0;
                        var keys2 = Object.keys(menu[k0][k1]);
                        if (readKeys(keys2)) {
                            var ul = docu.createElement("UL");
                            var ulElement2 = prevent[0].appendChild(ul);
                            //prevent[1] == true ? ul.className = "fixed" : 0;
                            for (var e = 0; e < keys2.length; e++) {
                                var k2 = keys2[e];
                                if (searchArr.indexOf(k2) == -1) {
                                    var prevent = doCreate(docu, ulElement2, menu[k0][k1][k2], k2, prevent);
                                    if (prevent[1] == true) {
                                        ulElement0.className = "fixed";
                                        ulElement1.className = "fixed";
                                    }
                                    var keys3 = Object.keys(menu[k0][k1][k2]);
                                    if (readKeys(keys3)) {
                                        var ul = docu.createElement("UL");
                                        var ulElement3 = prevent[0].appendChild(ul);
                                        //prevent[1] == true ? ul.className = "fixed" : 0;
                                        for (var t = 0; t < keys3.length; t++) {
                                            var k3 = keys3[t];
                                            if (searchArr.indexOf(k3) == -1) {
                                                var prevent = doCreate(docu, ulElement3, menu[k0][k1][k2][k3], k3, prevent);
                                                if (prevent[1] == true) {
                                                    ulElement0.className = "fixed";
                                                    ulElement1.className = "fixed";
                                                    ulElement2.className = "fixed";
                                                }
                                                var keys4 = Object.keys(menu[k0][k1][k2][k3]);
                                                if (readKeys(keys4)) {
                                                    var ul = docu.createElement("UL");
                                                    var ulElement4 = prevent[0].appendChild(ul);
                                                    //prevent[1] == true ? ul.className = "fixed" : 0;
                                                    for (var u = 0; u < keys4.length; u++) {
                                                        var k4 = keys4[u];
                                                        if (searchArr.indexOf(k4) == -1) {
                                                            var prevent = doCreate(docu, ulElement4, menu[k0][k1][k2][k3][k4], k4, prevent);
                                                            if (prevent[1] == true) {
                                                                ulElement0.className = "fixed";
                                                                ulElement1.className = "fixed";
                                                                ulElement2.className = "fixed";
                                                                ulElement3.className = "fixed";
                                                            }
                                                            var keys5 = Object.keys(menu[k0][k1][k2][k3][k4]);
                                                            if (readKeys(keys5)) {
                                                                var ul = docu.createElement("UL");
                                                                var ulElement5 = prevent[0].appendChild(ul);
                                                                //prevent[1] == true ? ul.className = "fixed" : 0;
                                                                for (var w = 0; w < keys5.length; w++) {
                                                                    var k5 = keys5[w];
                                                                    if (searchArr.indexOf(k5) == -1) {
                                                                        var prevent = doCreate(docu, ulElement5, menu[k0][k1][k2][k3][k4][k5], k5, prevent);
                                                                        if (prevent[1] == true) {
                                                                            ulElement0.className = "fixed";
                                                                            ulElement1.className = "fixed";
                                                                            ulElement2.className = "fixed";
                                                                            ulElement3.className = "fixed";
                                                                            ulElement4.className = "fixed";
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
};
function menuClickHandler(event) {
    var target = event.target;
    if (target.nodeName == "A") {
        target.className != "global" ? localNavigation(event, target.href) : globalNavigation(event, target.href);
    } else {
        target.className == "hideButton" ? toggleMenu() : 0;
    }
};
if (window.addEventListener) {
    document.getElementById("menu").addEventListener('click', menuClickHandler, false);
} else {
    document.getElementById("menu").attachEvent('click', menuClickHandler, false);
}