var canvasCenterSize = 20; // Размер центров в %
var defaultSpeed = 3;
var size = [screen.width / 100 * 55, screen.height / 100 * 60];
//var windowPadding = 51; // Прозрачная область (Определил автоматически в теле)
var border_2_5 = 1; // Ширина рамок у правого и левого контент-блока
var brd_2_5Style = "solid";
//var innerPadding = 40; // Отступ левой и правой колонки от центра (Определил автоматически в теле)
var innerPadding = innerPadding + border_2_5;
var charName = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var levelsFuncs0 = [];
var levelsFuncs1 = [];
var curCir = 0;
var switchThemesObj = {
    themeSettings: {
        "options": ["dark", "white"],
        "names": ["green", "azure", "orange", "red", "black"],
        "special": ["violet", "emerald"]
    },
    launchKeys: {
        "options": ["d", "w"],
        "names": ["g", "a", "o", "r", "b"],
        "special": ["v", "e"],
        "gn3": ["k", "j"]
    },
    canvasFill: {
        "options": ["#CCC", "#FFF"],
        "names": ["#598c00", "#027cb0", "#90511d", "#a11f1f", "#6B6B6B"],
        "special": ["#b19cd9", "#50c878"],
        "gn3": ["#fff", "#444"]
    },
    canvasSize: {
        "options": 289,
        "names": 265,
        "special": 97,
        "gn3": 7
    },
    speed: {
        "options": 1,
        "names": 3,
        "special": 11,
        "gn3": 3
    },
    unSettings: {
        "gn3": ["titan", "blue"]
    }
};
/* Скрытие неактивных */
function menuHelper(currElement) {
    if (!$(currElement).hasClass("active")) {
        $("#switchThemesMain .footer ul.menu").children("li").removeClass("active");
        $("#switchThemesMain .header ul.headtypes").children("li").css('display', 'none');
        $("#switchThemesMain").children("table").css('display', 'none');
        $("#switchThemesMain").width(size[0]).height(size[1]).center();
        $("#switchThemesMain .footer").children("ul").centerParent();
        var heFooter = calculating();
        /* Уровни */
        levelsFunction = function() {
            $("#switchThemesMain .footer ul.menu li.levels").addClass("active");
            $("#switchThemesMain .header ul.headtypes li.levels").css('display', 'block');
            $("#switchThemesMain table.levels").css('display', 'block');
            levelsSettings(heFooter);
        };
        /* Темы */
        themesFunction = function() {
            $("#switchThemesMain .footer ul.menu").children("li.themes").addClass("active");
            $("#switchThemesMain .header ul.headtypes li.themes").css('display', 'block');
            $("#switchThemesMain table.themes").css('display', 'block');
            themesSettings(heFooter);
        };
        /* Элементы */
        elementsFunction = function() {
            $("#switchThemesMain .footer ul.menu").children("li.elements").addClass("active");
            $("#switchThemesMain .header ul.headtypes li.elements").css('display', 'block');
            $("#switchThemesMain table.elements").css('display', 'block');
            elementsSettings(heFooter);
            eval(levelsFuncs1[curCir]);
        };
        /* Содержимое */
        contentFunction = function() {
            $("#switchThemesMain .footer ul.menu").children("li.content").addClass("active");
            $("#switchThemesMain .header ul.headtypes li.content").css('display', 'block');
            $("#switchThemesMain table.content").css('display', 'block');
            eval(levelsFuncs0[curCir]);
        };
        /* Общие настройки */
        commonFunction = function() {
            $("#switchThemesMain .footer ul.menu").children("li.common").addClass("active");
            $("#switchThemesMain .header ul.headtypes li.generalBlock").css('display', 'block');
            $("#switchThemesMain table.common").css('display', 'block');
            eval(levelsFuncs0[curCir]);
        };
    };
};
function levelsSettings(heFooter) {
    $("#switchThemesMain table.levels tr:nth-child(1), #switchThemesMain table.levels tr:nth-child(2), " +
        "#switchThemesMain table.levels tr:nth-child(3)").css({
            'width': size[0] / 2,
                });
    $("#switchThemesMain table.levels tr:nth-child(1)").css({
        'height': size[1] - heFooter
    });
    $("#switchThemesMain table.levels tr:nth-child(2), #switchThemesMain table.levels tr:nth-child(3)").css({
        'height': (size[1] - heFooter) / 2
    });
    $("#switchThemesMain table.levels tr td").css({
        'margin-left': (size[0] / 2) / 100 * 2.5,
        'margin-top': (size[1] - heFooter) / 100 * 2.5,
        'padding': (size[1] - heFooter) / 100 * 5
    });
    $("#switchThemesMain table.levels tr td small").css({
        'font-size': (size[0] / 2) / 100 * 3,
    });
};
function themesSettings(heFooter) {
    $("#switchThemesMain table.themes tr:nth-child(1), #switchThemesMain table.themes tr:nth-child(2), " +
        "#switchThemesMain table.themes tr:nth-child(3)").css({
            'width': size[0] / 2,
        });
    $("#switchThemesMain table.themes tr:nth-child(1)").css({
        'height': size[1] - heFooter
    });
    $("#switchThemesMain table.themes tr:nth-child(2), #switchThemesMain table.themes tr:nth-child(3)").css({
        'height': (size[1] - heFooter) / 2
    });
    $("#switchThemesMain table.themes tr td").css({
        'margin-left': (size[0] / 2) / 100 * 5,
        'margin-top': (size[1] - heFooter) / 100 * 3.3,
        'padding-left': (size[0] / 2) / 100 * 2.5,
        'padding-right': (size[0] / 2) / 100 * 2.5,
        'padding-top': (size[1] - heFooter) / 100 * 1,
        'padding-bottom': (size[1] - heFooter) / 100 * 1
    });
    $("#switchThemesMain table.themes tr td small").css({
        'font-size': (size[0] / 2) / 100 * 3,
    });
};
function elementsSettings(heFooter) {
    $("#switchThemesMain table.elements tr").css({
        'width': size[0],
        'height': (size[1] - heFooter) / 2
    });
    var x = size[0] / 100 * 90;
    var y = ((size[1] - heFooter) / 2) / 100 * 90;
    var mar = (((size[1] - heFooter) / 2) - y) / 2;
    $("#switchThemesMain table.elements tr td").css({
        'width': x,
        'height': y,
        'margin': mar + ' auto ' + mar + ' auto'
    });

};
function contentSettings(canvasSize, windowPadding, innerPadding, canvasSize1, yWraps, yError, heFooter) {
    $("#switchThemesMain table.content tr:nth-child(1), #switchThemesMain table.content tr:nth-child(4)").css({
        'width': size[0] / 3,
        'height': size[1] - heFooter
    });
    $("#switchThemesMain table.content tr:nth-child(2), #switchThemesMain table.content tr:nth-child(5)").css({
        'height': size[1] - heFooter // Отступы по бокам
    });
    $("#switchThemesMain table.content tr:nth-child(3)").css({
        // Круг
    });
};
function commonSettings(canvasSize, windowPadding, innerPadding, canvasSize1, yWraps, yError, heFooter) {
    $("#switchThemesMain table.common tr:nth-child(1), #switchThemesMain table.common tr:nth-child(2), " +
        "#switchThemesMain table.common tr:nth-child(4), #switchThemesMain table.common tr:nth-child(5)").css({
            'height': size[1] - heFooter
        });
    $("#switchThemesMain table.common tr:nth-child(1), #switchThemesMain table.common tr:nth-child(4)").css({
        'width': (size[0] - (innerPadding * 2) - canvasSize1) / 2
    });
    $("#switchThemesMain table.common tr:nth-child(2), #switchThemesMain table.common tr:nth-child(5)").css({
        'width': innerPadding
    });
    $("#switchThemesMain table.common tr:nth-child(2)").css({
        'border-left-width': border_2_5,
        'border-left-style': brd_2_5Style
    });
    $("#switchThemesMain table.common tr:nth-child(5)").css({
        'border-right-width': border_2_5,
        'border-right-style': brd_2_5Style
    });
    $("#switchThemesMain table.common tr:nth-child(3)").css({
        'width': canvasSize1,
        'height': yWraps - yError - (canvasSize.isEven() ? 1 : 0)
    });
    $("#switchThemesMain table.common tr:nth-child(7)").css({
        'width': canvasSize1,
        'height': yWraps + yError + (canvasSize.isEven() ? 1 : 0)
    });
    $("#switchThemesMain table.common tr:nth-child(6)").css({
        'width': canvasSize1,
        'height': canvasSize1
    });
    $("#themesMenuWrap").width(canvasSize1).height(canvasSize1).css({
        'display': 'block',
        'border-width': 0,
        'margin': 0
    }).center().css({
        'border-width': Math.round(canvasSize1 / 100 * 22),
        'border-style': 'solid',
        'margin': Math.round(-canvasSize1 / 100 * 22),
    });
};
function calculating() {
    var header = $("#switchThemesMain .header").outerHeight() > $(
            "#switchThemesMain .header").innerHeight() ? $("#switchThemesMain .header").outerHeight() : $("#switchThemesMain .header").innerHeight();
    var footer = $("#switchThemesMain .footer").outerHeight() > $(
        "#switchThemesMain .footer").innerHeight() ? $("#switchThemesMain .footer").outerHeight() : $("#switchThemesMain .footer").innerHeight();
    var heFooter = header + footer;
    return heFooter;
};
function oneBoxShow(chN) {
    curCir = charName.indexOf(chN);
    $("#switchThemesMain .header ul.headtypes li.elements ul li." + chN).addClass("active");
    $("#switchThemesMain table.elements .zonesPlace #boxZoneElement." + chN).addClass("active");
};
function otherBoxHide(chN) {
    for (var i = 0; i < chN.length; i++) {
        $("#switchThemesMain .header ul.headtypes li.elements ul li." + chN[i]).removeClass("active");
        $("#switchThemesMain table.elements .zonesPlace #boxZoneElement." + chN[i]).removeClass("active");

    }
};
function oneCircleShow(launchKeys) {
    var canvasSize = switchThemesObj.canvasSize;
    for (var i = 0; i < launchKeys.length; i++) {
        if (i < launchKeys.length - 1) {
            $("#ray" + launchKeys[i]).css('display', 'block').center();
            $("#key" + launchKeys[i]).css('display', 'block').center();
        } else {
            curCir = charName.indexOf(launchKeys[i]);
            $("#switchThemesMain .header ul.headtypes li.elements ul li." + launchKeys[i] +
                ", #switchThemesMain .header ul.headtypes li.content ul li." + launchKeys[i] +
                ", #switchThemesMain .header ul.headtypes li.generalBlock ul li." + launchKeys[i]).addClass("active");
            $("#circle" + launchKeys[i]).css('display', 'block').center();
            $("#center" + launchKeys[i]).css('display', 'block').center();
            var cS = Object.keys(canvasSize)[curCir];
            var windowPadding = launchKeys[i] === charName.charAt(0) ? 0 : canvasSize[Object.keys(canvasSize)[curCir - 1]] - canvasSize[cS];
            var innerPadding = (canvasSize[cS] + windowPadding) / 100 * 5;
            var heFooter = calculating();
            var canvasSize1 = canvasSize[cS] + windowPadding;
            var yWraps = (size[1] - heFooter - canvasSize1) / 2;
            var yError = canvasSize1 / 100 * $(window).height() / canvasSize1;
            contentSettings(canvasSize[cS], windowPadding, innerPadding, canvasSize1, yWraps, yError, heFooter);
            commonSettings(canvasSize[cS], windowPadding, innerPadding, canvasSize1, yWraps, yError, heFooter);
        }
    }
};
function otherCircleHide(launchKeys) {
    for (var i = 0; i < launchKeys.length; i++) {
        if (i <= launchKeys.length - Object.keys(switchThemesObj.themeSettings).length) {
                $("#ray" + launchKeys[i]).css('display', 'none');
                $("#key" + launchKeys[i]).css('display', 'none');
        } else {
            $("#switchThemesMain .header ul.headtypes li.elements ul li." + launchKeys[i] +
                ", #switchThemesMain .header ul.headtypes li.content ul li." + launchKeys[i] +
                ", #switchThemesMain .header ul.headtypes li.generalBlock ul li." + launchKeys[i]).removeClass("active");
            $("#circle" + launchKeys[i]).css('display', 'none');
            $("#center" + launchKeys[i]).css('display', 'none');
        }
    }
};
function addNew(current) {

};
/* Пересоздание содержимого объекта themesSettings  */
function resortFunc(curElement) {
    switchThemesObj = {
        themeSettings: {},
        unSettings: {},
        launchKeys: {},
        canvasSize: {},
        canvasFill: {},
        speed: {}
    };
    var isAssignOn = 0;
    var zones = $("#switchThemesMain table." + curElement + " .active").children("td");
    for (var i = 0; i < zones.length; i++) {
        var key = zones[i].id;
        key.search("_") === -1 ? key === "unassignedThemes" ? isAssignOn += 1 : 0 : key.slice(0, key.indexOf("_")) === "unassignedThemes" ? isAssignOn += 1 : 0;
        var themes = $(zones[i]).children("td");
        var value = [];
        var cF = [];
        var lK = [];
        for (var s = 0; s < themes.length; s++) {
            value[s] = themes[s].id;
            cF[s] = $(themes[s]).attr("cnvBckgr");
            lK[s] = $(themes[s]).attr("key");
        }
        switchThemesObj.canvasSize[key] = $("#" + key).attr("D");
        switchThemesObj.speed[key] = $("#" + key).attr("spd");
        switchThemesObj.canvasFill[key] = cF;
        switchThemesObj.launchKeys[key] = lK;
        switchThemesObj.themeSettings[key] = value;
    }
    var zones = $("#switchThemesMain table." + curElement + " .unactive").children("td");
    var unValue = [];
    var uCF = [];
    var uLK = [];
    var x = -1;
    for (var i = 0; i < zones.length; i++) {
        var key = zones[i].id;
        key.search("_") === -1 ? key === "unassignedThemes" ? isAssignOn += 1 : 0 : key.slice(0, key.indexOf("_")) === "unassignedThemes" ? isAssignOn += 1 : 0
        var value = [];
        var cF = [];
        var lK = [];
        var themes = $(zones[i]).children("td");
        var isAssign = !document.getElementById(key).hasAttribute("D") && themes.length === 0;
        if (isAssign === true) {
            x++;
            unValue[x] = key;
            var key = "unassignedThemes" + (isAssignOn > 0 ? ("_" + isAssignOn) : "");
            uCF[x] = $(zones[i]).attr("cnvBckgr");
            uLK[x] = $(zones[i]).attr("key");
        } else {
            for (var s = 0; s < themes.length; s++) {
                value[s] = themes[s].id;
                cF[s] = $(themes[s]).attr("cnvBckgr");
                lK[s] = $(themes[s]).attr("key");
            }
        }
        if (isAssign === true) {
            switchThemesObj.unSettings[key] = unValue;
            switchThemesObj.canvasFill[key] = uCF;
            switchThemesObj.launchKeys[key] = uLK;
        } else {
            switchThemesObj.unSettings[key] = value;
            switchThemesObj.canvasFill[key] = cF;
            switchThemesObj.launchKeys[key] = lK;
            switchThemesObj.canvasSize[key] = $("#" + key).attr("D");
            switchThemesObj.speed[key] = $("#" + key).attr("spd");
        }
    }
    defSettings(curElement, switchThemesObj);
};
function defSettings(curElement, switchThemesObj) {
    var canvasSize = switchThemesObj.canvasSize;
    var themeSettings = switchThemesObj.themeSettings;
    var speed = switchThemesObj.speed;
    var canvasFill = switchThemesObj.canvasFill;
    var launchKeys = switchThemesObj.launchKeys;
    if (curElement === "levels") {
        var keys = Object.keys(themeSettings); // Берём только из включённых
        var heFooter = calculating();
        var heFooter = Math.floor(size[1] - heFooter);
        var heFooter = heFooter - (size[1] / 100 * 4); // Это максимально допустимое значение
        var end = keys.length;
        var cS = [];
        for (var i = 0; i < end; i++) {
            if (canvasSize[keys[i]] === "undefined") {
                i > 0 & i < end ? switchThemesObj.canvasSize[keys[i]] = (canvasSize[keys[i - 1]] - canvasSize[keys[i + 1]]) / 2 : 0;
                i === 0 ? switchThemesObj.canvasSize[keys[i]] = keys.length > 0 ? Math.round(heFooter - (canvasSize[keys[i + 1]] / 2)) : heFooter : 0;
                i === end - 1 ? switchThemesObj.canvasSize[keys[i]] = (canvasSize[keys[i - 1]] - (canvasSize[keys[i - 1]] / 100 * canvasCenterSize)) / 2 : 0;
            }
            if (speed[keys[i]] === "undefined") {
                switchThemesObj.speed[keys[i]] = defaultSpeed;
            }
            if (canvasFill[keys[i]] === "undefined") {
                switchThemesObj.canvasFill[keys[i]] = "zebra";
            }
            if (launchKeys[keys[i]] === "undefined") {
                switchThemesObj.launchKeys[keys[i]] = "";
            }
            cS[i] = parseFloat(canvasSize[keys[i]]);
        }
        function bckSort(a, b) {
            return b - a;
        };
        var cS = cS.sort(bckSort);
        for (var i = 0; i < end; i++) {
            switchThemesObj.canvasSize[keys[i]] = cS[i];
        }
    }
    delHTML(curElement, switchThemesObj);
    toJsonForming(switchThemesObj);
};
function toJsonForming(switchThemesObj) {
    var switchThemesObj = JSON.stringify(switchThemesObj);

};
function delHTML(curElement, switchThemesObj) {
    var themeSettings = switchThemesObj.themeSettings;
    var unSettings = switchThemesObj.unSettings;
    var launchKeys = switchThemesObj.launchKeys;
    var canvasSize = switchThemesObj.canvasSize;
    var canvasFill = switchThemesObj.canvasFill;
    var speed = switchThemesObj.speed;
    if (curElement === "levels") {
        $("#switchThemesMain table.themes .active, #switchThemesMain table.themes .unactive, " +
            "#switchThemesMain .header ul.headtypes li.content ul, " +
            "#switchThemesMain .header ul.headtypes li.generalBlock ul, #switchThemesMain table.elements tr:nth-child(1), " +
            "#switchThemesMain .header ul.headtypes li.elements ul").children().remove();
        genThemesHTML(themeSettings, unSettings, launchKeys, canvasSize, canvasFill, speed);
        genKeysHTML(themeSettings, charName);
        onStartForming(themeSettings, unSettings);
    } else {
        $("#switchThemesMain table.levels .active, #switchThemesMain table.levels .unactive").children().remove();
        genLevelsHTML(themeSettings, unSettings, launchKeys, canvasSize, canvasFill, speed);
    }
};
function genLevelsHTML(themeSettings, unSettings, launchKeys, canvasSize, canvasFill, speed) {
    var keys = Object.keys(themeSettings);
    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        var name = key.replace(key.charAt(0), key.charAt(0).toUpperCase()).replace("_", " ");
        $("#switchThemesMain table.levels .active").append("<td id = " + key + " D = " +
            canvasSize[key] + " spd = " + speed[key] + "><small>" +
            name +
            "</small>" + (themeSettings[key].length > 0 ? "<small>Содержит тем: </small><span>" +
            themeSettings[key].length + "</span>" : "<small>Пока не содержит тем</small>") + "</td>");
        for (var u = 0; u < themeSettings[key].length; u++) {
            $("#switchThemesMain table.levels .active #" + key).append("<td id = " + themeSettings[key][u] +
                " cnvBckgr = " + canvasFill[key][u] + " key = " + launchKeys[key][u] + ">");
        }
    }
    var keys = Object.keys(unSettings);
    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        var name = key.replace(key.charAt(0), key.charAt(0).toUpperCase()).replace("_", " ");
            $("#switchThemesMain table.levels .unactive").append("<td id = " + key + " D = " +
                canvasSize[key] + " spd = " + speed[key] + "><small>" + name +
                "</small></td>");
            for (var u = 0; u < unSettings[key].length; u++) {
                $("#switchThemesMain table.levels .unactive #" + key).append("<td id = " + unSettings[key][u] +
                " cnvBckgr = " + canvasFill[key][u] + " key = " + launchKeys[key][u] + ">");
        }
    }
};
function genThemesHTML(themeSettings, unSettings, launchKeys, canvasSize, canvasFill, speed) {
    var keys = Object.keys(themeSettings);
    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        var name = key.replace(key.charAt(0), key.charAt(0).toUpperCase()).replace("_", " ");
        $("#switchThemesMain table.themes .active").append("<td id = " + key + " D = " +
            canvasSize[key] + " spd = " + speed[key] + "><small>" +
            name +
            "</small></td>");
        for (var u = 0; u < themeSettings[key].length; u++) {
            var name = themeSettings[key][u].replace(themeSettings[key][u].charAt(0),
                themeSettings[key][u].charAt(0).toUpperCase()).replace("_", " ");
            $("#switchThemesMain table.themes .active #" + key).append("<td id = " +
                themeSettings[key][u] + " cnvBckgr = " + canvasFill[key][u] + " key = " +
                launchKeys[key][u] + "><small>" + name + "</small><span>[" +
                launchKeys[key][u].toUpperCase() + "]</span></td>");
        }
    }
    var keys = Object.keys(unSettings);
    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        var name = key.replace(key.charAt(0), key.charAt(0).toUpperCase()).replace("_", " ");
        $("#switchThemesMain table.themes .unactive").append("<td id = " + key + " D = " +
            canvasSize[key] + " spd = " + speed[key] + "><small>" + name +"</small></td>");
        for (var u = 0; u < unSettings[key].length; u++) {
            var name = unSettings[key][u].replace(unSettings[key][u].charAt(0),
            unSettings[key][u].charAt(0).toUpperCase()).replace("_", " ");
            $("#switchThemesMain table.themes .unactive #" + key).append("<td id = " +
            unSettings[key][u] + " cnvBckgr = " + canvasFill[key][u] + " key = "
            + launchKeys[key][u] + "><small>" + name + "</small></td>");
        }
    }
};
function genKeysHTML(themeSettings, charName) {
    var keys = Object.keys(themeSettings);
    var arr = [];
    var a = "";
    var x = ""
    for (var i = 0; i < keys.length; i++) {
        arr[i] = "switchThemesObj.launchKeys['" + keys[i] + "']";
        i > 0 & i < keys.length - 1 ? x += ")" : 0;
    }
    for (var i = 0; i < keys.length; i++) {
        var a = arr.slice(0, i).join(".concat(") + (i > 0 & i < keys.length - 1 ? ".concat(" : "") + arr.slice(i + 1, arr.length).join(".concat(");
        /* Генерирование кнопок редактирования кругов */
        levelsFuncs0[i] = "oneCircleShow(switchThemesObj.launchKeys['" + keys[i] +
            "'].concat('" + charName.charAt(i) + "')); otherCircleHide(" +
            a + x + ".concat(charName.slice(0, " +
            i + ").split('')).concat(charName.slice(" + (i + 1) + ", " + keys.length + ").split('')));";
        $("#switchThemesMain .header ul.headtypes li.content ul, " +
            "#switchThemesMain .header ul.headtypes li.generalBlock ul").append(
            "<li class = " + charName.charAt(i) + " onclick = \"" + levelsFuncs0[i] + "\"><h3><a>" +
            keys[i].toUpperCase() +
            "</a></h3></li>");
        /* Генерирование квадратов (Уровни .elements) */
        $("#switchThemesMain table.elements tr:nth-child(1)").append("<td id = 'boxZoneElement' class = " + charName.charAt(i) +
            ">");
        levelsFuncs1[i] = "oneBoxShow(chN = charName.charAt(" + i + ")); otherBoxHide(chN = charName.slice(0, " + i +
            ").concat(charName.slice(" + (i + 1) + ", " + keys.length + ")).split(''));";
        $("#switchThemesMain .header ul.headtypes li.elements ul").append("<li class = " + charName.charAt(i) +
            " onclick = \"" + levelsFuncs1[i] + "\"><h3><a>" +
            keys[i].toUpperCase() +
            "</a></h3></li>");
    }
};
function onStartForming(themeSettings, unSettings) {
    var forUiel = "";
    var keys = Object.keys(themeSettings);
    for (i = 0; i < keys.length; i++) {
        forUiel += "#" + keys[i] + ".ui-sortable" + ", ";
    }
    var keys = Object.keys(unSettings);
    for (var i = 0; i < keys.length; i++) {
        forUiel += "#" + keys[i] + ".ui-sortable" + ", ";
    }
    forUiel += "#switchThemesMain table.levels .active.ui-sortable, " +
        "#switchThemesMain table.levels .unactive.ui-sortable, #switchThemesMain table.themes .unactive.ui-sortable";
    $("#switchThemesMain table.levels .active, #switchThemesMain table.themes .active td, " +
        "#switchThemesMain table.levels .unactive, #switchThemesMain table.themes .unactive").sortable({
            items: "td",
            connectWith: forUiel,
            revert: true,
            placeholder: ".testclass",
            stop: function () {
                var curElement = $("#switchThemesMain .footer ul.menu li.levels").hasClass("active") ? "levels" : "themes";
                resortFunc(curElement);
            }
        });
};
var switchWork = false;
var optionsWork = false;
var switchHoldKey = false;
var optionsHoldKey = false;
function switchReverse() {
    switchWork = !switchWork;
    switchShowHide();
};
function optionsReverse() {
    optionsWork = !optionsWork;
    optionsShowHide();
};
function switchShowHide() {
    if (optionsHoldKey === true) {
        if (switchWork === true) {
            optionHide();
            switchShow();
        } else {
            switchHide();
            optionShow();
        }
    } else {
        if (optionsWork === true) {
            if (switchWork === true) {
                optionHide();
                switchShow();
            } else {
                switchHide();
                optionShow();
            }
            typeOfPressOptions !== "keyup" ? optionsWork = false : 0;
        } else {
            if (switchWork === true) {
                switchShow();
            } else {
                switchHide();
            }
        }
    }
};
function optionsShowHide() {
    if (switchHoldKey === true) {
        if (optionsWork === true) {
            switchHide();
            optionShow();
        } else {
            optionHide();
            switchShow();
        }
    } else {
        if (switchWork === true) {
            if (optionsWork === true) {
                switchHide();
                optionShow();
            } else {
                optionHide();
                switchShow();
            }
            typeOfPressThemes !== "keyup" ? switchWork = false : 0;
        } else {
            if (optionsWork === true) {
                optionShow();
            } else {
                optionHide();
            }
        }
    }
};
function switchShow() {
    var themeSettings = switchThemesObj.themeSettings;
    var keys = Object.keys(themeSettings);
    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        for (var o = 0; o < themeSettings[key].length; o++) {
            $("#ray" + themeSettings[key][o].charAt(0)).css('display', 'block').center();
            $("#key" + themeSettings[key][o].charAt(0)).css('display', 'block').center();
        }
        $("#circle" + charName.charAt(i)).css('display', 'block').center();
        $("#center" + charName.charAt(i)).css('display', 'block').center();
    }
    $("#aPage #topP .mMenu ul li.switchthemesThemesButton").addClass("active");
};
function switchHide() {
    $("#themesMenu > canvas").css('display', 'none');
    $("#aPage #topP .mMenu ul li.switchthemesThemesButton").removeClass("active");
};
function optionShow() {
    $("#switchThemesMain").css('display', 'block');
    $("#aPage #topP .mMenu ul li.switchthemesConfButton").addClass("active");
    $($("#switchThemesMain .footer ul.menu").children("li")[0]).click().click();
};
function optionHide() {
    $("#switchThemesMain, #themesMenuWrap, #themesMenu > canvas").css('display', 'none');
    $("#aPage #topP .mMenu ul li.switchthemesConfButton").removeClass("active");
};