function doPostFormSimplifield(d, path, form, type, hasSock, event) {
    if (form.nodeName == "FORM") {
        var data = new FormData();
        var formItems = form.getElementsByTagName("input");
        var formItems1 = form.getElementsByTagName("textarea");
        var formItemsLength = formItems.length + formItems1.length - 1;
        for (var i = 0; i < formItemsLength; i++) {
            var item = formItems1[i] || formItems[i];
            var itemName = item.name;
            var itemVal = item.value;
            if (itemVal == "") {
                if (itemName == "csrfmiddlewaretoken") {
                    data.append(itemName, window.csrf);
                } else {
                    return;
                }
            } else {
                data.append(itemName, itemVal);
            }
        }
    } else {
        var data = null;
    }
    var response = new XMLHttpRequest();
    response.open("POST", path, true);
    response.responseType = "text";
    response.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    response.setRequestHeader('X-CSRFToken', csrf);
    response.timeout = 7000;
    response.onloadend = function (event) {
        if (hasSock == false) {
            var thisEvent = this || event.target;
            if (thisEvent.status == 200) {
                if (type == 'rmNotifyItem') {
                    // Эта затея ради синхронного поведения оповещений и счётчика на нескольких вкладках
                    var ul = form.parentNode;
                    var base = ul.previousElementSibling.previousElementSibling;
                    var baseCounter = base.firstElementChild;
                    var baseText = baseCounter.innerText;
                    var mainBase = baseText.slice(0, baseText.indexOf("."));
                    var currentNotifyItem = form.getElementsByClassName('countreble');
                    if (currentNotifyItem.length > 0) {
                        var currentNotifyItem = currentNotifyItem[0];
                        var currentNotifyItemValue = currentNotifyItem.innerText;
                        var currentNotifyItemValue = currentNotifyItemValue.match(/[0-9]+/i)[0];
                        var currentNotifyBaseValue = parseInt(mainBase) - parseInt(currentNotifyItemValue);
                    } else {
                        var currentNotifyBaseValue = 1;
                    }
                    mainBaseAnimation(base, baseCounter, true, currentNotifyBaseValue, false);
                    multiBaseAnimation(baseCounter, true, 1, false);
                    if (ul.childElementCount > 1) {
                        var mbStab = form.previousElementSibling || form.nextElementSibling || null;
                        mbStab && mbStab.className == "stab" ? ul.removeChild(mbStab) : 0
                        ul.removeChild(form);
                    } else {
                        var notify = ul.parentNode;
                        notify.parentNode.removeChild(notify);
                    }
                    return;
                } else if (type == "rmNotify") {
                    form.parentNode.removeChild(form);
                    return;
                }
                localNavigation(null, d.location.pathname);
            }
        }
    };
    response.ontimeout = function (event) {

    };
    response.onerror = function (event) {

    };
    response.send(data);
    event ? event.preventDefault() : 0;
};