function loadContent(bodyID, refUrl, event) {
    var d = document;
    if (location.href != refUrl) {
        var request = new XMLHttpRequest();
        request.open("GET", refUrl, true);
        request.responseType = "json";
        request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        if (document.addEventListener) {
            request.addEventListener('load', successLoad, false);
        } else {
            request.attachEvent('load', successLoad, false);
        }
        request.send();
        function successLoad(event) {
            var thisEvent = this || event.target;
            if (thisEvent.status == 200) {
                var w = window;
                var responseType = Object.keys(thisEvent.response)[0];
                var newPageObj = JSON.parse(thisEvent.response[responseType]);
                if (bodyID == "global") {
                    if (responseType == "newPage") {
                        var doc = d;
                        var headReady = false;
                        var scriptsReady = false;
                        var newPage = doc.implementation.createHTMLDocument();
                        newPage.documentElement.innerHTML = newPageObj.newTemplate;
                        var newBody = newPage.body;
                        var newScripts = newBody.lastElementChild.cloneNode(true);
                        newBody.removeChild(newBody.lastElementChild);
                        var headReady = insertingNewHead(newPage.head, 0);
                        if (headReady) {
                            var newContent = newBody.children || Array.prototype.slice.call(newBody.childNodes);
                            var page = doc.getElementById("aPage");
                            var menu = doc.getElementById('menu').cloneNode(true);
                            var r = /\S/;
                            for (var i = 0; i < newContent.length; i++) {
                                var newBlock = newContent[i];
                                var newBlockInner = newBlock.innerHTML;
                                var newBlockId = newBlock.id;
                                var element = doc.getElementById(newBlockId) || null;
                                if (element) {
                                    if (newBlockInner.search(r) != -1) {
                                        if (newBlockId == "topP") {
                                            element.innerHTML = menu.outerHTML + newBlockInner;
                                            menuEvent(doc);
                                        } else {
                                            element.innerHTML = newBlockInner;
                                        }
                                    } else {
                                        if (newBlockId != "topP") {
                                            element.parentNode.removeChild(element);
                                        } else {
                                            element.innerHTML = "" + menu.outerHTML;
                                            menuEvent(doc);
                                        }
                                    }
                                } else {
                                    page.appendChild(newBlock);
                                }
                            }
                            var scriptsReady = insertingNewStatics(newScripts, 0);
                            if (scriptsReady == true) {
                                var urlStr = newPageObj.url ? newPageObj.url : null;
                                var titleStr = newPageObj.titl ? newPageObj.titl : null;
                                changePageAttributes(urlStr, titleStr);
                                if (w.userSock === null) {
                                    var events = newPageObj.defaultListenerData || null;
                                    events ? defaultListener(events) : 0;
                                }
                                if (w.loadReadyCallBack) {
                                    var hidedElement = doc.getElementsByClassName("in-progress")[0] || null;
                                    hidedElement ? loadReadyCallBack(hidedElement) : 0;
                                }
                            }
                        }
                    } else if (responseType == "redirectTo") {
                        loadContent("global", newPageObj ? newPageObj : location);
                    }
                } else {
                    if (responseType == "newPage") {
                        var headStr = newPageObj.templateHead ? newPageObj.templateHead : null;
                        if (headStr) {
                            var headReady = insertingNewHead(headStr, 1);
                        } else {
                            var headReady = true;
                        }
                        if (headReady == true) {
                            var bodyReady = [false];
                            var bodyStr = newPageObj.templateBody ? newPageObj.templateBody : null;
                            if (bodyStr) {
                                var bodyReady = insertingNewBody(bodyID, bodyStr);
                            } else {
                                var bodyReady = [true];
                            }
                            if (bodyReady[0] == true) {
                                var pageAttrs = false;
                                var extended = false;
                                var urlStr = newPageObj.url ? newPageObj.url : null;
                                var titleStr = newPageObj.titl ? newPageObj.titl : null;
                                var extended = w.responseExtendedProperties ? responseExtendedProperties(newPageObj) : true;
                                var pageAttrs = changePageAttributes(urlStr, titleStr);
                                if (pageAttrs == true && extended == true) {
                                    var scriptsStr = newPageObj.templateScripts ? newPageObj.templateScripts : null;
                                    if (scriptsStr) {
                                        var scriptsReady = insertingNewStatics(scriptsStr, 1);
                                    } else {
                                        var scriptsReady = true;
                                    }
                                    if (scriptsReady == true) {
                                        generateMenu(newPageObj.menu, place);
                                        var body = bodyReady[1];
                                        if (w.userSock === null) {
                                            var events = newPageObj.defaultListenerData || null;
                                            events ? defaultListener(events) : 0;
                                        }
                                        w.loadReadyCallBack ? loadReadyCallBack(body) : 0;
                                    }
                                }
                            }
                        }
                    } else if (responseType == "redirectTo") {
                        loadContent(bodyID, newPageObj ? newPageObj : location);
                    } else if ("requiredLoginRedirect") {
                        seccessLoginPath
                    } else if (responseType == "requiredLoginLightbox") {

                    }
                }
            }
            function menuEvent(doc) {
                var menu = doc.getElementById('menu');
                if (doc.addEventListener) {
                    menu.addEventListener('click', menuClickHandler, false);
                } else {
                    menu.attachEvent('click', menuClickHandler, false);
                }
            };
        };
    }
    event ? event.preventDefault() : 0;
};
function changePageAttributes(url, title) {
    var urlReady = url ? changeUrl(url) : true;
    var titleReady = title ? changeTitle(title) : true;
    if (titleReady == true && urlReady == true) {
        return true
    }
};
function changeUrl(urlStr) {
    /* urlStr - Prinimaetsa ne polniy adres, tol'ko location.path */
    try {
        history.pushState(history.state, urlStr, urlStr);
        history.replaceState(history.state, urlStr, urlStr);
    } catch (ev) {
        location.href = "" + urlStr;
        location.hash = "" + urlStr;
    }
    return true
};
function changeTitle(titleStr) {
    var currentTittle = document.title;
    var newTitle = currentTittle.slice(0, currentTittle.lastIndexOf(":") + 2) + titleStr;
    document.title = newTitle;
    return true
};
function insertingNewStatics(staticStr, type) {
    var docu = document;
    var wind = window;
    var currlms = docu.body.lastElementChild;
    if (type < 2) {
        Array.prototype.slice.call(currlms.childNodes).forEach(function (elem) {
            type == 1 ? elem.className == "replaceble1" ? currlms.removeChild(elem) : 0 :
            elem.className == "replaceble0" || elem.className == "replaceble1" ? currlms.removeChild(elem) : 0;
        });
    }
        if (type >= 1) {
            var newStatics = docu.createElement("DIV");
            newStatics.innerHTML = staticStr;
            var newStatics = Array.prototype.slice.call(newStatics.childNodes);
        } else {
            var newStatics = staticStr.children || Array.prototype.slice.call(staticStr.childNodes);
        }
        for (var i = 0; i < newStatics.length; i++) {
            var elem = newStatics[i];
            if (elem.nodeType == 1) {
                if (elem.src == "" || elem.nodeName == "NOSCRIPT") {
                    type == 1 ? currlms.appendChild(elem) : 0;
                    elem.nodeName == "SCRIPT" ? wind.eval(elem.innerHTML) : 0;
                } else {
                    (function(elem) {
                        var scriptRequest = new XMLHttpRequest();
                        scriptRequest.open("GET", elem.src, true);
                        scriptRequest.responseType = 'text';
                        scriptRequest.onloadend = function(event) {
                            var data = (this || event.target).response;
                            elem.innerHTML = data;
                            currlms.appendChild(elem);
                            wind.eval(data);
                        };
                        scriptRequest.send();
                    })(elem);
                }
            }
        }
    return true;
};
function insertingNewHead(headStr, type) {
    var currlms = document.head;
    if (type < 2) {
        Array.prototype.slice.call(currlms.childNodes).forEach(function (elem) {
            type == 1 ? elem.className === "replaceble1" ? elem.parentNode.removeChild(elem) : 0 :
            elem.className == "replaceble0" || elem.className == "replaceble1" ? elem.parentNode.removeChild(elem) : 0;
        });
    }
    if (type >= 1) {
        var newH = document.createElement("head");
        newH.innerHTML = headStr;
        var newH = Array.prototype.slice.call(newH.childNodes);
    } else {
        var newH = Array.prototype.slice.call(headStr.childNodes);
    }
    for (var i = 0; i < newH.length; i++) {
        var elem = newH[i];
        if (elem.nodeName == "LINK") {
            currlms.appendChild(elem);
        }
    }
    return true;
};
function insertingNewBody(bodyID, bodyStr) {
    var doc = document;
    var newB = doc.createElement("DIV");
    newB.setAttribute("class", "in-progress");
    newB.id = bodyID;
    newB.innerHTML = bodyStr;
    var pageContent = doc.getElementById(bodyID);
    var pageContentParent = pageContent.parentNode;
    if (pageContent.remove) {
        pageContent.remove();
    } else {
        var parent = pageContent.parentNode;
        parent.removeChild(pageContent);
    }
    pageContentParent.appendChild(newB);
    return [true, newB];
};
function globalNavigation(event, refUrl) {
    var w = window;
    reInactiveMenu();
    event ? reBuildMenu(event) : 0;
    w.mainSockListener ? mainSockListener = null : 0;
    w.userSock ? userSock.onmessage = defaultListener : 0;
    w.statusInitial ? statusInitial = null : 0;
    loadContent("global", refUrl + "?global");
    if (event) {
        return event.preventDefault();
    }
};