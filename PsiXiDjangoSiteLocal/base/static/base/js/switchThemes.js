var canvasFontSize = 25; // 1 - 100%
var canvasTextToCenter = true; // Если не true, то числовое знаение от края контекста (Boolean/Number)
var ungleKeys = 90; // Базовая линия повёрнута к центру (90), иначе другой угол поворота (Number)
var animDirection = 1; // Если 1, то прямая по часовой, обратная против; если -1, то наоборот
var KeyBoardABC = {
    a: 65, b: 66, c: 67, d: 68, e: 69, f: 70, g: 71, h: 72, i: 73, j: 74, k: 75, l: 76,
    m: 77, n: 78, o: 79, p: 80, q: 81, r: 82, s: 83, t: 84, u: 85, v: 86, w: 87, x: 88, y: 89, z: 90,
    0: 48, 1: 49, 2: 50, 3: 51, 4: 52, 5: 53, 6: 54, 7: 55, 8: 56, 9: 57,
    shift: 16, ctrl: 17, alt: 18, spacebar: 32, tab: 9, enter: 13, ñ: 192
};
function generate(themeSettings, canvasSize, launchKeys, launchKeyThemes, launchKeyOptions,
    typeOfPressThemes, typeOfPressOptions, canvasHTML0) {
    var globalBody = "$(document).on('" + typeOfPressThemes +
        "', function(event) {\n\tkey = event.keyCode;\n\t" +
        "if (key === " + KeyBoardABC[launchKeyThemes] + ") {\n\t\t";
    if (typeOfPressThemes === "keyup") {
        globalBody += "switchReverse();\n\t}\n});\n";
    } else {
        globalBody += "if (switchHoldKey === false) {\n\t\t\tswitchHoldKey = true;\n\t\t\tswitchReverse();" +
            "\n\t\t}\n\t}\n});\n$(document).on('keyup', function(event) {\n\t" +
            "key = event.keyCode;\n\tif (key === " + KeyBoardABC[launchKeyThemes] + ") {\n\t\tswitchHoldKey = false;\n\t\t" +
            "switchReverse();\n\t}\n});\n";
    } 
    globalBody += "$(document).on('" + typeOfPressOptions +
        "', function(event) {\n\tkey = event.keyCode;\n\tif (key === " + KeyBoardABC[launchKeyOptions] + ") {\n\t\t";
    if (typeOfPressOptions === "keyup") {
        globalBody += "optionsReverse();\n\t}\n});\n";
    } else {
        globalBody += "if (optionsHoldKey === false) {\n\t\t\toptionsHoldKey = true;\n\t\t\toptionsReverse();" +
            "\n\t\t}\n\t}\n});\n$(document).on('keyup', function(event) {\n\t" +
            "key = event.keyCode;\n\tif (key === " + KeyBoardABC[launchKeyOptions] + ") {\n\t\toptionsHoldKey = false;\n\t\t" +
            "optionsReverse();\n\t}\n});\n";
    }
    var resultKeyDown = "$(document).on('keydown', function(event) {\n\tkey = event.keyCode;";
    var fstUpKey = "\t\tif (";
    var resultKeyUp = "$(document).on('keyup', function(event) {\n\tkey = event.keyCode;";
    var keysLevels1 = [];
    var generalBody0 = "function keyResult(themeNumering, resultUpp) {" +
        "\n\tvar launchKeys = switchThemesObj.launchKeys;" +
        "\n\tvar themeNumering = Object.keys(themeNumering);\n" +
        "\tvar keys = Object.keys(launchKeys);\n";
    var generalBody1 = "\tfor (var ma = 0; ma < themeNumering.length; ma++) {\n";
    var functionsBody0 = "";
    var functionsBody1 = "";
    var functionsBody2 = "";
    var canvasBody = "";
    var canvasHTML1 = "";
    var canvasHTML2 = [];
    var keys = Object.keys(themeSettings);
    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        var keysLevels = [];
        var zonesFstChar = charName.charAt(i);
        var canvasHTML1 = "";
        for (var a = 0; a < themeSettings[key].length; a++) {
            resultKeyDown += "\n\tif (key === " + KeyBoardABC[launchKeys[key][a]] +
                ") {\n\t\tif (switchWork === true) {\n\t\t\tfunction getKeyDown() {" +
                "\n\t\t\t\ttypeof(themeNumering) === 'undefined' ? themeNumering = {} : 0;\n\t\t\t\tthemeNumering['" +
                launchKeys[key][a] + "'] = " + (i + a) +
                ";\n\t\t\t\tkeyResult(themeNumering);\n\t\t\t};\n\t\t\tgetKeyDown();\n\t\t}\n\t}" +
                (i < keys.length - 1 | a < themeSettings[key].length - 1 ? " " : "");
            resultKeyUp += (i === 0 & a === 0 ? "\n\tif" : " else if") +
                " (key === " + KeyBoardABC[launchKeys[key][a]] + ") {\n\t\tfunction getKeyUp() {\n\t\t\tdelete themeNumering['" +
                launchKeys[key][a] + "'];\n\t\t\tvar resultUpp = '" + launchKeys[key][a] +
                "';\n\t\t\tkeyResult(themeNumering, resultUpp);\n\t\t};\n\t\tgetKeyUp();\n\t}";
            keysLevels[a] = KeyBoardABC[launchKeys[key][a]];
            canvasHTML1 += "<canvas id = 'ray" + launchKeys[key][a] + "' width = " + canvasSize[key] +
                " height = " + canvasSize[key] + " style = 'display: none;'></canvas><canvas id = 'key" + launchKeys[key][a] +
                "' width = " + canvasSize[key] + " height = " + canvasSize[key] + " style = 'display: none;'></canvas>";
        }
        canvasHTML0[i] = canvasHTML1 + "<canvas id = 'circle" + zonesFstChar +
            "' width = " + canvasSize[key] + " height = " + canvasSize[key] +
            " style = 'display: none;'></canvas>";
        canvasHTML2[i] = "<canvas id = 'center" + zonesFstChar + "' width = " + (canvasSize[key] / 100 * canvasCenterSize) +
            " height = " + (canvasSize[key] / 100 * canvasCenterSize) + "></canvas>";
        functionsBody0 += "function step" + zonesFstChar + "(zone" + zonesFstChar +
            ") {\n\tfor (var " + zonesFstChar + " = 0; " + zonesFstChar + " < zone" +
            zonesFstChar + ".length; " + zonesFstChar +
            "++) {\n\t\tif (anglesDyn[zone" + zonesFstChar + "[" + zonesFstChar +
            "]] === anglesComp[zone" + zonesFstChar + "[" + zonesFstChar +
            "]]) {\n\t\t\tvar selectr0 = $('#circle" + zonesFstChar +
            "');\n\t\t\tvar selectr1 = $('#ray' + zone" + zonesFstChar +
            "[" + zonesFstChar + "]);\n\t\t\tvar selectr2 = $('#key' + zone" + zonesFstChar +
            "[" + zonesFstChar + "]);\n\t\t\tselectr0.before(selectr1, selectr2);\n\t\t}\n\t}\n};\n";
        functionsBody1 += "\tzone" + zonesFstChar + ".length < 2 ? step" + zonesFstChar +
            "(zone" + zonesFstChar + ") : error" + zonesFstChar +
            "(zone" + zonesFstChar + ");\n";
        functionsBody2 += "\tacess" + zonesFstChar + " = function() {\n\t\t\n\t};\n";
        generalBody0 += "\tvar zone" + zonesFstChar + " = [];\n";
        generalBody1 += "\t\tfor (var " + zonesFstChar + " = 0; " + zonesFstChar +
            " < launchKeys[keys[" + i + "]].length; " + zonesFstChar +
                "++) {\n\t\t\t" + "themeNumering[ma] === launchKeys[keys[" + i +
                "]][" + zonesFstChar + "] ? zone" + zonesFstChar +
                "[ma] = launchKeys[keys[" + i +
                "]][" + zonesFstChar + "].charAt(0) : 0;\n\t\t};\n" + (i === keys.length - 1 ? "\t};\n" : "");
        keysLevels1[i] = "key === " + keysLevels.join(" || key === ");
        fstUpKey += keysLevels1[i] + ") {\n\t\t\tacess" + zonesFstChar +
            "();\n\t\t}" + (i < keys.length - 1 ? " else if (" : "");
    }
    var canvasHTML0 = canvasHTML0.concat(canvasHTML2);
    keysLevels = null;
    fstUpKey = "\n\tif (" + keysLevels1.join(" || ") + ") {\n\t\tacessAll();\n" + fstUpKey + "\n\t}\n";
    keysLevels1 = null;
    var generalBody0 = generalBody0 + generalBody1;
    var generalBody1 = null;
    var functionsBody2 = "\tacessAll = function() {\n\t\tbackAnimation(resultUpp);\n\t};\n" + functionsBody2;
    var functionsBody1 = functionsBody1 + "\tanimation(themeNumering, resultUpp);\n";
    var generalBody0 = generalBody0 + functionsBody1 + functionsBody2 + "};\n";
    var functionsBody1 = functionsBody2 = null;
    var resultKeyDown = functionsBody0 + generalBody0 + resultKeyDown + "\n});\n" + resultKeyUp +
        " " + fstUpKey + "});";
    var globalBody = globalBody + resultKeyDown;
    var generalBody0 = functionsBody0 = resultKeyUp = resultKeyDown = fstUpKey = null;
    eval(globalBody);
    //console.log(globalBody);
    return canvasHTML0;
};
function startCor(cnvContent, themeSettings, launchKeys, canvasSize, speed,
    anglesDyn, anglesComp, stepClear, stepStroke, sCZeroPoint, curRad) {
    var keys = Object.keys(cnvContent);
    var thKeys = Object.keys(themeSettings);
    for (var i = 0; i < keys.length; i++) {
        if (keys[i].slice(0, this.length - 1) === "ray") {
            var z = i === 0 ? 0 : keys[i - 1].slice(0, this.length - 1) === "circle" ? z + 1 : z;
            var t = i === 0 ? 0 : keys[i - 1].slice(0, this.length - 1) === "circle" ? 0 : t + 1;
            var lK = launchKeys[thKeys[z]][t];
            var sp = speed[thKeys[z]];
            var cS = canvasSize[thKeys[z]];
            curRad[lK] = cS / 2;
            anglesComp[lK] = (360 - (360 / themeSettings[thKeys[z]].length)) / sp;
            anglesDyn[lK] = anglesComp[lK] = Math.floor(anglesComp[lK]);
            stepClear[lK] = (Math.PI * cS) / anglesComp[lK];
            stepStroke[lK] = (360 - (360 / themeSettings[thKeys[z]].length)) / anglesComp[lK];
            var points = [];
            var counter = 0;
            for (var e = 0; e <= anglesComp[lK]; e++) {
                counter -= stepClear[lK] / anglesComp[lK];
                points[e] = counter;
            }
            points[this.length] = 0;
            sCZeroPoint[lK] = points.reverse();
        }
    }
    return anglesDyn, anglesComp, stepClear, stepStroke, sCZeroPoint, curRad;
};
function canvasRendering(cnvContent, themeSettings, launchKeys, canvasSize, canvasFill, curRad) {
    var keys = Object.keys(cnvContent);
    var thKeys = Object.keys(themeSettings);
    for (var i = 0; i < keys.length; i++) {
        if (keys[i].slice(0, this.length - 1) === "ray") {
            if (i === 0 || keys[i - 1].slice(0, this.length - 1) === "circle") {
                var u = typeof(u) === "undefined" ? 0 : u + 1;
                var t = 0;
                var angle = 0;
                var angleEnd = 360 / themeSettings[thKeys[u]].length;
            } else {
                var angle = 360 / themeSettings[thKeys[u]].length;
                angleEnd += angle;
                var angle = angleEnd - angle;
            }
            var lK = launchKeys[thKeys[u]][t];
            stroking(cnvContent[keys[i]], angle.toRad(), angleEnd.toRad(), curRad[lK], canvasFill[thKeys[u]][t]);
        } else if (keys[i].slice(0, this.length - 1) === "key") {
            var lK = launchKeys[thKeys[u]][t];
            var cS = canvasSize[thKeys[u]];
            var angleKey = angleEnd - ((360 / themeSettings[thKeys[u]].length) / 2);
            var align = (canvasTextToCenter === true ? u < thKeys.length - 1 ? cS - canvasSize[thKeys[u + 1]] : cS : canvasTextToCenter) / 2;
            var size = align * canvasFontSize / 100;
            var align = curRad[lK] - align + size;
            var x = curRad[lK] + Math.cos(angleKey.toRad()) * align;
            var y = curRad[lK] + Math.sin(angleKey.toRad()) * align;
            var angleKey = angleKey + ungleKeys;
            texting(cnvContent[keys[i]], lK.toUpperCase(), angleKey.toRad(), size, x, y);
            var t = t + 1;
        }
    }
};
function stroking(cnvContent, angle, angleEnd, curRad, canvasFill) {
    cnvContent.translate(curRad, curRad);
    cnvContent.beginPath();
    cnvContent.moveTo(0, 0);
    cnvContent.arc(0, 0, curRad, angle, angleEnd, false);
    cnvContent.closePath();
    cnvContent.rotate(angleEnd);
    cnvContent.translate(-curRad, -curRad);
    filling(cnvContent, canvasFill);
};
function texting(cnvContent, lK, angleKey, size, x, y) {
    cnvContent.font = "bold ".concat(size).concat("px sans-serif");
    cnvContent.textBaseline = "bottom";
    cnvContent.textAlign = "center";
    cnvContent.translate(x, y);
    cnvContent.rotate(angleKey);
    cnvContent.strokeText(lK, 0, 0);
    cnvContent.translate(-x, -y);
};
function filling(cnvContent, canvasFill) {
    cnvContent.fillStyle = canvasFill;
    cnvContent.fill();
};
function clearIng(cnvContent, angle, curRad, sCZeroPoint, stepClear) {
    cnvContent.translate(curRad, curRad);
    cnvContent.beginPath();
    cnvContent.moveTo(0, 0);
    cnvContent.clearRect(0, sCZeroPoint, curRad, stepClear);
    cnvContent.closePath();
    cnvContent.rotate(angle);
    cnvContent.translate(-curRad, -curRad);
};
function zebraStroking() {

};
function animation(themeNumering, resultUpp) {
    var launchKeys = switchThemesObj.launchKeys;
    var canvasFill = switchThemesObj.canvasFill;
    var speed = switchThemesObj.speed;
    var keys = Object.keys(launchKeys);
    for (var i = 0; i < themeNumering.length; i++) {
        if (anglesDyn[themeNumering[i]] > 0) {
            for (var x = 0; x < keys.length; x++) {
                if (themeNumering[i] === launchKeys[keys[x]][launchKeys[keys[x]].indexOf(themeNumering[i])]) {
                    var r = x;
                    var t = launchKeys[keys[r]].indexOf(themeNumering[i]);
                }
            }
            var angle = speed[keys[r]] * (-1); // Для учёта погрешности
            var angleEnd = animDirection * stepStroke[themeNumering[i]];
            stroking(cnvContent['ray' + themeNumering[i]], angle.toRad(), angleEnd.toRad(), curRad[themeNumering[i]], canvasFill[keys[r]][t]);
            anglesDyn[themeNumering[i]] -= 1;
        }
    }
};
function backAnimation(resultUpp) {
    var launchKeys = switchThemesObj.launchKeys;
    var keys = Object.keys(launchKeys);
    for (var i = 0; i < keys.length; i++) {
        if (launchKeys[keys[i]][launchKeys[keys[i]].indexOf(resultUpp)] === resultUpp) {
            var r = i;
            var t = launchKeys[keys[r]].indexOf(resultUpp);
        }
    }
    bckAni = setInterval(function() {
        breakBackAnim = function() {
            clearInterval(bckAni); bckAni = null;
        };
        var angle = anglesDyn[resultUpp] < anglesComp[resultUpp] ? animDirection * (-1) * stepStroke[resultUpp] : 0;
        clearIng(cnvContent['ray' + resultUpp], angle.toRad(), curRad[resultUpp], sCZeroPoint[resultUpp][anglesDyn[resultUpp]], stepClear[resultUpp]);
        anglesDyn[resultUpp] < anglesComp[resultUpp] ? anglesDyn[resultUpp] += 1 : breakBackAnim();
    }, 20);
};
function panelHTML(themeSettings, canvasHTML0, cnvContent) {
    for (var i = 0; i < canvasHTML0.length; i++) {
        $("#themesMenu").append(canvasHTML0[i]);
    }
    var canvasHTML1 = $("#themesMenu").children("canvas");
    for (var i = 0; i < canvasHTML1.length; i++) {
        cnvContent[canvasHTML1[i].id] = canvasHTML1[i].getContext("2d");
    } 
    return cnvContent;
};
function startingPanel() {
    // Тут потом будет локальный switchThemesObj
    var launchKeyThemes = "q";
    var launchKeyOptions = "z";
    var typeOfPressThemes = "keydown"; // тип вызова круга тем: по нажатию или по удерживанию (keyup или keydown/keyup)
    var typeOfPressOptions = "keyup"; // тип вызова панели опций: по нажатию или по удерживанию (keyup или keydown/keyup)
    $("li.switchthemesConfButton div small").text(launchKeyOptions.toUpperCase());
    $("li.switchthemesThemesButton div small").text(launchKeyThemes.toUpperCase());
    var themeSettings = switchThemesObj.themeSettings;
    var unSettings = switchThemesObj.unSettings;
    var launchKeys = switchThemesObj.launchKeys;
    var canvasSize = switchThemesObj.canvasSize;
    var canvasFill = switchThemesObj.canvasFill;
    var speed = switchThemesObj.speed;
    genLevelsHTML(themeSettings, unSettings, launchKeys, canvasSize, canvasFill, speed);
    genThemesHTML(themeSettings, unSettings, launchKeys, canvasSize, canvasFill, speed);
    genKeysHTML(themeSettings, charName);
    onStartForming(themeSettings, unSettings);
    $("#switchThemesMain .footer ul.menu").children("li").on('click', function (ev) {
        var currElement = ev.currentTarget;
        menuHelper(currElement);
        $("#switchThemesMain .footer ul.menu").children("li.levels").on('click', levelsFunction);
        $("#switchThemesMain .footer ul.menu").children("li.themes").on('click', themesFunction);
        $("#switchThemesMain .footer ul.menu").children("li.elements").on('click', elementsFunction);
        $("#switchThemesMain .footer ul.menu").children("li.content").on('click', contentFunction);
        $("#switchThemesMain .footer ul.menu").children("li.common").on('click', commonFunction);
    });
    var canvasHTML0 = [];
    generate(themeSettings, canvasSize, launchKeys, launchKeyThemes, launchKeyOptions,
        typeOfPressThemes, typeOfPressOptions, canvasHTML0);
    var cnvContent = {};
    var anglesDyn = {};
    var anglesComp = {};
    var stepClear = {};
    var stepStroke = {};
    var sCZeroPoint = {};
    var curRad = {};
    panelHTML(themeSettings, canvasHTML0, cnvContent);
    startCor(cnvContent, themeSettings, launchKeys, canvasSize, speed,
        anglesDyn, anglesComp, stepClear, stepStroke, sCZeroPoint, curRad);
    canvasRendering(cnvContent, themeSettings, launchKeys, canvasSize, canvasFill, curRad);
};
$(document).ready(function () {
    startingPanel();
});
