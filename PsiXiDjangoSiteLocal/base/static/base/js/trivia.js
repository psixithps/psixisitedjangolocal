/* Теперь revert создаст новый массив */
Array.prototype.reversedCopy = function() {
    var arr = [];
    for (var i = this.length; i--;) {
        arr.push(this[i]);
    };
    return arr;
};
/* Добавление функций к prototype */
Number.prototype.toRad = function() { return this * Math.PI / 180; };
Number.prototype.toGrad = function() { return this / Math.PI * 180; };
Number.prototype.isEven = function() { return this % 2 == 0 ? true : false; };
Number.prototype.isInteger = function () { return this % 1 == 0 ? true : false; };
/* Array update Вернёт расширенную другим массивом копию */
Array.prototype.update = function (arr) {
    var l = arr.length;
    do {
        var item = arr[--l];
        this.indexOf(item) == -1 ? this.push(item) : 0;
    } while (l)
    return this;
};
Array.prototype.discard = function(arr) {
    var arrL = arr.length;
    if (arrL) {
        var mainArray = this.concat();
        for (var i = arrL; i--;) {
            var f = mainArray.indexOf(arr[i]);
            f != -1 ? mainArray.splice(f, 1) : 0;
        }
        return mainArray;
    } else {
        return this;
    }
};
/* Возвращает копию массива, из которого удалён искомый элемент - СТРОКА */
Array.prototype.removeItem = function(elem) {
    var f = this.indexOf(elem);
    if (f > -1) {
        var newArr = this.concat();
        newArr.splice(f, 1);
        return newArr;
    } else {
        return this;
    }
};
/* Вычитание 2 списков: (1[].difference(2[]) = []) вернётся новый список, в нём будут уникальные элементы из 1[] СТРОКИ*/
Array.prototype.difference = function(arr1) {
    var newArr = this.concat();
    if (this.length) {
        var arr1L = arr1.length;
        if (arr1L) {
            for (var i = arr1L; i--;) {
                var index = newArr.indexOf(arr1[i]);
                if (index != -1) {
                    newArr.splice(index, 1);
                }
            }
        }
    }
    return newArr;
};
/* Объединение 2 массивов: (1[] + 2[] = []) вернётся новый массив СТРОКИ */
Array.prototype.union = function(arr) {
    if (this.length) {
        var arrL = arr.length;
        if (arrL) {
            var nArr = this.concat();
            for (var i = 0; i < arrL; i++) {
                var item = arr[i];
                nArr.indexOf(item) == -1 ? nArr.push(item) : 0;
            }
            return nArr;
        } else {
            return this.concat();
        }
    } else {
        if (arr.length) {
            return arr.concat();
        } else {
            return [];
        }
    }
};
Array.prototype.intersection = function(arr) {
    var currentArrLength = this.length;
    var arrLength = arr.length;
    if (arrLength && currentArrLength) {
        var newArr = [];
        for (var i = arrLength; i--;) {
            var item = arr[i];
            this.indexOf(item) != -1 ? newArr.unshift(item) : 0;
        }
        return newArr;
    } else {
        return [];
    }
};
/* Копия свойств объекта в другой объект - поверхностная копия */
if (!Object.assign) {
    Object.defineProperty(Object, 'assign', {
        enumerable: false,
        configurable: true,
        writable: true,
        value: function (target) {
            'use strict';
            if (target === undefined || target === null) {
                throw new TypeError('Cannot convert first argument to object');
            }
            var to = Object(target);
            for (var i = arguments.length; i >= 0; i--) {
                var nextSource = arguments[i];
                if (nextSource === undefined || nextSource === null) {
                    continue;
                }
                nextSource = Object(nextSource);

                var keysArray = Object.keys(nextSource);
                for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
                    var nextKey = keysArray[nextIndex];
                    var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
                    if (desc !== undefined && desc.enumerable) {
                        to[nextKey] = nextSource[nextKey];
                    }
                }
            }
            return to;
        }
    });
}
Object.prototype.pop = function (keyName) {
    if (this.hasOwnProperty(keyName)) {
        var value = this[keyName];
        delete this[keyName];
        return value;
    } else {
        return null;
    }
};
