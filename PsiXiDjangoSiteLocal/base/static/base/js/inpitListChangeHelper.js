﻿function formLifeHelper(namesArr, currentFieldElement) {
    for (currentFieldElement.lastChild in currentFieldElement.childNodes) {
        var delElem = currentFieldElement.lastChild;
        delElem ? delElem.remove() : 0;
    }
    for (var i = 0; i < namesArr.length; i++) {
        var newOption = document.createElement("option");
        newOption.value = namesArr[i];
        currentFieldElement.appendChild(newOption);
    }
};

function cleanOptions(currentFieldElement) {
    for (currentFieldElement.lastChild in currentFieldElement.childNodes) {
        var el = currentFieldElement.lastChild;
        el ? el.remove() : 0;
    }
};