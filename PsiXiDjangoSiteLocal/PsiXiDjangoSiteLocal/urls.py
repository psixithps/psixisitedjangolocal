"""
Definition of urls for PsiXiDjangoSiteLocal.
"""

from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings # Для раздачи media (применимо только для локального проекта)
from django.conf.urls.static import static # Для раздачи media (применимо только для локального проекта)
from psixiSocial.registration import profileRegistration, profileRegistrationStrong
from psixiSocial.login import login, logOut

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'', include('main.urls')),
    url(r'^community/', include('psixiSocial.urls')),
    url(r'^video/', include('videos.urls')),
    url(r'^library/', include('libraryTHPS.urls')),
    url(r'^service/', include('realtime.urls')),
    url(r'^login/$', login, name = "login"),
    url(r'logout', logOut, name = "logOut"),
    url(r'^registration/$', profileRegistration, name = "registration"),
    url(r'^registration/(?P<troubleUsername>[-\w]+)/$', profileRegistrationStrong, name = "registrationStrong"),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT) # Для раздачи media (применимо только для локального проекта)