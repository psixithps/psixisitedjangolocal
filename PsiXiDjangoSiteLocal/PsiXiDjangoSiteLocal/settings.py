"""
Django settings for PsiXiDjangoSiteLocal project.

Generated by 'django-admin startproject' using Django 1.9.1.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.9/ref/settings/
"""

import os
import posixpath

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '80bbda8c-c37e-4e43-a2de-59bf3ff19e9e'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['localhost',]


# Application definition

INSTALLED_APPS = [
	'django.contrib.admin',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.messages',
	'django.contrib.staticfiles',
	#'gdal', # не указывать на боевом
	'channels',
    'channels_redis',
	'cities',
	'imagekit',
	'adv_cache_tag',
	'realtime',
	'base',
	'main',
	'videos',
	'libraryTHPS',
	'psixiSocial',
]

AUTH_USER_MODEL = 'psixiSocial.mainUserProfile'

LOGIN_URL = '/login/'

# Начало django-cities

# Override the default source files and URLs
CITIES_FILES = {
	'city': {
		'filename': 'cities15000.zip',
		'urls': ['http://download.geonames.org/export/dump/' + '{filename}']
	},
}

CITIES_LOCALES = ['en', 'und', 'LANGUAGES']

#CITIES_DATA_DIR = '/home/fil/psixiSiteDir/psixiProject/citiesData' --> %APPDATA%\Local\Programs\Python\Python35\Lib\site-packages\cities\data

CITIES_POSTAL_CODES = []

# List of plugins to process data during import
CITIES_PLUGINS = [
	'cities.plugin.reset_queries.Plugin',  # plugin that helps to reduce memory usage when importing large datasets (e.g. "allCountries.zip")
]

# This setting may be specified if you use 'cities.plugin.reset_queries.Plugin'
CITIES_PLUGINS_RESET_QUERIES_CHANCE = 1.0 / 1000000

# Конец django-cities

FILE_UPLOAD_HANDLERS = [
	'django.core.files.uploadhandler.MemoryFileUploadHandler',
	'django.core.files.uploadhandler.TemporaryFileUploadHandler',
	]

MIDDLEWARE = [
	'django.middleware.security.SecurityMiddleware',
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.common.CommonMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
	'django.middleware.clickjacking.XFrameOptionsMiddleware',
	'realtime.middleware.OnlineNowMiddleware',
	'django_cprofile_middleware.middleware.ProfilerMiddleware',
    'django.middleware.gzip.GZipMiddleware'
]

ROOT_URLCONF = 'PsiXiDjangoSiteLocal.urls'

TEMPLATES = [
	{
		'BACKEND': 'django.template.backends.django.DjangoTemplates',
		'DIRS': [],
		'APP_DIRS': True,
		'OPTIONS': {
			'context_processors': [
				'django.template.context_processors.debug',
				'django.template.context_processors.request',
				'django.contrib.auth.context_processors.auth',
				'django.contrib.messages.context_processors.messages',
				'django.template.context_processors.media',
				'django.template.context_processors.i18n',
				'django.template.context_processors.tz',
			]
		},
	},
]

WSGI_APPLICATION = 'PsiXiDjangoSiteLocal.wsgi.application'


#  Начало channels

CHANNEL_LAYERS = {
    "default": {
        'BACKEND': 'channels.layers.InMemoryChannelLayer',
        }
    #"default": {
        #"BACKEND": "channels_redis.core.RedisChannelLayer",
        #"CONFIG": {
            #"hosts": [("localhost", 6379)],
        #},
    #},
}

ASGI_APPLICATION = 'realtime.routing.routeApp'

# Конец channels

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = {
# Запасная база данных, родная
#    'default': {
#        'ENGINE': 'django.db.backends.sqlite3',
#        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#    }
	'default': {
		'ENGINE': 'django.contrib.gis.db.backends.postgis',
		'NAME': 'psixidblocal',
	'USER': 'fildblocal',
	'PASSWORD': 'psixilocal',
	'HOST': 'localhost',
	'PORT': '',
	}
}

CACHES = {
	'default': {
		'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
		},
	#'default': { # templates cache
		#'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
		#'LOCATION': os.path.join(BASE_DIR, 'cache'),
		#},
	'onlineUsers': { # userIdStr: True;
		'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
		'LOCATION': 'onlineusers'
		},
	'iffyUsersOnlineStatus': { # userIdStr: datetimeStr;
		'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
		'LOCATION': 'iffyusers'
		},
    'chatStatusActive': { # userIdStr: True;
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
		'LOCATION': 'chatstatusactive'
        },
    'chatStatusMiddle': { # userIdStr: "datetimeStr|срок_жизни_сек";
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
		'LOCATION': 'chatstatusmiddle'
        },
	'unreadPersonalMessages': { # {dialogIdStr: {userIdStr: set(messageIdStr, messageIdStr)}
		'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
		'LOCATION': 'unreadpersonalmessages',
		'TIMEOUT': None
		},
    'unreadGroupMessages': { # {dialogIdStr: {messageIdStr: unreadUsersIdsSet, messageIdStr: unreadUsersIdsSet,}}
        'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
		'LOCATION': 'unreadgroupmessages',
		'TIMEOUT': None
        },
    'groupDialogsChannelsLayers': { # userIdStr: [groupDialogId, groupDialogId, groupDialogId], userIdStr: False, если количество диалогов равно 0
        # groupDialogId: {userId: set(channel_name, channel_name,), userId: set(channel_name, channel_name,)} # Текущее состояние - пользователи, которые подключены на данный момент
        # 
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
		'LOCATION': 'groupdialoglayers',
		'TIMEOUT': None # Указать в месте записи, время будет разным
        },
    'statusChannelLayers': { # Хранилище для каналов статуса
        #
        #'userId': {
        #    'online': {myId: set(channel_name, channel_name)}
        #    'active': {myId: set(channel_name, channel_name)}
        #    'self': { Найти тех, к чьим группам статуса подключены мои каналы
        #        'online': {channel_name: set(userId, userId), channel_name: set(userId, userId)},
        #        'active': {channel_name: set(userId, userId), channel_name: set(userId, userId)}
        #        }
        #    }
        #
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
		'LOCATION': 'statususerschannels',
		'TIMEOUT': 10800 # 3 часа
        },
    'usersChannelLayers': { # Ведётся учёт - userId: set(channelName, channelName, channelName), только если если пользователь фигурирует в групповых чатах
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
		'LOCATION': 'userschannels',
		'TIMEOUT': 259200 # 3 суток
        },
	'filesStorageMessages': { # dialogIdStr: {userIdStr: {"images:" : {"fileName": base64EncodedFile, "fileName": base64EncodedFile}}}
		'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
		'LOCATION': 'filestoragemessages',
		'TIMEOUT': 259200 # 3 суток
		},
	'dialogSingle': {
		'BACKEND': 'django.core.cache.backends.db.DatabaseCache', # Хранение ссылки на последний QuerySet диалога для пользователя, чтобы не нагружать БД {myId: {dialogQuerySet, 'withUserId': toUserIdStr}}
		'LOCATION': 'usersavelastdialogpersonal',
		'TIMEOUT': 300
		},
    'dialogGroup': {
        'BACKEND': 'django.core.cache.backends.db.DatabaseCache', # Хранение ссылки на последний QuerySet модели свойств диалога для пользователя, чтобы не нагружать БД {dialogSlug: {'dialog': dialogQuerySet, 'attributes': groupDialogAttributesQuerySet, 'users': dialogQuerySet.users, 'usersIds': set(dialogQuerySet.users._id)}}
        'LOCATION': 'usersavelastdialoggroup',
		'TIMEOUT': 300
        },
	'lastMessage': { # Последний QuerySet сообщения в диалоге
		'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
		'LOCATION': 'lastMessage',
		'TIMEOUT': 300
		},
	'dialogsCache': { # {userIdStr: {"dialogs": OrederedDict({dialogIdStr: dialog})}, {"last": {dialogIdStr: [personalMessage(), personalMessage(), personalMessage()]}}}
		'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
		'LOCATION': 'dialogsordereddict',
		'TIMEOUT': 604800 # 1 неделя
		},
	'friendshipRequests': { # requestUserID: [[userIdStr, datetimeStr], [userIdStr, datetimeStr]]
		'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
		'LOCATION': 'friendshiprequests',
		'TIMEOUT': None
		},
	'newUpdates': { # userIdStr: [('groupMessage', 'new', myId, dialogIdStr), ('personalMessage', 'new', myId, dialogIdStr)]
		'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
		'LOCATION': 'unwatchedactions',
		'TIMEOUT': None
		},
	'lastVisit': {
		'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
		'LOCATION': 'lastonline',
		'TIMEOUT': None
		},
	'smallDataCache': { # Кеш для краткосрочного хранения важных данных
		'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
		'LOCATION': 'smalldatastorage',
		'TIMEOUT': 210
		},
	'friendsCache': { # Хранение ссылок на кеш QuerySet друзей {id: QuerySet[QuerySet]}
		'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
		'LOCATION': 'friendscache',
		'TIMEOUT': 259200 # 3 суток
		},
	'friendsIdCache': { # id: set(id..., id..., id...)
		'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
		'LOCATION': 'friendsidcache',
		'TIMEOUT': 3600 # 1 час
		},
	'friendShipStatusCache': { # id: {userIdStr: "request", otherId: "myRequest", someId: True}
		'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
		'LOCATION': 'friendshipstatus',
		'TIMEOUT': 259200 # 3 суток
		},
	'privacyCache': { # id: {requestUserId: {'viewProfile': True, 'viewFriends': False}, {requestUserId: {'viewProfile': True, 'viewFriends': False}}}
		# id: {0: {'viewProfile': True, 'viewFriends': False}} - Гость 0
		'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
		'LOCATION': 'privacycache',
		'TIMEOUT': 259200 # 3 суток
		},
	'usersListThumbs': { # id: {requestUserId: {'thumb': 'thumbStr_Не_закрыт_</li>!', 'usersList': 'Список_опций_1', 'showInOnlineList': bool, 'showInActiveList': bool, 'type2': 'Список_опций_2'}, 
        # id: {requestUserId: False}, Если скрыт профиль
        #requestUserId: {'thumb': 'thumbStr_Не_закрыт_</li>!', 'usersList': 'Список_опций_1', 'showInOnlineList': bool, 'showInActiveList': bool, 'type2': 'Список_опций_2'}} 
        # миниатюры из страниц со списком пользователей (большие)
		'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
		'LOCATION': 'userslistthumbs',
		'TIMEOUT': 604800 # 1 неделя
		},
	'smallThumbs': { # id: {requestUserId: {'url': user.profileUrl, 'name': name, 'thumb': thumb, 'id': id}} миниатюрки друзей, (маленькие)
		'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
		'LOCATION': 'profilethumbs',
		'TIMEOUT': 604800 # 1 неделя
		},
	'dialogListThumbs': { # id: {requestUserId: {'url': user.profileUrl, 'name': name, 'thumb': thumb, 'id': id}} миниатюрки списка диалогов, (маленькие)
		'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
		'LOCATION': 'dialoglistthumbs',
		'TIMEOUT': 259200 # 3 суток
		},
	'messagesListThumbs': { # id: {requestUserId: {'url': user.profileUrl, 'name': name, 'thumb': thumb, 'id': id}} миниатюрки списка сообщений, (маленькие)
		'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
		'LOCATION': 'messageslistthumbs',
		'TIMEOUT': 259200 # 3 суток
		},
	'dialogThumbs': { # id: {requestUserId: {
        #'item': tuple(itemString0, itemString1), 'showOnlineStatus': bool, 'showActiveStatus': bool, 'name': nameStr, 'smallName': smallNameStr
        #}} миниатюрки диалога - оглавление, (средние)
		'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
		'LOCATION': 'dialogthumbs',
		'TIMEOUT': 259200 # 3 суток
		}
}


# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
	{
		'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
	},
	{
		'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
	},
	{
		'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
	},
	{
		'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
	},
]

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'RU-ru'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

STATIC_ROOT = posixpath.join(*(BASE_DIR.split(os.path.sep) + ['static']))
MEDIA_ROOT = posixpath.join(*(BASE_DIR.split(os.path.sep) + ['media']))

# На случай ошибки поиска библиотек GDAL (убрать на боевом)
if os.name == 'nt':
    import platform
    OSGEO4W = r"C:\OSGeo4W"
    if '64' in platform.architecture()[0]:
        OSGEO4W += "64"
    assert os.path.isdir(OSGEO4W), "Directory does not exist: " + OSGEO4W
    os.environ['OSGEO4W_ROOT'] = OSGEO4W
    os.environ['GDAL_DATA'] = OSGEO4W + r"\share\gdal"
    os.environ['PROJ_LIB'] = OSGEO4W + r"\share\proj"
    os.environ['PATH'] = OSGEO4W + r"\bin;" + os.environ['PATH']