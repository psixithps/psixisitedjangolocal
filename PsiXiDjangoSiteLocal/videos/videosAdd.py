import os
from django.conf import settings
from django.shortcuts import render, redirect
from django.core import serializers
from videos.models import videoItemUpload, videoItemYoutube
from libraryTHPS.models import stylesShip, clansMain, playersShip, mapsShip, seriesShip, searchingHelper
from videos.forms import choiseAddType, addVideoUpload, addVideoYoutube
from moviepy.video.io.VideoFileClip import VideoFileClip
from videos.ImagekitVideosThumbs import ThumbnailBig, ThumbnailMiddle, ThumbnailMiniLarge, ThumbnailSmall

def videoAdding(request, typeOfAdding):
    if typeOfAdding == "upload":
        if request.method == "POST":
            videoForm = addVideoUpload(request.POST, request.FILES)
        else:
            videoForm = addVideoUpload(initial = {'addedBy': request.user, 'status': 'd'})
    elif typeOfAdding == "youtube":
        if request.method == "POST":
            videoForm = addVideoYoutube(request.POST)
        else:
            videoForm = addVideoYoutube(initial = {'addedBy': request.user, 'status': 'd'})
    if request.method == "POST":
        if videoForm.is_valid():
            videoForm.save()
            videoName = videoForm.cleaned_data["name"]
            if typeOfAdding == "upload":
                id = videoItemUpload.objects.get(name = videoName).id
                video = videoItemUpload.objects.get(id = id)
                videoFile = video.videoFile
                #
                videoObject = VideoFileClip("{0}{1}".format(settings.BASE_DIR, videoFile.url))
                #
                thumbPath = "videos/thumbs" 
                originalThumbPath = "videos/thumbs/original"
                bigThumbPath = "videos/thumbs/big"
                middleThumbPath = "videos/thumbs/middle"
                smallThumbPath = "videos/thumbs/small"
                miniLargeThumbPath = "videos/thumbs/miniLarge"
                thumbPathRoot = os.path.normpath("{0}{1}{2}".format(settings.BASE_DIR, settings.MEDIA_URL, thumbPath)) 
                thumbsPathOriginal = os.path.normpath("{0}{1}{2}".format(settings.BASE_DIR, settings.MEDIA_URL, originalThumbPath))
                thumbsPathBig = os.path.normpath("{0}{1}{2}".format(settings.BASE_DIR, settings.MEDIA_URL, bigThumbPath))
                thumbsPathMiddle = os.path.normpath("{0}{1}{2}".format(settings.BASE_DIR, settings.MEDIA_URL, middleThumbPath))
                thumbsPathSmall = os.path.normpath("{0}{1}{2}".format(settings.BASE_DIR, settings.MEDIA_URL, smallThumbPath))
                thumbsPathMiniLarge = os.path.normpath("{0}{1}{2}".format(settings.BASE_DIR, settings.MEDIA_URL, miniLargeThumbPath))
                # Создание дирректорий, если их пока нет
                if not os.path.exists(thumbPathRoot):
                    os.mkdir(thumbPathRoot)
                if not os.path.exists(thumbsPathOriginal):
                    os.mkdir(thumbsPathOriginal)
                if not os.path.exists(thumbsPathBig):
                    os.mkdir(thumbsPathBig)
                if not os.path.exists(thumbsPathMiddle):
                    os.mkdir(thumbsPathMiddle)
                if not os.path.exists(thumbsPathSmall):
                    os.mkdir(thumbsPathSmall)
                if not os.path.exists(thumbsPathMiniLarge):
                    os.mkdir(thumbsPathMiniLarge)
                # Генерация оригинального
                frameSave = os.path.normpath("{0}/{1}.png".format(thumbsPathOriginal, id))
                videoObject.save_frame(frameSave, t = 7.00)
                # Генерация дополнительных
                # Генерация big
                sourceImage = open(frameSave, 'rb')
                image_generator = ThumbnailBig(source = sourceImage)
                result = image_generator.generate()
                # Сохранение big
                newImage = open('{0}/{1}.png'.format(thumbsPathBig, id), 'wb')
                newImage.write(result.read())
                newImage.close()
                sourceImage.close()
                # Генерация middle
                sourceImage = open(frameSave, 'rb')
                image_generator = ThumbnailMiddle(source = sourceImage)
                result = image_generator.generate()
                # Сохранение middle
                newImage = open('{0}/{1}.png'.format(thumbsPathMiddle, id), 'wb')
                newImage.write(result.read())
                newImage.close()
                sourceImage.close()
                # Генерация small
                sourceImage = open(frameSave, 'rb')
                image_generator = ThumbnailSmall(source = sourceImage)
                result = image_generator.generate()
                # Сохранение small
                newImage = open('{0}/{1}.png'.format(thumbsPathSmall, id), 'wb')
                newImage.write(result.read())
                newImage.close()
                sourceImage.close()
                # Генерация mini-large
                sourceImage = open(frameSave, 'rb')
                image_generator = ThumbnailMiniLarge(source = sourceImage)
                result = image_generator.generate()
                # Сохранение mini-large
                newImage = open('{0}/{1}.png'.format(thumbsPathMiniLarge, id), 'wb')
                newImage.write(result.read())
                newImage.close()
                sourceImage.close()
                request.POST['videoImgOriginal'] = "{0}/{1}.png".format(originalThumbPath, id)
                request.POST['videoImgBig'] = "{0}/{1}.png".format(bigThumbPath, id)
                request.POST['videoImgMiddle'] = "{0}/{1}.png".format(middleThumbPath, id)
                request.POST['videoImgSmall'] = "{0}/{1}.png".format(smallThumbPath, id)
                request.POST['videoImgMiniLarge'] = "{0}/{1}.png".format(miniLargeThumbPath, id)
                videoImageForm = addVideoUpload(request.POST, instance = video)
                if videoImageForm.is_valid():
                    videoImageForm.save()
            return redirect("playVideo", id)
    objects = list(searchingHelper.objects.all()) + list(seriesShip.objects.all())
    objects = objects + list(stylesShip.objects.all()) + list(clansShip.objects.all())
    objects = objects + list(playersShip.objects.all()) + list(mapsShip.objects.all())
    searchObj = serializers.serialize('json', objects, fields = ('name', 'tags', 'playerIn',
                                                                 'ignoredSymbols', 'specialSymbols'))
    return render(request, "videos/videosAdd.html", {'form': videoForm, 'searchObj': searchObj, 'typeAdd': typeOfAdding})

def choiseVideoAddingType(request):
    if request.method == "POST":
        form = choiseAddType(request.POST)
        if form.is_valid():
            result = form.cleaned_data["videoImportType"]
            if result == "u":
                return redirect("addVideo", "upload")
            elif result == "l":
                return redirect("addVideo", "youtube")
    else:
        form = choiseAddType(initial = {'videoImportType': 'l'})
    return render(request, 'videos/videosAddingChoise.html', {'form': form})