from django import forms
from videos.models import videoItemUpload, videoItemYoutube, addingType

class choiseAddType(forms.Form):
    videoImportType = forms.ChoiceField(choices = addingType)

class addVideoYoutube(forms.ModelForm):
    class Meta:
        model = videoItemYoutube
        fields = ('videoUrl', 'name', 'description', 'atPart', 'inStyle', 'byClan', 'byPlayer',
                  'byMap', 'status', 'addedBy',)
        widgets = {
            'name': forms.TextInput(attrs = {'class': 'nameToNewVideo'}),
            'description': forms.Textarea(attrs = {'class': 'descriptionToNewVideo'}),
            'atPart': forms.Select(attrs = {'class': 'part'}),
            'inStyle': forms.Select(attrs = {'class': 'style'}),
            'byClan': forms.Select(attrs = {'class': 'clan'}),
            'byPlayer': forms.Select(attrs = {'class': 'player'}),
            'byMap': forms.Select(attrs = {'class': 'map'}),
            'addedBy': forms.HiddenInput,
        }
    videoUrl = forms.URLField(max_length = 100,
                              widget = forms.URLInput(attrs = {
                                  'class': 'urlNewVideo', 'onchange': 'getValue(val = this.value);'
                                  }))

class addVideoUpload(forms.ModelForm):
    class Meta:
        model = videoItemUpload
        fields = ('videoFile', 'videoImgOriginal', 'videoImgBig',
                  'videoImgMiddle', 'videoImgSmall', 'videoImgMiniLarge',
                  'name', 'description', 'atPart',
                  'inStyle', 'byClan', 'byPlayer',
                  'byMap', 'status', 'addedBy',)
        widgets = {
            'videoImgOriginal': forms.HiddenInput,
            'videoImgBig': forms.HiddenInput,
            'videoImgMiddle': forms.HiddenInput,
            'videoImgSmall': forms.HiddenInput,
            'videoImgMiniLarge': forms.HiddenInput,
            'name': forms.TextInput(attrs = {'class': 'nameToNewVideo'}),
            'description': forms.Textarea(attrs = {'class': 'descriptionToNewVideo'}),
            'atPart': forms.Select(attrs = {'class': 'part'}),
            'inStyle': forms.Select(attrs = {'class': 'style'}),
            'byClan': forms.Select(attrs = {'class': 'clan'}),
            'byPlayer': forms.Select(attrs = {'class': 'player'}),
            'byMap': forms.Select(attrs = {'class': 'map'}),
            'addedBy': forms.HiddenInput,
        }

    videoFile = forms.FileField(max_length = 200,
                                        widget = forms.ClearableFileInput(attrs = {
                                                'class': 'fileToNewVideo', 'onchange': 'getValue(val = this.value)',
                                            }))

    def clean_videoFile(self):
        self.fileName = self.cleaned_data["videoFile"].name
        self.fileNameArr = self.fileName.split(".")
        self.formaT = self.fileNameArr[len(self.fileNameArr) - 1]
        if self.formaT != "mp4" and self.formaT != "webm":
            raise forms.ValidationError(
                "Загрузка видео в формате {0} на данный момент не поддерживается.".format(self.formaT),
                code = "invalid")
        return self.cleaned_data["videoFile"]

class editVideoPicture(forms.Form):
    def __init__(self, *args, **kwargs):
        self.duration = kwargs.pop("duration", None)
        self.postition = kwargs.pop("scrnMoment", None)
        self.munutesPosition = kwargs.pop("inMinPosition", None)
        self.minutesDuration = kwargs.pop("inMinDuration", None)
        super(editVideoPicture, self).__init__(*args, **kwargs)

    time = forms.CharField(max_length = 5, required = False, widget = forms.TimeInput(attrs = {
        'class': 'timeToScrn',
        }))

    def clean_time(self):
        if self.postition == "empty":
            raise forms.ValidationError("Укажите момент времени для скриншота.", code = "invalid")
        elif self.postition == "badValue0":
            raise forms.ValidationError("Время указывается в формате: mm:ss, цифрами от 0 до 9.")
        elif self.postition == "badValue1":
            raise forms.ValidationError("По правую и левую сторону от двоеточия должны быть только цифры.", code = "invalid")
        else:
            if self.duration < self.postition:
                raise forms.ValidationError(
                    "За пределами продолжительности видео, укажите время в промежутке 00:00 - {0}.".format(
                        self.minutesDuration),
                    code = "invalid")
        return self.cleaned_data["time"]

class addVideoUploadAdmin(forms.ModelForm):
    class Meta:
        form = addVideoUpload
        fields = ('videoFile', 'name', 'description', 'atPart',
                  'inStyle', 'byClan', 'byPlayer',
                  'byMap', 'status', 'addedBy',)

class addVideoYoutubeAdmin(forms.ModelForm):
    class Meta:
        form = addVideoYoutube
        firlds = ('videoUrl', 'name', 'description', 'atPart',
                  'inStyle', 'byClan', 'byPlayer',
                  'byMap', 'status', 'addedBy',)