from django.contrib import admin
from videos.models import videoItemUpload, videoItemYoutube
from videos.forms import addVideoUploadAdmin, addVideoYoutubeAdmin

def publishVideo(modeladmin, request, queryset):
    queryset.update(status = 'p')
publishVideo.short_description = "Опубликовать выбранные видео"

def unPublishVideo(modeladmin, request, queryset):
    queryset.update(status = 'w')
unPublishVideo.short_description = "Снять с публикации выбранные видео"

class formYoutube(admin.ModelAdmin):
    form = addVideoYoutubeAdmin
    list_display = ['name', 'addedBy', 'status']
    ordering = ['name']
    actions = [publishVideo, unPublishVideo]

class formUpload(admin.ModelAdmin):
    form = addVideoUploadAdmin
    list_display = ['name', 'addedBy', 'status']
    ordering = ['name']
    actions = [publishVideo, unPublishVideo]

admin.site.register(videoItemUpload, formUpload)
admin.site.register(videoItemYoutube, formYoutube)