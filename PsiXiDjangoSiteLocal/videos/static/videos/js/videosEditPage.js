﻿/* Вызов этой функции вставлен в файл px-video.js от AccessibleHTML5VideoPlayer, строка 500:
 * if (typeof (grabTimeValue) !== "undefined") {
 *     grabTimeValue(obj.mins, obj.secs);
 * }
 */
grabTimeValue = function(min, sec) {
    var timeInput = document.getElementById("videoEditBody").getElementsByClassName("timeToScrn");
    var v = min + ":" + sec;
    timeInput[0].value = v;
};
stopVideo = function() {
    var video = document.getElementById("playerBody");
    video.pause();
};