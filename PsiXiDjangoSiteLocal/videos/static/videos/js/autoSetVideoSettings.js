﻿function searching(jsonSearchArray, title) {
    var ignoreWords = [];
    var nums = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
    var specialWords = [];
    var choicesObj = {
        "style": {},
        "clan": {},
        "player": {},
        "map": {},
        "part": {}
    };
    /* Разбор фильтров */
    function specialWordsFunc(ignoreWords, specialWords, nums) {
        var ignoreWords = ignoreWords.join();
        var specialWords = specialWords.join();
        var ignoreWords = ignoreWords.split(",");
        var specialWords = specialWords.split(",");
        var ignoreWords = ignoreWords.concat(nums).concat(specialWords);
        return ignoreWords, specialWords;
    };
    /* Получение json объекта и его подготовка */
    function sorting() {
        for (var i = 0; i < jsonSearchArray.length; i++) {
            var key = jsonSearchArray[i].pk;
            var value = jsonSearchArray[i].fields.tags;
            var value1 = jsonSearchArray[i].fields.ignoredSymbols;
            var value2 = jsonSearchArray[i].fields.specialSymbols;
            var model = jsonSearchArray[i].model;
            if (value && value !== "") {
                if (model === "libraryTHPS.stylesship") {
                    choicesObj["style"][key] = value;
                } else if (model === "libraryTHPS.clansship") {
                    choicesObj["clan"][key] = value;
                } else if (model === "libraryTHPS.playersship") {
                    choicesObj["player"][key] = value;
                } else if (model === "libraryTHPS.mapsship") {
                    choicesObj["map"][key] = value;
                } else if (model === "libraryTHPS.seriesship") {
                    choicesObj["part"][key] = value;
                }
            } else {
                if (model === "libraryTHPS.searchinghelper") {
                    value1 !== "" ? ignoreWords.push(value1) : 0;
                    value2 !== "" ? specialWords.push(value2) : 0;
                }
            }
        }
        function preparation() {
            var keys = Object.keys(choicesObj);
            for (var i = 0; i < keys.length; i++) {
                var keys1 = Object.keys(choicesObj[keys[i]]);
                for (var r = 0; r < keys1.length; r++) {
                    choicesObj[keys[i]][keys1[r]] = choicesObj[keys[i]][keys1[r]].toLowerCase().split(",");
                }
            }
        };
        specialWordsFunc(ignoreWords, specialWords, nums);
        preparation();
    };
    sorting();
    /* Подготовка title от видоса */
    var titleArr = [];
    function sorting1() {
        titleArr = title.split(/\W/);
        function preparation1() {
            for (var i = 0; i < titleArr.length; i++) {
                if (titleArr[i] === "") {
                    titleArr.splice(i, 1);
                    i--;
                }
                titleArr[i] = titleArr[i].toLowerCase();
            }
            searchingNumbers();
            delIgnoreWords();
        };
        function delIgnoreWords() {
            for (var i = 0; i < titleArr.length; i++) {
                for (var w = 0; w < ignoreWords.length; w++) {
                    if (titleArr[i] === ignoreWords[w]) {
                        titleArr.splice(i, 1);
                        i--;
                    }
                }
            }
        };
        function searchingNumbers() {
            var newTitleElements = []; // Разбивка слитых цифер и букв
            var newTitleElements1 = [];
            var newTitleSpecial = [];
            for (var i = 0; i < titleArr.length; i++) {
                var word = titleArr[i];
                var index0 = -1; // Начальный индекс
                var index = 0; // Конечный индекс
                var wordIndex = -1; // Номер слова в массиве
                for (var t = 0; t < word.length; t++) {
                    for (var n = 0; n < nums.length; n++) {
                        if (word[t] === nums[n]) {
                            index0 === -1 ? index0 = t : 0;
                            var index = t + 1;
                            var wordIndex = i;
                            /* */
                            if (i > 0) {
                                newTitleElements1.push(titleArr[i - 1] + word);
                            }
                            if (i < titleArr.length - 1) {
                                newTitleElements1.push(word + titleArr[i + 1]);
                            }
                        }
                    }
                }
                if (wordIndex !== -1) {
                    var finded = titleArr[wordIndex];
                    var half1 = finded.slice(0, index0);
                    var half2 = finded.slice(index, finded.length);
                    half1 !== "" ? newTitleElements.push(half1) : 0;
                    half2 !== "" ? newTitleElements.push(half2) : 0;
                }
                for (var n = 0; n < specialWords.length; n++) {
                    if (i > 0) {
                        newTitleSpecial.push(titleArr[i - 1] + word);
                    }
                    if (i < specialWords.length - 1) {
                        newTitleSpecial.push(word + titleArr[i + 1]);
                    }
                }
            }
            titleArr = newTitleElements.length > 0 ? titleArr.concat(newTitleElements) : titleArr; // Разбитые
            titleArr = newTitleElements1.length > 0 ? titleArr.concat(newTitleElements1) : titleArr; // Слитые
            titleArr = newTitleSpecial.length > 0 ? titleArr.concat(newTitleSpecial) : 0;
        };
        preparation1();
    };
    sorting1();
    /* Выполнение поиска совпадений */
    function relation() {
        var matches = [];
        var p = 0;
        var keys = Object.keys(choicesObj);
        for (var i = 0; i < keys.length; i++) {
            var keys1 = Object.keys(choicesObj[keys[i]]);
            for (var v = 0; v < keys1.length; v++) {
                for (var q = 0; q < titleArr.length; q++) {
                    for (var t = 0; t < choicesObj[keys[i]][keys1[v]].length; t++) {
                        if (choicesObj[keys[i]][keys1[v]][t] === titleArr[q]) {
                            matches[p] = {
                                [keys[i]]: keys1[v]
                            };
                            p++;
                        }
                    }
                }
            }
        }
        /* Установка настроек */
        function setSettings() {
            function setFirstSettings(matches) {
                for (var i = 0; i < matches.length; i++) {
                    var key = Object.keys(matches[i])[0];
                    var val = matches[i][key];
                    var elements = document.getElementsByClassName(key);
                    elements[0].value = val;
                }
                var element = document.getElementById("id_status");
                element.value = "p";
            };
            setFirstSettings(matches);
            /* Ниже - функции дополнительного определения */
            function settingsClan() {
                var isClan = false;
                for (var i = 0; i < matches.length; i++) {
                    isClan === false ? isClan = Object.keys(matches[i])[0] === "clan" ? true : false : true;
                }
                function setClanIfFinded() {
                    var matchesNew = [];
                    for (var t = 0; t < matches.length; t++) {
                        if (Object.keys(matches[t])[0] === "player") {
                            for (var i = 0; i < jsonSearchArray.length; i++) {
                                if (matches[t][Object.keys(matches[t])[0]] === jsonSearchArray[i].pk) {
                                    if (jsonSearchArray[i].fields.playerIn && jsonSearchArray[i].fields.playerIn !== "") {
                                        matchesNew[0] = {
                                            ["clan"]: jsonSearchArray[i].fields.playerIn
                                        }
                                    }
                                }
                            }
                        }
                    }
                    callSettings(matchesNew);
                };
                function callSettings(matchesNew) {
                    var matches = [];
                    var matches = matchesNew;
                    setFirstSettings(matches);
                };
                isClan === false ? setClanIfFinded() : 0;
            };
            settingsClan();
        };
        setSettings();
    };
    relation();
};