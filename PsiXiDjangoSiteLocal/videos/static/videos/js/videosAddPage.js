﻿$(document).ready(function () {
    $("#video .helpButtons .showDetails").clickToggle(function() {
                $("#video form table tbody tr:not(:nth-child(1))").show("slideDown");
                $(this).val("Скрыть детали");
            }, function() {
                $("#video form table tbody tr:not(:nth-child(1))").hide("slideTop");
                $(this).val("Показать детали");
    });
});
resetAll = function () {
    var elements = $("#video td input").add("#video td select").add("#video td textarea");
    for (var i = 0; i < elements.length - 1; i++) {
        $(elements).val("");
    }
    $("#video .addButton, #video #shortInfo").css('display', 'none');
};