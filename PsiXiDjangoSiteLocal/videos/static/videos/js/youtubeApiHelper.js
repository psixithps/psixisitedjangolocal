﻿// Ключ API для google проекта - PsiXi-THPS(psixi-thps);  AIzaSyCSuVQhNqYtBxmLD-lEb3adBtmaXnuO0dU
getValue = function(val) {
    videoId = val.slice(val.indexOf("=") + 1, val.length);
    loadInfo(videoId);
};
var loadInfo = function() {
    var ytApiKey = "AIzaSyCSuVQhNqYtBxmLD-lEb3adBtmaXnuO0dU";
    $.get("https://www.googleapis.com/youtube/v3/videos?part=snippet&id=" + videoId + "&key=" + ytApiKey, function (data) {
        if (typeof (data.items[0]) !== "undefined") {
            var title = data.items[0].snippet.title;
            $(".nameToNewVideo").val(title);
            $(".descriptionToNewVideo").val(data.items[0].snippet.description)
            $("#shortInfo p.title").html(title);
            $("#shortInfo img.thumb").attr("src", data.items[0].snippet.thumbnails.high.url);
            $("#shortInfo small.desc").html(data.items[0].snippet.description);
            $("#video .addButton, #video #shortInfo").css('display', 'block');
            searching(jsonSearchArray, title);
        } else {
            $("#video .addButton, #video #shortInfo").css('display', 'none');
        }
    });
};