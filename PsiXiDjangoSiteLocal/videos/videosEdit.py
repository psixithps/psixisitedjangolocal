import os
from django.conf import settings
from django.shortcuts import render, redirect
from videos.models import videoItemUpload, videoItemYoutube
from videos.forms import editVideoPicture
from django.http import HttpResponse
from django.core.paginator import Paginator
from moviepy.video.io.VideoFileClip import VideoFileClip
from videos.ImagekitVideosThumbs import ThumbnailBig, ThumbnailMiddle, ThumbnailMiniLarge, ThumbnailSmall

def videoEditing(request, videoId, typeOfEditing):
    currentVideo = videoItemUpload.objects.all().get(id = videoId)
    currentVideoUrl = currentVideo.videoOriginalFile.url
    if typeOfEditing == "image":
        if request.method == "POST":
            videoObject = VideoFileClip(os.path.normpath("{0}{1}".format(settings.BASE_DIR, currentVideoUrl)))
            videoLength = round(videoObject.duration)
            inMinVideoMin = videoLength // 60
            inMinVideoSec = videoLength % 60
            if len(str(inMinVideoMin)) > 1 and len(str(inMinVideoSec)) > 1:
                inMinVideoStr = "{0}:{1}".format(inMinVideoMin, inMinVideoSec)
            else:
                if len(str(inMinVideoMin)) == 1 and len(str(inMinVideoSec)) > 1:
                    inMinVideoStr = "0{0}:{1}".format(inMinVideoMin, inMinVideoSec)
                elif len(str(inMinVideoSec)) == 1 and len(str(inMinVideoMin)) > 1:
                    inMinVideoStr = "{0}:0{1}".format(inMinVideoMin, inMinVideoSec)
                else:
                    inMinVideoStr = "0{0}:0{1}".format(inMinVideoMin, inMinVideoSec)
            timeValue = request.POST.get("time")
            if timeValue != "":
                if timeValue.find(":") != -1:
                    timeArr = timeValue.split(":")
                    min = timeArr[0]
                    sec = timeArr[1]
                    if min.isdigit() and sec.isdigit():
                        timePosition = (int(min) * 60) + int(sec)
                    else:
                        timePosition = "badValue1"
                else:
                    timePosition = "badValue0"
            else:
                timePosition = "empty"
            form = editVideoPicture(request.POST, duration = videoLength, scrnMoment = timePosition,
                                    inMinPosition = timeValue, inMinDuration = inMinVideoStr)
            if form.is_valid() and str(timePosition).isdigit():
                # Создание путей
                pathOriginal = currentVideo.videoImgOriginal
                pathBig = currentVideo.videoImgBig
                pathMiddle = currentVideo.videoImgMiddle
                pathSmall = currentVideo.videoImgSmall
                pathMiniLarge = currentVideo.videoImgMiniLarge
                #
                imageOriginal = os.path.normpath("{0}{1}{2}".format(settings.BASE_DIR, settings.MEDIA_URL, pathOriginal))
                imageBig = os.path.normpath("{0}{1}{2}".format(settings.BASE_DIR, settings.MEDIA_URL, pathBig))
                imageMiddle = os.path.normpath("{0}{1}{2}".format(settings.BASE_DIR, settings.MEDIA_URL, pathMiddle))
                imageSmall = os.path.normpath("{0}{1}{2}".format(settings.BASE_DIR, settings.MEDIA_URL, pathSmall))
                imageMiniLarge = os.path.normpath("{0}{1}{2}".format(settings.BASE_DIR, settings.MEDIA_URL, pathMiniLarge))
                # Удаление старых
                if os.path.exists(imageOriginal):
                    os.remove(imageOriginal)
                if os.path.exists(imageBig):
                    os.remove(imageBig)
                if os.path.exists(imageMiddle):
                    os.remove(imageMiddle)
                if os.path.exists(imageSmall):
                    os.remove(imageSmall)
                if os.path.exists(imageMiniLarge):
                    os.remove(imageMiniLarge)
                # Генерация изображений 
                    # Оригинал
                videoObject.save_frame(imageOriginal, t = float(timePosition))
                    # Big
                originalImage = open(imageOriginal, 'rb')
                newGenerate = ThumbnailBig(source = originalImage)
                result = newGenerate.generate()
                newImage = open(imageBig, 'wb')
                newImage.write(result.read())
                originalImage.close()
                newImage.close()
                    # Middle
                originalImage = open(imageOriginal, 'rb')
                newGenerate = ThumbnailMiddle(source = originalImage)
                result = newGenerate.generate()
                newImage = open(imageMiddle, 'wb')
                newImage.write(result.read())
                originalImage.close()
                newImage.close()
                    # Small
                originalImage = open(imageOriginal, 'rb')
                newGenerate = ThumbnailSmall(source = originalImage)
                result = newGenerate.generate()
                newImage = open(imageSmall, 'wb')
                newImage.write(result.read())
                originalImage.close()
                newImage.close()
                    # Mini - Large
                originalImage = open(imageOriginal, 'rb')
                newGenerate = ThumbnailMiniLarge(source = originalImage)
                result = newGenerate.generate()
                newImage = open(imageMiniLarge, 'wb')
                newImage.write(result.read())
                originalImage.close()
                newImage.close()
                #
                return redirect("playVideo", videoId)
        else:
            form = editVideoPicture()
    elif typeOfEditing == "detail":
        pass
    return render(request, 'videos/videosEdit.html', {'form': form, 'videoFile': currentVideoUrl})