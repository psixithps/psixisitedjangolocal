from django import template

from django.template.defaultfilters import stringfilter

register = template.Library()

@stringfilter

def getVideoFormat(videoUrl):
    arr = videoUrl.split('.')
    formatVideo = arr[len(arr) - 1]
    return formatVideo

register.filter('getVideoFormat', getVideoFormat)
