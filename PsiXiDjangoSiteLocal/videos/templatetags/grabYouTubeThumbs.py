import urllib
from urllib.parse import urlparse
from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@stringfilter

def youtubeThumbs(valUrl, args):
    '''Входящий атрибут: s - small, l - large, m - small(другое изображение);
    q - small(другое изображение), d - default(хз, похоже на автовыбор);
    md - mqdefault(как default, только широкоформатно);
    hd - hqdefault(максимальное разрешение)'''
    qs = valUrl.split('?')
    videoId = urllib.parse.parse_qs(qs[1])['v'][0]

    if args == 's':
        return "https://img.youtube.com/vi/%s/2.jpg" % videoId
    elif args == 'l':
        return "https://img.youtube.com/vi/%s/0.jpg" % videoId
    elif args == 'm':
        return "https://img.youtube.com/vi/%s/3.jpg" % videoId
    elif args == 'q':
        return "https://img.youtube.com/vi/%s/1.jpg" % videoId
    elif args == 'd':
        return "https://img.youtube.com/vi/%s/default.jpg" % videoId
    elif args == 'md':
        return "https://img.youtube.com/vi/%s/mqdefault.jpg" % videoId
    elif args == 'hq':
        return "https://img.youtube.com/vi/%s/hqdefault.jpg" % videoId
    else:
        return None

register.filter('youtubeThumbs', youtubeThumbs)