from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@stringfilter

def youtubeLink(defUrl):
    arr = defUrl.split('?')
    correctUrl = arr[1][2::1]
    return correctUrl

register.filter('youtubeLink', youtubeLink)