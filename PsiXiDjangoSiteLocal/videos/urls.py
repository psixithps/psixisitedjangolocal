from django.conf.urls import url
from videos.views import videosMain, videosAll, videoPlay
from videos.videosEdit import videoEditing
from videos.videosAdd import choiseVideoAddingType, videoAdding

urlpatterns = [
    url(r'^$', videosMain, name = "videoCentral"),
    url(r'^all$', videosAll, name = "allVideos"),
    url(r'^all/(?P<currentPage>([0-9]+))/$', videosAll, name = "allVideos"),
    url(r'^all/(?P<currentStyle>(\S+))/(?P<currentPage>([0-9]+))/$',
        videosAll, name = "allVideosStyle"),
    url(r'^all/(?P<currentStyle>(\S+))/(?P<currentClan>(\S+))/(?P<currentPage>([0-9]+))/$',
        videosAll, name = "allVideosClan"),
    url(r'^all/(?P<currentStyle>(\S+))/(?P<currentClan>(\S+))/(?P<currentPlayer>(\S+))/(?P<currentPage>([0-9]+))/$',
        videosAll, name = "allVideosPlayer"),
    url(r'^all/(?P<currentStyle>(\S+))/(?P<currentClan>(\S+))/(?P<currentPlayer>(\S+))/(?P<currentMap>(\S+))/(?P<currentPage>([0-9]+))/$',
        videosAll, name = "allVideosMap"),
    url(r'^add$', choiseVideoAddingType, name = "choiseAddVideo"),
    url(r'^add/(?P<typeOfAdding>(upload|youtube))/$', videoAdding, name = "addVideo"),
    url(r'^(?P<typeOfEditing>(detail|image))/(?P<videoId>(.+))/$', videoEditing, name = "editVideo"),
    url(r'^play/(?P<videoId>(.+))/$', videoPlay, name = "playVideo"),
]