import re
from videos.models import videoItemUpload, videoItemYoutube
from libraryTHPS.models import stylesShip, clansMain, playersShip, mapsShip, seriesShip, searchingHelper
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core.paginator import Paginator

def videosMain(request):
    s = str
    s = "videosMain вызвана"
    return HttpResponse(s)

def videoPlay(request, videoId):
    if re.fullmatch(videoId, "^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$"):
        try:
            video = videoItemUpload.objects.get(id = videoId)
        except videoItemUpload.DoesNotExist:
            try:
                video = videoItemYoutube.objects.get(id = videoId)
            except videoItemYoutube.DoesNotExist:
                msg = "Видео не найдено"
                return render(request, "base/404.html", {"msg": msg})
    else:
        msg = "Видео не найдено"
        return render(request, "base/404.html", {"msg": msg})
    typeAdd = video.loadType
    name = video.name
    description = video.description
    if typeAdd == 'l':
        videoLink = video.videoUrl
        image = None
    elif typeAdd == 'u':
        videoLink = video.videoFile.url
        image = video.videoImgOriginal
    style = video.inStyle
    clan = video.byClan
    player = video.byPlayer
    location = video.byMap
    date = video.dateAdd
    added = video.addedBy
    return render(request, 'videos/videosPlay.html', {'name': name, 'desc': description, 'url': videoLink,
                                                      'playerBackground': image,'style': style, 'clan': clan,
                                                      'player': player, 'map': location,'date': date,
                                                      'added': added, 'typeUpload': typeAdd})

def videosAll(request, **pars):
    curStyle = pars.get("currentStyle")
    curClan = pars.get("currentClan")
    curPlayer = pars.get("currentPlayer")
    curLocation = pars.get("currentMap")
    curPage = pars.get("currentPage")
    currentSearchParams = {}
    videoElementUploaded = videoItemUpload.objects.filter(status = "p").order_by('dateAdd')
    videoElementYoutubed = videoItemYoutube.objects.filter(status = "p").order_by('dateAdd')
    ''' Фильтрация и объект с текущими параметрами поиска '''
    if len(pars) > 0:
        if len(pars) == 1:
            urlVideos = "allVideos"
            if curStyle is not None and curStyle != "eachStyle":
                currentSearchParams["style"] = curStyle
            else:
                currentSearchParams["style"] = "eachStyle"
            if curClan is not None and curClan != "eachClan":
                currentSearchParams["clan"] = curClan
            else:
                currentSearchParams["clan"] = "eachClan"
            if curPlayer is not None and curPlayer != "eachPlayer":
                currentSearchParams["player"] = curPlayer
            else:
                currentSearchParams["player"] = "eachPlayer"
            if curLocation is not None and curLocation != "eachMap":
                currentSearchParams["map"] = curLocation
            else:
                currentSearchParams["map"] = "eachMap"
        if curPage is None:
            curPage = 1
        if len(pars) > 1:
            if len(pars) == 2:
                urlVideos = "allVideosStyle"
                if curClan is not None and curClan != "eachClan":
                    currentSearchParams["clan"] = curClan
                else:
                    currentSearchParams["clan"] = "eachClan"
                if curClan is not None and curClan != "eachClan":
                    currentSearchParams["clan"] = curClan
                else:
                    currentSearchParams["clan"] = "eachClan"
                if curPlayer is not None and curPlayer != "eachPlayer":
                    currentSearchParams["player"] = curPlayer
                else:
                    currentSearchParams["player"] = "eachPlayer"
                if curLocation is not None and curLocation != "eachMap":
                    currentSearchParams["map"] = curLocation
                else:
                    currentSearchParams["map"] = "eachMap"
            currentSearchParams["style"] = curStyle
            if curStyle != "eachStyle":
                videoElementUploaded = videoElementUploaded.filter(inStyle = curStyle)
                videoElementYoutubed = videoElementYoutubed.filter(inStyle = curStyle)
            if len(pars) > 2:
                if len(pars) == 3:
                    urlVideos = "allVideosClan"
                    if curPlayer is not None and curPlayer != "eachPlayer":
                        currentSearchParams["player"] = curPlayer
                    else:
                        currentSearchParams["player"] = "eachPlayer"
                    if curLocation is not None and curLocation != "eachMap":
                        currentSearchParams["map"] = curLocation
                    else:
                        currentSearchParams["map"] = "eachMap"
                currentSearchParams["clan"] = curClan
                if curClan != "eachClan":
                    videoElementUploaded = videoElementUploaded.filter(byClan = curClan)
                    videoElementYoutubed = videoElementYoutubed.filter(byClan = curClan)
                if len(pars) > 3:
                    if len(pars) == 4:
                        urlVideos = "allVideosPlayer"
                        if curLocation is not None and curLocation != "eachMap":
                            currentSearchParams["map"] = curLocation
                        else:
                            currentSearchParams["map"] = "eachMap"
                    currentSearchParams["player"] = curPlayer
                    if curPlayer != "eachPlayer":
                        videoElementUploaded = videoElementUploaded.filter(byPlayer = curPlayer)
                        videoElementYoutubed = videoElementYoutubed.filter(byPlayer = curPlayer)
                    if len(pars) > 4:
                        if len(pars) == 5: 
                            urlVideos = "allVideosMap"
                        currentSearchParams["map"] = curLocation
                        if curLocation != "eachMap":
                            videoElementUploaded = videoElementUploaded.filter(byMap = curLocation)
                            videoElementYoutubed = videoElementYoutubed.filter(byMap = curLocation)
    elif len(pars) == 0:
        curPage = 1
        urlVideos = ""
    curPage = int(curPage)
    ''' Сортировка на вывод '''
    videosList = []
    uploadedCount = videoElementUploaded.count()
    ypotubedCount = videoElementYoutubed.count()
    ''' Создание словаря из всех видео для пагинотора ''' 
    for v in range(uploadedCount + ypotubedCount):
        if v < uploadedCount and uploadedCount > 0: 
            videosList.append(videoElementUploaded[v].id)
        if v < ypotubedCount and ypotubedCount > 0:
            videosList.append(videoElementYoutubed[v].id)
    ''' Пагинатор '''
    pageNator = Paginator(videosList, 1, orphans = 1)
    videoByPage = pageNator.page(curPage)
    ''' Создание словаря с видео по страницам '''
    videosObjPage = {}
    for r in range(len(videoByPage)):
        currentId = videoByPage[r]
        if videoElementUploaded.filter(id = currentId).exists():
            curVideo = videoElementUploaded.get(id = currentId)
        elif videoElementYoutubed.filter(id = currentId).exists():
            curVideo = videoElementYoutubed.get(id = currentId)
        if curVideo.loadType == "u":
            videosObjPage[r] = {
                "name": curVideo.name,
                "date": curVideo.dateAdd,
                "style": curVideo.inStyle,
                "clan": curVideo.byClan,
                "url": curVideo.videoFile,
                "smallThumb": curVideo.videoImgMiniLarge,
                "id": curVideo.id,
                "type": curVideo.loadType,
            }
        elif curVideo.loadType == "l":
            videosObjPage[r] = {
                "name": curVideo.name,
                "date": curVideo.dateAdd,
                "style": curVideo.inStyle,
                "clan": curVideo.byClan,
                "url": curVideo.videoUrl,
                "id": curVideo.id,
                "type": curVideo.loadType,
            }
    ''' Составление поисковых параметров для "Фильтр" '''
    styleElement = stylesShip.objects.all()
    clanElement = clansShip.objects.all()
    playerElement = playersShip.objects.all()
    mapElement = mapsShip.objects.all()
    ''' Фильтрация '''
    if curClan is not None and curClan != "eachClan":
        playerElement = playerElement.filter(playerIn = curClan)
    ''' Упорядовачение '''
    if curStyle is not None and curStyle != "eachStyle":
        clanElement.order_by("clanInStyles")
        playerElement.order_by("playerInStyles")
    ''' Соритровка в объект на вывод '''
    style = []
    clan = []
    player = []
    location = []
    searchParams = {}
    for i in range(len(styleElement)):
        style.append(styleElement[i].name)
    for i in range(len(clanElement)):
        clan.append(clanElement[i].name)
    for i in range(len(playerElement)):
        player.append(playerElement[i].name)
    for i in range(len(mapElement)):
        location.append(mapElement[i].name)
    searchParams["styles"] = style
    searchParams["clans"] = clan
    searchParams["players"] = player
    searchParams["maps"] = location
    return render(request, 'videos/videosAll.html', {'videos': videosObjPage, "page": videoByPage, "urlLevel": urlVideos,
                                                     "searchNames": searchParams, "curSearchNames": currentSearchParams})