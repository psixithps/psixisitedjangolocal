import uuid
from django.db import models
from base.models import STATUS
from psixiSocial.models import mainUserProfile
from libraryTHPS.models import stylesShip, clansMain, playersShip, mapsShip, seriesShip

addingType = (
    ('l', 'Подключение по ссылке на видео из youtube'),
    ('u', 'Загрузка видео на дисковое пространство'),
    )

class videoItemUpload(models.Model):
    name = models.CharField(max_length = 100, unique = True, blank = False, default = "")
    videoFile = models.FileField(max_length = 200, blank = False, upload_to = "videos/originals/")
    videoImgOriginal = models.CharField(max_length = 300, blank = True)
    videoImgBig = models.CharField(max_length = 300, blank = True)
    videoImgMiddle = models.CharField(max_length = 300, blank = True)
    videoImgSmall = models.CharField(max_length = 300, blank = True)
    videoImgMiniLarge = models.CharField(max_length = 300, blank = True)
    id = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False)
    description = models.TextField(max_length = 500, blank = True)
    dateAdd = models.DateTimeField(auto_now_add = True)
    negativeRate = models.IntegerField(blank = False, default = 0)
    positiveRate = models.IntegerField(blank = False, default = 0)
    countWiews = models.PositiveIntegerField(blank = False, default = 0)
    atPart = models.ForeignKey('libraryTHPS.seriesship', blank = True, on_delete = models.SET_NULL, null = True)
    inStyle = models.ForeignKey('libraryTHPS.stylesShip', blank = True, on_delete = models.SET_NULL, null = True)
    byClan = models.ForeignKey('libraryTHPS.clansMain', blank = True, on_delete = models.SET_NULL, null = True)
    byPlayer = models.ForeignKey('libraryTHPS.playersShip', blank = True, on_delete = models.SET_NULL, null = True)
    byMap = models.ForeignKey('libraryTHPS.mapsShip', blank = True, on_delete = models.SET_NULL, null = True)
    addedBy = models.ForeignKey('psixiSocial.mainUserProfile', swappable = True, blank = True, on_delete = models.SET_NULL, null = True)
    status = models.CharField(max_length = 1, blank = False, choices = STATUS, default = "")
    loadType = models.CharField(max_length = 1, choices = addingType, editable = False, blank = False, default = "u")

    class Meta:
        permissions = (
            ("can_see", "Может видеть"),
            ("can_seeUnpublish", "Может видеть неопубликованное"),
            ("can_publish", "Может публиковать"),
            ("can_unUnpublish", "Может снимать с публикации"),    
        )

    def __str__(self):
        return self.name

class videoItemYoutube(models.Model):
    name = models.CharField(max_length = 100, unique = True, blank = False, default = "")
    videoUrl = models.URLField(max_length = 200, unique = True, blank = False, default = "")
    id = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False)
    description = models.TextField(max_length = 500, blank = True)
    dateAdd = models.DateTimeField(auto_now_add = True)
    negativeRate = models.IntegerField(blank = False, default = 0)
    positiveRate = models.IntegerField(blank = False, default = 0)
    countWiews = models.PositiveIntegerField(blank = False, default = 0)
    atPart = models.ForeignKey('libraryTHPS.seriesship', blank = True, on_delete = models.SET_NULL, null = True)
    inStyle = models.ForeignKey('libraryTHPS.stylesShip', blank = True, on_delete = models.SET_NULL, null = True)
    byClan = models.ForeignKey('libraryTHPS.clansMain', blank = True, on_delete = models.SET_NULL, null = True)
    byPlayer = models.ForeignKey('libraryTHPS.playersShip', blank = True, on_delete = models.SET_NULL, null = True)
    byMap = models.ForeignKey('libraryTHPS.mapsShip', blank = True, on_delete = models.SET_NULL, null = True)
    addedBy = models.ForeignKey('psixiSocial.mainUserProfile', swappable = True, blank = True, on_delete = models.SET_NULL, null = True) # Абстрактная модель User тоже в кавычках
    status = models.CharField(max_length = 1, blank = False, choices = STATUS, default = "")
    loadType = models.CharField(max_length = 1, choices = addingType, editable = False, blank = False, default = "l")

    class Meta:
        permissions = (
            ("can_see", "Может видеть"),
            ("can_seeUnpublish", "Может видеть неопубликованное"),
            ("can_publish", "Может публиковать"),
            ("can_unUnpublish", "Может снимать с публикации"),    
        )

    def __str__(self):
        return self.name

'''class subjectVideoMessage(models.Model):
    theme = models.CharField(max_length = 30, blank = False)
    subjectMatter = models.CharField(max_length = 300, blank = False)
    addedBy = models.ForeignKey('psixiSocial.mainUserProfile', swappable = True, related_name = 'psixiSocial.mainUserProfile+', blank = True, on_delete = models.SET_NULL, null = True)
    editedBy = models.ForeignKey('psixiSocial.mainUserProfile', swappable = True, related_name = 'psixiSocial.mainUserProfile+', blank = True, on_delete = models.SET_NULL, null = True, default = None)
    additAt = models.DateTimeField(auto_now_add = True)
    editedAt = models.DateTimeField(auto_now = True)

    def __str__(self):
        return self.theme

class videoRemark(models.Model):
    videoId = models.OneToOneField(videoItem, blank = False, on_delete = models.CASCADE, primary_key = True, default = None)
    subject = models.ForeignKey(subjectVideoMessage)
    text = models.CharField(max_length = 150, blank = True)
    addedBy = models.ForeignKey('psixiSocial.mainUserProfile', swappable = True, blank = False, on_delete = models.PROTECT, null = True)
    at = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.videoId'''