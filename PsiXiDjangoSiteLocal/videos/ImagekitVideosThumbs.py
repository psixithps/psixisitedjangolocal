from imagekit import ImageSpec
from imagekit.processors import ResizeToFill

class ThumbnailBig(ImageSpec):
    processors = [ResizeToFill(350, 350)]
    format = 'PNG'
    options = {'quality': 85}

class ThumbnailMiddle(ImageSpec):
    processors = [ResizeToFill(250, 250)]
    format = 'PNG'
    options = {'quality': 70}

class ThumbnailSmall(ImageSpec):
    processors = [ResizeToFill(150, 150)]
    format = 'PNG'
    options = {'quality': 55}

class ThumbnailMiniLarge(ImageSpec):
    processors = [ResizeToFill(120, 90)]
    format = 'PNG'
    options = {'quality': 40}