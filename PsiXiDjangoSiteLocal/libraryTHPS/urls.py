from django.conf.urls import url
from libraryTHPS.addFirst import addLibraryItemNew
from libraryTHPS.addClanContent import addLibraryClanItemContent
from libraryTHPS.addClanCrop import addLibraryClanItemCrop
from libraryTHPS.addClanMade import addLibraryClanItemMade, addLibraryClanItemAutoCopyImages
from libraryTHPS.addClanResize import addLibraryClanItemLogoResize
from libraryTHPS.storageToEditRedirect import addLibraryClanSectionRedirect
from libraryTHPS.itemsShow import libraryStoragePage, libraryStorageNoAsync, libraryStorageShow, libraryStorageRedirect
from libraryTHPS.resize import bigBackgroundResize

urlpatterns = [
    url(r'^$', libraryStorageRedirect),
    url(r'^loadBackground/$', bigBackgroundResize, name = "getBigClanImage"),
    url(r'^(?P<offJS>(noscripts))/$', libraryStorageRedirect, name = "storageNoJSRedirectUrl"),
    url(r'^(?P<typeOfItems>(clans|players))/$', libraryStorageRedirect, name = "storageUrl"),
    url(r'^(?P<typeOfItems>(clans|players))/(?P<page>([0-9]+))/$', libraryStoragePage, name = "storagePageUrl"),
    url(r'^(?P<typeOfItems>(clans|players))/(?P<ordering>(.*))/(?P<page>([0-9]+))/$', libraryStoragePage, name = "storageOrderingPageUrl"),
    #
    url(r'^edit/clan/(?P<item>[-\w]+)/(?P<editType>miniature|headpiece|content|auto)/$', addLibraryClanSectionRedirect, name = "libraryAddClanSections"),
    url(r'^(?P<typeOfItems>(clans|players))/(?P<offJS>(noscripts))/$', libraryStorageRedirect, name = "storageNoJSUrl"),
    url(r'^(?P<typeOfItems>(clans|players))/(?P<page>([0-9]+))/(?P<offJS>(noscripts))/$', libraryStorageNoAsync, name = "storagePageNoJSUrl"),
    url(r'^(?P<typeOfItems>(clans|players))/(?P<ordering>(.*))/(?P<page>([0-9]+))/(?P<offJS>(noscripts))/$', libraryStorageNoAsync, name = "storageOrderingPageNoJSUrl"),
    url(r'^(?P<typeOfItem>clan|player)/(?P<item>[-\w]+)/$', libraryStorageShow, name = "storageShowItemUrl"),
    url(r'^add/(?P<typeOfItem>(clan|player))/$', addLibraryItemNew, name = "libraryAddFirstUrl"),
    url(r'^edit/clan/(?P<item>[-\w]+)/(?P<step>name|detail|ground|logo)/$', addLibraryClanItemContent, name = "libraryAddClanContentUrl"),
    url(r'^edit/clan/(?P<item>[-\w]+)/(?P<section>miniature|headpiece|)(?P<urlSep>/|)(?P<step>(ground|logo))/crop/$', addLibraryClanItemCrop, name = "libraryAddClanCropUrl"),
    url(r'^edit/clan/(?P<item>[-\w]+)/(?P<section>miniature|headpiece|)(?P<urlSep>/|)(?P<step>ground|logo)/resize/$', addLibraryClanItemLogoResize, name = "libraryAddClanResizeLogoUrl"),
    url(r'^edit/clan/(?P<item>[-\w]+)/(?P<section>miniature|headpiece|)(?P<urlSep>/|)made/$', addLibraryClanItemMade, name = "libraryAddClanMadeUrl"),
    #url(r'^edit/clan/(?P<item>[-\w]+)/(?P<section>miniature|headpiece|)(?P<urlSep>/|)watch/$', addLibraryClanItemWatch, name = "libraryAddClanWatchUrl"),
    # Дальше функции автоматической вставки/замены
    url(r'^service/images/(?P<item>[-\w]+)/(?P<section>miniature|headpiece|both)/(?P<typeOfImage>ground|logo|both)/$', addLibraryClanItemAutoCopyImages, name = "libraryAddClanAutoCopyImagesUrl"),
]