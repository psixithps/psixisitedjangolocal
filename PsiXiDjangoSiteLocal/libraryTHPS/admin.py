from django.contrib import admin
from libraryTHPS.models import clansMain, clansThumbAttrs, clansItemAttrs
from libraryTHPS.models import stylesShip, playersShip, mapsShip, seriesShip, searchingHelper, \
	libraryClanThumbService, libraryClanThumbDefaultService, libraryClanPageService, libraryClanPageDefaultService

admin.site.register(searchingHelper)
admin.site.register(seriesShip)
admin.site.register(stylesShip)
admin.site.register(clansMain)
admin.site.register(clansThumbAttrs)
admin.site.register(clansItemAttrs)
admin.site.register(playersShip)
admin.site.register(mapsShip)
admin.site.register(libraryClanPageService)
admin.site.register(libraryClanPageDefaultService)
admin.site.register(libraryClanThumbService)
admin.site.register(libraryClanThumbDefaultService)
