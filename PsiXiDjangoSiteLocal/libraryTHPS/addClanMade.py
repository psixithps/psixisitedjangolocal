import json
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.conf import settings
from libraryTHPS.models import libraryClanThumbDefaultService, libraryClanThumbService, clansMain, clansItemAttrs, clansThumbAttrs
from libraryTHPS.forms import addClanFirst, addClanDetail
from libraryTHPS.forms import addBackground, addLogo, made
from libraryTHPS.forms import addPlayerFirst, addPlayerDetail
from libraryTHPS.helpers import filterFutureSteps, buildEditClanMenu
from base.helpers import checkCSRF
from django.urls import reverse
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string as rts
from django.contrib import messages
from django.db import transaction
from random import randint

def addLibraryClanItemMade(request, item, section, urlSep):
    mainItem = get_object_or_404(clansMain, pk = item)
    if request.method == "GET":
        if section == "miniature":
            if mainItem.background == False:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'ground'),
                                'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'made'),
                                'csrf': '',
                                'form': addBackground(),
                                'messages': ("Вы не можете начать создавать миниатюру, сначала загрузите изображение фона вашего клана. \
                                Чтобы продолжить, выберите изображение для формы ниже.",),
                                "fon0": static("libraryTHPS/img/17db.jpg"),
                                "fon1": static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/content.html"),
                        "templateBody": rts("libraryTHPS/body/add/image.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'ground'),
                            'csrf': '',
                            'form': addBackground(),
                            'messages': ("Вы не можете начать создавать миниатюру, сначала загрузите изображение фона вашего клана. \
                            Чтобы продолжить, выберите изображение для формы ниже.",)
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                        'menu': buildEditClanMenu(filterFutureSteps(item = mainItem), 'content', 'ground', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'made'),
                        "titl": "",
                        "url": reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'ground'}),
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                messages.add_message(request, messages.WARNING,
                                     "Вы не можете начать создавать миниатюру, сначала загрузите изображение фона вашего клана. \
                                     Чтобы продолжить, выберите изображение для формы ниже."
                                     )
                return redirect("libraryAddClanContentUrl", item, 'ground')
            if mainItem.logo == False:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'logo'),
                                'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'made'),
                                'csrf': '',
                                'form': addLogo(),
                                'messages': ("Вы не можете начать создавать миниатюру, сначала загрузите изображение логотипа вашего клана, \
                                или вырежьте логотип из фона. \ Чтобы продолжить, выберите изображение для формы ниже.",),
                                "fon0": static("libraryTHPS/img/17db.jpg"),
                                "fon1": static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/content.html"),
                        "templateBody": rts("libraryTHPS/body/add/image.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'logo'),
                            'csrf': '',
                            'form': addLogo(),
                            'messages': ("Вы не можете начать создавать миниатюру, сначала загрузите изображение логотипа вашего клана, \
                            или вырежьте логотип из фона. \ Чтобы продолжить, выберите изображение для формы ниже.",)
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                        'menu': buildEditClanMenu(filterFutureSteps(item = mainItem), 'content', 'logo', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'made'),
                        "titl": "",
                        "url": reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'logo'}),
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                messages.add_message(request, messages.WARNING,
                                     "Вы не можете начать создавать миниатюру, сначала загрузите изображение логотипа вашего клана, \
                                     или вырежьте логотип из фона. \ Чтобы продолжить, выберите изображение для формы ниже."
                                     )
                return redirect("libraryAddClanContentUrl", item, 'logo')
            try:
                thumb = clansThumbAttrs.objects.get(clanItem = mainItem.pk)
            except clansThumbAttrs.DoesNotExist:
                thumb = None
            if thumb is None:
                pass
            if thumb.backgroundCrop == False and thumb.logoCrop == False:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'menu': rts("libraryTHPS/libraryTHPSmenu.html"),
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',

                                }),
                                'url': request.path,
                                'titl': ''
                            })})
                    pass
                pass
            if thumb.backgroundCrop == False:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'menu': rts("libraryTHPS/libraryTHPSmenu.html"),
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',

                                }),
                                'url': request.path,
                                'titl': ''
                            })})
                    pass
                pass
            if thumb.logoCrop == False:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'menu': rts("libraryTHPS/libraryTHPSmenu.html"),
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',

                                }),
                                'url': request.path,
                                'titl': ''
                            })})
                    pass
                pass
            media = settings.MEDIA_URL
            if thumb.isMaded == 1:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'made', 'miniature'),
                                'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'made'),
                                "menu": json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb), 'miniature', 'made', item, mainItem.name)),
                                'csrf': '',
                                'form': made(),
                                'madeItems': {
                                    'rootImage': thumb.backgroundCrop.url,
                                    'logo': thumb.logoCrop.url,
                                    'maded': 1
                                    },
                                'initialMadeDict': {
                                    'positionLogo': thumb.logoPosition
                                    },
                                'titl': '',
                                'fon0': static("libraryTHPS/img/17db.jpg"),
                                'fon1': static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/made.html"),
                        "templateBody": rts("libraryTHPS/body/add/made.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'made', 'miniature'),
                            'csrf': '',
                            'form': made(),
                            'madeItems': {
                                'rootImage': thumb.backgroundCrop.url,
                                'logo': thumb.logoCrop.url,
                                'maded': 1
                                }
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/made.html", {
                            'initialMadeDict': {
                                'positionLogo': thumb.logoPosition
                                }
                            }),
                        "menu": buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb), 'miniature', 'made', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'made'),
                        'titl': '',
                        "url": request.path,
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                return render(request, "libraryTHPS/libraryTHPS.html", {
                    'parentTemplate': 'base/fullPage/base.html',
                    'content': ('libraryTHPS', 'add', 'clan', 'made', 'miniature'),
                    "menu": json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb), 'miniature', 'made', item, mainItem.name)),
                    'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'made'),
                    'csrf': checkCSRF(request),
                    'form': made(),
                    'madeItems': {
                        'rootImage': thumb.backgroundCrop.url,
                        'logo': thumb.logoCrop.url,
                        'maded': 1
                        },
                    'initialMadeDict': {
                        'positionLogo': thumb.logoPosition
                        },
                    'titl': '',
                    'fon0': static("libraryTHPS/img/17db.jpg"),
                    'fon1': static("libraryTHPS/img/noise.gif")
                    })
            if thumb.isMaded == 2:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'made', 'miniature'),
                                'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'made'),
                                "menu": json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb), 'miniature', 'made', item, mainItem.name)),
                                'csrf': '',
                                'form': made(),
                                'madeItems': {
                                    'rootImage': thumb.backgroundCrop.url,
                                    'logo': thumb.logoCrop.url,
                                    'maded': 2
                                    },
                                'initialMadeDict': {
                                    'positionName': thumb.namePosition
                                    },
                                'fon0': static("libraryTHPS/img/17db.jpg"),
                                'fon1': static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/made.html"),
                        "templateBody": rts("libraryTHPS/body/add/made.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'made', 'miniature'),
                            'csrf': '',
                            'form': made(),
                            'madeItems': {
                                'rootImage': thumb.backgroundCrop.url,
                                'logo': thumb.logoCrop.url,
                                'maded': 2
                                }
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/made.html", {
                            'initialMadeDict': {
                                'positionName': thumb.namePosition
                                }
                            }),
                        "menu": buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb), 'miniature', 'made', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'made'),
                        'titl': '',
                        "url": request.path,
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                return render(request, "libraryTHPS/libraryTHPS.html", {
                    'parentTemplate': 'base/fullPage/base.html',
                    'content': ('libraryTHPS', 'add', 'clan', 'made', 'miniature'),
                    "menu": json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb), 'miniature', 'made', item, mainItem.name)),
                    'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'made'),
                    'csrf': checkCSRF(request),
                    'form': made(),
                    'madeItems': {
                        'rootImage': thumb.backgroundCrop.url,
                        'logo': thumb.logoCrop.url,
                        'maded': 2
                        },
                    'initialMadeDict': {
                        'positionName': thumb.namePosition
                        },
                    'titl': '',
                    'fon0': static("libraryTHPS/img/17db.jpg"),
                    'fon1': static("libraryTHPS/img/noise.gif")
                    })
            if thumb.isMaded == 3:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'made', 'miniature'),
                                'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'made'),
                                "menu": json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb), 'miniature', 'made', item, mainItem.name)),
                                'csrf': '',
                                'form': made(),
                                'madeItems': {
                                    'rootImage': thumb.backgroundCrop.url,
                                    'logo': thumb.logoCrop.url,
                                    'maded': 3
                                    },
                                'initialMadeDict': {
                                    'positionLogo': thumb.logoPosition,
                                    'positionName': thumb.namePosition
                                    },
                                'fon0': static("libraryTHPS/img/17db.jpg"),
                                'fon1': static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/made.html"),
                        "templateBody": rts("libraryTHPS/body/add/made.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'made', 'miniature'),
                            'csrf': '',
                            'form': made(),
                            'madeItems': {
                                'rootImage': thumb.backgroundCrop.url,
                                'logo': thumb.logoCrop.url,
                                'maded': 3
                                }
                        }),
                        "templateScripts": rts("libraryTHPS/scripts/add/made.html", {
                            'initialMadeDict': {
                                'positionLogo': thumb.logoPosition,
                                'positionName': thumb.namePosition
                                }
                        }),
                        "menu": buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb), 'miniature', 'made', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'made'),
                        'titl': '',
                        "url": request.path,
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                return render(request, "libraryTHPS/libraryTHPS.html", {
                    'parentTemplate': 'base/fullPage/base.html',
                    'content': ('libraryTHPS', 'add', 'clan', 'made', 'miniature'),
                    "menu": json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb), 'miniature', 'made', item, mainItem.name)),
                    'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'made'),
                    'csrf': checkCSRF(request),
                    'form': made(),
                    'madeItems': {
                        'rootImage': thumb.backgroundCrop.url,
                        'logo': thumb.logoCrop.url,
                        'maded': 3
                        },
                    'initialMadeDict': {
                        'positionLogo': thumb.logoPosition,
                        'positionName': thumb.namePosition
                        },
                    'titl': '',
                    'fon0': static("libraryTHPS/img/17db.jpg"),
                    'fon1': static("libraryTHPS/img/noise.gif")
                    })
            if request.is_ajax():
                if "global" in request.GET:
                    return JsonResponse({"newPage": json.dumps({
                        'url': request.path,
                        'titl': '',
                        'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                            'parentTemplate': 'base/fullPage/emptyParent.html',
                            'content': ('libraryTHPS', 'add', 'clan', 'made', 'miniature'),
                            "menu": json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb), 'miniature', 'made', item, mainItem.name)),
                            'csrf': '',
                            'form': made(),
                            'madeItems': {
                                'rootImage': thumb.backgroundCrop.url,
                                'logo': thumb.logoCrop.url,
                                'maded': 0
                                },
                            'fon0': static("libraryTHPS/img/17db.jpg"),
                            'fon1': static("libraryTHPS/img/noise.gif")
                            })
                        })})
                return JsonResponse({"newPage": json.dumps({
                    "templateHead": rts("libraryTHPS/head/add/made.html"),
                    "templateBody": rts("libraryTHPS/body/add/made.html", {
                        'content': ('libraryTHPS', 'add', 'clan', 'made', 'miniature'),
                        'csrf': '',
                        'form': made(),
                        'madeItems': {
                            'rootImage': thumb.backgroundCrop.url,
                            'logo': thumb.logoCrop.url,
                            'maded': 0
                            }
                        }),
                    "templateScripts": rts("libraryTHPS/scripts/add/made.html"),
                    "menu": buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb), 'miniature', 'made', item, mainItem.name),
                    'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'made'),
                    'titl': '',
                    "url": request.path,
                    "fon0": static("libraryTHPS/img/17db.jpg"),
                    "fon1": static("libraryTHPS/img/noise.gif")
                    })})
            return render(request, "libraryTHPS/libraryTHPS.html", {
                'parentTemplate': 'base/fullPage/base.html',
                'content': ('libraryTHPS', 'add', 'clan', 'made', 'miniature'),
                "menu": json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb), 'miniature', 'made', item, mainItem.name)),
                'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'made'),
                'csrf': checkCSRF(request),
                'form': made(),
                'madeItems': {
                    'rootImage': thumb.backgroundCrop.url,
                    'logo': thumb.logoCrop.url,
                    'maded': 0
                    },
                'titl': '',
                'fon0': static("libraryTHPS/img/17db.jpg"),
                'fon1': static("libraryTHPS/img/noise.gif")
                })
        elif section == "headpiece":
            if mainItem.background == False:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'ground'),
                                'breadcrumbs': ('clans', 'editDetail', 'editHead', 'made'),
                                'csrf': '',
                                'form': addBackground(),
                                'messages': ("Вы не можете начать создавать представление на странице клана, сначала загрузите изображение фона вашего клана. \
                                Чтобы продолжить, выберите изображение для формы ниже.",),
                                "fon0": static("libraryTHPS/img/17db.jpg"),
                                "fon1": static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/content.html"),
                        "templateBody": rts("libraryTHPS/body/add/image.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'ground'),
                            'csrf': '',
                            'form': addBackground(),
                            'messages': ("Вы не можете начать создавать представление на странице клана, сначала загрузите изображение фона вашего клана. \
                            Чтобы продолжить, выберите изображение для формы ниже.",)
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                        "menu": buildEditClanMenu(filterFutureSteps(item = mainItem), 'content', 'ground', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editDetail', 'editHead', 'made'),
                        "titl": "",
                        "url": reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'ground'}),
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                messages.add_message(request, messages.WARNING,
                                     "Вы не можете начать создавать представление на странице клана, сначала загрузите изображение фона вашего клана. \
                                     Чтобы продолжить, выберите изображение для формы ниже."
                                     )
                return redirect("libraryAddClanContentUrl", item, 'ground')
            if mainItem.logo == False:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'logo'),
                                'breadcrumbs': ('clans', 'editDetail', 'editHead', 'made'),
                                'csrf': '',
                                'form': addLogo(),
                                'messages': ("Вы не можете начать создавать представление на странице клана, сначала загрузите изображение логотипа вашего клана, \
                                или вырежьте логотип из фона. \Чтобы продолжить, выберите изображение для формы ниже.",),
                                "fon0": static("libraryTHPS/img/17db.jpg"),
                                "fon1": static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/content.html"),
                        "templateBody": rts("libraryTHPS/body/add/image.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'logo'),
                            'csrf': '',
                            'form': addLogo(),
                            'messages': ("Вы не можете начать создавать представление на странице клана, сначала загрузите изображение логотипа вашего клана, \
                            или вырежьте логотип из фона. \Чтобы продолжить, выберите изображение для формы ниже.",)
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                        "menu": buildEditClanMenu(filterFutureSteps(item = mainItem), 'content', 'logo', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editDetail', 'editHead', 'made'),
                        "titl": "",
                        "url": reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'logo'}),
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                messages.add_message(request, messages.WARNING,
                                     "Вы не можете начать создавать представление на странице клана, сначала загрузите изображение логотипа вашего клана, \
                                     или вырежьте логотип из фона. \Чтобы продолжить, выберите изображение для формы ниже."
                                     )
                return redirect("libraryAddClanContentUrl", item, 'logo')
            try:
                head = clansItemAttrs.objects.get(pk = mainItem.pk)
            except clansItemAttrs.DoesNotExist:
                head = None
            if head is None:
                pass
            if head.backgroundCrop == False and head.logoCrop == False:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'menu': rts("libraryTHPS/libraryTHPSmenu.html"),
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',

                                }),
                                'url': request.path,
                                'titl': ''
                            })})
                    pass
                pass
            if head.backgroundCrop == False:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'menu': rts("libraryTHPS/libraryTHPSmenu.html"),
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',

                                }),
                                'url': request.path,
                                'titl': ''
                            })})
                    pass
                pass
            if head.logoCrop == False:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'menu': rts("libraryTHPS/libraryTHPSmenu.html"),
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',

                                }),
                                'url': request.path,
                                'titl': ''
                            })})
                    pass
                pass
            media = settings.MEDIA_URL
            if head.isMaded == 1:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'made', 'headpiece'),
                                'breadcrumbs': ('clans', 'editDetail', 'editHead', 'made'),
                                "menu": json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'made', item, mainItem.name)),
                                'csrf': '',
                                'form': made(),
                                'madeItems': {
                                    'rootImage': head.backgroundCrop.url,
                                    'logo': head.logoCrop.url,
                                    'maded': 1
                                    },
                                'initialMadeDict': {
                                    'positionLogo': head.logoPosition
                                    },
                                'fon0': static("libraryTHPS/img/17db.jpg"),
                                'fon1': static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/made.html"),
                        "templateBody": rts("libraryTHPS/body/add/made.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'made', 'headpiece'),
                            'csrf': '',
                            'form': made(),
                            'madeItems': {
                                'rootImage': head.backgroundCrop.url,
                                'logo': head.logoCrop.url,
                                'maded': 1
                                }
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/made.html", {
                            'initialMadeDict': {
                                'positionLogo': head.logoPosition
                                }
                        }),
                        "menu": buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'made', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editDetail', 'editHead', 'made'),
                        'titl': '',
                        "url": request.path,
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                return render(request, "libraryTHPS/libraryTHPS.html", {
                    'parentTemplate': 'base/fullPage/base.html',
                    'content': ('libraryTHPS', 'add', 'clan', 'made', 'headpiece'),
                    "menu": json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'made', item, mainItem.name)),
                    'breadcrumbs': ('clans', 'editDetail', 'editHead', 'made'),
                    'csrf': checkCSRF(request),
                    'form': made(),
                    'madeItems': {
                        'rootImage': head.backgroundCrop.url,
                        'logo': head.logoCrop.url,
                        'maded': 1
                        },
                    'initialMadeDict': {
                        'positionLogo': head.logoPosition
                        },
                    'titl': '',
                    'fon0': static("libraryTHPS/img/17db.jpg"),
                    'fon1': static("libraryTHPS/img/noise.gif")
                    })
            if head.isMaded == 2:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'made', 'headpiece'),
                                'breadcrumbs': ('clans', 'editDetail', 'editHead', 'made'),
                                "menu": json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'made', item, mainItem.name)),
                                'csrf': '',
                                'form': made(),
                                'madeItems': {
                                    'rootImage': head.backgroundCrop.url,
                                    'logo': head.logoCrop.url,
                                    'maded': 2
                                    },
                                'initialMadeDict': {
                                    'positionName': head.namePosition
                                    },
                                'fon0': static("libraryTHPS/img/17db.jpg"),
                                'fon1': static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/made.html"),
                        "templateBody": rts("libraryTHPS/body/add/made.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'made', 'headpiece'),
                            'csrf': '',
                            'form': made(),
                            'madeItems': {
                                'rootImage': head.backgroundCrop.url,
                                'logo': head.logoCrop.url,
                                'maded': 2
                                }
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/made.html", {
                            'initialMadeDict': {
                                'positionName': head.namePosition
                                }
                        }),
                        "menu": buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'made', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editDetail', 'editHead', 'made'),
                        'titl': '',
                        "url": request.path,
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                return render(request, "libraryTHPS/libraryTHPS.html", {
                    'parentTemplate': 'base/fullPage/base.html',
                    'content': ('libraryTHPS', 'add', 'clan', 'made', 'headpiece'),
                    "menu": json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'made', item, mainItem.name)),
                    'breadcrumbs': ('clans', 'editDetail', 'editHead', 'made'),
                    'csrf': checkCSRF(request),
                    'form': made(),
                    'madeItems': {
                        'rootImage': head.backgroundCrop.url,
                        'logo': head.logoCrop.url,
                        'maded': 2
                        },
                    'initialMadeDict': {
                        'positionName': head.namePosition
                        },
                    'titl': '',
                    'fon0': static("libraryTHPS/img/17db.jpg"),
                    'fon1': static("libraryTHPS/img/noise.gif")
                    })
            if head.isMaded == 3:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'made', 'headpiece'),
                                'breadcrumbs': ('clans', 'editDetail', 'editHead', 'made'),
                                "menu": json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'made', item, mainItem.name)),
                                'csrf': '',
                                'form': made(),
                                'madeItems': {
                                    'rootImage': head.backgroundCrop.url,
                                    'logo': head.logoCrop.url,
                                    'maded': 3
                                    },
                                'initialMadeDict': {
                                    'positionLogo': head.logoPosition,
                                    'positionName': head.namePosition
                                    },
                                'fon0': static("libraryTHPS/img/17db.jpg"),
                                'fon1': static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/made.html"),
                        "templateBody": rts("libraryTHPS/body/add/made.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'made', 'headpiece'),
                            'csrf': '',
                            'form': made(),
                            'madeItems': {
                                'rootImage': head.backgroundCrop.url,
                                'logo': head.logoCrop.url,
                                'maded': 3
                                }
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/made.html", {
                            'initialMadeDict': {
                                'positionLogo': head.logoPosition,
                                'positionName': head.namePosition
                                }
                            }),
                        "menu": buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'made', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editDetail', 'editHead', 'made'),
                        'titl': '',
                        "url": request.path,
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                return render(request, "libraryTHPS/libraryTHPS.html", {
                    'parentTemplate': 'base/fullPage/base.html',
                    'content': ('libraryTHPS', 'add', 'clan', 'made', 'headpiece'),
                    "menu": json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'made', item, mainItem.name)),
                    'breadcrumbs': ('clans', 'editDetail', 'editHead', 'made'),
                    'csrf': checkCSRF(request),
                    'form': made(),
                    'madeItems': {
                        'rootImage': head.backgroundCrop.url,
                        'logo': head.logoCrop.url,
                        'maded': 3
                        },
                    'initialMadeDict': {
                        'positionLogo': head.logoPosition,
                        'positionName': head.namePosition
                        },
                    'titl': '',
                    'fon0': static("libraryTHPS/img/17db.jpg"),
                    'fon1': static("libraryTHPS/img/noise.gif")
                    })
            if request.is_ajax():
                if "global" in request.GET:
                    return JsonResponse({"newPage": json.dumps({
                        'url': request.path,
                        'titl': '',
                        'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                            'parentTemplate': 'base/fullPage/emptyParent.html',
                            'content': ('libraryTHPS', 'add', 'clan', 'made', 'headpiece'),
                            'breadcrumbs': ('clans', 'editDetail', 'editHead', 'made'),
                            "menu": json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'made', item, mainItem.name)),
                            'csrf': '',
                            'form': made(),
                            'madeItems': {
                                'rootImage': head.backgroundCrop.url,
                                'logo': head.logoCrop.url,
                                'maded': 0
                                },
                            'fon0': static("libraryTHPS/img/17db.jpg"),
                            'fon1': static("libraryTHPS/img/noise.gif")
                            })
                        })})
                return JsonResponse({"newPage": json.dumps({
                    "templateHead": rts("libraryTHPS/head/add/made.html"),
                    "templateBody": rts("libraryTHPS/body/add/made.html", {
                        'content': ('libraryTHPS', 'add', 'clan', 'made', 'headpiece'),
                        'csrf': '',
                        'form': made(),
                        'madeItems': {
                            'rootImage': head.backgroundCrop.url,
                            'logo': head.logoCrop.url,
                            'maded': 0
                            }
                        }),
                    "templateScripts": rts("libraryTHPS/scripts/add/made.html"),
                    "menu": buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'made', item, mainItem.name),
                    'breadcrumbs': ('clans', 'editDetail', 'editHead', 'made'),
                    'titl': '',
                    "url": request.path,
                    "fon0": static("libraryTHPS/img/17db.jpg"),
                    "fon1": static("libraryTHPS/img/noise.gif")
                    })})
            return render(request, "libraryTHPS/libraryTHPS.html", {
                'parentTemplate': 'base/fullPage/base.html',
                'content': ('libraryTHPS', 'add', 'clan', 'made', 'headpiece'),
                "menu": json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'made', item, mainItem.name)),
                'breadcrumbs': ('clans', 'editDetail', 'editHead', 'made'),
                'csrf': checkCSRF(request),
                'form': made(),
                'madeItems': {
                    'rootImage': head.backgroundCrop.url,
                    'logo': head.logoCrop.url,
                    'maded': 0
                    },
                'titl': '',
                'fon0': static("libraryTHPS/img/17db.jpg"),
                'fon1': static("libraryTHPS/img/noise.gif")
                })
        else:
            if mainItem.background == False:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'ground'),
                                'breadcrumbs': ('clans', 'editInfo', 'ground'),
                                'csrf': '',
                                'form': addBackground(),
                                'messages': ("Вы не можете начать создавать миниатюру и представление на странице клана, сначала загрузите изображение фона вашего клана. \
                                Чтобы продолжить, выберите изображение для формы ниже.",),
                                "fon0": static("libraryTHPS/img/17db.jpg"),
                                "fon1": static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/content.html"),
                        "templateBody": rts("libraryTHPS/body/add/image.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'ground'),
                            'csrf': '',
                            'form': addBackground(),
                            'messages': ("Вы не можете начать создавать миниатюру и представление на странице клана, сначала загрузите изображение фона вашего клана. \
                            Чтобы продолжить, выберите изображение для формы ниже.",)
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                        "menu": buildEditClanMenu(filterFutureSteps(item = mainItem), 'content', 'ground', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editInfo', 'ground'),
                        "titl": "",
                        "url": reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'ground'}),
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                messages.add_message(request, messages.WARNING,
                                     "Вы не можете начать создавать миниатюру и представление на странице клана, сначала загрузите изображение фона вашего клана. \
                                     Чтобы продолжить, выберите изображение для формы ниже."
                                     )
                return redirect("libraryAddClanContentUrl", item, 'ground')
            if mainItem.logo == False:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'logo'),
                                'breadcrumbs': ('clans', 'editInfo', 'logo'),
                                'csrf': '',
                                'form': addLogo(),
                                'messages': ("Вы не можете начать создавать миниатюру и представление на странице клана, сначала загрузите изображение логотипа вашего клана, \
                                или вырежьте логотип из фона. \
                                Чтобы продолжить, выберите изображение для формы ниже.",),
                                "fon0": static("libraryTHPS/img/17db.jpg"),
                                "fon1": static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/content.html"),
                        "templateBody": rts("libraryTHPS/body/add/image.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'logo'),
                            'csrf': '',
                            'form': addLogo(),
                            'messages': ("Вы не можете начать создавать миниатюру и представление на странице клана, сначала загрузите изображение логотипа вашего клана, \
                            или вырежьте логотип из фона. \
                            Чтобы продолжить, выберите изображение для формы ниже.",)
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                        "menu": buildEditClanMenu(filterFutureSteps(item = mainItem), 'content', 'logo', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editInfo', 'logo'),
                        "titl": "",
                        "url": reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'logo'}),
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                messages.add_message(request, messages.WARNING,
                                     "Вы не можете начать создавать миниатюру и представление на странице клана, сначала загрузите изображение логотипа вашего клана, \
                                     или вырежьте логотип из фона. \Чтобы продолжить, выберите изображение для формы ниже.",
                                     )
                return redirect("libraryAddClanContentUrl", item, 'logo')
            try:
                thumb = clansThumbAttrs.objects.get(pk = mainItem.pk)
            except clansThumbAttrs.DoesNotExist:
                thumb = None
            try:
                head = clansItemAttrs.objects.get(pk = mainItem.pk)
            except:
                head = None
            if thumb is None and head is None:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'message'),
                                'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                                "menu": json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem), 'both', 'made', item, mainItem.name)),
                                'message': "<h3>Не спеши, %s!</h3>\
                                <p>У клана <a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>%s</a>\
                                , на данный момент, не было создано ни миниатюры, ни представления. А кроме того, на изображениях фона \
                                и логотипа - не были выделены ключевые области.</p>\
                                <p><a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>Определить наиболее важные области сейчас</a></p>\
                                <small>или</small>\
                                <p><input type = 'submit' formaction = '%s' onclick = 'doPostForm(\"%s\");' value = 'оставьте как есть'/>.</p>" % 
                                (request.user.username,
                                 reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}),
                                 mainItem.name,
                                 reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'ground'}),
                                 reverse("libraryAddClanAutoCopyImagesUrl", kwargs = {'item': item, 'section': 'both', 'typeOfImage': 'both'})),
                                 'csrf': '',
                                 "fon0": static("libraryTHPS/img/17db.jpg"),
                                 "fon1": static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/content.html"),
                        "templateBody": rts("libraryTHPS/body/add/message.html", {
                            'message': "<h3>Не спеши, %s!</h3>\
                            <p>У клана <a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>%s</a>\
                            , на данный момент, не было создано ни миниатюры, ни представления. А кроме того, на изображениях фона \
                            и логотипа - не были выделены ключевые области.</p>\
                            <p><a href = '%s' onclick = 'loadContent(this.href);'>Определить наиболее важные области сейчас</a></p>\
                            <small>или</small>\
                            <p><input type = 'submit' formaction = '%s' onclick = 'doPostForm(\"%s\");' value = 'оставьте как есть'/>.</p>" % 
                            (request.user.username,
                            reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}),
                            mainItem.name,
                            reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'ground'}),
                            reverse("libraryAddClanAutoCopyImagesUrl", kwargs = {'item': item, 'section': 'both', 'typeOfImage': 'both'}))
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                        "menu": buildEditClanMenu(filterFutureSteps(item = mainItem), 'both', 'made', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                        "titl": "",
                        "url": request.path,
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                return render(request, "libraryTHPS/libraryTHPS.html", {
                    'parentTemplate': 'base/fullPage/base.html',
                    'content': ('libraryTHPS', 'add', 'clan', 'message'),
                    "menu": json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem), 'both', 'made', item, mainItem.name)),
                    'message': "<h3>Не спеши, %s!</h3>\
                    <p>У клана <a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>%s</a>\
                    , на данный момент, не было создано ни миниатюры, ни представления. А кроме того, на изображениях фона \
                    и логотипа - не были выделены ключевые области.</p>\
                    <p><a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>Определить наиболее важные области сейчас</a></p>\
                    <small>или</small>\
                    <p><input type = 'submit' formaction = '%s' onclick = 'doPostForm(\"%s\");' value = 'оставьте как есть'/>.</p>" % 
                    (request.user.username,
                     reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}),
                     mainItem.name,
                     reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'ground'}),
                     reverse("libraryAddClanAutoCopyImagesUrl", kwargs = {'item': item, 'section': 'both', 'typeOfImage': 'both'})),
                     'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                     'csrf': checkCSRF(request),
                     "fon0": static("libraryTHPS/img/17db.jpg"),
                     "fon1": static("libraryTHPS/img/noise.gif")
                     })
            if thumb is None:
                if head.backgroundCrop == False and head.logoCrop == False:
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'url': request.path,
                                'titl': '',
                                'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                    'parentTemplate': 'base/fullPage/emptyParent.html',
                                    'content': ('libraryTHPS', 'add', 'clan', 'message'),
                                    'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                                    "menu": json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'both', 'made', item, mainItem.name)),
                                    'message': "<h3>Не спеши, %s!</h3>\
                                    <p>У клана <a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>%s</a>\
                                    , на данный момент, имеется представление, миниатюра была удалена или не была создана. А кроме того, на изображениях фона \
                                    и логотипа - не были выделены ключевые области.</p>\
                                    <p><a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>Определить наиболее важные области сейчас</a></p>\
                                    <small>или</small>\
                                    <p><input type = 'submit' formaction = '%s' onclick = 'doPostForm(\"%s\");' value = 'оставьте как есть'/>.</p>" % 
                                    (request.user.username,
                                     reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}),
                                     mainItem.name,
                                     reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'ground'}),
                                     reverse("libraryAddClanAutoCopyImagesUrl", kwargs = {'item': item, 'section': 'both', 'typeOfImage': 'both'})),
                                     'csrf': '',
                                     "fon0": static("libraryTHPS/img/17db.jpg"),
                                     "fon1": static("libraryTHPS/img/noise.gif")
                                    })
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/content.html"),
                            "templateBody": rts("libraryTHPS/body/add/message.html", {
                                'message': "<h3>Не спеши, %s!</h3>\
                                <p>У клана <a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>%s</a>\
                                , на данный момент, имеется представление, миниатюра была удалена или не была создана. А кроме того, на изображениях фона \
                                и логотипа - не были выделены ключевые области.</p>\
                                <p><a href = '%s' onclick = 'loadContent(this.href);'>Определить наиболее важные области сейчас</a></p>\
                                <small>или</small>\
                                <p><input type = 'submit' formaction = '%s' onclick = 'doPostForm(\"%s\");' value = 'оставьте как есть'/>.</p>" % 
                                (request.user.username,
                                 reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}),
                                 mainItem.name,
                                 reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'ground'}),
                                 reverse("libraryAddClanAutoCopyImagesUrl", kwargs = {'item': item, 'section': 'both', 'typeOfImage': 'both'}))
                                 }),
                            "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                            "menu": buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'both', 'made', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                            "titl": "",
                            "url": request.path,
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })})
                    return render(request, "libraryTHPS/libraryTHPS.html", {
                        'parentTemplate': 'base/fullPage/base.html',
                        'content': ('libraryTHPS', 'add', 'clan', 'message'),
                        "menu": json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'both', 'made', item, mainItem.name)),
                        'message': "<h3>Не спеши, %s!</h3>\
                        <p>У клана <a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>%s</a>\
                        , на данный момент, имеется представление, миниатюра была удалена или не была создана. А кроме того, на изображениях фона \
                        и логотипа - не были выделены ключевые области.</p>\
                        <p><a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>Определить наиболее важные области сейчас</a></p>\
                        <small>или</small>\
                        <p><input type = 'submit' formaction = '%s' onclick = 'doPostForm(\"%s\");' value = 'оставьте как есть'/>.</p>" % 
                        (request.user.username,
                         reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}),
                         mainItem.name,
                         reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'ground'}),
                         reverse("libraryAddClanAutoCopyImagesUrl", kwargs = {'item': item, 'section': 'both', 'typeOfImage': 'both'})),
                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                        'csrf': checkCSRF(request),
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })
                if head.backgroundCrop == False:
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'url': request.path,
                                'titl': '',
                                'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                    'parentTemplate': 'base/fullPage/emptyParent.html',
                                    'content': ('libraryTHPS', 'add', 'clan', 'message'),
                                    'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                                    'message': "<h3>Не спеши, %s!</h3>\
                                    <p>У клана <a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>%s</a>\
                                    , на данный момент, имеется представление, миниатюра была удалена или не была создана. А кроме того, на изображении фона \
                                    не была выделена ключевая область.</p>\
                                    <p><a href = '%s' onclick = 'loadContent(this.href);'>Определить наиболее важную область на изображении фона</a></p>\
                                    <small>или</small>\
                                    <p><input type = 'submit' formaction = '%s' onclick = 'doPostForm(\"%s\");' value = 'оставьте как есть'/>.</p>" % 
                                    (request.user.username,
                                     reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}),
                                     mainItem.name,
                                     reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'ground'}),
                                     reverse("libraryAddClanAutoCopyImagesUrl", kwargs = {'item': item, 'section': 'both', 'typeOfImage': 'ground'})),
                                     'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem), 'both', 'made', item, mainItem.name)),
                                     'csrf': '',
                                     "fon0": static("libraryTHPS/img/17db.jpg"),
                                     "fon1": static("libraryTHPS/img/noise.gif")
                                    })
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/content.html"),
                            "templateBody": rts("libraryTHPS/body/add/message.html", {
                                'message': "<h3>Не спеши, %s!</h3>\
                                <p>У клана <a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>%s</a>\
                                , на данный момент, имеется представление, миниатюра была удалена или не была создана. А кроме того, на изображении фона \
                                не была выделена ключевая область.</p>\
                                <p><a href = '%s' onclick = 'loadContent(this.href);'>Определить наиболее важную область на изображении фона</a></p>\
                                <small>или</small>\
                                <p><input type = 'submit' formaction = '%s' onclick = 'doPostForm(\"%s\");' value = 'оставьте как есть'/>.</p>" % 
                                (request.user.username,
                                 reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}),
                                 mainItem.name,
                                 reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'ground'}),
                                 reverse("libraryAddClanAutoCopyImagesUrl", kwargs = {'item': item, 'section': 'both', 'typeOfImage': 'ground'}))
                                 }),
                            "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                            "menu": buildEditClanMenu(filterFutureSteps(item = mainItem), 'both', 'made', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                            "titl": "",
                            "url": request.path,
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })
                                             })
                    return render(request, "libraryTHPS/libraryTHPS.html", {
                        'parentTemplate': 'base/fullPage/base.html',
                        'content': ('libraryTHPS', 'add', 'clan', 'message'),
                        'message': "<h3>Не спеши, %s!</h3>\
                        <p>У клана <a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>%s</a>\
                        , на данный момент, имеется представление, миниатюра была удалена или не была создана. А кроме того, на изображении фона \
                        не была выделена ключевая область.</p>\
                        <p><a href = '%s' onclick = 'loadContent(this.href);'>Определить наиболее важную область на изображении фона</a></p>\
                        <small>или</small>\
                        <p><input type = 'submit' formaction = '%s' onclick = 'doPostForm(\"%s\");' value = 'оставьте как есть'/>.</p>" % 
                        (request.user.username,
                         reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}),
                         mainItem.name,
                         reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'ground'}),
                         reverse("libraryAddClanAutoCopyImagesUrl", kwargs = {'item': item, 'section': 'both', 'typeOfImage': 'ground'})),
                        'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem), 'both', 'made', item, mainItem.name)),
                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                        'csrf': checkCSRF(request),
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })
                if head.logoCrop == False:
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'url': request.path,
                                'titl': '',
                                'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                    'parentTemplate': 'base/fullPage/emptyParent.html',
                                    'content': ('libraryTHPS', 'add', 'clan', 'message'),
                                    'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem), 'both', 'made', item, mainItem.name)),
                                    'message': "<h3>Не спеши, %s!</h3>\
                                    <p>У клана <a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>%s</a>\
                                    , на данный момент, имеется миниатюра, представление было удалено или не было создано. А кроме того, на изображении логотипа \
                                    не была выделена ключевая область.</p>\
                                    <p><a href = '%s' onclick = 'loadContent(this.href);'>Определить наиболее важную область на изображении логотипа</a></p>\
                                    <small>или</small>\
                                    <p><input type = 'submit' formaction = '%s' onclick = 'doPostForm(\"%s\");' value = 'оставьте как есть'/>.</p>" % 
                                    (request.user.username,
                                     reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}),
                                     mainItem.name,
                                     reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'logo'}),
                                     reverse("libraryAddClanAutoCopyImagesUrl", kwargs = {'item': item, 'section': 'both', 'typeOfImage': 'logo'})),
                                     'csrf': '',
                                     "fon0": static("libraryTHPS/img/17db.jpg"),
                                     "fon1": static("libraryTHPS/img/noise.gif")
                                    })
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/content.html"),
                            "templateBody": rts("libraryTHPS/body/add/message.html", {
                                'message': "<h3>Не спеши, %s!</h3>\
                                <p>У клана <a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>%s</a>\
                                , на данный момент, имеется миниатюра, представление было удалено или не было создано. А кроме того, на изображении логотипа \
                                не была выделена ключевая область.</p>\
                                <p><a href = '%s' onclick = 'loadContent(this.href);'>Определить наиболее важную область на изображении логотипа</a></p>\
                                <small>или</small>\
                                <p><input type = 'submit' formaction = '%s' onclick = 'doPostForm(\"%s\");' value = 'оставьте как есть'/>.</p>" % 
                                (request.user.username,
                                 reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}),
                                 mainItem.name,
                                 reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'logo'}),
                                 reverse("libraryAddClanAutoCopyImagesUrl", kwargs = {'item': item, 'section': 'both', 'typeOfImage': 'logo'})),
                                 }),
                            "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                            "menu": buildEditClanMenu(filterFutureSteps(item = mainItem), 'both', 'made', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                            "titl": "",
                            "url": request.path,
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })})
                    return render(request, "libraryTHPS/libraryTHPS.html", {
                        'parentTemplate': 'base/fullPage/base.html',
                        'content': ('libraryTHPS', 'add', 'clan', 'message'),
                        'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem), 'both', 'made', item, mainItem.name)),
                        'message': "<h3>Не спеши, %s!</h3>\
                        <p>У клана <a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>%s</a>\
                        , на данный момент, имеется миниатюра, представление было удалено или не было создано. А кроме того, на изображении логотипа \
                        не была выделена ключевая область.</p>\
                        <p><a href = '%s' onclick = 'loadContent(this.href);'>Определить наиболее важную область на изображении логотипа</a></p>\
                        <small>или</small>\
                        <p><input type = 'submit' formaction = '%s' onclick = 'doPostForm(\"%s\");' value = 'оставьте как есть'/>.</p>" % 
                        (request.user.username,
                         reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}),
                         mainItem.name,
                         reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'logo'}),
                         reverse("libraryAddClanAutoCopyImagesUrl", kwargs = {'item': item, 'section': 'both', 'typeOfImage': 'logo'})),
                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                        'csrf': checkCSRF(request),
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })
            if head is None:
                if thumb.backgroundCrop == False and thumb.logoCrop == False:
                        if request.is_ajax():
                            if "global" in request.GET:
                                return JsonResponse({"newPage": json.dumps({
                                    'url': request.path,
                                    'titl': '',
                                    'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                        'parentTemplate': 'base/fullPage/emptyParent.html',
                                        'content': ('libraryTHPS', 'add', 'clan', 'message'),
                                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                                        'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem), 'both', 'made', item, mainItem.name)),
                                        'message': "<h3>Не спеши, %s!</h3>\
                                        <p>У клана <a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>%s</a>\
                                        , на данный момент, имеется миниатюра, представление было удалено или не было создано. А кроме того, на изображениях фона \
                                        и логотипа не были выделены ключевые области.</p>\
                                        <p><a href = '%s' onclick = 'loadContent(this.href);'>Определить наиболее важные области на изображениях фона и логотипа</a></p>\
                                        <small>или</small>\
                                        <p><input type = 'submit' formaction = '%s' onclick = 'doPostForm(\"%s\");' value = 'оставьте как есть'/>.</p>" % 
                                        (request.user.username,
                                         reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}),
                                         mainItem.name,
                                         reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'ground'}),
                                         reverse("libraryAddClanAutoCopyImagesUrl", kwargs = {'item': item, 'section': 'both', 'typeOfImage': 'both'})),
                                         'csrf': '',
                                         "fon0": static("libraryTHPS/img/17db.jpg"),
                                         "fon1": static("libraryTHPS/img/noise.gif")
                                        })
                                    })})
                            return JsonResponse({"newPage": json.dumps({
                                "templateHead": rts("libraryTHPS/head/add/content.html"),
                                "templateBody": rts("libraryTHPS/body/add/message.html", {
                                    'message': "<h3>Не спеши, %s!</h3>\
                                    <p>У клана <a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>%s</a>\
                                    , на данный момент, имеется миниатюра, представление было удалено или не было создано. А кроме того, на изображениях фона \
                                    и логотипа не были выделены ключевые области.</p>\
                                    <p><a href = '%s' onclick = 'loadContent(this.href);'>Определить наиболее важные области на изображениях фона и логотипа</a></p>\
                                    <small>или</small>\
                                    <p><input type = 'submit' formaction = '%s' onclick = 'doPostForm(\"%s\");' value = 'оставьте как есть'/>.</p>" % 
                                    (request.user.username,
                                     reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}),
                                     mainItem.name,
                                     reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'ground'}),
                                     reverse("libraryAddClanAutoCopyImagesUrl", kwargs = {'item': item, 'section': 'both', 'typeOfImage': 'both'})),
                                    }),
                                "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                                "menu": buildEditClanMenu(filterFutureSteps(item = mainItem), 'both', 'made', item, mainItem.name),
                                'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                                "titl": "",
                                "url": request.path,
                                "fon0": static("libraryTHPS/img/17db.jpg"),
                                "fon1": static("libraryTHPS/img/noise.gif")
                                })})
                        return render(request, "libraryTHPS/libraryTHPS.html", {
                            'parentTemplate': 'base/fullPage/base.html',
                            'content': ('libraryTHPS', 'add', 'clan', 'message'),
                            'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem), 'both', 'made', item, mainItem.name)),
                            'message': "<h3>Не спеши, %s!</h3>\
                            <p>У клана <a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>%s</a>\
                            , на данный момент, имеется миниатюра, представление было удалено или не было создано. А кроме того, на изображениях фона \
                             и логотипа не были выделены ключевые области.</p>\
                            <p><a href = '%s' onclick = 'loadContent(this.href);'>Определить наиболее важные области на изображениях фона и логотипа</a></p>\
                            <small>или</small>\
                            <p><input type = 'submit' formaction = '%s' onclick = 'doPostForm(\"%s\");' value = 'оставьте как есть'/>.</p>" % 
                            (request.user.username,
                             reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}),
                             mainItem.name,
                             reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'ground'}),
                             reverse("libraryAddClanAutoCopyImagesUrl", kwargs = {'item': item, 'section': 'both', 'typeOfImage': 'both'})),
                            'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                            'csrf': checkCSRF(request),
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })
                if thumb.backgroundCrop == False:
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'url': request.path,
                                'titl': '',
                                'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                    'parentTemplate': 'base/fullPage/emptyParent.html',
                                    'content': ('libraryTHPS', 'add', 'clan', 'message'),
                                    'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem), 'both', 'made', item, mainItem.name)),
                                    'message': "<h3>Не спеши, %s!</h3>\
                                    <p>У клана <a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>%s</a>\
                                    , на данный момент, имеется миниютюра, представление было удалено или не было создано. А кроме того, на изображении фона \
                                    не была выделена ключевая область.</p>\
                                    <p><a href = '%s' onclick = 'loadContent(this.href);'>Определить наиболее важную область на изображении фона</a></p>\
                                    <small>или</small>\
                                    <p><input type = 'submit' formaction = '%s' onclick = 'doPostForm(\"%s\");' value = 'оставьте как есть'/>.</p>" % 
                                    (request.user.username,
                                     reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}),
                                     mainItem.name,
                                     reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'ground'}),
                                     reverse("libraryAddClanAutoCopyImagesUrl", kwargs = {'item': item, 'section': 'both', 'typeOfImage': 'ground'})),
                                     'csrf': '',
                                     "fon0": static("libraryTHPS/img/17db.jpg"),
                                     "fon1": static("libraryTHPS/img/noise.gif")
                                    })
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/content.html"),
                            "templateBody": rts("libraryTHPS/body/add/message.html", {
                                'message': "<h3>Не спеши, %s!</h3>\
                                <p>У клана <a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>%s</a>\
                                , на данный момент, имеется миниютюра, представление было удалено или не было создано. А кроме того, на изображении фона \
                                не была выделена ключевая область.</p>\
                                <p><a href = '%s' onclick = 'loadContent(this.href);'>Определить наиболее важную область на изображении фона</a></p>\
                                <small>или</small>\
                                <p><input type = 'submit' formaction = '%s' onclick = 'doPostForm(\"%s\");' value = 'оставьте как есть'/>.</p>" % 
                                (request.user.username,
                                 reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}),
                                 mainItem.name,
                                 reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'ground'}),
                                 reverse("libraryAddClanAutoCopyImagesUrl", kwargs = {'item': item, 'section': 'both', 'typeOfImage': 'ground'})),
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                            "menu": buildEditClanMenu(filterFutureSteps(item = mainItem), 'both', 'made', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                            "titl": "",
                            "url": request.path,
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })})
                    return render(request, "libraryTHPS/libraryTHPS.html", {
                        'parentTemplate': 'base/fullPage/base.html',
                        'content': ('libraryTHPS', 'add', 'clan', 'message'),
                        'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem), 'both', 'made', item, mainItem.name)),
                        'message': "<h3>Не спеши, %s!</h3>\
                        <p>У клана <a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>%s</a>\
                        , на данный момент, имеется миниютюра, представление было удалено или не было создано. А кроме того, на изображении фона \
                        не была выделена ключевая область.</p>\
                        <p><a href = '%s' onclick = 'loadContent(this.href);'>Определить наиболее важную область на изображении фона</a></p>\
                        <small>или</small>\
                        <p><input type = 'submit' formaction = '%s' onclick = 'doPostForm(\"%s\");' value = 'оставьте как есть'/>.</p>" % 
                        (request.user.username,
                         reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}),
                         mainItem.name,
                         reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'ground'}),
                         reverse("libraryAddClanAutoCopyImagesUrl", kwargs = {'item': item, 'section': 'both', 'typeOfImage': 'ground'})),
                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                        'csrf': checkCSRF(request),
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })
                if thumb.logoCrop == False:
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'url': request.path,
                                'titl': '',
                                'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                    'parentTemplate': 'base/fullPage/emptyParent.html',
                                    'content': ('libraryTHPS', 'add', 'clan', 'message'),
                                    'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem), 'both', 'made', item, mainItem.name)),
                                    'message': "<h3>Не спеши, %s!</h3>\
                                    <p>У клана <a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>%s</a>\
                                    , на данный момент, имеется миниатюра, представление было удалено или не было создано. А кроме того, на изображении логотипа \
                                    не была выделена ключевая область.</p>\
                                    <p><a href = '%s' onclick = 'loadContent(this.href);'>Определить наиболее важную область на изображении логотипа</a></p>\
                                    <small>или</small>\
                                    <p><input type = 'submit' formaction = '%s' onclick = 'doPostForm(\"%s\");' value = 'оставьте как есть'/>.</p>" % 
                                    (request.user.username,
                                     reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}),
                                     mainItem.name,
                                     reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'logo'}),
                                     reverse("libraryAddClanAutoCopyImagesUrl", kwargs = {'item': item, 'section': 'both', 'typeOfImage': 'logo'})),
                                     'csrf': '',
                                     "fon0": static("libraryTHPS/img/17db.jpg"),
                                     "fon1": static("libraryTHPS/img/noise.gif")
                                    })
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/content.html"),
                            "templateBody": rts("libraryTHPS/body/add/message.html", {
                                'message': "<h3>Не спеши, %s!</h3>\
                                <p>У клана <a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>%s</a>\
                                , на данный момент, имеется миниатюра, представление было удалено или не было создано. А кроме того, на изображении логотипа \
                                не была выделена ключевая область.</p>\
                                <p><a href = '%s' onclick = 'loadContent(this.href);'>Определить наиболее важную область на изображении логотипа</a></p>\
                                <small>или</small>\
                                <p><input type = 'submit' formaction = '%s' onclick = 'doPostForm(\"%s\");' value = 'оставьте как есть'/>.</p>" % 
                                (request.user.username,
                                 reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}),
                                 mainItem.name,
                                 reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'logo'}),
                                 reverse("libraryAddClanAutoCopyImagesUrl", kwargs = {'item': item, 'section': 'both', 'typeOfImage': 'logo'})),
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                            "menu": buildEditClanMenu(filterFutureSteps(item = mainItem), 'both', 'made', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                            "titl": "",
                            "url": request.path,
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })})
                    return render(request, "libraryTHPS/libraryTHPS.html", {
                        'parentTemplate': 'base/fullPage/base.html',
                        'content': ('libraryTHPS', 'add', 'clan', 'message'),
                        'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem), 'both', 'made', item, mainItem.name)),
                        'message': "<h3>Не спеши, %s!</h3>\
                        <p>У клана <a href = '%s' onclick = 'loadContent(this.href); event.preventDefault();'>%s</a>\
                        , на данный момент, имеется миниатюра, представление было удалено или не было создано. А кроме того, на изображении логотипа \
                        не была выделена ключевая область.</p>\
                        <p><a href = '%s' onclick = 'loadContent(this.href);'>Определить наиболее важную область на изображении логотипа</a></p>\
                        <small>или</small>\
                        <p><input type = 'submit' formaction = '%s' onclick = 'doPostForm(\"%s\");' value = 'оставьте как есть'/>.</p>" % 
                        (request.user.username,
                         reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}),
                         mainItem.name,
                         reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'logo'}),
                         reverse("libraryAddClanAutoCopyImagesUrl", kwargs = {'item': item, 'section': 'both', 'typeOfImage': 'logo'})),
                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                        'csrf': checkCSRF(request),
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })
            if head.backgroundCrop == False and head.logoCrop == False:
                pass
            if head.backgroundCrop == False:
                pass
            if head.logoCrop == False:
                pass
            if thumb.backgroundCrop == False and thumb.logoCrop == False:
                pass
            if thumb.backgroundCrop == False:
                pass
            if thumb.logoCrop == False:
                pass
            if head.backgroundCropCoordinates != thumb.backgroundCropCoordinates:
                pass
            if head.logoCropCoordinates != thumb.logoCropCoordinates:
                pass
            media = settings.MEDIA_URL
            if head.isMaded == 1:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'made', ''),
                                'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                                'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'made', item, mainItem.name)),
                                'csrf': '',
                                'form': made(),
                                'madeItems': {
                                    'rootImage': head.backgroundCrop.url,
                                    'logo': head.logoCrop.url,
                                    'maded': 1
                                    },
                                'initialMadeDict': {'positionLogo': head.logoPosition},
                                'fon0': static("libraryTHPS/img/17db.jpg"),
                                'fon1': static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/made.html"),
                        "templateBody": rts("libraryTHPS/body/add/made.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'made', ''),
                            'csrf': '',
                            'form': made(),
                            'madeItems': {
                                'rootImage': head.backgroundCrop.url,
                                'logo': head.logoCrop.url,
                                'maded': 1
                                }
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/made.html", {
                            'initialMadeDict': {
                                'positionLogo': head.logoPosition
                                }
                            }),
                        "menu": buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'made', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                        'titl': '',
                        "url": request.path,
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                return render(request, "libraryTHPS/libraryTHPS.html", {
                    'parentTemplate': 'base/fullPage/base.html',
                    'content': ('libraryTHPS', 'add', 'clan', 'made', ''),
                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'made', item, mainItem.name)),
                    'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                    'csrf': checkCSRF(request),
                    'form': made(),
                    'madeItems': {
                        'rootImage': head.backgroundCrop.url,
                        'logo': head.logoCrop.url,
                        'maded': 1
                        },
                    'initialMadeDict': {'positionLogo': head.logoPosition},
                    'titl': '',
                    'fon0': static("libraryTHPS/img/17db.jpg"),
                    'fon1': static("libraryTHPS/img/noise.gif")
                    })
            if head.isMaded == 2:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'made', ''),
                                'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                                'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'made', item, mainItem.name)),
                                'csrf': '',
                                'form': made(),
                                'madeItems': {
                                    'rootImage': head.backgroundCrop.url,
                                    'name': mainItem.name,
                                    'maded': 2
                                    },
                                'initialMadeDict': {
                                    'positionName': head.namePosition
                                    },
                                'fon0': static("libraryTHPS/img/17db.jpg"),
                                'fon1': static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/made.html"),
                        "templateBody": rts("libraryTHPS/body/add/made.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'made', ''),
                            'csrf': '',
                            'form': made(),
                            'madeItems': {
                                'rootImage': head.backgroundCrop.url,
                                'name': mainItem.name,
                                'maded': 2
                                }
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/made.html", {
                            'initialMadeDict': {
                                'positionName': head.namePosition
                                }
                            }),
                        "menu": buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'made', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                        'titl': '',
                        "url": request.path,
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                return render(request, "libraryTHPS/libraryTHPS.html", {
                    'parentTemplate': 'base/fullPage/base.html',
                    'content': ('libraryTHPS', 'add', 'clan', 'made', ''),
                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'made', item, mainItem.name)),
                    'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                    'csrf': checkCSRF(request),
                    'form': made(),
                    'madeItems': {
                        'rootImage': head.backgroundCrop.url,
                        'name': mainItem.name,
                        'maded': 2
                        },
                    'initialMadeDict': {
                        'positionName': head.namePosition
                        },
                    'titl': '',
                    'fon0': static("libraryTHPS/img/17db.jpg"),
                    'fon1': static("libraryTHPS/img/noise.gif")
                    })
            if head.isMaded == 3:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'made', ''),
                                'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                                'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'made', item, mainItem.name)),
                                'csrf': '',
                                'form': made(),
                                'madeItems': {
                                    'rootImage': head.backgroundCrop.url,
                                    'logo': head.logoCrop.url,
                                    'name': mainItem.name,
                                    'maded': 3
                                    },
                                'initialMadeDict': {
                                    'positionLogo': head.logoPosition,
                                    'positionName': head.namePosition
                                    },
                                'fon0': static("libraryTHPS/img/17db.jpg"),
                                'fon1': static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/made.html"),
                        "templateBody": rts("libraryTHPS/body/add/made.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'made', ''),
                            'csrf': '',
                            'form': made(),
                            'madeItems': {
                                'rootImage': head.backgroundCrop.url,
                                'logo': head.logoCrop.url,
                                'name': mainItem.name,
                                'maded': 3
                                }
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/made.html", {
                            'initialMadeDict': {
                                'positionLogo': head.logoPosition,
                                'positionName': head.namePosition
                                }
                            }),
                        "menu": buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'made', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                        'titl': '',
                        "url": request.path,
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                return render(request, "libraryTHPS/libraryTHPS.html", {
                    'parentTemplate': 'base/fullPage/base.html',
                    'content': ('libraryTHPS', 'add', 'clan', 'made', ''),
                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'made', item, mainItem.name)),
                    'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                    'csrf': checkCSRF(request),
                    'form': made(),
                    'madeItems': {
                        'rootImage': head.backgroundCrop.url,
                        'logo': head.logoCrop.url,
                        'name': mainItem.name,
                        'maded': 3
                        },
                    'initialMadeDict': {
                        'positionLogo': head.logoPosition,
                        'positionName': head.namePosition
                        },
                    'titl': '',
                    'fon0': static("libraryTHPS/img/17db.jpg"),
                    'fon1': static("libraryTHPS/img/noise.gif")
                    })
            if thumb.isMaded == 1:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'made', ''),
                                'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                                'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'made', item, mainItem.name)),
                                'csrf': '',
                                'form': made(),
                                'madeItems': {
                                    'rootImage': thumb.backgroundCrop.url,
                                    'logo': thumb.logoCrop.url,
                                    'maded': 1
                                    },
                                'initialMadeDict': {
                                    'logoPosition': thumb.logoCoordinates
                                    },
                                'fon0': static("libraryTHPS/img/17db.jpg"),
                                'fon1': static("libraryTHPS/img/noise.gif")
                                })

                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/made.html"),
                        "templateBody": rts("libraryTHPS/body/add/made.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'made', ''),
                            'csrf': '',
                            'form': made(),
                            'madeItems': {
                                'rootImage': thumb.backgroundCrop.url,
                                'logo': thumb.logoCrop.url,
                                'maded': 1
                                }
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/made.html", {
                            'initialMadeDict': {
                                'logoPosition': thumb.logoCoordinates
                                }
                            }),
                        "menu": buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'made', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                        'titl': '',
                        "url": request.path,
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                return render(request, "libraryTHPS/libraryTHPS.html", {
                    'parentTemplate': 'base/fullPage/base.html',
                    'content': ('libraryTHPS', 'add', 'clan', 'made', ''),
                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'made', item, mainItem.name)),
                    'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                    'csrf': checkCSRF(request),
                    'form': made(),
                    'madeItems': {
                        'rootImage': thumb.backgroundCrop.url,
                        'logo': thumb.logoCrop.url,
                        'maded': 1
                        },
                    'initialMadeDict': {
                        'logoPosition': thumb.logoCoordinates
                        },
                    'titl': '',
                    'fon0': static("libraryTHPS/img/17db.jpg"),
                    'fon1': static("libraryTHPS/img/noise.gif")
                    })
            if thumb.isMaded == 2:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'made', ''),
                                'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                                'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'made', item, mainItem.name)),
                                'csrf': '',
                                'form': made(),
                                'madeItems': {
                                    'rootImage': thumb.backgroundCrop.url,
                                    'name': mainItem.name,
                                    'maded': 2
                                    },
                                'initialMadeDict': {
                                    'positionName': thumb.namePosition
                                    },
                                'fon0': static("libraryTHPS/img/17db.jpg"),
                                'fon1': static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templatethumb": rts("libraryTHPS/thumb/add/made.html"),
                        "templateBody": rts("libraryTHPS/body/add/made.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'made', ''),
                            'csrf': '',
                            'form': made(),
                            'madeItems': {
                                'rootImage': thumb.backgroundCrop.url,
                                'name': mainItem.name,
                                'maded': 2
                                }
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/made.html", {
                            'initialMadeDict': {
                                'positionName': thumb.namePosition
                                },
                            }),
                        "menu": buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'made', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                        'titl': '',
                        "url": request.path,
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                return render(request, "libraryTHPS/libraryTHPS.html", {
                    'parentTemplate': 'base/fullPage/base.html',
                    'content': ('libraryTHPS', 'add', 'clan', 'made', ''),
                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'made', item, mainItem.name)),
                    'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                    'csrf': checkCSRF(request),
                    'form': made(),
                    'madeItems': {
                        'rootImage': thumb.backgroundCrop.url,
                        'name': mainItem.name,
                        'maded': 2
                        },
                    'initialMadeDict': {
                        'positionName': thumb.namePosition
                        },
                    'titl': '',
                    'fon0': static("libraryTHPS/img/17db.jpg"),
                    'fon1': static("libraryTHPS/img/noise.gif")
                    })
            if thumb.isMaded == 3:
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'made', ''),
                                'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                                'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'made', item, mainItem.name)),
                                'csrf': '',
                                'form': made(),
                                'madeItems': {
                                    'rootImage': thumb.backgroundCrop.url,
                                    'logo': thumb.logoCrop.url,
                                    'name': mainItem.name,
                                    'maded': 3
                                    },
                                'initialMadeDict': {
                                    'positionLogo': thumb.logoPosition,
                                    'positionName': thumb.namePosition
                                    },
                                'fon0': static("libraryTHPS/img/17db.jpg"),
                                'fon1': static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/made.html"),
                        "templateBody": rts("libraryTHPS/body/add/made.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'made', ''),
                            'csrf': '',
                            'form': made(),
                            'madeItems': {
                                'rootImage': thumb.backgroundCrop.url,
                                'logo': thumb.logoCrop.url,
                                'name': mainItem.name,
                                'maded': 3
                                }
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/made.html", {
                            'initialMadeDict': {
                                'positionLogo': thumb.logoPosition,
                                'positionName': thumb.namePosition
                                }
                            }),
                        "menu": buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'made', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                        'titl': '',
                        "url": request.path,
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                return render(request, "libraryTHPS/libraryTHPS.html", {
                    'parentTemplate': 'base/fullPage/base.html',
                    'content': ('libraryTHPS', 'add', 'clan', 'made', ''),
                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'made', item, mainItem.name)),
                    'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                    'csrf': checkCSRF(request),
                    'form': made(),
                    'madeItems': {
                        'rootImage': thumb.backgroundCrop.url,
                        'logo': thumb.logoCrop.url,
                        'name': mainItem.name,
                        'maded': 3
                        },
                    'initialMadeDict': {
                        'positionLogo': thumb.logoPosition,
                        'positionName': thumb.namePosition
                        },
                    'titl': '',
                    'fon0': static("libraryTHPS/img/17db.jpg"),
                    'fon1': static("libraryTHPS/img/noise.gif")
                    })
            if request.is_ajax():
                if "global" in request.GET:
                    return JsonResponse({"newPage": json.dumps({
                        'url': request.path,
                        'titl': '',
                        'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                            'parentTemplate': 'base/fullPage/emptyParent.html',
                            'content': ('libraryTHPS', 'add', 'clan', 'made', ''),
                            'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                            'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'made', item, mainItem.name)),
                            'csrf': '',
                            'form': made(),
                            'madeItems': {
                                'rootImage': head.backgroundCrop.url,
                                'logo': head.logoCrop.url,
                                'name': mainItem.name,
                                'maded': 0
                                },
                            'fon0': static("libraryTHPS/img/17db.jpg"),
                            'fon1': static("libraryTHPS/img/noise.gif")
                            })
                        })})
                return JsonResponse({"newPage": json.dumps({
                    "templateHead": rts("libraryTHPS/head/add/made.html"),
                    "templateBody": rts("libraryTHPS/body/add/made.html", {
                        'content': ('libraryTHPS', 'add', 'clan', 'made', ''),
                        'csrf': '',
                        'form': made(),
                        'madeItems': {
                            'rootImage': head.backgroundCrop.url,
                            'logo': head.logoCrop.url,
                            'name': mainItem.name,
                            'maded': 0
                            }
                        }),
                    "templateScripts": rts("libraryTHPS/scripts/add/made.html"),
                    "menu": buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'made', item, mainItem.name),
                    'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                    'titl': '',
                    "url": request.path,
                    "fon0": static("libraryTHPS/img/17db.jpg"),
                    "fon1": static("libraryTHPS/img/noise.gif")
                    })})
            return render(request, "libraryTHPS/libraryTHPS.html", {
                'parentTemplate': 'base/fullPage/base.html',
                'content': ('libraryTHPS', 'add', 'clan', 'made', ''),
                'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'made', item, mainItem.name)),
                'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'made'),
                'csrf': checkCSRF(request),
                'form': made(),
                'madeItems': {
                    'rootImage': head.backgroundCrop.url,
                    'logo': head.logoCrop.url,
                    'name': mainItem.name,
                    'maded': 0
                    },
                'titl': '',
                'fon0': static("libraryTHPS/img/17db.jpg"),
                'fon1': static("libraryTHPS/img/noise.gif")
                })
    elif request.method == "POST":
        if section == "miniature":
            if mainItem.background == False:
                pass
            if mainItem.logo == False:
                pass
            try:
                thumb = clansThumbAttrs.objects.get(pk = item)
            except clansThumbAttrs.DoesNotExist:
                thumb = None
            if thumb is None:
                pass
            form = made(request.POST, rImg = thumb.backgroundCrop, mImg = thumb.logoCrop)
            if form.is_valid():
                madeOption = form.cleaned_data["isMaded"]
                thumb.isMaded = madeOption
                if madeOption == "1":
                    thumb.logoPosition = thumbForm.cleaned_data["logoPosition"]
                    thumb.logoProportions = thumbForm.cleaned_data["logoProportions"]
                    thumb.showFormat = 5
                elif madeOption == "2":
                    thumb.namePosition = thumbForm.cleaned_data["namePosition"]
                    thumb.nameProportions = thumbForm.cleaned_data["nameProportions"]
                elif madeOption == "3":
                    thumb.logoPosition = thumbForm.cleaned_data["logoPosition"]
                    thumb.logoProportions = thumbForm.cleaned_data["logoProportions"]
                    thumb.namePosition = thumbForm.cleaned_data["namePosition"]
                    thumb.nameProportions = thumbForm.cleaned_data["nameProportions"]
                    thumb.save()
        elif section == "headpiece":
            if mainItem.background == False:
                pass
            if mainItem.logo == False:
                pass
            try:
                head = clansItemAttrs.objects.get(pk = item)
            except clansItemAttrs.DoesNotExist:
                head = None
            if head is None:
                pass
            form = made(request.POST, rImg = head.backgroundCrop, mImg = head.logoCrop)
            if form.is_valid():
                madeOption = form.cleaned_data["isMaded"]
                head.isMaded = madeOption
                if madeOption == "1":
                    head.logoPosition = thumbForm.cleaned_data["logoPosition"]
                    head.logoProportions = thumbForm.cleaned_data["logoProportions"]
                elif madeOption == "2":
                    head.namePosition = thumbForm.cleaned_data["namePosition"]
                    head.nameProportions = thumbForm.cleaned_data["nameProportions"]
                elif madeOption == "3":
                    head.logoPosition = thumbForm.cleaned_data["logoPosition"]
                    head.logoProportions = thumbForm.cleaned_data["logoProportions"]
                    head.namePosition = thumbForm.cleaned_data["namePosition"]
                    head.nameProportions = thumbForm.cleaned_data["nameProportions"]
                head.save()
        else:
            if mainItem.background == False:
                pass
            if mainItem.logo == False:
                pass
            try:
                thumb = clansThumbAttrs.objects.get(pk = item)
            except clansThumbAttrs.DoesNotExist:
                thumb = None
            try:
                head = clansItemAttrs.objects.get(pk = item)
            except clansItemAttrs.DoesNotExist:
                head = None
            if head is None:
                pass
            if thumb is None:
                pass
            thumbForm = made(request.POST,  rImg = thumb.backgroundCrop, mImg = thumb.logoCrop)
            headForm = made(request.POST, rImg = head.backgroundCrop, mImg = head.logoCrop)
            if thumbForm.is_valid():
                if headForm.is_valid():
                    madeOption = thumbForm.cleaned_data["isMaded"]
                    thumb.isMaded = madeOption
                    head.isMaded = madeOption
                    if madeOption == "1":
                        thumb.logoPosition = thumbForm.cleaned_data["logoPosition"]
                        thumb.logoProportions = thumbForm.cleaned_data["logoProportions"]
                        head.logoPosition = thumbForm.cleaned_data["logoPosition"]
                        head.logoProportions = thumbForm.cleaned_data["logoProportions"]
                        thumb.showFormat = 5
                    elif madeOption == "2":
                        thumb.namePosition = thumbForm.cleaned_data["namePosition"]
                        thumb.nameProportions = thumbForm.cleaned_data["nameProportions"]
                        head.namePosition = thumbForm.cleaned_data["namePosition"]
                        head.nameProportions = thumbForm.cleaned_data["nameProportions"]
                    elif madeOption == "3":
                        thumb.logoPosition = thumbForm.cleaned_data["logoPosition"]
                        thumb.logoProportions = thumbForm.cleaned_data["logoProportions"]
                        head.logoPosition = thumbForm.cleaned_data["logoPosition"]
                        head.logoProportions = thumbForm.cleaned_data["logoProportions"]
                        thumb.namePosition = thumbForm.cleaned_data["namePosition"]
                        thumb.nameProportions = thumbForm.cleaned_data["nameProportions"]
                        head.namePosition = thumbForm.cleaned_data["namePosition"]
                        head.nameProportions = thumbForm.cleaned_data["nameProportions"]
                    with transaction.atomic():
                        thumb.save()
                        head.save()
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps("%s%s" % (reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}), "?type=fullversion"))
                            })
                    return redirect("storageShowItemUrl", 'clan', item)

def addLibraryClanItemAutoCopyImages(request, item, section, typeOfImage):
    ''' Эта функция призвана копировать оригинальные загруженные изображения в папку с crop изображениями ,\
     если так решит пользователь.'''
    if request.method == "POST":
        if section == "miniature":
            pass
        elif section == "page":
            pass
        else:
            pass