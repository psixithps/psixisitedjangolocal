import json
import os
import math
from datetime import datetime
from PIL import Image
from imagekit import ImageSpec
from imagekit.processors import Adjust
from imagekit.processors import resize
from libraryTHPS.models import libraryClanThumbDefaultService, libraryClanThumbService, clansMain
from django.conf import settings
from django.http import JsonResponse, HttpResponseBadRequest

def bigBackgroundResize(request):
    clanMain = clansMain.objects.get(pk = json.loads(request.GET.get("item")))
    image = clanMain.background
    if image is not None:
        whTuple = tuple(json.loads(request.GET.get("clientScreenSize")))
        if whTuple[0] <= 4096 and whTuple[1] <= 3072:
            if image.width > 500 and image.height > 500:
                class resizeBig(ImageSpec):
                    processors = [Adjust(contrast = 0.5, sharpness = 0.1), resize.SmartResize(whTuple[0], whTuple[1], upscale = True)]
                    format = "png"
                    options = {'quality': 40}
            else:
                class resizeBig(ImageSpec):
                    processors = [Adjust(contrast = 0.5, sharpness = 0.1), resize.ResizeToFill(whTuple[0], whTuple[1], upscale = True)]
                    format = "png"
                    options = {'quality': 40}
            name, fAt = getNameFormatFromLink(image.name)
            cachedBigImagesPath = os.path.normpath("%s/libraryTHPS/cache/clan/%s/big/%s/" % (settings.MEDIA_URL, clanMain.slugName, "%dx%d" % whTuple))
            if not os.path.exists(os.path.normpath("%s%s" % (settings.BASE_DIR, cachedBigImagesPath))):
                os.makedirs(os.path.normpath("%s%s" % (settings.BASE_DIR, cachedBigImagesPath)))
            outputString = "%s/%s.%s" % (cachedBigImagesPath, name, fAt)
            newImage = os.path.normpath("%s%s" % (settings.BASE_DIR, outputString))
            if os.path.exists(newImage):
                return JsonResponse({"fonImg": json.dumps(outputString)})
            with open(os.path.normpath("%s%s" % (settings.BASE_DIR, image.url)), 'rb') as oldImage:
                gener = resizeBig(source = oldImage)
                g = gener.generate()
            with open(newImage, 'wb') as toSaveImage:
                toSaveImage.write(g.read())
            return JsonResponse({"fonImg": json.dumps(outputString)})
        return HttpResponseBadRequest
    else:
        return JsonResponse({"fonImg" : json.dumps(None)})

def getNameFormatFromLink(link):
    name = link[link.rfind("/") + 1 : link.rfind(".")]
    formaT = link[link.rfind(".") + 1 : len(link)]
    return name, formaT

class storageResizing():
    def __init__(self, userId, signedUser, thumbItemSize, itemType):
        self.size, self.id, self.isAuth, self.itemType = thumbItemSize, userId, signedUser, itemType

    def resizeBackground(self, slug, imageSize, image):
        name, fat = getNameFormatFromLink(image)
        imageFullName = "%s.%s" % (name, fat)
        if self.isAuth:
            imagePath = "%slibraryTHPS/cache/clan/%s/%s/%d/background/" % (settings.MEDIA_URL, slug, self.itemType, self.id)
        else:
            imagePath = "%slibraryTHPS/cache/clan/%s/%s/default/background/" % (settings.MEDIA_URL, slug, self.itemType)
        if not os.path.exists(os.path.normpath("%s%s" % (settings.BASE_DIR, imagePath))):
            os.makedirs(os.path.normpath("%s%s" % (settings.BASE_DIR, imagePath)))
        imgFull = os.path.normpath("%s%s" % (imagePath, imageFullName))
        newImageFullPath = "%s%s" % (settings.BASE_DIR, imgFull)
        if os.path.exists(os.path.normpath(newImageFullPath)):
            return imgFull
        else:
            size = []
            if self.size[0] > imageSize[0]:
                size.append(imageSize[0])
            else:
                size.append(self.size[0])
            if self.size[1] > imageSize[1]:
                size.append(imageSize[1])
            else:
                size.append(self.size[1])
            if fat == "jpg":
                class resizing(ImageSpec):
                    processors = [resize.ResizeToFit(size[0], size[1])]
                    format = "jpeg"
                    options = {'quality': 60}
            else:
                class resizing(ImageSpec):
                    processors = [resize.ResizeToFit(size[0], size[1])]
                    format = fat
                    options = {'quality': 60}
            with open(os.path.normpath("%s%s" % (settings.BASE_DIR, image)), 'rb') as origin:
                res = resizing(source = origin)
                g = res.generate()
            with open(os.path.normpath(newImageFullPath), 'wb') as newImg:
                newImg.write(g.read())
            return imgFull

    def resizeLogo(self, slug, imageSize, image):
        name, fat = getNameFormatFromLink(image)
        imageFullName = "%s.%s" % (name, fat)
        if self.isAuth:
            imagePath = "%slibraryTHPS/cache/clan/%s/%s/%d/logo/" % (settings.MEDIA_URL, slug, self.itemType, self.id)
        else:
            imagePath = "%slibraryTHPS/cache/clan/%s/%s/default/logo/" % (settings.MEDIA_URL, slug, self.itemType)
        if not os.path.exists(os.path.normpath("%s%s" % (settings.BASE_DIR, imagePath))):
            os.makedirs(os.path.normpath("%s%s" % (settings.BASE_DIR, imagePath)))
        imgFull = os.path.normpath("%s%s" % (imagePath, imageFullName))
        newImageFullPath = "%s%s" % (settings.BASE_DIR, imgFull)
        if os.path.exists(os.path.normpath(newImageFullPath)):
            return imgFull
        else:
            size = []
            if self.size[0] > imageSize[0]:
                size.append(imageSize[0])
            else:
                size.append(self.size[0])
            if self.size[1] > imageSize[1]:
                size.append(imageSize[1])
            else:
                size.append(self.size[1])
            if fat == "jpg":
                class resizing(ImageSpec):
                    processors = [resize.ResizeToFit(size[0], size[1])]
                    format = "jpeg"
                    options = {'quality': 85}
            else:
                class resizing(ImageSpec):
                    processors = [resize.ResizeToFit(size[0], size[1])]
                    format = fat
                    options = {'quality': 60}
            with open(os.path.normpath("%s%s" % (settings.BASE_DIR, image)), 'rb') as origin:
                res = resizing(source = origin)
                g = res.generate()
            with open(os.path.normpath(newImageFullPath), 'wb') as newImage:
                newImage.write(g.read())
            return imgFull

    def resizeBackgroundCrop(self, slug, imageSize, image):
        name, fat = getNameFormatFromLink(image)
        imageFullName = "%s.%s" % (name, fat)
        if self.isAuth:
            imagePath = "%slibraryTHPS/cache/clan/%s/%s/%d/background/crop/" % (settings.MEDIA_URL, slug, self.itemType, self.id)
        else:
            imagePath = "%slibraryTHPS/cache/clan/%s/%s/default/background/" % (settings.MEDIA_URL, slug, self.itemType)
        if not os.path.exists(os.path.normpath("%s%s" % (settings.BASE_DIR, imagePath))):
            os.makedirs(os.path.normpath("%s%s" % (settings.BASE_DIR, imagePath)))
        imgFull = "%s%s" % (imagePath, imageFullName)
        newImageFullPath = os.path.normpath("%s%s" % (settings.BASE_DIR, imgFull))
        if os.path.exists(newImageFullPath):
            return imgFull
        else:
            size = []
            if self.size[0] > imageSize[0]:
                size.append(imageSize[0])
            else:
                size.append(self.size[0])
            if self.size[1] > imageSize[1]:
                size.append(imageSize[1])
            else:
                size.append(self.size[1])
            if fat == "jpg":
                class resizing(ImageSpec):
                    processors = [resize.ResizeToFit(size[0], size[1])]
                    format = "jpeg"
                    options = {'quality': 60}
            else:
                class resizing(ImageSpec):
                    processors = [resize.ResizeToFit(size[0], size[1])]
                    format = fat
                    options = {'quality': 60}
            with open(os.path.normpath("%s%s" % (settings.BASE_DIR, image)), 'rb') as origin:
                res = resizing(source = origin)
                g = res.generate()
            with open(newImageFullPath, 'wb') as newImg:
                newImg.write(g.read())
            return imgFull

    def resizeLogoCrop(self, slug, imageSize, image):
        name, fat = getNameFormatFromLink(image)
        imageFullName = "%s.%s" % (name, fat)
        if self.isAuth:
            imagePath = "%slibraryTHPS/cache/clan/%s/%s/%d/logo/crop/" % (settings.MEDIA_URL, slug, self.itemType, self.id)
        else:
            imagePath = "%slibraryTHPS/cache/clan/%s/%s/default/logo/crop/" % (settings.MEDIA_URL, slug, self.itemType)
        if not os.path.exists(os.path.normpath("%s%s" % (settings.BASE_DIR, imagePath))):
            os.makedirs(os.path.normpath("%s%s" % (settings.BASE_DIR, imagePath)))
        imgFull = os.path.normpath("%s%s" % (imagePath, imageFullName))
        newImageFullPath = "%s%s" % (settings.BASE_DIR, imgFull)
        if os.path.exists(os.path.normpath(newImageFullPath)):
            return imgFull
        else:
            size = []
            if self.size[0] > imageSize[0]:
                size.append(imageSize[0])
            else:
                size.append(self.size[0])
            if self.size[1] > imageSize[1]:
                size.append(imageSize[1])
            else:
                size.append(self.size[1])
            if fat == "jpg":
                class resizing(ImageSpec):
                    processors = [resize.ResizeToFit(size[0], size[1])]
                    format = "jpeg"
                    options = {'quality': 60}
            else:
                class resizing(ImageSpec):
                    processors = [resize.ResizeToFit(size[0], size[1])]
                    format = fat
                    options = {'quality': 60}
            with open(os.path.normpath("%s%s" % (settings.BASE_DIR, image)), 'rb') as origin:
                res = resizing(source = origin)
                g = res.generate()
            with open(os.path.normpath(newImageFullPath), 'wb') as newImg:
                newImg.write(g.read())
            return imgFull

def grabMadeLogoSize(backgroundOriginalSize, backgroundResizedSize, logoCroppedSize, logoProportions):
    if logoProportions[0] < 100 and logoProportions[1] < 100:
        size = (int((backgroundResizedSize[0] * logoProportions[0]) / 100), int((backgroundResizedSize[1] * logoProportions[1]) / 100))
    else:
        if backgroundOriginalSize[0] > logoCroppedSize[0] and backgroundOriginalSize[1] > logoCroppedSize[1]:
            size = (int(logoCroppedSize[0] / (backgroundOriginalSize[0] / backgroundResizedSize[0])),
                    int(logoCroppedSize[1] / (backgroundOriginalSize[1] / backgroundResizedSize[1])))
        elif backgroundOriginalSize[0] > logoCroppedSize[0] or backgroundOriginalSize[1] > logoCroppedSize[1]:
            if backgroundOriginalSize[0] > logoCroppedSize[0]:
                size = (int(logoCroppedSize[0] / (backgroundOriginalSize[0] / backgroundResizedSize[0])),
                        logoCroppedSize[1])
            else:
                size = (logoCroppedSize[0],
                        int(logoCroppedSize[1] / (backgroundOriginalSize[1] / backgroundResizedSize[1])))
        else:
            size = (logoCroppedSize[0], logoCroppedSize[1])
    return size

def getResizedBackgroundSize(background):
    with Image.open(os.path.normpath("%s%s" % (settings.BASE_DIR, background)), 'r') as image:
        parentImageSize = image.size
    return parentImageSize

def reCalculateCoordinates(backgroundOriginalSize, background, position):
    ratio = (backgroundOriginalSize[0] / background[0], backgroundOriginalSize[1] / background[1])
    coordinates = (math.ceil(position[0] / ratio[0]) + 2, math.ceil(position[1] / ratio[1]) + 2) # Прибавил 2 из за рамки add.css:88 - padding
    return coordinates