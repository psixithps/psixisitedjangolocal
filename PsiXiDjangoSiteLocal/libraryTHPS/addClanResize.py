import os
from django.conf import settings
import json
from django.contrib.staticfiles.templatetags.staticfiles import static
from libraryTHPS.models import clansMain, clansItemAttrs, clansThumbAttrs
from libraryTHPS.forms import headResize, thumbResize, resizeUnited
from base.helpers import checkCSRF
from django.shortcuts import get_object_or_404
from libraryTHPS.helpers import cookiesEdit, filterFutureSteps
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.template.loader import render_to_string as rts
from django.urls import reverse

def addLibraryClanItemLogoResize(request, item, section, urlSep, step):
    mainItem, csrfValue, cookie = get_object_or_404(clansMain, pk = item),\
    checkCSRF(request), request.COOKIES.get("editClanItemSection", None)
    if request.method == "GET":
        if section == "miniature":
            try:
                thumb = clansThumbAttrs.objects.get(pk = mainItem.pk)
            except clansThumbAttrs.DoesNotExist:
                thumb = None
            if step == "ground":
                pass
            else:
                if request.is_ajax():
                    out = {'content': ('libraryTHPS', 'add', 'clan', 'resize', 'logo', 'miniature'), 'csrf': csrfValue,
                        'form': thumbResize(logo = mainItem.logo)}
                    out.update(initFormsNavi(request, ('add', 'clan', 'resize', 'logo', 'miniature', item, mainItem.name), zone = cookie,
                                            blocks = filterFutureSteps(item = mainItem, thumb = thumb)))
                    return cookiesEdit(JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/resize.html"),
                            "templateBody": rts("libraryTHPS/body/add/resize.html", out),
                            "templateScripts": rts("libraryTHPS/scripts/add/resize.html"),
                            'breadcrumbs': ('clans', 'add', 'resize'),
                            'titl': '',
                            "url": request.path,
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif") 
                                                                })
                                        }), request.COOKIES, {"editClanItemSection": cookie})
                out = {'content': ('libraryTHPS', 'add', 'clan', 'resize', 'logo', 'miniature'), 'breadcrumbs': ('clans', 'add', 'resize'),
                       'csrf': csrfValue, 'form': thumbResize(logo = mainItem.logo), 'titl': '', 'fon0': static("libraryTHPS/img/17db.jpg"),
                       'fon1': static("libraryTHPS/img/noise.gif")}
                out.update(initFormsNavi(request, ('add', 'clan', 'resize', 'logo', 'miniature', item, mainItem.name), zone = cookie,
                                            blocks = filterFutureSteps(item = mainItem, thumb = thumb)))
                return cookiesEdit(render(request, "libraryTHPS/libraryTHPS.html", out), request.COOKIES, {"editClanItemSection": cookie})
        elif section == "headpiece":
            pass
        else:
            pass
    elif request.method == "POST":
        if section == "miniature":
            pass
        elif section == "headpiece":
            pass
        else:
            pass