function madeClanThumb() {
    var madeWrap = document.getElementById("madeClanThumb");
    var form = document.forms.libraryForm;
    function setDraggable() {
        $(madeWrap.getElementsByClassName("place")[0]).children("div").draggable({
            stop: function (event, ui) {
                checkCoordsAndElems(event, ui);
            },
            containment: "#wrapMade"
        });
    };
    function getNaturalSize(image) {
        try {
            var size = [image.naturalWidth, image.naturalHeight];
        } catch (event) {
            var img = new Image();
            img.src = image.src;
            var size = [img.width, img.height];
        };
        return size
    };
    function checkCoordsAndElems(event, ui) {
        var dragElem = ui.helper["0"];
        var dragElemClassName = dragElem.className.slice(0, dragElem.className.indexOf(" "));
        var curInput = form.logoPosition;
        var rootImage = madeWrap.firstElementChild.firstElementChild;
        var rootWH = getNaturalSize(rootImage);
        var ratio = [rootWH[0] / rootImage.clientWidth, rootWH[1] / rootImage.clientHeight];
        var xy = [Math.round(ratio[0] * (ui.position.left - 2)), Math.round(ratio[1] * (ui.position.top - 2))];
        curInput.value = xy.join(","); // Отнял 2 из за add.css:88 - padding
    };
    madeWrap.addEventListener("dblclick", checkElements, false);
    function checkElements(event, isFirst) {
        var parent = event.target || event;
        if (parent.nodeName == "IMG" || parent.nodeName == "SPAN" || isFirst) {
            do {
                var parent = parent.parentNode;
                var target = parent ? parent.className.match(/^clanLogo.*|clanName.*$/i) ? parent : target || null : null;
            } while (parent.className != "place" && parent.className != "off");
            target ? target.className.indexOf(" ") != -1 ? target.className = target.className.slice(0, target.className.indexOf(" ")) : 0 : 0;
            if (isFirst) {
                if (parent.className == "place") { // Элемент был включен изначально
                    if (target.className == "clanLogo") {
                        setSizeByProportions(target.firstElementChild);
                        form.logoPosition.value = window.initialMadeObject ? initialMadeObject.positionLogo ?
                            initialMadeObject.positionLogo : "0,0" : "0,0";
                        setInitialCoordinates(madeWrap.firstElementChild.firstElementChild, target.firstElementChild,
                            form.logoPosition.value);
                    } else if (target.className == "clanName") {
                        form.namePosition.value = window.initialMadeObject ? initialMadeObject.positionName ?
                            initialMadeObject.positionName : "0,0" : "0,0";
                        setInitialCoordinates(madeWrap.firstElementChild.firstElementChild, target.firstElementChild,
                            form.namePosition.value);
                    }
                } else if (parent.className == "off") { // Элемент был выключен изначально
                    if (target.className == "clanLogo") {
                        form.logoPosition.value = "0,0";
                    } else if (target.className == "clanName") {
                    }
                }
            } else {
                if (parent.className == "place") { // Выключил элемент
                    if (target.className == "clanLogo") {
                        setSizeByDefault(event.target);
                    } else if (target.className == "clanName") {
                    }
                    madeWrap.lastElementChild.appendChild(target);
                } else if (parent.className == "off") { // Включил элемент
                    if (target.className == "clanLogo") {
                        setSizeByProportions(event.target);
                        form.logoPosition.value = window.initialMadeObject ? initialMadeObject.positionLogo ?
                            initialMadeObject.positionLogo : "0,0" : "0,0";
                    } else if (target.className == "clanName") {
                        form.namePosition.value = window.initialMadeObject ? initialMadeObject.positionName ?
                            initialMadeObject.positionName : "0,0" : "0,0";
                    }
                    madeWrap.firstElementChild.lastElementChild.appendChild(target);
                }
                delete target;
                setDraggable();
            }
            if (document.children) {
                var allOnPlaceNodes = madeWrap.getElementsByClassName("place")[0].children;
            } else {
                var allOnPlaceNodes = madeWrap.getElementsByClassName("place")[0].childNodes;
                var allOnPlaceNodes = Array.prototype.slice.call(allOnPlaceNodes).filter(function (item) {
                    return item.nodeType == 1 ? item : null;
                });
            }
            allOnPlaceNodes.length == 0 ? form.isMaded.value = 0 : 0;
            for (var i = 0; i < allOnPlaceNodes.length; i++) {
                var v = i == 0 ? i : v;
                if (allOnPlaceNodes[i].className.match(/^clanLogo.*$/i)) {
                    form.isMaded.value = 1;
                    v++;
                } else if (allOnPlaceNodes[i].className.match(/^clanName.*$/i)) {
                    form.isMaded.value = 2;
                    v++;
                }
                v > 1 ? form.isMaded.value = 3 : 0;
            }
            event.stopPropagation ? event.stopPropagation() : 0;
        }
    };
    if (window.initialMadeObject) {
        var place = madeWrap.getElementsByClassName("place")[0].childNodes;
        for (var i = 0; i < place.length; i++) {
            place[i].nodeType == 1 ? checkElements(place[i].firstElementChild, true) : 0;
        }
    }
    function setSizeByProportions(madeImage) {
        var rootImage = madeWrap.firstElementChild.firstElementChild;
        var rootImageOriginalWH = getNaturalSize(rootImage);
        var madeImageOriginalWH = getNaturalSize(madeImage);
        var rootImageWH = [rootImage.clientWidth, rootImage.clientHeight];
        var madeImageWH = [madeImage.clientWidth, madeImage.clientHeight];
        var ratio = [rootImageOriginalWH[0] / rootImageWH[0], rootImageOriginalWH[1] / rootImageWH[1]];
        var newSizes = [madeImageOriginalWH[0] / ratio[0], madeImageOriginalWH[1] / ratio[1]];
        if (newSizes[0] > newSizes[1]) {
            madeImage.style.width = "auto";
            madeImage.style.height = newSizes[1];
        } else {
            madeImage.style.width = newSizes[0];
            madeImage.style.height = "auto";
        }
        if (rootImageWH[0] < madeImageWH[0] && rootImageWH[1] < madeImageWH[1]) {
            var size = [Math.round(100 / (madeImageOriginalWH[0] / madeImageWH[0])), Math.round(100 / (madeImageOriginalWH[1] / madeImageWH[1]))];
            setProportionsField(size);
        } else if (rootImageWH[0] < madeImageWH[0] || rootImageWH[1] < madeImageWH[1]) {
            if (rootImageWH[0] < madeImageWH[0]) {
                var size = [Math.round(100 / (madeImageOriginalWH[0] / madeImageWH[0])), 100];
            } else {
                var size = [100, Math.round(100 / (madeImageOriginalWH[1] / madeImageWH[1]))];
            }
            setProportionsField(size);
        } else {
            setProportionsField([100, 100]);
        }
    };
    function setSizeByDefault(image) {
        image.style.width = null;
        image.style.height = null;
    };
    function setProportionsField(size) {
        var field = form.logoProportions;
        field.value = size.join(",");
    };
    madeWrap.getElementsByClassName("place")[0].childElementCount > 0 ? setDraggable() : 0;
    function setInitialCoordinates(rootImage, madeElement, coordinates) {
        var rootWH = getNaturalSize(rootImage);
        var coordinates = coordinates.split(",");
        madeElement.parentNode.style.left = Math.round(coordinates[0] / (rootWH[0] / rootImage.clientWidth)) + 2;
        madeElement.parentNode.style.top = Math.round(coordinates[1] / (rootWH[1] / rootImage.clientHeight)) + 2; // Прибавил 2 из за add.css:88 - padding
        madeElement.parentNode.style.visibility = "visible";
    };
};
if (document.readyState == "complete") {
    beginCheckLoad();
} else {
    if (window.addEventListener) {
        document.addEventListener("DOMContentLoaded", beginCheckLoad);
    } else {
        document.attachEvent("DOMContentLoaded", beginCheckLoad);
    }
}
function beginCheckLoad() {
    var images = document.getElementById("madeClanThumb").getElementsByTagName("img");
    var imagesLoadReady = 0;
    for (var i = 0; i < images.length; i++) {
        var link = images[i].src;
        images[i].src = "";
        images[i].src = link;
        if (window.addEventListener) {
            images[i].addEventListener("load", imgReady);
        } else {
            images[i].attachEvent("load", imgReady);
        }
    }
    function imgReady(event) {
        var img = this || event.currentTarget;
        if (img && img.width && img.width > 0) {
            imagesLoadReady++;
        } else {
            beginCheckLoad();
            alert("MADE IMAGE LOAD ERROR");
        }
        if (imagesLoadReady == 2) {
            madeClanThumb();
            centerFormDiv(document.getElementById("libraryTHPSContent"));
            for (var i = 0; i < images.length; i++) {
                if (window.removeEventListener) {
                    images[i].removeEventListener("load", imgReady);
                } else {
                    images[i].detachEvent("load", imgReady);
                }
                images[i].className = images[i].className.indexOf(" ") == -1 ? "ready" : images[i].className + " ready";
            }
        }
    };
};