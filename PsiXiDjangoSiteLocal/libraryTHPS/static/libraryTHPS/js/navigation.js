loadReadyCallBack = null;
function localNavigation(event, refUrl) {
    loadContent("libraryTHPSContent", refUrl);
    return event.preventDefault();
};
function changeFonIn(fonDivs) {
        fonDivs[0].style.opacity = "0.7";
        fonDivs[1].style.opacity = "0.6";
        fonDivs[2].style.backgroundColor = "transparent";
        fonDivs[2].style.opacity = "0";
        fonDivs[0].style.right = (Math.round(Math.random()) === 0 ? "" : "-") + Math.round(Math.random() * 100);
};
function changeFonBack(fonDivs) {
    fonDivs[0].style.opacity = "";
    fonDivs[1].style.opacity = "";
    fonDivs[2].style.backgroundColor = "";
    fonDivs[2].style.opacity = "";
};
function checkFons(fonDivs, fonImgs) {
    for (var i = 0; i < fonImgs.length; i++) {
        if (fonImgs[i] == null) {
            setNoImg(fonDivs[i]);
        } else {
            var old = fonDivs[i].style.backgroundImage;
            var old = old.slice(old.indexOf("(") + 2, old.indexOf(")") - 1);
            if (old != fonImgs[i]) {
                setImg(fonDivs[i], fonImgs[i]);
            }
        }
    }
};
function setImg(place, fonImg) {
    place.style.backgroundColor = "transparent";
    place.style.backgroundImage = "url('" + fonImg + "')";
    place.style.opacity = "1";
};
function setNoImg(place) {
    place.style = "";
};
function showLibraryContent(libraryContent) {
    libraryContent.removeAttribute("class");
};
function hideLibraryContent() {
    var libraryContent = document.getElementById("libraryTHPSContent");
    var imgs = libraryContent.getElementsByTagName("IMG");
    if (imgs.length > 0) {
        Array.prototype.slice.call(imgs).forEach(function(e) {
            e.setAttribute("class", "in-progress-image");
        });
    }
    libraryContent.setAttribute("class", "in-progress");
};
function responseExtendedProperties(newPageObj) {
    var fonImgs = [newPageObj.fon0 ? newPageObj.fon0 : null,
    newPageObj.fon1 ? newPageObj.fon1 : null,
    newPageObj.fon2 ? newPageObj.fon2 : null];
    var fons = fonDivsGetter();
    checkFons(fons, fonImgs);
    changeFonBack(fons);
    return true;
};
function fonDivsGetter() {
    var libBox = document.getElementById("libraryTHPSBox");
    var fonPlace0 = libBox.getElementsByClassName("backgroundOne")[0];
    var fonPlace1 = libBox.getElementsByClassName("backgroundTwo")[0];
    var fonPlace2 = libBox.getElementsByClassName("backgroundThree")[0];
    return [fonPlace0, fonPlace1, fonPlace2];

}