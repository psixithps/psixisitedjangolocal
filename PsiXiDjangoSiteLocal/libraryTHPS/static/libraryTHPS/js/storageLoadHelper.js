function loadItems(itemsPart, curl, item) {
    var windowSizePropertiesObj = {
        "width": window.outerWidth,
        "height": window.outerHeight
    }
    var itemsObj = {
        "itemPart": itemsPart,
        "dispAreaPropers": windowSizePropertiesObj
    };
    var request = new XMLHttpRequest();
    var curl = curl + '?itemsParts=' + JSON.stringify(itemsObj);
    request.open("GET", curl, true);
    request.responseType = "json";
    request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    if (window.addEventListener) {
        request.addEventListener("load", successLoadItems, false);
    } else {
        request.attachEvent("load", successLoadItems, false);
    }
    function successLoadItems(event) {
        var thisEvent = this || event.target;
            if (thisEvent.status == 200) {
                changeFonBack(fonDivsGetter());
                loadItems1(thisEvent.response, item);
            }
    };
    request.send();
};
function loadItems1(contentData, item) {
    var isBad = false;
    Object.keys(contentData).forEach(function (key) {
        isBad = key === "somethingBad" ? true : isBad === true ? true : false;
    });
    if (!isBad) {
        var content = JSON.parse(contentData.responseList);
        var contentProperties = JSON.parse(contentData.itemProperties);
        var hasLoadMore = JSON.parse(contentData.next);
        loadItems2(content, contentProperties, item, hasLoadMore);
    } else {
        var error = contentData.somethingBad;
        var errorMessage = contentData.systemMessage;
        setErrorMessage(error, errorMessage);
    }
};
function loadItems2(content, contentProperties, item, hasLoadMore) {
    var loadMore = document.getElementsByClassName("boxLoadMore")[0];
    if (item === "clans") {
        var rootElement = document.getElementById("libraryTHPSBox").getElementsByClassName("clansList")[0];
        var last = Array.prototype.slice.call(rootElement.childNodes).filter(function(item) {
            return item.nodeType == 1 ? item: null;
        });
        rootElement.style = "padding-left: " + contentProperties.windPaddingX +
            "px; padding-top: " + contentProperties.windPaddingY + "px;";
        var contentWrap = document.createElement("UL");
        contentWrap.innerHTML = content;
        rootElement.innerHTML = rootElement.innerHTML + content;
        var contentWrap = Array.prototype.slice.call(contentWrap.childNodes).filter(function (elem) {
            return elem.nodeType == 1 ? elem.className == "maded" ? elem : null : null;
        });
        for (var i = last.length; i < contentWrap.length; i++) {
            var elem = rootElement.getElementsByClassName("maded")[i].getElementsByClassName("itemBackground")[0];
            if (elem.addEventListener) {
                elem.addEventListener('mouseover', onHoverClan);
                elem.addEventListener('mouseout', outHoverClan);
            } else if (elem.attachEvent) {
                elem.attachEvent('onmouseover', onHoverClan);
                elem.attachEvent('mouseout', outHoverClan);
            }
        }
    } else if (item === "players") {
        var rootElement = document.getElementById("libraryTHPSBox").getElementsByClassName("playersList")[0];
    }
    loadMore.style = "display: " + (hasLoadMore === true ? "block;" : "none;");
};
function setErrorMessage(error, errorMessage) {

};

function onHoverClan(e) {
    try {
        var element = this;
    } catch (error) {
        var element = e.currentTarget;
    }
    var wrap = element.lastElementChild;
    var img = element.firstElementChild;
    img.style.filter = "blur(2px)";
    img.style.opacity = "0.7";
    element.style.boxShadow = "1px 0 20px #BBB, 1px 0 10px #333 inset";
    wrap.style.display = "block";
    e.stopPropagation();
};
function outHoverClan(e, element) {
    if (element) {
        var wrap = element.lastElementChild;
        var img = element.firstElementChild;
        element.style.boxShadow = null;
    } else {
        try {
            var element = this;
        } catch (error) {
            var element = e.currentTarget; // IE
        }
        var wrap = element.lastElementChild;
        var img = element.firstElementChild;
        element.style.boxShadow = null;
    }
    img.style.opacity = null;
    img.style.filter = null;
    wrap.style.display = "none";
    e ? e.stopPropagation() : 0;
};
function loadStorageItem(e, item, element) {
    var windowWidth = window.outerWidth;
    var isReady = false;
    var request = new XMLHttpRequest();
    var elementParent = element.parentNode;
    request.open("GET", item, true);
    request.responseType = 'json';
    request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    if (request.addEventListener) {
        request.addEventListener("load", successLoad, true);
    } else {
        request.attachEvent("load", successLoad, true);
    }
    function successLoad(event) {
        var thisEvent = this || event.terget;
        if (thisEvent.status == 200) {
                var page = JSON.parse(request.response.newPage);
                var head = page.templateHead ? page.templateHead : null;
                var scripts = page.templateScripts ? page.templateScripts : null;
                var itemObject = page.templateBody ? page.templateBody : null;
                var newUrl = page.url;
                var newTitle = page.titl;
                var isReadyHead = false;
                if (head) {
                    var isReadyHead = insertingNewHead(head, 1);
                } else {
                    var isReadyHead = true;
                }
                if (isReadyHead === true) {
                    var isReadyItem = false;
                    if (itemObject) {
                        var isReadyItem = openStorageItem(itemObject, element);
                    } else {
                        var isReadyItem = true;
                    }
                    if (isReadyItem === true) {
                        var isReadyScripts = false;
                        if (scripts) {
                            var isReadyScripts = insertingNewStatics(scripts, 1);
                        } else {
                            var isReadyScripts = true;
                        }
                        if (isReadyScripts === true) {
                            var isResized = false;
                            var isResized = itemResize(element, elementParent, itemMargin, paddingUL, windowWidth);
                            if (isResized == true) {
                                refToItem(item, newUrl, itemMargin);
                                if (request.addEventListener) {
                                    request.removeEventListener("load", successLoad, true);
                                } else {
                                    request.detachEvent("load", successLoad, true);
                                }
                            }
                        }
                    }
                }
            }
    };
    request.send();
};
function checkByOtherOpened(ul) {
    var opened = ul.getElementsByClassName("opened")[0] || null;
    opened ? closeClanItem(opened) : 0;
};
function openStorageItem(itemObject, element) {
    var element = element.parentNode.className == "nameOnly" ? element.parentNode : element;
    checkByOtherOpened(element.parentNode);
    element.lastElementChild.style.display = "none";
    element.setAttribute("name", element.className);
    element.setAttribute("value", element.style.marginRight +
        ";" + element.style.marginBottom);
    element.className = "opened";
    element.innerHTML = itemObject + element.innerHTML;
    return true;
};
function itemResize(element, elementParent, itemMargin, paddungUL, width) {
    var element = elementParent.getAttribute("name") == "nameOnly" ? elementParent : element;
    var margins = element.getAttribute("value").split(";").map(function (i) {
        return Number(i.slice(0, -2));
    });
    element.style.marginLeft = itemMargin[0] > margins[0] ? itemMargin[0] - margins[0] > 0 ? itemMargin[0] - margins[0] : margins[0] - itemMargin[0] :
    itemMargin[0] - margins[0];
    element.style.marginRight = itemMargin[0] + "px";
    element.style.marginTop = itemMargin[1] > margins[1] ? itemMargin[1] - margins[1] > 0 ? itemMargin[1] - margins[1] : margins[1] - itemMargin[1] :
        itemMargin[1] - margins[1];
    element.style.marginBottom = itemMargin[1] + "px";
    return true;
};
function refToItem(url, newTitle, itemMargin) {
    var url = url.replace(document.location.origin + "/", "/");
    changeUrl(url);
    changeTitle(newTitle);
    var idStr = "#mainInfo";
    anchorNavigation(idStr, itemMargin);
};
function anchorNavigation(idStr, itemMargin) {
    //var tar = $(idStr);
    //setTimeout(function () {
        //$('html,body').animate({
            //scrollTop: tar.offset().top + (idStr === "#head" ? -itemMargin : 0)
        //}, 'middle');
    //}, 100);
};
function closeClanItem(liItem) {
    if (liItem.remove) {
        Array.prototype.slice.call(liItem.children).forEach(function (el) {
            el.hasAttribute("style") ? el.style.display === "none" ? 0 : el.remove() : el.remove();
        });
    } else {
        Array.prototype.slice.call(liItem.children).forEach(function (el) {
            el.hasAttribute("style") ? el.style.display === "none" ? 0 : liItem.removeChild(el) : liItem.removeChild(el);
        });
    }
    liItem.className = liItem.getAttribute("name");
    var margins = liItem.getAttribute("value").split(";");
    liItem.style = "";
    liItem.style.marginRight = margins[0];
    liItem.style.marginBottom = margins[1];
    liItem.removeAttribute("name");
    liItem.removeAttribute("value");
    Array.prototype.slice.call(Array.prototype.concat.call(Array.prototype.slice.call(liItem.children),
        Array.prototype.slice.call(liItem.firstElementChild.children))).forEach(function (ch) {
            ch.hasAttribute("style") ? ch.removeAttribute("style") : 0;
        });
    if (liItem.className == "maded") {
        var liItem = liItem.firstElementChild.firstElementChild;
        outHoverClan(null, liItem);
        if (liItem.addEventListener) {
            liItem.addEventListener('mouseover', onHoverClan);
            liItem.addEventListener('mouseout', outHoverClan);
        } else if (liItem.attachEvent) {
            liItem.attachEvent('onmouseover', onHoverClan);
            liItem.attachEvent('onmouseout', outHoverClan);
        }
    }
};
function backFromItemToStorage(e, url) {
    var doc = document;
    var fonDivs = fonDivsGetter();
    var liItem = doc.getElementsByClassName("opened")[0] || null;
    var liItemFull = doc.getElementById("openClanItem") || null;
    setNoImg(fonDivs[2]);
    changeFonBack(fonDivs);
    if (liItem) {
        closeClanItem(liItem);
        changeUrl(url + itemsPart + "/");
    } else if (liItemFull) {
        loadContent("libraryTHPSContent", url);
    }
    e.stopPropagation();
    e.preventDefault()
};
function searchParent(e, searchStr) {
    var t = e.target;
    do {
        var t = t.parentNode;
    } while (t ? t.nodeName != searchStr : null);
    return t;
};
function firstShowStorage() {
    if (document.removeEventListener) {
        document.removeEventListener("DOMContentLoaded", firstShowStorage);
    } else {
        document.detachEvent("DOMContentLoaded", firstShowStorage);
    }
    var f = document.getElementById("libraryTHPSContent");
    showLibraryContent(f);
    var ul = f.getElementsByClassName("clansList")[0] || null;
    if (ul) {
        function fixActiveLinks(tar) {
            do {
                var tar = tar.parentNode.nodeType != 9 ? tar.parentNode : null;
            } while (tar && tar.nodeType == 1 && tar.className != "opened" && !tar.hasAttribute("value"));
            return tar;
        };
        if (ul.addEventListener) {
            ul.addEventListener('click', function (event) {
                var tar = event.target;
                var t = !fixActiveLinks(tar) ? tar.nodeName != "A" ? searchP(tar, "A") : tar : null;
                t ? loadStorageItem(event, t.href, t.parentNode) : 0;
                event.stopPropagation();
                return event.preventDefault();
            });
        } else if (ul.attachEvent) {
            ul.attachEvent('click', function(event) {
                var tar = event.target;
                var t = !fixActiveLinks(tar) ? tar.nodeName != "A" ? searchP(tar, "A") : tar : null;
                t ? loadStorageItem(event, t.href, t.parentNode) : 0;
                event.stopPropagation();
                return event.preventDefault();
            });
        }
    }
};
typeof (itemsPart) != "undefined" ? itemsPart === 0 ? changeContent() : 0 : 0;
if (document.readyState == "complete") {
    firstShowStorage();
} else {
    if (document.addEventListener) {
        document.addEventListener("DOMContentLoaded", firstShowStorage);
    } else {
        document.attachEvent("DOMContentLoaded", firstShowStorage)
    }
}