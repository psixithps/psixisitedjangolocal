function getBigGround(item, url) {
    var fonDivs = fonDivsGetter();
    var url = url + '?clientScreenSize=' +
        JSON.stringify([screen.width, screen.height]) + '&item=' + JSON.stringify(item);
    var request = new XMLHttpRequest();
    request.open("GET", url, true);
    request.setRequestHeader('Content-type', 'application/json;charset:UTF-8');
    request.onreadystatechange = function () {
        if (request.readyState == 4) {
            if (request.status == 200 || request.status == 304) {
                var fonImageLink = JSON.parse(request.responseText);
                var fonImageLink = JSON.parse(fonImageLink.fonImg);
                changeFonBack(fonDivs);
                if (fonImageLink) {
                    var format = fonImageLink.slice(fonImageLink.indexOf(".") + 1, fonImageLink.length);
                    var img = new XMLHttpRequest();
                    img.open("GET", fonImageLink, true);
                    img.responseType = 'blob';
                    img.addEventListener('load', loadImage, false);
                    img.send();
                    function loadImage(event) {
                        changeFonIn(fonDivs);
                        if (this.status == 200 || this.status == 304) {
                            var img = new Blob([this.response], {
                                type: "image/" + format
                            });
                            var imgObj = URL.createObjectURL(img);
                            setImg(fonDivs[2], imgObj);
                        }
                    };
                } else {
                    setNoImg(fonDivs[2]);
                }
            }
        }
    };
    request.send();
};
if (document.readyState == "complete") {
    loadFon();
} else {
    if (document.addEventListener) {
        document.addEventListener("DOMContentLoaded", loadFon);
    } else {
        document.attachEvent("DOMContentLoaded", loadFon);
    }
}