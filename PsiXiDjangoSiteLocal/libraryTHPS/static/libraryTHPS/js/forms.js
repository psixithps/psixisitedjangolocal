function successHelper(context) {
    centerFormDiv(context);
    showLibraryContent(context);
};
function centerFormDiv(formDiv) {
    var docWH = [window.outerWidth, window.outerHeight];
    var formInner = formDiv.firstElementChild;
    function getFormInnerSize(fI) {
        var w = [];
        var h = 0;
        var v = 0;
        for (var i = 0; i < fI.childNodes.length; i++) {
            var e = fI.childNodes[i];
            if (e.nodeType == 1) {
                w[v] = e.clientWidth;
                h += e.clientHeight;
                var v = v + 1;
            }
        }
        var wh = [Math.max.apply(Number, w), h];
        var formStyle = window.getComputedStyle(fI);
        var paddingX = formStyle.paddingLeft;
        var paddingY = formStyle.paddingTop;
        var paddingX = parseInt(paddingX.slice(0, paddingX.indexOf("px")));
        var paddingY = parseInt(paddingY.slice(0, paddingY.indexOf("px")));
        return [wh[0] + (paddingX * 2), wh[1] + (paddingY * 2)];
    };
    var formSize = getFormInnerSize(formInner);
    formDiv.style.width = formSize[0];
    formDiv.style.height = formDiv[1];
    formDiv.style.top = (docWH[1] - formSize[1]) / 2;
    formDiv.style.right = (docWH[0] - formSize[0]) / 2;
};
function onSubmit() {
    var form = document.forms.libraryForm;
    var button = form.sendAddForm || null;
    if (button) {
        button.onclick = function(event) {
            hideLibraryContent();
            doPostForm("libraryTHPSContent", form, location.pathname);
            return event.preventDefault();
        };
    }
};
if (document.readyState == "complete") {
    firstCenterForm();
} else {
    if (window.addEventListener) {
        document.addEventListener("DOMContentLoaded", firstCenterForm);
    } else {
        document.attachEvent("DOMContentLoaded", firstCenterForm);
    }
}
function firstCenterForm() {
    var f = document.getElementById("libraryTHPSContent");
    onSubmit();
    showLibraryContent(f);
    centerFormDiv(f);
    if (window.addEventListener) {
        document.removeEventListener("DOMContentLoaded", firstCenterForm);
    } else {
        document.detachEvent("DOMContentLoaded", firstCenterForm);
    }
};