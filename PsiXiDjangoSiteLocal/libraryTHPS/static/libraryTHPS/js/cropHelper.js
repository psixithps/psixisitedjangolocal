function setJCrop() {
    var cropTarget = document.getElementById("cropTarget");
    $(cropTarget).Jcrop({
        trackDocument: true,
        onSelect: grabCorrds,
        onChange: grabCorrds,
        setSelect: typeof (window.initCropCoords) != "undefined" ?
            grabCorrds(initCropCoords) : [0, 0, cropTarget.clientWidth, cropTarget.clientHeight],
    });
};
function grabCorrds(sizeOrCords) {
    var rImg = document.getElementById("cropTarget");
    var ratio = [];
    var keys = Object.keys(sizeOrCords);
    if (keys[0] != "0") {
        var cropImage = document.getElementsByClassName("jcrop-holder")[0];
        var styleW = cropImage.clientWidth;
        var styleH = cropImage.clientHeight;
    } else {
        var styleW = rImg.clientWidth;
        var styleH = rImg.clientHeight;
    }
    try {
        ratio[0] = rImg.naturalWidth / styleW;
        ratio[1] = rImg.naturalHeight / styleH;
    } catch (evr) {
        var nImg = new Image();
        nImg.src = rImg.src;
        ratio[0] = nImg.width / styleW;
        ratio[1] = nImg.height / styleH;
    }
    var coordinates = [];
    if (keys[0] != "0") {
        var keys = Object.keys(sizeOrCords);
        for (var i = 0; i < keys.length - 2; i++) {
            coordinates[i] = Math.round(sizeOrCords[keys[i]] * ratio[i % 2 === 0 ? 0 : 1]);
        }
        var input = document.forms.libraryForm[1];
        input.value = coordinates.join();
    } else {
        for (var i = 0; i < sizeOrCords.length; i++) {
            coordinates[i] = Math.round(sizeOrCords[i] / ratio[i % 2 === 0 ? 0 : 1]);
        }
        return coordinates
    }
};
if (document.readyState == "complete") {
    beginCheckLoad();
} else {
    if (window.addEventListener) {
        document.addEventListener("DOMContentLoaded", beginCheckLoad);
    } else {
        document.attachEvent("DOMContentLoaded", beginCheckLoad);
    }
}
function beginCheckLoad() {
    var image = document.getElementById("cropTarget");
    var link = image.src;
    image.src = "";
    image.src = link;
    if (window.addEventListener) {
        image.addEventListener("load", imgReady);
    } else {
        image.attachEvent("load", imgReady);
    }
    function imgReady(event) {
        var img = this || event.currentTarget;
        if (img && img.width && img.width > 0) {
            image.className = image.className.indexOf(" ") == -1 ? "ready" : image.className + " ready";
            setJCrop();
            centerFormDiv(document.getElementById("libraryTHPSContent"));
            if (window.removeEventListener) {
                document.removeEventListener("DOMContentLoaded", beginCheckLoad);
                image.removeEventListener("load", imgReady);
            } else {
                document.detachEvent("DOMContentLoaded", beginCheckLoad);
                image.detachEvent("load", imgReady);
            }
        } else {
            beginCheckLoad();
        }
    };
};