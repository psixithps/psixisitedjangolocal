import json
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.conf import settings
from libraryTHPS.models import clansMain, clansItemAttrs, clansThumbAttrs
from libraryTHPS.forms import addClanFirst, addClanDetail
from libraryTHPS.forms import addBackground, addLogo
from libraryTHPS.forms import addPlayerFirst, addPlayerDetail
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.http import JsonResponse
from django.shortcuts import redirect

def addLibraryClanSectionRedirect(request, item, editType):
    if request.method == "GET":
        main = get_object_or_404(clansMain, slugName = item)
        if editType == "miniature":
            try:
                thumb = clansThumbAttrs.objects.get(pk = main.pk)
            except clansThumbAttrs.DoesNotExist:
                thumb = None
            if thumb is None:
                if main.logo == False:
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'logo'}))
                        })
                    return redirect("libraryAddClanContentUrl", item, 'logo')
                if main.background == False:
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'ground'}))
                        })
                    return redirect("libraryAddClanContentUrl", item, 'ground')
            if main.logo == False:
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps(reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'logo'}))
                    })
                return redirect("libraryAddClanContentUrl", item, 'logo')
            if main.background == False:
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps(reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'ground'}))
                    })
                return redirect("libraryAddClanContentUrl", item, 'ground')
            if thumb.logoCrop == False:
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps(reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': 'miniature', 'step': 'logo'}))
                    })
                return redirect("libraryAddClanCropUrl", item, 'miniature', 'logo')
            if thumb.backgroundCrop == False:
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps(reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': 'miniature', 'step': 'ground'}))
                    })
                return redirect("libraryAddClanCropUrl", item, 'miniature', 'ground')
            if thumb.isMaded == False:
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps(reverse("libraryAddClanMadeUrl", kwargs = {'item': item, 'section': 'miniature', 'urlSep': '/'}))
                    })
                return redirect("libraryAddClanMadeUrl", item, 'miniature', '/')
            if request.is_ajax():
                return JsonResponse({
                    "redirectTo": json.dumps(reverse("libraryAddClanMadeUrl", kwargs = {'item': item, 'section': 'miniature', 'urlSep': '/'}))
                })
            return redirect("libraryAddClanMadeUrl", item, 'miniature', '/')
        elif editType == "headpiece":
            try:
                head = clansItemAttrs.objects.get(pk = main.pk)
            except clansItemAttrs.DoesNotExist:
                head = None
            if head is None:
                if main.logo == False:
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'logo'}))
                        })
                    return redirect("libraryAddClanContentUrl", item, 'logo')
                if main.background == False:
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'ground'}))
                        })
                    return redirect("libraryAddClanContentUrl", item, 'ground')
            if main.logo == False:
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps(reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'logo'}))
                    })
                return redirect("libraryAddClanContentUrl", item, 'logo')
            if main.background == False:
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps(reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'ground'}))
                    })
                return redirect("libraryAddClanContentUrl", item, 'ground')
            if head.logoCrop == False:
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps(reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': 'page', 'step': 'logo'}))
                    })
                return redirect("libraryAddClanCropUrl", item, 'page', 'logo')
            if head.backgroundCrop == False:
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps(reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': 'page', 'step': 'ground'}))
                    })
                return redirect("libraryAddClanCropUrl", item, 'page', 'ground')
            if head.isMaded == False:
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps(reverse("libraryAddClanMadeUrl", kwargs = {'item': item, 'section': 'miniature', 'urlSep': '/'}))
                    })
                return redirect("libraryAddClanMadeUrl", item, 'miniature', '/')
            if request.is_ajax():
                return JsonResponse({
                    "redirectTo": json.dumps(reverse("libraryAddClanMadeUrl", kwargs = {'item': item, 'section': 'headpiece', 'urlSep': '/'}))
                })
            return redirect("libraryAddClanMadeUrl", item, 'headpiece', '/')
        elif editType == "content":
            if request.is_ajax():
                return JsonResponse({
                    "redirectTo": json.dumps(reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'detail'}))
                    })
            return redirect("libraryAddClanContentUrl", item, 'detail')
        else:
            try:
                thumb = clansThumbAttrs.objects.get(pk = main.pk)
            except clansThumbAttrs.DoesNotExist:
                thumb = None
            try:
                head = clansItemAttrs.objects.get(pk = main.pk)
            except clansItemAttrs.DoesNotExist:
                head = None
            if thumb is not None and head is not None:
                if main.background == False:
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'ground'}))
                        })
                    return redirect("libraryAddClanContentUrl", item, 'ground')
                if main.logo == False:
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'logo'}))
                        })
                    return redirect("libraryAddClanContentUrl", item, 'logo')
                if thumb.backgroundCrop == False or head.backgroundCrop == False:
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'ground'}))
                        })
                    return redirect("libraryAddClanCropUrl", item, '', '', 'ground')
                if thumb.logoCrop == False or head.logoCrop == False:
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'logo'}))
                        })
                    return redirect("libraryAddClanCropUrl", item, '', '', 'logo')
                if thumb.isMaded == False or head.isMaded == False:
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(reverse("libraryAddClanMadeUrl", kwargs = {'item': item, 'section': '', 'urlSep': ''}))
                        })
                    return redirect("libraryAddClanMadeUrl", item, '', '')
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps(reverse("libraryAddClanMadeUrl", kwargs = {'item': item, 'section': '', 'urlSep': ''}))
                    })
                return redirect("libraryAddClanMadeUrl", item, '', '')
            if main.background == False:
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps(reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'ground'}))
                        })
                return redirect("libraryAddClanContentUrl", item, 'ground')
            if main.logo == False:
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps(reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'logo'}))
                    })
                return redirect("libraryAddClanContentUrl", item, 'logo')
            if thumb is not None:
                if head.backgroundCrop == False:
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'ground'}))
                        })
                    return redirect("libraryAddClanCropUrl", item, '', '', 'ground')
                if head.logoCrop == False:
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'logo'}))
                        })
                    return redirect("libraryAddClanCropUrl", item, '', '', 'logo')
                if head.isMaded == False:
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(reverse("libraryAddClanMadeUrl", kwargs = {'item': item, 'section': '', 'urlSep': ''}))
                        })
                        return redirect("libraryAddClanMadeUrl", item, '', '')
            if head is not None:
                if thumb.backgroundCrop == False:
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'ground'}))
                        })
                    return redirect("libraryAddClanCropUrl", item, '', '', 'ground')
                if thumb.logoCrop == False:
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'logo'}))
                        })
                    return redirect("libraryAddClanCropUrl", item, '', '', 'logo')
                if thumb.isMaded == False:
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(reverse("libraryAddClanMadeUrl", kwargs = {'item': item, 'section': '', 'urlSep': ''}))
                        })
                    return redirect("libraryAddClanMadeUrl", item, '', '')
            if main.background == False:
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps(reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'ground'}))
                    })
                return redirect("libraryAddClanContentUrl", item, 'ground')
            if main.logo == False:
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps(reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'logo'}))
                    })
                return redirect("libraryAddClanContentUrl", item, 'logo')
            if request.is_ajax():
                return JsonResponse({
                    "redirectTo": json.dumps(reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'ground'}))
                    })
            return redirect("libraryAddClanCropUrl", item, '', '', 'ground')
