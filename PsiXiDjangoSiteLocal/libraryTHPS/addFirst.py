import json, copy
from collections import OrderedDict
from django.contrib.staticfiles.templatetags.staticfiles import static
from libraryTHPS.forms import addClanFirst, addPlayerFirst
from libraryTHPS.helpers import cookiesEdit
from base.helpers import checkCSRF
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.template.loader import render_to_string as rts
from django.urls import reverse
from django.views.decorators.cache import cache_page

@cache_page(900)
def addLibraryItemNew(request, typeOfItem):
    if request.method == "GET":
        if typeOfItem == "clan":
            if request.is_ajax():
                if "global" in request.GET:
                    return JsonResponse({"newPage": json.dumps({
                        'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                            'parentTemplate': 'base/fullPage/emptyParent.html',
                            'content': ('libraryTHPS', 'add', 'clan', 'first'),
                            'breadcrumbs': ('clans', 'add', 'first'),
                            'url': request.path,
                            'titl': 'Добавление клана: шаг 1',
                            'menu': json.dumps(OrderedDict({
                                "clans": {
                                    "url": [
                                        reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'clans', 'offJS': 'noscripts'}),
                                        reverse("storageUrl", kwargs = {'typeOfItems': 'clans'})
                                        ],
                                    "textname": "Кланы",
                                    "handler": "localNavigation",
                                    "add": {
                                        "textname": "Добавить",
                                        "first": {
                                            "textname": "Имя клана"
                                            }
                                        }
                                    },
                                "players": {
                                    "url": [
                                        reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'players', 'offJS': 'noscripts'}),
                                        reverse("storageUrl", kwargs = {'typeOfItems': 'players'})
                                        ],
                                    "textname": "Игроки",
                                    "handler": "localNavigation",
                                    "add": {
                                        "url": reverse("libraryAddFirstUrl", kwargs = {'typeOfItem': 'player'}),
                                        "textname": "Добавить",
                                        "handler": "formsNavigation"
                                        }
                                    }
                                })),
                            'csrf': '',
                            'form': addClanFirst(
                                isExistItem = False,
                                initial = {'addedBy': request.user.id, 'status': 'n'}
                                ),
                            'fon0': static("libraryTHPS/img/17db.jpg"),
                            'fon1': static("libraryTHPS/img/noise.gif")
                            })
                        })})
                return JsonResponse({"newPage": json.dumps({
                    "templateHead": rts("libraryTHPS/head/add/content.html"),
                    "templateBody": rts("libraryTHPS/body/add/content.html", {
                        'content': ('libraryTHPS', 'add', 'clan', 'first'),
                        'csrf': '',
                        'form': addClanFirst(
                            isExistItem = False,
                            initial = {'addedBy': request.user.id, 'status': 'n'}
                            )
                        }),
                    "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                    'menu': OrderedDict({
                        "clans": {
                            "url": [
                                reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'clans', 'offJS': 'noscripts'}),
                                reverse("storageUrl", kwargs = {'typeOfItems': 'clans'})
                                ],
                            "textname": "Кланы",
                            "handler": "localNavigation",
                            "add": {
                                "textname": "Добавить",
                                "first": {
                                    "textname": "Имя клана"
                                    }
                                }
                            },
                        "players": {
                            "url": [
                                reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'players', 'offJS': 'noscripts'}),
                                reverse("storageUrl", kwargs = {'typeOfItems': 'players'})
                                ],
                            "textname": "Игроки",
                            "handler": "localNavigation",
                            "add": {
                                "url": reverse("libraryAddFirstUrl", kwargs = {'typeOfItem': 'player'}),
                                "textname": "Добавить игрока",
                                "handler": "formsNavigation",
                                }
                            }
                        }),
                    'breadcrumbs': ('clans', 'add', 'first'),
                    "titl": 'Добавление клана: шаг 1',
                    "url": request.path,
                    "fon0": static("libraryTHPS/img/17db.jpg"),
                    "fon1": static("libraryTHPS/img/noise.gif")
                    })})
            return render(request, "libraryTHPS/libraryTHPS.html", {
                'parentTemplate': 'base/fullPage/base.html',
                'content': ('libraryTHPS', 'add', 'clan', 'first'),
                'menu': json.dumps(OrderedDict({
                    "clans": {
                        "url": [
                            reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'clans', 'offJS': 'noscripts'}),
                            reverse("storageUrl", kwargs = {'typeOfItems': 'clans'})
                            ],
                        "textname": "Кланы",
                        "handler": "localNavigation",
                        "add": {
                            "textname": "Добавить",
                            "first": {
                                "textname": "Имя клана"
                                }
                            }
                        },
                    "players": {
                        "url": [
                            reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'players', 'offJS': 'noscripts'}),
                            reverse("storageUrl", kwargs = {'typeOfItems': 'players'})
                            ],
                        "textname": "Игроки",
                        "handler": "localNavigation",
                        "add": {
                            "url": reverse("libraryAddFirstUrl", kwargs = {'typeOfItem': 'player'}),
                            "textname": "Добавить",
                            "handler": "formsNavigation"
                            }
                        }
                    })),
                'breadcrumbs': ('clans', 'add', 'first'),
                'csrf': checkCSRF(request),
                'form': addClanFirst(
                    isExistItem = False,
                    initial = {'addedBy': request.user.id, 'status': 'n'}
                    ),
                'titl': 'Добавление клана: шаг 1',
                'fon0': static("libraryTHPS/img/17db.jpg"),
                'fon1': static("libraryTHPS/img/noise.gif")
                })
        elif typeOfItem == "player":
            if request.is_ajax():
                if "global" in request.GET:
                    return JsonResponse({"newPage": json.dumps({
                        'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                            'parentTemplate': 'base/fullPage/emptyParent.html',
                            'breadcrumbs': ('players', 'add', 'first'),

                            }),
                            'url': '',
                            'titl': ''
                        })})
                return JsonResponse({"newPage": json.dumps({
                    "templateHead": rts("libraryTHPS/head/add/content.html"),
                    "templateBody": rts("libraryTHPS/body/add/content.html", {
                        'content': ('libraryTHPS', 'add', 'player', 'first'),
                        'csrf': '',
                        'form': addPlayerFirst(
                            isExistItem = False,
                            initial = {'addedBy': request.user.id, 'status': 'n'}
                            )
                        }),
                    "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                    'menu': OrderedDict({
                        "clans": {
                            "url": [
                                reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'clans', 'offJS': 'noscripts'}),
                                reverse("storageUrl", kwargs = {'typeOfItems': 'clans'})
                                ],
                            "textname": "Кланы",
                            "handler": "localNavigation",
                            "add": {
                                "textname": "Добавить",
                                "url": reverse("libraryAddFirstUrl", kwargs = {'typeOfItem': 'clan'}),
                                "handler": "localNavigation"
                                }
                            },
                        "players": {
                            "url": [
                                reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'players', 'offJS': 'noscripts'}),
                                reverse("storageUrl", kwargs = {'typeOfItems': 'players'})
                                ],
                            "textname": "Игроки",
                            "handler": "localNavigation",
                            "add": {
                                "textname": "Добавить",
                                "first": {
                                    "textname": "Имя игрока"
                                    }
                                }
                            }
                        }),
                    'breadcrumbs': ('players', 'add', 'first'),
                    "titl": 'Добавление клана: шаг 1',
                    "url": request.path,
                    "fon0": static("libraryTHPS/img/17db.jpg"),
                    "fon1": static("libraryTHPS/img/noise.gif")
                    })})
            return render(request, "libraryTHPS/libraryTHPS.html", {
                'parentTemplate': 'base/fullPage/base.html',
                'content': ('libraryTHPS', 'add', 'player', 'first'),
                'menu': json.dumps(OrderedDict({
                    "clans": {
                        "url": [
                            reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'clans', 'offJS': 'noscripts'}),
                            reverse("storageUrl", kwargs = {'typeOfItems': 'clans'})
                            ],
                        "textname": "Кланы",
                        "handler": "localNavigation",
                        "add": {
                            "url": reverse("libraryAddFirstUrl", kwargs = {'typeOfItem': 'clan'}),
                            "textname": "Добавить",
                            "handler": "localNavigation",
                            }
                        },
                    "players": {
                        "url": [
                            reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'players', 'offJS': 'noscripts'}),
                            reverse("storageUrl", kwargs = {'typeOfItems': 'players'})
                            ],
                        "textname": "Игроки",
                        "handler": "localNavigation",
                        "add": {
                            "textname": "Добавить",
                            "first": {
                                "textname": "Имя игрока"
                                }
                            }
                        }
                    })),
                'breadcrumbs': ('players', 'add', 'first'),
                'csrf': checkCSRF(request),
                'form': addPlayerFirst(
                    isExistItem = False,
                    initial = {'addedBy': request.user.id, 'status': 'n'}
                    ),
                'titl': 'Добавление клана: шаг 1',
                'fon0': static("libraryTHPS/img/17db.jpg"),
                'fon1': static("libraryTHPS/img/noise.gif")
                })
    elif request.method == "POST":
        if typeOfItem == "clan":
            form = addClanFirst(request.POST, isExistItem = False)
            if form.is_valid():
                itemSlug = form.cleaned_data.get("slugName")
                form.save()
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps(reverse("libraryAddClanContentUrl", kwargs = {'item': itemSlug, 'step': 'ground'}))
                        })
                return redirect(reverse("libraryAddClanContentUrl", kwargs = {'item': itemSlug, 'step': 'ground'}))
            if request.is_ajax():
                return JsonResponse({"newPage": json.dumps({
                    "templateHead": rts("libraryTHPS/head/add/content.html"),
                    "templateBody": rts("libraryTHPS/body/add/content.html", {
                        'content': ('libraryTHPS', 'add', 'clan', 'first'),
                        'csrf': '',
                        'form': form
                        }),
                    "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                    'breadcrumbs': ('clans', 'add', 'first'),
                    "titl": 'Добавление клана: шаг 1',
                    "fon0": static("libraryTHPS/img/17db.jpg"),
                    "fon1": static("libraryTHPS/img/noise.gif")
                    })})
            return render(request, "libraryTHPS/libraryTHPS.html", {
                'parentTemplate': 'base/fullPage/base.html',
                'content': ('libraryTHPS', 'add', 'clan', 'first'),
                'breadcrumbs': ('clans', 'add', 'first'),
                'csrf': checkCSRF(request),
                'form': form,
                'titl': 'Добавление клана: шаг 1',
                'fon0': static("libraryTHPS/img/17db.jpg"),
                'fon1': static("libraryTHPS/img/noise.gif")
                })
        elif typeOfItem == "player":
            form = addPlayerFirst(request.POST, isExistItem = False)
            if form.is_valid():
                itemSlug = form.cleaned_data.get("slugName")
                form.save()
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps(reverse("libraryAddClanContentUrl", kwargs = {'item': itemSlug, 'step': 'ground'}))
                        })
                return redirect(reverse("libraryAddClanContentUrl", kwargs = {'item': itemSlug, 'step': 'ground'}))
            if request.is_ajax():
                return JsonResponse({"newPage": json.dumps({
                    "templateHead": rts("libraryTHPS/head/add/content.html"),
                    "templateBody": rts("libraryTHPS/body/add/content.html", {
                        'content': ('libraryTHPS', 'add', 'clan', 'first'),
                        'csrf': '',
                        'form': form
                        }),
                    "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                    'breadcrumbs': ('players', 'add', 'first'),
                    "titl": 'Добавление клана: шаг 1',
                    "fon0": static("libraryTHPS/img/17db.jpg"),
                    "fon1": static("libraryTHPS/img/noise.gif")
                    })})
            return render(request, "libraryTHPS/libraryTHPS.html", {
                'parentTemplate': 'base/fullPage/base.html',
                'content': ('libraryTHPS', 'add', 'clan', 'first'),
                'breadcrumbs': ('players', 'add', 'first'),
                'csrf': checkCSRF(request),
                'form': form, 'titl': 'Добавление клана: шаг 1',
                'fon0': static("libraryTHPS/img/17db.jpg"),
                'fon1': static("libraryTHPS/img/noise.gif")
                })