import os
from django.conf import settings
import json
from django.contrib.staticfiles.templatetags.staticfiles import static
from libraryTHPS.models import clansMain, clansItemAttrs, clansThumbAttrs
from libraryTHPS.forms import addClanFirst, addClanDetail, addBackground, addLogo
from django.shortcuts import get_object_or_404
from libraryTHPS.helpers import filterFutureSteps, buildEditClanMenu
from base.helpers import checkCSRF
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.template.loader import render_to_string as rts
from django.urls import reverse
from django.views.decorators.cache import cache_page

def addLibraryClanItemContent(request, item, step):
    mainItem = get_object_or_404(clansMain, pk = item)
    if request.method == "GET":
        try:
            head = clansItemAttrs.objects.get(pk = item)
        except:
            head = None
        try:
            thumb = clansThumbAttrs.objects.get(pk = item)
        except:
            thumb = None
        if step == "name":
            if request.is_ajax():
                if "global" in request.GET:
                    return JsonResponse({"newPage": json.dumps({
                        'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                            'parentTemplate': 'base/fullPage/emptyParent.html',
                            'content': ('libraryTHPS', 'add', 'clan', 'name'),
                            'breadcrumbs': ('clans', 'editInfo', 'content'),
                            'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'name', item, mainItem.name)),
                            'csrf': '',
                            'form': addClanFirst(
                                isExistItem = True,
                                initial = {'name': mainItem.name, 'editedBy': request.user.id}
                                ),
                            'url': request.path,
                            'titl': '',
                            'fon0': static("libraryTHPS/img/17db.jpg"),
                            'fon1': static("libraryTHPS/img/noise.gif")
                            })
                        })})
                return JsonResponse({"newPage": json.dumps({
                    "templateHead": rts("libraryTHPS/head/add/content.html"),
                    "templateBody": rts("libraryTHPS/body/add/content.html", {
                        'content': ('libraryTHPS', 'add', 'clan', 'name'),
                        'csrf': '',
                        'form': addClanFirst(
                            isExistItem = True,
                            initial = {'name': mainItem.name, 'editedBy': request.user.id}
                            )
                        }),
                    "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                    "menu": buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'name', item, mainItem.name),
                    'breadcrumbs': ('clans', 'editInfo', 'content'),
                    "titl": '',
                    "url": request.path,
                    "fon0": static("libraryTHPS/img/17db.jpg"),
                    "fon1": static("libraryTHPS/img/noise.gif")
                    })})
            return render(request, "libraryTHPS/libraryTHPS.html", {
                'parentTemplate': 'base/fullPage/base.html',
                'content': ('libraryTHPS', 'add', 'clan', 'name'),
                'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'name', item, mainItem.name)),
                'breadcrumbs': ('clans', 'editInfo', 'content'),
                'csrf': checkCSRF(request),
                'form': addClanFirst(
                    isExistItem = True,
                    initial = {'name': mainItem.name, 'editedBy': request.user.id}
                    ),
                'titl': '',
                'fon0': static("libraryTHPS/img/17db.jpg"),
                'fon1': static("libraryTHPS/img/noise.gif")
                })
        elif step == "detail":
            if mainItem.status != "d":
                s = mainItem.status
            else:
                s = "p"
            if request.is_ajax():
                if "global" in request.GET:
                    return JsonResponse({"newPage": json.dumps({
                        'url': request.path,
                        'titl': '',
                        'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                            'parentTemplate': 'base/fullPage/emptyParent.html',
                            'content': ('libraryTHPS', 'add', 'clan', 'detail'),
                            'breadcrumbs': ('clans', 'editInfo', 'detail'),
                            'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'detail', item, mainItem.name)),
                            'csrf': '',
                            'form': addClanDetail(
                                initial = {'description': mainItem.description,
                                           'foundedDate': mainItem.foundedDate,
                                           'editedBy': request.user.id, 'status': s
                                           }
                                ),
                            'fon0': static("libraryTHPS/img/17db.jpg"),
                            'fon1': static("libraryTHPS/img/noise.gif")
                            })
                        })})
                return JsonResponse({"newPage": json.dumps({
                    "templateHead": rts("libraryTHPS/head/add/content.html"),
                    "templateBody": rts("libraryTHPS/body/add/content.html", {
                        'content': ('libraryTHPS', 'add', 'clan', 'detail'),
                        'csrf': '',
                        'form': addClanDetail(
                            initial = {'description': mainItem.description,
                                       'foundedDate': mainItem.foundedDate,
                                       'editedBy': request.user.id, 'status': s
                                       }
                            )
                        }),
                    "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                    'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'detail', item, mainItem.name),
                    'breadcrumbs': ('clans', 'editInfo', 'detail'),
                    "titl": '',
                    "url": request.path,
                    "fon0": static("libraryTHPS/img/17db.jpg"),
                    "fon1": static("libraryTHPS/img/noise.gif")
                    })})
            return render(request, "libraryTHPS/libraryTHPS.html", {
                'parentTemplate': 'base/fullPage/base.html',
                'content': ('libraryTHPS', 'add', 'clan', 'detail'),
                'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'detail', item, mainItem.name)),
                'breadcrumbs': ('clans', 'editInfo', 'detail'),
                'csrf': checkCSRF(request),
                'form': addClanDetail(
                    initial = {'description': mainItem.description,
                               'foundedDate': mainItem.foundedDate,
                               'editedBy': request.user.id, 'status': s
                               }
                    ),
                'titl': '',
                'fon0': static("libraryTHPS/img/17db.jpg"),
                'fon1': static("libraryTHPS/img/noise.gif")
                })
        elif step == "ground":
            if request.is_ajax():
                if "global" in request.GET:
                    return JsonResponse({"newPage": json.dumps({
                        'url': request.path,
                        'titl': '',
                        'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                            'parentTemplate': 'base/fullPage/emptyParent.html',
                            'content': ('libraryTHPS', 'add', 'clan', 'ground'),
                            'breadcrumbs': ('clans', 'editInfo', 'ground'),
                            'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'ground', item, mainItem.name)),
                            'csrf': '',
                            'form': addBackground(),
                            'fon0': static("libraryTHPS/img/17db.jpg"),
                            'fon1': static("libraryTHPS/img/noise.gif")
                            })
                        })})
                return JsonResponse({"newPage": json.dumps({
                    "templateHead": rts("libraryTHPS/head/add/content.html"),
                    "templateBody": rts("libraryTHPS/body/add/image.html", {
                        'content': ('libraryTHPS', 'add', 'clan', 'ground'),
                        'csrf': '',
                        'form': addBackground()
                        }),
                    "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                    'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'ground', item, mainItem.name),
                    'breadcrumbs': ('clans', 'editInfo', 'ground'),
                    "titl": "",
                    "url": request.path,
                    "fon0": static("libraryTHPS/img/17db.jpg"),
                    "fon1": static("libraryTHPS/img/noise.gif")
                    })})
            return render(request, "libraryTHPS/libraryTHPS.html", {
                'parentTemplate': 'base/fullPage/base.html',
                'content': ('libraryTHPS', 'add', 'clan', 'ground'),
                'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'ground', item, mainItem.name)),
                'breadcrumbs': ('clans', 'editInfo', 'ground'),
                'csrf': checkCSRF(request),
                'form': addBackground(),
                'titl': "",
                'fon0': static("libraryTHPS/img/17db.jpg"),
                'fon1': static("libraryTHPS/img/noise.gif")
                })
        elif step == "logo":
            if request.is_ajax():
                if "global" in request.GET:
                    return JsonResponse({"newPage": json.dumps({
                        'url': request.path,
                        'titl': '',
                        'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                            'parentTemplate': 'base/fullPage/emptyParent.html',
                            'content': ('libraryTHPS', 'add', 'clan', 'logo'),
                            'breadcrumbs': ('clans', 'editInfo', 'logo'),
                            'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'logo', item, mainItem.name)),
                            'csrf': '',
                            'form': addLogo(background = mainItem.background),
                            'fon0': static("libraryTHPS/img/17db.jpg"),
                            'fon1': static("libraryTHPS/img/noise.gif")
                            })
                        })})
                return JsonResponse({"newPage": json.dumps({
                    "templateHead": rts("libraryTHPS/head/add/content.html"),
                    "templateBody": rts("libraryTHPS/body/add/image.html", {
                        'content': ('libraryTHPS', 'add', 'clan', 'logo'),
                        'csrf': '',
                        'form': addLogo(
                            background = mainItem.background
                            )
                        }),
                    "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                    'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'logo', item, mainItem.name),
                    'breadcrumbs': ('clans', 'editInfo', 'logo'),
                    "titl": "",
                    "url": request.path,
                    "fon0": static("libraryTHPS/img/17db.jpg"),
                    "fon1": static("libraryTHPS/img/noise.gif")
                    })})
            return render(request, "libraryTHPS/libraryTHPS.html", {
                'parentTemplate': 'base/fullPage/base.html',
                'content': ('libraryTHPS', 'add', 'clan', 'logo'),
                'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'logo', item, mainItem.name)),
                'breadcrumbs': ('clans', 'editInfo', 'logo'),
                'csrf': checkCSRF(request),
                'form': addLogo(background = mainItem.background),
                'titl': "", 'fon0': static("libraryTHPS/img/17db.jpg"),
                'fon1': static("libraryTHPS/img/noise.gif")
                })
    elif request.method == "POST":
        if step == "name":
            form = addClanFirst(request.POST, instance = mainItem, isExistItem = True, slug = item)
            if form.is_valid():
                newName = form.cleaned_data["slugName"]
                if mainItem.background == True:
                    mainItem.background.name = mainItem.background.name.replace(item, newName)
                if mainItem.logo == True:
                    mainItem.logo.name = mainItem.logo.name.replace(item, newName)
                form.save()
                try:
                    head = clansItemAttrs.objects.get(pk = item)
                except:
                    head = None
                try:
                    thumb = clansThumbAttrs.objects.get(pk = item)
                except:
                    thumb = None
                if head is not None:
                    head.clanItem = mainItem
                    if head.backgroundCrop == True:
                        head.backgroundCropUrl = head.backgroundCropUrl.replace(item, newName)
                    if head.logoCrop == True:
                        head.logoCropUrl = head.logoCropUrl.replace(item, newName)
                    head.save()
                if thumb is not None:
                    thumb.clanItem = mainItem
                    if thumb.backgroundCrop == True:
                        thumb.backgroundCropUrl = thumb.backgroundCropUrl.replace(item, newName)
                    if thumb.logoCrop == True:
                        thumb.logoCropUrl = thumb.logoCropUrl.replace(item, newName)
                    thumb.save()
                clansMain.objects.get(pk = item).delete()
                if os.path.exists(os.path.normpath("%s/libraryTHPS/clan/%s/" % (settings.MEDIA_ROOT, item))): # Убрать на боевом
                    os.rename(os.path.normpath("%s/libraryTHPS/clan/%s/" % (settings.MEDIA_ROOT, item)),
                              os.path.normpath("%s/libraryTHPS/clan/%s/" % (settings.MEDIA_ROOT, newName)))
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps("%s%s" % (reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': newName}), "?type=fullversion"))
                        }) 
                return redirect("storageShowItemUrl", 'clan', item)
            if request.is_ajax():
                return JsonResponse({"newPage": json.dumps({
                    "templateBody": rts("libraryTHPS/body/add/content.html", {
                        'content': ('libraryTHPS', 'add', 'clan', 'name'),
                        'csrf': '',
                        'form': form
                        }),
                    })})
            return render(request, "libraryTHPS/libraryTHPS.html", {
                'parentTemplate': 'base/fullPage/base.html',
                'content': ('libraryTHPS', 'add', 'clan', 'name'),
                'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem), 'content', 'name', item, mainItem.name)),
                'breadcrumbs': ('clans', 'editInfo', 'name'),
                'csrf': checkCSRF(request),
                'form': form,
                'titl': '',
                'fon0': static("libraryTHPS/img/17db.jpg"),
                'fon1': static("libraryTHPS/img/noise.gif")
                })
        elif step == "detail":
            form = addClanDetail(request.POST, instance = mainItem)
            if form.is_valid():
                form.save()
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps(None)
                        })
                return redirect(request.path)
                if cookie is None:
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps("%s%s" % (reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}), "?type=fullversion"))
                            }) 
                    return redirect("storageShowItemUrl", 'clan', item)
                if cookie == "content":
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'ground'}))
                            })
                    return redirect("libraryAddClanContentUrl", item, 'ground')
                if cookie == "":
                    if mainItem.background == True:
                        if request.is_ajax():
                            return JsonResponse({
                                "redirectTo": json.dumps(reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'ground'}))
                            })
                        return redirect("libraryAddClanCropUrl", item, '', '', 'ground')
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'ground'}))
                        })
                    return redirect("libraryAddClanContentUrl", item, 'ground')
            if request.is_ajax():
                return JsonResponse({"newPage": json.dumps({
                    "templateHead": rts("libraryTHPS/head/add/content.html"),
                    "templateBody": rts("libraryTHPS/body/add/content.html", {
                        'content': ('libraryTHPS', 'add', 'clan', 'detail'),
                        'csrf': '',
                        'form': form
                        }),
                    "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                    'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'detail', item, mainItem.name),
                    'breadcrumbs': ('clans', 'add', 'detail'),
                    "titl": '',
                    "url": request.path,
                    "fon0": static("libraryTHPS/img/17db.jpg"),
                    "fon1": static("libraryTHPS/img/noise.gif")
                    })})
            return render(request, "libraryTHPS/libraryTHPS.html", {
                'parentTemplate': 'base/fullPage/base.html',
                'content': ('libraryTHPS', 'add', 'clan', 'detail'),
                'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'detail', item, mainItem.name)),
                'breadcrumbs': ('clans', 'add', 'detail'),
                'csrf': checkCSRF(request),
                'form': form,
                'titl': '',
                'fon0': static("libraryTHPS/img/17db.jpg"),
                'fon1': static("libraryTHPS/img/noise.gif")
                })
        elif step == "ground":
            form = addBackground(request.POST, request.FILES, instance = mainItem)
            if form.is_valid():
                form.save()
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps("%s%s" % (reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}), "?type=fullversion"))
                    })
                return redirect("storageShowItemUrl", 'clan', item) 
                if cookie is None:
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps("%s%s" % (reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}), "?type=fullversion"))
                        }) 
                    return redirect("storageShowItemUrl", 'clan', item)
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps(reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'logo'}))
                        })
                return redirect("libraryAddClanContentUrl", item, 'logo')
            if request.is_ajax():
                return JsonResponse({"newPage": json.dumps({
                    "templateHead": rts("libraryTHPS/head/add/content.html"),
                    "templateBody": rts("libraryTHPS/body/add/image.html", {
                        'content': ('libraryTHPS', 'add', 'clan', 'ground'),
                        'csrf': '',
                        'form': form
                        }),
                    "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                    'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'ground', item, mainItem.name),
                    'breadcrumbs': ('clans', 'editInfo', 'ground'),
                    "titl": '',
                    "url": request.path,
                    "fon0": static("libraryTHPS/img/17db.jpg"),
                    "fon1": static("libraryTHPS/img/noise.gif") 
                    })})
            return render(request, "libraryTHPS/libraryTHPS.html", {
                'parentTemplate': 'base/fullPage/base.html',
                'content': ('libraryTHPS', 'add', 'clan', 'ground'),
                'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'ground', item, mainItem.name)),
                'breadcrumbs': ('clans', 'editInfo', 'ground'),
                'csrf': checkCSRF(request),
                'form': form,
                'titl': '',
                'fon0': static("libraryTHPS/img/17db.jpg"),
                'fon1': static("libraryTHPS/img/noise.gif")
                })
        elif step == "logo":
            form = addLogo(request.POST, request.FILES, instance = mainItem,
                           background = mainItem.background)
            if form.is_valid():
                form.save()
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps("%s%s" % (reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}), "?type=fullversion"))
                        }) 
                return redirect("storageShowItemUrl", 'clan', item)
            if request.is_ajax():
                return JsonResponse({"newPage": json.dumps({
                    "templateHead": rts("libraryTHPS/head/add/content.html"),
                    "templateBody": rts("libraryTHPS/body/add/image.html", {
                        'content': ('libraryTHPS', 'add', 'clan', 'logo'),
                        'csrf': '',
                        'form': form
                        }),
                    "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                    'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'logo', item, mainItem.name),
                    'breadcrumbs': ('clans', 'editInfo', 'logo'),
                    "titl": '',
                    "url": request.path,
                    "fon0": static("libraryTHPS/img/17db.jpg"),
                    "fon1": static("libraryTHPS/img/noise.gif")
                    })})
            return render(request, "libraryTHPS/libraryTHPS.html", {
                'parentTemplate': 'base/fullPage/base.html',
                'content': ('libraryTHPS', 'add', 'clan', 'logo'),
                'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'logo', item, mainItem.name)),
                'breadcrumbs': ('clans', 'editInfo', 'logo'),
                'csrf': checkCSRF(request),
                'form': form,
                'titl': '',
                'fon0': static("libraryTHPS/img/17db.jpg"),
                'fon1': static("libraryTHPS/img/noise.gif")
                })