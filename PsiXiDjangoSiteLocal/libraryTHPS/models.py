import os # Убрать на боевом
import shutil
from datetime import datetime
from django.conf import settings # Убрать на боевом
from django.db import models
from base.models import STATUS
from django.core.validators import MaxValueValidator
from django.core.validators import validate_comma_separated_integer_list

class stylesShip(models.Model):
	name = models.SlugField(max_length = 50, blank = False, unique = True, primary_key = True, default = "")
	tags = models.CharField(max_length = 2000, blank = True, default = "")
	description = models.TextField(max_length = 500, blank = False)
	sampleVideoUrl = models.URLField(max_length = 200, blank = True)
	image = models.ImageField(upload_to = "libraryTHPS/style/", blank = True, null = True)
	status = models.CharField(max_length = 1, choices = STATUS)
	createdBy = models.ForeignKey('psixiSocial.mainUserProfile', swappable = True, blank = False, on_delete = models.SET_NULL, null = True)

	class Meta:
		permissions = (("can_see", "Может видеть"),)

	def __str__(self):
		return self.name

def generateBackgroundPath(self, imageName):
	return "libraryTHPS/clan/%s/background/%s/" % (self.slugName, imageName)

def generateLogoPath(self, imageName):
	return "libraryTHPS/clan/%s/logo/%s/" % (self.slugName, imageName)

class clansMain(models.Model):
	slugName = models.SlugField(max_length = 20, blank = True, unique = True, default = "", primary_key = True)
	background = models.ImageField(upload_to = generateBackgroundPath, blank = True, null = True, default = None)
	logo = models.ImageField(upload_to = generateLogoPath, blank = True, null = True, default = None)
	bckgrAndLgIdent = models.BooleanField(blank = False, default = False) # True, если лого это копия из background
	name = models.CharField(max_length = 20, blank = False, unique = True, default = "")
	tags = models.CharField(max_length = 2000, blank = True, default = "")
	description = models.TextField(max_length = 500, blank = True)
	foundedDate = models.DateField(blank = True, null = True)
	clanUnionShips = models.ManyToManyField('self', blank = True)
	clanInStyles = models.ManyToManyField('stylesShip', blank = True) # В каких стилях играет клан
	members = models.ManyToManyField('playersShip', related_name = 'playersShip+', blank = True)
	status = models.CharField(max_length = 1, choices = STATUS)
	addedBy = models.ForeignKey('psixiSocial.mainUserProfile', related_name = 'psixiSocial.mainUserProfile+', swappable = True, blank = False, on_delete = models.PROTECT)
	editedBy = models.ForeignKey('psixiSocial.mainUserProfile', related_name = 'psixiSocial.mainUserProfile+', swappable = True, blank = True, on_delete = models.PROTECT, default = None, null = True)
	leader = models.CharField(max_length = 70, choices = members.name, blank = True, default = "")
	createdBy = models.CharField(max_length = 70, choices = members.name, blank = True, default = "")
	pubDate = models.DateTimeField(auto_now_add = True)
	editDate = models.DateTimeField(auto_now = True)

	def __str__(self):
		return self.name

	def delete(self, *args, **kwargs):
		mediaPath = os.path.join(settings.MEDIA_ROOT, "/libraryTHPS/clan/", self.pk)
		if os.path.exists(mediaPath):
			shutil.rmtree(mediaPath)
		cachePath = os.path.join(settings.MEDIA_ROOT, "/libraryTHPS/cache/clan/", self.pk)
		if os.path.exists(cachePath):
			shutil.rmtree(cachePath)
		super(clansMain, self).delete(*args, **kwargs)

	class Meta:
		permissions = (("can_see", "Может видеть"),)

def generateBackgroundCropThumbPath(self, imageName):
	name, format = imageName.split(".")
	return "libraryTHPS/clan/%s/crop/thumb/background/%s__%s.%s/" % (self.clanItem_id, name, datetime.now().strftime('%Y-%m-%d-%H-%M-%S'), format)

def generateLogoCropThumbPath(self, imageName):
	name, format = imageName.split(".")
	return "libraryTHPS/clan/%s/crop/thumb/logo/%s__%s.%s/" % (self.clanItem_id, name, datetime.now().strftime('%Y-%m-%d-%H-%M-%S'), format)

class clansThumbAttrs(models.Model):
	clanItem = models.OneToOneField(clansMain, to_field = "slugName", on_delete = models.CASCADE, primary_key = True)
	isMaded = models.PositiveSmallIntegerField(blank = True, default = 0) # 0 - ничего; 1 - лого; 2 - имя; 3 - лого и имя;
	logoCrop = models.ImageField(upload_to = generateLogoCropThumbPath, blank = True, null = True, default = None)
	backgroundCrop = models.ImageField(upload_to = generateBackgroundCropThumbPath, blank = True, null = True, default = None)
	backgroundCropCoordinates = models.CharField(max_length = 20, blank = True, validators = [validate_comma_separated_integer_list])
	logoCropCoordinates = models.CharField(max_length = 20, blank = True, validators = [validate_comma_separated_integer_list])
	logoPosition = models.CharField(max_length = 20, blank = True, validators = [validate_comma_separated_integer_list])
	namePosition = models.CharField(max_length = 20, blank = True, validators = [validate_comma_separated_integer_list])
	showFormat = models.PositiveSmallIntegerField(blank = False, default = 0) # 0 -Текст; 1 - Фон из оригинала; 2 - лого из оригинала 3 - Фон; 4 - лого; 5 -
																		   # Наложение
																																					 # лого
																																																							   # на
																																																																										 # фон;
																																																																																												   # 6
																																																																																												   # -
																																																																																												   # Наложение
																																																																																												   # (resized
																																																																																												   # background);
																																																																																												   # 7
																																																																																												   # -
																																																																																												   # Наложение
																																																																																												   # (resized
																																																																																												   # logo);
																																																																																												   # 8
																																																																																												   # -
																																																																																												   # Наложение
																																																																																												   # (resized
																																																																																												   # оба)
	logoProportions = models.CharField(max_length = 20, blank = True, validators = [validate_comma_separated_integer_list])
	nameProportions = models.CharField(max_length = 20, blank = True, validators = [validate_comma_separated_integer_list])
	addedBy = models.ForeignKey('psixiSocial.mainUserProfile', related_name = 'psixiSocial.mainUserProfile+', swappable = True, blank = True, on_delete = models.PROTECT, default = None, null = True)
	editedBy = models.ForeignKey('psixiSocial.mainUserProfile', related_name = 'psixiSocial.mainUserProfile+', swappable = True, blank = True, on_delete = models.PROTECT, default = None, null = True)
	pubDate = models.DateTimeField(auto_now_add = True)
	editDate = models.DateTimeField(auto_now = True)

	def __str__(self):
		try:
			return clansMain.objects.get(pk = self.clanItem_id).name
		except:
			return ""

	def save(self, *args, **kwargs):
		try:
			thumb = clansThumbAttrs.objects.get(pk = self.clanItem_id)
			if thumb.showFormat > 0 and thumb.showFormat != self.showFormat:
				if self.showFormat == 4:
					for path, dirs, files in os.walk(os.path.normpath("%s/libraryTHPS/cache/clan/%s/" % (settings.MEDIA_ROOT, self.pk))):
						currentClan = path
						for typeOfItem in dirs:
							for path, dirs, files in os.walk(os.path.join(currentClan, typeOfItem)):
								for dir in dirs:
									if dir == "logo":
										for path, dirs, files in os.walk(os.path.join(currentClan, typeOfItem, dir, "crop")):
											for image in files:
												os.remove(os.path.join(path, image))
				if self.showFormat == 5:
					for path, dirs, files in os.walk(os.path.normpath("%s/libraryTHPS/cache/clan/%s/" % (settings.MEDIA_ROOT, self.pk))):
						currentClan = path
						for typeOfItem in dirs:
							for path, dirs, files in os.walk(os.path.join(currentClan, typeOfItem)):
								for dir in dirs:
									for path, dirs, files in os.walk(os.path.join(currentClan, typeOfItem, dir, "crop")):
										for image in files:
											os.remove(os.path.join(path, image))
		except:
			pass
		super(clansThumbAttrs, self).save(*args, **kwargs)

	def delete(self, *args, **kwargs):
		self.backgroundCrop.delete(save = False)
		self.logoCrop.delete(save = False)
		cachePath = os.path.join(settings.MEDIA_ROOT, "/libraryTHPS/cache/clan/", self.clanItem_id, "thumb")
		if os.path.exists(cachePath):
			shutil.rmtree(cachePath)
		main = clansMain.objects.get(pk = self.clanItem_id)
		main.showFormat = 0
		main.save()
		super(clansThumbAttrs, self).delete(*args, **kwargs)

def generateBackgroundCropHeadPath(self, imageName):
	name, format = imageName.split(".")
	return "libraryTHPS/clan/%s/crop/head/background/%s__%s.%s/" % (self.clanItem_id, name, datetime.now().strftime('%Y-%m-%d-%H-%M-%S'), format)

def generateLogoCropHeadPath(self, imageName):
	name, format = imageName.split(".")
	return "libraryTHPS/clan/%s/crop/head/logo/%s__%s.%s/" % (self.clanItem_id, name, datetime.now().strftime('%Y-%m-%d-%H-%M-%S'), format)

class clansItemAttrs(models.Model):
	clanItem = models.OneToOneField(clansMain, to_field = "slugName", on_delete = models.CASCADE, primary_key = True)
	isMaded = models.PositiveSmallIntegerField(blank = True, default = 0) # 0 - ничего; 1 - лого; 2 - имя; 3 - лого и имя;
	logoCrop = models.ImageField(upload_to = generateLogoCropHeadPath, blank = True, null = True, default = None)
	backgroundCrop = models.ImageField(upload_to = generateBackgroundCropHeadPath, blank = True, null = True, default = None)
	backgroundCropCoordinates = models.CharField(max_length = 20, blank = True, validators = [validate_comma_separated_integer_list])
	logoCropCoordinates = models.CharField(max_length = 20, blank = True, validators = [validate_comma_separated_integer_list])
	logoPosition = models.CharField(max_length = 20, blank = True, validators = [validate_comma_separated_integer_list])
	namePosition = models.CharField(max_length = 20, blank = True, validators = [validate_comma_separated_integer_list])
	nameProportions = models.CharField(max_length = 20, blank = True, validators = [validate_comma_separated_integer_list])
	logoProportions = models.CharField(max_length = 20, blank = True, validators = [validate_comma_separated_integer_list])
	showFormat = models.PositiveSmallIntegerField(blank = False, default = 0) # 0 -Текст; 1 - Фон из оригинала; 2 - лого из оригинала 3 - Фон; 4 - лого; 5 -
																		   # Наложение
																																					 # лого
																																																							   # на
																																																																										 # фон;
																																																																																												   # 6
																																																																																												   # -
																																																																																												   # Наложение
																																																																																												   # (resized
																																																																																												   # background);
																																																																																												   # 7
																																																																																												   # -
																																																																																												   # Наложение
																																																																																												   # (resized
																																																																																												   # logo);
																																																																																												   # 8
																																																																																												   # -
																																																																																												   # Наложение
																																																																																												   # (resized
																																																																																												   # оба)
	clanColor = models.CharField(max_length = 20, blank = True, validators = [validate_comma_separated_integer_list])
	addedBy = models.ForeignKey('psixiSocial.mainUserProfile', related_name = 'psixiSocial.mainUserProfile+', swappable = True, blank = True, on_delete = models.PROTECT, default = None, null = True)
	editedBy = models.ForeignKey('psixiSocial.mainUserProfile', related_name = 'psixiSocial.mainUserProfile+', swappable = True, blank = True, on_delete = models.PROTECT, default = None, null = True)
	pubDate = models.DateTimeField(auto_now_add = True)
	editDate = models.DateTimeField(auto_now = True)

	def __str__(self):
		try:
			return clansMain.objects.get(pk = self.clanItem_id).name
		except:
			return ""

	def delete(self, *args, **kwargs):
		self.backgroundCrop.delete(save = False)
		self.logoCrop.delete(save = False)
		cachePath = os.path.join(settings.MEDIA_ROOT, "/libraryTHPS/cache/clan/", self.clanItem_id, "head")
		if os.path.exists(cachePath):
			shutil.rmtree(cachePath)
		super(clansThumbAttrs, self).delete(*args, **kwargs)

class playersShip(models.Model):
	slugName = models.SlugField(max_length = 25, blank = False, unique = True, default = "", primary_key = True)
	name = models.CharField(max_length = 25, blank = False, unique = True, default = "")
	tags = models.CharField(max_length = 2000, blank = True, default = "")
	description = models.TextField(max_length = 500, blank = True)
	foundedDate = models.DateField(blank = True, null = True)
	playerIn = models.ForeignKey(clansMain, blank = True, on_delete = models.CASCADE, null = True)
	playerInStyles = models.ManyToManyField('stylesShip', blank = True) # В каких стилях играет игрок
	image = models.ImageField(upload_to = "libraryTHPS/player/", blank = True, null = True)
	status = models.CharField(max_length = 1, choices = STATUS)
	createdBy = models.ForeignKey('psixiSocial.mainUserProfile', swappable = True, blank = True, on_delete = models.SET_NULL, null = True) # Абстрактная модель User тоже в кавычках

	class Meta:
		permissions = (("can_see", "Может видеть"),)

	def __str__(self):
		return self.name

class mapsShip(models.Model):
	name = models.SlugField(max_length = 70, blank = False, unique = True, primary_key = True, default = "")
	parts = models.ManyToManyField('libraryTHPS.seriesship', blank = True) # В каких частях встречаются карты
	tags = models.CharField(max_length = 2000, blank = True, default = "")
	description = models.TextField(max_length = 500, blank = True)
	image = models.ImageField(upload_to = "libraryTHPS/map/", blank = True, null = True)
	status = models.CharField(max_length = 1, choices = STATUS)
	createdBy = models.ForeignKey('psixiSocial.mainUserProfile', swappable = True, blank = True, on_delete = models.SET_NULL, null = True) # Абстрактная модель User тоже в кавычках

	class Meta:
		permissions = (("can_see", "Может видеть"),)

	def __str__(self):
		return self.name

class seriesShip(models.Model):
	name = models.SlugField(max_length = 70, blank = False, unique = True, primary_key = True, default = "")
	tags = models.CharField(max_length = 2000, blank = True, default = "")
	description = models.TextField(max_length = 500, blank = True)
	image = models.ImageField(upload_to = "libraryTHPS/series/", blank = True, null = True)
	status = models.CharField(max_length = 1, choices = STATUS)
	releaseDate = models.DateField(blank = True, null = True)
	createdBy = models.ForeignKey('psixiSocial.mainUserProfile', swappable = True, blank = True, on_delete = models.SET_NULL, null = True) # Абстрактная модель User тоже в кавычках

	class Meta:
		permissions = (("can_see", "Может видеть"),)

	def __str__(self):
		return self.name

class searchingHelper(models.Model):
	name = models.SlugField(max_length = 30, blank = False, unique = True, primary_key = True, default = "") # Название фильтра
	description = models.CharField(max_length = 500, blank = True, default = "") # Цель его существования
	ignoredSymbols = models.CharField(max_length = 5000, blank = True, default = "")
	specialSymbols = models.CharField(max_length = 5000, blank = True, default = "")
	editedBy = models.ForeignKey('psixiSocial.mainUserProfile', swappable = True, blank = False, related_name = 'psixiSocial.mainUserProfile+', on_delete = models.PROTECT, default = None)
	addedBy = models.ForeignKey('psixiSocial.mainUserProfile', swappable = True, blank = False, related_name = 'psixiSocial.mainUserProfile+', on_delete = models.PROTECT, default = None)

	class Meta:
		permissions = (("can_see", "Может видеть"),)

	def __str__(self):
		return self.name

class libraryClanThumbService(models.Model):
	toUser = models.OneToOneField('psixiSocial.mainUserProfile', swappable = True, blank = False, primary_key = True, on_delete = models.CASCADE, default = "")
	thumbItemWidth = models.PositiveSmallIntegerField(blank = False, validators = [MaxValueValidator(1500)], default = 300)
	thumbItemHeight = models.PositiveSmallIntegerField(blank = False, validators = [MaxValueValidator(1500)], default = 300)
	thumbItemMarginX = models.PositiveSmallIntegerField(blank = False, validators = [MaxValueValidator(60)], default = 30)
	thumbItemMarginY = models.PositiveSmallIntegerField(blank = False, validators = [MaxValueValidator(60)], default = 30)
	thumbItemPaddingUl = models.PositiveSmallIntegerField(blank = False, validators = [MaxValueValidator(100)], default = 15)

	def save(self, *args, **kwargs):
		for path, dirs, files in os.walk(os.path.normpath("%s/libraryTHPS/cache/clan/" % settings.MEDIA_ROOT)):
			for clan in dirs:
				curClanPath = os.path.normpath("%s/%s/thumb/%d/" % (path, clan, self.toUser_id))
				for path, dirs, files in os.walk(curClanPath):
					for dir in dirs:
						curDir = dir
						for path, dirs, files in os.walk(os.path.normpath("%s/%s/" % (curClanPath, curDir))):
							for image in files:
								os.remove(os.path.join(path, image))
						for path, dirs, files in os.walk(os.path.normpath("%s/%s/%s/" % (curClanPath, curDir, "crop"))):
							for dir in dirs:
								for image in files:
									os.remove(os.path.join(path, dir, image))
		super(libraryClanThumbService, self).save(*args, **kwargs)

	def delete(self, *args, **kwargs):
		for path, dirs, files in os.walk(os.path.normpath("%s/libraryTHPS/cache/clan/" % settings.MEDIA_ROOT)):
			for clan in dirs:
				current = os.path.normpath("%s/%s/thumb/%s" % (path, clan, self.toUser_id))
				if os.path.exists(current):
					shutil.rmtree(current)
		super(libraryClanThumbService, self).delete(*args, **kwargs)


class libraryClanThumbDefaultService(models.Model):
	thumbItemWidth = models.PositiveSmallIntegerField(blank = False, validators = [MaxValueValidator(1500)], default = 400)
	thumbItemHeight = models.PositiveSmallIntegerField(blank = False, validators = [MaxValueValidator(1500)], default = 400)
	thumbItemMarginX = models.PositiveSmallIntegerField(blank = False, validators = [MaxValueValidator(60)], default = 30)
	thumbItemMarginY = models.PositiveSmallIntegerField(blank = False, validators = [MaxValueValidator(60)], default = 30)
	thumbItemPaddingUl = models.PositiveSmallIntegerField(blank = False, validators = [MaxValueValidator(100)], default = 15)

	def save(self, *args, **kwargs):
		self.pk = 1
		for path, dirs, files in os.walk(os.path.normpath("%s/libraryTHPS/cache/clan/" % settings.MEDIA_ROOT)):
			for clan in dirs:
				curSettings = os.path.normpath("%s/%s/thumb/default/" % (path, clan))
				for path, dirs, files in os.walk(curSettings):
					for dir in dirs:
						for image in files:
							os.remove(os.path.join(path, dir, image))
				for path, dirs, files in os.walk(os.path.normpath("%s/crop/" % curSettings)):
					for dir in dirs:
						for image in files:
							os.remove(os.path.join(path, dir, image))
		super(libraryClanThumbDefaultService, self).save(*args, **kwargs)

	def delete(self, *args, **kwargs):
		for path, dirs, files in os.walk(os.path.normpath("%s/libraryTHPS/cache/clan/" % settings.MEDIA_ROOT)):
			for clanDir in dirs:
				current = os.path.normpath("%s/thumb/default/" % clanDir)
				if os.path.exists(current):
					shutil.rmtree(current)
		super(libraryClanThumbDefaultService, self).delete(*args, **kwargs)

class libraryClanPageService(models.Model):
	toUser = models.OneToOneField('psixiSocial.mainUserProfile', swappable = True, blank = False, primary_key = True, on_delete = models.CASCADE, default = "")
	pageItemWidth = models.PositiveSmallIntegerField(blank = False, validators = [MaxValueValidator(1000)], default = 400)
	pageItemHeight = models.PositiveSmallIntegerField(blank = False, validators = [MaxValueValidator(1000)], default = 400)
	pageItemMarginX = models.PositiveSmallIntegerField(blank = False, validators = [MaxValueValidator(200)], default = 70)
	pageItemMarginY = models.PositiveSmallIntegerField(blank = False, validators = [MaxValueValidator(200)], default = 70)

class libraryClanPageDefaultService(models.Model):
	pageItemWidth = models.PositiveSmallIntegerField(blank = False, validators = [MaxValueValidator(1000)], default = 400)
	pageItemHeight = models.PositiveSmallIntegerField(blank = False, validators = [MaxValueValidator(1000)], default = 400)
	pageItemMarginX = models.PositiveSmallIntegerField(blank = False, validators = [MaxValueValidator(200)], default = 70)
	pageItemMarginY = models.PositiveSmallIntegerField(blank = False, validators = [MaxValueValidator(200)], default = 70)