import json
from collections import OrderedDict
from django.core.paginator import Paginator, EmptyPage
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, redirect
from django.template.loader import render_to_string as rts
from django.urls import reverse
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.conf import settings # Убрать на боевом
from libraryTHPS.models import libraryClanThumbDefaultService, libraryClanThumbService, \
   libraryClanPageDefaultService, libraryClanPageService, playersShip
from libraryTHPS.models import clansMain, clansThumbAttrs, clansItemAttrs
from libraryTHPS.resize import storageResizing, reCalculateCoordinates, bigBackgroundResize, grabMadeLogoSize, getResizedBackgroundSize

def loadStorageClanElements(request, itemPage, **kwargs):
    contentList = []
    # Рассчёт количества элементов на 1 странице в пагинаторе
    signedUser = request.user.is_authenticated()
    if signedUser:
        try:
            clanSettings = libraryClanThumbService.objects.get(toUser = request.user)
        except libraryClanThumbService.DoesNotExist:
            clanSettings = libraryClanThumbDefaultService.objects.get(pk = 1)
            signedUser = False
    else:
        clanSettings = libraryClanThumbDefaultService.objects.get(pk = 1)
    itemMarginWidth = clanSettings.thumbItemMarginX
    itemMarginHeight = clanSettings.thumbItemMarginY
    if kwargs.get('dAreaSize', None):
        placePaddingX = kwargs['dAreaSize']["width"] * 10 // 100
        placePaddingY = kwargs['dAreaSize']["height"] * 10 // 100
    # Здесь в блоке else - случай выключенного javascript
    else:
        placePaddingX = 30
        placePaddingY = 30
    allItems = clansMain.objects.all().order_by('-pubDate')
    # Надо делать рассчёт количества выводимых элементов для одной страницы,
    # В зависимости от window.width/height, но
    # Делать это придётся на фронтенде
    clansItems = Paginator(allItems, 6, orphans = 0)
    userId = request.user.id
    itemWH = (clanSettings.thumbItemWidth, clanSettings.thumbItemHeight)
    resize = storageResizing(userId, signedUser, itemWH, "thumb")
    try:
        curPageItems = clansItems.page(itemPage)
    except:
        contentsDict = None # Тут надо написать несколько исключений:
        # Вне доступного диапазона страниц, пустая страница итп
    for curItem in curPageItems:
        if curItem.status == "p":
            try:
                thumb = clansThumbAttrs.objects.get(pk = curItem.pk)
            except clansThumbAttrs.DoesNotExist:
                thumb = None
            if thumb is None:
                contentsDict = {
                    "slug": curItem.slugName,
                    "name": curItem.name, "format": 0
                    }
            else:
                if thumb.showFormat == 0:
                    contentsDict = {
                        "slug": curItem.slugName,
                        "name": curItem.name, "format": 0
                        }
                elif thumb.showFormat == 1:
                    slug = curItem.slugName
                    image = curItem.background
                    contentsDict = {
                        "slug": slug,
                        "name": curItem.name,
                        "background": resize.resizeBackground(slug, (image.width, image.height), image.url),
                        "format": 1
                        }
                elif thumb.showFormat == 2:
                    slug = curItem.slugName
                    image = curItem.logo
                    contentsDict = {
                        "slug": slug,
                        "name": curItem.name,
                        "logo": resize.resizeLogo(slug, (image.width, image.height), image.url),
                        "format": 2
                        }
                elif thumb.showFormat == 3:
                    slug = curItem.slugName
                    image = thumb.backgroundCrop
                    contentsDict = {
                        "slug": slug,
                        "name": curItem.name,
                        "background": resize.resizeBackgroundCrop(slug, (image.width, image.height), image.url),
                        "format": 1
                        }
                elif thumb.showFormat == 4:
                    slug = curItem.slugName
                    image = thumb.logoCrop
                    contentsDict = {
                        "slug": slug,
                        "name": curItem.name,
                        "logo": resize.resizeLogoCrop(slug, (image.width, image.height), image.url),
                        "format": 2
                        }
                elif thumb.showFormat == 5:
                    slug = curItem.slugName
                    backgroundCroppedSize = (thumb.backgroundCrop.width, thumb.backgroundCrop.height)
                    logoCroppedSize = (thumb.logoCrop.width, thumb.logoCrop.height)
                    background = resize.resizeBackgroundCrop(slug, backgroundCroppedSize, thumb.backgroundCrop.url)
                    resizedBackground = getResizedBackgroundSize(background)
                    logoSize = grabMadeLogoSize(backgroundCroppedSize, resizedBackground, logoCroppedSize, tuple(map(int, thumb.logoProportions.split(","))))
                    contentsDict = {
                        "slug": slug,
                        "name": curItem.name,
                        "background": background,
                        "logo": resize.resizeLogoCrop(slug, logoSize, thumb.logoCrop.url),
                        "coordinates": reCalculateCoordinates(backgroundCroppedSize, resizedBackground, tuple(map(int, thumb.logoPosition.split(","))))
                        }
            contentList.append(contentsDict)
    return (contentList, curPageItems.has_next(), {"windPaddingX": placePaddingX, "windPaddingY": placePaddingY},
            {"marginX": itemMarginWidth, "marginY": itemMarginHeight})

def libraryStoragePage(request, typeOfItems, page, **kwargs):
    if request.method == "GET":
        if typeOfItems == "clans":
            if request.is_ajax():
                if "global" in request.GET:
                    return JsonResponse({"newPage": json.dumps({
                        'url': request.path,
                        'titl': '',
                        'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                            'parentTemplate': 'base/fullPage/emptyParent.html',
                            'fon0': static("libraryTHPS/img/17db.jpg"),
                            'fon1': static("libraryTHPS/img/noise.gif"),
                            'content': ('libraryTHPS', 'storage', typeOfItems),
                            'breadcrumbs': ('clans', 'storage'),
                            'menu': json.dumps(OrderedDict({
                                "clans": {
                                    "textname": "Кланы",
                                    "url": [
                                        reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'clans', 'offJS': 'noscripts'}),
                                        reverse("storageUrl", kwargs = {'typeOfItems': 'clans'})
                                        ],
                                    "handler": "localNavigation",
                                    "review": {
                                        "textname": "Обзор кланов",
                                        }
                                    },
                                "players": {
                                    "textname": "Игроки",
                                    "url": [
                                        reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'players', 'offJS': 'noscripts'}),
                                        reverse("storageUrl", kwargs = {'typeOfItems': 'players'})
                                        ],
                                    "handler": "localNavigation"
                                    }
                                })),
                            'sync': True
                            })
                        })})
                if request.GET.get('itemsParts', None):
                    item = json.loads(request.GET['itemsParts'])
                    elements = loadStorageClanElements(request, itemPage = item['itemPart'], dAreaSize = item['dispAreaPropers'])
                    contents = rts("libraryTHPS/body/storage/clanElements.html", {
                        'itemType': 'clan',
                        'contentItems': elements[0],
                        'params': elements[3]
                        })
                    return JsonResponse({
                        'itemProperties': json.dumps(elements[2]),
                        'responseList': json.dumps(contents),
                        'next': json.dumps(elements[1])
                        })
                return JsonResponse({"newPage": json.dumps({
                    "templateHead": rts("libraryTHPS/head/storage/storage.html"),
                    "templateBody": rts("libraryTHPS/body/storage/clansList.html"),
                    "templateScripts": rts("libraryTHPS/scripts/storage/list.html", {'content': typeOfItems}),
                    'menu': {
                        "clans": {
                            "textname": "Кланы",
                            "url": [
                                reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'clans', 'offJS': 'noscripts'}),
                                reverse("storageUrl", kwargs = {'typeOfItems': 'clans'})
                                ],
                            "handler": "localNavigation",
                            "review": {
                                "textname": "Обзор кланов",
                                }
                            },
                        "players": {
                            "textname": "Игроки",
                            "url": [
                                reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'players', 'offJS': 'noscripts'}),
                                reverse("storageUrl", kwargs = {'typeOfItems': 'players'})
                                ],
                            "handler": "localNavigation"
                            }
                        },
                    'breadcrumbs': ('clans', 'storage'),
                    "url": request.path,
                    "fon0": static("libraryTHPS/img/17db.jpg"),
                    "fon1": static("libraryTHPS/img/noise.gif")
                    })})
            return render(request, "libraryTHPS/libraryTHPS.html", {
                'parentTemplate': 'base/fullPage/base.html',
                'fon0': static("libraryTHPS/img/17db.jpg"),
                'fon1': static("libraryTHPS/img/noise.gif"),
                'content': ('libraryTHPS', 'storage', typeOfItems),
                'menu': json.dumps(OrderedDict({
                    "clans": {
                        "textname": "Кланы",
                        "url": [
                            reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'clans', 'offJS': 'noscripts'}),
                            reverse("storageUrl", kwargs = {'typeOfItems': 'clans'})
                            ],
                        "handler": "localNavigation",
                        "review": {
                            "textname": "Обзор кланов",
                            }
                        },
                    "players": {
                        "textname": "Игроки",
                        "url": [
                            reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'players', 'offJS': 'noscripts'}),
                            reverse("storageUrl", kwargs = {'typeOfItems': 'players'})
                            ],
                        "handler": "localNavigation"
                        }
                    })),
                'breadcrumbs': ('clans', 'storage'),
                'sync': True
                })

def libraryStorageNoAsync(request, typeOfItems, page, **kwargs):
    if request.method == "GET":
        if page is None:
            page = 1
        elements = loadStorageClanElements(request, itemPage = page)
        return render(request, "libraryTHPS/libraryTHPS.html", {
            'parentTemplate': 'base/fullPage/base.html',
            'fon0': static("libraryTHPS/img/17db.jpg"),
            'sync': False,
            'fon1': static("libraryTHPS/img/noise.gif"),
            'content': ('libraryTHPS', 'storage', typeOfItems, 'all'),
            'type': 'noASync',
            'itemType': typeOfItems[:-1],
            'elements': elements[0]
            })

def libraryStorageRedirect(request, **kwargs):
    if not "typeOfItems" in kwargs:
        kwargs.update({'typeOfItems': 'clans'})
    if not "page" in kwargs:
        kwargs.update({'page': 1})
    if kwargs.get('offJS', None):
        newUrl = reverse("storagePageNoJSUrl", kwargs = kwargs)
    else:
        newUrl = reverse("storagePageUrl", kwargs = kwargs)
    if request.is_ajax():
        if "global" in request.GET:
            return JsonResponse({"redirectTo": json.dumps('%s?global' % newUrl)})
        return JsonResponse({"redirectTo": json.dumps(newUrl)})
    return redirect(newUrl)

def libraryStorageShow(request, typeOfItem, item):
    if request.method == "GET":
        if request.user.is_authenticated == False:
            pageSettings = libraryClanPageDefaultService.objects.get(pk = 1)
            thumbSettings = libraryClanThumbDefaultService.objects.get(pk = 1)
        else:
            try:
                pageSettings = libraryClanPageService.objects.get(toUser = request.user.id)
                thumbSettings = libraryClanThumbService.objects.get(toUser = request.user.id)
            except libraryClanPageService.DoesNotExist:
                pageSettings = libraryClanPageDefaultService.objects.get(pk = 1)
                thumbSettings = libraryClanThumbDefaultService.objects.get(pk = 1)
        if typeOfItem == "clan":
            try:
                mainItem = clansMain.objects.get(slugName = item)
            except clansMain.DoesNotExist:
                mainItem = None
            try:
                thumbAttrs = clansThumbAttrs.objects.get(clanItem = item)
            except clansThumbAttrs.DoesNotExist:
                thumbAttrs = None
            try:
                attrsItem = clansItemAttrs.objects.get(clanItem = item)
            except clansItemAttrs.DoesNotExist:
                attrsItem = None
            itemDict = {
                "name": mainItem.name,
                "nameSlug": mainItem.slugName,
                "desc": mainItem.description,
                "tags": mainItem.tags,
                "addDate": mainItem.pubDate,
                "foundedBy": mainItem.createdBy,
                "foundedDate": mainItem.foundedDate,
                "clanLeader": mainItem.leader,
                "clanMembers": mainItem.members,
                #"clanOutgoingLeaders": mainItem # Создать поле "Бывшие лидеры"
                #"clanOutgoingMembers": mainItem # создать поле "Бывшие игроки"
                "styles": mainItem.clanInStyles,
                "clanUnions": mainItem.clanUnionShips,
            }
            if attrsItem:
                if attrsItem.showFormat >= 1:
                        itemDict["background"] = "%s%s" % (settings.MEDIA_URL, attrsItem.background)
                        if attrsItem.showFormat >= 2:
                            itemDict["logo"] = "%s%s" % (settings.MEDIA_URL, attrsItem.logo)
                            if attrsItem.showFormat == 3:
                                itemDict["logoPosition"] = attrsItem.logoPosition
        if request.is_ajax():
            if "global" in request.GET:
                return JsonResponse({"newPage": json.dumps({
                    'url': request.path,
                    'titl': 'Клан %s' % (itemDict["name"]),
                    'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                        'parentTemplate': 'base/fullPage/emptyParent.html',
                        'content': ('libraryTHPS', 'storage', typeOfItem, item),
                        'breadcrumbs': ('clans', 'storage'),
                        'menu': json.dumps(OrderedDict({
                            "clans": {
                                "textname": "Кланы",
                                "url": [
                                    reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'clans', 'offJS': 'noscripts'}),
                                    reverse("storageUrl", kwargs = {'typeOfItems': 'clans'})
                                    ],
                                "handler": "localNavigation",
                                },
                            "players": {
                                "textname": "Игроки",
                                "url": [
                                    reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'players', 'offJS': 'noscripts'}),
                                    reverse("storageUrl", kwargs = {'typeOfItems': 'players'})
                                    ]
                                }
                            })),
                        "itemInfo": itemDict,
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif"),
                        "itemTop": thumbSettings.thumbItemPaddingUl + pageSettings.pageItemMarginY
                        })
                    })})
            if "fullversion" in request.GET:
                return JsonResponse({"newPage": json.dumps({
                    "templateBody": rts("libraryTHPS/body/storage/clanItem.html", {
                        "itemInfo": itemDict
                    }),
                    "templateScripts": rts("libraryTHPS/scripts/storage/clanItem.html", {
                        'content': ('libraryTHPS', 'storage', typeOfItem, item),
                        "itemMargin": (pageSettings.pageItemMarginX, pageSettings.pageItemMarginY), "ulPadding": thumbSettings.thumbItemPaddingUl
                        }),
                    'menu': OrderedDict({
                        "clans": {
                            "textname": "Кланы",
                            "url": [
                                reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'clans', 'offJS': 'noscripts'}),
                                reverse("storageUrl", kwargs = {'typeOfItems': 'clans'})
                                ],
                            "handler": "localNavigation"
                            },
                        "players": {
                            "textname": "Игроки","url": [
                                reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'players', 'offJS': 'noscripts'}),
                                reverse("storageUrl", kwargs = {'typeOfItems': 'players'})
                                ]
                            }
                        }),
                    'breadcrumbs': ('clans', 'storage'),
                    "templateHead": rts("libraryTHPS/head/storage/storage.html"),
                    "titl": "Клан %s" % (itemDict["name"]),
                    "url": request.path,
                    "fon0": static("libraryTHPS/img/17db.jpg"),
                    "fon1": static("libraryTHPS/img/noise.gif")
                    })})
            return JsonResponse({"newPage": json.dumps({
                "templateBody": rts("libraryTHPS/body/storage/clanItem.html", {
                    "itemInfo": itemDict
                    }),
                "templateScripts": rts("libraryTHPS/scripts/storage/clanItem.html", {
                    'content': item,
                    "itemMargin": (pageSettings.pageItemMarginX, pageSettings.pageItemMarginY), "ulPadding": thumbSettings.thumbItemPaddingUl
                    }),
                'menu': OrderedDict({
                    "clans": {
                        "textname": "Кланы",
                        "url": [
                            reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'clans', 'offJS': 'noscripts'}),
                            reverse("storageUrl", kwargs = {'typeOfItems': 'clans'})
                            ],
                        "handler": "localNavigation",
                        },
                    "players": {
                        "textname": "Игроки","url": [
                            reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'players', 'offJS': 'noscripts'}),
                            reverse("storageUrl", kwargs = {'typeOfItems': 'players'})
                            ]
                        }
                    }),
                'breadcrumbs': ('clans', 'storage'),
                "templateHead": rts("libraryTHPS/head/storage/storage.html"),
                "titl": "Клан %s" % (itemDict["name"]),
                "url": request.path,
                "fon0": static("libraryTHPS/img/17db.jpg"),
                "fon1": static("libraryTHPS/img/noise.gif")
                })})
        return render(request, "libraryTHPS/libraryTHPS.html", {
            'parentTemplate': 'base/fullPage/base.html',
            'content': ('libraryTHPS', 'storage', typeOfItem, item),
            'menu': json.dumps(OrderedDict({
                "clans": {
                    "textname": "Кланы",
                    "url": [
                        reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'clans', 'offJS': 'noscripts'}),
                        reverse("storageUrl", kwargs = {'typeOfItems': 'clans'})
                        ],
                    "handler": "localNavigation",
                    },
                "players": {
                    "textname": "Игроки",
                    "url": [
                        reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'players', 'offJS': 'noscripts'}),
                        reverse("storageUrl", kwargs = {'typeOfItems': 'players'})
                        ]
                    }
                })),
            'breadcrumbs': ('clans', 'storage'),
            "itemInfo": itemDict,
            "fon0": static("libraryTHPS/img/17db.jpg"),
            "fon1": static("libraryTHPS/img/noise.gif"),
            'titl': "Клан %s" % (itemDict["name"]),
            "itemTop": thumbSettings.thumbItemPaddingUl + pageSettings.pageItemMarginY
            })