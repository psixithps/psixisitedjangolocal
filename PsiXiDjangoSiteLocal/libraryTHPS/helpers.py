import json, os
from collections import OrderedDict
from io import BytesIO
from django.conf import settings
from PIL import Image
from django.urls import reverse

def filterFutureSteps(**kwargs):
    m, t, p = kwargs.get('item', None), kwargs.get('thumb', None),\
    kwargs.get('page', None)
    result = set()
    if m is not None:
        b, l = m.background, m.logo
        if b == False:
            result.update({'groundcrop', 'made', 'resize'})
        if l == False:
            result.update({'logocrop', 'made', 'resize'})
    if t is not None:
        b, l = t.backgroundCrop, t.logoCrop
        if b == False or l == False:
            result.update({'made', 'resize'})
    else:
        result.update({'editThumb', 'editBoth'})
    if p is not None:
        b, l = p.backgroundCrop, p.logoCrop
        if b == False or l == False:
            result.update({'made', 'resize'})
    else:
        result.update({'editHead', 'editBoth'})
    return result

def buildEditClanMenu(blockedSteps, zoneEdit, step, *args):
    ''' args[slug, name] '''
    menu = OrderedDict({
        "clans": {
            "textname": "Кланы",
            "url": [
                reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'clans', 'offJS': 'noscripts'}),
                reverse("storageUrl", kwargs = {'typeOfItems': 'clans'})
                ],
            "handler": "localNavigation",
            "editInfo": {
                "textname": "Базовая информации клана %s" % args[1],
                "name": {
                    "textname": '%s' % args[1],
                    "url": reverse("libraryAddClanContentUrl", kwargs = {'item': args[0], 'step': 'name'}),
                    "handler": "localNavigation"
                    },
                "ground": {
                    "textname": "Фон",
                    "url": reverse("libraryAddClanContentUrl", kwargs = {'item': args[0], 'step': 'ground'}),
                    "handler": "localNavigation"
                    },
                "logo": {
                    "textname": "Логотип",
                    "url": reverse("libraryAddClanContentUrl", kwargs = {'item': args[0], 'step': 'logo'}),
                    "handler": "localNavigation"
                    },
                "detail": {
                    "textname": "Информация",
                    "url": reverse("libraryAddClanContentUrl", kwargs = {'item': args[0], 'step': 'detail'}),
                    "handler": "localNavigation"
                    }
                },
            "editDetail": {
                "textname": "Отображение",
                "editThumb": {
                    "textname": "Миниатюра",
                    "groundcrop": {
                        "textname": "Обрезка фона",
                        "url": reverse("libraryAddClanCropUrl", kwargs = {'item': args[0], 'section': 'miniature', 'urlSep': '/', 'step': 'ground'}),
                        "handler": "localNavigation"
                        },
                    "logocrop": {
                        "textname": "Обрезка логотипа",
                        "url": reverse("libraryAddClanCropUrl", kwargs = {'item': args[0], 'section': 'miniature', 'urlSep': '/', 'step': 'logo'}),
                        "handler": "localNavigation"
                        },
                    "made": {
                        "textname": "Создание миниатюры",
                        "url": reverse("libraryAddClanMadeUrl", kwargs = {'item': args[0], 'section': 'miniature', 'urlSep': '/'}),
                        "handler": "localNavigation"
                        }
                    },
                "editHead": {
                    "textname": "Страница",
                    "groundcrop": {
                        "textname": "Обрезка фона",
                        "url": reverse("libraryAddClanCropUrl", kwargs = {'item': args[0], 'section': 'headpiece', 'urlSep': '/', 'step': 'ground'}),
                        "handler": "localNavigation"
                        },
                    "logocrop": {
                        "textname": "Обрезка логотипа",
                        "url": reverse("libraryAddClanCropUrl", kwargs = {'item': args[0], 'section': 'headpiece', 'urlSep': '/', 'step': 'logo'}),
                        "handler": "localNavigation"
                        },
                    "made": {
                        "textname": "Создание представления",
                        "url": reverse("libraryAddClanMadeUrl", kwargs = {'item': args[0], 'section': 'headpiece', 'urlSep': '/'}),
                        "handler": "localNavigation"
                        }
                    },
                "editBoth": {
                    "textname": "Совместно",
                    "groundcrop": {
                        "textname": "Обрезка фона",
                        "url": reverse("libraryAddClanCropUrl", kwargs = {'item': args[0], 'section': '', 'urlSep': '', 'step': 'ground'}),
                        "handler": "localNavigation"
                        },
                    "logocrop": {
                        "textname": "Обрезка логотипа",
                        "url": reverse("libraryAddClanCropUrl", kwargs = {'item': args[0], 'section': '', 'urlSep': '', 'step': 'logo'}),
                        "handler": "localNavigation"
                        },
                    "made": {
                        "textname": "Создание миниатюры и представления",
                        "url": reverse("libraryAddClanMadeUrl", kwargs = {'item': args[0], 'section': '', 'urlSep': ''}),
                        "handler": "localNavigation"
                        }
                    }
                }
            },
        "players": {
            "textname": "Игроки",
            "url": [
                reverse("storageNoJSUrl", kwargs = {'typeOfItems': 'players', 'offJS': 'noscripts'}),
                reverse("storageUrl", kwargs = {'typeOfItems': 'players'})
                ]
            }
        })
    if zoneEdit == "both":
        menu["clans"]["editDetail"].update({"fixed": ""})
        menu["clans"]["editDetail"]["editBoth"].update({"fixed": ""})
        if "groundcrop" in blockedSteps:
            menu["clans"]["editDetail"]["editBoth"]["groundcrop"].update({"blocked": "groundcrop"})
        if "logocrop" in blockedSteps:
            menu["clans"]["editDetail"]["editBoth"]["logocrop"].update({"blocked": "logocrop"})
        if step == "groundcrop":
            menu["clans"]["editDetail"]["editBoth"]["groundcrop"].update({"fixed": "", "current": "groundcrop"})
        elif step == "logocrop":
            menu["clans"]["editDetail"]["editBoth"]["logocrop"].update({"fixed": "", "current": "logocrop"})
        elif step == "made":
            menu["clans"]["editDetail"]["editBoth"]["made"].update({"fixed": "", "current": "made"})
    elif zoneEdit == "miniature":
        menu["clans"]["editDetail"].update({"fixed": ""})
        menu["clans"]["editDetail"]["editThumb"].update({"fixed": ""})
        if step == "groundcrop":
            menu["clans"]["editDetail"]["editThumb"]["groundcrop"].update({"fixed": "", "current": "groundcrop"})
        elif step == "logocrop":
            menu["clans"]["editDetail"]["editThumb"]["logocrop"].update({"fixed": "", "current": "logocrop"})
        elif step == "made":
            menu["clans"]["editDetail"]["editThumb"]["made"].update({"fixed": "", "current": "made"})
    elif zoneEdit == "headpiece":
        menu["clans"]["editDetail"].update({"fixed": ""})
        menu["clans"]["editDetail"]["editHead"].update({"fixed": ""})
        if step == "groundcrop":
            menu["clans"]["editDetail"]["editHead"]["groundcrop"].update({"fixed": "", "current": "groundcrop"})
        elif step == "logocrop":
            menu["clans"]["editDetail"]["editHead"]["logocrop"].update({"fixed": "", "current": "logocrop"})
        elif step == "made":
            menu["clans"]["editDetail"]["editHead"]["made"].update({"fixed": "", "current": "made"})
    elif zoneEdit == "content":
        menu["clans"]["editInfo"].update({"fixed": ""})
        if step == "name":
            menu["clans"]["editInfo"]["name"].update({"fixed": "", "current": "name"})
        elif step == "ground":
            menu["clans"]["editInfo"]["ground"].update({"fixed": "", "current": "ground"})
        elif step == "logo":
            menu["clans"]["editInfo"]["logo"].update({"fixed": "", "current": "logo"})
        if step == "detail":
            menu["clans"]["editInfo"]["detail"].update({"fixed": "", "current": "detail"})
    return menu

def getOrNone(model, *args):
    '''
    Эта функция позволяет упростить получение QuerySet модели или None, если нету.
    model - модель, args[0] - первичный ключ, args[1] - значение.
    '''
    try:
        return model.objects.get(args[0], args[1])
    except model.DoesNotExist:
        return None

def cookiesEdit(rendered, currentCooks, cooksDict):
    ''' Функция установки/удаления кук. входящие автрибуты вызова:
    ("отрендеренный" реквест, текущие куки, новые куки dict). '''
    for k, v in cooksDict.items():
        if not k == "delete":
            if k not in currentCooks.keys() or v != currentCooks[k]:
                rendered.set_cookie(k, v)
        else:
            rendered.delete_cookie(v)
    return rendered

def croppingFunction(img, coordinates):
    imgUrl = img.url
    fullName = img.name
    name = fullName[fullName.rfind("/") + 1 : fullName.rfind(".")]
    ft = fullName[fullName.rfind(".") + 1 : len(fullName)]
    if ft == "jpg":
        ft = "jpeg"
    with Image.open(os.path.normpath("%s%s" % (settings.BASE_DIR, imgUrl)), 'r') as rootImage: # Убрать на боевом
        cropped = rootImage.crop(coordinates)
    bufferIO = BytesIO()
    cropped.save(bufferIO, format = ft)
    croppedBytes = bufferIO.getvalue()
    return ("%s.%s" % (name, ft), croppedBytes)

def changePosition(position, initialWidth, initialHeight, currentWidth, currentHeight):
    if currentWidth > initialWidth:
        differentX = currentWidth / initialWidth
        proportionsDifferentX = (currentWidth - initialWidth) * differentX
        x = 0 + position[0] + proportionsDifferentX
    elif currentWidth < initialWidth:
        differentX = initialWidth / currentWidth
        proportionsDifferentX = (initialWidth - currentWidth) / differentX
        x = 0 + position[0] - proportionsDifferentX
    else:
        x = position[0]
    if currentHeight > initialHeight:
        differentY = currentHeight / initialHeight
        proportionsDifferentY = (currentHeight - initialHeight) * differentY
        y = 0 + position[1] + proportionsDifferentY
    elif currentHeight < initialHeight:
        differentY = initialHeight / currentHeight
        proportionsDifferentY = (initialHeight - currentHeight) / differentY
        y = 0 + position[1] - proportionsDifferentY
    else:
        y = position[1]
    return "%d,%d" % (int(x), int(y))