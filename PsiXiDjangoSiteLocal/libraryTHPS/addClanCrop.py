import json, os, shutil
from django.conf import settings
from django.core.files.base import ContentFile
from django.db import transaction
from django.contrib.staticfiles.templatetags.staticfiles import static
from libraryTHPS.models import clansMain, clansItemAttrs, clansThumbAttrs, libraryClanThumbDefaultService, libraryClanThumbService, libraryClanPageDefaultService, libraryClanPageService
from libraryTHPS.forms import addBackground, addLogo, crop
from libraryTHPS.helpers import filterFutureSteps, changePosition, croppingFunction, buildEditClanMenu
from base.helpers import checkCSRF
from django.shortcuts import get_object_or_404
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.template.loader import render_to_string as rts
from django.urls import reverse
from django.contrib import messages
from random import randint

def addLibraryClanItemCrop(request, item, section, urlSep, step):
    mainItem = get_object_or_404(clansMain, pk = item)
    if request.method == "GET":
        if section == "miniature":
            try:
                thumb = clansThumbAttrs.objects.get(pk = mainItem.pk)
            except clansThumbAttrs.DoesNotExist:
                thumb = None
            if step == "ground":
                if mainItem.background == False:
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'url': request.path,
                                'titl': '',
                                'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                    'parentTemplate': 'base/fullPage/emptyParent.html',
                                    'content': ('libraryTHPS', 'add', 'clan', 'ground'),
                                    'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'groundcrop'),
                                    'csrf': '',
                                    'form': addBackground(),
                                    'messages': ("Вы не можете произвести обрезку изображения фона для миниатюры до тех пор, пока оно не было загружено или не было выбрано. \
                                    Чтобы продолжить, выберите изображение для формы ниже."),
                                    "fon0": static("libraryTHPS/img/17db.jpg"),
                                    "fon1": static("libraryTHPS/img/noise.gif")
                                    })
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/content.html"),
                            "templateBody": rts("libraryTHPS/body/add/image.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'ground'),
                                'csrf': '',
                                'form': addBackground(),
                                'messages': ("Вы не можете произвести обрезку изображения фона для миниатюры до тех пор, пока оно не было загружено или не было выбрано. \
                                Чтобы продолжить, выберите изображение для формы ниже.")
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'ground', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'groundcrop'),
                            "titl": "",
                            "url": reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'ground'}),
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })})
                    messages.add_message(request, messages.WARNING,
                                         "Вы не можете произвести обрезку изображения фона для миниатюры до тех пор, пока оно не было загружено или не было выбрано. \
                                         Чтобы продолжить, выберите изображение для формы ниже."
                                         )
                    return redirect("libraryAddClanContentUrl", item, 'ground')
                if thumb is None:
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'url': request.path,
                                'titl': '',
                                'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                    'parentTemplate': 'base/fullPage/emptyParent.html',
                                    'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', 'miniature'),
                                    'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'groundcrop'),
                                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'miniature', 'groundcrop', item, mainItem.name)),
                                    'csrf': '',
                                    'form': crop(),
                                    'cropImage': mainItem.background,
                                    'fon0': static("libraryTHPS/img/17db.jpg"),
                                    'fon1': static("libraryTHPS/img/noise.gif")
                                    })
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/crop.html"),
                            "templateBody": rts("libraryTHPS/body/add/crop.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', 'miniature'),
                                'csrf': '',
                                'form': crop(),
                                'cropImage': mainItem.background
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/crop.html"),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'miniature', 'groundcrop', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'groundcrop'),
                            'titl': '',
                            "url": request.path,
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })})
                    return render(request, "libraryTHPS/libraryTHPS.html", {
                        'parentTemplate': 'base/fullPage/base.html',
                        'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', 'miniature'),
                        'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'miniature', 'groundcrop', item, mainItem.name)),
                        'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'groundcrop'),
                        'csrf': checkCSRF(request),
                        'form': crop(),
                        'cropImage': mainItem.background,
                        'titl': '',
                        'fon0': static("libraryTHPS/img/17db.jpg"),
                        'fon1': static("libraryTHPS/img/noise.gif")
                        })
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', 'miniature'),
                                'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'groundcrop'),
                                'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem), 'miniature', 'groundcrop', item, mainItem.name)),
                                'csrf': '',
                                'form': crop(),
                                'cropImage': mainItem.background,
                                'initialCropCoordinates': thumb.backgroundCropCoordinates,
                                'fon0': static("libraryTHPS/img/17db.jpg"),
                                'fon1': static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/crop.html"),
                        "templateBody": rts("libraryTHPS/body/add/crop.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', 'miniature'),
                            'csrf': '',
                            'form': crop(),
                            'cropImage': mainItem.background
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/crop.html", {
                            'initialCropCoordinates': thumb.backgroundCropCoordinates,
                        }),
                        'menu': buildEditClanMenu(filterFutureSteps(item = mainItem), 'miniature', 'groundcrop', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'groundcrop'),
                        'titl': '',
                        "url": request.path,
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                return render(request, "libraryTHPS/libraryTHPS.html", {
                    'parentTemplate': 'base/fullPage/base.html',
                    'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', 'miniature'),
                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem), 'miniature', 'groundcrop', item, mainItem.name)),
                    'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'groundcrop'),
                    'csrf': checkCSRF(request),
                    'form': crop(),
                    'cropImage': mainItem.background,
                    'initialCropCoordinates': thumb.backgroundCropCoordinates,
                    'titl': '',
                    'fon0': static("libraryTHPS/img/17db.jpg"),
                    'fon1': static("libraryTHPS/img/noise.gif")
                    })
            else:
                if mainItem.logo == False:
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'url': request.path,
                                'titl': '',
                                'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                    'parentTemplate': 'base/fullPage/emptyParent.html',
                                    'content': ('libraryTHPS', 'add', 'clan', 'logo'),
                                    'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'logocrop'),
                                    'csrf': '',
                                    'form': addLogo(background = mainItem.background, isAddedBackground = mainItem.background),
                                    'messages': ("Вы не можете произвести обрезку логотипа до тех пор, пока оно не было загружено или не было выбрано. \
                                    Чтобы продолжить, выберите изображение для формы ниже."),
                                    "fon0": static("libraryTHPS/img/17db.jpg"),
                                    "fon1": static("libraryTHPS/img/noise.gif")
                                    })
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/content.html"),
                            "templateBody": rts("libraryTHPS/body/add/image.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'logo'),
                                'csrf': '',
                                'form': addLogo(background = mainItem.background, isAddedBackground = mainItem.background),
                                'messages': ("Вы не можете произвести обрезку логотипа до тех пор, пока оно не было загружено или не было выбрано. \
                                Чтобы продолжить, выберите изображение для формы ниже.")
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'logo', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'logocrop'),
                            "titl": "",
                            "url": reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'logo'}),
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })})
                    messages.add_message(request, messages.WARNING,
                                         "Вы не можете произвести обрезку изображения логотипа до тех пор, пока оно не было загружено или не было выбрано. \
                                         Чтобы продолжить, выберите изображение для формы ниже."
                                         )
                    return redirect("libraryAddClanContentUrl", item, 'logo')
                if thumb is None:
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'url': request.path,
                                'titl': '',
                                'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                    'parentTemplate': 'base/fullPage/emptyParent.html',
                                    'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', 'miniature'),
                                    'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'logocrop'),
                                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'miniature', 'logocrop', item, mainItem.name)),
                                    'csrf': '',
                                    'form': crop(),
                                    'cropImage': mainItem.logo,
                                    'titl': '',
                                    'fon0': static("libraryTHPS/img/17db.jpg"),
                                    'fon1': static("libraryTHPS/img/noise.gif")
                                    })
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/crop.html"),
                            "templateBody": rts("libraryTHPS/body/add/crop.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', 'miniature'),
                                'csrf': '',
                                'form': crop(),
                                'cropImage': mainItem.logo
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/crop.html"),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'miniature', 'logocrop', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'logocrop'),
                            'titl': '',
                            "url": request.path,
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })})
                    return render(request, "libraryTHPS/libraryTHPS.html", {
                        'parentTemplate': 'base/fullPage/base.html',
                        'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', 'miniature'),
                        'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'miniature', 'logocrop', item, mainItem.name)),
                        'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'logocrop'),
                        'csrf': checkCSRF(request),
                        'form': crop(),
                        'cropImage': mainItem.logo,
                        'titl': '',
                        'fon0': static("libraryTHPS/img/17db.jpg"),
                        'fon1': static("libraryTHPS/img/noise.gif")
                        })
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', 'miniature'),
                                'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'logocrop'),
                                'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem), 'miniature', 'logocrop', item, mainItem.name)),
                                'csrf': '',
                                'form': crop(),
                                'cropImage': mainItem.logo,
                                'initialCropCoordinates': thumb.logoCropCoordinates,
                                'fon0': static("libraryTHPS/img/17db.jpg"),
                                'fon1': static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/crop.html"),
                        "templateBody": rts("libraryTHPS/body/add/crop.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', 'miniature'),
                            'csrf': '',
                            'form': crop(),
                            'cropImage': mainItem.logo
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/crop.html", {
                            'initialCropCoordinates': thumb.logoCropCoordinates,
                        }),
                        'menu': buildEditClanMenu(filterFutureSteps(item = mainItem), 'miniature', 'logocrop', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'logocrop'),
                        'titl': '',
                        "url": request.path,
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })
                                         })
                return render(request, "libraryTHPS/libraryTHPS.html", {
                    'parentTemplate': 'base/fullPage/base.html',
                    'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', 'miniature'),
                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem), 'miniature', 'logocrop', item, mainItem.name)),
                    'breadcrumbs': ('clans', 'editDetail', 'editThumb', 'logocrop'),
                    'csrf': checkCSRF(request),
                    'form': crop(),
                    'cropImage': mainItem.logo,
                    'initialCropCoordinates': thumb.logoCropCoordinates,
                    'titl': '',
                    'fon0': static("libraryTHPS/img/17db.jpg"),
                    'fon1': static("libraryTHPS/img/noise.gif")
                    })
        elif section == "headpiece":
            try:
                head = clansItemAttrs.objects.get(pk = mainItem.pk)
            except clansItemAttrs.DoesNotExist:
                head = None
            if step == "ground":
                if mainItem.background == False:
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'url': request.path,
                                'titl': '',
                                'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                    'parentTemplate': 'base/fullPage/emptyParent.html',
                                    'content': ('libraryTHPS', 'add', 'clan', 'ground'),
                                    'breadcrumbs': ('clans', 'editDetail', 'editHead', 'groundcrop'),
                                    'csrf': '',
                                    'form': addBackground(),
                                    'messages': ("Вы не можете произвести обрезку изображения фона для страницы клана до тех пор, пока оно не было загружено или не было выбрано. \
                                    Чтобы продолжить, выберите изображение для формы ниже."),
                                    "fon0": static("libraryTHPS/img/17db.jpg"),
                                    "fon1": static("libraryTHPS/img/noise.gif")
                                    })
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/content.html"),
                            "templateBody": rts("libraryTHPS/body/add/image.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'ground'),
                                'csrf': '',
                                'form': addBackground(),
                                'messages': ("Вы не можете произвести обрезку изображения фона для страницы клана до тех пор, пока оно не было загружено или не было выбрано. \
                                Чтобы продолжить, выберите изображение для формы ниже."),
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'content', 'ground', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editDetail', 'editHead', 'groundcrop'),
                            "titl": "",
                            "url": reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'ground'}),
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })})
                    messages.add_message(request, messages.WARNING,
                                         "Вы не можете произвести обрезку изображения фона для страницы клана до тех пор, пока оно не было загружено или не было выбрано. \
                                         Чтобы продолжить, выберите изображение для формы ниже."
                                         )
                    return redirect("libraryAddClanContentUrl", item, 'ground')
                if head is None:
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'url': request.path,
                                'titl': '',
                                'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                    'parentTemplate': 'base/fullPage/emptyParent.html',
                                    'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', 'headpiece'),
                                    'breadcrumbs': ('clans', 'editDetail', 'editHead', 'groundcrop'),
                                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'groundcrop', item, mainItem.name)),
                                    'csrf': '',
                                    'form': crop(),
                                    'cropImage': mainItem.background,
                                    'titl': '',
                                    'fon0': static("libraryTHPS/img/17db.jpg"),
                                    'fon1': static("libraryTHPS/img/noise.gif")
                                    })
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/crop.html"),
                            "templateBody": rts("libraryTHPS/body/add/crop.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', 'headpiece'),
                                'csrf': '',
                                'form': crop(),
                                'cropImage': mainItem.background
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/crop.html"),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'groundcrop', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editDetail', 'editHead', 'groundcrop'),
                            'titl': '',
                            "url": request.path,
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })})
                    return render(request, "libraryTHPS/libraryTHPS.html", {
                        'parentTemplate': 'base/fullPage/base.html',
                        'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', 'headpiece'),
                        'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'groundcrop', item, mainItem.name)),
                        'breadcrumbs': ('clans', 'editDetail', 'editHead', 'groundcrop'),
                        'csrf': checkCSRF(request),
                        'form': crop(),
                        'cropImage': mainItem.background,
                        'titl': '',
                        'fon0': static("libraryTHPS/img/17db.jpg"),
                        'fon1': static("libraryTHPS/img/noise.gif")
                        })
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', 'headpiece'),
                                'breadcrumbs': ('clans', 'editDetail', 'editHead', 'groundcrop'),
                                'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'groundcrop', item, mainItem.name)),
                                'csrf': '',
                                'form': crop(),
                                'cropImage': mainItem.background,
                                'initialCropCoordinates': head.backgroundCropCoordinates,
                                'titl': '',
                                'fon0': static("libraryTHPS/img/17db.jpg"),
                                'fon1': static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/crop.html"),
                        "templateBody": rts("libraryTHPS/body/add/crop.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', 'headpiece'),
                            'csrf': '',
                            'form': crop(),
                            'cropImage': mainItem.background
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/crop.html", {
                            'initialCropCoordinates': head.backgroundCropCoordinates,
                        }),
                        'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), cookie, 'headpiece', 'groundcrop', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editDetail', 'editHead', 'groundcrop'),
                        'titl': '',
                        "url": request.path,
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                return render(request, "libraryTHPS/libraryTHPS.html", {
                    'parentTemplate': 'base/fullPage/base.html',
                    'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', 'headpiece'),
                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'groundcrop', item, mainItem.name)),
                    'csrf': checkCSRF(request),
                    'form': crop(),
                    'cropImage': mainItem.background,
                    'initialCropCoordinates': head.backgroundCropCoordinates,
                    'breadcrumbs': ('clans', 'editDetail', 'editHead', 'groundcrop'),
                    'titl': '',
                    'fon0': static("libraryTHPS/img/17db.jpg"),
                    'fon1': static("libraryTHPS/img/noise.gif")
                    })
            else:
                if mainItem.logo == False:
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'url': request.path,
                                'titl': '',
                                'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                    'parentTemplate': 'base/fullPage/emptyParent.html',
                                    'content': ('libraryTHPS', 'add', 'clan', 'logo'),
                                    'breadcrumbs': ('clans', 'editDetail', 'editHead', 'logocrop'),
                                    'csrf': '',
                                    'form': addLogo(background = mainItem.background, isAddedBackground = mainItem.background),
                                    'messages': ("Вы не можете произвести обрезку логотипа до тех пор, пока оно не было загружено или не было выбрано. \
                                    Чтобы продолжить, выберите изображение для формы ниже."),
                                    "fon0": static("libraryTHPS/img/17db.jpg"),
                                    "fon1": static("libraryTHPS/img/noise.gif")
                                    })
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/content.html"),
                            "templateBody": rts("libraryTHPS/body/add/image.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'logo'),
                                'csrf': '',
                                'form': addLogo(background = mainItem.background, isAddedBackground = mainItem.background),
                                'messages': ("Вы не можете произвести обрезку логотипа до тех пор, пока оно не было загружено или не было выбрано. \
                                Чтобы продолжить, выберите изображение для формы ниже.")
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'content', 'logo', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editDetail', 'editHead', 'logocrop'),
                            "titl": "",
                            "url": reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'logo'}),
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })})
                    messages.add_message(request, messages.WARNING,
                                         "Вы не можете произвести обрезку изображения логотипа до тех пор, пока оно не было загружено или не было выбрано. \
                                         Чтобы продолжить, выберите изображение для формы ниже."
                                         )
                    return redirect("libraryAddClanContentUrl", item, 'logo')
                if head is None:
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'url': request.path,
                                'titl': '',
                                'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                    'parentTemplate': 'base/fullPage/emptyParent.html',
                                    'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', 'headpiece'),
                                    'breadcrumbs': ('clans', 'editDetail', 'editHead', 'logocrop'),
                                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'logocrop', item, mainItem.name)),
                                    'csrf': '',
                                    'form': crop(),
                                    'cropImage': mainItem.logo, 'titl': '',
                                    'fon0': static("libraryTHPS/img/17db.jpg"),
                                    'fon1': static("libraryTHPS/img/noise.gif")
                                    })
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/crop.html"),
                            "templateBody": rts("libraryTHPS/body/add/crop.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', 'headpiece'),
                                'csrf': '',
                                'form': crop(),
                                'cropImage': mainItem.logo
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/crop.html"),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'logocrop', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editDetail', 'editHead', 'logocrop'),
                            'titl': '',
                            "url": request.path,
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif") 
                                                                })
                                            })
                    return render(request, "libraryTHPS/libraryTHPS.html", {
                        'parentTemplate': 'base/fullPage/base.html',
                        'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', 'headpiece'),
                        'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'logocrop', item, mainItem.name)),
                        'breadcrumbs': ('clans', 'editDetail', 'editHead', 'logocrop'),
                        'csrf': checkCSRF(request),
                        'form': crop(),
                        'cropImage': mainItem.logo, 'titl': '',
                        'fon0': static("libraryTHPS/img/17db.jpg"),
                        'fon1': static("libraryTHPS/img/noise.gif")
                        })
                if request.is_ajax():
                    if "global" in request.GET:
                        return JsonResponse({"newPage": json.dumps({
                            'url': request.path,
                            'titl': '',
                            'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                'parentTemplate': 'base/fullPage/emptyParent.html',
                                'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', 'headpiece'),
                                'breadcrumbs': ('clans', 'editDetail', 'editHead', 'logocrop'),
                                'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'logocrop', item, mainItem.name)),
                                'csrf': '',
                                'form': crop(),
                                'cropImage': mainItem.logo,
                                'initialCropCoordinates': head.logoCropCoordinates,
                                'fon0': static("libraryTHPS/img/17db.jpg"),
                                'fon1': static("libraryTHPS/img/noise.gif")
                                })
                            })})
                    return JsonResponse({"newPage": json.dumps({
                        "templateHead": rts("libraryTHPS/head/add/crop.html"),
                        "templateBody": rts("libraryTHPS/body/add/crop.html", {
                            'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', 'headpiece'),
                            'csrf': '',
                            'form': crop(),
                            'cropImage': mainItem.logo
                            }),
                        "templateScripts": rts("libraryTHPS/scripts/add/crop.html", {
                            'initialCropCoordinates': head.logoCropCoordinates,
                        }),
                        'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'logocrop', item, mainItem.name),
                        'breadcrumbs': ('clans', 'editDetail', 'editHead', 'logocrop'),
                        'titl': '',
                        "url": request.path,
                        "fon0": static("libraryTHPS/img/17db.jpg"),
                        "fon1": static("libraryTHPS/img/noise.gif")
                        })})
                return render(request, "libraryTHPS/libraryTHPS.html", {
                    'parentTemplate': 'base/fullPage/base.html',
                    'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', 'headpiece'),
                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, page = head), 'headpiece', 'logocrop', item, mainItem.name)),
                    'breadcrumbs': ('clans', 'editDetail', 'editHead', 'logocrop'),
                    'csrf': checkCSRF(request),
                    'form': crop(),
                    'cropImage': mainItem.logo,
                    'initialCropCoordinates': head.logoCropCoordinates,
                    'titl': '', 'fon0': static("libraryTHPS/img/17db.jpg"),
                    'fon1': static("libraryTHPS/img/noise.gif")
                    })
        else:
            try:
                thumb = clansThumbAttrs.objects.get(pk = mainItem.pk)
            except clansThumbAttrs.DoesNotExist:
                thumb = None
            try:
                head = clansItemAttrs.objects.get(pk = mainItem.pk)
            except clansItemAttrs.DoesNotExist:
                head = None
            if step == "ground":
                if mainItem.background == False:
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'url': request.path,
                                'titl': '',
                                'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                    'parentTemplate': 'base/fullPage/emptyParent.html',
                                    'content': ('libraryTHPS', 'add', 'clan', 'ground'),
                                    'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'groundcrop'),
                                    'csrf': '',
                                    'form': addBackground(),
                                    'messages': ("Вы не можете произвести обрезку изображения фона до тех пор, пока оно не было загружено или не было выбрано. \
                                    Чтобы продолжить, выберите изображение для формы ниже."),
                                    "fon0": static("libraryTHPS/img/17db.jpg"),
                                    "fon1": static("libraryTHPS/img/noise.gif")
                                    })
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/content.html"),
                            "templateBody": rts("libraryTHPS/body/add/image.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'ground'),
                                'csrf': '',
                                'form': addBackground(),
                                'messages': ("Вы не можете произвести обрезку изображения фона до тех пор, пока оно не было загружено или не было выбрано. \
                                Чтобы продолжить, выберите изображение для формы ниже.")
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'ground', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'groundcrop'),
                            "titl": "",
                            "url": reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'ground'}),
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })
                                             })
                    messages.add_message(request, messages.WARNING,
                                         "Вы не можете произвести обрезку изображения фона до тех пор, пока оно не было загружено или не было выбрано. \
                                         Чтобы продолжить, выберите изображение для формы ниже."
                                         )
                    return redirect("libraryAddClanContentUrl", item, 'ground')
                if thumb is None and head is None:
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'url': request.path,
                                'titl': '',
                                'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                    'parentTemplate': 'base/fullPage/emptyParent.html',
                                    'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', ''),
                                    'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'groundcrop'),
                                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'groundcrop', item, mainItem.name)),
                                    'csrf': '',
                                    'form': crop(),
                                    'cropImage': mainItem.background,
                                    'fon0': static("libraryTHPS/img/17db.jpg"),
                                    'fon1': static("libraryTHPS/img/noise.gif")
                                    })
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/crop.html"),
                            "templateBody": rts("libraryTHPS/body/add/crop.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', ''),
                                'csrf': '',
                                'form': crop(),
                                'cropImage': mainItem.background
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/crop.html"),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'groundcrop', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'groundcrop'),
                            'titl': '',
                            "url": request.path,
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })})
                    return render(request, "libraryTHPS/libraryTHPS.html", {
                        'parentTemplate': 'base/fullPage/base.html',
                        'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', ''),
                        'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'groundcrop', item, mainItem.name)),
                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'groundcrop'),
                        'csrf': checkCSRF(request),
                        'form': crop(),
                        'cropImage': mainItem.background,
                        'titl': '',
                        'fon0': static("libraryTHPS/img/17db.jpg"),
                        'fon1': static("libraryTHPS/img/noise.gif")
                        })
                if thumb is not None and head is not None:
                    if thumb.backgroundCropCoordinates == head.backgroundCropCoordinates:
                        if request.is_ajax():
                            if "global" in request.GET:
                                return JsonResponse({"newPage": json.dumps({
                                    'url': request.path,
                                    'titl': '',
                                    'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                        'parentTemplate': 'base/fullPage/emptyParent.html',
                                        'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', ''),
                                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'groundcrop'),
                                        'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'groundcrop', item, mainItem.name)),
                                        'csrf': '',
                                        'form': crop(),
                                        'cropImage': mainItem.background,
                                        'initialCropCoordinates': thumb.backgroundCropCoordinates,
                                        'fon0': static("libraryTHPS/img/17db.jpg"),
                                        'fon1': static("libraryTHPS/img/noise.gif")
                                        })
                                    })})
                            return JsonResponse({"newPage": json.dumps({
                                "templateHead": rts("libraryTHPS/head/add/crop.html"),
                                "templateBody": rts("libraryTHPS/body/add/crop.html", {
                                    'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', ''),
                                    'csrf': '',
                                    'form': crop(),
                                    'cropImage': mainItem.background
                                    }),
                                "templateScripts": rts("libraryTHPS/scripts/add/crop.html", {
                                    'initialCropCoordinates': thumb.backgroundCropCoordinates
                                }),
                                'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'groundcrop', item, mainItem.name),
                                'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'groundcrop'),
                                'titl': '',
                                "url": request.path,
                                "fon0": static("libraryTHPS/img/17db.jpg"),
                                "fon1": static("libraryTHPS/img/noise.gif")
                                })})
                        return render(request, "libraryTHPS/libraryTHPS.html", {
                            'parentTemplate': 'base/fullPage/base.html',
                            'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', ''),
                            'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'groundcrop', item, mainItem.name)),
                            'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'groundcrop'),
                            'csrf': checkCSRF(request),
                            'form': crop(),
                            'cropImage': mainItem.background,
                            'initialCropCoordinates': thumb.backgroundCropCoordinates, 'titl': '',
                            'fon0': static("libraryTHPS/img/17db.jpg"),
                            'fon1': static("libraryTHPS/img/noise.gif")
                            })
                    vary = (thumb.backgroundCropCoordinates, head.backgroundCropCoordinates)
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'url': request.path,
                                'titl': '',
                                'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                    'parentTemplate': 'base/fullPage/emptyParent.html',
                                    'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', ''),
                                    'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'groundcrop'),
                                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'groundcrop', item, mainItem.name)),
                                    'csrf': '',
                                    'form': crop(),
                                    'cropImage': mainItem.background,
                                    'initialCropCoordinates': vary[randint(0, 1)],
                                    'fon0': static("libraryTHPS/img/17db.jpg"),
                                    'fon1': static("libraryTHPS/img/noise.gif")
                                    })
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/crop.html"),
                            "templateBody": rts("libraryTHPS/body/add/crop.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', ''),
                                'csrf': '',
                                'form': crop(),
                                'cropImage': mainItem.background
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/crop.html", {
                                'initialCropCoordinates': vary[randint(0, 1)]
                                }),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'groundcrop', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'groundcrop'),
                            'titl': '',
                            "url": request.path,
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })})
                    return render(request, "libraryTHPS/libraryTHPS.html", {
                        'parentTemplate': 'base/fullPage/base.html',
                        'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', ''),
                        'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'groundcrop', item, mainItem.name)),
                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'groundcrop'),
                        'csrf': checkCSRF(request),
                        'form': crop(),
                        'cropImage': mainItem.background,
                        'initialCropCoordinates': vary[randint(0, 1)],
                        'titl': '',
                        'fon0': static("libraryTHPS/img/17db.jpg"),
                        'fon1': static("libraryTHPS/img/noise.gif")
                        })
                else:
                    if thumb is None:
                        if request.is_ajax():
                            if "global" in request.GET:
                                return JsonResponse({"newPage": json.dumps({
                                    'url': request.path,
                                    'titl': '',
                                    'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                        'parentTemplate': 'base/fullPage/emptyParent.html',
                                        'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', ''),
                                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'groundcrop'),
                                        'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'groundcrop', item, mainItem.name)),
                                        'csrf': '',
                                        'form': crop(),
                                        'cropImage': mainItem.background,
                                        'initialCropCoordinates': head.backgroundCropCoordinates,
                                        'titl': '',
                                        'fon0': static("libraryTHPS/img/17db.jpg"),
                                        'fon1': static("libraryTHPS/img/noise.gif")
                                        })
                                    })})
                            return JsonResponse({"newPage": json.dumps({
                                "templateHead": rts("libraryTHPS/head/add/crop.html"),
                                "templateBody": rts("libraryTHPS/body/add/crop.html", {
                                    'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', ''),
                                    'csrf': '',
                                    'form': crop(),
                                    'cropImage': mainItem.background
                                    }),
                                "templateScripts": rts("libraryTHPS/scripts/add/crop.html", {
                                    'initialCropCoordinates': head.backgroundCropCoordinates,
                                }),
                                'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'groundcrop', item, mainItem.name),
                                'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'groundcrop'),
                                'titl': '',
                                "url": request.path,
                                "fon0": static("libraryTHPS/img/17db.jpg"),
                                "fon1": static("libraryTHPS/img/noise.gif")
                                })})
                        return render(request, "libraryTHPS/libraryTHPS.html", {
                            'parentTemplate': 'base/fullPage/base.html',
                            'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', ''),
                            'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'groundcrop', item, mainItem.name)),
                            'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'groundcrop'),
                            'csrf': checkCSRF(request),
                            'form': crop(),
                            'cropImage': mainItem.background,
                            'initialCropCoordinates': head.backgroundCropCoordinates,
                            'titl': '',
                            'fon0': static("libraryTHPS/img/17db.jpg"),
                            'fon1': static("libraryTHPS/img/noise.gif")
                            })
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'url': request.path,
                                'titl': '',
                                'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                    'parentTemplate': 'base/fullPage/emptyParent.html',
                                    'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', ''),
                                    'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'groundcrop'),
                                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'groundcrop', item, mainItem.name)),
                                    'csrf': '',
                                    'form': crop(),
                                    'cropImage': mainItem.background,
                                    'initialCropCoordinates': thumb.backgroundCropCoordinates,
                                    'fon0': static("libraryTHPS/img/17db.jpg"),
                                    'fon1': static("libraryTHPS/img/noise.gif")
                                    })
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/crop.html"),
                            "templateBody": rts("libraryTHPS/body/add/crop.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', ''),
                                'csrf': '',
                                'form': crop(),
                                'cropImage': mainItem.background
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/crop.html", {
                                'initialCropCoordinates': thumb.backgroundCropCoordinates,
                            }),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'groundcrop', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'groundcrop'),
                            'titl': '',
                            "url": request.path,
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })})
                    return render(request, "libraryTHPS/libraryTHPS.html", {
                        'parentTemplate': 'base/fullPage/base.html',
                        'content': ('libraryTHPS', 'add', 'clan', 'crop', 'ground', ''),
                        'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'groundcrop', item, mainItem.name)),
                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'groundcrop'),
                        'csrf': checkCSRF(request),
                        'form': crop(),
                        'cropImage': mainItem.background,
                        'initialCropCoordinates': thumb.backgroundCropCoordinates,
                        'titl': '',
                        'fon0': static("libraryTHPS/img/17db.jpg"),
                        'fon1': static("libraryTHPS/img/noise.gif")
                        })
            else:
                if mainItem.logo == False:
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'url': request.path,
                                'titl': '',
                                'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                    'parentTemplate': 'base/fullPage/emptyParent.html',
                                    'content': ('libraryTHPS', 'add', 'clan', 'logo'),
                                    'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'logocrop'),
                                    'csrf': '',
                                    'form': addLogo(background = mainItem.background, isAddedBackground = mainItem.background),
                                    'messages': ("Вы не можете произвести обрезку логотипа до тех пор, пока оно не было загружено или не было выбрано. \
                                    Чтобы продолжить, выберите изображение для формы ниже."),
                                    "fon0": static("libraryTHPS/img/17db.jpg"),
                                    "fon1": static("libraryTHPS/img/noise.gif")
                                    })
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/content.html"),
                            "templateBody": rts("libraryTHPS/body/add/image.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'logo'),
                                'csrf': '',
                                'form': addLogo(background = mainItem.background, isAddedBackground = mainItem.background),
                                'messages': ("Вы не можете произвести обрезку логотипа до тех пор, пока оно не было загружено или не было выбрано. \
                                Чтобы продолжить, выберите изображение для формы ниже.")
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'logocrop', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'logocrop'),
                            "titl": "",
                            "url": reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'logo'}),
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })})
                    messages.add_message(request, messages.WARNING,
                                         "Вы не можете произвести обрезку изображения логотипа до тех пор, пока оно не было загружено или не было выбрано. \
                                         Чтобы продолжить, выберите изображение для формы ниже."
                                         )
                    return redirect("libraryAddClanContentUrl", item, 'logo')
                if thumb is None and head is None:
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'url': request.path,
                                'titl': '',
                                'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                    'parentTemplate': 'base/fullPage/emptyParent.html',
                                    'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', ''),
                                    'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'logocrop'),
                                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'logocrop', item, mainItem.name)),
                                    'csrf': '',
                                    'form': crop(),
                                    'cropImage': mainItem.logo,
                                    'fon0': static("libraryTHPS/img/17db.jpg"),
                                    'fon1': static("libraryTHPS/img/noise.gif")
                                    })
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/crop.html"),
                            "templateBody": rts("libraryTHPS/body/add/crop.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', ''),
                                'csrf': '',
                                'form': crop(), 'cropImage': mainItem.logo
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/crop.html"),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'logocrop', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'logocrop'),
                            'titl': '',
                            "url": request.path,
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })})
                    return render(request, "libraryTHPS/libraryTHPS.html", {
                        'parentTemplate': 'base/fullPage/base.html',
                        'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', ''),
                        'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'logocrop', item, mainItem.name)),
                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'logocrop'),
                        'csrf': checkCSRF(request),
                        'form': crop(),
                        'cropImage': mainItem.logo,
                        'titl': '',
                        'fon0': static("libraryTHPS/img/17db.jpg"),
                        'fon1': static("libraryTHPS/img/noise.gif")
                        })
                if thumb is not None and head is not None:
                    if thumb.logoCropCoordinates == head.logoCropCoordinates:
                        if request.is_ajax():
                            if "global" in request.GET:
                                return JsonResponse({"newPage": json.dumps({
                                    'url': request.path,
                                    'titl': '',
                                    'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                        'parentTemplate': 'base/fullPage/emptyParent.html',
                                        'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', ''),
                                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'logocrop'),
                                        'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'logocrop', item, mainItem.name)),
                                        'csrf': '',
                                        'form': crop(),
                                        'cropImage': mainItem.logo,
                                        'initialCropCoordinates': thumb.logoCropCoordinates,
                                        'fon0': static("libraryTHPS/img/17db.jpg"),
                                        'fon1': static("libraryTHPS/img/noise.gif")
                                        })
                                    })})
                            return JsonResponse({"newPage": json.dumps({
                                "templateHead": rts("libraryTHPS/head/add/crop.html"),
                                "templateBody": rts("libraryTHPS/body/add/crop.html", {
                                    'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', ''),
                                    'csrf': '',
                                    'form': crop(),
                                    'cropImage': mainItem.logo
                                    }),
                                "templateScripts": rts("libraryTHPS/scripts/add/crop.html", {
                                    'initialCropCoordinates': thumb.logoCropCoordinates,
                                }),
                                'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'logocrop', item, mainItem.name),
                                'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'logocrop'),
                                'titl': '',
                                "url": request.path,
                                "fon0": static("libraryTHPS/img/17db.jpg"),
                                "fon1": static("libraryTHPS/img/noise.gif")
                                })})
                        return render(request, "libraryTHPS/libraryTHPS.html", {
                            'parentTemplate': 'base/fullPage/base.html',
                            'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', ''),
                            'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'logocrop', item, mainItem.name)),
                            'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'logocrop'),
                            'csrf': checkCSRF(request),
                            'form': crop(),
                            'cropImage': mainItem.logo,
                            'initialCropCoordinates': thumb.logoCropCoordinates,
                            'titl': '',
                            'fon0': static("libraryTHPS/img/17db.jpg"),
                            'fon1': static("libraryTHPS/img/noise.gif")
                            })
                    vary = (thumb.logoCropCoordinates, head.logoCropCoordinates)
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'url': request.path,
                                'titl': '',
                                'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                    'parentTemplate': 'base/fullPage/emptyParent.html',
                                    'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', ''),
                                    'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'logocrop'),
                                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'logocrop', item, mainItem.name)),
                                    'csrf': '',
                                    'form': crop(),
                                    'cropImage': mainItem.logo,
                                    'initialCropCoordinates': vary[randint(0, 1)],
                                    'fon0': static("libraryTHPS/img/17db.jpg"),
                                    'fon1': static("libraryTHPS/img/noise.gif")
                                    })
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/crop.html"),
                            "templateBody": rts("libraryTHPS/body/add/crop.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', ''),
                                'csrf': '',
                                'form': crop(),
                                'cropImage': mainItem.logo
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/crop.html", {
                                'initialCropCoordinates': vary[randint(0, 1)]
                            }),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'logocrop', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'logocrop'),
                            'titl': '',
                            "url": request.path,
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })})
                    return render(request, "libraryTHPS/libraryTHPS.html", {
                        'parentTemplate': 'base/fullPage/base.html',
                        'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', ''),
                        'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'logocrop', item, mainItem.name)),
                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'logocrop'),
                        'csrf': checkCSRF(request),
                        'form': crop(),
                        'cropImage': mainItem.logo,
                        'initialCropCoordinates': vary[randint(0, 1)],
                        'titl': '',
                        'fon0': static("libraryTHPS/img/17db.jpg"),
                        'fon1': static("libraryTHPS/img/noise.gif")
                        })
                else:
                    if thumb is None:
                        if request.is_ajax():
                            if "global" in request.GET:
                                return JsonResponse({"newPage": json.dumps({
                                    'url': request.path,
                                    'titl': '',
                                    'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                        'parentTemplate': 'base/fullPage/emptyParent.html',
                                        'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', ''),
                                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'logocrop'),
                                        'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'logocrop', item, mainItem.name)),
                                        'csrf': '',
                                        'form': crop(),
                                        'cropImage': mainItem.logo,
                                        'initialCropCoordinates': head.logoCropCoordinates,
                                        'fon0': static("libraryTHPS/img/17db.jpg"),
                                        'fon1': static("libraryTHPS/img/noise.gif")
                                        })
                                    })})
                            return JsonResponse({"newPage": json.dumps({
                                "templateHead": rts("libraryTHPS/head/add/crop.html"),
                                "templateBody": rts("libraryTHPS/body/add/crop.html", {
                                    'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', ''),
                                    'csrf': '',
                                    'form': crop(),
                                    'cropImage': mainItem.logo
                                    }),
                                "templateScripts": rts("libraryTHPS/scripts/add/crop.html", {
                                    'initialCropCoordinates': head.logoCropCoordinates
                                    }),
                                'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'logocrop', item, mainItem.name),
                                'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'logocrop'),
                                'titl': '',
                                "url": request.path,
                                "fon0": static("libraryTHPS/img/17db.jpg"),
                                "fon1": static("libraryTHPS/img/noise.gif")
                                })})
                        return render(request, "libraryTHPS/libraryTHPS.html", {
                            'parentTemplate': 'base/fullPage/base.html',
                            'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', ''),
                            'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'logocrop', item, mainItem.name)),
                            'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'logocrop'),
                            'csrf': checkCSRF(request),
                            'form': crop(),
                            'cropImage': mainItem.logo,
                            'initialCropCoordinates': head.logoCropCoordinates,
                            'titl': '',
                            'fon0': static("libraryTHPS/img/17db.jpg"),
                            'fon1': static("libraryTHPS/img/noise.gif")
                            })
                    if request.is_ajax():
                        if "global" in request.GET:
                            return JsonResponse({"newPage": json.dumps({
                                'url': request.path,
                                'titl': '',
                                'newTemplate': rts("libraryTHPS/libraryTHPS.html", {
                                    'parentTemplate': 'base/fullPage/emptyParent.html',
                                    'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', ''),
                                    'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'logocrop'),
                                    'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'logocrop', item, mainItem.name)),
                                    'csrf': '',
                                    'form': crop(),
                                    'cropImage': mainItem.logo,
                                    'initialCropCoordinates': thumb.logoCropCoordinates,
                                    'fon0': static("libraryTHPS/img/17db.jpg"),
                                    'fon1': static("libraryTHPS/img/noise.gif")
                                    })
                                })})
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/crop.html"),
                            "templateBody": rts("libraryTHPS/body/add/crop.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', ''),
                                'csrf': '',
                                'form': crop(),
                                'cropImage': mainItem.logo
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/crop.html", {
                                'initialCropCoordinates': thumb.logoCropCoordinates
                                }),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'logocrop', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'logocrop'),
                            'titl': '',
                            "url": request.path,
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })})
                    return render(request, "libraryTHPS/libraryTHPS.html", {
                        'parentTemplate': 'base/fullPage/base.html',
                        'content': ('libraryTHPS', 'add', 'clan', 'crop', 'logo', ''),
                        'menu': json.dumps(buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'both', 'logocrop', item, mainItem.name)),
                        'breadcrumbs': ('clans', 'editDetail', 'editBoth', 'logocrop'),
                        'csrf': checkCSRF(request),
                        'form': crop(),
                        'cropImage': mainItem.logo,
                        'initialCropCoordinates': thumb.logoCropCoordinates,
                        'titl': '',
                        'fon0': static("libraryTHPS/img/17db.jpg"),
                        'fon1': static("libraryTHPS/img/noise.gif")
                        })
    elif request.method == "POST":
        if section == "miniature":
            if step == "ground":
                if mainItem.background == False:
                    if request.is_ajax():
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/content.html"),
                            "templateBody": rts("libraryTHPS/body/add/image.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'ground'),
                                'csrf': '',
                                'form': addBackground(),
                                'messages': ("Вы не можете произвести обрезку фона для миниатюры клана до тех пор, пока оно не было загружено или не было выбрано. \
                                Чтобы продолжить, выберите изображение для формы ниже.")
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'ground', item, mainItem.name),
                            'breadcrumbs': ('clans', 'add', 'crop'),
                            "titl": "",
                            "url": reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'ground'}),
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })
                                             })
                    messages.add_message(request, messages.WARNING,
                                         "Вы не можете произвести обрезку изображения фона для миниатюры клана до тех пор, пока оно не было загружено или не было выбрано. \
                                         Чтобы продолжить, выберите изображение для формы ниже."
                                         )
                    return redirect("libraryAddClanContentUrl", item, 'ground')
                form = crop(request.POST, img = mainItem.background)
                if form.is_valid():
                    cropResult = croppingFunction(mainItem.background, tuple(map(int, form.cleaned_data["cropCoordinates"].split(","))))
                    try:
                        thumb = clansThumbAttrs.objects.get(pk = mainItem.pk)
                    except clansThumbAttrs.DoesNotExist:
                        thumb = None
                    if thumb is None:
                        thumb = clansThumbAttrs()
                        thumb.clanItem = mainItem
                        thumb.backgroundCropCoordinates = form.cleaned_data["cropCoordinates"]
                        thumb.backgroundCrop.save(cropResult[0], ContentFile(cropResult[1]), save = False)
                        thumb.addedBy = request.user
                    else:
                        thumb.backgroundCrop.delete(save = False)
                        thumb.backgroundCropCoordinates = form.cleaned_data["cropCoordinates"]
                        thumb.backgroundCrop.save(cropResult[0], ContentFile(cropResult[1]), save = False)
                        if thumb.isMaded == 1:
                            thumb.logoPosition = changePosition(tuple(map(int, thumb.logoPosition.split(","))), mainItem.background.width,
                                                                mainItem.background.height, thumb.backgroundCrop.width, thumb.backgroundCrop.height)
                        elif thumb.isMaded == 2:
                            pass
                        elif thumb.isMaded == 3:
                            pass
                        try:
                            thumbDefaultSettings = libraryClanThumbDefaultService.objects.get(pk = 1)
                        except:
                            thumbDefaultSettings = None
                        try:
                            thumbSettings = libraryClanThumbService.objects.get(pk = request.user)
                        except:
                            thumbSettings = None
                        if thumbDefaultSettings is not None:
                            currentPath = os.path.normpath("%s/libraryTHPS/cache/clan/%s/thumb/default/background/" % (settings.MEDIA_ROOT, item))
                            for path, dirs, files in os.walk(currentPath):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                            for path, dirs, files in os.walk(os.path.normpath("%s/crop/" % currentPath)):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                        if thumbSettings is not None:
                            currentPath = os.path.normpath("%s/libraryTHPS/cache/clan/%s/thumb/%d/background/" % (settings.MEDIA_ROOT, item, request.user.id))
                            for path, dirs, files in os.walk(currentPath):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                            for path, dirs, files in os.walk(os.path.normpath("%s/crop/" % currentPath)):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                    thumb.save()
                    if cookie is None:
                        if request.is_ajax():
                            return JsonResponse({
                                "redirectTo": json.dumps("%s%s" % (reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}), "?type=fullversion"))
                            }) 
                        return redirect("storageShowItemUrl", 'clan', item)
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': 'miniature', 'urlSep': '/', 'step': 'logo'}))
                        })
                    return redirect("libraryAddClanCropUrl", item, 'miniature', '/', 'logo')
            else:
                if mainItem.logo == False:
                    if request.is_ajax():
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/content.html"),
                            "templateBody": rts("libraryTHPS/body/add/image.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'logo'),
                                'csrf': '',
                                'form': addLogo(background = mainItem.background, isAddedBackground = mainItem.background),
                                'messages': ("Вы не можете произвести обрезку логотипа для миниатюры клана до тех пор, пока оно не было загружено или не было выбрано. \
                                Чтобы продолжить, выберите изображение для формы ниже.")
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'logo', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editInfo', 'logo'),
                            "titl": "",
                            "url": reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'logo'}),
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })
                                             })
                    messages.add_message(request, messages.WARNING,
                                         "Вы не можете произвести обрезку изображения логотипа для миниатюры клана до тех пор, пока оно не было загружено или не было выбрано. \
                                         Чтобы продолжить, выберите изображение для формы ниже."
                                         )
                    return redirect("libraryAddClanContentUrl", item, 'logo')
                form = crop(request.POST, img = mainItem.logo)
                if form.is_valid():
                    cropResult = croppingFunction(mainItem.logo, tuple(map(int, form.cleaned_data["cropCoordinates"].split(","))))
                    try:
                        thumb = clansThumbAttrs.objects.get(pk = mainItem.pk)
                    except clansThumbAttrs.DoesNotExist:
                        thumb = None
                    if thumb is None:
                        thumb = clansThumbAttrs()
                        thumb.clanItem = mainItem
                        thumb.logoCropCoordinates = form.cleaned_data["cropCoordinates"]
                        thumb.logoCrop.save(cropResult[0], ContentFile(cropResult[1]), save = False)
                        thumb.addedBy = request.user
                    else:
                        thumb.logoCrop.delete(save = False)
                        thumb.logoCropCoordinates = form.cleaned_data["cropCoordinates"]
                        thumb.logoCrop.save(cropResult[0], ContentFile(cropResult[1]), save = False)
                        if thumb.isMaded == 1:
                            thumb.logoPosition = changePosition(tuple(map(int, thumb.logoPosition.split(","))), mainItem.logo.width,
                                                                mainItem.logo.height, thumb.logoCrop.width, thumb.logoCrop.height)
                        elif thumb.isMaded == 2:
                            pass
                        elif thumb.isMaded == 3:
                            pass
                        try:
                            thumbDefaultSettings = libraryClanThumbDefaultService.objects.get(pk = 1)
                        except:
                            thumbDefaultSettings = None
                        try:
                            thumbSettings = libraryClanThumbService.objects.get(pk = request.user)
                        except:
                            thumbSettings = None
                        if thumbDefaultSettings is not None:
                            currentPath = os.path.normpath("%s/libraryTHPS/cache/clan/%s/thumb/default/logo/" % (settings.MEDIA_ROOT, item))
                            for path, dirs, files in os.walk(currentPath):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                            for path, dirs, files in os.walk(os.path.normpath("%s/crop/" % currentPath)):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                        if thumbSettings is not None:
                            currentPath = os.path.normpath("%s/libraryTHPS/cache/clan/%s/thumb/%d/logo/" % (settings.MEDIA_ROOT, item, request.user.id))
                            for path, dirs, files in os.walk(currentPath):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                            for path, dirs, files in os.walk(os.path.normpath("%s/crop/" % currentPath)):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                    thumb.save()
                    if cookie is None:
                        if request.is_ajax():
                            return JsonResponse({
                                "redirectTo": json.dumps("%s%s" % (reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}), "?type=fullversion"))
                            }) 
                        return redirect("storageShowItemUrl", 'clan', item)
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(reverse("libraryAddClanMadeUrl", kwargs = {'item': item, 'section': 'miniature', 'urlSep': '/'}))
                        })
                    return redirect("libraryAddClanMadeUrl", item, 'miniature', '/')
        elif section == "headpiece":
            if step == "ground":
                if mainItem.background == False:
                    if request.is_ajax():
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/content.html"),
                            "templateBody": rts("libraryTHPS/body/add/image.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'ground'),
                                'csrf': '',
                                'form': addBackground(),
                                'messages': ("Вы не можете произвести обрезку фона для страницы клана до тех пор, пока оно не было загружено или не было выбрано. \
                                Чтобы продолжить, выберите изображение для формы ниже.")
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'ground', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editInfo', 'ground'),
                            "titl": "",
                            "url": reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'ground'}),
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })
                                             })
                    messages.add_message(request, messages.WARNING,
                                         "Вы не можете произвести обрезку изображения фона для страницы клана до тех пор, пока оно не было загружено или не было выбрано. \
                                         Чтобы продолжить, выберите изображение для формы ниже."
                                         )
                    return redirect("libraryAddClanContentUrl", item, 'ground')
                form = crop(request.POST, img = mainItem.background)
                if form.is_valid():
                    cropResult = croppingFunction(mainItem.background, tuple(map(int, form.cleaned_data["cropCoordinates"].split(","))))
                    try:
                        head = clansItemAttrs.objects.get(pk = mainItem.pk)
                    except clansItemAttrs.DoesNotExist:
                        head = None
                    if head is None:
                        head = clansItemAttrs()
                        head.clanItem = mainItem
                        head.backgroundCropCoordinates = form.cleaned_data["cropCoordinates"]
                        head.backgroundCrop.save(cropResult[0], ContentFile(cropResult[1]), save = False)
                        head.addedBy = request.user
                    else:
                        head.backgroundCrop.delete(save = False)
                        head.backgroundCropCoordinates = form.cleaned_data["cropCoordinates"]
                        head.backgroundCrop.save(cropResult[0], ContentFile(cropResult[1]), save = False)
                        if head.isMaded == 1:
                            head.logoPosition = changePosition(tuple(map(int, head.logoPosition.split(","))), mainItem.background.width,
                                                                mainItem.background.height, head.backgroundCrop.width, head.backgroundCrop.height)
                        elif head.isMaded == 2:
                            pass
                        elif head.isMaded == 3:
                            pass
                        try:
                            headDefaultSettings = libraryClanPageDefaultService.objects.get(pk = 1)
                        except:
                            headDefaultSettings = None
                        try:
                            headSettings = libraryClanPageService.objects.get(pk = request.user)
                        except:
                            headSettings = None
                        if headDefaultSettings is not None:
                            currentPath = os.path.normpath("%s/libraryTHPS/cache/clan/%s/head/default/background/" % (settings.MEDIA_ROOT, item))
                            for path, dirs, files in os.walk(currentPath):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                            for path, dirs, files in os.walk(os.path.normpath("%s/crop/" % currentPath)):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                        if headSettings is not None:
                            currentPath = os.path.normpath("%s/libraryTHPS/cache/clan/%s/head/%d/background/" % (settings.MEDIA_ROOT, item, request.user.id))
                            for path, dirs, files in os.walk(currentPath):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                            for path, dirs, files in os.walk(os.path.normpath("%s/crop/" % currentPath)):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                    head.save()
                    if cookie is None:
                        if request.is_ajax():
                            return JsonResponse({
                                "redirectTo": json.dumps("%s%s" % (reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}), "?type=fullversion"))
                            }) 
                        return redirect("storageShowItemUrl", 'clan', item)
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': 'headpiece', 'urlSep': '/', 'step': 'logo'}))
                        })
                    return redirect("libraryAddClanCropUrl", item, 'headpiece', '/', 'logo')
            else:
                if mainItem.logo == False:
                    if request.is_ajax():
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/content.html"),
                            "templateBody": rts("libraryTHPS/body/add/image.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'logo'),
                                'csrf': '',
                                'form': addLogo(background = mainItem.background, isAddedBackground = mainItem.background),
                                'messages': ("Вы не можете произвести обрезку логотипа для страницы клана до тех пор, пока оно не было загружено или не было выбрано. \
                                Чтобы продолжить, выберите изображение для формы ниже.")
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'logo', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editInfo', 'logo'),
                            "titl": "",
                            "url": reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'logo'}),
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })
                                             })
                    messages.add_message(request, messages.WARNING,
                                         "Вы не можете произвести обрезку изображения логотипа для страницы клана до тех пор, пока оно не было загружено или не было выбрано. \
                                         Чтобы продолжить, выберите изображение для формы ниже."
                                         )
                    return redirect("libraryAddClanContentUrl", item, 'logo')
                form = crop(request.POST, img = mainItem.logo)
                if form.is_valid():
                    cropResult = croppingFunction(mainItem.logo, tuple(map(int, form.cleaned_data["cropCoordinates"].split(","))))
                    try:
                        head = clansItemAttrs.objects.get(pk = mainItem.pk)
                    except clansItemAttrs.DoesNotExist:
                        head = None
                    if head is None:
                        head = clansItemAttrs()
                        head.clanItem = mainItem
                        head.logoCropCoordinates = form.cleaned_data["cropCoordinates"]
                        head.logoCrop.save(cropResult[0], ContentFile(cropResult[1]), save = False)
                        head.addedBy = request.user
                    else:
                        head.logoCrop.delete(save = False)
                        head.logoCropCoordinates = form.cleaned_data["cropCoordinates"]
                        head.logoCrop.save(cropResult[0], ContentFile(cropResult[1]), save = False)
                        if head.isMaded == 1:
                            head.logoPosition = changePosition(tuple(map(int, head.logoPosition.split(","))), mainItem.logo.width,
                                                                mainItem.logo.height, head.logoCrop.width, head.logoCrop.height)
                        elif head.isMaded == 2:
                            pass
                        elif head.isMaded == 3:
                            pass
                        try:
                            headDefaultSettings = libraryClanPageDefaultService.objects.get(pk = 1)
                        except:
                            headDefaultSettings = None
                        try:
                            headSettings = libraryClanPageService.objects.get(pk = request.user)
                        except:
                            headSettings = None
                        if headDefaultSettings is not None:
                            currentPath = os.path.normpath("%s/libraryTHPS/cache/clan/%s/head/default/logo/" % (settings.MEDIA_ROOT, item))
                            for path, dirs, files in os.walk(currentPath):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                            for path, dirs, files in os.walk(os.path.normpath("%s/crop/" % currentPath)):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                        if headSettings is not None:
                            currentPath = os.path.normpath("%s/libraryTHPS/cache/clan/%s/head/%d/logo/" % (settings.MEDIA_ROOT, item, request.user.id))
                            for path, dirs, files in os.walk(currentPath):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                            for path, dirs, files in os.walk(os.path.normpath("%s/crop/" % currentPath)):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                    head.save()
                    if cookie is None:
                        if request.is_ajax():
                            return JsonResponse({
                                "redirectTo": json.dumps("%s%s" % (reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}), "?type=fullversion"))
                            }) 
                        return redirect("storageShowItemUrl", 'clan', item)
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(reverse("libraryAddClanMadeUrl", kwargs = {'item': item, 'section': 'headpiece', 'urlSep': '/'}))
                        })
                    return redirect("libraryAddClanMadeUrl", item, 'headpiece', '/')
        else:
            if mainItem.background == False:
                if step == "ground":
                    if request.is_ajax():
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/content.html"),
                            "templateBody": rts("libraryTHPS/body/add/image.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'ground'),
                                'csrf': '',
                                'form': addBackground(),
                                'messages': ("Вы не можете произвести обрезку фона до тех пор, пока оно не было загружено или не было выбрано. \
                                Чтобы продолжить, выберите изображение для формы ниже.")
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'ground', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editInfo', 'ground'),
                            "titl": "",
                            "url": reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'ground'}),
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })
                                             })
                    messages.add_message(request, messages.WARNING,
                                         "Вы не можете произвести обрезку изображения фона до тех пор, пока оно не было загружено или не было выбрано. \
                                         Чтобы продолжить, выберите изображение для формы ниже."
                                         )
                    return redirect("libraryAddClanContentUrl", item, 'ground')
            if mainItem.logo == False:
                if step == "logo":
                    if request.is_ajax():
                        return JsonResponse({"newPage": json.dumps({
                            "templateHead": rts("libraryTHPS/head/add/content.html"),
                            "templateBody": rts("libraryTHPS/body/add/image.html", {
                                'content': ('libraryTHPS', 'add', 'clan', 'logo'),
                                'csrf': '',
                                'form': addLogo(background = mainItem.background, isAddedBackground = mainItem.background),
                                'messages': ("Вы не можете произвести обрезку логотипа до тех пор, пока оно не было загружено или не было выбрано. \
                                Чтобы продолжить, выберите изображение для формы ниже.")
                                }),
                            "templateScripts": rts("libraryTHPS/scripts/add/content.html"),
                            'menu': buildEditClanMenu(filterFutureSteps(item = mainItem, thumb = thumb, page = head), 'content', 'logo', item, mainItem.name),
                            'breadcrumbs': ('clans', 'editInfo', 'logo'),
                            "titl": "",
                            "url": reverse("libraryAddClanContentUrl", kwargs = {'item': item, 'step': 'logo'}),
                            "fon0": static("libraryTHPS/img/17db.jpg"),
                            "fon1": static("libraryTHPS/img/noise.gif")
                            })
                                             })
                    messages.add_message(request, messages.WARNING,
                                         "Вы не можете произвести обрезку изображения логотипа до тех пор, пока оно не было загружено или не было выбрано. \
                                         Чтобы продолжить, выберите изображение для формы ниже."
                                         )
                    return redirect("libraryAddClanContentUrl", item, 'logo')
            if step == "ground":
                form = crop(request.POST, img = mainItem.background)
                if form.is_valid():
                    cropResult = croppingFunction(mainItem.background, tuple(map(int, form.cleaned_data["cropCoordinates"].split(","))))
                    try:
                        thumb = clansThumbAttrs.objects.get(pk = mainItem.pk)
                    except clansThumbAttrs.DoesNotExist:
                        thumb = None
                    try:
                        head = clansItemAttrs.objects.get(pk = mainItem.pk)
                    except clansItemAttrs.DoesNotExist:
                        head = None
                    image = ContentFile(cropResult[1])
                    if thumb is None:
                        thumb = clansThumbAttrs()
                        thumb.clanItem = mainItem
                        thumb.backgroundCropCoordinates = form.cleaned_data["cropCoordinates"]
                        thumb.backgroundCrop.save(cropResult[0], image, save = False)
                        thumb.addedBy = request.user
                    else:
                        thumb.backgroundCrop.delete(save = False)
                        thumb.backgroundCropCoordinates = form.cleaned_data["cropCoordinates"]
                        thumb.backgroundCrop.save(cropResult[0], image, save = False)
                        if thumb.isMaded == 1:
                            thumb.logoPosition = changePosition(tuple(map(int, thumb.logoPosition.split(","))), mainItem.background.width,
                                                                mainItem.background.height, thumb.backgroundCrop.width, thumb.backgroundCrop.height)
                        elif thumb.isMaded == 2:
                            pass
                        elif thumb.isMaded == 3:
                            pass
                        try:
                            thumbDefaultSettings = libraryClanThumbDefaultService.objects.get(pk = 1)
                        except:
                            thumbDefaultSettings = None
                        try:
                            thumbSettings = libraryClanThumbService.objects.get(pk = request.user)
                        except:
                            thumbSettings = None
                        try:
                            headDefaultSettings = libraryClanPageDefaultService.objects.get(pk = 1)
                        except:
                            headDefaultSettings = None
                        try:
                            headSettings = libraryClanPageService.objects.get(pk = request.user)
                        except:
                            headSettings = None
                        if thumbDefaultSettings is not None: # Удалить из папок logo тоже
                            currentPath = os.path.normpath("%s/libraryTHPS/cache/clan/%s/thumb/default/background/" % (settings.MEDIA_ROOT, item))
                            for path, dirs, files in os.walk(currentPath):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                            for path, dirs, files in os.walk(os.path.normpath("%s/crop/" % currentPath)):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                        if thumbSettings is not None:
                            currentPath = os.path.normpath("%s/libraryTHPS/cache/clan/%s/thumb/%d/background/" % (settings.MEDIA_ROOT, item, request.user.id))
                            for path, dirs, files in os.walk(currentPath):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                            for path, dirs, files in os.walk(os.path.normpath("%s/crop/" % currentPath)):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                        if headDefaultSettings is not None:
                            currentPath = os.path.normpath("%s/libraryTHPS/cache/clan/%s/head/default/background/" % (settings.MEDIA_ROOT, item))
                            for path, dirs, files in os.walk(currentPath):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                            for path, dirs, files in os.walk(os.path.normpath("%s/crop/" % currentPath)):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                        if headSettings is not None:
                            currentPath = os.path.normpath("%s/libraryTHPS/cache/clan/%s/head/%d/background/" % (settings.MEDIA_ROOT, item, request.user.id))
                            for path, dirs, files in os.walk(currentPath):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                            for path, dirs, files in os.walk(os.path.normpath("%s/crop/" % currentPath)):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                    if head is None:
                        head = clansItemAttrs()
                        head.clanItem = mainItem
                        head.backgroundCropCoordinates = form.cleaned_data["cropCoordinates"]
                        head.backgroundCrop.save(cropResult[0], image, save = False)
                        head.addedBy = request.user
                    else:
                        head.backgroundCrop.delete(save = False)
                        head.backgroundCropCoordinates = form.cleaned_data["cropCoordinates"]
                        head.backgroundCrop.save(cropResult[0], image, save = False)
                        if head.isMaded == 1:
                            head.logoPosition = changePosition(tuple(map(int, head.logoPosition.split(","))), mainItem.background.width,
                                                                mainItem.background.height, head.backgroundCrop.width, head.backgroundCrop.height)
                        elif head.isMaded == 2:
                            pass
                        elif head.isMaded == 3:
                            pass
                    with transaction.atomic():
                        head.save()
                        thumb.save()
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(request.path)
                        })
                    return redirect(request.path)
                if cookie == '':
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps("%s%s" % (reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}), "?type=fullversion"))
                        })
                    return redirect("storageShowItemUrl", 'clan', item)
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps(reverse("libraryAddClanCropUrl", kwargs = {'item': item, 'section': '', 'urlSep': '', 'step': 'logo'}))
                        })
                return redirect("libraryAddClanCropUrl", item, '', '', 'logo')
            else:
                form = crop(request.POST, img = mainItem.logo)
                if form.is_valid():
                    cropResult = croppingFunction(mainItem.logo, tuple(map(int, form.cleaned_data["cropCoordinates"].split(","))))
                    try:
                        thumb = clansThumbAttrs.objects.get(pk = mainItem.pk)
                    except clansThumbAttrs.DoesNotExist:
                        thumb = None
                    try:
                        head = clansItemAttrs.objects.get(pk = mainItem.pk)
                    except clansItemAttrs.DoesNotExist:
                        head = None
                    image = ContentFile(cropResult[1])
                    if thumb is None:
                        thumb = clansThumbAttrs()
                        thumb.clanItem = mainItem
                        thumb.logoCropCoordinates = form.cleaned_data["cropCoordinates"]
                        thumb.logoCrop.save(cropResult[0], image, save = False)
                        thumb.addedBy = request.user
                    else:
                        thumb.logoCrop.delete(save = False)
                        thumb.logoCropCoordinates = form.cleaned_data["cropCoordinates"]
                        thumb.logoCrop.save(cropResult[0], image, save = False)
                        if thumb.isMaded == 1:
                            thumb.logoPosition = changePosition(tuple(map(int, thumb.logoPosition.split(","))), mainItem.logo.width,
                                                                mainItem.logo.height, thumb.logoCrop.width, thumb.logoCrop.height)
                        elif thumb.isMaded == 2:
                            pass
                        elif thumb.isMaded == 3:
                            pass
                    if head is None:
                        head = clansItemAttrs()
                        head.clanItem = mainItem
                        head.logoCropCoordinates = form.cleaned_data["cropCoordinates"]
                        head.logoCrop.save(cropResult[0], image, save = False)
                        head.addedBy = request.user
                    else:
                        head.logoCrop.delete(save = False)
                        head.logoCropCoordinates = form.cleaned_data["cropCoordinates"]
                        head.logoCrop.save(cropResult[0], image, save = False)
                        if head.isMaded == 1:
                            head.logoPosition = changePosition(tuple(map(int, head.logoPosition.split(","))), mainItem.logo.width,
                                                                mainItem.logo.height, head.logoCrop.width, head.logoCrop.height)
                        elif head.isMaded == 2:
                            pass
                        elif head.isMaded == 3:
                            pass
                        try:
                            thumbDefaultSettings = libraryClanThumbDefaultService.objects.get(pk = 1)
                        except:
                            thumbDefaultSettings = None
                        try:
                            thumbSettings = libraryClanThumbService.objects.get(pk = request.user)
                        except:
                            thumbSettings = None
                        try:
                            headDefaultSettings = libraryClanPageDefaultService.objects.get(pk = 1)
                        except:
                            headDefaultSettings = None
                        try:
                            headSettings = libraryClanPageService.objects.get(pk = request.user)
                        except:
                            headSettings = None
                        if thumbDefaultSettings is not None:
                            currentPath = os.path.normpath("%s/libraryTHPS/cache/clan/%s/thumb/default/logo/" % (settings.MEDIA_ROOT, item))
                            for path, dirs, files in os.walk(currentPath):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                            for path, dirs, files in os.walk(os.path.normpath("%s/crop/" % currentPath)):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                        if thumbSettings is not None:
                            currentPath = os.path.normpath("%s/libraryTHPS/cache/clan/%s/thumb/%d/logo/" % (settings.MEDIA_ROOT, item, request.user.id))
                            for path, dirs, files in os.walk(currentPath):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                            for path, dirs, files in os.walk(os.path.normpath("%s/crop/" % currentPath)):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                        if headDefaultSettings is not None:
                            currentPath = os.path.normpath("%s/libraryTHPS/cache/clan/%s/head/default/logo/" % (settings.MEDIA_ROOT, item))
                            for path, dirs, files in os.walk(currentPath):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                            for path, dirs, files in os.walk(os.path.normpath("%s/crop/" % currentPath)):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                        if headSettings is not None:
                            currentPath = os.path.normpath("%s/libraryTHPS/cache/clan/%s/head/%d/logo/" % (settings.MEDIA_ROOT, item, request.user.id))
                            for path, dirs, files in os.walk(currentPath):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                            for path, dirs, files in os.walk(os.path.normpath("%s/crop/" % currentPath)):
                                for image in files:
                                    os.remove(os.path.join(path, image))
                    with transaction.atomic():
                        head.save()
                        thumb.save()
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps(request.path)
                        })
                    return redirect(request.path)
                if cookie == '':
                    if request.is_ajax():
                        return JsonResponse({
                            "redirectTo": json.dumps("%s%s" % (reverse("storageShowItemUrl", kwargs = {'typeOfItem': 'clan', 'item': item}), "?type=fullversion"))
                        })
                    return redirect("storageShowItemUrl", 'clan', item)
                if request.is_ajax():
                    return JsonResponse({
                        "redirectTo": json.dumps(reverse("libraryAddClanMadeUrl", kwargs = {'item': item, 'section': '', 'urlSep': ''}))
                        })
                return redirect("libraryAddClanMadeUrl", item, '', '')