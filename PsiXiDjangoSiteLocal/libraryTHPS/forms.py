import os, copy, io
from django import forms
from libraryTHPS.models import clansMain, clansThumbAttrs, clansItemAttrs
from libraryTHPS.models import playersShip, mapsShip, libraryClanThumbService, STATUS
from django.utils.text import slugify
from imagekit import ImageSpec
from imagekit.processors import resize, ResizeToFit, ResizeToFill
from django.conf import settings # Убрать на боевом

addingTypes = (
    ("c", "clan"),
    ("p", "player"),
    ("m", "map"),
)

class libraryTHPSaddType(forms.Form):
    typeOfAdd = forms.ChoiceField(required = True, choices = addingTypes)

class addClanFirst(forms.ModelForm):
    class Meta:
        model = clansMain
        fields = ('name', 'slugName', 'addedBy', 'editedBy', 'status',)
        widgets = {
            'slugName': forms.HiddenInput,
            'addedBy': forms.HiddenInput,
            'editedBy': forms.HiddenInput,
            'status': forms.HiddenInput,
        }
    def __init__(self, *args, **kwargs):
        self.isAlreadyAdded = kwargs.pop("isExistItem")
        self.curSlug = kwargs.pop("slug", None)
        super(addClanFirst, self).__init__(*args, **kwargs)
        if self.isAlreadyAdded == True:
            del self.fields["addedBy"], self.fields["status"]

    def clean_slugName(self):
        slug = slugify(self.cleaned_data.get("name", None))
        if self.isAlreadyAdded == True:
            if self.curSlug == slug:
                raise forms.ValidationError("Выберите другое название.")
        self.cleaned_data["slugName"] = slug
        return self.cleaned_data["slugName"]

class addClanDetail(forms.ModelForm):
    class Meta:
        model = clansMain
        fields = ('description', 'foundedDate', 'clanInStyles',
                  'members', 'editedBy', 'status',)
        widgets = {
            'editedBy': forms.HiddenInput,   
        }
    status = forms.ChoiceField(required = False, choices = STATUS)

class addBackground(forms.ModelForm):
    class Meta:
        model = clansMain
        fields = ('background',
                  #'addedBy', 'editedBy',
                  )
        widgets = {
            'background': forms.FileInput(attrs = {'onchange': 'doPostForm("libraryTHPSContent", document.forms.libraryForm, location.pathname);'}),
            #'addedBy': forms.HiddenInput,
            #'editedBy': forms.HiddenInput,
        }

class addLogo(forms.ModelForm):
    class Meta:
        model = clansMain
        fields = ('logo', 'bckgrAndLgIdent',
                  #'editedBy',
                  )
        widgets = {
            'logo': forms.FileInput(attrs = {'onchange': 'doPostForm("libraryTHPSContent", document.forms.libraryForm, location.pathname);',}),
            #'editedBy': forms.HiddenInput,
        }

    bckgrAndLgIdent = forms.BooleanField(required = False, initial = False, label = "Использовать текущий фон в качестве логотипа",
                                         widget = forms.CheckboxInput(attrs = {
                                             'onchange': 'this.checked === true ? doPostForm("libraryTHPSContent", document.forms.libraryForm, location.pathname) : 0;'
                                             }),
                                         help_text = "Вы можете вырезать логотип из уже загруженного фона своего клана.",)

    def __init__(self, *args, **kwargs):
        self.background = kwargs.pop("background", None)
        super(addLogo, self).__init__(*args, **kwargs)
        if self.background == False:
            del self.fields['bckgrAndLgIdent']

    def clean(self):
        if self.background:
            bckgrAndLgIdent = self.cleaned_data['bckgrAndLgIdent']
            if bckgrAndLgIdent == True:
                self.cleaned_data['logo'] = self.background
        return self.cleaned_data

class crop(forms.Form):
    cropCoordinates = forms.CharField(max_length = 20, required = True, widget = forms.HiddenInput)

    def __init__(self, *args, **kwargs):
        self.image = kwargs.pop("img", None)
        super(crop, self).__init__(*args, **kwargs)

    def clean_cropCoordinates(self):
        coordinates = tuple(map(int, self.cleaned_data['cropCoordinates'].split(",")))
        imageSizeTuple = self.image.size
        if imageSizeTuple < coordinates[2] - coordinates[0]:
            raise forms.ValidationError("", code = 'invalid')
        if imageSizeTuple < coordinates[3] - coordinates[1]:
            raise forms.ValidationError("", code = 'invalid')
        return self.cleaned_data["cropCoordinates"]

    def clean(self):
        if not os.path.exists(os.path.normpath("%s%s" % (settings.BASE_DIR, self.image.url))):
            raise forms.ValidationError("", code = 'invalid')
        return self.cleaned_data

class made(forms.Form):
    logoPosition = forms.CharField(max_length = 20, required = True, widget = forms.HiddenInput)
    isMaded = forms.CharField(required = True, widget = forms.HiddenInput)
    logoProportions = forms.CharField(max_length = 50, required = True, widget = forms.HiddenInput)

    def __init__(self, *args, **kwargs):
        self.rootImage = kwargs.pop("rImg", None)
        self.madeImage = kwargs.pop("mImg", None)
        super(made, self).__init__(*args, **kwargs)

    def clean_logoPosition(self):
        pass
        return self.cleaned_data["logoPosition"]

    def clean(self):
        if not os.path.exists(os.path.normpath("%s%s" % (settings.BASE_DIR, self.rootImage.url))):
            raise forms.ValidationError("", code = 'error')
        if not os.path.exists(os.path.normpath("%s%s" % (settings.BASE_DIR, self.madeImage.url))):
            raise forms.ValidationError("", code = 'error')
        return self.cleaned_data

class libraryTHPSAddClanWatch(forms.ModelForm):
    class Meta:
        model = clansItemAttrs
        fields = ('showFormat', 'clanColor',)
        widgets = {'clanColor': forms.HiddenInput}

    def __init__(self, *args, **kwargs):
        self.backgroundAndLogoIdent = kwargs.pop("ghjfgj")
        super(libraryTHPSAddClanWatch, self).__init__(*args, **kwargs)

    com = {1: 'Показывать фон', 2: 'Показывать логотип', 3: 'Показывать наложение логотипа на фон'}

    showFormat = forms.ChoiceField(required = True, choices = com)


class headResize(forms.ModelForm):
    class Meta:
        model = clansItemAttrs
        fields = ('logoProportions',)
        widgets = {
            'logoProportions': forms.HiddenInput,
            }

class thumbResize(forms.ModelForm):
    class Meta:
        model = clansThumbAttrs
        fields = ('logoProportions',)
        widgets = {
            'logoProportions': forms.HiddenInput,
            }

class resizeUnited(forms.Form):
    logoProportions = forms.CharField(max_length = 20, required = True, widget = forms.HiddenInput)

class addPlayerFirst(forms.ModelForm):
    class Meta:
        model = playersShip
        fields = ('name', 'description', 'image', 'status', 'createdBy',)

class addPlayerDetail(forms.ModelForm):
    class Meta:
        model = playersShip
        fields = ('foundedDate', 'playerIn', 'status', 'createdBy',)