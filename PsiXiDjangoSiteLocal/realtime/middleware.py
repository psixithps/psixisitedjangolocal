import threading, json, sys, logging, traceback
from datetime import datetime
from dateutil import tz
from django.utils.functional import cached_property
from django.core.cache import caches
from django.conf import settings
from django.urls import reverse
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.db import OperationalError, ProgrammingError, Error
ServerError, memcashedError = BaseException, BaseException # Заглушка
#from pylibmc import Error as memcachedError, ServerError Раскомментировать на продакшене

logg = logging.getLogger(__name__)

ONLINE_THRESHOLD_MAX = getattr(settings, 'ONLINE_THRESHOLD_MAX', 600000) # ВебСокет
ONLINE_THRESHOLD_FULL = getattr(settings, 'ONLINE_THRESHOLD_FULL', 300) # Снхронный HTTP
ONLINE_THRESHOLD_SHORT = getattr(settings, 'ONLINE_THRESHOLD_SHORT', 30) # XHR
IFFY_THRESHOLD = getattr(settings, 'IFFY_THRESHOLD', 20)

ONLINE_MAX = getattr(settings, 'ONLINE_MAX', 150) # Ограничитель количества
IFFY_MAX = getattr(settings, 'IFFY_MAX', 200) # Ограничитель количества

ACTIVE_TIME_SHORT = getattr(settings, 'ACTIVE_TIME_SHORT', 40) # Chat status
ACTIVE_TIME_FULL = 210 # Chat status
MIDDLE_TIME_FULL = 240 # Chat status


def setUserLeftTime(userId, outTime, fromSockClose, disconnectCallback):
	if fromSockClose:
		try:
			caches['onlineUsers'].delete(userId)
			caches['iffyUsersOnlineStatus'].set(userId, True, IFFY_THRESHOLD)
		except ServerError as errorMsg:
			logg.log(logging.CRITICAL, "[{0}]\tКеш memcache - Ошибка сохранения\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
		except memcachedError as errorMsg:
			logg.log(logging.CRITICAL, "[{0}]\tКеш memcache недоступен\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
		try:
			caches["lastVisit"].set(userId, outTime)
		except ProgrammingError as errorMsg: # Нарушение в процессе транзакций
			logg.log(logging.CRITICAL, "[{0}]\tТранзакция базы данных\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
		except OperationalError as errMsg: # На уровне connect() драйвера
			logg.log(logging.CRITICAL, "[{0}]\tПодключение к базе данных\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
		thread = threading.main_thread()
		onlineStatusTimersDict = getattr(thread, "onlineTimers", None)
		activeStatusTimersDict = getattr(thread, "activeTimers", None)
		if onlineStatusTimersDict is not None:
			onlineTimer = onlineStatusTimersDict.pop(userId, None)
			onlineTimer.cancel() if onlineTimer is not None else None
			if len(onlineStatusTimersDict.keys()) == 0:
				delattr(thread, "onlineTimers")
		if activeStatusTimersDict is not None:
			activeTimer = activeStatusTimersDict.pop(userId, None)
			activeTimer.cancel() if activeTimer is not None else None
			if len(activeStatusTimersDict.keys()) == 0:
				delattr(thread, "activeTimers")
		async_to_sync(get_channel_layer().group_send)("%s_onl_stat" % userId, {
			"type": "sendToClient",
			"data": {
				"item": json.dumps({
					'method': 'stateTimer',
					'id': userId
				})
			}
		})
		disconnectCallback()
	else:
		try:
			isOnline = caches["onlineUsers"].get(userId, False)
		except ServerError as errorMsg:
			logg.log(logging.CRITICAL, "[{0}]\tКеш memcache - Ошибка сохранения\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
			sys.exit()
			return
		except memcachedError as errorMsg:
			logg.log(logging.CRITICAL, "[{0}]\tКеш memcache недоступен\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
			sys.exit()
			return
		if isOnline is False:
			try:
				caches["lastVisit"].set(userId, outTime)
			except ProgrammingError as errorMsg: # Нарушение в процессе транзакций
				logg.log(logging.CRITICAL, "[{0}]\tТранзакция базы данных\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
				sys.exit()
				return
			except OperationalError as errMsg: # На уровне connect() драйвера
				logg.log(logging.CRITICAL, "[{0}]\tПодключение к базе данных\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
				sys.exit()
				return
			thread = threading.main_thread()
			activeStatusTimersDict = getattr(thread, "activeTimers", None)
			if activeStatusTimersDict is not None:
				activeTimer = activeStatusTimersDict.pop(userId, None)
				activeTimer.cancel() if activeTimer is not None else None
				if len(activeStatusTimersDict.keys()) == 0:
					delattr(thread, "activeTimers")
			async_to_sync(get_channel_layer().group_send)("%s_onl_stat" % userId, {
				"type": "sendToClient",
				"data": {
					"item": json.dumps({
						'method': 'stateLeave',
						'time': str(outTime),
						'id': userId
					})
				}
			})
		try:
			sys.exit() # Для выхода потока "из Python"(удаления), это поток из threading.Timer
		except SystemExit:
			pass

def setMiddleStatus(id, threshhold):
	try:
		cacheMiddleStatus = caches["chatStatusMiddle"]
		middleUser = cacheMiddleStatus.get(id, False)
	except ServerError as errorMsg:
		logg.log(logging.CRITICAL, "[{0}]\tКеш memcache - Ошибка поиска и получения записи\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
		try:
			sys.exit()
		except SystemExit:
			pass
		return
	except memcachedError as errorMsg:
		logg.log(logging.CRITICAL, "[{0}]\tКеш memcache недоступен\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
		try:
			sys.exit()
		except SystemExit:
			pass
		return
	if middleUser is False:
		try:
			cacheMiddleStatus.set(id, "{0}|{1}".format(datetime.now(), threshhold), threshhold)
		except ServerError as errorMsg:
			logg.log(logging.CRITICAL, "[{0}]\tКеш memcache - Ошибка сохранения\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
			try:
				sys.exit()
			except SystemExit:
				pass
			return
		async_to_sync(get_channel_layer().group_send)("%s_act_stat" % id, {
			"type": "sendToClient", "data": {
				"item": json.dumps({
					'method': 'stateMiddle',
					'id': id,
					'totalTime': threshhold
					})
				}
			})
	try:
		sys.exit() # Для выхода потока "из Python"(удаления), это поток из threading.Timer
	except SystemExit:
		pass

class OnlineNowMiddleware(object):
	def __init__(self, get_response):
		self.get_response = get_response

	def __call__(self, request):
		if request.is_ajax() is False and request.method == "GET":
			requestUser = request.user
			if requestUser.is_authenticated:
				cookiesDict = request.COOKIES
				uid = str(requestUser.id)
				mainThread = threading.main_thread()
				if cookiesDict.get('ws', None) is not None:
					try:
						onlineStatusCache = caches["onlineUsers"]
						isOnline = onlineStatusCache.touch(uid, ONLINE_THRESHOLD_MAX)
					except ServerError as errorMsg:
						logg.log(logging.CRITICAL, "[{0}]\tКеш memcache - Ошибка сохранения\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
					except memcachedError as errorMsg:
						logg.log(logging.CRITICAL, "[{0}]\tКеш memcache недоступен\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
					threshholdChatStatus = ACTIVE_TIME_SHORT
					activeTimersDict = getattr(mainThread, "activeTimers", {})
					if isOnline:
						try:
							activeStatusCache = caches['chatStatusActive']
							isActive = activeStatusCache.touch(uid, threshholdChatStatus)
						except ServerError as errorMsg:
							logg.log(logging.CRITICAL, "[{0}]\tКеш memcache - Ошибка сохранения\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
						except memcachedError as errorMsg:
							logg.log(logging.CRITICAL, "[{0}]\tКеш memcache недоступен\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
						if isActive:
							lastActiveTimer = activeTimersDict.get(uid, None)
							lastActiveTimer.cancel() if lastActiveTimer is not None else None
						else:
							try:
								caches['chatStatusMiddle'].delete(uid)
								activeStatusCache.set(uid, True, threshholdChatStatus)
							except ServerError as errorMsg:
								logg.log(logging.CRITICAL, "[{0}]\tКеш memcache - Ошибка сохранения\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
							except memcachedError as errorMsg:
								logg.log(logging.CRITICAL, "[{0}]\tКеш memcache недоступен\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
							async_to_sync(get_channel_layer().group_send)("%s_act_stat" % uid, {
								"type": "sendToClient",
								"data": {
									"item": json.dumps({
										'method': 'stateActive',
										'id': uid
										})
									}
								})
						onlineTimer = getattr(mainThread, "onlineTimers", {}).get(uid, None)
						onlineTimer.cancel() if onlineTimer is not None else None
					else:
						try:
							iffyOnlineStatusCache = caches['iffyUsersOnlineStatus']
							isIffy = iffyOnlineStatusCache.get(uid, False)
							iffyOnlineStatusCache.delete(uid)
							onlineStatusCache.set(uid, ONLINE_THRESHOLD_MAX)
						except ServerError as errorMsg:
							logg.log(logging.CRITICAL, "[{0}]\tКеш memcache - Ошибка сохранения\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
						except memcachedError as errorMsg:
							logg.log(logging.CRITICAL, "[{0}]\tКеш memcache недоступен\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
						if isIffy:
							try:
								activeStatusCache = caches['chatStatusActive']
								isActive = activeStatusCache.touch(uid, threshholdChatStatus)
							except ServerError as errorMsg:
								logg.log(logging.CRITICAL, "[{0}]\tКеш memcache - Ошибка сохранения\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
							except memcachedError as errorMsg:
								logg.log(logging.CRITICAL, "[{0}]\tКеш memcache недоступен\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
							if isActive:
								lastActiveTimer = activeTimersDict.get(uid, None)
								lastActiveTimer.cancel() if lastActiveTimer is not None else None
							else:
								try:
									caches['chatStatusMiddle'].delete(uid)
									activeStatusCache.set(uid, True, threshholdChatStatus)
								except ServerError as errorMsg:
									logg.log(logging.CRITICAL, "[{0}]\tКеш memcache - Ошибка сохранения\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
								except memcachedError as errorMsg:
									logg.log(logging.CRITICAL, "[{0}]\tКеш memcache недоступен\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
								async_to_sync(get_channel_layer().group_send)("%s_act_stat" % uid, {
									"type": "sendToClient",
									"data": {
										"item": json.dumps({
											'method': 'stateActive',
											'id': uid
											})
										}
									})
							onlineTimer = getattr(mainThread, "onlineTimers", {}).get(uid, None)
							onlineTimer.cancel() if onlineTimer is not None else None
						async_to_sync(get_channel_layer().group_send)("%s_onl_stat" % uid, {
							"type": "sendToClient",
							"data": {
								"item": json.dumps({
									'method': 'stateOnline',
									'id': uid
									})
								}
							})
					activeTimer = threading.Timer(float(threshholdChatStatus) + 0.5, setMiddleStatus, [uid, ACTIVE_TIME_FULL])
					activeTimer.start()
					activeTimersDict.update({uid: activeTimer})
					setattr(mainThread, "activeTimers", activeTimersDict) if hasattr(mainThread, "activeTimers") is False else None
					return self.get_response(request)
				if cookiesDict.get("js", False):
					onlineThreshhold = ONLINE_THRESHOLD_SHORT
					activeThreshhold = ACTIVE_TIME_SHORT
				else:
					onlineThreshhold = ONLINE_THRESHOLD_FULL
					activeThreshhold = ACTIVE_TIME_FULL
				try:
					onlineStatusCache = caches["onlineUsers"]
					isOnline = onlineStatusCache.touch(uid, onlineThreshhold)
				except ServerError as errorMsg:
					logg.log(logging.CRITICAL, "[{0}]\tКеш memcache - Ошибка сохранения\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
				except memcachedError as errorMsg:
					logg.log(logging.CRITICAL, "[{0}]\tКеш memcache недоступен\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
				onlineTimersDict = getattr(mainThread, "onlineTimers", {})
				activeTimersDict = getattr(mainThread, "activeTimers", {})
				if isOnline:
					try:
						activeStatusCache = caches['chatStatusActive']
						isActive = activeStatusCache.touch(uid, activeThreshhold)
					except ServerError as errorMsg:
						logg.log(logging.CRITICAL, "[{0}]\tКеш memcache - Ошибка сохранения\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
					except memcachedError as errorMsg:
						logg.log(logging.CRITICAL, "[{0}]\tКеш memcache недоступен\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
					if isActive:
						lastActiveTimer = activeTimersDict.get(uid, None)
						lastActiveTimer.cancel() if lastActiveTimer is not None else None
					else:
						try:
							activeStatusCache.set(uid, True, activeThreshhold)
							caches['chatStatusMiddle'].delete(uid)
						except ServerError as errorMsg:
							logg.log(logging.CRITICAL, "[{0}]\tКеш memcache - Ошибка сохранения\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
						except memcachedError as errorMsg:
							logg.log(logging.CRITICAL, "[{0}]\tКеш memcache недоступен\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
						async_to_sync(get_channel_layer().group_send)("%s_act_stat" % uid, {
							"type": "sendToClient",
							"data": {
								"item": json.dumps({
									'method': 'stateActive',
									'id': uid
									})
								}
							})
					lastOnlineTimer = onlineTimersDict.get(uid, None)
					lastOnlineTimer.cancel() if lastOnlineTimer is not None else None
				else:
					try:
						iffyOnlineStatusCache = caches['iffyUsersOnlineStatus']
						isIffy = iffyOnlineStatusCache.get(uid, False)
						iffyOnlineStatusCache.delete(uid)
						onlineStatusCache.set(uid, True, onlineThreshhold)
					except ServerError as errorMsg:
						logg.log(logging.CRITICAL, "[{0}]\tКеш memcache - Ошибка сохранения\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
					except memcachedError as errorMsg:
						logg.log(logging.CRITICAL, "[{0}]\tКеш memcache недоступен\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
					if isIffy:
						try:
							activeStatusCache = caches['chatStatusActive']
							isActive = activeStatusCache.touch(uid, activeThreshhold)
						except ServerError as errorMsg:
							logg.log(logging.CRITICAL, "[{0}]\tКеш memcache - Ошибка сохранения\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
						except memcachedError as errorMsg:
							logg.log(logging.CRITICAL, "[{0}]\tКеш memcache недоступен\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
						if isActive:
							lastActiveTimer = activeTimersDict.get(uid, None)
							lastActiveTimer.cancel() if lastActiveTimer is not None else None
						else:
							caches['chatStatusMiddle'].delete(uid)
							activeStatusCache.set(uid, True, activeThreshhold)
							async_to_sync(get_channel_layer().group_send)("%s_act_stat" % uid, {
								"type": "sendToClient",
								"data": {
									"item": json.dumps({
											'method': 'stateActive',
											'id': uid
										})
									}
								})
						lastOnlineTimer = onlineTimersDict.get(uid, None)
						lastOnlineTimer.cancel() if lastOnlineTimer is not None else None
					async_to_sync(get_channel_layer().group_send)("%s_onl_stat" % uid, {
						"type": "sendToClient",
						"data": {
							"item": json.dumps({
									'method': 'stateOnline',
									'id': uid
								})
							}
						})
				onlineTimer = threading.Timer(float(onlineThreshhold) + 0.5, setUserLeftTime, [uid, datetime.now(), False, None])
				onlineTimer.start()
				onlineTimersDict.update({uid: onlineTimer})
				setattr(mainThread, "onlineTimers", onlineTimersDict) if hasattr(mainThread, "onlineTimers") is False else None
		return self.get_response(request)