#import pylibmc
import traceback, logging, json
from datetime import timedelta
from django.utils import timezone
from asgiref.sync import async_to_sync
from django.core.cache import caches
from django.db.transaction import atomic
from django.db import DatabaseError, InterfaceError, Error, OperationalError, DataError
from psixiSocial.models import dialogMessage, groupDialogAttributes, mainUserProfile
from django.db.models import Q
from channels.exceptions import DenyConnection, ChannelFull, MessageTooLarge, StopConsumer
from redis.exceptions import ConnectionError, RedisError

logg = logging.getLogger(__name__)

class groupMsg():

    def connectToGroupDialogs(self):
        userId = self.userId
        groupDialogsIdsCache = caches["groupDialogsChannelsLayers"]
        #try:
        groupDialogsIdList = groupDialogsIdsCache.get(userId, None)
        #except pylibmc.Error:
            #logg.log(logging.CRITITCAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
        if groupDialogsIdList is None:
            groupDialogsInstance = dialogMessage.objects.filter(Q(users__exact = userId) & Q(type__exact = False))
            if groupDialogsInstance.count() > 0:
                groupDialogsAttrsQuerySet = groupDialogAttributes.objects.filter(dialog__in = groupDialogsInstance).filter(lastUpdate__gte = timezone.now() - timedelta(days = 10)).order_by("-lastUpdate")[:35]
                if groupDialogsAttrsQuerySet.count() > 0:
                    channel = self.channel_name
                    groupDialogsIdList = []
                    groupDialogsAttrsQuerySetValues = groupDialogsAttrsQuerySet.values("dialog_id")
                    for Qset in groupDialogsAttrsQuerySetValues:
                        dialogId = str(Qset.get("dialog_id"))
                        groupDialogsIdList.append(dialogId)
                        try:
                            async_to_sync(self.channel_layer.group_add)(dialogId, channel)
                        except DenyConnection as er:
                            logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
                        except ChannelFull as er:
                            logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
                        except StopConsumer as er:
                            logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
                        except ConnectionError as er:
                            logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
                        except RedisError as er:
                            logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
                    dialogListenersMany = groupDialogsIdsCache.get_many(groupDialogsIdList)
                    isnotExistDialogListeners = frozenset(groupDialogsIdList) - frozenset(dialogListenersMany.keys())
                    u = {userId: {self.channel_name,}}
                    for channelsDictSet in dialogListenersMany.values():
                        channelsSet = channelsDictSet.get(userId, None)
                        if channelsSet is None:
                            channelsDictSet.update({userId: u})
                        else:
                            channelsSet.add(channel)
                    if len(isnotExistDialogListeners) > 0:
                        for dId in isnotExistDialogListeners:
                            dialogListenersMany.update({dId: u})
                    #try:
                    groupDialogsIdsCache.set(userId, groupDialogsIdList, 604800)
                    groupDialogsIdsCache.set_many(dialogListenersMany, 86400)
                    #except pylibmc.Error:
                        #logg.log(logging.CRITITCAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
                    return groupDialogsIdList
                else:
                    groupDialogsIdsCache.set(userId, False, 604800)
            else:
                groupDialogsIdsCache.set(userId, False, 604800)
        else:
            if groupDialogsIdList is not False:
                channel = self.channel_name
                for dialogId in groupDialogsIdList:
                    try:
                        async_to_sync(self.channel_layer.group_add)(dialogId, channel)
                    except DenyConnection as er:
                        logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
                    except ChannelFull as er:
                        logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
                    except StopConsumer as er:
                        logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
                    except ConnectionError as er:
                        logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
                    except RedisError as er:
                        logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
                dialogListenersMany = groupDialogsIdsCache.get_many(groupDialogsIdList)
                isnotExistDialogListeners = frozenset(groupDialogsIdList) - frozenset(dialogListenersMany.keys())
                u = {userId: {self.channel_name,}}
                for channelsDictSet in dialogListenersMany.values():
                    channelsSet = channelsDictSet.get(userId, None)
                    if channelsSet is None:
                        channelsDictSet.update({userId: u})
                    else:
                        channelsSet.add(channel)
                if len(isnotExistDialogListeners) > 0:
                    for dId in isnotExistDialogListeners:
                        dialogListenersMany.update({dId: u})
                #try:
                groupDialogsIdsCache.set_many(dialogListenersMany, 86400)
                #except pylibmc.Error:
                    #logg.log(logging.CRITITCAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
                return groupDialogsIdList

    def sendGroupMessage(self, data):
        try:
            self.send(text_data = json.dumps(data['data']))
        except MessageTooLarge as err:
            logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
            return


    def disconnectFromGroupDialogs(self):
        if hasattr(self, "groupDialogs"):
            channel = self.channel_name
            for id in self.groups:
                async_to_sync(self.channel_layer.group_discard)(id, channel)
            groupDialogsIdsCache = caches["groupDialogsChannelsLayers"]
            myId = self.userId
            myGroupDialogs = groupDialogsIdsCache.get(myId, None)
            if myGroupDialogs is not None:
                groupDialogUsersDictSet = groupDialogsIdsCache.get_many(myGroupDialogs)
                changes = False
                for dialogId, channelsDictSet in dict(groupDialogUsersDictSet).items(): # использовал dict() для создания поверхностной копии
                    channelsDictKeys = channelsDictSet.keys()
                    if myId in channelsDictKeys:
                        del channelsDictSet[myId]
                        if len(channelsDictKeys) == 0:
                            del groupDialogUsersDictSet[dialogId]
                            if changes is False:
                                changes = True
                if changes is True:
                    groupDialogsIdsCache.set_many(groupDialogUsersDictSet)

    def readGroupMessages(self, data):
        dialogId = data.pop("dialogId", None)
        if dialogId is not None:
            messageIdList = data.pop("readedMessagesIds", None)
            if messageIdList is not None and 50 < len(messageIdList) > 0:
                try:
                    unreadGroupMessagesCache = caches['unreadGroupMessages']
                except InterfaceError as dbErr:
                    logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(dbErr), traceback.format_exc()))
                    return
                try:
                    unreadGroupMessagesDictSet = unreadGroupMessagesCache.get(dialogId, None)
                except DatabaseError as dbErr:
                    logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(dbErr), traceback.format_exc()))
                    return
                if unreadGroupMessagesDictSet is not None:
                    userId = self.userId
                    readedMessages = []
                    for messageId, unreadUsersIdSet in unreadGroupMessagesDictSet.items():
                        for readMessageId in messageIdList:
                            if messageId == readMessageId:
                                if userId in unreadUsersIdSet:
                                    if len(unreadUsersIdSet) > 1:
                                        unreadUsersIdSet.remove(userId)
                                    else:
                                        del unreadGroupMessagesDictSet[messageId]
                                    readedMessages.append(messageId)
                                messageIdList.remove(messageId)
                                break
                    if len(unreadGroupMessagesDictSet.keys()) > 0:
                        if len(readedMessages) > 0:
                            # Проверим остался ли где-то в непрочтённых сообщениях мой ID
                            findOtherUnreadMessages = False
                            for unreadUsersIdSet in unreadGroupMessagesDictSet.values():
                                if userId in unreadUsersIdSet:
                                    findOtherUnreadMessages = True
                                    break
                            if findOtherUnreadMessages is True:
                                try:
                                    unreadGroupMessagesCache.set(dialogId, unreadGroupMessagesDictSet)
                                except DataError as dbErr:
                                    logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(dbErr), traceback.format_exc()))
                                    return
                            else:
                                try:
                                    updatesCache = caches["newUpdates"]
                                except InterfaceError as dbErr:
                                    logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(dbErr), traceback.format_exc()))
                                    return
                                try:
                                    myUpdatesListTuple = updatesCache.get(userId, None)
                                except DatabaseError as dbErr:
                                    logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(dbErr), traceback.format_exc()))
                                    return
                                if myUpdatesListTuple is not None:
                                    find = False
                                    for updateSet in myUpdatesListTuple:
                                        if updateSet[0] == "groupMessage" and updateSet[3] == dialogId:
                                            myUpdatesListTuple.remove(myUpdatesListTuple.index(updateSet))
                                            find = True
                                            break
                                    if find is True:
                                        if len(myUpdatesListTuple) > 0:
                                            try:
                                                with atomic():
                                                    updatesCache.set(userId, myUpdatesListTuple)
                                                    unreadGroupMessagesCache.set(dialogId, unreadGroupMessagesDictSet)
                                            except DataError as dbErr:
                                                logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(dbErr), traceback.format_exc()))
                                                return
                                        else:
                                            try:
                                                with atomic():
                                                    updateCache.delete(userId)
                                                    unreadGroupMessagesCache.set(dialogId, unreadGroupMessagesDictSet)
                                            except DataError as dbErr:
                                                logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(dbErr), traceback.format_exc()))
                                                return
                                    else:
                                        try:
                                            unreadGroupMessagesCache.set(dialogId, unreadGroupMessagesDictSet)
                                        except DataError as dbErr:
                                            logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(dbErr), traceback.format_exc()))
                                            return
                                else:
                                    try:
                                        unreadGroupMessagesCache.set(dialogId, unreadGroupMessagesDictSet)
                                    except DataError as dbErr:
                                        logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(dbErr), traceback.format_exc()))
                                        return
                            try:
                                groupDialogListeners = caches['groupDialogsChannelsLayers'].get(dialogId)
                            except:
                                pass
                        
                    else:
                        try:
                            updatesCache = caches["newUpdates"]
                        except InterfaceError as dbErr:
                            logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(dbErr), traceback.format_exc()))
                            return
                        try:
                            myUpdatesListTuple = updatesCache.get(userId, None)
                        except DatabaseError as dbErr:
                            logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(dbErr), traceback.format_exc()))
                            return
                        if myUpdatesListTuple is not None:
                            find = False
                            for updateSet in myUpdatesListTuple:
                                if updateSet[0] == "groupMessage" and updateSet[3] == dialogId:
                                    myUpdatesListTuple.remove(myUpdatesListTuple.index(updateSet))
                                    find = True
                                    break
                            if find is True:
                                if len(myUpdatesListTuple) > 0:
                                    try:
                                        with atomic():
                                            unreadGroupMessagesCache.delete(dialogId)
                                            updatesCache.set(userId, myUpdatesListTuple)
                                    except DataError as dbErr:
                                        logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(dbErr), traceback.format_exc()))
                                        return
                                else:
                                    try:
                                        with atomic():
                                            unreadGroupMessagesCache.delete(dialogId)
                                            updatesCache.delete(userId)
                                    except DataError as dbErr:
                                        logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(dbErr), traceback.format_exc()))
                                        return
                            else:
                                try:
                                    unreadGroupMessagesCache.delete(dialogId)
                                except DataError as dbErr:
                                    logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(dbErr), traceback.format_exc()))
                                    return
                        else:
                            try:
                                unreadGroupMessagesCache.delete(dialogId)
                            except DataError as dbErr:
                                logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(dbErr), traceback.format_exc()))
                                return
                        