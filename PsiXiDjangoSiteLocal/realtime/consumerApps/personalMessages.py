import json
from django.core.cache import caches
from django.db.transaction import atomic, Error
from asgiref.sync import async_to_sync

class personalMsg():
    def readPersonalMessage(self, data):
        dialogId = data.pop("dialogId", None)
        if dialogId is not None:
            senderIdStr = data.pop("sender", None)
            if senderIdStr is not None:
                messageIdsList = data.pop("readedMessagesIds", None)
                if messageIdsList is not None and 50 > len(messageIdsList) > 0:
                    unreadPersonalMessagesCache = caches["unreadPersonalMessages"]
                    unreadPersonalMessagesDict = unreadPersonalMessagesCache.get(dialogId, None)
                    if unreadPersonalMessagesDict is not None:
                        userIdStr = self.userId
                        unreadPersonalMessagesSet = unreadPersonalMessagesDict.get(userIdStr, None)
                        if unreadPersonalMessagesSet is not None:
                            messageIdsSet = frozenset(messageIdsList)
                            messageIdsToSend = unreadPersonalMessagesSet & messageIdsSet
                            if len(messageIdsToSend) > 0:
                                messageIdsToSet = unreadPersonalMessagesSet - messageIdsSet
                                messageIdsToSendList = list(messageIdsToSend)
                                save = True
                                messageIdsToSetLen = len(messageIdsToSet)
                                if messageIdsToSetLen > 0:
                                    unreadPersonalMessagesDict[userIdStr] = messageIdsToSet
                                    updatesCache = caches["newUpdates"]
                                    updatesListTuple = updatesCache.get(userIdStr, None)
                                    if updatesListTuple is not None and len(updatesListTuple) > 0:
                                        saveUpdatesCache = False
                                        changeNotifyCounter = True
                                        for updateTuple in updatesListTuple:
                                            if updateTuple[0] == "personalMessage" and updateTuple[3] == dialogId:
                                                if updateTuple[1] == "hidden":
                                                    changeNotifyCounter = False
                                                del updatesListTuple[updatesListTuple.index(updateTuple)]
                                                saveUpdatesCache = True
                                                break
                                        if saveUpdatesCache is True:
                                            if len(updatesListTuple) > 0:
                                                try:
                                                    with atomic():
                                                        unreadPersonalMessagesCache.set(dialogId, unreadPersonalMessagesDict)
                                                        updatesCache.set(userIdStr, updatesListTuple)
                                                except Error:
                                                    save = False
                                                if save is True:
                                                    messagesReadDataObj = {
                                                        'messages': messageIdsToSendList,
                                                        'dialogId': dialogId
                                                        }
                                                    if changeNotifyCounter is True:
                                                        async_to_sync(self.channel_layer.group_send)(userIdStr, {"type": "sendToClient", "data": {"item": json.dumps({"personalMessageRead": [{
                                                            'method': 'decrementNotifyValue',
                                                            'originalMethod': 'messagePersonal',
                                                            'value': 'dialogMessage-new-%s' % senderIdStr,
                                                            'valuesOfItemsToRemoveId': messageIdsToSendList,
                                                            'reduceBaseValueBy': messageIdsToSetLen
                                                            }, {
                                                                "personalMessageRead": messagesReadDataObj
                                                                }]})}})
                                                    else:
                                                        async_to_sync(self.channel_layer.group_send)(userIdStr, {"type": "sendToClient", "data": {"item": json.dumps({"personalMessageRead": messagesReadDataObj})}})
                                                    async_to_sync(self.channel_layer.group_send)(senderIdStr, {"type": "sendToClient", "data": {"item": json.dumps({"personalMessageRead": messagesReadDataObj})}})
                                            else:
                                                try:
                                                    with atomic():
                                                        unreadPersonalMessagesCache.set(dialogId, unreadPersonalMessagesDict)
                                                        updatesCache.delete(userIdStr)
                                                except Error:
                                                    save = False
                                                if save is True:
                                                    messagesReadDataObj = {
                                                        'messages': messageIdsToSendList,
                                                        'dialogId': dialogId
                                                        }
                                                    if changeNotifyCounter is True:
                                                        async_to_sync(self.channel_layer.group_send)(userIdStr, {"type": "sendToClient", "data": {"item": json.dumps(({
                                                            'method': 'decrementNotifyValue',
                                                            'originalMethod': 'messagePersonal',
                                                            'value': 'dialogMessage-new-%s' % senderIdStr,
                                                            'valuesOfItemsToRemoveId': messageIdsToSendList,
                                                            'reduceBaseValueBy': messageIdsToSetLen
                                                            }, {
                                                                "personalMessageRead": messagesReadDataObj
                                                                }))}})
                                                    else:
                                                        async_to_sync(self.channel_layer.group_send)(userIdStr, {"type": "sendToClient", "data": {"item": json.dumps({"personalMessageRead": messagesReadDataObj})}})
                                                    async_to_sync(self.channel_layer.group_send)(senderIdStr, {"type": "sendToClient", "data": {"item": json.dumps({"personalMessageRead": messagesReadDataObj})}})
                                        else:
                                            try:
                                                unreadPersonalMessagesCache.set(dialogId, unreadPersonalMessagesDict)
                                            except Error:
                                                save = False
                                            if save is True:
                                                messagesReadDataStr = json.dumps({
                                                "personalMessageRead": {
                                                    'messages': messageIdsToSendList,
                                                    'dialogId': dialogId
                                                    }
                                                })
                                                async_to_sync(self.channel_layer.group_send)(userIdStr, {"type": "sendToClient", "data": {"item": messagesReadDataStr}})
                                                async_to_sync(self.channel_layer.group_send)(senderIdStr, {"type": "sendToClient", "data": {"item": messagesReadDataStr}})
                                    else:
                                        try:
                                            unreadPersonalMessagesCache.set(dialogId, unreadPersonalMessagesDict)
                                        except Error:
                                            save = False
                                        if save is True:
                                            messagesReadDataStr = json.dumps({
                                                "personalMessageRead": {
                                                    'messages': messageIdsToSendList,
                                                    'dialogId': dialogId
                                                    }
                                                })
                                            async_to_sync(self.channel_layer.group_send)(userIdStr, {"type": "sendToClient", "data": {"item": messagesReadDataStr}})
                                            async_to_sync(self.channel_layer.group_send)(senderIdStr, {"type": "sendToClient", "data": {"item": messagesReadDataStr}})
                                else:
                                    del unreadPersonalMessagesDict[userIdStr]
                                    updatesCache = caches["newUpdates"]
                                    updatesListTuple = updatesCache.get(userIdStr, None)
                                    if updatesListTuple is not None and len(updatesListTuple) > 0:
                                        saveUpdatesCache = False
                                        changeNotifyCounter = True
                                        for updateTuple in updatesListTuple:
                                            if updateTuple[0] == "personalMessage" and updateTuple[3] == dialogId:
                                                if updateTuple[1] == "hidden":
                                                    changeNotifyCounter = False
                                                del updatesListTuple[updatesListTuple.index(updateTuple)]
                                                saveUpdatesCache = True
                                                break
                                        if saveUpdatesCache is True:
                                            if len(updatesListTuple) > 0:
                                                if len(unreadPersonalMessagesDict.keys()) > 0:
                                                    try:
                                                        with atomic():
                                                            unreadPersonalMessagesCache.set(dialogId, unreadPersonalMessagesDict)
                                                            updatesCache.set(userIdStr, updatesListTuple)
                                                    except Error:
                                                        save = False
                                                else:
                                                    try:
                                                        with atomic():
                                                            unreadPersonalMessagesCache.delete(dialogId)
                                                            updatesCache.set(userIdStr, updatesListTuple)
                                                    except Error:
                                                        save = False
                                                if save is True:
                                                    messagesReadDataObj = {
                                                        'messages': messageIdsToSendList,
                                                        'dialogId': dialogId
                                                        }
                                                    if changeNotifyCounter is True:
                                                        async_to_sync(self.channel_layer.group_send)(userIdStr, {"type": "sendToClient", "data": {"item": json.dumps(({
                                                                'method': 'decrementNotifyValue',
                                                                'originalMethod': 'messagePersonal',
                                                                'value': 'dialogMessage-new-%s' % senderIdStr,
                                                                'valuesOfItemsToRemoveId': messageIdsToSendList,
                                                                'reduceBaseValueBy': messageIdsToSetLen
                                                                }, {
                                                                    "personalMessageRead": messagesReadDataObj
                                                                    }))}})
                                                    else:
                                                        async_to_sync(self.channel_layer.group_send)(userIdStr, {"type": "sendToClient", "data": {"item": json.dumps({"personalMessageRead": messagesReadDataObj})}})
                                                    async_to_sync(self.channel_layer.group_send)(senderIdStr, {"type": "sendToClient", "data": {"item": json.dumps({"personalMessageRead": messagesReadDataObj})}})
                                            else:
                                                if len(unreadPersonalMessagesDict.keys()) > 0:
                                                    try:
                                                        with atomic():
                                                            unreadPersonalMessagesCache.set(dialogId, unreadPersonalMessagesDict)
                                                            updatesCache.delete(userIdStr)
                                                    except Error:
                                                        save = False
                                                else:
                                                    try:
                                                        with atomic():
                                                            unreadPersonalMessagesCache.delete(dialogId)
                                                            updatesCache.delete(userIdStr)
                                                    except Error:
                                                        save = False
                                                if save is True:
                                                    messagesReadDataObj = {
                                                        'messages': messageIdsToSendList,
                                                        'dialogId': dialogId
                                                        }
                                                    if changeNotifyCounter is True:
                                                        async_to_sync(self.channel_layer.group_send)(userIdStr, {"type": "sendToClient", "data": {"item": json.dumps(({
                                                                'method': 'decrementNotifyValue',
                                                                'originalMethod': 'messagePersonal',
                                                                'value': 'dialogMessage-new-%s' % senderIdStr,
                                                                'valuesOfItemsToRemoveId': messageIdsToSendList,
                                                                'reduceBaseValueBy': messageIdsToSetLen
                                                                }, {
                                                                    "personalMessageRead": messagesReadDataObj
                                                                    }))}})
                                                    else:
                                                        async_to_sync(self.channel_layer.group_send)(userIdStr, {"type": "sendToClient", "data": {"item": json.dumps({"personalMessageRead": messagesReadDataObj})}})
                                                    async_to_sync(self.channel_layer.group_send)(senderIdStr, {"type": "sendToClient", "data": {"item": json.dumps({"personalMessageRead": messagesReadDataObj})}})
                                        else:
                                            if len(unreadPersonalMessagesDict.keys()) > 0:
                                                try:
                                                    unreadPersonalMessagesCache.set(dialogId, unreadPersonalMessagesDict)
                                                except Error:
                                                    save = False
                                            else:
                                                try:
                                                    unreadPersonalMessagesCache.delete(dialogId)
                                                except Error:
                                                    save = False
                                            if save is True:
                                                messagesReadDataStr = json.dumps({
                                                "personalMessageRead": {
                                                    'messages': messageIdsToSendList,
                                                    'dialogId': dialogId
                                                    }
                                                })
                                                async_to_sync(self.channel_layer.group_send)(userIdStr, {"type": "sendToClient", "data": {"item": messagesReadDataStr}})
                                                async_to_sync(self.channel_layer.group_send)(senderIdStr, {"type": "sendToClient", "data": {"item": messagesReadDataStr}})
                                    else:
                                        if len(unreadPersonalMessagesDict.keys()) > 0:
                                            try:
                                                unreadPersonalMessagesCache.set(dialogId, unreadPersonalMessagesDict)
                                            except Error:
                                                save = False
                                        else:
                                            try:
                                                unreadPersonalMessagesCache.delete(dialogId)
                                            except Error:
                                                save = False
                                        if save is True:
                                            messagesReadDataStr = json.dumps({
                                                "personalMessageRead": {
                                                    'messages': messageIdsToSendList,
                                                    'dialogId': dialogId
                                                    }
                                                })
                                            async_to_sync(self.channel_layer.group_send)(userIdStr, {"type": "sendToClient", "data": {"item": messagesReadDataStr}})
                                            self.send(text_data = messagesReadDataStr)
                                            async_to_sync(self.channel_layer.group_send)(senderIdStr, {"type": "sendToClient", "data": {"item": messagesReadDataStr}})
