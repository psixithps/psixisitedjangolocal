import json, traceback, logging, copy, datetime
from threading import Timer
from django.conf import settings
from psixiSocial.models import userProfilePrivacy
from django.core.cache import caches
from psixiSocial.privacyIdentifier import checkPrivacy
from asgiref.sync import async_to_sync
from channels.exceptions import DenyConnection, AcceptConnection, ChannelFull, MessageTooLarge, StopConsumer
from redis.exceptions import ConnectionError, RedisError

logg = logging.getLogger(__name__)

ONLINE_THRESHOLD_MAX = getattr(settings, 'ONLINE_THRESHOLD_FULL', 1200) # Вебсокет

class status():

	def startListernStatus(self, usersData):
		usersList = usersData.get("users", None)
		if isinstance(usersList, list):
			usersListLen = len(usersList)
			if 300 > usersListLen > 0:
				usersSet = set(usersList)
				for item in usersSet:
					if isinstance(item, int) is True or item.isdigit() is False or item.startswith("0") is True:
						return
				myId = self.userId
				usersSet.discard(myId)
				statusChannelsToGetOnlineSet = set()
				statusChannelsToGetActiveSet = set()
				privacyCache = caches['privacyCache']
				privacyDictMany = privacyCache.get_many(usersSet)
				privacyDictManySet = set(privacyDictMany.keys())
				usersToGetPrivacyIdsSet = usersSet - privacyDictManySet
				usersToSearchPrivacyKeysSet = privacyDictManySet - usersToGetPrivacyIdsSet
				if len(usersToSearchPrivacyKeysSet) > 0:
					for userId in usersToSearchPrivacyKeysSet:
						privacyDict = privacyDictMany[userId]
						if myId not in privacyDict.keys():
							usersToGetPrivacyIdsSet.add(userId)
						else:
							privacyDictMy = privacyDict[myId].keys()
							if 'viewOnlineStatus' not in privacyDictMy or 'viewActiveStatus' not in privacyDictMy:
								usersToGetPrivacyIdsSet.add(userId)
				privacyToSetDict = None
				if len(usersToGetPrivacyIdsSet) > 0:
					friendShipStatusToSetDict = None
					user = self.user
					isAuth = user.is_authenticated
					privacyInstance = userProfilePrivacy.objects.filter(user_id__in = usersToGetPrivacyIdsSet)
					if privacyInstance.count() < usersListLen:
						logg.log(logging.WARNING, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), "Несоответствие входящих параметров", traceback.format_exc()))
						usersToGetPrivacyIdsSet = set(str(id.get("user_id")) for id in privacyInstance.values("user_id"))
						if len(usersToGetPrivacyIdsSet) == 0:
							return
						usersSet = usersToGetPrivacyIdsSet
					friendShipStatusCache = caches["friendShipStatusCache"]
					friendShipStatusDict = friendShipStatusCache.get(myId, None)
					if friendShipStatusDict is None:
						friendsIdsCache = caches["friendsIdCache"]
						friendsIds = friendsIdsCache.get(myId, None)
						if friendsIds is None:
							friendsCache = caches["friendsCache"]
							friends = friendsCache.get(myId, None)
							if friends is None:
								friends = user.friends.all()
								if friends.count() == 0:
									friendsIds = set()
									friendsCache.set(myId, False)
								else:
									friendsIds = {str(friend.get("id")) for friend in friends.values("id")}
									friendsCache.set(myId, friends)
							else:
								if friends is False:
									friendsIds = set()
								else:
									friendsIds = {str(friend.get("id")) for friend in friends.values("id")}
							friendsIdsCache.set(myId, friendsIds)
						friendShipRequestsIdsSet = usersToGetPrivacyIdsSet - friendsIds
						if len(friendShipRequestsIdsSet) > 0:
							friendShipRequestsCache = caches["friendshipRequests"]
							if usersListLen >= 30:
								friendShipRequestsMyMany = friendShipRequestsCache.get(myId, None)
								if friendShipRequestsMyMany is not None:
									friendShipRequestsIdsToSearch = friendShipRequestsIdsSet - friendShipRequestsMyMany.keys()
									if len(friendShipRequestsIdsToSearch) > 0:
										friendShipRequestsMany = friendShipRequestsCache.get_many(friendShipRequestsIdsToSearch)
								else:
									friendShipRequestsCache.get_many(friendShipRequestsIdsSet)
							else:
								friendShipRequestsMyMany = None
								friendShipRequestsIdsSet.add(myId)
								friendShipRequestsMany = friendShipRequestsCache.get_many(friendShipRequestsIdsSet)
						friendShipStatusToSetDict = {}
					else:
						friendShipStatusIdsSet = usersToGetPrivacyIdsSet - friendShipStatusDict.keys()
						if len(friendShipStatusIdsSet) > 0:
							friendsIdsCache = caches["friendsIdCache"]
							friendsIds = friendsIdsCache.get(myId, None)
							if friendsIds is None:
								friendsCache = caches["friendsCache"]
								friends = friendsCache.get(myId, None)
								if friends is None:
									friends = user.friends.all()
									if friends.count() == 0:
										friendsIds = set()
										friendsCache.set(myId, False)
									else:
										friendsIds = {str(friend.get("id")) for friend in friends.values("id")}
										friendsCache.set(myId, friends)
								else:
									if friends is False:
										friendsIds = set()
									else:
										friendsIds = {str(friend.get("id")) for friend in friends.values("id")}
								friendsIdsCache.set(myId, friendsIds)
							friendShipRequestsIdsSet = friendShipStatusIdsSet - friendsIds
							if len(friendShipRequestsIdsSet) > 0:
								friendShipRequestsCache = caches["friendshipRequests"]
								if usersListLen >= 30:
									friendShipRequestsMyMany = friendShipRequestsCache.get(myId, None)
									if friendShipRequestsMyMany is not None:
										friendShipRequestsIdsToSearch = friendShipRequestsIdsSet - set(friendShipRequestsMyMany.keys())
										if len(friendShipRequestsIdsToSearch) > 0:
											friendShipRequestsMany = friendShipRequestsCache.get_many(friendShipRequestsIdsToSearch)
									else:
										friendShipRequestsCache.get_many(friendShipRequestsIdsSet)
								else:
									friendShipRequestsMyMany = None
									friendShipRequestsIdsSet.add(myId)
									friendShipRequestsMany = friendShipRequestsCache.get_many(friendShipRequestsIdsSet)
							friendShipStatusToSetDict = {}
					privacyToSetDict = {}
				for id in usersSet:
					privacyDict = privacyDictMany.get(id, None)
					if privacyDict is None:
						if friendShipStatusDict is None:
							if id in friendsIds:
								friendShipStatus = True
								friendsIds.remove(id)
							else:
								friendShipStatus = False
								if friendShipRequestsMyMany is None:
									friendShipRequestsMy = friendShipRequestsMany.get(myId, None)
									if friendShipRequestsMy is not None:
										for requestList in friendShipRequestsMy:
											if requestList[0] == id:
												friendShipStatus = "request"
												break
										if friendShipStatus is False:
											friendShipRequests = friendShipRequestsMany.get(id, None)
											if friendShipRequests is not None:
												for requestList in friendShipRequests:
													if requestList[0] == myId:
														friendShipStatus = "myRequest"
														break
									else:
										friendShipRequests = friendShipRequestsMany.get(id, None)
										if friendShipRequests is not None:
											for requestList in friendShipRequests:
												if requestList[0] == myId:
													friendShipStatus = "myRequest"
													break
								else:
									for requestList in friendShipRequestsMyMany:
										if requestList[0] == id:
											friendShipStatus = "request"
											break
									if friendShipStatus is False:
										friendShipRequests = friendShipRequestsMany.get(id, None)
										if friendShipRequests is not None:
											for requestList in friendShipRequests:
												if requestList[0] == myId:
													friendShipStatus = "myRequest"
													break
								friendShipStatusToSetDict.update({id: friendShipStatus})
						else:
							friendShipStatus = friendShipStatusDict.get(id, False)
							if friendShipStatus is False:
								if id in friendsIds:
									friendShipStatus = True
									friendsIds.remove(id)
								else:
									if friendShipRequestsMyMany is None:
										friendShipRequestsMy = friendShipRequestsMany.get(myId, None)
										if friendShipRequestsMy is not None:
											for requestList in friendShipRequestsMy:
												if requestList[0] == id:
													friendShipStatus = "request"
													break
											if friendShipStatus is False:
												friendShipRequests = friendShipRequestsMany.get(id, None)
												if friendShipRequests is not None:
													for requestList in friendShipRequests:
														if requestList[0] == myId:
															friendShipStatus = "myRequest"
															break
										else:
											friendShipRequests = friendShipRequestsMany.get(id, None)
											if friendShipRequests is not None:
												for requestList in friendShipRequests:
													if requestList[0] == myId:
														friendShipStatus = "myRequest"
														break
									else:
										for requestList in friendShipRequestsMyMany:
											if requestList[0] == id:
												friendShipStatus = "request"
												break
										if friendShipStatus is False:
											friendShipRequests = friendShipRequestsMany.get(id, None)
											if friendShipRequests is not None:
												for requestList in friendShipRequests:
													if requestList[0] == myId:
														friendShipStatus = "myRequest"
														break
								friendShipStatusToSetDict.update({id: friendShipStatus})
						privacyChecker = checkPrivacy(privacy = privacyInstance.get(user_id__exact = id), isAuthenticated = isAuth, isFriend = friendShipStatus is True or friendShipStatus == "request")
						showOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
						showActiveStatus = privacyChecker.checkActiveStatusPrivacy()
						privacyToSetDict.update({id: {myId: {'viewOnlineStatus': showOnlineStatus, 'viewActiveStatus': showActiveStatus}}})
					else:
						privacyDictMy = privacyDict.get(myId, None)
						if privacyDictMy is None:
							if friendShipStatusDict is None:
								if id in friendsIds:
									friendShipStatus = True
									friendsIds.remove(id)
								else:
									friendShipStatus = False
									if friendShipRequestsMyMany is None:
										friendShipRequestsMy = friendShipRequestsMany.get(myId, None)
										if friendShipRequestsMy is not None:
											for requestList in friendShipRequestsMy:
												if requestList[0] == id:
													friendShipStatus = "request"
													break
											if friendShipStatus is False:
												friendShipRequests = friendShipRequestsMany.get(id, None)
												if friendShipRequests is not None:
													for requestList in friendShipRequests:
														if requestList[0] == myId:
															friendShipStatus = "myRequest"
															break
										else:
											friendShipRequests = friendShipRequestsMany.get(id, None)
											if friendShipRequests is not None:
												for requestList in friendShipRequests:
													if requestList[0] == myId:
														friendShipStatus = "myRequest"
														break
									else:
										for requestList in friendShipRequestsMyMany:
											if requestList[0] == id:
												friendShipStatus = "request"
												break
										if friendShipStatus is False:
											friendShipRequests = friendShipRequestsMany.get(id, None)
											if friendShipRequests is not None:
												for requestList in friendShipRequests:
													if requestList[0] == myId:
														friendShipStatus = "myRequest"
														break
									friendShipStatusToSetDict.update({id: friendShipStatus})
							else:
								friendShipStatus = friendShipStatusDict.get(id, False)
								if friendShipStatus is False:
									if id in friendsIds:
										friendShipStatus = True
										friendsIds.remove(id)
									else:
										if friendShipRequestsMyMany is None:
											friendShipRequestsMy = friendShipRequestsMany.get(myId, None)
											if friendShipRequestsMy is not None:
												for requestList in friendShipRequestsMy:
													if requestList[0] == id:
														friendShipStatus = "request"
														break
												if friendShipStatus is False:
													friendShipRequests = friendShipRequestsMany.get(id, None)
													if friendShipRequests is not None:
														for requestList in friendShipRequests:
															if requestList[0] == myId:
																friendShipStatus = "myRequest"
																break
											else:
												friendShipRequests = friendShipRequestsMany.get(id, None)
												if friendShipRequests is not None:
													for requestList in friendShipRequests:
														if requestList[0] == myId:
															friendShipStatus = "myRequest"
															break
										else:
											for requestList in friendShipRequestsMyMany:
												if requestList[0] == id:
													friendShipStatus = "request"
													break
											if friendShipStatus is False:
												friendShipRequests = friendShipRequestsMany.get(id, None)
												if friendShipRequests is not None:
													for requestList in friendShipRequests:
														if requestList[0] == myId:
															friendShipStatus = "myRequest"
															break
									friendShipStatusToSetDict.update({id: friendShipStatus})
							privacyChecker = checkPrivacy(privacy = privacyInstance.get(user_id__exact = id), isAuthenticated = isAuth, isFriend = friendShipStatus is True or friendShipStatus == "request")
							showOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
							showActiveStatus = privacyChecker.checkActiveStatusPrivacy()
							privacyDict.update({myId: {'viewOnlineStatus': showOnlineStatus, 'viewActiveStatus': showActiveStatus}})
							privacyToSetDict.update({id: privacyDict})
						else:
							showOnlineStatus = privacyDictMy.get('viewOnlineStatus', None)
							showActiveStatus = privacyDictMy.get('viewActiveStatus', None)
							if showOnlineStatus is None or showActiveStatus is None:
								if friendShipStatusDict is None:
									if id in friendsIds:
										friendShipStatus = True
										friendsIds.remove(id)
									else:
										friendShipStatus = False
										if friendShipRequestsMyMany is None:
											friendShipRequestsMy = friendShipRequestsMany.get(myId, None)
											if friendShipRequestsMy is not None:
												for requestList in friendShipRequestsMy:
													if requestList[0] == id:
														friendShipStatus = "request"
														break
												if friendShipStatus is False:
													friendShipRequests = friendShipRequestsMany.get(id, None)
													if friendShipRequests is not None:
														for requestList in friendShipRequests:
															if requestList[0] == myId:
																friendShipStatus = "myRequest"
																break
											else:
												friendShipRequests = friendShipRequestsMany.get(id, None)
												if friendShipRequests is not None:
													for requestList in friendShipRequests:
														if requestList[0] == myId:
															friendShipStatus = "myRequest"
															break
										else:
											for requestList in friendShipRequestsMyMany:
												if requestList[0] == id:
													friendShipStatus = "request"
													break
											if friendShipStatus is False:
												friendShipRequests = friendShipRequestsMany.get(id, None)
												if friendShipRequests is not None:
													for requestList in friendShipRequests:
														if requestList[0] == myId:
															friendShipStatus = "myRequest"
															break
										friendShipStatusToSetDict.update({id: friendShipStatus})
								else:
									friendShipStatus = friendShipStatusDict.get(id, False)
									if friendShipStatus is False:
										if id in friendsIds:
											friendShipStatus = True
											friendsIds.remove(id)
										else:
											if friendShipRequestsMyMany is None:
												friendShipRequestsMy = friendShipRequestsMany.get(myId, None)
												if friendShipRequestsMy is not None:
													for requestList in friendShipRequestsMy:
														if requestList[0] == id:
															friendShipStatus = "request"
															break
													if friendShipStatus is False:
														friendShipRequests = friendShipRequestsMany.get(id, None)
														if friendShipRequests is not None:
															for requestList in friendShipRequests:
																if requestList[0] == myId:
																	friendShipStatus = "myRequest"
																	break
												else:
													friendShipRequests = friendShipRequestsMany.get(id, None)
													if friendShipRequests is not None:
														for requestList in friendShipRequests:
															if requestList[0] == myId:
																friendShipStatus = "myRequest"
																break
											else:
												for requestList in friendShipRequestsMyMany:
													if requestList[0] == id:
														friendShipStatus = "request"
														break
												if friendShipStatus is False:
													friendShipRequests = friendShipRequestsMany.get(id, None)
													if friendShipRequests is not None:
														for requestList in friendShipRequests:
															if requestList[0] == myId:
																friendShipStatus = "myRequest"
																break
										friendShipStatusToSetDict.update({id: friendShipStatus})
								privacyChecker = checkPrivacy(privacy = privacyInstance.get(user_id__exact = id), isAuthenticated = isAuth, isFriend = friendShipStatus is True or friendShipStatus == "request")
								if showOnlineStatus is None:
									showOnlineStatus = privacyChecker.checkOnlineStatusPrivacy()
								if showActiveStatus is None:
									showActiveStatus = privacyChecker.checkActiveStatusPrivacy()
								privacyDictMy.update({'viewOnlineStatus': showOnlineStatus, 'viewActiveStatus': showActiveStatus})
								privacyToSetDict.update({id: privacyDict})
					if showOnlineStatus is True:
						statusChannelsToGetOnlineSet.add(id)
					if showActiveStatus is True:
						statusChannelsToGetActiveSet.add(id)
				if privacyToSetDict is not None:
					privacyCache.set_many(privacyToSetDict)
					if friendShipStatusToSetDict is not None:
						friendShipStatusCache.set(myId, friendShipStatusToSetDict)
				statusChannelsToGet = statusChannelsToGetOnlineSet | statusChannelsToGetActiveSet
				if len(statusChannelsToGet) > 0:
					setDict = {}
					channel = self.channel_name
					layer = self.channel_layer
					groupAdd = layer.group_add
					statusChannelsCache = caches["statusChannelLayers"]
					statusChannelsToGet.add(myId)
					statusChannelsDictMany = statusChannelsCache.get_many(statusChannelsToGet)
					statusChannelsToGet.remove(myId)
					statusChannelsToSearch = statusChannelsToGet - statusChannelsDictMany.keys()
					controlConnectToOnline = set()
					controlConnectToActive = set()
					for id, itemDict in statusChannelsDictMany.items():
						match = False
						if id in statusChannelsToGetOnlineSet:
							itemOnlineDictSet = itemDict.get('online', None)
							if itemOnlineDictSet is None:
								itemDict.update({'online': {myId: {channel,}}})
								match = True
								controlConnectToOnline.add(id)
							else:
								itemOnlineSet = itemOnlineDictSet.get(myId, None)
								if itemOnlineSet is None:
									itemOnlineDictSet.update({myId: {channel,}})
									match = True
									controlConnectToOnline.add(id)
								else:
									if channel not in itemOnlineSet:
										itemOnlineSet.add(channel)
										match = True
										controlConnectToOnline.add(id)
							statusChannelsToGetOnlineSet.remove(id)
						if id in statusChannelsToGetActiveSet:
							itemActiveDictSet = itemDict.get('active', None)
							if itemActiveDictSet is None:
								itemDict.update({'active': {myId: {channel,}}})
								if match is False:
									match = True
								controlConnectToActive.add(id)
							else:
								itemActiveSet = itemActiveDictSet.get(myId, None)
								if itemActiveSet is None:
									itemActiveDictSet.update({myId: {channel,}})
									if match is False:
										match = True
									controlConnectToActive.add(id)
								else:
									if channel not in itemActiveSet:
										itemActiveSet.add(channel)
										if match is False:
											match = True
										controlConnectToActive.add(id)
							statusChannelsToGetActiveSet.remove(id)
						if match is True:
							setDict.update({id: itemDict})
					if len(statusChannelsToSearch) > 0:
						ao = {'online': {myId: {channel,}}}
						a1 = {myId: {channel,}}
						a2 = {'active': a1}
						for id in statusChannelsToSearch:
							if id in statusChannelsToGetOnlineSet:
								setDict.update({id: ao})
								statusChannelsToGetOnlineSet.remove(id)
								controlConnectToOnline.add(id)
							if id in statusChannelsToGetActiveSet:
								item = setDict.get(id, None)
								if item is None:
									setDict.update({id: a2})
								else:
									item.update({'active': a1})
								statusChannelsToGetActiveSet.remove(id)
								controlConnectToActive.add(id)
					hasConnectToOnline = len(controlConnectToOnline) > 0
					hasConnectToActive = len(controlConnectToActive) > 0
					if hasConnectToOnline is True or hasConnectToActive is True:
						allConnectToOnline = copy.copy(controlConnectToOnline)
						allConnectToActive = copy.copy(controlConnectToActive)
						statusChannelsDictMy = statusChannelsDictMany.get(myId, None)
						if statusChannelsDictMy is None:
							if hasConnectToOnline is True and hasConnectToActive is True:
								setDict.update({myId: {'self': {'online': {channel: controlConnectToOnline}, 'active': {channel: controlConnectToActive}}}})
							else:
								if hasConnectToOnline is True:
									setDict.update({myId: {'self': {'online': {channel: controlConnectToOnline}}}})
								else:
									setDict.update({myId: {'self': {'active': {channel: controlConnectToActive}}}})
						else:
							selfDict = statusChannelsDictMy.get('self', None)
							if selfDict is None:
								if hasConnectToOnline is True and hasConnectToActive is True:
									statusChannelsDictMy.update({'self': {'online': {channel: controlConnectToOnline}, 'active': {channel: controlConnectToActive}}})
								else:
									if hasConnectToOnline is True:
										statusChannelsDictMy.update({'self': {'online': {channel: controlConnectToOnline}}})
									else:
										statusChannelsDictMy.update({'self': {'active': {channel: controlConnectToActive}}})
							else:
								if hasConnectToOnline is True:
									selfOnlineChannelsDictSet = selfDict.get('online', None)
									if selfOnlineChannelsDictSet is None:
										selfDict.update({'online': {channel: controlConnectToOnline}})

									else:
										selfOnlineChannelsDictSet.update({channel: controlConnectToOnline})
								if hasConnectToActive is True:
									selfActiveChannelsDictSet = selfDict.get('active', None)
									if selfActiveChannelsDictSet is None:
										selfDict.update({'active': {channel: controlConnectToActive}})
									else:
										selfActiveChannelsDictSet.update({channel: controlConnectToActive})
							setDict.update({myId: statusChannelsDictMy})
						if len(setDict.keys()) > 0:
							#try:
							statusChannelsCache.set_many(setDict)
							#except pylibmc.Error:
								#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
						for id in statusChannelsToGet:
							if id in controlConnectToOnline:
								try:
									async_to_sync(groupAdd)('%s_onl_stat' % id, channel)
									controlConnectToOnline.remove(id)
								except DenyConnection as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								except ChannelFull as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								except StopConsumer as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								except ConnectionError as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								except RedisError as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
							if id in controlConnectToActive:
								try:
									async_to_sync(groupAdd)('%s_act_stat' % id, channel)
									controlConnectToActive.remove(id)
								except DenyConnection as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								except ChannelFull as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								except StopConsumer as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								except ConnectionError as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								except RedisError as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
						# На случай, если кого-то не удалось подключить, то нужно удалить запись из кеша
						if len(controlConnectToOnline) > 0:
							hasFailureToOnline = True
							itemsToRemove = set()
							dictMy = setDict[myId]
							selfDictMy = dictMy['self']
							selfOnlineDictSet = selfDictMy['online']
							selfOnlineSet = selfOnlineDictSet[channel]
							selfOnlineSet = selfOnlineSet - controlConnectToOnline
							if len(selfOnlineSet) == 0:
								if len(selfOnlineDictSet.keys()) > 1:
									del selfOnlineDictSet[channel]
								else:
									if len(selfDictMy.keys()) == 1:
										if len(dictMy.keys()) == 1:
											del setDict[myId]
											itemsToRemove.add(myId)
										else:
											del dictMy['self']
									else:
										del selfDictMy['online']
							else:
								selfOnlineDictSet[channel] = selfOnlineSet
							for id in controlConnectToOnline:
								channelsDictRoot = setDict[id]
								channelsDict = channelsDictRoot['online']
								channelsSet = channelsDict[myId]
								if len(channelsSet) == 1:
									if len(channelsDict.keys()) == 1:
										if len(channelsDictRoot.keys()) == 1:
											del setDict[id]
											itemsToRemove.add(id)
										else:
											del channelsDictRoot['online']
									else:
										del channelsDict[myId]
								else:
									channelsSet.remove(channel)
							if len(controlConnectToActive) > 0:
								hasFailureToActive = True
								selfActiveDictSet = selfDictMy['active']
								selfActiveSet = selfActiveDictSet[channel]
								selfActiveSet = selfActiveSet - controlConnectToActive
								if len(selfActiveSet) == 0:
									if len(selfActiveDictSet.keys()) > 1:
										del selfActiveDictSet[channel]
									else:
										if len(selfDictMy.keys()) == 1:
											if len(dictMy.keys()) == 1:
												del setDict[myId]
												itemsToRemove.add(myId)
											else:
												del dictMy['self']
										else:
											del selfDictMy['active']
								else:
									selfActiveDictSet[channel] = selfActiveSet
								for id in controlConnectToActive:
									channelsDictRoot = setDict[id]
									channelsDict = channelsDictRoot['active']
									channelsSet = channelsDict[myId]
									if len(channelsSet) == 1:
										if len(channelsDict.keys()) == 1:
											if len(channelsDictRoot.keys()) == 1:
												del setDict[id]
												itemsToRemove.add(id)
											else:
												del channelsDictRoot['active']
										else:
											del channelsDict[myId]
									else:
										channelsSet.remove(channel)
							else:
								hasFailureToActive = False
							if len(setDict.keys()) > 0:
								#try:
								statusChannelsCache.set_many(setDict)
								#except pylibmc.Error:
									#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
							if len(itemsToRemove) > 0:
								#try:
								statusChannelsCache.delete_many(itemsToRemove)
								#except pylibmc.Error:
								#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
						else:
							hasFailureToOnline = False
							if len(controlConnectToActive) > 0:
								hasFailureToActive = True
								itemsToRemove = set()
								selfActiveDictSet = selfDictMy['active']
								selfActiveSet = selfActiveDictSet[channel]
								selfActiveSet = selfActiveSet - controlConnectToActive
								if len(selfActiveSet) == 0:
									if len(selfActiveDictSet.keys()) > 1:
										del selfActiveDictSet[channel]
									else:
										if len(selfDictMy.keys()) == 1:
											if len(dictMy.keys()) == 1:
												del setDict[myId]
												itemsToRemove.add(myId)
											else:
												del dictMy['self']
										else:
											del selfDictMy['active']
								else:
									selfActiveDictSet[channel] = selfActiveSet
								for id in controlConnectToActive:
									channelsDictRoot = setDict[id]
									channelsDict = channelsDictRoot['active']
									channelsSet = channelsDict[myId]
									if len(channelsSet) == 1:
										if len(channelsDict.keys()) == 1:
											if len(channelsDictRoot.keys()) == 1:
												del setDict[id]
												itemsToRemove.add(id)
											else:
												del channelsDictRoot['active']
										else:
											del channelsDict[myId]
									else:
										channelsSet.remove(channel)
								if len(setDict.keys()) > 0:
									#try:
									statusChannelsCache.set_many(setDict)
									#except pylibmc.Error:
										#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								if len(itemsToRemove) > 0:
									#try:
									statusChannelsCache.delete_many(itemsToRemove)
									#except pylibmc.Error:
									#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
							else:
								hasFailureToActive = False
						connectOnlineTuple = tuple(allConnectToOnline - controlConnectToOnline)
						connectActiveTuple = tuple(allConnectToActive - controlConnectToActive)
						if len(connectOnlineTuple) > 0:
							if len(connectActiveTuple) > 0:
								if hasFailureToOnline is True:
									if hasFailureToActive is True:
										async_to_sync(layer.group_send)(myId, {
											"type": "sendToClient",
											"data": {
												"item": json.dumps({
													"method": "acceptListernUserStatus",
													"online": connectOnlineTuple,
													"active": connectActiveTuple,
													"failure": {
														"online": tuple(controlConnectToOnline),
														"active": tuple(controlConnectToActive)
													}
												})
											}
										})
									else:
										async_to_sync(layer.group_send)(myId, {
											"type": "sendToClient",
											"data": {
												"item": json.dumps({
													"method": "acceptListernUserStatus",
													"online": connectOnlineTuple,
													"active": connectActiveTuple,
													"failure": {
														"online": tuple(controlConnectToOnline)
													}
												})
											}
										})
								else:
									if hasFailureToActive is True:
										async_to_sync(layer.group_send)(myId, {
											"type": "sendToClient",
											"data": {
												"item": json.dumps({
													"method": "acceptListernUserStatus",
													"online": connectOnlineTuple,
													"active": connectActiveTuple,
													"failure": {
														"active": tuple(controlConnectToActive)
													}
												})
											}
										})
									else:
										async_to_sync(layer.group_send)(myId, {
											"type": "sendToClient",
											"data": {
												"item": json.dumps({
													"method": "acceptListernUserStatus",
													"online": connectOnlineTuple,
													"active": connectActiveTuple
												})
											}
										})
							else:
								if hasFailureToOnline is True:
									async_to_sync(layer.group_send)(myId, {
										"type": "sendToClient",
										"data": {
											"item": json.dumps({
												"method": "acceptListernUserStatus",
												"online": connectOnlineTuple,
												"failure": {
													"online": tuple(controlConnectToOnline)
												}
											})
										}
									})
								else:
									async_to_sync(layer.group_send)(myId, {
										"type": "sendToClient",
										"data": {
											"item": json.dumps({
												"method": "acceptListernUserStatus",
												"online": connectOnlineTuple
											})
										}
									})
						else:
							if len(connectActiveTuple) > 0:
								if hasFailureToActive is True:
									async_to_sync(layer.group_send)(myId, {
										"type": "sendToClient",
										"data": {
											"item": json.dumps({
												"method": "acceptListernUserStatus",
												"active": connectActiveTuple,
												"failure": {
												"active": tuple(controlConnectToActive)
												}
											})
										}
									})
								else:
									async_to_sync(layer.group_send)(myId, {
										"type": "sendToClient",
										"data": {
											"item": json.dumps({
												"method": "acceptListernUserStatus",
												"active": connectActiveTuple
											})
										}
									})
							else:
								async_to_sync(layer.group_send)(myId, {
									"type": "sendToClient",
									"data": {
										"item": json.dumps({
											"method": "acceptListernUserStatus",
											"failure": {
												"online": tuple(controlConnectToOnline),
												"active": tuple(controlConnectToActive)
											}
										})
									}
								})

	def endListernStatus(self, usersData):
		usersList = usersData.get("users", None)
		if isinstance(usersList, list):
			if 300 > len(usersList) > 0:
				usersSet = set(usersList)
				for item in usersSet:
					if isinstance(item, int) is True or item.isdigit() is False or item.startswith("0") is True:
						return
				myId = self.userId
				usersSet.add(myId)
				#try:
				statusChannelsCache = caches["statusChannelLayers"]
				statusChannelsDictMany = statusChannelsCache.get_many(usersSet, None)
				#except pylibmc.Error:
					#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
					#return
				usersSet = set(statusChannelsDictMany.keys())
				if len(usersSet) > 0:
					setManyDict = {}
					deleteItemsSet = set()
					channel = self.channel_name
					disconnectOnlineSet = set()
					disconnectActiveSet = set()
					disconnectOnlineErrorSet = set()
					disconnectActiveErrorSet = set()
					layer = self.channel_layer
					groupDiscard = layer.group_discard
					myselfDict = statusChannelsDictMany.pop(myId, None)
					for id, statusDataDict in statusChannelsDictMany.items():
						match = False
						onlineStatusListenersDict = statusDataDict.get('online', None)
						activeStatusListenersDict = statusDataDict.get('active', None)
						if onlineStatusListenersDict is not None:
							onlineChannelsSet = onlineStatusListenersDict.get(myId, None)
							if onlineChannelsSet is not None and channel in onlineChannelsSet:
								try:
									async_to_sync(groupDiscard)('%s_onl_stat' % id, channel)
									disconnectOnlineSet.add(id)
									if len(onlineChannelsSet) > 1:
										onlineChannelsSet.remove(channel)
										match = True
									else:
										if len(onlineStatusListenersDict.keys()) > 1:
											del onlineStatusListenersDict[myId]
											match = True
										else:
											if len(statusDataDict.keys()) > 1:
												del statusDataDict['online']
												match = True
											else:
												deleteItemsSet.add(id)
								except DenyConnection as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
									disconnectOnlineErrorSet.add(id)
								except ChannelFull as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
									disconnectOnlineErrorSet.add(id)
								except StopConsumer as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
									disconnectOnlineErrorSet.add(id)
								except ConnectionError as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
									disconnectOnlineErrorSet.add(id)
								except RedisError as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
									disconnectOnlineErrorSet.add(id)
						if activeStatusListenersDict is not None:
							activeChannelsSet = activeStatusListenersDict.get(myId, None)
							if activeChannelsSet is not None and channel in activeChannelsSet:
								try:
									async_to_sync(groupDiscard)('%s_act_stat' % id, channel)
									disconnectActiveSet.add(id)
									if len(activeChannelsSet) > 1:
										activeChannelsSet.remove(channel)
										if match is False:
											match = True
									else:
										if len(activeStatusListenersDict.keys()) > 1:
											del activeStatusListenersDict[myId]
											if match is False:
												match = True
										else:
											if len(statusDataDict.keys()) > 1:
												del statusDataDict['active']
												if match is False:
													match = True
											else:
												deleteItemsSet.add(id)
												if match is True:
													match = False
								except DenyConnection as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
									disconnectActiveErrorSet.add(id)
								except ChannelFull as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
									disconnectActiveErrorSet.add(id)
								except StopConsumer as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
									disconnectActiveErrorSet.add(id)
								except ConnectionError as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
									disconnectActiveErrorSet.add(id)
								except RedisError as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
									disconnectActiveErrorSet.add(id)
						if match is True:
							setManyDict.update({id: statusDataDict})
					disconnectOnlineSetLength = len(disconnectOnlineSet)
					disconnectActiveSetLength = len(disconnectActiveSet)
					hasDisconnectOnline = disconnectOnlineSetLength > 0
					hasDisconnectActive = disconnectActiveSetLength > 0
					if hasDisconnectOnline is True or hasDisconnectActive is True:
						selfItems = myselfDict['self']
						if hasDisconnectOnline is True:
							selfOnlineItemsDict = selfItems['online']
							selfOnlineItemsSet = selfOnlineItemsDict[channel]
							if len(selfOnlineItemsSet) == disconnectOnlineSetLength:
								if len(selfOnlineItemsDict.keys()) > 1:
									del selfOnlineItemsDict[channel]
								else:
									if len(selfItems.keys()) > 1:
										del selfItems['online']
									else:
										if len(myselfDict.keys()) > 1:
											del myselfDict['self']
										else:
											myselfDict = {}
							else:
								selfOnlineItemsDict[channel] = selfOnlineItemsSet - disconnectOnlineSet
						if hasDisconnectActive is True:
							selfActiveItemsDict = selfItems['active']
							selfActiveItemsSet = selfActiveItemsDict[channel]
							if len(selfActiveItemsSet) == disconnectActiveSetLength:
								if len(selfActiveItemsDict.keys()) > 1:
									del selfActiveItemsDict[channel]
								else:
									if len(selfItems.keys()) > 1:
										del selfItems['active']
									else:
										if len(myselfDict.keys()) > 1:
											del myselfDict['self']
										else:
											myselfDict = {}
							else:
								selfActiveItemsDict[channel] = selfActiveItemsSet - disconnectActiveSet
						if len(myselfDict.keys()) > 1:
							setManyDict.update({myId: myselfDict})
						else:
							deleteItemsSet.add(myId)
						if len(setManyDict.keys()) > 0:
							#try:
							statusChannelsCache.set_many(setManyDict)
							#except pylibmc.Error:
								#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								#return
						if len(deleteItemsSet) > 0:
							#try:
							statusChannelsCache.delete_many(deleteItemsSet)
							#except pylibmc.Error:
								#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								#return
					outputMessageDict = {"method": "disconnectUserStatus"}
					if hasDisconnectOnline is True:
						outputMessageDict.update({"online": tuple(disconnectOnlineSet)})
					if hasDisconnectActive is True:
						outputMessageDict.update({"active": tuple(disconnectActiveSet)})
					hasDisconnectOnlineError = len(disconnectOnlineErrorSet) > 0
					hasDisconnectActiveError = len(disconnectActiveErrorSet) > 0
					if hasDisconnectOnlineError or hasDisconnectActiveError:
						errorDict = {}
						if hasDisconnectOnlineError is True:
							errorDict.update({"online": tuple(disconnectOnlineErrorSet)})
						else:
							errorDict.update({"active": tuple(disconnectActiveErrorSet)})
						outputMessageDict.update({"failure": errorDict})
					async_to_sync(layer.group_send)(myId, {"type": "sendToClient", "data": {"item": json.dumps(outputMessageDict)
						}
					})

	def disconnectListernStatus(self):
		'''
		Пользователь вышел с сайта
		'''
		myId = self.userId
		#try:
		statusChannelsCache = caches["statusChannelLayers"]
		statusChannelsDictMy = statusChannelsCache.get(myId, None)
		#except pylibmc.Error:
			#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
			# Добавить в CELERY задачу
		if statusChannelsDictMy is not None:
			myselfListenersDict = statusChannelsDictMy.get("self", None)
			if myselfListenersDict is not None:
				onlineChannelsDict = myselfListenersDict.get("online", None)
				activeChannelsDict = myselfListenersDict.get("active", None)
				if onlineChannelsDict is not None or activeChannelsDict is not None:
					channel = self.channel_name
					onlineSet = onlineChannelsDict.get(channel, None)
					activeSet = activeChannelsDict.get(channel, None)
					if onlineSet is not None or activeSet is not None:
						groupDiscard = self.channel_layer.group_discard
						if onlineSet is not None:
							onlineSetIter = copy.copy(onlineSet)
							for id in onlineSetIter:
								try:
									async_to_sync(groupDiscard)('%s_onl_stat' % id, channel)
									if len(onlineSet) > 1:
										onlineSet.remove(id)
									else:
										onlineSet = set()
										if len(onlineChannelsDict.keys()) > 1:
											del onlineChannelsDict[channel]
										else:
											if len(myselfListenersDict.keys()) > 1:
												del myselfListenersDict["online"]
											else:
												del statusChannelsDictMy["self"]
								except DenyConnection as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								except ChannelFull as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								except StopConsumer as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								except ConnectionError as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								except RedisError as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
						if activeSet is not None:
							activeSetIter = copy.copy(activeSet)
							for id in activeSetIter:
								try:
									async_to_sync(groupDiscard)('%s_act_stat' % id, channel)
									if len(activeSet) > 1:
										activeSet.remove(id)
									else:
										activeSet = set()
										if len(activeChannelsDict.keys()) > 1:
											del activeChannelsDict[channel]
										else:
											if len(myselfListenersDict.keys()) > 1:
												del myselfListenersDict["active"]
											else:
												del statusChannelsDictMy["self"]
								except DenyConnection as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								except ChannelFull as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								except StopConsumer as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								except ConnectionError as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								except RedisError as er:
									logg.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
						if onlineSet is not None and activeSet is not None:
							successOnlineDisconnect = onlineSetIter - onlineSet
							successActiveDisconnect = activeSetIter - activeSet
							hasOnlineDisconnect = len(successOnlineDisconnect) > 0
							hasActiveDisconnect = len(successActiveDisconnect) > 0
							if hasOnlineDisconnect is True and hasActiveDisconnect is True:
								if len(statusChannelsDictMy.keys()) > 0:
									setManyDict = {myId: statusChannelsDictMy}
									itemsToRemove = set()
								else:
									setManyDict = {}
									itemsToRemove = {myId,}
								#try:
								statusChannelsDictMany = statusChannelsCache.get_many((successOnlineDisconnect | successActiveDisconnect))
								#except pylibmc.Error:
			#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								# Добавить в CELERY задачу
								for id, statusDict in statusChannelsDictMany.items():
									match = False
									if id in successOnlineDisconnect:
										onlineDict = statusDict["online"]
										onlineDictSet = onlineDict[myId]
										if len(onlineDictSet) > 1:
											onlineDictSet.remove(channel)
											match = True
										else:
											if len(onlineDict.keys()) > 1:
												del onlineDict[myId]
												match = True
											else:
												if len(statusDict.keys()) > 1:
													del statusDict["online"]
													match = True
												else:
													itemsToRemove.add(id)
										successOnlineDisconnect.remove(id)
									if id in successActiveDisconnect:
										activeDict = statusDict["active"]
										activeDictSet = activeDict[myId]
										if len(activeDictSet) > 1:
											activeDictSet.remove(channel)
											if match is False:
												match = True
										else:
											if len(activeDict.keys()) > 1:
												del activeDict[myId]
												if match is False:
													match = True
											else:
												if len(statusDict.keys()) > 1:
													del statusDict["active"]
													if match is False:
														match = True
												else:
													if match is True:
														match = False
													itemsToRemove.add(id)
										successActiveDisconnect.remove(id)
									if match is True:
										setManyDict.update({id: statusDict})
								if len(setManyDict.keys()) > 0:
									#try:
									statusChannelsCache.set_many(setManyDict)
									#except pylibmc.Error:
			#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								if len(itemsToRemove) > 0:
									#try:
									statusChannelsCache.delete_many(itemsToRemove)
									#except pylibmc.Error:
			#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
							else:
								if hasOnlineDisconnect is True:
									if len(statusChannelsDictMy.keys()) > 0:
										setManyDict = {myId: statusChannelsDictMy}
										itemsToRemove = set()
									else:
										setManyDict = {}
										itemsToRemove = {myId,}
									#try:
									statusChannelsDictMany = statusChannelsCache.get_many(successOnlineDisconnect)
									#except pylibmc.Error:
			#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
									# Добавить в CELERY задачу
									for id, statusDict in statusChannelsDictMany.items():
										if id in successOnlineDisconnect:
											onlineDict = statusDict["online"]
											onlineDictSet = onlineDict[myId]
											if len(onlineDictSet) > 1:
												onlineDictSet.remove(channel)
												setManyDict.update({id: statusDict})
											else:
												if len(onlineDict.keys()) > 1:
													del onlineDict[myId]
													setManyDict.update({id: statusDict})
												else:
													if len(statusDict.keys()) > 1:
														del statusDict["online"]
														setManyDict.update({id: statusDict})
													else:
														itemsToRemove.add(id)
											successOnlineDisconnect.remove(id)
									if len(setManyDict.keys()) > 0:
										#try:
										statusChannelsCache.set_many(setManyDict)
										#except pylibmc.Error:
				#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
									if len(itemsToRemove) > 0:
										#try:
										statusChannelsCache.delete_many(itemsToRemove)
										#except pylibmc.Error:
				#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								elif hasActiveDisconnect is True:
									if len(statusChannelsDictMy.keys()) > 0:
										setManyDict = {myId: statusChannelsDictMy}
										itemsToRemove = set()
									else:
										setManyDict = {}
										itemsToRemove = {myId,}
									#try:
									statusChannelsDictMany = statusChannelsCache.get_many(successActiveDisconnect)
									#except pylibmc.Error:
			#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
									# Добавить в CELERY задачу
									for id, statusDict in statusChannelsDictMany.items():
										if id in successActiveDisconnect:
											activeDict = statusDict["active"]
											activeDictSet = activeDict[myId]
											if len(activeDictSet) > 1:
												activeDictSet.remove(channel)
												setManyDict.update({id: statusDict})
											else:
												if len(activeDict.keys()) > 1:
													del activeDict[myId]
													setManyDict.update({id: statusDict})
												else:
													if len(statusDict.keys()) > 1:
														del statusDict["active"]
														setManyDict.update({id: statusDict})
													else:
														itemsToRemove.add(id)
											successActiveDisconnect.remove(id)
									if len(setManyDict.keys()) > 0:
										#try:
										statusChannelsCache.set_many(setManyDict)
										#except pylibmc.Error:
				#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
									if len(itemsToRemove) > 0:
										#try:
										statusChannelsCache.delete_many(itemsToRemove)
										#except pylibmc.Error:
				#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
							# Обработать ошибки в множествах onlineSet и activeSet
						else:
							if onlineSet is not None:
								successOnlineDisconnect = onlineSetIter - onlineSet
								if len(successOnlineDisconnect) > 0:
									if len(statusChannelsDictMy.keys()) > 0:
										setManyDict = {myId: statusChannelsDictMy}
										itemsToRemove = set()
									else:
										setManyDict = {}
										itemsToRemove = {myId,}
									#try:
									statusChannelsDictMany = statusChannelsCache.get_many(successOnlineDisconnect)
									#except pylibmc.Error:
			#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
									# Добавить в CELERY задачу

								# Обработать ошибки в множествах onlineSet
							else:
								successActiveDisconnect = activeSetIter - activeSet
								if len(successActiveDisconnect) > 0:
									if len(statusChannelsDictMy.keys()) > 0:
										setManyDict = {myId: statusChannelsDictMy}
										itemsToRemove = set()
									else:
										setManyDict = {}
										itemsToRemove = {myId,}
									#try:
									statusChannelsDictMany = statusChannelsCache.get_many(successActiveDisconnect)
									#except pylibmc.Error:
			#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
									# Добавить в CELERY задачу
									for id, statusDict in statusChannelsDictMany.items():
										if id in successActiveDisconnect:
											activeDict = statusDict["active"]
											activeDictSet = activeDict[myId]
											if len(activeDictSet) > 1:
												activeDictSet.remove(channel)
												setManyDict.update({id: statusDict})
											else:
												if len(activeDict.keys()) > 1:
													del activeDict[myId]
													setManyDict.update({id: statusDict})
												else:
													if len(statusDict.keys()) > 1:
														del statusDict["active"]
														setManyDict.update({id: statusDict})
													else:
														itemsToRemove.add(id)
											successActiveDisconnect.remove(id)
									if len(setManyDict.keys()) > 0:
										#try:
										statusChannelsCache.set_many(setManyDict)
										#except pylibmc.Error:
				#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
									if len(itemsToRemove) > 0:
										#try:
										statusChannelsCache.delete_many(itemsToRemove)
										#except pylibmc.Error:
				#logg.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
								# Обработать ошибки в множествах activeSet

	def connectToStatus(self):
		'Сообщить в свой канал, что я зашёл'
		id = self.userId
		channel = self.channel_name
		send = self.channel_layer.group_send
		async_to_sync(send)("%s_onl_stat" % id, {
			"type": "sendToClient",
			"data": {
				"item": json.dumps({
					'method': 'stateOnline',
					'id': id
				})
			}
		})
		async_to_sync(send)("%s_act_stat" % id, {
			"type": "sendToClient",
			"data": {
				"item": json.dumps({
					'method': 'stateActive',
					'id': id
				})
			}
		})

	def freshOnline(self):
		user = self.scope["user"]
		if user.is_authenticated:
			uid = str(user.id)
			users = caches["onlineUsers"]
			cacheKeys = users.get('online', set())
			usersOnline = set(users.get_many(cacheKeys).keys())
			if uid not in usersOnline:
				usersOnline.add(uid)
				if len(usersOnline) > ONLINE_MAX:
					del usersOnline[0]
				users.set_many({'online': usersOnline, uid: True}, ONLINE_THRESHOLD_MAX)
				async_to_sync(self.channel_layer.group_send)("%s_onl_stat" % uid, {
					"type": "sendToClient",
					"data": {
						"item": json.dumps({
							'method': 'stateOnline',
							'id': uid
						})
					}
				})
			else:
				iffyStatusCache = caches['iffyUsersOnlineStatus']
				iffyAll = iffyStatusCache.get_many({'iffy', uid})
				iffyMany = iffyAll.get('iffy', set())
				iffyUser = iffyAll.get(uid, False)
				iffyUserBool = isinstance(iffyUser, datetime)
				userInIffyMany = uid in iffyMany
				if userInIffyMany and iffyUserBool:
					iffyMany.remove(uid)
					if bool(iffyMany):
						iffyStatusCache.set('iffy', iffyMany)
						iffyStatusCache.delete(uid)
					else:
						iffyStatusCache.delete_many({'iffy', uid})
					async_to_sync(self.channel_layer.group_send)("%s_onl_stat" % uid, {
						"type": "sendToClient",
						"data": {
							"item": json.dumps({
								'method': 'stateOnline',
								'id': uid
							})
						}
					})
				elif iffyUserBool:
					iffyStatusCache.delete(uid)
					async_to_sync(self.channel_layer.group_send)("%s_onl_stat" % uid, {
						"type": "sendToClient",
						"data": {
							"item": json.dumps({
								'method': 'stateOnline',
								'id': uid
							})
						}
					})
				elif userInIffyMany:
					iffyMany.remove(uid)
					if bool(iffyMany):
						iffyStatusCache.set('iffy', iffyMany)
					else:
						iffyStatusCache.delete('iffy')
				else:
					threshhold = ONLINE_THRESHOLD_MAX
					users.touch(uid, threshhold)
					users.touch('online', threshhold)
			self.send(text_data = json.dumps({
				'method': 'checkSession',
				'status': 'in'
				})
			)
		else:
			self.send(text_data = json.dumps({
				'method': 'checkSession',
				'status': 'out'
				})
			)

	def freshActive():
		pass
