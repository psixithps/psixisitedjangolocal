from django.conf.urls import url
from realtime.views import checkUserStatus, checkUsersStatusFirst, updateChatStatusXHR
from realtime.notification import checkUnreadUpdates, removeUpdate, removeAllUpdates

urlpatterns = [
    url(r'^online/$', checkUserStatus, name = "checkOnline"),
    url(r'^online-first/$', checkUsersStatusFirst, name = "checkOnlineFirst"),
    url(r'^active/$', updateChatStatusXHR, name = "checkChatStatus"),
    url(r'^updates/$', checkUnreadUpdates, name = "getUpdates"),
    url(r'^update/remove/(?P<type>\S+)/(?P<state>\S+)/(?P<senderId>\d+)/$', removeUpdate, name = "deleteNotifyEvent"),
    url(r'^updates/remove/all/$', removeAllUpdates, name = "deleteAllNotifyEvents")
    ]