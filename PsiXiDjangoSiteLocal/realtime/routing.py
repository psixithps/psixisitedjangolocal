from channels.security.websocket import AllowedHostsOriginValidator
from channels.auth import AuthMiddlewareStack
from django.conf.urls import url
from channels.routing import ProtocolTypeRouter, URLRouter
from realtime.consumers import mainConsumer


routeApp = ProtocolTypeRouter({
    "websocket": AllowedHostsOriginValidator(
        AuthMiddlewareStack(
            URLRouter([
                url(r"^sock$", mainConsumer),
            ])
            )
        )
})