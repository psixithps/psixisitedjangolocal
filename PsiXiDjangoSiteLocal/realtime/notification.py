import json
from functools import lru_cache
from collections import OrderedDict
from django.core.cache import caches
from base.helpers import checkCSRF
from psixiSocial.models import mainUserProfile
from django.http import JsonResponse
from django.template.loader import render_to_string as rts
from django.urls import reverse
from django.shortcuts import render, redirect
from django.contrib import messages as templateMessage
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

class cachedFragments():
    def getMenu():
        menu = OrderedDict({
            
            })
        menu = json.dumps(menu)
        return menu

def checkUnreadUpdates(request):
    if request.method == "GET":
        if request.user.is_authenticated:
            updates = caches["newUpdates"]
            id = request.user.id
            events = updates.get(id, [])
            if len(events) == 0:
                if request.is_ajax():
                    return JsonResponse({"updates": json.dumps(({
                        'method': 'closeFullNotify'
                        }))})
                pass
            resultListDict = []
            messages = None
            for event in events:
                state = event[1]
                if state != "hidden":
                    type = event[0]
                    senderId = event[2]
                    user = mainUserProfile.objects.get(id = senderId)
                    username = user.username
                    userSlug = user.profileUrl
                    profileLink = reverse("profile", kwargs = {'currentUser': userSlug})
                    if type == 'personalMessage':
                        if messages is None:
                            messagesCache = caches["unreadPersonalMessages"]
                            messages = messagesCache.get(id, [])
                        messagesCounter = 0
                        for message in messages:
                            if message[2] == event[2]:
                                messagesCounter += 1
                        resultListDict.append({
                            'method': 'messageMany',
                            'value': 'dialogMessage-new-%s' % senderId,
                            'slug': userSlug,
                            'profileLink': profileLink,
                            'dialogLink': reverse("personalMessage", kwargs = {'toUser': userSlug}),
                            'username': username,
                            'count': messagesCounter,
                            'forgetLink': reverse("deleteNotifyEvent", kwargs = {'type': type, 'state': 'new', 'senderId': senderId}),
                            })
                    elif type == "friend":
                        if state == 'new':
                            resultListDict.append({
                                'method': 'friendRequestToMeNotify',
                                'value': 'friend-new-%s' % senderId,
                                'eventType': 'notify',
                                'profileLink': profileLink,
                                'username': username,
                                'confirmLink': reverse("addFriend", kwargs = {'userId': senderId}),
                                'rejectLink': reverse("rejectFriend", kwargs = {'userId': senderId}),
                                'forgetLink': reverse("deleteNotifyEvent", kwargs = {'type': 'friend', 'state': 'new', 'senderId': senderId}),
                                })
                        elif state == 'rejected':
                            resultListDict.append({
                                'method': 'simpleNotify',
                                'textKeyName': 'friendRejected',
                                'value': 'friend-rejected-%s' % senderId,
                                'eventType': 'notify',
                                'profileLink': profileLink,
                                'username': username,
                                'forgetLink': reverse("deleteNotifyEvent", kwargs = {'type': 'friend', 'state': 'rejected', 'senderId': senderId})
                                })
                        elif state == 'confirmed':
                            resultListDict.append({
                                'method': 'simpleNotify',
                                'textKeyName': 'friendConfirmed',
                                'value': 'friend-confirmed-%s' % senderId,
                                'eventType': 'notify',
                                'profileLink': profileLink,
                                'username': username,
                                'forgetLink': reverse("deleteNotifyEvent", kwargs = {'type': 'friend', 'state': 'confirmed', 'senderId': senderId})
                                })
                        elif state == 'removed':
                            resultListDict.append({
                                'method': 'simpleNotify',
                                'textKeyName': 'friendRemoved',
                                'value': 'friend-removed-%s' % senderId,
                                'eventType': 'notify',
                                'profileLink': profileLink,
                                'username': username,
                                'forgetLink': reverse("deleteNotifyEvent", kwargs = {'type': 'friend', 'state': 'removed', 'senderId': senderId})
                                })
                    else:
                        pass
            if request.is_ajax():
                return JsonResponse({'updates': json.dumps(resultListDict)})
            fragments = cachedFragments()
            return render(request, "base/classicPage/base.html", {
                'content': ('psixiSocial', 'userUpdates'),
                'menu': fragments.getMenu(),
                'notifyDict': resultListDict
                })

def removeUpdate(request, type, state, senderId):
    updatesCache = caches["newUpdates"]
    id = request.user.id
    senderId = int(senderId)
    updates = updatesCache.get(id, [])
    finded = False
    getMessagesCache = False
    messages = None
    for update in updates:
        updateType = update[0]
        updateState = update[1]
        if updateType == type and updateState == state and update[2] == senderId:
            finded = True
            if updateType == "friend":
                if updateState == "rejected":
                    del updates[updates.index(update)]
                    async_to_sync(layer)(id, {"type": "sendToClient", "data": {"item": json.dumps({
                        'method': 'removeNotifyEvent',
                        'reduceBaseValueBy': 1,
                        'value': 'friend-rejected-%s' % update[2]
                        })}})
                elif updateState == "confirmed":
                    del updates[updates.index(update)]
                    async_to_sync(layer)(id, {"type": "sendToClient", "data": {"item": json.dumps({
                        'method': 'removeNotifyEvent',
                        'reduceBaseValueBy': 1,
                        'value': 'friend-confirmed-%s' % update[2]
                        })}})
                elif updateState == "removed":
                    del updates[updates.index(update)]
                    async_to_sync(layer)(id, {"type": "sendToClient", "data": {"item": json.dumps({
                        'method': 'removeNotifyEvent',
                        'reduceBaseValueBy': 1,
                        'value': 'friend-removed-%s' % update[2]
                        })}})
                else:
                    updates[updates.index(update)][1] = "hidden"
                    async_to_sync(layer)(id, {"type": "sendToClient", "data": {"item": json.dumps({
                        'method': 'removeNotifyEvent',
                        'reduceBaseValueBy': 1,
                        'value': 'friend-new-%s' % update[2]
                        })}})
            elif updateType == "dialogMessage":
                updates[updates.index(update)][1] = "hidden"
                if getMessagesCache is False:
                    personalMessagesCache = caches["unreadPersonalMessages"]
                    messages = personalMessagesCache.get(id, [])
                    getMessagesCache = True
                messagesCounter = 0
                senderId = update[2]
                for message in messages:
                    if message[4] == senderId:
                        messagesCounter += 1
                async_to_sync(layer)(id, {"type": "sendToClient", "data": {"item": json.dumps({
                    'method': 'removeNotifyEvent',
                    'originalMethod': 'message',
                    'reduceBaseValueBy': messagesCounter,
                    'value': 'dialogMessage-new-%s' % senderId
                    })}})
            else:
                updates[updates.index(update)][1] = "hidden"
                async_to_sync(layer)(id, {"type": "sendToClient", "data": {"item": json.dumps({
                    'method': 'removeNotifyEvent',
                    'reduceBaseValueBy': 1,
                    'value': '%s-%s-%s' % (updateType, updateState, update[2])
                    })}})
            break
    if finded is True:
        if len(updates) > 0:
            updatesCache.set(id, updates)
        else:
            updatesCache.delete(id)
    if request.is_ajax():
        return JsonResponse({"updates": json.dumps(True)})
    return redirect()

def removeAllUpdates(request):
    updatesCache = caches["newUpdates"]
    id = request.user.id
    updates = updatesCache.get(id, tuple())
    for update in updates:
        updateType = update[0]
        updateState = update[1]
        if updateType == "friend":
            if updateState == "rejected" or updateState == "confirmed" or updateState == "removed":
                del updates[updates.index(update)]
            else:
                updates[updates.index(update)][1] = "hidden"
        else:
            updates[updates.index(update)][1] = "hidden"
    if bool(updates):
        updatesCache.set(id, updates)
    else:
        updatesCache.delete(id)
    async_to_sync(layer)(id, {"type": "sendToClient", "data": {"item": json.dumps({
        'method': 'removeNotify'
        })}})
    if request.is_ajax():
        return JsonResponse({"updates": json.dumps(True)})
    return render()