var defaultListener;
function realtimeEvents(csrf, ajaxUpdatesUrl, requestUserId, checkOnlineUrl, checkOnlineFirstUrl, checkActiveUrl, csrf, notifyMainTextObject, removeAllUpdatesUrl) {
    var doc = document;
    var win = window;
    function checkCookies(doc, userSock) {
        /* На боевом добавить в строку: 'domain=psixi-thps.pro; secure' */
        var cookiesString = doc.cookie;
        var time = null;
        var js = cookiesString.indexOf("js") != -1;
        var ws = cookiesString.indexOf("ws") != -1;
        if (!js) {
            var time = new Date(new Date().getTime() + (24 * 7 * 60 * 60 * 1000)).toUTCString();
            doc.cookie = "js=True; expires=" + time + "; path=/";
        }
        if (!ws) {
            !time ? time = new Date(new Date().getTime() + (24 * 7 * 60 * 60 * 1000)).toUTCString() : 0;
            if (userSock !== null) {
                doc.cookie = "ws=True; expires =" + time + "; path=/";
            } else {
                doc.cookie = "ws=False; expires=" + time + "; path=/";
            }
        }
        return new Object({"js": js, "ws": ws});
    };
    var cookiesObj = checkCookies(doc, userSock);
    var sockTryReconnect = false; // В локальном обработчике ошибок вебсокета поменять значение после первого запуска reconnect()
    function sockReAddListeners(fromTimer) {
        function createXHRResponce() {
             return function statusResponce(unUsedObj, statusUrl, token) {
                var response = new XMLHttpRequest();
                response.open("POST", statusUrl, true);
                response.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                response.setRequestHeader("X-CSRFToken", token);
                response.responseType = 'json';
                response.onload = function (event) {
                    var thisEvent = this || event.currentTarget;
                    if (thisEvent.readyState == 4) {
                        if (thisEvent.status >= 200) {
                            if (thisEvent.response.status == "out") {
                                window.location = doc.location.origin + "/login/";
                            }
                        }
                    }
                };
                response.send(null);
            };
        };
        var w = win;
        // Тут можно установить явно null для режима отладки без websocket, используя ajax
        var userSock = w.WebSocket ? new WebSocket("ws://" + w.location.host + "/sock") : null;
        if (userSock) {
            userSock.onclose = function(event) {
                function reconnect() {
                    setTimeout(function () {
                        userSock = sockReAddListener(true);
                    }, 3000);
                    if (sockTryReconnect == false) {
                        var w = window;
                        var sockErrorHandler = w.sockErrorInit || null;
                        sockErrorHandler ? sockErrorHandler() : 0;
                        sockTryReconnect = true;
                    }
                };
                !event.wasClean ? reconnect() : 0;
            };
            userSock.onopen = function(event) {
                if (sockTryReconnect == true || typeof (fromTimer) == "undefined") {
                    var localSockErrorHandler = w.sockErrorClose || null;
                    // Возврат прежнего состояния после того, как была ошибка подключения
                    localSockErrorHandler ? localSockErrorHandler(event) : 0;
                    sockTryReconnect = false;
                }
                if (typeof (fromTimer) == "undefined") {
                    var cookiesObject = cookiesObj;
                    if (cookiesObject.ws) {
                        setInterval(sendData, 599995, {
                            'type': 'freshOnline',
                            'long': !cookiesObject.js
                        });
                    } else {
                        var xhrFunc = createXHRResponce();
                        if (cookiesObject.js) {
                            setInterval(xhrFunc, 18000, null, checkOnlineUrl, csrf);
                        } else {
                            var token = csrf;
                            xhrFunc(null, checkOnlineFirstUrl, token);
                            setInterval(xhrFunc, 18000, null, checkOnlineUrl, token);
                        }
                    }
                }
            };
            userSock.onmessage = w.mainSockListener || w.defaultListener;
        } else {
            var xhrFunc = createXHRResponce();
            !cookiesObj.js ? xhrFunc(null, checkOnlineFirstUrl, csrf) : 0;
            setInterval(xhrFunc, 18000, null, checkOnlineUrl, csrf);
        }
        return userSock;
    };
    userSock = sockReAddListeners();
    function initStatusCheckers(onlineUrl, onlineFirstUrl, activeUrl, onlineStatusMessageObj, token, socket, XHRRequestType) {
        /* XHRRequestType - специальный параметр, определит явное назначение типа запросов: true - XHR; false - WS */
        var cookieString = doc.cookie;
        if (cookieString.indexOf("ws=True") == -1) {
            statusSenderFunc = function (statusUrl, token) {
                var response = new XMLHttpRequest();
                response.open("POST", statusUrl, true);
                response.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                response.setRequestHeader("X-CSRFToken", token);
                response.responseType = 'json';
                response.onload = function (event) {
                    var thisEvent = this || event.currentTarget;
                    if (thisEvent.readyState == 4) {
                        if (thisEvent.status >= 200) {
                            if (thisEvent.response.status == "out") {
                                window.location = doc.location.origin + "/login/";
                            }
                        }
                    }
                };
                response.send(null);
            };
            var timer = accurateTimer(function () { checkOnlineStatus(onlineUrl, true, token); }, true, 18000);
            checkOnlineStatus((cookieString.indexOf("js=True") == -1 ? onlineFirstUrl : onlineUrl), null, token);
        } else {
            statusSenderFunc = function (socket, dataObj) {
                sendData(socket, dataObj);
            };
            var timer = accurateTimer(callback, breakCallback, time);
            checkOnlineStatus(userSock, onlineStatusMessageObj);
        }
        return statusSenderFunc;
    };

    var updates = doc.getElementById("user-notifications") || null;
    if (updates) {
        var w = window;
        var spanCounter = updates.firstElementChild;
        spanCounter.firstElementChild.setAttribute("title", notifyMainTextObject.getList);
        var small = doc.createElement("SMALL");
        small.className = "remove-all";
        var a = doc.createElement("A");
        a.setAttribute("href", removeAllUpdatesUrl);
        a.innerText = notifyMainTextObject.hideAll;
        small.appendChild(a);
        updates.appendChild(small);
        var screen = w.screen;
        var htmlFragmentsFactory = w.htmlFragmentsFactory;
        var center = htmlFragmentsFactory.centerX;
        center.call(htmlFragmentsFactory, screen, spanCounter);
        center.call(htmlFragmentsFactory, w.screen, updates.lastElementChild);
        if (doc.addEventListener) {
            updates.addEventListener("click", fullNotifyHandler, true);
        } else {
            updates.attachEvent("click", fullNotifyHandler, true);
        }
    }
    var time = 40000;
    var timeCounter = time;
    var canSend = false;
    setTimeout(function () {
        setInterval(function () {
            if (timeCounter == 0) {
                timeCounter = time;
                canSend = true;
            } else {
                timeCounter -= 1000;
            }
        }, 1000);
    }, time);
    function checkChatStatus(event) {
        function sendActiveStatusXHR(url, time) {
            var response = new XMLHttpRequest();
            response.open("POST", url, true);
            response.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            response.setRequestHeader("X-CSRFToken", csrf);
            response.onloadend = function (event) {
                timeCounter = time;
            };
            response.send(null);
        };
        if (canSend == true) {
            sendActiveStatusXHR(checkActiveUrl, time);
            canSend = false;
        }
    };
    if (doc.addEventListener) {
        doc.addEventListener("mousemove", checkChatStatus, false);
        doc.addEventListener("keyup", checkChatStatus, false);
    } else {
        doc.attachEvent("mousemove", checkChatStatus, false);
        doc.attachEvent("keyup", checkChatStatus, false);
    }

    htmlFragmentsFactory = {
        doReloadIfCurrentLink: function(parsedData) {
            var currentPath = doc.location.pathname;
            currentPath == parsedData.link ? localNavigation(null, currentPath) : 0;
        },
        messagePersonal: function(parsedData) {
            var d = doc;
            var event = d.createElement("LI");
            event.className = "message";
            event.setAttribute('value', parsedData.id);
            var subject = d.createElement("STRONG");
            var link = d.createElement("A");
            var body = d.createElement("SPAN");
            var bodyA = d.createElement("A");
            link.href = parsedData.profileLink;
            link.innerText = parsedData.username;
            subject.appendChild(link);
            subject.innerHTML += ":";
            bodyA.href = parsedData.dialogLink;
            bodyA.innerText = parsedData.body;
            body.appendChild(bodyA);
            event.appendChild(subject);
            event.appendChild(body);
            return event;
        },
        messageMany: function(parsedData) {
            var d = doc;
            var li = d.createElement("LI");
            li.className = parsedData.value;
            var text = d.createElement("P");
            var count = parsedData.count;
            var textObject = notifyTextObject;
            var textContentObj = textObject.messageMany;
            var countSpec = count > 5 ? 'more' : count > 1 ? 5 : 1;
            text.innerHTML = "<a href = '" + parsedData.profileLink + "'>" + parsedData.username + "</a> " + textContentObj[0] +
                " <a href = '" + parsedData.dialogLink + "' title = '"+ textContentObj.dialogText + "' class = 'countreble'>" + count + " " +
                textObject['messagePersonalCountText'][countSpec] + "</a>";
            li.appendChild(text);
            return li;
        },
        addFriend: function(parsedData) {
            var d = doc;
            var textObject = notifyTextObject.addFriend;
            var form = d.createElement("FORM");
            var input3 = d.createElement("INPUT");
            form.setAttribute("class", parsedData.class);
            input3.setAttribute("type", "submit");
            input3.setAttribute("formaction", parsedData.addFriendLink);
            input3.setAttribute("value", textObject.addFriendText);
            input3.setAttribute("class", "important");
            form.appendChild(input3);
            return form;
        },
        friendRequest: function(parsedData) {
            var d = doc;
            var textObject = notifyTextObject[parsedData.method];
            var form = d.createElement("FORM");
            var text = d.createElement("P");
            var input3 = d.createElement("INPUT");
            parsedData.hasOwnProperty('class') ? form.setAttribute("class", parsedData.class) : 0;
            form.setAttribute("method", "post");
            text.innerText = textObject.text;
            input3.setAttribute("type", "submit");
            input3.setAttribute("formaction", parsedData.cancelFriendLink);
            input3.setAttribute("value", textObject.cancelFriend);
            input3.setAttribute("class", 'nobutton');
            form.appendChild(text);
            form.appendChild(input3);
            return form;
        },
       friendRequestToMe: function(parsedData) {
            var d = doc;
            var form = d.createElement("FORM");
            var text = d.createElement("P");
            var input3 = d.createElement("INPUT");
            var input4 = d.createElement("INPUT");
            form.setAttribute("class", parsedData.class);
            form.setAttribute("method", "post");
            text.innerText = parsedData.text;
            input3.setAttribute("type", "submit");
            input3.setAttribute("formaction", parsedData.addFriendLink);
            input3.setAttribute("value", parsedData.addFriendText);
            input3.setAttribute("class", parsedData.addFriendClass);
            input4.setAttribute("type", "submit");
            input4.setAttribute("formaction", parsedData.rejectFriendLink);
            input4.setAttribute("value", parsedData.rejectFriendText);
            form.appendChild(text);
            form.appendChild(input3);
            form.appendChild(input4);
            return form;
        },
        friendRequestToMeNotify: function(parsedData) {
            var d = doc;
            var textObject = notifyTextObject.friendRequestToMeNotify;
            var form = d.createElement("FORM");
            var text = d.createElement("P");
            var link = d.createElement("A");
            link.setAttribute("href", parsedData.profileLink);
            link.innerText = parsedData.username;
            var input2 = d.createElement("INPUT");
            var input3 = d.createElement("INPUT");
            form.setAttribute("method", "post");
            text.appendChild(link);
            text.innerHTML += (" " + textObject.text);
            input2.setAttribute("type", "submit");
            input2.setAttribute("formaction", parsedData.confirmLink);
            input2.setAttribute("value", textObject.confirm);
            input2.setAttribute("class", 'important');
            input3.setAttribute("type", "submit");
            input3.setAttribute("formaction", parsedData.rejectLink);
            input3.setAttribute("value", textObject.reject);
            form.appendChild(text);
            form.appendChild(input2);
            form.appendChild(input3);
            return form;
        },
        simpleNotify: function(parsedData) {
            var d = doc;
            var textObject = notifyTextObject[parsedData.textKeyName];
            var p = d.createElement("P");
            var link = d.createElement("A");
            link.setAttribute("href", parsedData.profileLink);
            link.innerText = parsedData.username;
            p.appendChild(link);
            p.innerHTML += (" " + textObject.text);
            return p;
        },
        removeFriend: function(parsedData) {
            var d = doc;
            var textObject = notifyTextObject.removeFriend;
            var form = d.createElement("FORM");
            var input1 = d.createElement("INPUT");
            var text = d.createElement("P");
            text.innerText = textObject.text;
            form.setAttribute("class", parsedData.class);
            input1.setAttribute("type", "submit");
            input1.setAttribute("formaction", parsedData.removeFriendLink);
            input1.setAttribute("value", textObject.removeText);
            input1.setAttribute("class", "nobutton");
            form.appendChild(text);
            form.appendChild(input1);
            return form;
        },
        generateFullNotifyPropertiesElements: function(d, notifyTextData, forgetLink) {
            var closeButton = d.createElement("SMALL");
            var link = d.createElement("A");
            link.innerText = notifyTextData.hide;
            link.setAttribute("href", forgetLink);
            closeButton.className = 'forget';
            closeButton.appendChild(link);
            return [closeButton];
        },
        removeNotify: function() {
            var d = doc;
            var fullNotify = d.getElementById('user-notifications') || null;
            var shortNoty = d.getElementById('notiList') || null;
            fullNotify ? fullNotify.parentNode.removeChild(fullNotify) : 0;
            shortNoty ? shortNoty.parentNode.removeChild(shortNoty) : 0;
        },
        removeNotifyEvent: function (parsedData, parsedDataSubMethod) {
            function remove(notify, ul, element) {
                if (ul.childElementCount > 1) {
                    var stab = element.nextElementSibling || element.previousElementSibling || null;
                    stab && stab.className == "stab" ? ul.removeChild(stab) : 0;
                    ul.removeChild(element);
                } else {
                    notify.parentNode.removeChild(notify);
                }
            };
            var d = doc;
            var fullNotify = d.getElementById('user-notifications') || null;
            if (fullNotify) {
                var base = fullNotify.firstElementChild;
                var baseCounter = base.firstElementChild;
                var value = parsedDataSubMethod ? parsedDataSubMethod.reduceBaseValueBy : parsedData.reduceBaseValueBy || 0;
                if (value > 0) {
                    mainBaseAnimation(base, baseCounter, true, value, false);
                    multiBaseAnimation(baseCounter, true, 1, false);
                }
                var itemClass = parsedDataSubMethod ? parsedDataSubMethod.value : parsedData.value || null;
                if (itemClass) {
                    if (fullNotify.className == "opened") {
                        var fullNotifyUl = fullNotify.lastElementChild;
                        var li = fullNotifyUl.getElementsByClassName(itemClass);
                        var li = li.length > 0 ? li[0] : null;
                        if (li) {
                            var target = li.getElementsByClassName('countreble');
                            if (target.length > 0) {
                                var target = target[0];
                                var time = value * 140;
                                setTimeout(function() {
                                    remove(fullNotify, fullNotifyUl, li);
                                }, time);
                                var method = parsedData.originalMethod || parsedData.method;
                                var countrebleTextObject = notifyTextObject[method + 'CountText'];
                                notifyItemAnimation(target, true, value, false, countrebleTextObject);
                            } else {
                                remove(fullNotify, fullNotifyUl, li);
                            }
                        }
                    }
                }
            }
            var shortNoty = d.getElementById('notiList') || null;
            if (shortNoty) {
                if (shortNoty.childElementCount > 1) {
                    var li1 = shortNoty.getElementsByClassName(itemClass);
                    var li1 = li1.length > 0 ? li1[0] : null;
                    if (li1) {
                        shortNoty.removeChild(li1);
                    }
                } else {
                    shortNoty.parentNode.removeChild(shortNoty);
                }
            }
        },
        stateOnline: function(parsedData) {
            var statusPlace = doc.getElementsByClassName(parsedData.class);
            if (statusPlace.length > 0) {
                var valueStr = parsedData.status;
                var value = valueStr.split("/");
                var time = new Date(value);
                statusPlace[0].firstElementChild.innerText = time.getUTCDay();
            }
        },
        decrementNotifyValue: function (parsedData, parsedDataSubMethod) {
            var value = parsedDataSubMethod ? parsedDataSubMethod.reduceBaseValueBy : parsedData.reduceBaseValueBy;
            if (value > 0) {
                var fullNotify = doc.getElementById('user-notifications');
                var shortNotify = null;
                if (fullNotify) {
                    var base = fullNotify.firstElementChild;
                    var baseCounter = base.firstElementChild;
                    if (fullNotify.className == "opened") {
                        var target = fullNotify.lastElementChild.getElementsByClassName(parsedDataSubMethod ? parsedDataSubMethod.value : parsedData.value)[0];
                        var target = target.getElementsByClassName('countreble')[0];
                        var countrebleTextObject = notifyTextObject[(parsedData.originalMethod || parsedData.method) + 'CountText'];
                        notifyItemAnimation(target, true, value, false, countrebleTextObject);
                    }
                    mainBaseAnimation(base, baseCounter, true, value, false);
                }
            }
        },
        incrementNotifyValue: function (parsedData, parsedDataSubMethod) {
            var fullNotify = doc.getElementById('user-notifications');
            if (fullNotify && parsedDataSubMethod) {
                var itemClass = parsedDataSubMethod.class || null;
                var baseValue = parsedDataSubMethod.baseValue || null;
                var multiValue = parsedDataSubMethod.multiValue || null;
                if (fullNotify.className == "opened" && itemClass) {
                    var item = fullNotify.lastElementChild.getElementsByClassName(parsedDataSubMethod.class);
                    if (item.length > 0) {
                        var item = item[0];
                        var target = item.getElementsByClassName('countreble')[0];
                        var countrebleTextObject = notifyTextObject[parsedData.method + 'CountText'];
                        notifyItemAnimation(target, false, baseValue, false, countrebleTextObject);
                    }
                }
                var base = fullNotify.firstElementChild;
                var baseCounter = base.firstElementChild;
                baseValue ? mainBaseAnimation(base, baseCounter, false, baseValue, false) : 0;
                multiValue ? multiBaseAnimation(baseCounter, false, multiValue, false) : 0;
            }
        },
        centerX: function(screen, target) {
            target.style.left = ((screen.width - target.offsetWidth) / 2) + "px";
        },
        acceptListernUserStatus: function (data) {
            /* Синхронизация объекта "usersOnListern", 
             * а затем вызов локальной функции "statusInitial", если она существует в текущем модуле. */
            var newOnlineUsers = data.online || [];
            var newActiveUsers = data.active || [];
            var errorObjectArray = data.failure || {};
            var users = usersOnListern;
            var connectArr = users.connect;
            if (newOnlineUsers.length) {
                users.online.update(newOnlineUsers);;
                var connectArr = connectArr.difference(newOnlineUsers);
            }
            if (newActiveUsers.length) {
                users.active.update(newActiveUsers);
                var connectArr = connectArr.difference(newActiveUsers);
            }
            users.connect = connectArr;
            var f = window.statusInitial || null;
            f ? f(newOnlineUsers, newActiveUsers, errorObjectArray.online || [], errorObjectArray.active || []) : 0;
            usersOnListern = users;
        },
        disconnectUserStatus: function(data) {
            var onlineUsers = data.online || null;
            var activeUsers = data.active || null;
            var errorObj = data.failure || null;
            if (onlineUsers || activeUsers || errorObj) {
                var users = usersOnListern;
                if (onlineUsers) {
                    users.online.discard(onlineUsers);
                    users.disconnect = users.disconnect.difference(onlineUsers);
                }
                if (activeUsers) {
                    users.active.discard(activeUsers);
                    users.disconnect = users.disconnect.difference(activeUsers);
                }
                if (errorObj) {
                    var onlineError = errorObj.online || null;
                    var activeError = errorObj.active || null;
                    onlineError ? users.disconnect.update(onlineError) : 0;
                    activeError ? users.disconnect.update(activeError) : 0;
                }
                usersOnListern = users;
            }
        }
    };
    defaultListener = function(event) { // 3 Варианта вхождения сюда event (sock --> event.data; hmlHttpRequest --> event.target.response; готовый распарсенный объект)
        var data = event.target || false ? event.target.response || false ? JSON.parse(this.response.data || event.target.response.data) : JSON.parse(event.data) : event;
        if (data.nodeName) {
            return;
        }
        data.constructor === Array ? 0 : data = [data];
        for (var i = data.length; i--;) {
            var arg = null;
            var curData = data[i];
            var curEventName = curData.method;
            var defineMethod = htmlFragmentsFactory[curEventName] || null;
            if (defineMethod) {
                var element = defineMethod.call(htmlFragmentsFactory, curData, arg) || null;
                var type = curData.eventType || null;
                if (type && element) {
                    var d = document;
                    if (type == 'fragment') { // Интерактивные элементы - замена / смена состояния
                        d.location.pathname == curData.path ? embedAction(d, element) : 0;
                    } else if (type == 'notify') { // Оповещение
                        var classForLi = curData.value || null;
                        var subMethodData = curData.notifyCounterChange || curData[userId] || null;
                        if (subMethodData && subMethodData.hasOwnProperty('multiValue')) {
                            var fullNotify = d.getElementById('user-notifications') || null;
                            if (fullNotify && fullNotify.className == 'opened') {
                                var subMethodName = curEventName + "Many";
                                if (htmlFragmentsFactory.hasOwnProperty(subMethodName)) {
                                    var fullNotifyElement = htmlFragmentsFactory[subMethodName].call(htmlFragmentsFactory, curData);
                                    insertFullNoti(d, fullNotify, fullNotifyElement, subMethodName, curData, classForLi);
                                } else if (htmlFragmentsFactory.hasOwnProperty(curEventName)) {
                                    var fullNotifyElement = htmlFragmentsFactory[curEventName].call(htmlFragmentsFactory, curData);
                                    insertFullNoti(d, fullNotify, fullNotifyElement, curEventName, curData, classForLi);
                                }
                            }
                        }
                        var subMethod = subMethodData ? htmlFragmentsFactory[subMethodData.method] || null : null;
                        subMethod ? subMethod.call(htmlFragmentsFactory, curData, subMethodData) : 0;
                        insertNoti(element, classForLi);
                    }
                }
            } else {
                console.log("Не удалось найти метод в объекте --> htmlFragmentsFactory", "Метод -->  " + curEventName);
            }
        }
    };
    return userSock;
};
var userSock = initRealtime(htmlFragmentsFactory, csrf);
function embedAction(d, element) {
    var findFragment = d.getElementById("middleP").getElementsByClassName(element.className)[0] || null;
    if (findFragment) {
        var parent = findFragment.parentNode;
        var fragment = parent.insertBefore(element, findFragment);
        parent.removeChild(findFragment);
    }
};
function insertNoti(element, classForLi) {
    var d = document;
    if (element.nodeName != "LI") {
        var li = d.createElement("LI");
        li.appendChild(element);
    } else {
        var li = element;
    }
    classForLi ? li.className = classForLi : 0;
        var shortNotifyPlace = d.getElementById("notiList") || null;
        if (!shortNotifyPlace) {
            var shortNotifyPlace = d.createElement("UL");
            shortNotifyPlace.id = "notiList";
            var top = d.getElementById("aPage");
            top.parentNode.insertBefore(shortNotifyPlace, top);
            if (d.addEventListener) {
                shortNotifyPlace.addEventListener('click', shortNotifyHandler, false);
            } else {
                shortNotifyPlace.attachEvent('click', shortNotifyHandler, false);
            }
        }
        shortNotifyPlace.appendChild(li);
};
function insertFullNoti(d, fullNotify, fullNotifyElement, method, parsedData, liClass) {
    if (fullNotifyElement.nodeName != "LI") {
        var li = d.createElement("LI");
        li.appendChild(fullNotifyElement);
        li.className = liClass;
        var fullNotifyElement = li;
    }
    var textObject = notifyTextObject.notifyItemProperties;
    var properties = htmlFragmentsFactory.generateFullNotifyPropertiesElements(d, textObject, parsedData.forgetLink);
    var liTop = fullNotifyElement.firstElementChild;
    for (var i = 0; i < properties.length; i++) {
        fullNotifyElement.insertBefore(properties[i], liTop);
    }
    var notifyUl = fullNotify.nodeName != "UL" ? fullNotify.lastElementChild : fullNotify;
    if (notifyUl.childElementCount >= 1) {
        var stab = d.createElement("LI");
        var stabP = d.createElement("P");
        stab.className = "stab";
        stabP.innerText = "+";
        stab.appendChild(stabP);
        notifyUl.appendChild(stab);
    }
    notifyUl.appendChild(fullNotifyElement);
};
function loadUpdates(fullNotifyPlace, link) {
    var request = new XMLHttpRequest();
    request.open("GET", link, true);
    request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    request.responseType = 'json';
    (function (fullNotifyPlace) {
        request.onloadend = function (event) {
            var events = this || event.target;
            if (events) {
                var updates = JSON.parse(events.response.updates);
                var d = document;
                var notiUl = d.createElement("UL");
                var updatesCount = updates.length;
                for (var i = 0; i < updatesCount; i++) {
                    var event = updates[i];
                    var method = event.method;
                    var liClass = event.value;
                    var defineMethod = htmlFragmentsFactory[method];
                    var element = defineMethod.call(htmlFragmentsFactory, event);
                    insertFullNoti(d, notiUl, element, method, event, liClass);
                }
                if (notiUl.addEventListener) {
                    notiUl.addEventListener('mouseover', fullNotifyPropertiesShowHandler, false);
                    notiUl.addEventListener('mouseout', fullNotifyPropertiesHideHandler, false);
                } else {
                    notiUl.attachEvent('mouseover', fullNotifyPropertiesShowHideHandler, false);
                    notiUl.attachEvent('mouseout', fullNotifyPropertiesHideHandler, false);
                }
                var counterA = fullNotifyPlace.firstElementChild;
                counterA.className = 'updates-loader full';
                counterA.style.bottom = "25%";
                fullNotifyPlace.className = 'opened';
                fullNotifyPlace.appendChild(notiUl);
                counterA.firstElementChild.title = window.notifyTextObject.notifyItemProperties.closeList;
            } else {
                fullNotifyPlace.parentNode.removeChild(fullNotifyPlace);
            }
        };
    })(fullNotifyPlace);
    request.send();
};
function fullNotifyHandler(event) {
    var target = event.target;
    var targetNodeName = target.nodeName;
    if (targetNodeName == "A") {
        var fullNotifyPlace = this || event.currentTarget;
        var parent = target.parentNode;
        var parentName = parent.nodeName;
        var parentClass = parent.className;
        var path = target.href;
        if (parentName == "SPAN") {
            if (parentClass == "updates-loader") {
                loadUpdates(fullNotifyPlace, path);
            } else if (parentClass == "updates-loader full") {
                fullNotifyPlace.className = "";
                target.title = window.notifyTextObject.notifyItemProperties.getList;
                var span = fullNotifyPlace.firstElementChild;
                var updates = fullNotifyPlace.getElementsByTagName("UL")[0];
                fullNotifyPlace.removeChild(updates);
                span.className = "updates-loader";
                span.style.bottom = "";
            }
        } else if (parentName == "SMALL") {
            if (parentClass == "remove-all") {
                doPostFormSimplifield(document, path, fullNotifyPlace, 'rmNotify');
            } else {
                var item = searchP(target, "LI");
                doPostFormSimplifield(document, path, item, 'rmNotifyItem');
            }

        } else if (parentName == "P") {
            globalNavigation(null, path);
        }
    } else {
        if (target.hasAttribute("type") && target.type == "submit") {
            var path = target.hasAttribute("formAction") ? target.formAction : target.parentNode.action;
            var item = searchP(target, "LI");
            doPostFormSimplifield(document, path, item, 'rmNotifyItem');
        }
    }
    event.preventDefault();
};
function fullNotifyPropertiesShowHandler(event) {
    var eventHolder = this || event.currentTarget;
    var target = event.target;
    if (target != eventHolder) {
        var li = searchP(target, "LI") || target;
        var liClass = li.className;
        liClass != 'stab' && liClass.indexOf(' ') == -1 ? li.className = liClass + " show" : 0;
    }
};
function fullNotifyPropertiesHideHandler(event) {
    var target = event.target;
    var relTar = event.relatedTarget;
    var li = searchP(target, "LI") || target;
    var relLi = searchP(relTar, "LI");
    if (li && li != relLi) {
        var liClass = li.className;
        var i = liClass.indexOf(' ');
        li.className = liClass.slice(0, i == -1 ? liClass.length : i);
    }
};
function shortNotifyHandler(event) {
    var thisEvent = event.target;
    var tarNodeName = thisEvent.nodeName;
    if (tarNodeName == "A") {
        var href = thisEvent.href;
        var parent = thisEvent.parentNode;
        var parentNodeName = parent.nodeName;
        if (parentNodeName == "STRONG" || parentNodeName == "SPAN") {
            globalNavigation(null, href);
        }
    } else {
        // small 
    }
    return event.preventDefault();
};
/* Animations
type = true - уменьшение, false - наращивание
absoluteValue - Довести до значения value, иначе value значит изменить текущее значение НА +- value
*/

function mainBaseAnimation(base, target, type, value, absoluteValue) {
    var text = target.innerText;
    var counter = parseInt(text.slice(0, text.indexOf('.')));
    var newBaseValue = absoluteValue ? value : type ? counter - value : counter + value;
    subBaseAnimation(base, target, value * 2);
    var decr = setInterval(function() {
        if (type ? counter > newBaseValue : counter < newBaseValue) {
            type ? counter-- : counter++;
            var text = target.innerText;
            target.innerText = counter + text.slice(text.indexOf('.'), text.length);
        } else {
            clearInterval(decr);
            decr = null;
        }
    }, 140);
};
function notifyItemAnimation(target, type, value, absoluteValue, countrebleText) {
    var text = target.innerText;
    var ct = Object.keys(countrebleText);
    var ctl = Object.keys(countrebleText).length;
    var counter = text.match(/[0-9]+/i)[0];
    var counter = parseInt(counter);
    var newValue = absoluteValue ? value : type ? counter - value : counter + value;
    var decr = setInterval(function () {
        if (type ? newValue < counter : newValue > counter) {
            var text = target.innerText;
            var pos0, pos1 = null;
            for (var i = 0; i < ctl; i++) {
                var el = countrebleText[ct[i]];
                var pos = text.indexOf(el);
                if (pos != -1 && !pos0) {
                    var pos0 = pos;
                    var pos1 = pos + el.length;
                }
            };
            type ? counter-- : counter++;
            if (counter == 1) {
                var subStr = countrebleText[1];
            } else if (counter < 5) {
                var subStr = countrebleText[5];
            } else {
                var subStr = countrebleText['more'];
            }
            var newText = text.slice(0, pos0) + subStr + text.slice(pos1, text.length);
            var newText = newText.replace(String(type ? counter + 1 : counter - 1), counter)
            target.innerText = newText;
        } else {
            clearInterval(decr);
            decr = null;
            stop = true;
        }
    }, 140);
};
function subBaseAnimation(base, target, steps) {
    var screen = window.screen;
    var decr = setInterval(function() {
        if (steps > 0) {
            var text = target.innerText;
            var value = String(Math.random()).slice(2, 5);
            target.innerText = text.slice(0, text.indexOf(".") + 1) + value + text.slice(text.indexOf(" x "), text.length);
            htmlFragmentsFactory.centerX(screen, base);
        } else {
            clearInterval(decr);
            decr = null;
            stop = true;
        }
        steps--;
    }, 70);
};
function multiBaseAnimation(target, type, value, absoluteValue) {
    var text = target.innerText;
    var counter = parseInt(text.slice(text.indexOf(' x ') + 3, text.length));
    var newValue = absoluteValue ? value : type ? counter - value : counter + value;
    var time = newValue * 140;
    var decr = setInterval(function () {
        var text = target.innerText;
        if ((type ? counter > newValue : counter < newValue) && counter > 0) {
            type ? counter-- : counter++;
            target.innerText = text.slice(0, text.indexOf(' x ') + 3) + counter;
        } else {
            counter <= 0 ? htmlFragmentsFactory.removeNotify() : 0;
            clearInterval(decr);
            decr = null;
        }
    }, time);
};