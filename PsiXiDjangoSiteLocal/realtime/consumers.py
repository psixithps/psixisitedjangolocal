import copy, json, logging, traceback, threading
from datetime import datetime
from threading import Timer
from django.core.cache import caches
from realtime.middleware import setUserLeftTime
from django.db.transaction import atomic, Error
from psixiSocial.models import mainUserProfile, THPSProfile, userProfilePrivacy
from psixiSocial.privacyIdentifier import checkPrivacy
from psixiSocial.translators import singleElementGetter
from channels.generic.websocket import WebsocketConsumer, AsyncWebsocketConsumer, JsonWebsocketConsumer, AsyncJsonWebsocketConsumer
from realtime.consumerApps.personalMessages import personalMsg
from realtime.consumerApps.groupMessages import groupMsg
from realtime.consumerApps.userStatus import status
from asgiref.sync import async_to_sync
from channels.exceptions import DenyConnection, AcceptConnection, ChannelFull, MessageTooLarge, StopConsumer
from redis.exceptions import ConnectionError, RedisError


logger = logging.getLogger(__name__)

class mainConsumer(WebsocketConsumer, personalMsg, groupMsg, status):

	def connect(self):
		''' Решено использовать группы для пользователей, 
		чтобы решить проблему нескольких вкладок или параллельные сессии на других устройствах.'''
		try:
			self.accept()
		except AcceptConnection as er:
			logger.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
			return
		user = self.scope["user"]
		userId = str(user.id)
		try:
			async_to_sync(self.channel_layer.group_add)(userId, self.channel_name)
		except DenyConnection as er:
			logger.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
		except ChannelFull as er:
			logger.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
		except StopConsumer as er:
			logger.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
		except ConnectionError as er:
			logger.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
		except RedisError as er:
			logger.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
		self.userId = userId
		self.user = user
		personalMsg.__init__(self)
		groupMsg.__init__(self)
		status.__init__(self)
		groupDialogsList = self.connectToGroupDialogs()
		if isinstance(groupDialogsList, list):
			usersCahhelsCache = caches['usersChannelLayers']
			channelsSet = usersCahhelsCache.get(userId, None)
			if channelsSet is None:
				#try:
				usersCahhelsCache.set(userId, {self.channel_name,})
				#except pylibmc.Error:
						#logger.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
			else:
				channelsSet.add(self.channel_name)
				#try:
				usersCahhelsCache.set(userId, channelsSet)
				#except pylibmc.Error:
						#logger.log(logging.CRITICAL, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
			self.groupDialogs = groupDialogsList
		self.connectToStatus()

	def websocket_receive(self, message):
		try:
			message = json.loads(message["text"])
		except ValueError:
			pass
		messageType = type(message)
		if messageType is dict:
			methodName = message.get("type", None)
			if methodName is not None:
				method = getattr(self, methodName, None)
				if method is not None:
					method(message)
		elif messageType is list:
			if 0 < len(message) < 10:
				for incomingMessageDict in message:
					if isinstance(incomingMessageDict, dict):
						methodName = incomingMessageDict.get("type", None)
						if methodName is not None:
							method = getattr(self, methodName, None)
							if method is not None:
								method(incomingMessageDict)


	def sendToClient(self, msg):
		try:
			self.send(text_data = msg["data"]["item"])
		except MessageTooLarge as er:
			logger.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
		except ValueError as er:
			logger.log(logging.ERROR, "[{0}]\r\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))

	def disconnect(self, status):
		id = self.userId
		async_to_sync(self.channel_layer.group_discard)(id, self.channel_name)
		setUserLeftTime(id, datetime.now(), True, self.disconnectListernStatus)
		self.disconnectFromGroupDialogs()
		self.close()