import json, threading
from datetime import datetime
from django.conf import settings
from django.core.cache import caches
from django.http import JsonResponse
from realtime.middleware import setUserLeftTime, setMiddleStatus, ONLINE_THRESHOLD_SHORT, ACTIVE_TIME_SHORT
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

def checkUserStatus(request):
	if request.is_ajax():
		if request.method == "POST":
			requestUser = request.user
			if requestUser.is_authenticated:
				uid = str(requestUser.id)
				thread = threading.main_thread()
				onlineTimersDict = getattr(thread, "onlineTimers", {})
				lastOnlineTimer = onlineTimersDict.get(uid, None)
				lastOnlineTimer.cancel() if lastOnlineTimer is not None else None
				threshhold = ONLINE_THRESHOLD_SHORT
				try:
					onlineStatusCache = caches["onlineUsers"]
					isOnline = onlineStatusCache.touch(uid, threshhold)
				except ServerError as errorMsg:
					logg.log(logging.CRITICAL, "[{0}]\tКеш memcache - Ошибка сохранения\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
					return JsonResponse({"status": "error"})
				except memcachedError as errorMsg:
					logg.log(logging.CRITICAL, "[{0}]\tКеш memcache недоступен\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
					return JsonResponse({"status": "error"})
				if isOnline:
					pass
				else:
					try:
						onlineStatusCache.set(uid, threshhold)
					except ServerError as errorMsg:
						logg.log(logging.CRITICAL, "[{0}]\tКеш memcache - Ошибка сохранения\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
						return JsonResponse({"status": "error"})
					except memcachedError as errorMsg:
						logg.log(logging.CRITICAL, "[{0}]\tКеш memcache недоступен\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
						return JsonResponse({"status": "error"})

				onlineTimer = threading.Timer(float(threshhold) + 0.5, setUserLeftTime, [uid, datetime.now(), False, None])
				onlineTimer.start()
				onlineTimersDict.update({uid: onlineTimer})
				setattr(thread, "onlineTimers", onlineTimersDict) if hasattr(thread, "onlineTimers") is False else None
				return JsonResponse({"status": "in"})
			return JsonResponse({"status": "out"})

def updateChatStatusXHR(request):
	if request.is_ajax():
		if request.method == "POST":
			user = request.user
			if user.is_authenticated:
				id = str(user.id)
				thread = threading.main_thread()
				activeTimersDict = getattr(thread, "activeTimers", {})
				activeThreshhold = ACTIVE_TIME_SHORT
				try:
					activeStatusCache = caches["chatStatusActive"]
					isActive = activeStatusCache.touch(id, activeThreshhold)
				except ServerError as errorMsg:
					logg.log(logging.CRITICAL, "[{0}]\tКеш memcache - Ошибка сохранения\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
					return JsonResponse({"status": "error"})
				except memcachedError as errorMsg:
					logg.log(logging.CRITICAL, "[{0}]\tКеш memcache недоступен\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
					return JsonResponse({"status": "error"})
				if isActive is False:
					try:
						caches["chatStatusMiddle"].delete(id)
						caches["chatStatusActive"].set(id, activeThreshhold)
					except ServerError as errorMsg:
						logg.log(logging.CRITICAL, "[{0}]\tКеш memcache - Ошибка сохранения\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
						return JsonResponse({"status": "error"})
					except memcachedError as errorMsg:
						logg.log(logging.CRITICAL, "[{0}]\tКеш memcache недоступен\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
						return JsonResponse({"status": "error"})
					async_to_sync(get_channel_layer().group_send)("%s_act_stat" % id, {
						"type": "sendToClient",
						"data": {
							"item": json.dumps({
								'method': 'stateActive',
								'id': id
								})
							}
						})
				else:
					lastActiveTimer = activeTimersDict.get(id, None)
					lastActiveTimer.cancel() if lastActiveTimer is not None else None
				activeTimer = threading.Timer(float(activeThreshhold) + 0.5, setMiddleStatus, [id, activeThreshhold])
				activeTimer.start()
				activeTimersDict.update({id: activeTimer})
				setattr(thread, "activeTimers", activeTimersDict) if hasattr(thread, "activeTimers") is False else None
				return JsonResponse({"status": "in"})
			return JsonResponse({"status": "out"})

def checkUsersStatusFirst(request):
	''' Эта функция будет работать однажды, если у клиента нету кук: - "js" и "ws", то
		обнулить длительные таймеры и долгоживущий кеш, 
		ранее установленные в 'realtime.middleware.OnlineNowMiddleware'.'''
	if request.is_ajax():
		if request.method == "POST":
			requestUser = request.user
			if requestUser.is_authenticated:
				uid = str(requestUser.id)
				thread = threading.main_thread()
				onlineTimersDict = getattr(thread, "onlineTimers", {})
				activeTimersDict = getattr(thread, "activeTimers", {})
				lastOnlineTimer = onlineTimersDict.get(uid, None)
				lastActiveTimer = activeTimersDict.get(uid, None)
				lastOnlineTimer.cancel() if lastOnlineTimer is not None else None
				lastActiveTimer.cancel() if lastActiveTimer is not None else None
				onlineThreshhold = ONLINE_THRESHOLD_SHORT
				activeThreshhold = ACTIVE_TIME_SHORT
				try:
					caches["onlineUsers"].set(uid, onlineThreshhold)
					caches["chatStatusActive"].set(uid, activeThreshhold)
				except ServerError as errorMsg:
					logg.log(logging.CRITICAL, "[{0}]\tКеш memcache - Ошибка сохранения\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
					return JsonResponse({"status": "error"})
				except memcachedError as errorMsg:
					logg.log(logging.CRITICAL, "[{0}]\tКеш memcache недоступен\t{1}\r\t{2}\r-----\r".format(datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(errorMsg), traceback.format_exc()))
					return JsonResponse({"status": "error"})
				onlineTimer = threading.Timer(float(onlineThreshhold) + 0.5, setUserLeftTime, [uid, datetime.now(), False, None])
				activeTimer = threading.Timer(float(activeThreshhold) + 0.5, setMiddleStatus, [uid, activeThreshhold])
				onlineTimer.start()
				activeTimer.start()
				onlineTimersDict.update({uid: onlineTimer})
				activeTimersDict.update({uid: activeTimer})
				setattr(thread, "onlineTimers", onlineTimersDict) if hasattr(thread, "onlineTimers") is False else None
				setattr(thread, "activeTimers", activeTimersDict) if hasattr(thread, "activeTimers") is False else None
				return JsonResponse({"status": "in"})
			return JsonResponse({"status": "out"})