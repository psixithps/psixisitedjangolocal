# Репозиторий в формате решения Microsoft Visual Studio 2015 #

## Репозиторий создан для совместной разработки локальной версии проекта и предназначен для разработчиков из PsiXi - THPS ##

### Что нужно сделать (по порядку): ###

-----------------------------------------------------------------------------------------------------------------------------------------

1. Создать рабочее окружение: скачать необходимые компоненты:

**Опираясь на разрядность вашей ОС Windows, следует устанавливать все пакеты x64, или x32 соответственно**

* [Скачать Microsoft Visual Studio 2015 Community](https://www.visualstudio.com/ru-ru/products/visual-studio-community-vs.aspx)

* [Скачать Python 3.5](https://www.python.org/downloads/)

* [Скачать Python Tools for VS 2015](https://github.com/Microsoft/PTVS/releases/latest)

* [Скачать последний GIT](https://git-scm.com/downloads)

* [Скачать PostgreSQL==9.5 или 9.6 (не beta)](http://www.enterprisedb.com/products-services-training/pgdownload#windows)

* [Скачать OSGeo4W](https://trac.osgeo.org/osgeo4w/)

* [Скачать последний win-psycopg для Python 3.5](http://www.stickpeople.com/projects/python/win-psycopg/)

* [Скачать Twisted==18.7.0 для Python 3.5](https://www.lfd.uci.edu/~gohlke/pythonlibs/#twisted)

* [Скачать NumPY==1.15.0 для Python 3.5](https://www.lfd.uci.edu/~gohlke/pythonlibs/#numpy)

* [Скачать GDAL==2.2.4 для Python 3.5](https://www.lfd.uci.edu/~gohlke/pythonlibs/#gdal)

---------------------------------------------------------------------------------------------------------------------------------------

2. Настроить рабочее окружение: установить необходимые компоненты:

* Запустить установщик Python 3.5 и установить

![python.PNG](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/397940096-python.PNG)

*После установки перезагрузиться*

-------------------------------

* Запустить установщик GIT и установить

![git.PNG](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/4c7e55ace51100d3094a95b66ad5764b0d9cbfd0/README%20Images/3889273269-git.PNG)

*После установки перезагрузиться*

--------------------------------

* Запустить установщик Visual Studio, отметить следующие чекбоксы

*** Предпочтительнее установка на системный диск; ещё лучше, если операционная система свежая ***

![Безымянный.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/1559113182-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B9.png)

![visualstudio1.PNG](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/3923568917-visualstudio1.PNG)

![visualstudio3.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/3083589094-visualstudio3.png)

*После установки перезагрузиться*

---------------------------------

* Если в списке не было Python Tools for Visual Studio, то запустить скачанный ранее установщик Python Tools for VS 2015 и установить отдельно

![ptvs.PNG](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/2461411667-ptvs.PNG)

*После установки перезагрузиться*

---------------------------------

* PostgreSQL

**На windows 8 или 10 нужо предварительно создатать пользователя-администратора с именем postgres, пароль которого должен будет фигурировать и при дальнейшей установке postgreSQL! 
А все дальнейшие действия по установке postgreSQL должны быть произведены из под учётной записи postgres, кроме создания базы данных в pgAdmin.**

![win8-10-note.PNG](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/9a942819935cd42fde063256697837f6f665f827/README%20Images/win8-10-note.PNG)

----------------------------------

* Запустить установщик PostgreSQL и установить

![postgres.PNG](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/3876632652-postgres.PNG)

![postgres1.PNG](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/903848087-postgres1.PNG)

----------------------------------

После установки будет предложено запустить Application Stack Builder, нужно сделать это, выбрав чекбокс

![postgres2.PNG](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/2439163976-postgres2.PNG)

Следующий

![stackbuild.PNG](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/1402727120-stackbuild.PNG)

Выбрать следующие пакеты

![stackbuild1.PNG](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/1879061075-stackbuild1.PNG)

![PostGIS0.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/2874366755-PostGIS0.png)

Выбрать последнюю версию базы данных (не beta)

![stackbuild2.PNG](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/3553505861-stackbuild2.PNG)

Терпеливо ждать инициализаций выбранных драйверов и компонентов БД, считать их по количеству, пока не установятся все выбранные

![stackbuild3.PNG](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/555617257-stackbuild3.PNG)

В процессе установки **pgAdmin** надо создать системную учётную запись **postgres** (эта учётная запись нужна только для доступа к созданию и удалению новых пользователей или баз данных)

![pgagent.PNG](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/3399006980-pgagent.PNG)

Пакет **postGIS** предоставит свои окна с настройками:

![PostGIS.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/2f242d1c77c854ffef27171cc637d76dfb83f3b4/README%20Images/111147034-PostGIS.png)

* Указать свои данные от главного пользователя **postgres**

![PostGIS_1.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/4273488024-PostGIS_1.png)

![PostGIS1.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/1041650875-PostGIS1.png)

![PostGIS2.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/2090685426-PostGIS2.png)

![PostGIS3.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/1371752677-PostGIS3.png)

![PostGIS31.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/1003610709-PostGIS31.png)

Убедиться в успехе

![postgres3.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/184160978-postgres3.png)

*После установки перезагрузиться*

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

3. Настроить рабочее окружение под проект: настройка PostgreSQL, создание виртуального окружения python, клонирование проекта и пробный запуск

Вместе с PostgreSQL должна была установиться утилита pgAdmin, которую нужно запустить и сделать следующее ниже представленное

* pgAdmin

* Ввести пароль от главного пользователя postgres для входа

![setbd.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/3438955828-setbd.png)

* Создать пользователя

![setbd1.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/4161163995-setbd1.png)

* Указать имя: **fildblocal**

![setbd2.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/3158829061-setbd2.png)

* Задать пароль: **psixilocal**

![setbd3.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/3906305141-setbd3.png)

----------------------------------

**Если был установлен pgAdmin версии 4, то нужно в свойствах пользователя включить** ***can login***.

![pgadmin4.PNG](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/c975637a52bd7743bd4b5788c03623cc7a00f18d/README%20Images/pgadmin4.PNG)

![pgadmin41.PNG](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/c975637a52bd7743bd4b5788c03623cc7a00f18d/README%20Images/pgadmin41.PNG)

----------------------------------

* Создать базу данных с именем: **psixidblocal**

![setbd4.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/4221290435-setbd4.png)

* Сделать хозяином базы данных **psixidblocal** пользователя **fildblocal**

![111.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/1898197630-111.png)

* Установить во вкладке **Определение** шаблон **template0**

![112.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/913962036-112.png)  

* Создать расширение **postgis**

![113.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/3420166335-113.png)

![114.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/1228236607-114.png)

* Создать расширение **postgis_topology**

![Безымянный3.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/2546826902-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B93.png)

![Безымянный4.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/416096882-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B94.png)

Убедиться в правильности ввода, исходя из данных в окне свойств

![Безымянный5.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/224953348-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B95.png)

* Закрыть pgAdmin

------------------------------------

4. OSGeo4W

![osgeowin.png](https://bitbucket.org/psixithps/psixisitedjangolocal/raw/eb8ce7818ca16adb979b0b57a36fc02ff7b5d9e8/README%20Images/osgeowin.png)

![osgeowin2.png](https://bitbucket.org/psixithps/psixisitedjangolocal/raw/eb8ce7818ca16adb979b0b57a36fc02ff7b5d9e8/README%20Images/osgeowin2.png)

![osgeowin3.png](https://bitbucket.org/psixithps/psixisitedjangolocal/raw/eb8ce7818ca16adb979b0b57a36fc02ff7b5d9e8/README%20Images/osgeowin3.png)

![osgeowin4.png](https://bitbucket.org/psixithps/psixisitedjangolocal/raw/eb8ce7818ca16adb979b0b57a36fc02ff7b5d9e8/README%20Images/osgeowin4.png)


*После установки перезагрузиться*

------------------------------------

* Запустить Visual Studio

* Открыть "Управление подключениями"

![vs.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/877493257-vs.png)

* Нажать на "Клонировать", указать адрес: https://логин_на_bitbucket@bitbucket.org/psixithps/psixisitedjangolocal , задать имя папки репозитория

![vs1.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/1175664491-vs1.png)

* После клонирования, открыть репозиторий

![vs2.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/1404443611-vs2.png)

* После открытия репозитория, открыть решение

![vs3.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/3274284101-vs3.png)

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

5. Настройка проекта: создание и настройка виртуального пространства, настройка пакетов

*  Создать своё виртуальное пространство для Python: указать системные пути - **используйте кнопку Auto Detect (если доступна)**, выбрать произвольное название и путь, сохранить

![Настройка env.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/472777229-%D0%9D%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B0%20env.png)

* Перейти во вкладку pip, глянуть что есть

![Настройка env1.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/2468904576-%D0%9D%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B0%20env1.png)

* Скачать предложенные нам **pip** и **setuptools**, если их нет в списке приложений

![Настройка env2.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/3141815363-%D0%9D%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B0%20env2.png)

* Альтернатива, если они присутствуют в списке, то нажать на **стрелочку**, представленную ниже; сперва у **pip**, затем у **setuptools**

![projectsettings2.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/3946649918-projectsettings2.png)

* Задать проекту созданное нами виртуальное пространство

![Настройка проекта.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/2875871104-%D0%9D%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B0%20%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B0.png)

![Настройка проекта1.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/1766253398-%D0%9D%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B0%20%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B01.png)

* Назначить способ запуска: выбрать **web launcher** или **Django Web launcher**, если есть в списке

![personalenvironment1.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/c975637a52bd7743bd4b5788c03623cc7a00f18d/README%20Images/personalenvironment1.png)

* Установить при помощи **easy_install** скачанный нами ранее **win_psycopg**

![Настройка проекта3.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/1816030473-%D0%9D%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B0%20%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B03.png)

![Настройка проекта4.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/1751176283-%D0%9D%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B0%20%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B04.png)

* Установить при помощи **pip** скачанный нами ранее **Twisted**

![Настройка проекта3.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/1816030473-%D0%9D%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B0%20%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B03.png)

![настройка_пакетов.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/71b0d868b190f0261750ec0170b918dfb331436f/README%20Images/%D0%BD%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B0_%D0%BF%D0%B0%D0%BA%D0%B5%D1%82%D0%BE%D0%B2.png)

* Установить при помощи **pip** скачанный нами ранее **NumPY**

![настройка_пакетов1.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/71b0d868b190f0261750ec0170b918dfb331436f/README%20Images/%D0%BD%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B0%20%D0%BF%D0%B0%D0%BA%D0%B5%D1%82%D0%BE%D0%B21.png)

* Установить при помощи **pip** скачанный нами ранее **GDAL**

![настройка_пакетов2.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/71b0d868b190f0261750ec0170b918dfb331436f/README%20Images/%D0%BD%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B0%20%D0%BF%D0%B0%D0%BA%D0%B5%D1%82%D0%BE%D0%B22.png)

* Установить при помощи **pip** **channels**

![Безымянный1.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/2371704799-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B91.png)


![настройка_пакетов3.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/71b0d868b190f0261750ec0170b918dfb331436f/README%20Images/%D0%BD%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B0%20%D0%BF%D0%B0%D0%BA%D0%B5%D1%82%D0%BE%D0%B23.png)


* В автоматическом режиме скачать и установить пакеты из requirements.txt, в процессе следить за окном "Вывод", убедиться в успехе установок

![Настройка проекта2.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/97749294-%D0%9D%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B0%20%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B02.png)

* Открыть bash - интерфейс вашего виртуального пространства

![env-conf-ffmpeg.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/f9a427cbaa1c6e575dede4393a8f047a2d9fc617/README%20Images/env-conf-install-by-bash.PNG)

* Скачать **ffmpeg** при помощи скрипта, двумя следующими командами

![env-conf-ffmpeg.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/f9a427cbaa1c6e575dede4393a8f047a2d9fc617/README%20Images/env-conf-ffmpeg.PNG)

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

6. Тестирование

** Если были миграции до обновления, то удалить файлы из папки:**

	%appdata%\Local\Programs\Python\Python35\Lib\site-packages\cities\migrations

Проект скачен и почти готов к запуску в localhost. Осталось произвести миграции, собрать статику и создать суперпользователя

* Открыть "Обозреватель решений"

![vs4.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/2632151461-vs4.png)

* Поочерёдно выполнить следующие 4 команды, появится окно "Вывод", убедиться, что ошибок нет; указать данные от своего суперпользователя

![vs5.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/2408415930-vs5.png)

---------------------------------

* Импорт геоданных

![Безымянный1.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/2371704799-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B91.png)

* В командной строке выполнить следующие 4 команды, по порядку:

![111.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/737427560-111.png)

* Импортирование геоданных(для правильной работы форм выбора места проживания)
 
	manage.py cities --import=country --force

	manage.py cities --import=region --force

	manage.py cities --import=subregion --force

	manage.py cities --import=city --force
	
* Создание в базе данных таблиц кеша:

	manage.py createcachetable

*После каждой операции нужно дождаться появления строки ввода*

---------------------------------

* Пробный запуск локального проекта

![vs6.png](https://bytebucket.org/psixithps/psixisitedjangolocal/raw/96541f00bd5e02b3862193bd533dd17b6d37dc6e/README%20Images/506382156-vs6.png)

-------------------------------------------------------------------------------------------------------------

Заключение: создание альтернативного репозитория в виде решения для VS2015 оправдывает трудозатраты при разворачивании индивидуальных средств разработки, а также, благодаря использованию менеджера контроля версий git, сводит на нет риск утратить локальную версию проекта и делает совместную разработку продуктивной.